<?php
App::uses('CuposController', 'Controller');

/**
 * CuposController Test Case
 */
class CuposControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.cupo',
		'app.tipohabitacione',
		'app.consumo',
		'app.habitacione',
		'app.habistatus',
		'app.reserindividuale',
		'app.tipocliente',
		'app.cliente',
		'app.tipoclientesub',
		'app.pai',
		'app.tarjetacredito',
		'app.tipotarjeta',
		'app.auditoria',
		'app.resermultiple',
		'app.reserstatusmultiple',
		'app.resermulhabitacione',
		'app.factura',
		'app.facturatipopago',
		'app.facturapago',
		'app.facturadetalle',
		'app.facturatipoproducto',
		'app.user',
		'app.role',
		'app.rolesmodulo',
		'app.modulo',
		'app.personale',
		'app.reserstatusindividuale',
		'app.reserindivistatus',
		'app.conftipopagoreserva',
		'app.reserindividualeextra',
		'app.consumoproducto',
		'app.proproducto',
		'app.protipo',
		'app.proinventario',
		'app.promarca',
		'app.protipopiso',
		'app.proalmacene',
		'app.pstemporada',
		'app.tipotemporada'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
