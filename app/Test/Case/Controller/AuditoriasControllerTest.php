<?php
App::uses('AuditoriasController', 'Controller');

/**
 * AuditoriasController Test Case
 */
class AuditoriasControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.auditoria',
		'app.habitacione',
		'app.tipohabitacione',
		'app.habistatus',
		'app.reserindividuale',
		'app.tipocliente',
		'app.cliente',
		'app.tipoclientesub',
		'app.pai',
		'app.tarjetacredito',
		'app.tipotarjeta',
		'app.reserstatusindividuale',
		'app.resermultiple',
		'app.reserstatusmultiple',
		'app.resermulhabitacione',
		'app.reserindivistatus',
		'app.conftipopagoreserva',
		'app.reserindividualeextra',
		'app.factura',
		'app.facturadetalle',
		'app.facturatipoproducto',
		'app.user',
		'app.role',
		'app.rolesmodulo',
		'app.modulo',
		'app.personale'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
