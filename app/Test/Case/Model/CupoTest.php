<?php
App::uses('Cupo', 'Model');

/**
 * Cupo Test Case
 */
class CupoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.cupo',
		'app.tipohabitacione',
		'app.consumo',
		'app.habitacione',
		'app.habistatus',
		'app.reserindividuale',
		'app.tipocliente',
		'app.cliente',
		'app.tipoclientesub',
		'app.pai',
		'app.tarjetacredito',
		'app.tipotarjeta',
		'app.auditoria',
		'app.resermultiple',
		'app.reserstatusmultiple',
		'app.resermulhabitacione',
		'app.factura',
		'app.facturatipopago',
		'app.facturapago',
		'app.facturadetalle',
		'app.facturatipoproducto',
		'app.user',
		'app.role',
		'app.rolesmodulo',
		'app.modulo',
		'app.personale',
		'app.reserstatusindividuale',
		'app.reserindivistatus',
		'app.conftipopagoreserva',
		'app.reserindividualeextra',
		'app.consumoproducto',
		'app.proproducto',
		'app.protipo',
		'app.proinventario',
		'app.promarca',
		'app.protipopiso',
		'app.proalmacene',
		'app.pstemporada',
		'app.tipotemporada'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Cupo = ClassRegistry::init('Cupo');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Cupo);

		parent::tearDown();
	}

}
