<?php
App::uses('Auditorium', 'Model');

/**
 * Auditorium Test Case
 */
class AuditoriumTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Auditorium = ClassRegistry::init('Auditorium');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Auditorium);

		parent::tearDown();
	}

}
