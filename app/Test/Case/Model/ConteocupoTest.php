<?php
App::uses('Conteocupo', 'Model');

/**
 * Conteocupo Test Case
 */
class ConteocupoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.conteocupo'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Conteocupo = ClassRegistry::init('Conteocupo');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Conteocupo);

		parent::tearDown();
	}

}
