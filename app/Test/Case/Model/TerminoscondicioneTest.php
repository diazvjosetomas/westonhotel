<?php
App::uses('Terminoscondicione', 'Model');

/**
 * Terminoscondicione Test Case
 */
class TerminoscondicioneTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.terminoscondicione'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Terminoscondicione = ClassRegistry::init('Terminoscondicione');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Terminoscondicione);

		parent::tearDown();
	}

}
