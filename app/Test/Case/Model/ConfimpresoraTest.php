<?php
App::uses('Confimpresora', 'Model');

/**
 * Confimpresora Test Case
 *
 */
class ConfimpresoraTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.confimpresora'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Confimpresora = ClassRegistry::init('Confimpresora');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Confimpresora);

		parent::tearDown();
	}

}
