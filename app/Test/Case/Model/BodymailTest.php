<?php
App::uses('Bodymail', 'Model');

/**
 * Bodymail Test Case
 *
 */
class BodymailTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.bodymail'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Bodymail = ClassRegistry::init('Bodymail');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Bodymail);

		parent::tearDown();
	}

}
