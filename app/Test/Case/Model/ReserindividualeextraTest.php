<?php
App::uses('Reserindividualeextra', 'Model');

/**
 * Reserindividualeextra Test Case
 *
 */
class ReserindividualeextraTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.reserindividualeextra'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Reserindividualeextra = ClassRegistry::init('Reserindividualeextra');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Reserindividualeextra);

		parent::tearDown();
	}

}
