<?php
App::uses('Tmpreserva', 'Model');

/**
 * Tmpreserva Test Case
 */
class TmpreservaTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tmpreserva',
		'app.tipotemporada',
		'app.tipohabitacione',
		'app.consumo',
		'app.habitacione',
		'app.habistatus',
		'app.reserindividuale',
		'app.tipocliente',
		'app.cliente',
		'app.tipoclientesub',
		'app.pai',
		'app.tarjetacredito',
		'app.tipotarjeta',
		'app.auditoria',
		'app.resermultiple',
		'app.reserstatusmultiple',
		'app.resermulhabitacione',
		'app.factura',
		'app.facturatipopago',
		'app.facturapago',
		'app.facturadetalle',
		'app.facturatipoproducto',
		'app.user',
		'app.role',
		'app.rolesmodulo',
		'app.modulo',
		'app.personale',
		'app.reserstatusindividuale',
		'app.reserindivistatus',
		'app.conftipopagoreserva',
		'app.reserindividualeextra',
		'app.consumoproducto',
		'app.proproducto',
		'app.protipo',
		'app.proinventario',
		'app.promarca',
		'app.protipopiso',
		'app.proalmacene',
		'app.pstemporada',
		'app.cupo'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Tmpreserva = ClassRegistry::init('Tmpreserva');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Tmpreserva);

		parent::tearDown();
	}

}
