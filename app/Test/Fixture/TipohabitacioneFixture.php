<?php
/**
 * Tipohabitacione Fixture
 */
class TipohabitacioneFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary'),
		'denominacion' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 30, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'cantadultos' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'cantninos' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'cantbebes' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'id' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'denominacion' => 'Lorem ipsum dolor sit amet',
			'created' => '2018-08-16 13:00:05',
			'modified' => '2018-08-16 13:00:06',
			'cantadultos' => 1,
			'cantninos' => 1,
			'cantbebes' => 1
		),
	);

}
