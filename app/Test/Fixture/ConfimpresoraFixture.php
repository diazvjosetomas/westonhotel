<?php
/**
 * ConfimpresoraFixture
 *
 */
class ConfimpresoraFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'denominacion' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 90, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'puerto' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'tipodocumento' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'salidaimpresora' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'letradedocumento' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'cantcopias' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'tipodeformulario' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'sizecaracter' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'respvendedoriva' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'respclienteiva' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'formalmacenadatos' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'date', 'null' => false, 'default' => null),
		'modified' => array('type' => 'date', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'denominacion' => 'Lorem ipsum dolor sit amet',
			'puerto' => 1,
			'tipodocumento' => 'Lorem ipsum dolor sit ame',
			'salidaimpresora' => 'Lorem ipsum dolor sit ame',
			'letradedocumento' => 'Lorem ipsum dolor sit ame',
			'cantcopias' => 1,
			'tipodeformulario' => 'Lorem ipsum dolor sit ame',
			'sizecaracter' => 'Lorem ipsum dolor sit ame',
			'respvendedoriva' => 'Lorem ipsum dolor sit ame',
			'respclienteiva' => 'Lorem ipsum dolor sit ame',
			'formalmacenadatos' => 'Lorem ipsum dolor sit ame',
			'created' => '2017-01-18',
			'modified' => '2017-01-18'
		),
	);

}
