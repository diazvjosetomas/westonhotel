<?php
/**
 * Tmpreserva Fixture
 */
class TmpreservaFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'identificador' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 999, 'collate' => 'utf8mb4_general_ci', 'charset' => 'utf8mb4'),
		'tipotemporada_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'tipohabitacione_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'cupo_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'cant_personas' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8mb4', 'collate' => 'utf8mb4_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'identificador' => 'Lorem ipsum dolor sit amet',
			'tipotemporada_id' => 1,
			'tipohabitacione_id' => 1,
			'cupo_id' => 1,
			'cant_personas' => 1
		),
	);

}
