<?php
App::uses('AppModel', 'Model');
/**
 * Resermulhabitacione Model
 *
 * @property Resermultiple $Resermultiple
 * @property Habitacione $Habitacione
 */
class Resermulhabitacione extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'resermultiple_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'habitacione_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Resermultiple' => array(
			'className' => 'Resermultiple',
			'foreignKey' => 'resermultiple_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Habitacione' => array(
			'className' => 'Habitacione',
			'foreignKey' => 'habitacione_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
