<?php
App::uses('AppModel', 'Model');
/**
 * Cliente Model
 *
 * @property Tipocliente $Tipocliente
 * @property Tipoclientesub $Tipoclientesub
 * @property Tarjetacredito $Tarjetacredito
 */
class Cliente extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'da-ci';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'tipocliente_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
        /*'dni' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Campo vacio!'
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'message' => 'Dni Existe!'
            )
        ),
        'nombre_completo' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'campo vacio',
                //'allowEmpty' => false,
                //'required' => false                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),

        /*'fecha_nacimiento1' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Campo vacio'
            ),
        ),

        'codigo_postal' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Campo vacio',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
        /*'telf' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Campo vacio',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' ='create', // Limit validation to 'create' or 'update' operations
            ),
        ),

        /*'telf' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Campo vacio',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),

        'email' => array(
            'email' => array(
                'rule' => array('email'),
                'message' => 'Campo email',
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'message' => 'Email Existente',
            ),
        ),*/
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Tipocliente' => array(
			'className' => 'Tipocliente',
			'foreignKey' => 'tipocliente_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Tipoclientesub' => array(
			'className' => 'Tipoclientesub',
			'foreignKey' => 'tipoclientesub_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
		,
		'Pai' => array(
			'className' => 'Pai',
			'foreignKey' => 'pai_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Tarjetacredito' => array(
			'className' => 'Tarjetacredito',
			'foreignKey' => 'cliente_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),

		'Auditoria' => array(
			'className' => 'Auditoria',
			'foreignKey' => 'cliente_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	);


	var $virtualFields = array('da-ci'  =>"CONCAT(Cliente.dni,' - ',Cliente.nombre_completo)");

}
