<?php
App::uses('AppModel', 'Model');
/**
 * Proveedore Model
 *
 */
class Proveedore extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'razon_social';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'cuit' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo vacio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'razon_social' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo vacio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'direccion' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo vacio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'telefono' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo vacio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				'message' => 'email invalido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'impuesto' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo vacio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
