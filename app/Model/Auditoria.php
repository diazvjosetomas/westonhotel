<?php
App::uses('AppModel', 'Model');
/**
 * Auditoria Model
 *
 * @property Habitacione $Habitacione
 * @property Reserindividuale $Reserindividuale
 * @property Resermultiple $Resermultiple
 * @property Cliente $Cliente
 * @property Factura $Factura
 * @property User $User
 */
class Auditoria extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Habitacione' => array(
			'className' => 'Habitacione',
			'foreignKey' => 'habitacione_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Reserindividuale' => array(
			'className' => 'Reserindividuale',
			'foreignKey' => 'reserindividuale_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),

		'Resermultiple' => array(
			'className' => 'Resermultiple',
			'foreignKey' => 'resermultiple_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		
		'Cliente' => array(
			'className' => 'Cliente',
			'foreignKey' => 'cliente_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Factura' => array(
			'className' => 'Factura',
			'foreignKey' => 'factura_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
