<?php
App::uses('AppModel', 'Model');
/**
 * Cupo Model
 *
 * @property Tipohabitacione $Tipohabitacione
 */
class Cupo extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'fecha';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'fecha' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'tipohabitacione_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cantidad' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Tipohabitacione' => array(
			'className' => 'Tipohabitacione',
			'foreignKey' => 'tipohabitacione_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Tipotemporada' => array(
			'className' => 'Tipotemporada',
			'foreignKey' => 'tipotemporada_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);


		// public $hasMany = array(

		// 	'Tipotemporada' => array(
		// 		'className' => 'Tipotemporada',
		// 		'foreignKey' => 'id',
		// 		'dependent' => false,
		// 		'conditions' => '',
		// 		'fields' => '',
		// 		'order' => '',
		// 		'limit' => '',
		// 		'offset' => '',
		// 		'exclusive' => '',
		// 		'finderQuery' => '',
		// 		'counterQuery' => ''
		// 	)
		// );

}



