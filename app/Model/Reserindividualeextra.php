<?php
App::uses('AppModel', 'Model');
/**
 * Reserindividualeextra Model
 *
 */
class Reserindividualeextra extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nombres';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'dni' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'nombres' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
