<?php
App::uses('AppModel', 'Model');
/**
 * Reserindivistatus Model
 *
 * @property Reserindividuale $Reserindividuale
 * @property Reserstatusindividuale $Reserstatusindividuale
 * @property Conftipopagoreserva $Conftipopagoreserva
 */
class Reserindivistatus extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'reserindivistatus';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'reserindividuale_id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'reserindividuale_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'reserstatusindividuale_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'fecha' => array(
			'date' => array(
				'rule' => array('date'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Reserindividuale' => array(
			'className' => 'Reserindividuale',
			'foreignKey' => 'reserindividuale_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Reserstatusindividuale' => array(
			'className' => 'Reserstatusindividuale',
			'foreignKey' => 'reserstatusindividuale_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Conftipopagoreserva' => array(
			'className' => 'Conftipopagoreserva',
			'foreignKey' => 'conftipopagoreserva_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
