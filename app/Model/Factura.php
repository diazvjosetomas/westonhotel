<?php
App::uses('AppModel', 'Model');
/**
 * Factura Model
 *
 * @property Tipocliente $Tipocliente
 * @property Tipoclientesub $Tipoclientesub
 * @property Cliente $Cliente
 * @property Facturadetalle $Facturadetalle
 */
class Factura extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'numero';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'numero' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo vacio',
			),
			'isUnique' => array(
				'rule' => array('isUnique'),
				'message' => 'Intente nuevamente el numero de la factura fue utilizado',
			),
		),
		'fecha' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo vacio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'tipocliente_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				
			),
		),
		'tipoclientesub_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cliente_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'concepto' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo vacio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'total' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'pagado' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Tipocliente' => array(
			'className' => 'Tipocliente',
			'foreignKey' => 'tipocliente_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Tipoclientesub' => array(
			'className' => 'Tipoclientesub',
			'foreignKey' => 'tipoclientesub_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Cliente' => array(
			'className' => 'Cliente',
			'foreignKey' => 'cliente_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		
		'Reserindividuale' => array(
			'className' => 'Reserindividuale',
			'foreignKey' => 'Reserindividuale_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),

		'Facturatipopago' => array(
			'className' => 'Facturatipopago',
			'foreignKey' => 'facturatipopago_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Facturadetalle' => array(
			'className' => 'Facturadetalle',
			'foreignKey' => 'factura_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),

		'Auditoria' => array(
			'className' => 'Auditoria',
			'foreignKey' => 'factura_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	);

}
