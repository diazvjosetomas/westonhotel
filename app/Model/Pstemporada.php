<?php
App::uses('AppModel', 'Model');
/**
 * Pstemporada Model
 *
 * @property Tipotemporada $Tipotemporada
 * @property Tipohabitacione $Tipohabitacione
 */
class Pstemporada extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'tipohabitacione_id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'tipotemporada_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'tipohabitacione_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'precio' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Tipotemporada' => array(
			'className' => 'Tipotemporada',
			'foreignKey' => 'tipotemporada_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Tipohabitacione' => array(
			'className' => 'Tipohabitacione',
			'foreignKey' => 'tipohabitacione_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
