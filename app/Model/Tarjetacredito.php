<?php
App::uses('AppModel', 'Model');
/**
 * Tarjetacredito Model
 *
 * @property Cliente $Cliente
 * @property Tipotarjeta $Tipotarjeta
 */
class Tarjetacredito extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'num_tajeta';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'tipotarjeta_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo vacio',
			),
		),
		'cliente_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo vacio',
			),
		),
		'num_tajeta' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo vacio',
			),
			'isUnique' => array(
				'rule' => array('isUnique'),
				'message' => 'El numero de tarjeta existe!',
			),
		),
		'vencimiento' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo vacio',
			),
		),
		'nombre_segun_tarjeta' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo vacio',
				
			),
		),
		'cuatro_ultimos' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo vacio',
				
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Cliente' => array(
			'className' => 'Cliente',
			'foreignKey' => 'cliente_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Tipotarjeta' => array(
			'className' => 'Tipotarjeta',
			'foreignKey' => 'tipotarjeta_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
