<?php
App::uses('AppModel', 'Model');
/**
 * Compra Model
 *
 * @property Proveedore $Proveedore
 * @property Tipocompra $Tipocompra
 */
class Compra extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'num_compra';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'proveedore_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo vacio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'tipocompra_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo vacio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'ano_compra' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo vacio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'num_compra' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo vacio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'fecha_compra' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo vacio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'num_factura' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo vacio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Proveedore' => array(
			'className' => 'Proveedore',
			'foreignKey' => 'proveedore_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Tipocompra' => array(
			'className' => 'Tipocompra',
			'foreignKey' => 'tipocompra_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
