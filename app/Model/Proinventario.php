<?php
App::uses('AppModel', 'Model');
/**
 * Proinventario Model
 *
 * @property Protipo $Protipo
 * @property Promarca $Promarca
 * @property Proproducto $Proproducto
 * @property Protipopiso $Protipopiso
 * @property Proalmacene $Proalmacene
 */
class Proinventario extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'proalmacene_id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'protipo_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'promarca_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'proproducto_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'protipopiso_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'proalmacene_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cantidad' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cantidadminima' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Protipo' => array(
			'className' => 'Protipo',
			'foreignKey' => 'protipo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Promarca' => array(
			'className' => 'Promarca',
			'foreignKey' => 'promarca_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Proproducto' => array(
			'className' => 'Proproducto',
			'foreignKey' => 'proproducto_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Protipopiso' => array(
			'className' => 'Protipopiso',
			'foreignKey' => 'protipopiso_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Proalmacene' => array(
			'className' => 'Proalmacene',
			'foreignKey' => 'proalmacene_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
