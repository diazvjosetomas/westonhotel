<?php
App::uses('AppModel', 'Model');
/**
 * Resermultiple Model
 *
 * @property Tipocliente $Tipocliente
 * @property Tipoclientesub $Tipoclientesub
 * @property Cliente $Cliente
 * @property Tipohabitacione $Tipohabitacione
 * @property Reserstatusmultiple $Reserstatusmultiple
 * @property Reserindividuale $Reserindividuale
 * @property Resermulhabitacione $Resermulhabitacione
 */
class Resermultiple extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'tipocliente_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'tipoclientesub_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cliente_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'tipohabitacione_id' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo vacio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cantidad' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo vacio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'fecha_entrada' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo vacio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'fecha_salida' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo vacio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'fecha_tope' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo vacio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'dias' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo vacio',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'precioxdia' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo numerico',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'total' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo numerico',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Tipocliente' => array(
			'className' => 'Tipocliente',
			'foreignKey' => 'tipocliente_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Tipoclientesub' => array(
			'className' => 'Tipoclientesub',
			'foreignKey' => 'tipoclientesub_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Cliente' => array(
			'className' => 'Cliente',
			'foreignKey' => 'cliente_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Tipohabitacione' => array(
			'className' => 'Tipohabitacione',
			'foreignKey' => 'tipohabitacione_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Reserstatusmultiple' => array(
			'className' => 'Reserstatusmultiple',
			'foreignKey' => 'reserstatusmultiple_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Reserindividuale' => array(
			'className' => 'Reserindividuale',
			'foreignKey' => 'resermultiple_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Resermulhabitacione' => array(
			'className' => 'Resermulhabitacione',
			'foreignKey' => 'resermultiple_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Auditoria' => array(
			'className' => 'Auditoria',
			'foreignKey' => 'resermultiple_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
