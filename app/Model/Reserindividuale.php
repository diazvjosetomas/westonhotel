<?php
App::uses('AppModel', 'Model');
/**
 * Reserindividuale Model
 *
 * @property Tipocliente $Tipocliente
 * @property Tipoclientesub $Tipoclientesub
 * @property Cliente $Cliente
 * @property Tipohabitacione $Tipohabitacione
 * @property Habitacione $Habitacione
 * @property Reserstatusindividuale $Reserstatusindividuale
 * @property Resermultiple $Resermultiple
 */
class Reserindividuale extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'tipocliente_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'tipoclientesub_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cliente_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'tipohabitacione_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'habitacione_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'fecha_entrada' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'fecha_salida' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'dias' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'precioxdia' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'total' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Tipocliente' => array(
			'className' => 'Tipocliente',
			'foreignKey' => 'tipocliente_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Tipoclientesub' => array(
			'className' => 'Tipoclientesub',
			'foreignKey' => 'tipoclientesub_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Cliente' => array(
			'className' => 'Cliente',
			'foreignKey' => 'cliente_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Tipohabitacione' => array(
			'className' => 'Tipohabitacione',
			'foreignKey' => 'tipohabitacione_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Habitacione' => array(
			'className' => 'Habitacione',
			'foreignKey' => 'habitacione_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Reserstatusindividuale' => array(
			'className' => 'Reserstatusindividuale',
			'foreignKey' => 'reserstatusindividuale_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Resermultiple' => array(
			'className' => 'Resermultiple',
			'foreignKey' => 'resermultiple_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),

		
	);


	public $hasMany = array(
		'Reserindivistatus' => array(
			'className' => '',
			'foreignKey' => 'reserindividuale_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Reserindividualeextra' => array(
			'className' => '',
			'foreignKey' => 'reserindividuale_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),

		'Auditoria' => array(
			'className' => '',
			'foreignKey' => 'reserindividuale_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),


		'Factura' => array(
			'className' => '',
			'foreignKey' => 'reserindividuale_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),


	);



	//var $virtualFields = array('ha-ci'  =>"CONCAT(Habitacione.numhabitacion,' - ',Cliente.nombre_completo)");
}
