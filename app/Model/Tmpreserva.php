<?php
App::uses('AppModel', 'Model');
/**
 * Tmpreserva Model
 *
 * @property Tipotemporada $Tipotemporada
 * @property Tipohabitacione $Tipohabitacione
 * @property Cupo $Cupo
 */
class Tmpreserva extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'identificador';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'identificador' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'tipotemporada_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'tipohabitacione_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cupo_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cant_personas' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Tipotemporada' => array(
			'className' => 'Tipotemporada',
			'foreignKey' => 'tipotemporada_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Tipohabitacione' => array(
			'className' => 'Tipohabitacione',
			'foreignKey' => 'tipohabitacione_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Cupo' => array(
			'className' => 'Cupo',
			'foreignKey' => 'cupo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
