<?php
App::uses('AppController', 'Controller');
/**
 * Cajas Controller
 *
 * @property Caja $Caja
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class EstadisticasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');


	var $uses = array('Reserindividuale', 'Habitacione', 'Cliente', 'Consumo', 'Factura');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(12);
	}

/**
 * ocupacion method
 *
 * @return void
 */
	public function ocupacion() {
		if ($this->request->is('post')) {
			        $op1 = $this->request->data['Ocupacion']['tipo'];
			        $op2 = $this->request->data['Ocupacion']['datos'];
			        $op3 = $this->request->data['Ocupacion']['estatus'];
             return $this->redirect(array('action' => 'ocupaciongrafica/'.$op1.'/'.$op2.'/'.$op3));
		}
		$tipo = array('1'=>'Año', '2'=>'Mes', '3'=>'dia');
		$this->set('tipo', $tipo);

	}


		public function ocupaciongraficaajax($var1=null, $var2=null, $var3=null) {
				$this->layout = 'ajax';
		        $cantidad_hbitaciones = $this->Habitacione->find('count');
		        $data[1]  = "Enero";
	    		$data[2]  = "Febrero";
	    		$data[3]  = "Marzo";
	    		$data[4]  = "Abril";
	    		$data[5]  = "Mayo";
	    		$data[6]  = "Junio";
	    		$data[7]  = "Julio";
	    		$data[8]  = "Agosto";
	    		$data[9]  = "Septiembre";
	    		$data[10] = "Obctubre";
	    		$data[11] = "Noviembre";
	    		$data[12] = "Diciembre";
		
		if($var1=='1'){ 
		      	$fechaa = $var2.'-'.'01-01';
		      	$fechab = $var2.'-'.'12-31';
		      	$titulo   = " Grafica Ocupaciones año: ".$var2;
		      	$cantidad = $this->Reserindividuale->find('count', array('fields'=>'DISTINCT Habitacione.id', 'conditions'=>array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($var3)), 'Reserindividuale.fecha_entrada >='=>$fechaa, 'Reserindividuale.fecha_entrada <='=>$fechab )));
    	}else if($var1=='2'){ 
    		    $titulo = " Grafica Ocupaciones mes: ".$data[$var2];
    			$fechaa = date('Y').'-'.$var2.'-01';
		      	$fechab = date('Y').'-'.$var2.'-31';
	    		$cantidad = $this->Reserindividuale->find('count', array('fields'=>'DISTINCT Habitacione.id', 'conditions'=>array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($var3)), 'Reserindividuale.fecha_entrada >='=>$fechaa, 'Reserindividuale.fecha_entrada <='=>$fechab )));
    	}else if($var1=='3'){ 
    		    $titulo   = " Grafica Ocupaciones dia: ".$var2;
    	        $cantidad = $this->Reserindividuale->find('count', array('fields'=>'DISTINCT Habitacione.id', 'conditions'=>array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($var3)), 'Reserindividuale.fecha_entrada'=>$var2)));

    	}
    	$this->set('cantidad_hbitaciones', $cantidad_hbitaciones);
    	$this->set('var', $var1);
    	$this->set('titulo', $titulo);
    	$this->set('cantidad', $cantidad);
	}
















		public function ocupaciongrafica($var1=null, $var2=null, $var3=null) {

			        $cantidad_hbitaciones = $this->Habitacione->find('count');
			        $data[1]  = "Enero";
		    		$data[2]  = "Febrero";
		    		$data[3]  = "Marzo";
		    		$data[4]  = "Abril";
		    		$data[5]  = "Mayo";
		    		$data[6]  = "Junio";
		    		$data[7]  = "Julio";
		    		$data[8]  = "Agosto";
		    		$data[9]  = "Septiembre";
		    		$data[10] = "Octubre";
		    		$data[11] = "Noviembre";
		    		$data[12] = "Diciembre";

		    		$cant = 0;
		    		$dias_diferencia = 0;
		    		$segundos_diferencia = 0;


		    		$timestamp1 = 0;
		    		$timestamp2 = 0;


		    		//Cantidad de dias en el mes
		    		$numero = cal_days_in_month(CAL_GREGORIAN, $var2, date('Y'));
		    		for ($i=1; $i <= $numero ; $i++) { 

		    			$arraycont[$i] = 0;

		    		}



			
			if($var1=='1'){ 
			      	$fechaa = $var2.'-'.'01-01';
			      	$fechab = $var2.'-'.'12-31';
			      	$titulo   = " Grafica Ocupaciones año: ".$var2;

					if ($var3 == 7) {
						$cantidad = $this->Reserindividuale->find('all', array('or'=>array('Reserindividuale.fecha_entrada >='=>$fechaa, 'Reserindividuale.fecha_entrada <='=>$fechab )));
					}else{
						$cantidad = $this->Reserindividuale->find('all', array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($var3)), 'Reserindividuale.fecha_entrada >='=>$fechaa, 'Reserindividuale.fecha_entrada <='=>$fechab ));
					}


					foreach ($cantidad as $cant) {
						
						
						$fecha1 = $cant['Reserindividuale']['fecha_entrada'];
						$fecha2 = $cant['Reserindividuale']['fecha_salida'];


						

						$fecha1 = explode('-', $fecha1);
						$fecha2 = explode('-', $fecha2);

						$dia1 = $fecha1[2];
						$mes1 = $fecha1[1];
						$anho1 = $fecha1[0];



						$dia2 = $fecha2[2];
						$mes2 = $fecha2[1];
						$anho2 = $fecha2[0];

						$timestamp1 += mktime(0,0,0,$mes1,$dia1,$anho1);				

						$timestamp2 += mktime(0,0,0,$mes2,$dia2,$anho2);

						


						
						
					}


						

						


						$segundos_diferencia =  ($timestamp1 - $timestamp2);

						$segundos_diferencia = abs($segundos_diferencia);
						$cantidad =  ($segundos_diferencia / (60 * 60 * 24));




	    	}else if($var1=='2'){ 
	    		    $titulo = " Grafica Ocupaciones mes: ".$data[$var2];
	    			$fechaa = date('Y').'-'.$var2.'-01';
			      	$fechab = date('Y').'-'.$var2.'-31';		    		


			      	/**
			      	 * Todas dentro del mes
			      	 */

		    		$cantidad = $this->Reserindividuale->find('all', 
		    			array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$var3,
		    				'Reserindividuale.fecha_entrada BETWEEN ? AND ? '=>array($fechaa, $fechab),  'Reserindividuale.fecha_salida BETWEEN ? AND ? '=>array($fechaa, $fechab))));

		    		/**
		    		 * Todas entran dentro del mes y salen fuera del mes
		    		 */

		    		$cantidad1 = $this->Reserindividuale->find('all', 
		    			array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$var3,
		    				'Reserindividuale.fecha_entrada BETWEEN ? AND ? '=>array($fechaa, $fechab),  'Reserindividuale.fecha_salida > ' => $fechab)));

		    		/**
		    		 * [$cantidad2 description]
		    		 * @var number
		    		 * Todas entran antes del mes, y salen el mes
		    		 */
		    		$cantidad2 = $this->Reserindividuale->find('all', 
		    			array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$var3,
		    				'Reserindividuale.fecha_salida BETWEEN ? AND ? '=>array($fechaa, $fechab),  'Reserindividuale.fecha_entrada <'=>$fechaa)));


		    		/**
		    		 * Empiezan antes y terminan despues del mes seleccionado
		    		 */

		    		$cantidad3 = $this->Reserindividuale->find('all', 
		    			array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$var3,
		    				'Reserindividuale.fecha_salida > '=> $fechab ,  'Reserindividuale.fecha_entrada < '=>$fechaa )));


		    		$cantidadarray = array_merge($cantidad, $cantidad1, $cantidad2, $cantidad3);




		    		foreach ($cantidadarray as $cant) {
		    			
		    			
		    			$fecha1 = $cant['Reserindividuale']['fecha_entrada'];
		    			$fecha2 = $cant['Reserindividuale']['fecha_salida'];


		    			

		    			$fecha1 = explode('-', $fecha1);
		    			$fecha2 = explode('-', $fecha2);

		    			$dia1 = $fecha1[2];
		    			$mes1 = $fecha1[1];
		    			$anho1 = $fecha1[0];



		    			$dia2 = $fecha2[2];
		    			$mes2 = $fecha2[1];
		    			$anho2 = $fecha2[0];

		    			$timestamp1 += mktime(0,0,0,$mes1,$dia1,$anho1);				

		    			$timestamp2 += mktime(0,0,0,$mes2,$dia2,$anho2);

		    			if ($mes2 > $mes1) {
		    				$dia2 = $numero;
		    			}



		    			for ($i = $dia1; $i <= $dia2; $i++) { 
		    				$arraycont[$i] = $arraycont[$i] + 1;
		    			}



		    			
		    		}





		    			
		    			$cat = '[';
		    			$dat = '[';

		    			for ($i=1; $i <= $numero ; $i++) { 
		    				

		    				$cat .= $i.','; 
		    				$dat .= $arraycont[$i].',';
		    				//$var .= '{y: '.$arraycont[$i].', label: '.$i.'},';
		    			}

		    			$cat .= ']'; 
		    			$dat .= ']'; 
		    			
		    			$this->set('cat', $cat);
		    			$this->set('dat', $dat);

		    			$segundos_diferencia =  ($timestamp1 - $timestamp2);

		    			$segundos_diferencia = abs($segundos_diferencia);
		    			$cantidad =  ($segundos_diferencia / (60 * 60 * 24));



		    		if ($var3 == 7) {
		    			//$cantidad = $this->Reserindividuale->find('count', array('fields'=>'DISTINCT Habitacione.id', 'conditions'=>array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($var3)), 'Reserindividuale.fecha_entrada >='=>$fechaa, 'Reserindividuale.fecha_entrada <='=>$fechab )));
		    			    		


		    	      	/**
				      	* Todas dentro del mes
		    			*/

		    			$cantidad = $this->Reserindividuale->find('all', 
		    			  			array('conditions'=>array('Reserindividuale.fecha_entrada BETWEEN ? AND ? '=>array($fechaa, $fechab),  'Reserindividuale.fecha_salida BETWEEN ? AND ? '=>array($fechaa, $fechab))));

		    			/**
		    			* Todas entran dentro del mes y salen fuera del mes
		    			*/

		    			$cantidad1 = $this->Reserindividuale->find('all', 
		    			  			array('conditions'=>array('Reserindividuale.fecha_entrada BETWEEN ? AND ? '=>array($fechaa, $fechab),  'Reserindividuale.fecha_salida > ' => $fechab)));

		    			/**
		    			* [$cantidad2 description]
		    			* @var number
		    			* Todas entran antes del mes, y salen el mes
		    			*/
		    			$cantidad2 = $this->Reserindividuale->find('all', 
		    			  			array('conditions'=>array('Reserindividuale.fecha_salida BETWEEN ? AND ? '=>array($fechaa, $fechab),  'Reserindividuale.fecha_entrada <'=>$fechaa)));


		    			/**
		    			* Empiezan antes y terminan despues del mes seleccionado
		    			*/

		    			$cantidad3 = $this->Reserindividuale->find('all', 
		    			  			array('conditions'=>array('Reserindividuale.fecha_salida > '=> $fechab ,  'Reserindividuale.fecha_entrada < '=>$fechaa )));
		    			
		    			$cantidadarray = array_merge($cantidad, $cantidad1, $cantidad2, $cantidad3);




		    			foreach ($cantidadarray as $cant) {
		    				
		    				
		    				$fecha1 = $cant['Reserindividuale']['fecha_entrada'];
		    				$fecha2 = $cant['Reserindividuale']['fecha_salida'];


		    				

		    				$fecha1 = explode('-', $fecha1);
		    				$fecha2 = explode('-', $fecha2);

		    				$dia1 = $fecha1[2];
		    				$mes1 = $fecha1[1];
		    				$anho1 = $fecha1[0];



		    				$dia2 = $fecha2[2];
		    				$mes2 = $fecha2[1];
		    				$anho2 = $fecha2[0];

		    				$timestamp1 += mktime(0,0,0,$mes1,$dia1,$anho1);				

		    				$timestamp2 += mktime(0,0,0,$mes2,$dia2,$anho2);

		    				


		    				
		    				
		    			}


		    				

		    				


		    				$segundos_diferencia =  ($timestamp1 - $timestamp2);

		    				$segundos_diferencia = abs($segundos_diferencia);
		    				$cantidad =  ($segundos_diferencia / (60 * 60 * 24));
		    		}












	    	}else if($var1=='3'){ 
	    		    $titulo   = " Grafica Ocupaciones dia: ".$var2;
	    	       


	    		    //$cantidad = $this->Reserindividuale->find('count', array('fields'=>'DISTINCT Habitacione.id', 'conditions'=>array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($var3)), 'Reserindividuale.fecha_entrada'=>$var2)));


	    	        $cantidad = $this->Reserindividuale->find('all', 
	    	        	array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$var3, 
	    	        		          'or'=>array('Reserindividuale.fecha_entrada'=>$var2,'Reserindividuale.fecha_salida'=>$var2))));
	    		    if ($var3 == 7) {
	    		    	 $cantidad = $this->Reserindividuale->find('all', 
	    	        	array('conditions'=>array('or'=>array('Reserindividuale.fecha_entrada'=>$var2,'Reserindividuale.fecha_salida'=>$var2))));
	    		    }


	    	




	    	foreach ($cantidad as $cant) {
	    		
	    		
	    		$fecha1 = $cant['Reserindividuale']['fecha_entrada'];
	    		$fecha2 = $cant['Reserindividuale']['fecha_salida'];


	    		

	    		$fecha1 = explode('-', $fecha1);
	    		$fecha2 = explode('-', $fecha2);

	    		$dia1 = $fecha1[2];
	    		$mes1 = $fecha1[1];
	    		$anho1 = $fecha1[0];



	    		$dia2 = $fecha2[2];
	    		$mes2 = $fecha2[1];
	    		$anho2 = $fecha2[0];

	    		$timestamp1 += mktime(0,0,0,$mes1,$dia1,$anho1);				

	    		$timestamp2 += mktime(0,0,0,$mes2,$dia2,$anho2);

	    		


	    		
	    		
	    	}


	    		

	    		


	    		$segundos_diferencia =  ($timestamp1 - $timestamp2);

	    		$segundos_diferencia = abs($segundos_diferencia);
	    		$cantidad =  ($segundos_diferencia / (60 * 60 * 24));



	    	}
	    	$this->set('cantidad_hbitaciones', $cantidad_hbitaciones);
	    	$this->set('var', $var1);
	    	$this->set('titulo', $titulo);
	    	$this->set('cantidad', $cantidad);
	    	$this->set('tipografica', $var1);
		}
















	public function ocupaciongraficaolder($var1=null, $var2=null, $var3=null) {

		        $cantidad_hbitaciones = $this->Habitacione->find('count');
		        $data[1]  = "Enero";
	    		$data[2]  = "Febrero";
	    		$data[3]  = "Marzo";
	    		$data[4]  = "Abril";
	    		$data[5]  = "Mayo";
	    		$data[6]  = "Junio";
	    		$data[7]  = "Julio";
	    		$data[8]  = "Agosto";
	    		$data[9]  = "Septiembre";
	    		$data[10] = "Octubre";
	    		$data[11] = "Noviembre";
	    		$data[12] = "Diciembre";


		
		if($var1=='1'){ 
		      	$fechaa = $var2.'-'.'01-01';
		      	$fechab = $var2.'-'.'12-31';
		      	$titulo   = " Grafica Ocupaciones año: ".$var2;

				if ($var3 == 7) {
					$cantidad = $this->Reserindividuale->find('count', array('or'=>array('Reserindividuale.fecha_entrada >='=>$fechaa, 'Reserindividuale.fecha_entrada <='=>$fechab )));
				}else{
					$cantidad = $this->Reserindividuale->find('count', array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($var3)), 'Reserindividuale.fecha_entrada >='=>$fechaa, 'Reserindividuale.fecha_entrada <='=>$fechab ));
				}


    	}else if($var1=='2'){ 
    		    $titulo = " Grafica Ocupaciones mes: ".$data[$var2];
    			$fechaa = date('Y').'-'.$var2.'-01';
		      	$fechab = date('Y').'-'.$var2.'-31';


		      	//$cantidad = $this->Reserindividuale->find('count', array('fields'=>'DISTINCT Habitacione.id', 'conditions'=>array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($var3)), 'Reserindividuale.fecha_entrada >='=>$fechaa, 'Reserindividuale.fecha_entrada <='=>$fechab )));
	    		


		      	/**
		      	 * Todas dentro del mes
		      	 */

	    		$cantidad = $this->Reserindividuale->find('count', 
	    			array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$var3,
	    				'Reserindividuale.fecha_entrada BETWEEN ? AND ? '=>array($fechaa, $fechab),  'Reserindividuale.fecha_salida BETWEEN ? AND ? '=>array($fechaa, $fechab))));

	    		/**
	    		 * Todas entran dentro del mes y salen fuera del mes
	    		 */

	    		$cantidad1 = $this->Reserindividuale->find('count', 
	    			array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$var3,
	    				'Reserindividuale.fecha_entrada BETWEEN ? AND ? '=>array($fechaa, $fechab),  'Reserindividuale.fecha_salida > ' => $fechab)));

	    		/**
	    		 * [$cantidad2 description]
	    		 * @var number
	    		 * Todas entran antes del mes, y salen el mes
	    		 */
	    		$cantidad2 = $this->Reserindividuale->find('count', 
	    			array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$var3,
	    				'Reserindividuale.fecha_salida BETWEEN ? AND ? '=>array($fechaa, $fechab),  'Reserindividuale.fecha_entrada <'=>$fechaa)));


	    		/**
	    		 * Empiezan antes y terminan despues del mes seleccionado
	    		 */

	    		$cantidad3 = $this->Reserindividuale->find('count', 
	    			array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$var3,
	    				'Reserindividuale.fecha_salida > '=> $fechab ,  'Reserindividuale.fecha_entrada < '=>$fechaa )));




	    		if ($var3 == 7) {
	    			//$cantidad = $this->Reserindividuale->find('count', array('fields'=>'DISTINCT Habitacione.id', 'conditions'=>array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($var3)), 'Reserindividuale.fecha_entrada >='=>$fechaa, 'Reserindividuale.fecha_entrada <='=>$fechab )));
	    			    		


	    	      	/**
			      	* Todas dentro del mes
	    			*/

	    			$cantidad = $this->Reserindividuale->find('count', 
	    			  			array('conditions'=>array('Reserindividuale.fecha_entrada BETWEEN ? AND ? '=>array($fechaa, $fechab),  'Reserindividuale.fecha_salida BETWEEN ? AND ? '=>array($fechaa, $fechab))));

	    			/**
	    			* Todas entran dentro del mes y salen fuera del mes
	    			*/

	    			$cantidad1 = $this->Reserindividuale->find('count', 
	    			  			array('conditions'=>array('Reserindividuale.fecha_entrada BETWEEN ? AND ? '=>array($fechaa, $fechab),  'Reserindividuale.fecha_salida > ' => $fechab)));

	    			/**
	    			* [$cantidad2 description]
	    			* @var number
	    			* Todas entran antes del mes, y salen el mes
	    			*/
	    			$cantidad2 = $this->Reserindividuale->find('count', 
	    			  			array('conditions'=>array('Reserindividuale.fecha_salida BETWEEN ? AND ? '=>array($fechaa, $fechab),  'Reserindividuale.fecha_entrada <'=>$fechaa)));


	    			/**
	    			* Empiezan antes y terminan despues del mes seleccionado
	    			*/

	    			$cantidad3 = $this->Reserindividuale->find('count', 
	    			  			array('conditions'=>array('Reserindividuale.fecha_salida > '=> $fechab ,  'Reserindividuale.fecha_entrada < '=>$fechaa )));
	    			
	    		}









	    		$cantidad = $cantidad + $cantidad1 + $cantidad2 + $cantidad3;



    	}else if($var1=='3'){ 
    		    $titulo   = " Grafica Ocupaciones dia: ".$var2;
    	       


    		    //$cantidad = $this->Reserindividuale->find('count', array('fields'=>'DISTINCT Habitacione.id', 'conditions'=>array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($var3)), 'Reserindividuale.fecha_entrada'=>$var2)));


    	        $cantidad = $this->Reserindividuale->find('count', 
    	        	array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$var3, 
    	        		          'or'=>array('Reserindividuale.fecha_entrada'=>$var2,'Reserindividuale.fecha_salida'=>$var2))));
    		    if ($var3 == 7) {
    		    	 $cantidad = $this->Reserindividuale->find('count', 
    	        	array('conditions'=>array('or'=>array('Reserindividuale.fecha_entrada'=>$var2,'Reserindividuale.fecha_salida'=>$var2))));
    		    }



    	}
    	$this->set('cantidad_hbitaciones', $cantidad_hbitaciones);
    	$this->set('var', $var1);
    	$this->set('titulo', $titulo);
    	$this->set('cantidad', $cantidad);
	}




    public function tipoocupacion($var=null){
    	$this->layout = "ajax";
    	      if($var=='1'){

    	}else if($var=='2'){
	    		$data[1]  = "Enero";
	    		$data[2]  = "Febrero";
	    		$data[3]  = "Marzo";
	    		$data[4]  = "Abril";
	    		$data[5]  = "Mayo";
	    		$data[6]  = "Junio";
	    		$data[7]  = "Julio";
	    		$data[8]  = "Agosto";
	    		$data[9]  = "Septiembre";
	    		$data[10] = "Obctubre";
	    		$data[11] = "Noviembre";
	    		$data[12] = "Diciembre";
	    		$this->set('data', $data);
    	}else if($var=='3'){

    	}
    	$this->set('var', $var);
    }




 /**
 * reserhabitacion method
 *
 * @return void
 */
	public function reserhabitacion() {
		if ($this->request->is('post')) {
			$op1 = $this->request->data['Grafica']['tipohabitacione_id'];
			$op2 = $this->request->data['Grafica']['habitacione_id'];
			$op3 = $this->request->data['Grafica']['tipo'];
			$op4 = $this->request->data['Grafica']['datos'];
			$op5 = $this->request->data['Grafica']['estatus'];
             return $this->redirect(array('action' => 'reserhabitaciongrafica/'.$op1.'/'.$op2.'/'.$op3.'/'.$op4.'/'.$op5));
		}
		$tipo = array('1'=>'Año', '2'=>'Mes', '3'=>'dia');
		$this->set('tipo', $tipo);
		$tipohabitaciones = $this->Reserindividuale->Tipohabitacione->find('list');
		$this->set('tipohabitaciones', $tipohabitaciones);
	}
    public function reserhabitaciongrafica($var1=null, $var2=null, $var3=null, $var4=null, $var5 = null) {
    	        $tipohabitacione_id   = $var1;
                $habitacione_id       = $var2;
                $habitacion           = $this->Habitacione->find('all', array('conditions'=>array('Habitacione.id'=>$habitacione_id)));
		        $cantidad_hbitaciones = $this->Reserindividuale->find('count', array('fields'=>'DISTINCT Habitacione.id', 'conditions'=>array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($var5)))));
		        $data[1]  = "Enero";
	    		$data[2]  = "Febrero";
	    		$data[3]  = "Marzo";
	    		$data[4]  = "Abril";
	    		$data[5]  = "Mayo";
	    		$data[6]  = "Junio";
	    		$data[7]  = "Julio";
	    		$data[8]  = "Agosto";
	    		$data[9]  = "Septiembre";
	    		$data[10] = "Obctubre";
	    		$data[11] = "Noviembre";
	    		$data[12] = "Diciembre";
		if($var3=='1'){ 
		      	$fechaa = $var4.'-'.'01-01';
		      	$fechab = $var4.'-'.'12-31';
		      	$titulo   = " Grafica Reservaciones habitación ".$habitacion[0]['Habitacione']['numhabitacion']." año: ".$var4;
		      	$cantidad = $this->Reserindividuale->find('count', array('conditions'=>array('Reserindividuale.tipohabitacione_id'=>$tipohabitacione_id, 'Reserindividuale.habitacione_id'=>$habitacione_id, 'or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($var5)), "Reserindividuale.fecha_entrada BETWEEN '".$fechaa."' AND '".$fechab."'")));
    	}else if($var3=='2'){ 
    		    $titulo = " Grafica Reservaciones habitación ".$habitacion[0]['Habitacione']['numhabitacion']." mes: ".$data[$var4];
    			$fechaa = date('Y').'-'.$var4.'-01';
		      	$fechab = date('Y').'-'.$var4.'-31';
	    		$cantidad = $this->Reserindividuale->find('count', array('conditions'=>array('Reserindividuale.tipohabitacione_id'=>$tipohabitacione_id, 'Reserindividuale.habitacione_id'=>$habitacione_id, 'or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($var5)), "Reserindividuale.fecha_entrada BETWEEN '".$fechaa."' AND '".$fechab."'")));
    	}else if($var3=='3'){ 
    		    $titulo   = " Grafica Reservaciones habitación ".$habitacion[0]['Habitacione']['numhabitacion']." dia: ".$var4;
    	        $cantidad = $this->Reserindividuale->find('count', array('conditions'=>array('Reserindividuale.tipohabitacione_id'=>$tipohabitacione_id, 'Reserindividuale.habitacione_id'=>$habitacione_id, 'or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($var5)), "Reserindividuale.fecha_entrada " =>$var4)));

    	}
    	$this->set('cantidad_hbitaciones', $cantidad_hbitaciones);
    	$this->set('var', $var3);
    	$this->set('titulo', $titulo);
    	$this->set('cantidad', $cantidad);
	}
    public function reserhabitaciontipo($var=null){
    	$this->layout = "ajax";
    	      if($var=='1'){

    	}else if($var=='2'){
	    		$data[1]  = "Enero";
	    		$data[2]  = "Febrero";
	    		$data[3]  = "Marzo";
	    		$data[4]  = "Abril";
	    		$data[5]  = "Mayo";
	    		$data[6]  = "Junio";
	    		$data[7]  = "Julio";
	    		$data[8]  = "Agosto";
	    		$data[9]  = "Septiembre";
	    		$data[10] = "Obctubre";
	    		$data[11] = "Noviembre";
	    		$data[12] = "Diciembre";
	    		$this->set('data', $data);
    	}else if($var=='3'){

    	}
    	$this->set('var', $var);
    }





    /**
 * resercliente method
 *
 * @return void
 */
	public function resercliente() {
		if ($this->request->is('post')) {
            $op1 = $this->request->data['Grafica']['tipocliente_id'];
			$op2 = $this->request->data['Grafica']['tipoclientesub_id'];
			$op3 = $this->request->data['Grafica']['cliente_id'];
			$op4 = $this->request->data['Grafica']['tipo'];
			$op5 = $this->request->data['Grafica']['datos'];
			$op6 = $this->request->data['Grafica']['estatus'];
            return $this->redirect(array('action' => 'reserclientegrafica/'.$op1.'/'.$op2.'/'.$op3.'/'.$op4.'/'.$op5.'/'.$op6));
		}
		$tipo = array('1'=>'Año', '2'=>'Mes', '3'=>'dia');
		$this->set('tipo', $tipo);
		$tipoclientes = $this->Reserindividuale->Tipocliente->find('list');
		$this->set('tipoclientes', $tipoclientes);
	}
    public function reserclientegrafica($var1=null, $var2=null, $var3=null, $var4=null, $var5=null, $var6 = null) {
		        $tipocliente_id    = $var1;
                $tipoclientesub_id = $var2;
                $cliente_id        = $var3;
                $cliente           = $this->Cliente->find('all', array('conditions'=>array('Cliente.id'=>$cliente_id)));
		        $cantidad_hbitaciones = $this->Habitacione->find('count');
		        $data[1]  = "Enero";
	    		$data[2]  = "Febrero";
	    		$data[3]  = "Marzo";
	    		$data[4]  = "Abril";
	    		$data[5]  = "Mayo";
	    		$data[6]  = "Junio";
	    		$data[7]  = "Julio";
	    		$data[8]  = "Agosto";
	    		$data[9]  = "Septiembre";
	    		$data[10] = "Obctubre";
	    		$data[11] = "Noviembre";
	    		$data[12] = "Diciembre";
		if($var4=='1'){ 
		      	$fechaa = $var5.'-'.'01-01';
		      	$fechab = $var5.'-'.'12-31';
		      	$titulo   = " Reservaciones del Cliente: ".$cliente[0]['Cliente']['dni']." ".$cliente[0]['Cliente']['nombre_completo']." para el año: ".$var5;
		      	$cantidad = $this->Reserindividuale->find('count', array('conditions'=>array('Reserindividuale.tipocliente_id'=>$tipocliente_id, 'Reserindividuale.tipoclientesub_id'=>$tipoclientesub_id, 'Reserindividuale.cliente_id'=>$cliente_id, 'or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($var6)), "Reserindividuale.fecha_entrada BETWEEN '".$fechaa."' AND '".$fechab."'")));
    	}else if($var4=='2'){ 
    		    $titulo = " Reservaciones del Cliente: ".$cliente[0]['Cliente']['dni']." ".$cliente[0]['Cliente']['nombre_completo']." para el mes: ".$data[$var5];
    			$fechaa = date('Y').'-0'.$var5.'-01';
		      	$fechab = date('Y').'-0'.$var5.'-31';
	    		$cantidad = $this->Reserindividuale->find('count', array('conditions'=>array('Reserindividuale.cliente_id'=>$cliente_id, 'or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($var6)), "Reserindividuale.fecha_entrada BETWEEN '".$fechaa."' AND '".$fechab."'")));
    	}else if($var4=='3'){ 
    		    $titulo   = " Grafica Reservaciones clientes ".$cliente[0]['Cliente']['dni']." dia: ".$var5;
    	        $cantidad = $this->Reserindividuale->find('count', array('conditions'=>array('Reserindividuale.tipocliente_id'=>$tipocliente_id, 'Reserindividuale.tipoclientesub_id'=>$tipoclientesub_id, 'Reserindividuale.cliente_id'=>$cliente_id, 'or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($var6)), 'Reserindividuale.fecha_entrada'=>$var5)));
    	}

    	$this->set('cantidad_hbitaciones', $cantidad_hbitaciones);
    	$this->set('var', $var4);
    	$this->set('titulo', $titulo);
    	$this->set('cantidad', $cantidad);
	}
    public function reserclientetipo($var=null){
    	$this->layout = "ajax";
    	      if($var=='1'){

    	}else if($var=='2'){
	    		$data[1]  = "Enero";
	    		$data[2]  = "Febrero";
	    		$data[3]  = "Marzo";
	    		$data[4]  = "Abril";
	    		$data[5]  = "Mayo";
	    		$data[6]  = "Junio";
	    		$data[7]  = "Julio";
	    		$data[8]  = "Agosto";
	    		$data[9]  = "Septiembre";
	    		$data[10] = "Obctubre";
	    		$data[11] = "Noviembre";
	    		$data[12] = "Diciembre";
	    		$this->set('data', $data);
    	}else if($var=='3'){

    	}
    	$this->set('var', $var);
    }



    /**
 * resercliente method
 *
 * @return void
 */
	public function consumoresto(){
		$consumo  = $this->Consumo->find('all', array());
		$this->set('consumo', $consumo);
	}
	public function consumorestografica($var1=null, $var2=null) {
		      if($var1=='1'){

    	}else if($var1=='2'){
	    		
    	}else if($var1=='3'){

    	}
    	$this->set('var', $var1);
	}
    public function consumorestotipo($var=null){
    	$this->layout = "ajax";
    	      if($var=='1'){

    	}else if($var=='2'){
	    		$data[1]  = "Enero";
	    		$data[2]  = "Febrero";
	    		$data[3]  = "Marzo";
	    		$data[4]  = "Abril";
	    		$data[5]  = "Mayo";
	    		$data[6]  = "Junio";
	    		$data[7]  = "Julio";
	    		$data[8]  = "Agosto";
	    		$data[9]  = "Septiembre";
	    		$data[10] = "Obctubre";
	    		$data[11] = "Noviembre";
	    		$data[12] = "Diciembre";
	    		$this->set('data', $data);
    	}else if($var=='3'){

    	}
    	$this->set('var', $var);
    }




        /**
 * resercliente method
 *
 * @return void
 */
	public function ocupacionfecha() {
		if ($this->request->is('post')) {
                    $op1 = $this->request->data['Grafica']['fecha_desde'];
			        $op2 = $this->request->data['Grafica']['fecha_hasta'];
			        $op3 = $this->request->data['Grafica']['estatus'];
             return $this->redirect(array('action' => 'ocupacionfechagrafica/'.$op1.'/'.$op2.'/'.$op3));

		}
	}
    public function ocupacionfechagrafica($var1=null, $var2=null, $var3 = null) {
		        $cantidad_hbitaciones = $this->Habitacione->find('count');
    		    $titulo = " Grafica Ocupaciones fecha desde: ".$var1." a fecha hasta: ".$var2;
    			$fechaa = $var1;
		      	$fechab = $var2;
	    		





	    		$cantidad = $this->Reserindividuale->find('count', array('fields'=>'DISTINCT Habitacione.id', 'conditions'=>array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($var3)),
	    		 'Reserindividuale.fecha_entrada >='=>$fechaa, 'Reserindividuale.fecha_entrada <='=>$fechab )));

	    			      	/**
	    			      	 * Todas dentro del mes
	    			      	 */

	    		    		$cantidad = $this->Reserindividuale->find('all', 
	    		    			array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$var3,
	    		    				'Reserindividuale.fecha_entrada BETWEEN ? AND ? '=>array($fechaa, $fechab),  'Reserindividuale.fecha_salida BETWEEN ? AND ? '=>array($fechaa, $fechab))));

	    		    		/**
	    		    		 * Todas entran dentro del mes y salen fuera del mes
	    		    		 */

	    		    		$cantidad1 = $this->Reserindividuale->find('all', 
	    		    			array( 'conditions'=>array( 'Reserindividuale.reserstatusindividuale_id'=>$var3,
	    		    				'Reserindividuale.fecha_entrada BETWEEN ? AND ? '=>array( $fechaa, $fechab ),  'Reserindividuale.fecha_salida > ' => $fechab ) ) );

	    		    		/**
	    		    		 * [$cantidad2 description]
	    		    		 * @var number
	    		    		 * Todas entran antes del mes, y salen el mes
	    		    		 */
	    		    		$cantidad2 = $this->Reserindividuale->find('all', 
	    		    			array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$var3,
	    		    				'Reserindividuale.fecha_salida BETWEEN ? AND ? '=>array($fechaa, $fechab),  'Reserindividuale.fecha_entrada <'=>$fechaa)));


	    		    		/**
	    		    		 * Empiezan antes y terminan despues del mes seleccionado
	    		    		 */

	    		    		$cantidad3 = $this->Reserindividuale->find('all', 
	    		    			array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$var3,
	    		    				'Reserindividuale.fecha_salida > '=> $fechab ,  'Reserindividuale.fecha_entrada < '=>$fechaa )));


	    		    		$cantidadarray = array_merge($cantidad, $cantidad1, $cantidad2, $cantidad3);




	    		    		foreach ($cantidadarray as $cant) {
	    		    			
	    		    			
	    		    			$fecha1 = $cant['Reserindividuale']['fecha_entrada'];
	    		    			$fecha2 = $cant['Reserindividuale']['fecha_salida'];


	    		    			

	    		    			$fecha1 = explode('-', $fecha1);
	    		    			$fecha2 = explode('-', $fecha2);

	    		    			$dia1  = $fecha1[2];
	    		    			$mes1  = $fecha1[1];
	    		    			$anho1 = $fecha1[0];



	    		    			$dia2  = $fecha2[2];
	    		    			$mes2  = $fecha2[1];
	    		    			$anho2 = $fecha2[0];

	    		    			$timestamp1 += mktime(0,0,0,$mes1,$dia1,$anho1);				

	    		    			$timestamp2 += mktime(0,0,0,$mes2,$dia2,$anho2);

	    		    			if ($mes2 > $mes1) {
	    		    				$dia2 = $numero;
	    		    			}



	    		    			for ($i = $dia1; $i <= $dia2; $i++) { 
	    		    				$arraycont[$i] = $arraycont[$i] + 1;
	    		    			}



	    		    			
	    		    		}





	    		    			
	    		    			$cat = '[';
	    		    			$dat = '[';

	    		    			for ($i=1; $i <= $numero ; $i++) { 
	    		    				

	    		    				$cat .= $i.','; 
	    		    				$dat .= $arraycont[$i].',';
	    		    				//$var .= '{y: '.$arraycont[$i].', label: '.$i.'},';
	    		    			}

	    		    			$cat .= ']'; 
	    		    			$dat .= ']'; 
	    		    			
	    		    			$this->set('cat', $cat);
	    		    			$this->set('dat', $dat);

	    		    			$segundos_diferencia =  ($timestamp1 - $timestamp2);

	    		    			$segundos_diferencia = abs($segundos_diferencia);
	    		    			$cantidad =  ($segundos_diferencia / (60 * 60 * 24));





		    	$this->set('cantidad_hbitaciones', $cantidad_hbitaciones);
		    	$this->set('titulo', $titulo);
		    	$this->set('cantidad', $cantidad);
	}

/**
 * factura method
 *
 * @return void
 */
	public function factura() {
		$factura  = $this->Factura->find('all', array());
		$this->set('factura', $factura);
		
	}
	public function facturagrafica($var1=null, $var2=null) {
		$this->layout = "ajax";
	}
    public function facturatipo($var=null){
    	$this->layout = "ajax";
    	      if($var=='1'){

    	}else if($var=='2'){
	    		$data[1]  = "Enero";
	    		$data[2]  = "Febrero";
	    		$data[3]  = "Marzo";
	    		$data[4]  = "Abril";
	    		$data[5]  = "Mayo";
	    		$data[6]  = "Junio";
	    		$data[7]  = "Julio";
	    		$data[8]  = "Agosto";
	    		$data[9]  = "Septiembre";
	    		$data[10] = "Obctubre";
	    		$data[11] = "Noviembre";
	    		$data[12] = "Diciembre";
	    		$this->set('data', $data);
    	}else if($var=='3'){

    	}
    	$this->set('var', $var);
    }



/**
 * sub_cliente method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function sub_cliente($id = null) {
		$this->layout = 'ajax';
	    $subclientes = $this->Reserindividuale->Tipoclientesub->find('list', array('fields'=>array('Tipoclientesub.id', 'Tipoclientesub.nombre'), 'conditions'=>array('Tipoclientesub.tipocliente_id'=>$id)));
	    $this->set(compact('subclientes'));
	    $this->set('id', $id);

	}

/**
 * cliente method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cliente($id = null, $id2 = null) {
		$this->layout = 'ajax';
	    $clientes = $this->Reserindividuale->Cliente->find('list', array('fields'=>array('Cliente.id', 'Cliente.nombre_completo'), 'conditions'=>array('Cliente.tipocliente_id'=>$id, 'Cliente.tipoclientesub_id'=>$id2)));
	    $this->set(compact('clientes'));

	}


/**
 * habitacion method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function habitacion($id = null) {
		$this->layout = 'ajax';
	    $habitaciones = $this->Reserindividuale->Habitacione->find('list', array('fields'=>array('Habitacione.id', 'Habitacione.numhabitacion'), 'conditions'=>array('Habitacione.tipohabitacione_id'=>$id)));
	    $this->set(compact('habitaciones'));
	    $this->set('id', $id);

	}





}
?>