<?php
App::uses('AppController', 'Controller');
/**
 * Cajaaperturas Controller
 *
 * @property Cajaapertura $Cajaapertura
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CajaaperturasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(9);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Cajaapertura->recursive = 0;
		//$this->set('cajaaperturas', $this->Paginator->paginate());
		  $this->set('cajaaperturas', $this->Cajaapertura->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Cajaapertura->exists($id)) {
			throw new NotFoundException(__('Invalid cajaapertura'));
		}
		$options = array('conditions' => array('Cajaapertura.' . $this->Cajaapertura->primaryKey => $id));
		$this->set('cajaapertura', $this->Cajaapertura->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Cajaapertura->create();
			if ($this->Cajaapertura->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
		$cajas = $this->Cajaapertura->Caja->find('list');
		$cajaturnos = $this->Cajaapertura->Cajaturno->find('list');
		$users = $this->Cajaapertura->User->find('list');
		$this->set(compact('cajas', 'cajaturnos', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Cajaapertura->exists($id)) {
			throw new NotFoundException(__('Invalid cajaapertura'));
		}
		if ($this->request->is(array('post', 'put'))) {



			$this->request->data['Cajaapertura']['status'] = 1;	
			if ($this->Cajaapertura->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Cajaapertura.' . $this->Cajaapertura->primaryKey => $id));
			$this->request->data = $this->Cajaapertura->find('first', $options);
		}
		$cajas = $this->Cajaapertura->Caja->find('list');
		$cajaturnos = $this->Cajaapertura->Cajaturno->find('list');
		$users = $this->Cajaapertura->User->find('list');
		$this->set(compact('cajas', 'cajaturnos', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Cajaapertura->id = $id;
		if (!$this->Cajaapertura->exists()) {
			throw new NotFoundException(__('Invalid cajaapertura'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Cajaapertura->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
