<?php
App::uses('AppController', 'Controller');
/**
 * Cajacierredias Controller
 *
 * @property Cajacierredia $Cajacierredia
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CajacierrediasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(9);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Cajacierredia->recursive = 0;
		//$this->set('cajacierredias', $this->Paginator->paginate());
		  $this->set('cajacierredias', $this->Cajacierredia->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Cajacierredia->exists($id)) {
			throw new NotFoundException(__('Invalid cajacierredia'));
		}
		$options = array('conditions' => array('Cajacierredia.' . $this->Cajacierredia->primaryKey => $id));
		$this->set('cajacierredia', $this->Cajacierredia->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Cajacierredia->create();
			if ($this->Cajacierredia->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
		$cajas = $this->Cajacierredia->Caja->find('list');
		$users = $this->Cajacierredia->User->find('list');
		$this->set(compact('cajas', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Cajacierredia->exists($id)) {
			throw new NotFoundException(__('Invalid cajacierredia'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Cajacierredia->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Cajacierredia.' . $this->Cajacierredia->primaryKey => $id));
			$this->request->data = $this->Cajacierredia->find('first', $options);
		}
		$cajas = $this->Cajacierredia->Caja->find('list');
		$users = $this->Cajacierredia->User->find('list');
		$this->set(compact('cajas', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Cajacierredia->id = $id;
		if (!$this->Cajacierredia->exists()) {
			throw new NotFoundException(__('Invalid cajacierredia'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Cajacierredia->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
