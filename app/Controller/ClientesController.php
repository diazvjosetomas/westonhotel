<?php
App::uses('AppController', 'Controller');
/**
 * Clientes Controller
 *
 * @property Cliente $Cliente
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ClientesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(1);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Cliente->recursive = 0;
		//$this->set('clientes', $this->Paginator->paginate());
		  $this->set('clientes', $this->Cliente->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Cliente->exists($id)) {
			throw new NotFoundException(__('Invalid cliente'));
		}
		$options = array('conditions' => array('Cliente.' . $this->Cliente->primaryKey => $id));
		$this->set('cliente', $this->Cliente->find('first', $options));
	}



public function preferencias($idcliente = null){
	$this->layout = 'ajax';

	$this->set('preferencias', $this->Cliente->find('all', 
									[
										'conditions'=>[ 

											'Cliente.id ' => $idcliente

										    ]
										] 
									)
								);



}
public function descuento($idcliente = null){
	$this->layout = 'ajax';

	$this->set('preferencias', $this->Cliente->find('all', 
									[
										'conditions'=>[ 

											'Cliente.id ' => $idcliente

										    ]
										] 
									)
								);



}



/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->request->data['Cliente']['fecha_nacimiento'] = !empty($this->request->data['Cliente']['fecha_nacimiento1']) ? formatfecha($this->request->data['Cliente']['fecha_nacimiento1']) : '';
			$this->Cliente->create();
			if ($this->Cliente->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
		$tipoclientes = $this->Cliente->Tipocliente->find('list');
		$tipoclientesubs = $this->Cliente->Tipoclientesub->find('list');
		$pais = $this->Cliente->Pai->find('list');
		$cliente = $this->Cliente;
		$this->set(compact('cliente','tipoclientes', 'tipoclientesubs','pais'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Cliente->exists($id)) {
			throw new NotFoundException(__('Invalid cliente'));
		}

		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Cliente']['fecha_nacimiento'] = !empty($this->request->data['Cliente']['fecha_nacimiento1']) ? formatfecha($this->request->data['Cliente']['fecha_nacimiento1']) : '';
			if ($this->Cliente->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Cliente.' . $this->Cliente->primaryKey => $id));
			$this->request->data = $this->Cliente->find('first', $options);
			$this->request->data['Cliente']['fecha_nacimiento1'] = formatdmy($this->request->data['Cliente']['fecha_nacimiento']);
		}
		$tipoclientes = $this->Cliente->Tipocliente->find('list');
		$tipoclientesubs = $this->Cliente->Tipoclientesub->find('list', array('conditions' => array('Tipoclientesub.tipocliente_id'=>$this->request->data["Cliente"]["tipocliente_id"])));
		$pais = $this->Cliente->Pai->find('list');
		$this->set(compact('tipoclientes', 'tipoclientesubs','pais'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Cliente->id = $id;
		if (!$this->Cliente->exists()) {
			throw new NotFoundException(__('Invalid cliente'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Cliente->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}




/**
 * sub_cliente method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function sub_cliente($id = null) {
		$this->layout = 'ajax';
	    $subclientes = $this->Cliente->Tipoclientesub->find('list', array('fields'=>array('Tipoclientesub.id', 'Tipoclientesub.nombre'), 'conditions'=>array('Tipoclientesub.tipocliente_id'=>$id)));
	    $this->set(compact('subclientes'));

	}
}
