<?php
App::uses('AppController', 'Controller');
/**
 * Descuentos Controller
 *
 * @property Descuento $Descuento
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class DescuentosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');
	public $layout = "dashbord";

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Descuento->recursive = 0;
		$this->set('descuentos', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Descuento->exists($id)) {
			throw new NotFoundException(__('Invalid descuento'));
		}
		$options = array('conditions' => array('Descuento.' . $this->Descuento->primaryKey => $id));
		$this->set('descuento', $this->Descuento->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Descuento->create();
			if ($this->Descuento->save($this->request->data)) {
				$this->Flash->success(__('The descuento has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The descuento could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Descuento->exists($id)) {
			throw new NotFoundException(__('Invalid descuento'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Descuento->save($this->request->data)) {
				$this->Flash->success(__('The descuento has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The descuento could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Descuento.' . $this->Descuento->primaryKey => $id));
			$this->request->data = $this->Descuento->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Descuento->id = $id;
		if (!$this->Descuento->exists()) {
			throw new NotFoundException(__('Invalid descuento'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Descuento->delete()) {
			$this->Flash->success(__('The descuento has been deleted.'));
		} else {
			$this->Flash->error(__('The descuento could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
