<?php
App::uses('AppController', 'Controller');
/**
 * Resermultiples Controller
 *
 * @property Resermultiple $Resermultiple
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ResermultiplesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

	var $uses = array('Resermultiple', 'Pstemporada', 'Tipotemporada', 'Habitacione','Resermulhabitacione', 'Reserindividuale');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(6);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Resermultiple->recursive = 0;
		//$this->set('resermultiples', $this->Paginator->paginate());
		$resermultiples	= $this->Resermultiple->find('all');

		$this->set(compact('resermultiples'));
		  //pr($this->Resermultiple->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Resermultiple->exists($id)) {
			throw new NotFoundException(__('Invalid resermultiple'));
		}
		$this->Resermultiple->recursive = 2;
		$options = array('conditions' => array('Resermultiple.' . $this->Resermultiple->primaryKey => $id));
		$this->set('resermultiple', $this->Resermultiple->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {

			$this->request->data['Resermultiple']['fecha_entrada'] = !empty($this->request->data['Resermultiple']['fecha_entrada1']) ? formatfecha($this->request->data['Resermultiple']['fecha_entrada1']) : '';
			$this->request->data['Resermultiple']['fecha_salida'] =  !empty($this->request->data['Resermultiple']['fecha_salida1']) ? formatfecha($this->request->data['Resermultiple']['fecha_salida1']) : '';
			$this->request->data['Resermultiple']['fecha_tope'] =  !empty($this->request->data['Resermultiple']['fecha_tope1']) ? formatfecha($this->request->data['Resermultiple']['fecha_tope1']) : '';

			$this->request->data['Resermultiple']['reserstatusmultiple_id'] = 6;
			$this->Resermultiple->create();
			
			if ($this->Resermultiple->save($this->request->data)) {
				$last_id = $this->Resermultiple->getLastInsertID();
                $id   =  $this->Resermultiple->id;
				$stop = 0;
				if(!empty($this->request->data['destino'])){
					//$habitaciones = explode(",", $this->request->data['Resermultiple']["habitaciones"]);
					$habitaciones = $this->request->data["destino"];
					
					foreach($habitaciones as $key => $value){

						$this->request->data["Resermulhabitacione"]["resermultiple_id"] = $id;
						$this->request->data["Resermulhabitacione"]["habitacione_id"]   = $value;
						//Ingresar datos a reservas individuales
						
						$this->Resermulhabitacione->create();


						if ($this->Resermulhabitacione->save($this->request->data)){
								
								$this->Reserindividuale->create();
								$this->request->data["Reserindividuale"]["tipocliente_id"] = $this->request->data['Resermultiple']['tipocliente_id'];
								$this->request->data["Reserindividuale"]["cliente_id"] = $this->request->data['Resermultiple']['cliente_id'];
								$this->request->data["Reserindividuale"]["tipohabitacione_id"] = $this->request->data['Resermultiple']['tipohabitacione_id'];
								$this->request->data["Reserindividuale"]["habitacione_id"] = $value;
								$this->request->data["Reserindividuale"]["reserstatusindividuale_id"] = 1;
								$this->request->data["Reserindividuale"]["fecha_entrada"] = $this->request->data['Resermultiple']['fecha_entrada'];
								$this->request->data["Reserindividuale"]["fecha_salida"] =$this->request->data['Resermultiple']['fecha_salida'];;
								$this->request->data["Reserindividuale"]["obseraciones"] = 'Sin observaciones';
								$this->request->data["Reserindividuale"]["observaciones_cliente"] = 'Sin observaciones';
								$this->request->data["Reserindividuale"]["dias"] = $this->request->data['Resermultiple']['dias'];
								$this->request->data["Reserindividuale"]["precioxdia"] = $this->request->data['Resermultiple']['precioxdia'];
								$this->request->data["Reserindividuale"]["total"] = $this->request->data['Resermultiple']['total'];
								$this->request->data["Reserindividuale"]["pagado"] = 0;
								$this->request->data["Reserindividuale"]["facturado"] = 0;
								$this->request->data["Reserindividuale"]["descuento"] = 0;
								$this->request->data["Reserindividuale"]["resermultiple_id"] =  $id;
								$this->request->data["Reserindividuale"]["desayuno"] = 0;
								$this->request->data["Reserindividuale"]["almuerzo"] = 0;
								$this->request->data["Reserindividuale"]["cena"] = 0;
								$this->request->data["Reserindividuale"]["automovil"] = '0';
								$this->request->data["Reserindividuale"]["cantidad_personas"] =  0;
								$this->request->data["Reserindividuale"]["detallereservas"] =  $this->request->data['Resermultiple']['obseraciones'];;
								$this->request->data["Reserindividuale"]["localizador"] =  $this->request->data['Resermultiple']['localizador'];
								$this->Reserindividuale->save($this->request->data);


						}else{
							$stop = 1;
						}

					}
					
				}
				if($stop==0){

					$nums = '';
					foreach ($this->request->data['destino'] as $key => $value) {
						$nums .= $this->requestAction('habitaciones/get_numero/'.$value).', ';
					}

					$auditoria['Auditoria']['user_id']             = $this->Session->read('USUARIO_ID');
		            $auditoria['Auditoria']['habitacione_id']      = 0;
		            $auditoria['Auditoria']['resermultiple_id']    = $last_id;
		            $auditoria['Auditoria']['cliente_id']          = $this->request->data['Resermultiple']['cliente_id'];
		            $auditoria['Auditoria']['factura_id']          = 0;
		            $auditoria['Auditoria']['hora']                = DboSource::expression('NOW()');
		            $auditoria['Auditoria']['motivo']              = 'N/A';
		            $auditoria['Auditoria']['tipo_operacion']      = 'NUEVA RESERVA';
		            $auditoria['Auditoria']['especifico']          = 'RESERVA CREADA';

		            $auditoria['Auditoria']['num_habitacion']      = $nums;
		            $auditoria['Auditoria']['capacidad']           = '-';
		            $auditoria['Auditoria']['detalle_reserva']     = '-';
		            $auditoria['Auditoria']['costo_total']         = $this->request->data['Resermultiple']['total'];
		            $auditoria['Auditoria']['forma_pago']          = '-';
		            $auditoria['Auditoria']['num_factura']         = 0;
		            

					$this->loadModel('Auditoria');
		            $this->Auditoria->save($auditoria);

                    $this->Resermultiple->commit();
                	$this->Flash->success(__('Registro Guardado.'));
					return $this->redirect(array('action' => 'index'));
                }else{
                	$this->Resermultiple->rollback();
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                }
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
		$tipoclientes = $this->Resermultiple->Tipocliente->find('list');
		$tipoclientesubs = $this->Resermultiple->Tipoclientesub->find('list');
		$clientes = $this->Resermultiple->Cliente->find('list');
		$tipohabitaciones = $this->Resermultiple->Tipohabitacione->find('list');
		$reserstatusmultiples = $this->Resermultiple->Reserstatusmultiple->find('list');
		$this->set(compact('tipoclientes', 'tipoclientesubs', 'clientes', 'tipohabitaciones', 'reserstatusmultiples'));
	}




	/**
 * add method envia desde el calendario
 *
 * @return void
 */
public function add_multiples($hab = null, $dia = null, $mes = null, $anho = null) {

	$mes = str_pad($mes, 2, '0', STR_PAD_LEFT);
	$dia = str_pad($dia, 2, '0', STR_PAD_LEFT);

	$this->set('fecha_prestablecida', $anho.'-'.$mes.'-'.$dia);
	if ($this->request->is('post')) {

		$this->request->data['Resermultiple']['fecha_entrada'] = !empty($this->request->data['Resermultiple']['fecha_entrada1']) ? formatfecha($this->request->data['Resermultiple']['fecha_entrada1']) : '';
		$this->request->data['Resermultiple']['fecha_salida'] = !empty($this->request->data['Resermultiple']['fecha_salida1']) ? formatfecha($this->request->data['Resermultiple']['fecha_salida1']) : '';
		$this->request->data['Resermultiple']['fecha_tope'] = !empty($this->request->data['Resermultiple']['fecha_tope1']) ? formatfecha($this->request->data['Resermultiple']['fecha_tope1']) : '';
		$this->request->data['Resermultiple']['reserstatusmultiple_id'] = 6;

		$this->Resermultiple->create();
		if ($this->Resermultiple->save($this->request->data)) {
			$last_id = $this->Resermultiple->getLastInsertID();
			$id   =  $this->Resermultiple->id;
			$stop = 0;
			if(!empty($this->request->data["destino"])){
				//$habitaciones = explode(",", $this->request->data['Resermultiple']["habitaciones"]);
				$habitaciones = $this->request->data["destino"];
				foreach($habitaciones as $key => $value){
					$this->request->data["Resermulhabitacione"]["resermultiple_id"] = $id;
					$this->request->data["Resermulhabitacione"]["habitacione_id"]   = $value;
					$this->Resermulhabitacione->create();
					if ($this->Resermulhabitacione->save($this->request->data)){


						$this->Reserindividuale->create();
						$this->request->data["Reserindividuale"]["tipocliente_id"] = $this->request->data['Resermultiple']['tipocliente_id'];
						$this->request->data["Reserindividuale"]["cliente_id"] = $this->request->data['Resermultiple']['cliente_id'];
						$this->request->data["Reserindividuale"]["tipohabitacione_id"] = $this->request->data['Resermultiple']['tipohabitacione_id'];
						$this->request->data["Reserindividuale"]["habitacione_id"] = $value;
						$this->request->data["Reserindividuale"]["reserstatusindividuale_id"] = 1;
						$this->request->data["Reserindividuale"]["fecha_entrada"] = $this->request->data['Resermultiple']['fecha_entrada'];
						$this->request->data["Reserindividuale"]["fecha_salida"] =$this->request->data['Resermultiple']['fecha_salida'];;
						$this->request->data["Reserindividuale"]["obseraciones"] = 'Sin observaciones';
						$this->request->data["Reserindividuale"]["observaciones_cliente"] = 'Sin observaciones';
						$this->request->data["Reserindividuale"]["dias"] = $this->request->data['Resermultiple']['dias'];
						$this->request->data["Reserindividuale"]["precioxdia"] = $this->request->data['Resermultiple']['precioxdia'];
						$this->request->data["Reserindividuale"]["total"] = $this->request->data['Resermultiple']['total'];
						$this->request->data["Reserindividuale"]["pagado"] = 0;
						$this->request->data["Reserindividuale"]["facturado"] = 0;
						$this->request->data["Reserindividuale"]["descuento"] = 0;
						$this->request->data["Reserindividuale"]["resermultiple_id"] =  $id;
						$this->request->data["Reserindividuale"]["desayuno"] = 0;
						$this->request->data["Reserindividuale"]["almuerzo"] = 0;
						$this->request->data["Reserindividuale"]["cena"] = 0;
						$this->request->data["Reserindividuale"]["automovil"] = '0';
						$this->request->data["Reserindividuale"]["cantidad_personas"] =  0;
						$this->request->data["Reserindividuale"]["detallereservas"] =  $this->request->data['Resermultiple']['obseraciones'];;
						$this->request->data["Reserindividuale"]["localizador"] =  !empty($this->request->data['Resermultiple']['localizador']) ? $this->request->data['Resermultiple']['localizador'] : '';
						$this->Reserindividuale->save($this->request->data);

					}else{
						$stop = 1;
					}
				}
			}
			if($stop==0){

				$nums = '';
				foreach ($this->request->data['destino'] as $key => $value) {
					$nums .= $this->requestAction('habitaciones/get_numero/'.$value).', ';
				}

				$auditoria['Auditoria']['user_id']             = $this->Session->read('USUARIO_ID');
	            $auditoria['Auditoria']['habitacione_id']      = !empty($this->request->data['Resermultiple']['habitacione_id']) ? $this->request->data['Resermultiple']['habitacione_id'] : 0;
	            $auditoria['Auditoria']['resermultiple_id']    = $last_id;
	            $auditoria['Auditoria']['cliente_id']          = $this->request->data['Resermultiple']['cliente_id'];
	            $auditoria['Auditoria']['factura_id']          = 0;
	            $auditoria['Auditoria']['hora']                = DboSource::expression('NOW()');
	            $auditoria['Auditoria']['motivo']              = 'N/A';
	            $auditoria['Auditoria']['tipo_operacion']      = 'NUEVA RESERVA';
	            $auditoria['Auditoria']['especifico']          = 'RESERVA CREADA';

	            $auditoria['Auditoria']['num_habitacion']      = $nums;
	            $auditoria['Auditoria']['capacidad']           = '-';
	            $auditoria['Auditoria']['detalle_reserva']     = '-';
	            $auditoria['Auditoria']['costo_total']         = $this->request->data['Resermultiple']['total'];
	            $auditoria['Auditoria']['forma_pago']          = '-';
	            $auditoria['Auditoria']['num_factura']         = 0;
	            

				$this->loadModel('Auditoria');
	            $this->Auditoria->save($auditoria);


				$this->Resermultiple->commit();
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			}else{
				$this->Resermultiple->rollback();
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
		}
	}
	$tipoclientes = $this->Resermultiple->Tipocliente->find('list');
	$tipoclientesubs = $this->Resermultiple->Tipoclientesub->find('list');
	$clientes = $this->Resermultiple->Cliente->find('list');
	$tipohabitaciones = $this->Resermultiple->Tipohabitacione->find('list');
	$reserstatusmultiples = $this->Resermultiple->Reserstatusmultiple->find('list');
	$this->set(compact('tipoclientes', 'tipoclientesubs', 'clientes', 'tipohabitaciones', 'reserstatusmultiples'));
}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {

		if (!$this->Resermultiple->exists($id)) {
			throw new NotFoundException(__('Invalid resermultiple'));
		}
		if ($this->request->is(array('post', 'put'))) {

			$this->request->data['Resermultiple']['fecha_entrada'] = !empty($this->request->data['Resermultiple']['fecha_entrada1']) ? formatfecha($this->request->data['Resermultiple']['fecha_entrada1']) : '';
			$this->request->data['Resermultiple']['fecha_salida'] = !empty($this->request->data['Resermultiple']['fecha_salida1']) ? formatfecha($this->request->data['Resermultiple']['fecha_salida1']) : '';
			$this->request->data['Resermultiple']['fecha_tope'] = !empty($this->request->data['Resermultiple']['fecha_tope1']) ? formatfecha($this->request->data['Resermultiple']['fecha_tope1']) : '';

			if ($this->Resermultiple->save($this->request->data)) {
				$existentes1 = $this->Resermulhabitacione->find('list', ['fields' => [ 'Resermulhabitacione.habitacione_id' ],'conditions' => [ 'Resermulhabitacione.resermultiple_id' => $id ] ]);
					

				if(!empty($this->request->data['destino'])){
					$habitaciones = $this->request->data["destino"];
					
					$nuevas_habs = [];
					
					foreach ($existentes1 as $key => $value) {
						$actual[$key] = $value;
					}

					foreach ($habitaciones as $key => $value) {
						$existentes = $this->Resermulhabitacione->find('first', ['conditions' => [ 'Resermulhabitacione.resermultiple_id' => $id, 'Resermulhabitacione.habitacione_id' => $value ] ]);
						
						if( empty($existentes) ){
							$nuevas_habs[] = $value;
						}else{
							$prevs_habs[]  = $value;
						}
					}
					if(!empty($actual)){
						$diff = array_diff($actual, $nuevas_habs, $habitaciones);
					}

					if(isset($diff) && !empty($diff)){
						foreach ($diff as $key => $value) {
							$this->Resermulhabitacione->delete($key);
						}
					}
					
					foreach($nuevas_habs as $key => $value){
						$this->request->data["Resermulhabitacione"]["resermultiple_id"] = $id;
						$this->request->data["Resermulhabitacione"]["habitacione_id"]   = $value;
						$this->Resermulhabitacione->create();
						if ($this->Resermulhabitacione->save($this->request->data)){

						}
					}
				}else{
					foreach ($existentes1 as $key => $value) {
						$this->Resermulhabitacione->delete($key);
					}
				}

				$this->Flash->success(__('Registro Actualizado con exito!.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Resermultiple.' . $this->Resermultiple->primaryKey => $id));
			$this->request->data = $this->Resermultiple->find('first', $options);

			$this->request->data['Resermultiple']['fecha_entrada1'] = formatdmy($this->request->data['Resermultiple']['fecha_entrada']);
			$this->request->data['Resermultiple']['fecha_salida1'] = formatdmy($this->request->data['Resermultiple']['fecha_salida']);
			$this->request->data['Resermultiple']['fecha_tope1'] = formatdmy($this->request->data['Resermultiple']['fecha_tope']);


		}
		$this->Resermulhabitacione->recursive = 2;
		$resermulhabitaciones = $this->Resermulhabitacione->find('all', array('conditions'=>array('Resermulhabitacione.resermultiple_id'=>$id)));
		$tipoclientes = $this->Resermultiple->Tipocliente->find('list');
		@$tipoclientesubs = $this->Resermultiple->Tipoclientesub->find('list', array('conditions'=>array('Tipoclientesub.tipocliente_id'=>$this->request->data['Resermultiple']['tipocliente_id'])));
		@$clientes = $this->Resermultiple->Cliente->find('list', array('conditions'=>array('Cliente.id'=>$this->request->data['Resermultiple']['cliente_id'])));
		$tipohabitaciones = $this->Resermultiple->Tipohabitacione->find('list');
		$reserstatusmultiples = $this->Resermultiple->Reserstatusmultiple->find('list');
		$dest = $this->Resermultiple->Resermulhabitacione->find('all', [ 'conditions' => [ 'Resermulhabitacione.resermultiple_id' => $id ] ]);
		
		
		foreach ($dest as $key => $value) {
			$destinos[$dest[$key]['Resermulhabitacione']['habitacione_id']] = $this->requestAction('habitaciones/get_numero/'.$dest[$key]['Resermulhabitacione']['habitacione_id']);
		}
		
		if( empty($destinos) ){
			$destinos = array();
		}
		
		$this->set(compact('resermulhabitaciones','tipoclientes', 'tipoclientesubs', 'clientes', 'tipohabitaciones', 'reserstatusmultiples', 'destinos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null, $motivo = null ) {

		$this->Resermultiple->id = $id;



		if (!$this->Resermultiple->exists()) {
			throw new NotFoundException(__('Invalid resermultiple'));
		}

		$res = $this->Resermultiple->find('first', ['conditions' => [ 'Resermultiple.id' => $id ] ]);
		$hab = '';

		foreach ($res['Resermulhabitacione'] as $key => $value) {
			$this->loadModel('Habitacione');
			$r = $this->Habitacione->find('first', ['conditions' => ['Habitacione.id' => $value['habitacione_id'] ] ]);
			if(!empty($r)){
				$hab .= $r['Habitacione']['numhabitacion'].', '; 
			}
		}

        $auditoria['Auditoria']['user_id']             = $this->Session->read('USUARIO_ID');
        $auditoria['Auditoria']['habitacione_id']      = 0;
        $auditoria['Auditoria']['resermultiple_id']    = $res['Resermultiple']['id'];
        $auditoria['Auditoria']['reserindividuale_id'] = 0;
        $auditoria['Auditoria']['cliente_id']          = $res['Resermultiple']['cliente_id'];
        $auditoria['Auditoria']['factura_id']          = 0;
        $auditoria['Auditoria']['hora']                = DboSource::expression('NOW()');
        $auditoria['Auditoria']['motivo']              = $motivo;
        $auditoria['Auditoria']['tipo_operacion']      = 'ELIMINADO DE RESERVA';
        $auditoria['Auditoria']['especifico']          = !empty($hab) ? 'ELIMINADO HABS '.$hab : 'ELIMINADO';

        $this->loadModel('Auditoria');
        $this->Auditoria->save($auditoria);


		$rol = $this->Session->read('ROL');
        if($rol!=1){

                $this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
        }else{
				
				$this->request->allowMethod('post', 'delete', 'get');
				if ($this->Resermultiple->delete()) {


					$sql = "DELETE FROM reserindividuales WHERE resermultiple_id = ".$id." ";

					$this->Reserindividuale->query($sql);
					$this->Flash->success(__('El Registro fue eliminado.'));
				} else {
					$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
				}
				return $this->redirect(array('action' => 'index'));
		}
	}

	
/**
 * habitacion method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function habitacion($id = null) {
		$this->layout = 'ajax';
	    $habitaciones = $this->Resermultiple->Habitacione->find('list', array('fields'=>array('Habitacione.id', 'Habitacione.numhabitacion'), 'conditions'=>array('Habitacione.tipohabitacione_id'=>$id, 'Habitacione.habistatu_id'=>1)));
	    $this->set(compact('habitaciones'));
	    $this->set('id', $id);

	}


/**
 * fecha_entrada method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fecha_entrada($id = null, $id2 = null) {
		$this->layout = 'ajax';
	    $this->set('id', $id);
	}


/**
 * precioxdia method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function precioxdia($id = null, $id2 = null) {
		$this->layout       = 'ajax';
        $conuntemp          = $this->Tipotemporada->find('count', array('conditions'=>array('Tipotemporada.fechadesde <='=>$id2, 'Tipotemporada.fechahasta >='=>$id2)));
		$tipotemporadas     = $this->Tipotemporada->find('all', array('conditions'=>array('Tipotemporada.fechadesde <='=>$id2, 'Tipotemporada.fechahasta >='=>$id2)));
        $tipohabitacione_id = $id;
        if($conuntemp>1){
        	$listo = 0;
        	foreach($tipotemporadas as $data){
        		if($listo==0){
        			$listo      = $this->Pstemporada->find('count',   array('conditions'=>array('Pstemporada.tipohabitacione_id'=>$tipohabitacione_id, 'Pstemporada.tipotemporada_id'=>$data['Tipotemporada']['id'])));
        			$precios    = $this->Pstemporada->find('all',   array('conditions'=>array('Pstemporada.tipohabitacione_id'=>$tipohabitacione_id, 'Pstemporada.tipotemporada_id'=>$data['Tipotemporada']['id'])));
        		}
        	}
        }else{
        	$tipotemporada_id   = isset($tipotemporadas[0]['Tipotemporada']['id'])?$tipotemporadas[0]['Tipotemporada']['id']:0;
        	$precios            = $this->Pstemporada->find('all',   array('conditions'=>array('Pstemporada.tipohabitacione_id'=>$tipohabitacione_id, 'Pstemporada.tipotemporada_id'=>$tipotemporada_id)));
        }
		
		$this->set('value', isset($precios[0]['Pstemporada']['precio'])?$precios[0]['Pstemporada']['precio']:0);
	}



/**
 * sub_cliente method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function sub_cliente($id = null) {
		$this->layout = 'ajax';
	    $subclientes = $this->Resermultiple->Tipoclientesub->find('list', array('fields'=>array('Tipoclientesub.id', 'Tipoclientesub.nombre'), 'conditions'=>array('Tipoclientesub.tipocliente_id'=>$id)));
	    $this->set(compact('subclientes'));
	    $this->set('id', $id);

	}

/**
 * cliente method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cliente($id = null, $id2 = null) {
		$this->layout = 'ajax';
	    $clientes = $this->Resermultiple->Cliente->find('list', array('fields'=>array('Cliente.id', 'Cliente.nombre_completo'), 'conditions'=>array('Cliente.tipocliente_id'=>$id, 'Cliente.tipoclientesub_id'=>$id2)));
	    $this->set(compact('clientes'));

	}


/**
 * disponibidad method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function disponibidad($a = null, $b = null) {
		$this->layout = 'ajax';
		$a = $this->request->data['fecha_a'];
		$b = $this->request->data['fecha_b'];
		$c = $this->request->data['tipohabitacione_id']; 
		$activa = 1;
		//pr($this->request->data['habitacione_id']);
		$cadena = "uno,dos,tres,cuatro,cinco";
        $array = explode(",", $cadena);
        //pr($array );
		foreach ($this->request->data['habitacione_id'] as $key => $value) {
			$tipotemporadas_e = $this->Resermulhabitacione->find('count', array('conditions'=>array('Resermulhabitacione.habitacione_id'=>$value, 'Resermultiple.tipohabitacione_id'=>$c, 'Resermultiple.fecha_entrada <='=>$a, 'Resermultiple.fecha_salida >'=>$a)));
			$tipotemporadas_s = $this->Resermulhabitacione->find('count', array('conditions'=>array('Resermulhabitacione.habitacione_id'=>$value, 'Resermultiple.tipohabitacione_id'=>$c, 'Resermultiple.fecha_entrada <'=>$b, 'Resermultiple.fecha_salida >='=>$b)));
			$tipotemporadas_c = $this->Resermulhabitacione->find('count', array('conditions'=>array('Resermulhabitacione.habitacione_id'=>$value, 'Resermultiple.tipohabitacione_id'=>$c, 'Resermultiple.fecha_entrada >='=>$a, 'Resermultiple.fecha_salida <='=>$b)));
	        $habitaciones     = $this->Habitacione->find('all', array('conditions'=>array('Habitacione.id'=>$value, 'Habitacione.tipohabitacione_id'=>$c)));
	        if($tipotemporadas_e==0){
	        	if($tipotemporadas_s==0){
		        	if($tipotemporadas_c==0){
		        		//$activa = 0;
		        		//INDIVIDUAL RESERVA
			        		$tipotemporadas_e_i = $this->Reserindividuale->find('count', array('conditions'=>array('Reserindividuale.habitacione_id'=>$value, 'Reserindividuale.tipohabitacione_id'=>$c, 'Reserindividuale.fecha_entrada <='=>$a, 'Reserindividuale.fecha_salida >'=>$a)));
							$tipotemporadas_s_i = $this->Reserindividuale->find('count', array('conditions'=>array('Reserindividuale.habitacione_id'=>$value, 'Reserindividuale.tipohabitacione_id'=>$c, 'Reserindividuale.fecha_entrada <'=>$b, 'Reserindividuale.fecha_salida >='=>$b)));
							$tipotemporadas_c_i = $this->Reserindividuale->find('count', array('conditions'=>array('Reserindividuale.habitacione_id'=>$value, 'Reserindividuale.tipohabitacione_id'=>$c, 'Reserindividuale.fecha_entrada >='=>$a, 'Reserindividuale.fecha_salida <='=>$b)));
		                    if($tipotemporadas_e_i==0){
					        	if($tipotemporadas_s_i==0){
						        	if($tipotemporadas_c_i==0){
						        		$activa = 0;
							        }else{
							        	$activa = $habitaciones[0]['Habitacione']['numhabitacion'];
							        	break;
							        }
						        }else{
						        	$activa = $habitaciones[0]['Habitacione']['numhabitacion'];
						        	break;
						        }
					        }else{
					        	$activa = $habitaciones[0]['Habitacione']['numhabitacion'];
					        	break;
					        }
                        //FIN INDIVIDUAL RESERVA
			        }else{
			        	$activa = $habitaciones[0]['Habitacione']['numhabitacion'];
			        	break;
			        }
		        }else{
		        	$activa = $habitaciones[0]['Habitacione']['numhabitacion'];
		        	break;
		        }
	        }else{
	        	$activa = $habitaciones[0]['Habitacione']['numhabitacion'];
	        	break;
	        }
        }
        echo $activa;
	}

/**
 * disponibidad method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function habitaciones(){
        $this->layout = 'ajax';
        $id = $this->request->data["tipohabitacione_id"];
        $habitaciones = $this->Habitacione->find('list', array('fields'=>array('Habitacione.id', 'Habitacione.numhabitacion'), 'conditions'=>array('Habitacione.tipohabitacione_id'=>$id)));
	    $this->set(compact('habitaciones'));

	}


/**
 * status method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function status($id = null) {
		return $this->redirect('/Resermultistatuses/add/'.$id);
	}

}
