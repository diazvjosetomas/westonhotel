<?php
App::uses('AppController', 'Controller');
/**
 * Tipotemporadas Controller
 *
 * @property Tipotemporada $Tipotemporada
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class TipotemporadasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(2);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Tipotemporada->recursive = 0;
		//$this->set('tipotemporadas', $this->Paginator->paginate());
		  $this->set('tipotemporadas', $this->Tipotemporada->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Tipotemporada->exists($id)) {
			throw new NotFoundException(__('Invalid tipotemporada'));
		}
		$options = array('conditions' => array('Tipotemporada.' . $this->Tipotemporada->primaryKey => $id));
		$this->set('tipotemporada', $this->Tipotemporada->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->request->data['Tipotemporada']['fechadesde'] = !empty($this->request->data['Tipotemporada']['fechadesde1']) ? formatfecha($this->request->data['Tipotemporada']['fechadesde1']) : '';
			$this->request->data['Tipotemporada']['fechahasta'] = !empty($this->request->data['Tipotemporada']['fechahasta1']) ? formatfecha($this->request->data['Tipotemporada']['fechahasta1']) : '';
			$this->Tipotemporada->create();
			if ($this->Tipotemporada->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Tipotemporada->exists($id)) {
			throw new NotFoundException(__('Invalid tipotemporada'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Tipotemporada']['fechadesde'] = !empty($this->request->data['Tipotemporada']['fechadesde1']) ? formatfecha($this->request->data['Tipotemporada']['fechadesde1']) : '';
			$this->request->data['Tipotemporada']['fechahasta'] = !empty($this->request->data['Tipotemporada']['fechahasta1']) ? formatfecha($this->request->data['Tipotemporada']['fechahasta1']) : '';
			if ($this->Tipotemporada->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Tipotemporada.' . $this->Tipotemporada->primaryKey => $id));
			$this->request->data = $this->Tipotemporada->find('first', $options);
			$this->request->data['Tipotemporada']['fechadesde1'] = formatdmy($this->request->data['Tipotemporada']['fechadesde']);
			$this->request->data['Tipotemporada']['fechahasta1'] = formatdmy($this->request->data['Tipotemporada']['fechahasta']);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Tipotemporada->id = $id;
		if (!$this->Tipotemporada->exists()) {
			throw new NotFoundException(__('Invalid tipotemporada'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Tipotemporada->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
