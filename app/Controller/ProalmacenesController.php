<?php
App::uses('AppController', 'Controller');
/**
 * Proalmacenes Controller
 *
 * @property Proalmacene $Proalmacene
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ProalmacenesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession();
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Proalmacene->recursive = 0;
		//$this->set('proalmacenes', $this->Paginator->paginate());
		  $this->set('proalmacenes', $this->Proalmacene->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Proalmacene->exists($id)) {
			throw new NotFoundException(__('Invalid proalmacene'));
		}
		$options = array('conditions' => array('Proalmacene.' . $this->Proalmacene->primaryKey => $id));
		$this->set('proalmacene', $this->Proalmacene->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Proalmacene->create();
			if ($this->Proalmacene->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
		$protipopisos = $this->Proalmacene->Protipopiso->find('list');
		$this->set(compact('protipopisos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Proalmacene->exists($id)) {
			throw new NotFoundException(__('Invalid proalmacene'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Proalmacene->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Proalmacene.' . $this->Proalmacene->primaryKey => $id));
			$this->request->data = $this->Proalmacene->find('first', $options);
		}
		$protipopisos = $this->Proalmacene->Protipopiso->find('list');
		$this->set(compact('protipopisos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Proalmacene->id = $id;
		if (!$this->Proalmacene->exists()) {
			throw new NotFoundException(__('Invalid proalmacene'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Proalmacene->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
