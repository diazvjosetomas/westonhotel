<?php
App::uses('AppController', 'Controller');
/**
 * Personales Controller
 *
 * @property Personale $Personale
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PersonalesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(10);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Personale->recursive = 0;
		//$this->set('personales', $this->Paginator->paginate());
		  $this->set('personales', $this->Personale->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Personale->exists($id)) {
			throw new NotFoundException(__('Invalid personale'));
		}
		$options = array('conditions' => array('Personale.' . $this->Personale->primaryKey => $id));
		$this->set('personale', $this->Personale->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->request->data['Personale']['fecha_ingreso'] = !empty($this->request->data['Personale']['fecha_ingreso1']) ? formatfecha($this->request->data['Personale']['fecha_ingreso1']) : '';
			$this->Personale->create();
			if ($this->Personale->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Personale->exists($id)) {
			throw new NotFoundException(__('Invalid personale'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Personale']['fecha_ingreso'] = !empty($this->request->data['Personale']['fecha_ingreso1']) ? formatfecha($this->request->data['Personale']['fecha_ingreso1']) : '';
			if ($this->Personale->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Personale.' . $this->Personale->primaryKey => $id));
			$this->request->data = $this->Personale->find('first', $options);
			$this->request->data['Personale']['fecha_ingreso1'] = formatdmy($this->request->data['Personale']['fecha_ingreso']);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Personale->id = $id;
		if (!$this->Personale->exists()) {
			throw new NotFoundException(__('Invalid personale'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Personale->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
