<?php
App::uses('AppController', 'Controller');
/**
 * Resermultistatuses Controller
 *
 * @property Resermultistatus $Resermultistatus
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ResermultistatusesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

	var $uses = array('Resermultistatus', 'Resermultiple');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(6);
	}

/**
 * index method
 *
 * @return void
 */
	public function index($id=null) {
		//$this->Resermultistatus->recursive = 0;
		//$this->set('resermultistatuses', $this->Paginator->paginate());
		  $this->set('resermultistatuses', $this->Resermultistatus->find('all'));
		  if($id!=null){
		  	return $this->redirect('/Resermultiples/index');
		  }
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Resermultistatus->exists($id)) {
			throw new NotFoundException(__('Invalid resermultistatus'));
		}
		$options = array('conditions' => array('Resermultistatus.' . $this->Resermultistatus->primaryKey => $id));
		$this->set('resermultistatus', $this->Resermultistatus->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($id=null) {
		if ($this->request->is('post')) {
			$this->Resermultistatus->create();
			if ($this->Resermultistatus->save($this->request->data)) {
				//$this->Flash->success(__('Registro Guardado.'));
				//return $this->redirect(array('action' => 'index'));
                   $this->request->data['Resermultiple']['id']                     = $this->request->data['Resermultistatus']['resermultiple_id'];
				   $this->request->data['Resermultiple']['reserstatusmultiple_id'] = $this->request->data['Resermultistatus']['reserstatusmultiple_id'];
               //pr($this->request->data);
				   if($this->request->data['Resermultiple']['reserstatusmultiple_id']==2){
					  $this->request->data['Resermultiple']['pagado'] = $this->request->data['Resermultistatus']['pago'];
					}
               if ($this->Resermultiple->save($this->request->data)){
	               	$this->Flash->success(__('Registro Guardado.'));
					return $this->redirect('/Resermultiples/index');
               }else{
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
               }


			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
		$resermultiples       = $this->Resermultistatus->Resermultiple->find('list');
		$resermultiples2      = $this->Resermultistatus->Resermultiple->find('all', array('conditions'=>array('Resermultiple.id'=>$id)));
		$reserstatusmultiples = $this->Resermultistatus->Reserstatusmultiple->find('list');
		$conftipopagoreservas = $this->Resermultistatus->Conftipopagoreserva->find('list');
		$this->set(compact('resermultiples2', 'resermultiples', 'reserstatusmultiples', 'conftipopagoreservas'));
		$this->set('id',$id);
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Resermultistatus->exists($id)) {
			throw new NotFoundException(__('Invalid resermultistatus'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Resermultistatus->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Resermultistatus.' . $this->Resermultistatus->primaryKey => $id));
			$this->request->data = $this->Resermultistatus->find('first', $options);
		}
		$resermultiples = $this->Resermultistatus->Resermultiple->find('list');
		$reserstatusmultiples = $this->Resermultistatus->Reserstatusmultiple->find('list');
		$conftipopagoreservas = $this->Resermultistatus->Conftipopagoreserva->find('list');
		$this->set(compact('resermultiples', 'reserstatusmultiples', 'conftipopagoreservas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Resermultistatus->id = $id;
		if (!$this->Resermultistatus->exists()) {
			throw new NotFoundException(__('Invalid resermultistatus'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Resermultistatus->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
