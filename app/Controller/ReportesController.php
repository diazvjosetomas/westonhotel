<?php
App::uses('AppController', 'Controller');
/**
 * Cajas Controller
 *
 * @property Caja $Caja
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ReportesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');


	var $uses = array('Reserindividuale', 'Habitacione', 'Cliente', 'Consumo', 'Factura','Facturapago', 'Pai');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(14);
	}


	public function grafreporte1(){
		

	}


	public function grafreporte8(){
		

	}

	public function grafreporte9(){
		

	}

	public function reporte1($fecha1, $fecha2, $tipo){

		if(!empty($fecha1)){
			$fecha1 = formatfecha($fecha1);
		} 

		if(!empty($fecha2)){
			$fecha2 = formatfecha($fecha2);
		}

		$this->layout = 'pdf';
		App::import('Vendor', 'Fpdf', array('file' => 'fpdf181/fpdf.php'));
		$this->Reserindividuale->recursive = 3;



		if ($tipo == '1') {
			$estados = 'Iniciado';	
		}else if ($tipo == '2') {
			$estados = 'Confirmado';
		}else if ($tipo == '3') {
			$estados = 'Auditada';
		}else if($tipo == '4'){
			$estados = 'Ingresada';
		}else if($tipo == '5'){
			$estados = 'Egresada';
		}else if($tipo == '6'){
			$estados = 'No Show';
		}


		
		



		$r1 = $this->Reserindividuale->find('all',array('conditions'=>array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($tipo)),'Reserindividuale.fecha_entrada >='=>$fecha1,'Reserindividuale.fecha_salida <='=>$fecha2), 'order'=>array('Habitacione.numhabitacion'=>'asc')));

		$r2 = $this->Reserindividuale->find('all',array('conditions'=>array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($tipo)),'Reserindividuale.fecha_entrada <'=>$fecha1,'Reserindividuale.fecha_salida >'=>$fecha2), 'order'=>array('Habitacione.numhabitacion'=>'asc')));


		$r3 = array_merge($r1, $r2);

		$this->set('reserindividuales', $r3);

		$this->set('cantidad_de_ingresos', $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$tipo,'Reserindividuale.fecha_entrada >='=>$fecha1,'Reserindividuale.fecha_salida <='=>$fecha2), 'order'=>array('Habitacione.numhabitacion'=>'asc'))));



		$this->set('pais', $this->Pai->find('all'));
		$this->set('name', "pasajeros_".$estados."_".date('d_m_Y').".pdf");
		$this->set('info',"Reporte: Pasajeros ".$estados." desde el ".$fecha1." hasta el ".$fecha2);
		$this->set('titulo','Pasajeros Estados: '.$estados);
		
	}




	public function reporte1ajax($fecha1, $fecha2, $estados){
		$this->layout = 'ajax';	

		$a = explode('-', $fecha1);
		$this->set('primerdia', $a[2]);
		$primerdia = $a[2];

		$b = explode('-', $fecha2);
		$this->set('ultimodia', $b[2]);
		$ultimodia = $b[2];

		$data[0] = 'Hello!';
		for ($i=$primerdia; $i <=$ultimodia ; $i++) { 
			$data[$i] = 3; 
		}
		$tabla = '
				<table id="data" class="table table-striped table-bordered" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>Dia
						</th>

						<th>Cantidad Personas
						</th>
					</tr>

				 <thead>
				 <tbody>';
		

		for ($i=$primerdia; $i <=$ultimodia ; $i++) { 


			$fecha_a = $a[0].'-'.$a[1].'-0'.$i;


			




			$result1 = $this->Reserindividuale->find('all', 
				[
					'conditions'=>[
						'Reserindividuale.reserstatusindividuale_id' =>array($estados),
						'Reserindividuale.fecha_entrada <= ' => $fecha_a, 'Reserindividuale.fecha_salida  >= ' => $fecha_a
					],
					'order'=> ['Habitacione.numhabitacion'=>'asc']

				]);

			$cantidadiaperson = 0;
			foreach ($result1 as $key) {
				$cantidadiaperson  += $key['Reserindividuale']['cantidad_personas'];
				//echo $key['Reserindividuale']['cantidad_personas'];
			}

			
			


			$tabla .= "<tr><td>".$i." </td><td> ".$cantidadiaperson."</td></tr>";
		} 

		$tabla .= '</tbody>';

		$this->set('tabla', $tabla);


		

		//No se usa
		$this->Reserindividuale->recursive = 1;

	


		//------------------------------------------------> NUEVO



		// CUANDO LA FECHA DE ENTRADA ES MENOR O IGUAL A LA PRIMERA ENTRADA DE TODAS LAS RESERVAS
		// AND LA FECHA DE SALIDA ES MAYOR O IGUAL A LA ULTIMA FECHA DE SALIDA REGISTRADA DE X RESERVA

		$r1 = $this->Reserindividuale->find('all', 
				[
					'conditions'=>[
						'Reserindividuale.reserstatusindividuale_id' =>array($estados),
						'Reserindividuale.fecha_entrada >= ' => $fecha1, 'Reserindividuale.fecha_entrada  <= ' => $fecha2
					],
					'order'=> ['Habitacione.numhabitacion'=>'asc']

				]);


		foreach ($r1 as $key) {
			$var1[] = $key['Reserindividuale']['id'];
		}
		//****************
/*		echo "var1";
		var_dump($var1);
*/
		//--------------------------------------------------> R2
		// Fechas de entrada o de salida que coinciden

		$r2 = $this->Reserindividuale->find('all', 
				[
					'conditions'=>[
						'Reserindividuale.reserstatusindividuale_id' =>array($estados),
						'or'  => ['Reserindividuale.fecha_entrada ' => $fecha1, 'Reserindividuale.fecha_salida ' => $fecha2],
						'not' => [$var1]
						
					],
					'order'=> ['Habitacione.numhabitacion'=>'asc']

				]);

		


		foreach ($r2 as $key) {
			$var2[] = $key['Reserindividuale']['id'];
		}
		//***************************
/*		echo "var2";
		var_dump($var2);
*/


		$r3 = $this->Reserindividuale->find('all', 
				[
					'conditions'=>[
						'Reserindividuale.reserstatusindividuale_id' =>array($estados),
						'Reserindividuale.fecha_entrada < ' => $fecha1,
						'Reserindividuale.fecha_salida  > ' => $fecha1,
						'not'=>[$var2]
					],
					'order'=> ['Habitacione.numhabitacion'=>'asc']

				]);


		foreach ($r3 as $key) {
			$var3[] = $key['Reserindividuale']['id'];
		}
/*		echo "var3";
		var_dump($var3);
*/



		$r4 = $this->Reserindividuale->find('all', 
				[
					'conditions'=>[
						'Reserindividuale.reserstatusindividuale_id' =>array($estados),
						'Reserindividuale.fecha_entrada < ' => $fecha2,
						'Reserindividuale.fecha_salida > ' => $fecha2,
						//'not'=>[$var3]
						
					],
					'order'=> ['Habitacione.numhabitacion'=>'asc']

				]);


		foreach ($r4 as $key) {
			$var4[] = $key['Reserindividuale']['id'];
		}
/*		echo "var4";
		var_dump($var4);
*/


		$r5 = $this->Reserindividuale->find('all', 
				[
					'conditions'=>[
						'Reserindividuale.reserstatusindividuale_id' =>array($estados),
						'Reserindividuale.fecha_entrada < ' => $fecha2,
						'Reserindividuale.fecha_salida > ' => $fecha2,
						'not'=>[$var4]
						
					],
					'order'=> ['Habitacione.numhabitacion'=>'asc']

				]);

		//----------- NUEVAS CONSULTAS

		$r1 =  $this->Reserindividuale->find('all', 
				[
					'conditions'=>[
						'Reserindividuale.reserstatusindividuale_id' =>array($estados),
						'Reserindividuale.fecha_salida <= ' => $fecha2, 'Reserindividuale.fecha_salida  >= ' => $fecha1,
						
					],
					'order'=> ['Habitacione.numhabitacion'=>'asc']

				]);

		foreach ($r1 as $key) {
			$var1[] = $key['Reserindividuale']['id'];
		}

		$r2 =  $this->Reserindividuale->find('all', 
				[
					'conditions'=>[
						'Reserindividuale.reserstatusindividuale_id' =>array($estados),
						'Reserindividuale.fecha_entrada <= ' => $fecha2, 'Reserindividuale.fecha_salida  >= ' => $fecha2,
						'not'=>$var1
					],
					'order'=> ['Habitacione.numhabitacion'=>'asc']

				]);


	



		$rt = array_merge_recursive( $r1, $r2);

	


		$this->set('reserindividuales', $rt);

		$this->set('pais', $this->Pai->find('all'));
		$this->set('estados', $estados);
		$this->set('name', "pasajeros_alojados_".date('d_m_Y').".pdf");
		$this->set('info', "Reporte: Pasajeros alojados desde el ".$fecha1." hasta el ".$fecha2);

	}



	public function reporte1ajax_old($fecha1, $fecha2, $estados){



		



		$this->layout = 'ajax';		

		//No se usa
		$this->Reserindividuale->recursive = 1;

	


		//------------------------------------------------> NUEVO



		// CUANDO LA FECHA DE ENTRADA ES MENOR O IGUAL A LA PRIMERA ENTRADA DE TODAS LAS RESERVAS
		// AND LA FECHA DE SALIDA ES MAYOR O IGUAL A LA ULTIMA FECHA DE SALIDA REGISTRADA DE X RESERVA

		$r1 = $this->Reserindividuale->find('all', 
				[
					'conditions'=>[
						'Reserindividuale.reserstatusindividuale_id' =>array($estados),
						'Reserindividuale.fecha_entrada >= ' => $fecha1, 'Reserindividuale.fecha_entrada  <= ' => $fecha2
					],
					'order'=> ['Habitacione.numhabitacion'=>'asc']

				]);


		foreach ($r1 as $key) {
			$var1[] = $key['Reserindividuale']['id'];
		}
		//****************
		echo "var1";
		var_dump($var1);

		//--------------------------------------------------> R2
		// Fechas de entrada o de salida que coinciden

		$r2 = $this->Reserindividuale->find('all', 
				[
					'conditions'=>[
						'Reserindividuale.reserstatusindividuale_id' =>array($estados),
						'or'  => ['Reserindividuale.fecha_entrada ' => $fecha1, 'Reserindividuale.fecha_salida ' => $fecha2],
						'not' => [$var1]
						
					],
					'order'=> ['Habitacione.numhabitacion'=>'asc']

				]);

		


		foreach ($r2 as $key) {
			$var2[] = $key['Reserindividuale']['id'];
		}
		//***************************
		echo "var2";
		var_dump($var2);



		$r3 = $this->Reserindividuale->find('all', 
				[
					'conditions'=>[
						'Reserindividuale.reserstatusindividuale_id' =>array($estados),
						'Reserindividuale.fecha_entrada < ' => $fecha1,
						'Reserindividuale.fecha_salida  > ' => $fecha1,
						'not'=>[$var2]
					],
					'order'=> ['Habitacione.numhabitacion'=>'asc']

				]);


		foreach ($r3 as $key) {
			$var3[] = $key['Reserindividuale']['id'];
		}
		echo "var3";
		var_dump($var3);




		$r4 = $this->Reserindividuale->find('all', 
				[
					'conditions'=>[
						'Reserindividuale.reserstatusindividuale_id' =>array($estados),
						'Reserindividuale.fecha_entrada < ' => $fecha2,
						'Reserindividuale.fecha_salida > ' => $fecha2,
						//'not'=>[$var3]
						
					],
					'order'=> ['Habitacione.numhabitacion'=>'asc']

				]);


		foreach ($r4 as $key) {
			$var4[] = $key['Reserindividuale']['id'];
		}
		echo "var4";
		var_dump($var4);



		$r5 = $this->Reserindividuale->find('all', 
				[
					'conditions'=>[
						'Reserindividuale.reserstatusindividuale_id' =>array($estados),
						'Reserindividuale.fecha_entrada < ' => $fecha2,
						'Reserindividuale.fecha_salida > ' => $fecha2,
						'not'=>[$var4]
						
					],
					'order'=> ['Habitacione.numhabitacion'=>'asc']

				]);

		//----------- NUEVAS CONSULTAS

		$r1 =  $this->Reserindividuale->find('all', 
				[
					'conditions'=>[
						'Reserindividuale.reserstatusindividuale_id' =>array($estados),
						'Reserindividuale.fecha_salida <= ' => $fecha2, 'Reserindividuale.fecha_salida  >= ' => $fecha1,
						
					],
					'order'=> ['Habitacione.numhabitacion'=>'asc']

				]);

		foreach ($r1 as $key) {
			$var1[] = $key['Reserindividuale']['id'];
		}

		$r2 =  $this->Reserindividuale->find('all', 
				[
					'conditions'=>[
						'Reserindividuale.reserstatusindividuale_id' =>array($estados),
						'Reserindividuale.fecha_entrada <= ' => $fecha2, 'Reserindividuale.fecha_salida  >= ' => $fecha2,
						'not'=>$var1
					],
					'order'=> ['Habitacione.numhabitacion'=>'asc']

				]);


	



		$rt = array_merge_recursive( $r1, $r2);

	


		$this->set('reserindividuales', $rt);

		$this->set('pais', $this->Pai->find('all'));
		$this->set('estados', $estados);
		$this->set('name', "pasajeros_alojados_".date('d_m_Y').".pdf");
		$this->set('info', "Reporte: Pasajeros alojados desde el ".$fecha1." hasta el ".$fecha2);
	}


	public function reporte1_excell($fecha1, $fecha2, $tipo){
		App::import('Vendor', 'PHPExcel', array('file' => 'PHPExcel_1.8.0_doc/Classes/PHPExcel.php'));
		$this->layout = 'excell';
		$this->Reserindividuale->recursive = 1;

		if ($tipo == '1') {
			$estados = 'Iniciado';	
		}else if ($tipo == '2') {
			$estados = 'Confirmado';
		}else if ($tipo == '3') {
			$estados = 'Auditada';
		}else if($tipo == '4'){
			$estados = 'Ingresada';
		}else if($tipo == '5'){
			$estados = 'Egresada';
		}else if($tipo == '6'){
			$estados = 'No Show';
		}
		
		//Pasajeros proximos a llegar
		$r1 = $this->Reserindividuale->find('all',array('conditions'=>array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($tipo)),'Reserindividuale.fecha_entrada >='=>$fecha1,'Reserindividuale.fecha_salida <='=>$fecha2), 'order'=>array('Habitacione.numhabitacion'=>'asc')));

		$r2 = $this->Reserindividuale->find('all',array('conditions'=>array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($tipo)),'Reserindividuale.fecha_entrada <'=>$fecha1,'Reserindividuale.fecha_salida >'=>$fecha2), 'order'=>array('Habitacione.numhabitacion'=>'asc')));


		$r3 = array_merge($r1, $r2);

		$this->set('reserindividuales', $r3);
			
			$this->set('name', "pasa_".$estados."_".date('d_m_Y').".xls");
			$this->set('info',"Reporte: Pasajeros ".$estados." desde el ".$fecha1." hasta el ".$fecha2);

		
	}


	//=====================================================================> REPORTE 2

	public function grafreporte2(){}

	//Primer reporte
	public function reporte2ajax($fecha1, $fecha2, $estados){
		$this->layout = 'ajax';		
		$this->Reserindividuale->recursive = 1;


		$r1 = $this->Reserindividuale->find('all',array('conditions'=>array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($estados)), 'Reserindividuale.automovil !='=>' ','Reserindividuale.fecha_entrada >='=>$fecha1,'Reserindividuale.fecha_salida <='=>$fecha2), 'order'=>array('Habitacione.numhabitacion'=>'asc')));

		$r2 = $this->Reserindividuale->find('all',array('conditions'=>array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($estados)), 'Reserindividuale.automovil !='=>' ','Reserindividuale.fecha_entrada <= '=>$fecha1,'Reserindividuale.fecha_salida >= '=>$fecha2), 'order'=>array('Habitacione.numhabitacion'=>'asc')));




		$this->set('reserindividuales', array_merge($r1, $r2));


		

		$this->set('estados',$estados);
		$this->set('name', "pasajeros_autos_".date('d_m_Y').".pdf");
		$this->set('info',"Reporte: Pasajeros alojados (con auto) desde el ".$fecha1." hasta el ".$fecha2);


	}



	public function reporte2($fecha1, $fecha2, $tipo){
		$this->layout = 'pdf';
		App::import('Vendor', 'Fpdf', array('file' => 'fpdf181/fpdf.php'));
		$this->Reserindividuale->recursive = 1;


		      if($tipo == '1') {
			$estados = 'Iniciado';	
		}else if($tipo == '2') {
			$estados = 'Confirmado';
		}else if($tipo == '3') {
			$estados = 'Auditada';
		}else if($tipo == '4'){
			$estados = 'Ingresada';
		}else if($tipo == '5'){
			$estados = 'Egresada';
		}else if($tipo == '6'){
			$estados = 'No Show';
		}


		$r1 = $this->Reserindividuale->find('all',array('conditions'=>array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($tipo)), 'Reserindividuale.automovil !='=>'','Reserindividuale.fecha_entrada >='=>$fecha1,'Reserindividuale.fecha_salida <='=>$fecha2), 'order'=>array('Habitacione.numhabitacion'=>'asc')));

		$r2 = $this->Reserindividuale->find('all',array('conditions'=>array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($tipo)), 'Reserindividuale.automovil !='=>'','Reserindividuale.fecha_entrada < '=>$fecha1,'Reserindividuale.fecha_salida > '=>$fecha2), 'order'=>array('Habitacione.numhabitacion'=>'asc')));

		$this->set('reserindividuales', array_merge($r1, $r2));



		$this->set('name', "pasajeros_autos_".$estados."".date('d_m_Y').".pdf");
		$this->set('info',"Reporte: Pasajeros ".$estados." (con auto) desde el ".$fecha1." hasta el ".$fecha2);
		
	}





	public function reporte2_excell($fecha1, $fecha2, $tipo){
		App::import('Vendor', 'PHPExcel', array('file' => 'PHPExcel_1.8.0_doc/Classes/PHPExcel.php'));
		$this->layout = 'excell';
		
		$this->Reserindividuale->recursive = 1;

		      if($tipo == '1') {
			$estados = 'Iniciado';	
		}else if($tipo == '2') {
			$estados = 'Confirmado';
		}else if($tipo == '3') {
			$estados = 'Auditada';
		}else if($tipo == '4'){
			$estados = 'Ingresada';
		}else if($tipo == '5'){
			$estados = 'Egresada';
		}else if($tipo == '6'){
			$estados = 'No Show';
		}


		$r1 = $this->Reserindividuale->find('all',array('conditions'=>array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($tipo)), 'Reserindividuale.automovil !='=>'','Reserindividuale.fecha_entrada >='=>$fecha1,'Reserindividuale.fecha_salida <='=>$fecha2), 'order'=>array('Habitacione.numhabitacion'=>'asc')));
		

		$r2 = $this->Reserindividuale->find('all',array('conditions'=>array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array($tipo)), 'Reserindividuale.automovil !='=>'','Reserindividuale.fecha_entrada < '=>$fecha1,'Reserindividuale.fecha_salida > '=>$fecha2), 'order'=>array('Habitacione.numhabitacion'=>'asc')));
		

		$this->set('reserindividuales', array_merge($r1, $r2));
		
			$this->set('name', "pasajeros_autos_".date('d_m_Y'));
			$this->set('info',"Reporte: Pasajeros ".$estados." desde el ".$fecha1." hasta el ".$fecha2);
	}

	//=====================================================================> REPORTE 3

	public function grafreporte3(){}


	public function reporte3ajax($fecha){
		$this->layout = 'ajax';
		$this->Reserindividuale->recursive = 1;

		$result1 = $this->Reserindividuale->find('all', 
			[
				'conditions'=>[
					'Reserindividuale.reserstatusindividuale_id IN ' => [1,4], 
					'Reserindividuale.fecha_entrada <= ' => $fecha, 'Reserindividuale.fecha_salida  >= ' => $fecha,
					
					'or'=>['Reserindividuale.desayuno = ' => 1, 'Reserindividuale.almuerzo = ' => 1, 'Reserindividuale.cena = ' => 1]
				],
				'order'=> ['Habitacione.numhabitacion'=>'asc']

			]);

		$this->set('reserindividuales',$result1);
		

		$this->set('tipo',1);
	}





	public function reporte3ajax_old($fecha1, $fecha2,$tipo){
		$this->layout = 'ajax';
		
		$this->Reserindividuale->recursive = 1;
	


		$r1 =  $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>array(2,4),'Reserindividuale.fecha_entrada >='=>$fecha1,'Reserindividuale.fecha_salida <= '=>$fecha2,'or'=>array('Reserindividuale.desayuno != '=> 0, 'Reserindividuale.almuerzo != '=> 0,'Reserindividuale.cena != ' => 0)), 'order'=>array('Habitacione.numhabitacion'=>'asc')));

		

		$r2 =  $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$tipo,'Reserindividuale.fecha_entrada <'=>$fecha1,'Reserindividuale.fecha_salida > '=>$fecha2,'or'=>array('Reserindividuale.desayuno != '=> 0, 'Reserindividuale.almuerzo != '=> 0,'Reserindividuale.cena != ' => 0)), 'order'=>array('Habitacione.numhabitacion'=>'asc')));

		$this->set('reserindividuales', array_merge($r1, $r2));
		

		$this->set('tipo',$tipo);
	}






	public function rcheckinout($fecha1, $fecha2,$tipo){
		$this->layout = 'ajax';
		
		$this->Reserindividuale->recursive = 1;
	


		$r1 =  $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$tipo,'Reserindividuale.fecha_entrada ='=>$fecha1,'Reserindividuale.fecha_salida ='=>$fecha2,'or'=>array('Reserindividuale.desayuno != '=> 0, 'Reserindividuale.almuerzo != '=> 0,'Reserindividuale.cena != ' => 0)), 'order'=>array('Habitacione.numhabitacion'=>'asc')));


		$this->set('reserindividuales', array_merge($r1));
		

		$this->set('tipo',$tipo);
	}




	public function rcheckin($fecha1, $tipo){
		$this->layout = 'ajax';
		
		$this->Reserindividuale->recursive = 1;
	

		$r1 =  $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$tipo,'Reserindividuale.fecha_entrada ='=>$fecha1, 'or'=>array('Reserindividuale.desayuno != '=> 0, 'Reserindividuale.almuerzo != '=> 0,'Reserindividuale.cena != ' => 0)), 'order'=>array('Habitacione.numhabitacion'=>'asc')));



		$this->set('reserindividuales', array_merge($r1));
		

		$this->set('tipo',$tipo);
	}


	public function rcheckinoutpdf($fecha1, $fecha2,$tipo){
		$this->layout = 'pdf';
		App::import('Vendor', 'Fpdf', array('file' => 'fpdf181/fpdf.php'));
		$this->Reserindividuale->recursive = 1;


		      if($tipo == '1') {
			$estados = 'Iniciado';	
		}else if($tipo == '2') {
			$estados = 'Confirmado';
		}else if($tipo == '3') {
			$estados = 'Auditada';
		}else if($tipo == '4'){
			$estados = 'Ingresada';
		}else if($tipo == '5'){
			$estados = 'Egresada';
		}else if($tipo == '6'){
			$estados = 'No Show';
		}


		$r1 =  $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$tipo,'Reserindividuale.fecha_entrada ='=>$fecha1,'Reserindividuale.fecha_salida ='=>$fecha2,'or'=>array('Reserindividuale.desayuno != '=> 0, 'Reserindividuale.almuerzo != '=> 0,'Reserindividuale.cena != ' => 0)), 'order'=>array('Habitacione.numhabitacion'=>'asc')));


		$this->set('reserindividuales', array_merge($r1));



		$this->set('name', "check_in_out_".$estados."".date('d_m_Y').".pdf");
		$this->set('info',"Reporte: Check In / Out ".$estados."  desde el ".$fecha1." hasta el ".$fecha2);
	}


		public function rcheckinpdf($fecha1,$tipo){
		$this->layout = 'pdf';
		App::import('Vendor', 'Fpdf', array('file' => 'fpdf181/fpdf.php'));
		$this->Reserindividuale->recursive = 1;


		      if($tipo == '1') {
			$estados = 'Iniciado';	
		}else if($tipo == '2') {
			$estados = 'Confirmado';
		}else if($tipo == '3') {
			$estados = 'Auditada';
		}else if($tipo == '4'){
			$estados = 'Ingresada';
		}else if($tipo == '5'){
			$estados = 'Egresada';
		}else if($tipo == '6'){
			$estados = 'No Show';
		}


		$r1 =  $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$tipo,'Reserindividuale.fecha_entrada ='=>$fecha1,'or'=>array('Reserindividuale.desayuno != '=> 0, 'Reserindividuale.almuerzo != '=> 0,'Reserindividuale.cena != ' => 0)), 'order'=>array('Habitacione.numhabitacion'=>'asc')));


		$this->set('reserindividuales', array_merge($r1));



		$this->set('name', "check_in_".$estados."".date('d_m_Y').".pdf");
		$this->set('info',"Reporte: Check In ".$estados."  de fecha ".$fecha1);
	}

	public function reporte3($fecha1, $fecha2,$tipo){

		$this->layout = 'pdf';
		App::import('Vendor', 'Fpdf', array('file' => 'fpdf181/fpdf.php'));
		//$this->Reserindividuale->recursive = 1;



		// Fecha entrada mayor o igual / Fecha salida menor o igual
		$r1 =  $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>array(2,4),'Reserindividuale.fecha_entrada >='=>$fecha1,'Reserindividuale.fecha_salida <= '=>$fecha2,'or'=>array('Reserindividuale.desayuno != '=> 0, 'Reserindividuale.almuerzo != '=> 0,'Reserindividuale.cena != ' => 0)), 'order'=>array('Habitacione.numhabitacion'=>'asc')));

		// Fecha entrada menor / Fecha salida menor o igual
		$r3 =  $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$tipo,'Reserindividuale.fecha_entrada <'=>$fecha1,'Reserindividuale.fecha_salida <= '=>$fecha2,'or'=>array('Reserindividuale.desayuno != '=> 0, 'Reserindividuale.almuerzo != '=> 0,'Reserindividuale.cena != ' => 0)), 'order'=>array('Habitacione.numhabitacion'=>'asc')));
		
		$r2 =  $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$tipo,'Reserindividuale.fecha_entrada <'=>$fecha1,'Reserindividuale.fecha_salida > '=>$fecha2,'or'=>array('Reserindividuale.desayuno != '=> 0, 'Reserindividuale.almuerzo != '=> 0,'Reserindividuale.cena != ' => 0)), 'order'=>array('Habitacione.numhabitacion'=>'asc')));

		// Fecha entrada mayor o igual / Fecha salida menor
		$r4 =  $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$tipo,'Reserindividuale.fecha_entrada >='=>$fecha1,'Reserindividuale.fecha_salida >= '=>$fecha2,'or'=>array('Reserindividuale.desayuno != '=> 0, 'Reserindividuale.almuerzo != '=> 0,'Reserindividuale.cena != ' => 0)), 'order'=>array('Habitacione.numhabitacion'=>'asc')));

		$r1 =  $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>array(2,4),'Reserindividuale.fecha_entrada >='=>$fecha1,'Reserindividuale.fecha_salida <= '=>$fecha2,'or'=>array('Reserindividuale.desayuno != '=> 0, 'Reserindividuale.almuerzo != '=> 0,'Reserindividuale.cena != ' => 0)), 'order'=>array('Habitacione.numhabitacion'=>'asc')));

		

		$r2 =  $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$tipo,'Reserindividuale.fecha_entrada <'=>$fecha1,'Reserindividuale.fecha_salida > '=>$fecha2,'or'=>array('Reserindividuale.desayuno != '=> 0, 'Reserindividuale.almuerzo != '=> 0,'Reserindividuale.cena != ' => 0)), 'order'=>array('Habitacione.numhabitacion'=>'asc')));

		$this->set('reserindividuales', array_merge($r1,$r2));



		$this->set('name', "pasajeros_en_comedor_".date('d_m_Y').".pdf");
		$this->set('info',"Reporte: Pasajeros comedor desde el ".$fecha1." hasta el ".$fecha2);
	}


	public function reporte3_excell($fecha1, $fecha2,$tipo){
		App::import('Vendor', 'PHPExcel', array('file' => 'PHPExcel_1.8.0_doc/Classes/PHPExcel.php'));
		$this->layout = 'excell';	
		$this->Reserindividuale->recursive = 1;


		$r1 =  $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$tipo,'Reserindividuale.fecha_entrada >='=>$fecha1,'Reserindividuale.fecha_salida <= '=>$fecha2,'or'=>array('Reserindividuale.desayuno != '=> 0, 'Reserindividuale.almuerzo != '=> 0,'Reserindividuale.cena != ' => 0)), 'order'=>array('Habitacione.numhabitacion'=>'asc')));

		$r2 =  $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>$tipo,'Reserindividuale.fecha_entrada <'=>$fecha1,'Reserindividuale.fecha_salida > '=>$fecha2,'or'=>array('Reserindividuale.desayuno != '=> 0, 'Reserindividuale.almuerzo != '=> 0,'Reserindividuale.cena != ' => 0)), 'order'=>array('Habitacione.numhabitacion'=>'asc')));

		$this->set('reserindividuales', array_merge($r1, $r2));



		$this->set('name', "pasajeros_en_comedor_".date('d_m_Y'));
		$this->set('info',"Reporte: Pasajeros comedor desde el ".$fecha1." hasta el ".$fecha2);
	}



	//=====================================================================> REPORTE 4

	public function grafreporte4(){}
	public function reporte4($fecha1, $fecha2){
		$this->layout = 'pdf';
		App::import('Vendor', 'Fpdf', array('file' => 'fpdf181/fpdf.php'));
		$this->Reserindividuale->recursive = 1;
		
		$this->set('reserindividuales', $this->Reserindividuale->find('all',array('conditions'=>array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array(4,5)), 'Reserindividuale.fecha_entrada >='=>$fecha1, 'Reserindividuale.fecha_salida <='=>$fecha2), 'order'=>array('Habitacione.numhabitacion'=>'asc'))));


		$this->set('cant_entrada', $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>4, 'Reserindividuale.fecha_entrada >='=>$fecha1, 'Reserindividuale.fecha_salida <='=>$fecha2), 'order'=>array('Habitacione.numhabitacion'=>'asc'))));

		$this->set('cant_salida', $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>5, 'Reserindividuale.fecha_entrada >='=>$fecha1, 'Reserindividuale.fecha_salida <='=>$fecha2), 'order'=>array('Habitacione.numhabitacion'=>'asc'))));


		$this->set('name', "ingresos_egresos_".date('d_m_Y').".pdf");
		$this->set('info',"Reporte: Ingresos / Egresos desde el ".$fecha1." hasta el ".$fecha2);
	}


	
	public function reporte4ajax($fecha1, $fecha2){
		$this->layout = 'ajax';
		$this->Reserindividuale->recursive = 1;
		
		$this->set('reserindividuales', $this->Reserindividuale->find('all',array('conditions'=>array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array(4,5)), 'Reserindividuale.fecha_entrada >='=>$fecha1, 'Reserindividuale.fecha_salida <='=>$fecha2), 'order'=>array('Habitacione.numhabitacion'=>'asc'))));
		
		$this->set('cant_entrada', $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>4, 'Reserindividuale.fecha_entrada >='=>$fecha1, 'Reserindividuale.fecha_salida <='=>$fecha2), 'order'=>array('Habitacione.numhabitacion'=>'asc'))));

		$this->set('cant_salida', $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>5, 'Reserindividuale.fecha_entrada >='=>$fecha1, 'Reserindividuale.fecha_salida <='=>$fecha2), 'order'=>array('Habitacione.numhabitacion'=>'asc'))));
		
	}

	public function reporte4_excell($fecha1, $fecha2){
		App::import('Vendor', 'PHPExcel', array('file' => 'PHPExcel_1.8.0_doc/Classes/PHPExcel.php'));
		$this->layout = 'excell';
		$this->Reserindividuale->recursive = 1;
		
		$this->set('reserindividuales', $this->Reserindividuale->find('all',array('conditions'=>array('or'=>array('Reserindividuale.reserstatusindividuale_id'=>array(4,5)), 'Reserindividuale.fecha_entrada >='=>$fecha1, 'Reserindividuale.fecha_salida <='=>$fecha2), 'order'=>array('Habitacione.numhabitacion'=>'asc'))));




		$this->set('name', "ingresos_egresos_".date('d_m_Y'));
		$this->set('info',"Reporte: Ingresos / Egresos desde el ".$fecha1." hasta el ".$fecha2);


		$this->set('cant_entrada', $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>4, 'Reserindividuale.fecha_entrada >='=>$fecha1, 'Reserindividuale.fecha_salida <='=>$fecha2), 'order'=>array('Habitacione.numhabitacion'=>'asc'))));

		$this->set('cant_salida', $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>5, 'Reserindividuale.fecha_entrada >='=>$fecha1, 'Reserindividuale.fecha_salida <='=>$fecha2), 'order'=>array('Habitacione.numhabitacion'=>'asc'))));
	}


	//=====================================================================> REPORTE 5

	
	public function grafreporte5(){}
	public function reporte5(){
		$this->layout = 'pdf';
		App::import('Vendor', 'Fpdf', array('file' => 'fpdf181/fpdf.php'));
		$this->Habitacione->recursive = 2;
		$this->set('habitaciones', $this->Habitacione->find('all', array('order'=>array('Habitacione.numhabitacion'=>'asc'))));
		$this->set('name', "informe_habitaciones_".date('d_m_Y').".pdf");
	}
	public function reporte5ajax($fecha1){
		$this->layout = 'ajax';
		
		$this->Habitacione->recursive = 2;
		$this->set('habitaciones', $this->Habitacione->find('all', array('order'=>array('Habitacione.numhabitacion'=>'asc'))));


		$this->set('cant_entrada', $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>4, 'Reserindividuale.fecha_entrada '=>$fecha1), 'order'=>array('Habitacione.numhabitacion'=>'asc'))));

		$this->set('cant_salida',  $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>5, 'Reserindividuale.fecha_entrada '=>$fecha1), 'order'=>array('Habitacione.numhabitacion'=>'asc'))));


		$this->set('reserindividuales', $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.fecha_entrada '=>$fecha1))));
		$this->set('name', "informe_habitaciones_".date('d_m_Y').".pdf");
	}

	public function reporte5_excell(){
		App::import('Vendor', 'PHPExcel', array('file' => 'PHPExcel_1.8.0_doc/Classes/PHPExcel.php'));
		$this->layout = 'excell';
		$this->Habitacione->recursive = 2;
		$this->set('habitaciones', $this->Habitacione->find('all', array('order'=>array('Habitacione.numhabitacion'=>'asc'))));
		$this->set('name', "informe_hab_".date('d_m_Y').".pdf");
	}



	//================================================================= Reporte 6
	// Pasajero Último Pago


	public function grafreporte6(){
		
		$tipoclientes = $this->Reserindividuale->Tipocliente->find('list');
		$tipoclientesubs = $this->Reserindividuale->Tipoclientesub->find('list');
		$clientes = $this->Reserindividuale->Cliente->find('list');
		$this->set(compact('tipoclientes', 'tipoclientesubs', 'clientes'));
	
	}


	public function reporte6ajax($idCliente){
			$this->layout = 'ajax';
			$this->Reserindividuale->recursive = 3;
			$this->Factura->recursive = 2;
			$this->Facturapago->recursive = 2;
			$cliente_id = $idCliente;

			$this->set('reserindividuales', $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.cliente_id'=>$cliente_id), 'order'=>array('Habitacione.numhabitacion'=>'asc')    )));

			$facturas = $this->Factura->find('all',array('conditions'=>array('Factura.cliente_id ' => $cliente_id)));
			
			foreach ($facturas as $key) {
				$array[] = $key['Factura']['id'];
			}

			$this->set('facturas', $this->Facturapago->find('all',array('conditions'=>array('Facturapago.factura_id '=>$array))));



			$this->set('idCliente', $idCliente);
			$this->set('name', "pasajero_ultimo_pago_".date('d_m_Y').".pdf");
		
	
	}

	public function reporte6($idCliente){
		
			$this->layout = 'pdf';
			App::import('Vendor', 'Fpdf', array('file' => 'fpdf181/fpdf.php'));
			$this->Reserindividuale->recursive = 3;
			$this->Factura->recursive = 2;
			$this->Facturapago->recursive = 2;
			$cliente_id = $idCliente;

			$this->set('reserindividuales', $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.cliente_id'=>$cliente_id), 'order'=>array('Habitacione.numhabitacion'=>'asc')    )));

			$facturas = $this->Factura->find('all',array('conditions'=>array('Factura.cliente_id ' => $cliente_id)));
			
			foreach ($facturas as $key) {
				$array[] = $key['Factura']['id'];
			}

			$this->set('facturas', $this->Facturapago->find('all',array('conditions'=>array('Facturapago.factura_id '=>$array))));
			$this->set('name', "pasajero_ultimo_pago_".date('d_m_Y').".pdf");
		
	
	}
	public function reporte6_excell($idCliente){
		
			App::import('Vendor', 'PHPExcel', array('file' => 'PHPExcel_1.8.0_doc/Classes/PHPExcel.php'));
			$this->layout = 'excell';
			$this->Reserindividuale->recursive = 3;
			$this->Factura->recursive = 2;
			$this->Facturapago->recursive = 2;
			$cliente_id = $idCliente;


			$this->set('reserindividuales', $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.cliente_id'=>$cliente_id), 'order'=>array('Habitacione.numhabitacion'=>'asc')    )));

			$facturas = $this->Factura->find('all',array('conditions'=>array('Factura.cliente_id ' => $cliente_id)));
			
			foreach ($facturas as $key) {
				$array[] = $key['Factura']['id'];
			}

			$this->set('facturas', $this->Facturapago->find('all',array('conditions'=>array('Facturapago.factura_id '=>$array))));

			
			$this->set('name', "pasajero_ultimo_pago_".date('d_m_Y'));
		
	
	}
	//====================================================================================================

	public function grafreporte7(){}
	public function reporte7(){
		$this->layout = 'pdf';
		App::import('Vendor', 'Fpdf', array('file' => 'fpdf181/fpdf.php'));
		$this->Reserindividuale->recursive = 1;
		$this->set('reserindividuales', $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>4), 'order'=>array('Habitacione.numhabitacion'=>'asc')    )));
		$this->set('name', "ingresos_fechas_".date('d_m_Y').".pdf");
	}

	public function reporte7_excell(){
		App::import('Vendor', 'PHPExcel', array('file' => 'PHPExcel_1.8.0_doc/Classes/PHPExcel.php'));
		$this->layout = 'excell';
		$this->Reserindividuale->recursive = 1;
		$this->set('reserindividuales', $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.reserstatusindividuale_id'=>4), 'order'=>array('Habitacione.numhabitacion'=>'asc')    )));
		$this->set('name', "ingresos_fechas_".date('d_m_Y'));
	}


	/**
	 * sub_cliente method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
		public function sub_cliente($id = null) {
			$this->layout = 'ajax';
		    $subclientes = $this->Reserindividuale->Tipoclientesub->find('list', array('fields'=>array('Tipoclientesub.id', 'Tipoclientesub.nombre'), 'conditions'=>array('Tipoclientesub.tipocliente_id'=>$id)));
		    $this->set(compact('subclientes'));
		    $this->set('id', $id);

		}

	/**
	 * cliente method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
		public function cliente($id = null, $id2 = null) {
			$this->layout = 'ajax';
		    $clientes = $this->Reserindividuale->Cliente->find('list', array('fields'=>array('Cliente.id', 'Cliente.nombre_completo'), 'conditions'=>array('Cliente.tipocliente_id'=>$id, 'Cliente.tipoclientesub_id'=>$id2)));
		    $this->set(compact('clientes'));

		}


		public function grafreporte10(){}
}
?>