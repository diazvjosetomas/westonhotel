<?php
App::uses('AppController', 'Controller');
/**
 * Tmpreservas Controller
 *
 * @property Tmpreserva $Tmpreserva
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class TmpreservasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Tmpreserva->recursive = 0;
		$this->set('tmpreservas', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Tmpreserva->exists($id)) {
			throw new NotFoundException(__('Invalid tmpreserva'));
		}
		$options = array('conditions' => array('Tmpreserva.' . $this->Tmpreserva->primaryKey => $id));
		$this->set('tmpreserva', $this->Tmpreserva->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Tmpreserva->create();
			if ($this->Tmpreserva->save($this->request->data)) {
				$this->Flash->success(__('The tmpreserva has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The tmpreserva could not be saved. Please, try again.'));
			}
		}
		$tipotemporadas = $this->Tmpreserva->Tipotemporada->find('list');
		$tipohabitaciones = $this->Tmpreserva->Tipohabitacione->find('list');
		$cupos = $this->Tmpreserva->Cupo->find('list');
		$this->set(compact('tipotemporadas', 'tipohabitaciones', 'cupos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Tmpreserva->exists($id)) {
			throw new NotFoundException(__('Invalid tmpreserva'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Tmpreserva->save($this->request->data)) {
				$this->Flash->success(__('The tmpreserva has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The tmpreserva could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Tmpreserva.' . $this->Tmpreserva->primaryKey => $id));
			$this->request->data = $this->Tmpreserva->find('first', $options);
		}
		$tipotemporadas = $this->Tmpreserva->Tipotemporada->find('list');
		$tipohabitaciones = $this->Tmpreserva->Tipohabitacione->find('list');
		$cupos = $this->Tmpreserva->Cupo->find('list');
		$this->set(compact('tipotemporadas', 'tipohabitaciones', 'cupos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Tmpreserva->id = $id;
		if (!$this->Tmpreserva->exists()) {
			throw new NotFoundException(__('Invalid tmpreserva'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Tmpreserva->delete()) {
			$this->Flash->success(__('The tmpreserva has been deleted.'));
		} else {
			$this->Flash->error(__('The tmpreserva could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
