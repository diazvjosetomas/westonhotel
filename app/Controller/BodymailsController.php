<?php
App::uses('AppController', 'Controller');
/**
 * Bodymails Controller
 *
 * @property Bodymail $Bodymail
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class BodymailsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	


/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Bodymail->recursive = 0;
		//$this->set('bodymails', $this->Paginator->paginate());
		  $this->set('bodymails', $this->Bodymail->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Bodymail->exists($id)) {
			throw new NotFoundException(__('Invalid bodymail'));
		}
		$options = array('conditions' => array('Bodymail.' . $this->Bodymail->primaryKey => $id));
		$this->set('bodymail', $this->Bodymail->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Bodymail->create();
			if ($this->Bodymail->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Bodymail->exists($id)) {
			throw new NotFoundException(__('Invalid bodymail'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Bodymail->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Bodymail.' . $this->Bodymail->primaryKey => $id));
			$this->request->data = $this->Bodymail->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Bodymail->id = $id;
		if (!$this->Bodymail->exists()) {
			throw new NotFoundException(__('Invalid bodymail'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Bodymail->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
