<?php
App::uses('AppController', 'Controller');
/**
 * Habistatuses Controller
 *
 * @property Habistatus $Habistatus
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class HabistatusesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(2);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Habistatus->recursive = 0;
		//$this->set('habistatuses', $this->Paginator->paginate());
		  $this->set('habistatuses', $this->Habistatus->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Habistatus->exists($id)) {
			throw new NotFoundException(__('Invalid habistatus'));
		}
		$options = array('conditions' => array('Habistatus.' . $this->Habistatus->primaryKey => $id));
		$this->set('habistatus', $this->Habistatus->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Habistatus->create();
			if ($this->Habistatus->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Habistatus->exists($id)) {
			throw new NotFoundException(__('Invalid habistatus'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Habistatus->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Habistatus.' . $this->Habistatus->primaryKey => $id));
			$this->request->data = $this->Habistatus->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Habistatus->id = $id;
		if (!$this->Habistatus->exists()) {
			throw new NotFoundException(__('Invalid habistatus'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Habistatus->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
