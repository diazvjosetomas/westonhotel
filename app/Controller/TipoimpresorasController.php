<?php
App::uses('AppController', 'Controller');
/**
 * Tipoimpresoras Controller
 *
 * @property Tipoimpresora $Tipoimpresora
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class TipoimpresorasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(13);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Tipoimpresora->recursive = 0;
		//$this->set('tipoimpresoras', $this->Paginator->paginate());
		  $this->set('tipoimpresoras', $this->Tipoimpresora->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Tipoimpresora->exists($id)) {
			throw new NotFoundException(__('Invalid tipoimpresora'));
		}
		$options = array('conditions' => array('Tipoimpresora.' . $this->Tipoimpresora->primaryKey => $id));
		$this->set('tipoimpresora', $this->Tipoimpresora->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Tipoimpresora->create();
			if ($this->Tipoimpresora->save($this->request->data)) {
				$this->Flash->success(__('The tipoimpresora has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The tipoimpresora could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Tipoimpresora->exists($id)) {
			throw new NotFoundException(__('Invalid tipoimpresora'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Tipoimpresora->save($this->request->data)) {
				$this->Flash->success(__('The tipoimpresora has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The tipoimpresora could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Tipoimpresora.' . $this->Tipoimpresora->primaryKey => $id));
			$this->request->data = $this->Tipoimpresora->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Tipoimpresora->id = $id;
		if (!$this->Tipoimpresora->exists()) {
			throw new NotFoundException(__('Invalid tipoimpresora'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Tipoimpresora->delete()) {
			$this->Flash->success(__('The tipoimpresora has been deleted.'));
		} else {
			$this->Flash->error(__('The tipoimpresora could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
