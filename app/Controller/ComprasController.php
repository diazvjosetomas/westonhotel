<?php
App::uses('AppController', 'Controller');
/**
 * Compras Controller
 *
 * @property Compra $Compra
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ComprasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

	var $uses = array('Compra', 'Proproductos', 'Compraproducto');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(5);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Compra->recursive = 0;
		//$this->set('compras', $this->Paginator->paginate());
		  $this->set('compras', $this->Compra->find('all'));
	}

/**
 * index method
 *
 * @return void
 */
	public function cuentaxpagar() {
         $this->set('compras', $this->Compra->find('all', array('conditions'=>array('Compra.deuda !='=>0))));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Compra->exists($id)) {
			throw new NotFoundException(__('Invalid compra'));
		}
		$options = array('conditions' => array('Compra.' . $this->Compra->primaryKey => $id));
		$this->set('compra', $this->Compra->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {

		if ($this->request->is('post')) {

			$this->Compra->create();
			$this->Compra->begin();
			$this->request->data['Compra']['fecha_compra'] = !empty($this->request->data['Compra']['fecha_compra1']) ? formatfecha($this->request->data['Compra']['fecha_compra1']) : '';
            if($this->request->data["Compra"]['tipocompra_id']==1){
               $this->request->data["Compra"]['pagado'] = $this->request->data["Compra"]['total'];
               $this->request->data["Compra"]['deuda'] = 0;
            }else{
               $this->request->data["Compra"]['pagado'] = 0;
               $this->request->data["Compra"]['deuda']  = $this->request->data["Compra"]['total'];
            }

			if ($this->Compra->save($this->request->data)) {
				$id   =  $this->Compra->id;
				$stop = 0;

				

				if(!empty($this->request->data["Compra"]["Productos"])){
					foreach($this->request->data["Compra"]["Productos"] as $producto){
						$this->request->data["Compraproducto"]["compra_id"] = $id;
						$this->request->data["Compraproducto"]["proproducto_id"] = $producto['pro'];
						$this->request->data["Compraproducto"]["cantidad"]       = $producto['cant'];
						$this->request->data["Compraproducto"]["precio"]         = $producto['pre'];
						$this->request->data["Compraproducto"]["total"]          = $producto['total'];
						$this->Compraproducto->create();
						if ($this->Compraproducto->save($this->request->data)){

						}else{
							$stop = 1;
						}
					}
				}
				if($stop==0){
                    $this->Compra->commit();
                	$this->Flash->success(__('Registro Guardado.'));
					return $this->redirect(array('action' => 'index'));

                }else{
                	$this->Compra->rollback();
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                }
			} else {
				$this->Compra->rollback();
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
		$proveedores = $this->Compra->Proveedore->find('list');
		$tipocompras = $this->Compra->Tipocompra->find('list');
		$proproductos = $this->Proproductos->find('all');
		$this->set(compact('proveedores', 'tipocompras', 'proproductos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Compra->exists($id)) {
			throw new NotFoundException(__('Invalid compra'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Compra']['fecha_compra'] = !empty($this->request->data['Compra']['fecha_compra1']) ? formatfecha($this->request->data['Compra']['fecha_compra1']) : '';
			$this->Compra->begin();
			if ($this->Compra->save($this->request->data)) {
				$this->Compraproducto->deleteAll(array('Compraproducto.compra_id'=>$id));
				$stop = 0;
				if(!empty($this->request->data["Compra"]["Productos"])){
					foreach($this->request->data["Compra"]["Productos"] as $producto){
						$this->request->data["Compraproducto"]["compra_id"] = $id;
						$this->request->data["Compraproducto"]["proproducto_id"] = $producto['pro'];
						$this->request->data["Compraproducto"]["cantidad"]       = $producto['cant'];
						$this->request->data["Compraproducto"]["precio"]         = $producto['pre'];
						$this->request->data["Compraproducto"]["total"]          = $producto['total'];
						$this->Compraproducto->create();
						if ($this->Compraproducto->save($this->request->data)){

						}else{
							$stop = 1;
						}
					}
				}
				if($stop==0){
                    $this->Compra->commit();
                	$this->Flash->success(__('Registro Guardado.'));
					return $this->redirect(array('action' => 'index'));

                }else{
                	$this->Compra->rollback();
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                    return $this->redirect(array('action' => 'edit/'.$id));
                }
			} else {
				$this->Compra->rollback();
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Compra.' . $this->Compra->primaryKey => $id));
			$this->request->data = $this->Compra->find('first', $options);
			$this->request->data['Compra']['fecha_compra1'] = formatdmy($this->request->data['Compra']['fecha_compra']);
		}
		$compraproductos = $this->Compraproducto->find('all', array('conditions'=>array('compra_id'=>$id)));
		$proveedores     = $this->Compra->Proveedore->find('list', array('conditions'=>array('Proveedore.id'=>$this->request->data['Compra']['proveedore_id'])));
		$tipocompras     = $this->Compra->Tipocompra->find('list');
		$proproductos    = $this->Proproductos->find('all');
		$this->set(compact('proveedores', 'tipocompras', 'compraproductos', 'proproductos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Compra->id = $id;
		if (!$this->Compra->exists()) {
			throw new NotFoundException(__('Invalid compra'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Compra->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


/**
 * precio_p method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function precio(){
		$this->layout = "ajax";
		$proproductos = $this->Proproductos->find('all', array('conditions'=>array('Proproductos.id'=>$this->request->data['id'])));
		$this->set('precio', isset($proproductos[0]['Proproductos']['pvp'])?$proproductos[0]['Proproductos']['pvp']:0);
	}




/**
 * abonar method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function abonar($id = null) {
		return $this->redirect('/Compraabonas/add/'.$id);
	}








}
