<?php
App::uses('AppController', 'Controller');
/**
 * Reserindividuales Controller
 *
 * @property Reserindividuale $Reserindividuale
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ReserindividualesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session', 'Flash');

    var $uses = array('Reserindividuale', 'Pstemporada', 'Empresa', 'Cliente', 'Tipotemporada', 'Habitacione', 'Reserindividualeextra', 'Resermulhabitacione', 'Resermultiple', 'Pai','Bodymail','Reserindivistatu','Setting','Cupo', 'Tipohabitacione');

    /*
    ** var de layout
    *
    */
    public $layout = "dashbord";

    /*
    *  *  beforeFilter check de session
    *
    */
    /*	public function beforeFilter() {
            $this->checkSession(6);
        }*/
    /**
     * index method
     *
     * @return void
     */
    public function consulta_habitacion() {
        $this->layout='ajax';
        $a = $this->request->data['fechai'];
        $b = $this->request->data['fechas'];
        $c = $this->request->data['personas'];

        echo $a;

        $dispcupos = $this->Cupo->find('all', 
                                              [

                                                'conditions'=>[

                                                    'Cupo.fecha'=>$a


                                                ]
                                              

                                              ]);

        

        foreach ($dispcupos as $key) {
          $id[] = $key['Cupo']['tipohabitacione_id'];
        }



        




        $opciones= array('conditions'=> array('Tipohabitacione.cantninos >= ' => $c, 'Tipohabitacione.id'=>$id));



        $data = $this->Tipohabitacione->find('all',$opciones);


        //Buscar en la tabla cupo disponibilidad



        

        if (count($data) > 0 ){
            $this->set('data', $data);
        }else{
            $this->set('data', 0);

        }

    }

    public function index($id=null) {
        //$this->Reserindividuale->recursive = 0;
        //$this->set('reserindividuales', $this->Paginator->paginate());
        $this->set('reserindividuales', $this->Reserindividuale->find('all'));
        if($id!=null){$this->set('id_reporte', $id);}
    }
    public function inicio($id=null) {
        //$this->Reserindividuale->recursive = 0;
        //$this->set('reserindividuales', $this->Paginator->paginate());
        $this->set('reserindividuales', $this->Reserindividuale->find('all'));
        if($id!=null){$this->set('id_reporte', $id);}
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Reserindividuale->exists($id)) {
            throw new NotFoundException(__('Invalid reserindividuale'));
        }


        $this->set('detallesextra', $this->Reserindivistatu->find('all', array('conditions'=>array('Reserindivistatu.reserindividuale_id = ' => $id))));
        $options = array('conditions' => array('Reserindividuale.' . $this->Reserindividuale->primaryKey => $id));
        $this->set('reserindividuale', $this->Reserindividuale->find('first', $options));
    }

    public function get_previous($fecha_salida = null, $habitacione_id = null) {

        $nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha_salida ) ) ;
        $nuevafecha = date ( 'Y-m-d' , $nuevafecha );

        $data = $this->Reserindividuale->find('first', [
            'conditions' => [
                'Reserindividuale.fecha_entrada'  => $nuevafecha,
                'Reserindividuale.habitacione_id' => $habitacione_id
            ]
        ]);

        if ( !empty($data) ){
            return true;
        }

        return false;
    }


    public function datatipohabitacion($idtipohabitacion){
        $this->layout = 'ajax';
        $this->Tipohabitacione->recursive = 0;
        $this->set('datatipohabitacion', $this->Tipohabitacione->find('all', array('conditions'=>array('Tipohabitacione.id'=>$idtipohabitacion))));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {


            $this->request->data['Reserindividuale']['tipohabitaciones'] = json_encode($this->request->data['Reserindividuale']['tipohabitaciones']);





            $this->request->data['Reserindividuale']['fecha_entrada'] = !empty($this->request->data['Reserindividuale']['fecha_entrada1']) ? formatfecha($this->request->data['Reserindividuale']['fecha_entrada1']) : '';
            $this->request->data['Reserindividuale']['fecha_salida'] = !empty($this->request->data['Reserindividuale']['fecha_salida1']) ? formatfecha($this->request->data['Reserindividuale']['fecha_salida1']) : '';

            $cliente = $this->Cliente->find('first', [ [ 'conditions' => [ 'Cliente.id' => $this->request->data['Reserindividuale']['cliente_id'] ] ] ]);
            if(!empty($cliente)){

                $this->request->data['Reserindividuale']['tipocliente_id']    = $cliente['Tipocliente']['id'];
                $this->request->data['Reserindividuale']['tipoclientesub_id'] = $cliente['Cliente']['tipoclientesub_id'];
                $this->request->data['Reserindividuale']['resermultiple_id']  = 0;

            }else{
                $this->request->data['Reserindividuale']['tipocliente_id']    = 0;
                $this->request->data['Reserindividuale']['tipoclientesub_id'] = 0;
                $this->request->data['Reserindividuale']['resermultiple_id']  = 0;
            }

            $this->Reserindividuale->create();
            $this->Reserindividuale->begin();


            if ($this->Reserindividuale->save($this->request->data)) {
                // $this->request->data['Habitacione']['id']           = $this->request->data['Reserindividuale']['habitacione_id'];
                // $this->request->data['Habitacione']['habistatu_id'] = 2;
                //if ($this->Habitacione->save($this->request->data)) {

                $preferencias = $this->request->data['Reserindividuale']['observaciones_cliente'];
                $idclienter   = $this->request->data['Reserindividuale']['cliente_id'];
                $sqlUpdateCliente = "UPDATE clientes SET preferencias = '".$preferencias."' WHERE id =  $idclienter";

                $this->Cliente->query($sqlUpdateCliente);



                $last_id = $this->Reserindividuale->getLastInsertID();
                $id   =  $this->Reserindividuale->id;
                $stop = 0;
                if(!empty($this->request->data["Reserindividuale"]["Nombre"])){
                    foreach($this->request->data["Reserindividuale"]["Nombre"] as $nombre){
                        $this->request->data["Reserindividualeextra"]["reserindividuale_id"] = $id;
                        $this->request->data["Reserindividualeextra"]["nombres"] = $nombre;
                        $this->Reserindividualeextra->create();
                        if ($this->Reserindividualeextra->save($this->request->data)){

                        }else{
                            $stop = 1;
                        }
                    }
                }
                if($stop==0){
                    $this->Reserindividuale->commit();
                    $empresas = $this->Empresa->find('all');
                    $clientes = $this->Cliente->find('all', array('conditions'=>array('Cliente.id'=>$this->request->data['Reserindividuale']['cliente_id'])));






                    /**
                     * Enviamos mail
                     */
                    $idcliente = $this->request->data["Reserindividuale"]["cliente_id"];
                    $datacliente = $this->Cliente->find('all', array('conditions'=>array('Cliente.id'=>$idcliente)));
                    $email = $datacliente[0]['Cliente']['correo'];
                    $nombres = $datacliente[0]['Cliente']['nombre_completo'];
                    $fechainicio = $this->request->data["Reserindividuale"]["fecha_entrada"];
                    $fechasalida = $this->request->data["Reserindividuale"]["fecha_salida"];
                    $total = $this->request->data["Reserindividuale"]["total"];




                    /**
                     * Extraemos el cuerpo de el mensaje
                     */


                    $cabeceras  = "MIME-Version: 1.0\r\n";
                    $cabeceras .= "Content-Transfer-Encoding: 8Bit\r\n";
                    $cabeceras .= "Content-Type: text/html; charset=\"utf-8\"\r\n";
                    $cabeceras .= "Reply-to: \r\n";
                    $cabeceras .= "From: serviciodereservas \r\n";
                    $cabeceras .= "Errors-To: \r\n";


                    /**
                     * Mail para reserva iniciada
                     */

                    $bodymail = $this->Bodymail->find('all', array('conditions'=>array('Bodymail.id'=>1)));

                    $subject = $bodymail[0]['Bodymail']['denominacion'];

                    $bodymail = $bodymail[0]['Bodymail']['cuerpoemail'];

                    $cuerpo = str_replace('_nombres_', $nombres, $bodymail);

                    $cuerpo = str_replace('_fechainicio_', $fechainicio, $cuerpo);

                    $cuerpo = str_replace('_fechasalida_', $fechasalida, $cuerpo);

                    $cuerpo = str_replace('_valorreserva_', $total, $cuerpo);



                    App::import('Vendor', 'PHPMailer', array('file' => 'phpmailer/class.phpmailer.php'));
                        $mail_r = new PHPMailer();
                        $mail_r->IsHTML(true);
                        $mail_r->IsSMTP();
                        $mail_r->SMTPAuth   = true;                  // enable SMTP authentication
                        $mail_r->SMTPSecure = "ssl";                 // sets the prefix to the servier
                        $mail_r->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
                        $mail_r->Port       = 465;                   // set the SMTP port for the GMAIL server
                        $mail_r->Username   = 'diazvjosetomas@gmail.com';  // GMAIL username
                        $mail_r->Password   = '9gdi3MZzAubw8rksaR';   // GMAIL password
                        $mail_r->From       = 'webmaster@hoteleria.tk';
                        $mail_r->FromName   = 'Reservas';
                        $mail_r->AddAddress($email,'Estimado usuario');
                        $mail_r->Subject  = "Confirmacion de la reserva";
                        $email_a_revista  = "<table class='shop-item-selections'>
                          <tr>
                              <td width='100%'>
                                  Saludos Estimado ".$email." 
                                  <br>  
                                  El siguiente correo es un enlace de confirmación por el registro de su reserva  en Hoteleria.tk
                                  con fecha de entrada ".$fechainicio." y fecha de salida ".$fechasalida."
                                  <br>
                                  Gracias por Preferirnos!!!               
                              </td>
                            </tr>
                          </table>";
                        $mail_r->MsgHTML($email_a_revista);
                        $mail_r->Send();


                   // mail($email,$subject,$cuerpo, $cabeceras, 'CC: diazvjosetomas@hotmail.com');



                    $auditoria['Auditoria']['user_id']             = $this->Session->read('USUARIO_ID');
                    $auditoria['Auditoria']['habitacione_id']      = $this->request->data['Reserindividuale']['habitacione_id'];
                    $auditoria['Auditoria']['reserindividuale_id'] = $last_id;
                    $auditoria['Auditoria']['cliente_id']          = $this->request->data['Reserindividuale']['cliente_id'];
                    $auditoria['Auditoria']['factura_id']          = 0;
                    $auditoria['Auditoria']['hora']                = DboSource::expression('NOW()');
                    $auditoria['Auditoria']['motivo']              = 'N/A';
                    $auditoria['Auditoria']['tipo_operacion']      = 'NUEVA RESERVA';
                    $auditoria['Auditoria']['especifico']          = 'RESERVA CREADA';
                    $auditoria['Auditoria']['fecha_entrada']       = $this->request->data['Reserindividuale']['fecha_entrada'];
                    $auditoria['Auditoria']['fecha_salida']        = $this->request->data['Reserindividuale']['fecha_salida'];

                    $auditoria['Auditoria']['num_habitacion']      = $this->requestAction('habitaciones/get_numero/'.$this->request->data['Reserindividuale']['habitacione_id']);
                    $auditoria['Auditoria']['capacidad']           = $this->requestAction('habitaciones/get_capacidad/'.$this->request->data['Reserindividuale']['habitacione_id']);
                    $auditoria['Auditoria']['detalle_reserva']     = !empty($this->request->data['Reserindividuale']['detallereserva']) ? $this->request->data['Reserindividuale']['detallereserva'] : '';
                    $auditoria['Auditoria']['costo_total']         = $this->request->data['Reserindividuale']['total'];
                    $auditoria['Auditoria']['forma_pago']          = '-';
                    $auditoria['Auditoria']['num_factura']         = 0;


                    $this->loadModel('Auditoria');
                    $this->Auditoria->save($auditoria);


                    $this->Flash->success(__('Registro Guardado.'));
                    if(!empty($this->request->data['Reserindividuale']['inicio'])){
                        return $this->redirect(array('action' => 'index'));
                    }else{
                    return $this->redirect(array('action' => 'index'));
                    }
                }else{
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                }
                /*}else{
                	$this->Reserindividuale->rollback();
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                }*/
            } else {
                $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
            }




        }
        $tipoclientes = $this->Reserindividuale->Tipocliente->find('list');
        $tipoclientesubs = $this->Reserindividuale->Tipoclientesub->find('list');
        $clientes = $this->Reserindividuale->Cliente->find('list');
        $tipohabitaciones = $this->Reserindividuale->Tipohabitacione->find('list');
        

        $this->set('tphabit',  $this->Reserindividuale->Tipohabitacione->find('all') ) ;

        $habitaciones = $this->Reserindividuale->Habitacione->find('list');
        $reserstatusindividuales = $this->Reserindividuale->Reserstatusindividuale->find('list');
        $resermultiples = $this->Reserindividuale->Resermultiple->find('list');
        $pais = $this->Pai->find('list');
        $this->set(compact('pais','tipoclientes', 'tipoclientesubs', 'clientes', 'tipohabitaciones', 'habitaciones', 'reserstatusindividuales', 'resermultiples'));
    }






    /**
     * Añadir con habitacion predeterminada y fecha
     *
     * @return void
     */
    public function add_reserva($hab = null, $dia = null, $mes = null, $anhio = null) {
        if ($this->request->is('post')) {
            $this->request->data['Reserindividuale']['fecha_entrada'] = !empty($this->request->data['Reserindividuale']['fecha_entrada']) ? formatfecha($this->request->data['Reserindividuale']['fecha_entrada']) : '';
            $this->request->data['Reserindividuale']['fecha_salida'] = !empty($this->request->data['Reserindividuale']['fecha_salida']) ? formatfecha($this->request->data['Reserindividuale']['fecha_salida']) : '';
            $this->Reserindividuale->create();
            $this->Reserindividuale->begin();
            if ($this->Reserindividuale->save($this->request->data)) {
                $last_id = $this->Reserindividuale->getLastInsertID();
                // $this->request->data['Habitacione']['id']           = $this->request->data['Reserindividuale']['habitacione_id'];
                // $this->request->data['Habitacione']['habistatu_id'] = 2;
                //if ($this->Habitacione->save($this->request->data)) {


                $id   =  $this->Reserindividuale->id;
                $stop = 0;
                if(!empty($this->request->data["Reserindividuale"]["Nombre"])){
                    foreach($this->request->data["Reserindividuale"]["Nombre"] as $nombre){
                        $this->request->data["Reserindividualeextra"]["reserindividuale_id"] = $id;
                        $this->request->data["Reserindividualeextra"]["nombres"] = $nombre;
                        $this->Reserindividualeextra->create();
                        if ($this->Reserindividualeextra->save($this->request->data)){

                        }else{
                            $stop = 1;
                        }
                    }
                }
                if($stop==0){
                    $this->Reserindividuale->commit();
                    $empresas = $this->Empresa->find('all');
                    $clientes = $this->Cliente->find('all', array('conditions'=>array('Cliente.id'=>$this->request->data['Reserindividuale']['cliente_id'])));

                    $auditoria['Auditoria']['user_id']             = $this->Session->read('USUARIO_ID');
                    $auditoria['Auditoria']['habitacione_id']      = $this->request->data['Reserindividuale']['habitacione_id'];
                    $auditoria['Auditoria']['reserindividuale_id'] = $last_id;
                    $auditoria['Auditoria']['cliente_id']          = $this->request->data['Reserindividuale']['cliente_id'];
                    $auditoria['Auditoria']['factura_id']          = 0;
                    $auditoria['Auditoria']['hora']                = DboSource::expression('NOW()');
                    $auditoria['Auditoria']['motivo']              = 'N/A';
                    $auditoria['Auditoria']['tipo_operacion']      = 'NUEVA RESERVA';
                    $auditoria['Auditoria']['especifico']          = 'RESERVA CREADA';
                    $auditoria['Auditoria']['fecha_entrada']       = $this->request->data['Reserindividuale']['fecha_entrada'];
                    $auditoria['Auditoria']['fecha_salida']        = $this->request->data['Reserindividuale']['fecha_salida'];

                    $auditoria['Auditoria']['num_habitacion']      = $this->requestAction('habitaciones/get_numero/'.$this->request->data['Reserindividuale']['habitacione_id']);
                    $auditoria['Auditoria']['capacidad']           = $this->requestAction('habitaciones/get_capacidad/'.$this->request->data['Reserindividuale']['habitacione_id']);
                    $auditoria['Auditoria']['detalle_reserva']     = !empty($this->request->data['Reserindividuale']['detallereserva']) ? $this->request->data['Reserindividuale']['detallereserva'] : '' ;
                    $auditoria['Auditoria']['costo_total']         = $this->request->data['Reserindividuale']['total'];
                    $auditoria['Auditoria']['forma_pago']          = '';
                    $auditoria['Auditoria']['num_factura']         = 0;

                    $this->loadModel('Auditoria');
                    $this->Auditoria->save($auditoria);



                    /**
                     * Enviamos mail
                     */
                    $idcliente = $this->request->data["Reserindividuale"]["cliente_id"];
                    $datacliente = $this->Cliente->find('all', array('conditions'=>array('Cliente.id'=>$idcliente)));
                    $email = $datacliente[0]['Cliente']['correo'];
                    $nombres = $datacliente[0]['Cliente']['nombre_completo'];
                    $fechainicio = $this->request->data["Reserindividuale"]["fecha_entrada"];
                    $fechasalida = $this->request->data["Reserindividuale"]["fecha_salida"];




                    /**
                     * Extraemos el cuerpo de el mensaje
                     */


                    $cabeceras  = "MIME-Version: 1.0\r\n";
                    $cabeceras .= "Content-Transfer-Encoding: 8Bit\r\n";
                    $cabeceras .= "Content-Type: text/html; charset=\"utf-8\"\r\n";
                    $cabeceras .= "Reply-to: \r\n";
                    $cabeceras .= "From: serviciodereservas \r\n";
                    $cabeceras .= "Errors-To: \r\n";


                    /**
                     * Mail para reserva iniciada
                     */

                    $bodymail = $this->Bodymail->find('all', array('conditions'=>array('Bodymail.id'=>1)));

                    $subject = $bodymail[0]['Bodymail']['denominacion'];

                    $bodymail = $bodymail[0]['Bodymail']['cuerpoemail'];

                    $cuerpo = str_replace('_nombres_', $nombres, $bodymail);

                    $cuerpo = str_replace('_fechainicio_', $fechainicio, $cuerpo);

                    $cuerpo = str_replace('_fechasalida_', $fechasalida, $cuerpo);





                    mail($email,$subject,$cuerpo, $cabeceras);


                    $this->Flash->success(__('Registro Guardado.'));



                    return $this->redirect(array('controller'=>'Reservas','action' => 'reservas'));
                }else{
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                }
                /*}else{
                    $this->Reserindividuale->rollback();
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                }*/
            } else {
                $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
            }
        }



        /**
         * Sacaremos el tipo de habitacion con el numero de habitacion
         */
        $datahab = $this->Habitacione->find('all', array('conditions'=>array('Habitacione.numhabitacion'=>$hab)));


        $tipohab = $datahab[0]['Habitacione']['tipohabitacione_id'];



        if ($dia < 10) {
            $dia = '0'.$dia;
        }

        $this->set('fecha_entrada_establecida', $anhio.'-'.$mes.'-'.$dia);

        $this->set('numhabitacion', $hab );

        $tipoclientes = $this->Reserindividuale->Tipocliente->find('list');
        $tipoclientesubs = $this->Reserindividuale->Tipoclientesub->find('list');
        $clientes = $this->Reserindividuale->Cliente->find('list');
        $tipohabitaciones = $this->Reserindividuale->Tipohabitacione->find('list', array('conditions'=>array('Tipohabitacione.id'=>$tipohab)));
        $habitaciones = $this->Reserindividuale->Habitacione->find('list', array('conditions'=>array('Habitacione.numhabitacion'=>$hab)));

        $reserstatusindividuales = $this->Reserindividuale->Reserstatusindividuale->find('list');
        $resermultiples = $this->Reserindividuale->Resermultiple->find('list');
        $pais = $this->Pai->find('list');
        $this->set(compact('pais','tipoclientes', 'tipoclientesubs', 'clientes', 'tipohabitaciones', 'habitaciones', 'reserstatusindividuales', 'resermultiples'));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add_from_calendar() {
        if ($this->request->is('post')) {

            $this->request->data['Reserindividuale']['fecha_entrada'] = !empty($this->request->data['Reserindividuale']['fecha_entrada1']) ? formatfecha($this->request->data['Reserindividuale']['fecha_entrada1']) : '';
            $this->request->data['Reserindividuale']['fecha_salida'] = !empty($this->request->data['Reserindividuale']['fecha_salida1']) ? formatfecha($this->request->data['Reserindividuale']['fecha_salida1']) : '';

            $this->Reserindividuale->create();
            $this->Reserindividuale->begin();
            if ($this->Reserindividuale->save($this->request->data)) {
                $last_id = $this->Reserindividuale->getLastInsertID();
                // $this->request->data['Habitacione']['id']           = $this->request->data['Reserindividuale']['habitacione_id'];
                // $this->request->data['Habitacione']['habistatu_id'] = 2;
                //if ($this->Habitacione->save($this->request->data)) {
                $id   =  $this->Reserindividuale->id;
                $stop = 0;
                if(!empty($this->request->data["Reserindividuale"]["Nombre"])){
                    foreach($this->request->data["Reserindividuale"]["Nombre"] as $nombre){
                        $this->request->data["Reserindividualeextra"]["reserindividuale_id"] = $id;
                        $this->request->data["Reserindividualeextra"]["nombres"] = $nombre;
                        $this->Reserindividualeextra->create();
                        if ($this->Reserindividualeextra->save($this->request->data)){

                        }else{
                            $stop = 1;
                        }
                    }
                }
                if($stop==0){
                    $this->Reserindividuale->commit();
                    $empresas = $this->Empresa->find('all');
                    $clientes = $this->Cliente->find('all', array('conditions'=>array('Cliente.id'=>$this->request->data['Reserindividuale']['cliente_id'])));
                    if(isset($empresas[0]['Empresa']['correo_smtp'])){
                        //EMAIL AQUI
                        App::import('Vendor', 'PHPMailer', array('file' => 'phpmailer/class.phpmailer.php'));
                        $mail_r = new PHPMailer();
                        $mail_r->IsHTML(true);
                        $mail_r->IsSMTP();
                        $mail_r->SMTPAuth   = true;                  // enable SMTP authentication
                        $mail_r->SMTPSecure = "ssl";                 // sets the prefix to the servier
                        $mail_r->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
                        $mail_r->Port       = 465;                   // set the SMTP port for the GMAIL server
                        $mail_r->Username   = $empresas[0]['Empresa']['correo_smtp'];  // GMAIL username
                        $mail_r->Password   = $empresas[0]['Empresa']['clave_smtp'];   // GMAIL password
                        $mail_r->From       = $empresas[0]['Empresa']['correo_smtp'];
                        $mail_r->FromName   = $empresas[0]['Empresa']['razon_social'];
                        $mail_r->AddAddress($clientes[0]['Cliente']['correo'], $clientes[0]['Cliente']['nombre_completo']);
                        $mail_r->Subject  = "Confirmacion de la reserva";
                        $email_a_revista  = "<table class='shop-item-selections'>
					      <tr>
					          <td width='100%'>
                                  Saludos Estimado ".$clientes[0]['Cliente']['nombre_completo']." 
                                  <br>  
                                  El siguiente correo es un enlace de confirmación por el registro de su reserva  en ".$empresas[0]['Empresa']['razon_social']."
                                  con fecha de entrada ".$this->request->data['Reserindividuale']['fecha_entrada']." y fecha de salida ".$this->request->data['Reserindividuale']['fecha_salida']."
                                  <br>
                                  Gracias por Preferirnos!!!               
					          </td>
					        </tr>
					      </table>";
                        $mail_r->MsgHTML($email_a_revista);
                        $mail_r->Send();
                    }

                    $auditoria['Auditoria']['user_id']             = $this->Session->read('USUARIO_ID');
                    $auditoria['Auditoria']['habitacione_id']      = $this->request->data['Reserindividuale']['habitacione_id'];
                    $auditoria['Auditoria']['reserindividuale_id'] = $last_id;
                    $auditoria['Auditoria']['cliente_id']          = $this->request->data['Reserindividuale']['cliente_id'];
                    $auditoria['Auditoria']['factura_id']          = 0;
                    $auditoria['Auditoria']['hora']                = DboSource::expression('NOW()');
                    $auditoria['Auditoria']['motivo']              = 'N/A';
                    $auditoria['Auditoria']['tipo_operacion']      = 'NUEVA RESERVA';
                    $auditoria['Auditoria']['especifico']          = 'RESERVA CREADA';
                    $auditoria['Auditoria']['fecha_entrada']       = $this->request->data['Reserindividuale']['fecha_entrada'];
                    $auditoria['Auditoria']['fecha_salida']        = $this->request->data['Reserindividuale']['fecha_salida'];

                    $auditoria['Auditoria']['num_habitacion']      = $this->requestAction('habitaciones/get_numero/'.$this->request->data['Reserindividuale']['habitacione_id']);
                    $auditoria['Auditoria']['capacidad']           = $this->requestAction('habitaciones/get_capacidad/'.$this->request->data['Reserindividuale']['habitacione_id']);
                    $auditoria['Auditoria']['detalle_reserva']     = $this->request->data['Reserindividuale']['detallereservas'];
                    $auditoria['Auditoria']['costo_total']         = $this->request->data['Reserindividuale']['total'];
                    $auditoria['Auditoria']['forma_pago']          = '';
                    $auditoria['Auditoria']['num_factura']         = 0;

                    $this->loadModel('Auditoria');
                    $this->Auditoria->save($auditoria);

                    $this->Flash->success(__('Registro Guardado.'));
                    return $this->redirect('/Reservas/reservas/');
                }else{
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                }
                /*}else{
                	$this->Reserindividuale->rollback();
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                }*/
            } else {
                $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
            }
        }
        $tipoclientes = $this->Reserindividuale->Tipocliente->find('list');
        $tipoclientesubs = $this->Reserindividuale->Tipoclientesub->find('list');
        $clientes = $this->Reserindividuale->Cliente->find('list');
        $tipohabitaciones = $this->Reserindividuale->Tipohabitacione->find('list');
        $habitaciones = $this->Reserindividuale->Habitacione->find('list');
        $reserstatusindividuales = $this->Reserindividuale->Reserstatusindividuale->find('list');
        $resermultiples = $this->Reserindividuale->Resermultiple->find('list');
        $pais = $this->Pai->find('list');
        $this->set(compact('pais','tipoclientes', 'tipoclientesubs', 'clientes', 'tipohabitaciones', 'habitaciones', 'reserstatusindividuales', 'resermultiples'));
    }



    /**
     * add_m method
     *
     * @return void
     */
    public function add_m($id1=null, $id2=null) {
        if ($this->request->is('post')) {
            $this->Reserindividuale->create();
            $this->Reserindividuale->begin();
            $this->request->data['Reserindividuale']['id'] = 4;
            if ($this->Reserindividuale->save($this->request->data)) {
                $last_id = $this->getLastInsertID();
                // $this->request->data['Habitacione']['id']           = $this->request->data['Reserindividuale']['habitacione_id'];
                // $this->request->data['Habitacione']['habistatu_id'] = 2;
                //if ($this->Habitacione->save($this->request->data)) {
                $id   =  $this->Reserindividuale->id;
                $stop = 0;
                if(!empty($this->request->data["Reserindividuale"]["Nombre"])){
                    foreach($this->request->data["Reserindividuale"]["Nombre"] as $nombre){
                        $this->request->data["Reserindividualeextra"]["reserindividuale_id"] = $id;
                        $this->request->data["Reserindividualeextra"]["nombres"] = $nombre;
                        $this->Reserindividualeextra->create();
                        if ($this->Reserindividualeextra->save($this->request->data)){

                        }else{
                            $stop = 1;
                        }
                    }
                }
                if($stop==0){
                    $this->Reserindividuale->commit();
                    $empresas = $this->Empresa->find('all');
                    $clientes = $this->Cliente->find('all', array('conditions'=>array('Cliente.id'=>$this->request->data['Reserindividuale']['cliente_id'])));
                    if(isset($empresas[0]['Empresa']['correo_smtp'])){
                        //EMAIL AQUI
                        App::import('Vendor', 'PHPMailer', array('file' => 'phpmailer/class.phpmailer.php'));
                        $mail_r = new PHPMailer();
                        $mail_r->IsHTML(true);
                        $mail_r->IsSMTP();
                        $mail_r->SMTPAuth   = true;                  // enable SMTP authentication
                        $mail_r->SMTPSecure = "ssl";                 // sets the prefix to the servier
                        $mail_r->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
                        $mail_r->Port       = 465;                   // set the SMTP port for the GMAIL server
                        $mail_r->Username   = $empresas[0]['Empresa']['correo_smtp'];  // GMAIL username
                        $mail_r->Password   = $empresas[0]['Empresa']['clave_smtp'];   // GMAIL password
                        $mail_r->From       = $empresas[0]['Empresa']['correo_smtp'];
                        $mail_r->FromName   = $empresas[0]['Empresa']['razon_social'];
                        $mail_r->AddAddress($clientes[0]['Cliente']['correo'], $clientes[0]['Cliente']['nombre_completo']);
                        $mail_r->Subject  = "Confirmacion de la reserva";
                        $email_a_revista  = "<table class='shop-item-selections'>
					      <tr>
					          <td width='100%'>
                                  Saludos Estimado ".$clientes[0]['Cliente']['nombre_completo']." 
                                  <br>  
                                  El siguiente correo es un enlace de confirmación por el registro de su reserva  en ".$empresas[0]['Empresa']['razon_social']."
                                  con fecha de entrada ".$this->request->data['Reserindividuale']['fecha_entrada']." y fecha de salida ".$this->request->data['Reserindividuale']['fecha_salida']."
                                  <br>
                                  Gracias por Preferirnos!!!               
					          </td>
					        </tr>
					      </table>";
                        $mail_r->MsgHTML($email_a_revista);
                        $mail_r->Send();
                    }
                    $auditoria['Auditoria']['user_id']             = $this->Session->read('USUARIO_ID');
                    $auditoria['Auditoria']['habitacione_id']      = $this->request->data['Reserindividuale']['habitacione_id'];
                    $auditoria['Auditoria']['reserindividuale_id'] = $last_id;
                    $auditoria['Auditoria']['cliente_id']          = $this->request->data['Reserindividuale']['cliente_id'];
                    $auditoria['Auditoria']['factura_id']          = 0;
                    $auditoria['Auditoria']['hora']                = DboSource::expression('NOW()');
                    $auditoria['Auditoria']['motivo']              = 'N/A';
                    $auditoria['Auditoria']['tipo_operacion']      = 'NUEVA RESERVA';
                    $auditoria['Auditoria']['especifico']          = 'RESERVA CREADA';
                    $auditoria['Auditoria']['fecha_entrada']       = $this->request->data['Reserindividuale']['fecha_entrada'];
                    $auditoria['Auditoria']['fecha_salida']        = $this->request->data['Reserindividuale']['fecha_salida'];

                    $auditoria['Auditoria']['num_habitacion']      = $this->requestAction('habitaciones/get_numero/'.$this->request->data['Reserindividuale']['habitacione_id']);
                    $auditoria['Auditoria']['capacidad']           = $this->requestAction('habitaciones/get_capacidad/'.$this->request->data['Reserindividuale']['habitacione_id']);
                    $auditoria['Auditoria']['detalle_reserva']     = $this->request->data['Reserindividuale']['detallereserva'];
                    $auditoria['Auditoria']['costo_total']         = $this->request->data['Reserindividuale']['total'];
                    $auditoria['Auditoria']['forma_pago']          = '';
                    $auditoria['Auditoria']['num_factura']         = 0;

                    $this->loadModel('Auditoria');
                    $this->Auditoria->save($auditoria);

                    $this->Flash->success(__('Registro Guardado.'));
                    return $this->redirect(array('action' => 'index'));
                }else{
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                }
                /*}else{
                	$this->Reserindividuale->rollback();
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                }*/
            } else {
                $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
            }
        }


        $resermultiples2         = $this->Resermultiple->find('all', array('conditions'=>array('Resermultiple.id'=>$id1)));


        $tipohabitacion  = $this->Habitacione->find('all', array('conditions'=>array('Habitacione.id'=>$id2)));

        $tipohabitacion = $tipohabitacion[0]['Habitacione']['tipohabitacione_id'];





        $tipoclientes            = $this->Reserindividuale->Tipocliente->find('list');
        $tipoclientesubs         = $this->Reserindividuale->Tipoclientesub->find('list');
        $clientes                = $this->Reserindividuale->Cliente->find('list');

        $tipohabitaciones        = $this->Reserindividuale->Tipohabitacione->find('list', array('conditions'=>array('Tipohabitacione.id'=>$tipohabitacion)));


        $habitaciones            = $this->Reserindividuale->Habitacione->find('list', array('conditions'=>array('Habitacione.id'=>$id2)));



        $reserstatusindividuales = $this->Reserindividuale->Reserstatusindividuale->find('list');
        $resermultiples          = $this->Reserindividuale->Resermultiple->find('list');
        $pais = $this->Pai->find('list');
        $this->set('resermultiple_id', $id1);
        $this->set(compact('pais','resermultiples2','tipoclientes', 'tipoclientesubs', 'clientes', 'tipohabitaciones', 'habitaciones', 'reserstatusindividuales', 'resermultiples'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Reserindividuale->exists($id)) {
            throw new NotFoundException(__('Invalid reserindividuale'));
        }
        if ($this->request->is(array('post', 'put'))) {

            $this->request->data['Reserindividuale']['tipohabitaciones'] = json_encode($this->request->data['Reserindividuale']['tipohabitaciones']);

            $this->request->data['Reserindividuale']['fecha_entrada'] = !empty($this->request->data['Reserindividuale']['fecha_entrada1']) ? formatfecha($this->request->data['Reserindividuale']['fecha_entrada1']) : '';
            $this->request->data['Reserindividuale']['fecha_salida']  = !empty($this->request->data['Reserindividuale']['fecha_salida1']) ? formatfecha($this->request->data['Reserindividuale']['fecha_salida1']) : '';


            if ($this->Reserindividuale->save($this->request->data)) {

                $this->Reserindividualeextra->deleteAll(array('Reserindividualeextra.reserindividuale_id'=>$id));
                $stop = 0;
                if(!empty($this->request->data["Reserindividuale"]["Nombre"])){
                    foreach($this->request->data["Reserindividuale"]["Nombre"] as $nombre){
                        $this->request->data["Reserindividualeextra"]["reserindividuale_id"] = $id;
                        $this->request->data["Reserindividualeextra"]["nombres"] = $nombre;
                        $this->Reserindividualeextra->create();
                        if ($this->Reserindividualeextra->save($this->request->data)){

                        }else{
                            $stop = 1;
                        }
                    }
                }
                if($stop==0){
                    $this->Flash->success(__('Registro Guardado.'));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                }
            } else {
                $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
            }
        } else {
            $options = array('conditions' => array('Reserindividuale.' . $this->Reserindividuale->primaryKey => $id));
            $this->request->data = $this->Reserindividuale->find('first', $options);
            $this->request->data['Reserindividuale']['fecha_entrada1'] = formatdmy($this->request->data['Reserindividuale']['fecha_entrada']);
            $this->request->data['Reserindividuale']['fecha_salida1'] = formatdmy($this->request->data['Reserindividuale']['fecha_salida']);
            $this->set('data', $this->request->data);
        }
        $tipoclientes = $this->Reserindividuale->Tipocliente->find('list');
        $tipoclientesubs = $this->Reserindividuale->Tipoclientesub->find('list', array('conditions'=>array('Tipoclientesub.tipocliente_id'=>$this->request->data['Reserindividuale']['tipocliente_id'], 'Tipoclientesub.id'=>$this->request->data['Reserindividuale']['tipoclientesub_id'])));
        $clientes = $this->Reserindividuale->Cliente->find('list', array('conditions'=>array('Cliente.id'=>$this->request->data['Reserindividuale']['cliente_id'])));
        $tipohabitaciones = $this->Reserindividuale->Tipohabitacione->find('list');
        $habitaciones = $this->Reserindividuale->Habitacione->find('all', array('conditions'=>array('Habitacione.tipohabitacione_id'=>$this->request->data['Reserindividuale']['tipohabitacione_id'], 'Habitacione.id'=>$this->request->data['Reserindividuale']['habitacione_id'])));
        $reserstatusindividuales = $this->Reserindividuale->Reserstatusindividuale->find('list');
        $resermultiples = $this->Reserindividuale->Resermultiple->find('list');
        $this->set(compact('tipoclientes', 'tipoclientesubs', 'clientes', 'tipohabitaciones', 'habitaciones', 'reserstatusindividuales', 'resermultiples'));
    }











    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit_3($id = null) {

        $this->set('id',$id);

        if (!$this->Reserindividuale->exists($id)) {
            throw new NotFoundException(__('Invalid reserindividuale'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Reserindividuale->save($this->request->data)) {
                $this->Reserindividualeextra->deleteAll(array('Reserindividualeextra.reserindividuale_id'=>$id));
                $stop = 0;
                if(!empty($this->request->data["Reserindividuale"]["Nombre"])){
                    foreach($this->request->data["Reserindividuale"]["Nombre"] as $nombre){
                        $this->request->data["Reserindividualeextra"]["reserindividuale_id"] = $id;
                        $this->request->data["Reserindividualeextra"]["nombres"] = $nombre;
                        $this->Reserindividualeextra->create();
                        if ($this->Reserindividualeextra->save($this->request->data)){

                        }else{
                            $stop = 1;
                        }
                    }
                }
                if($stop==0){

                    /*$auditoria['Auditoria']['user_id']             = $this->Session->read('USUARIO_ID');
                    $auditoria['Auditoria']['habitacione_id']      = $this->request->data['Reserindividuale']['habitacione_id'];
                    $auditoria['Auditoria']['reserindividuale_id'] = $id;
                    $auditoria['Auditoria']['cliente_id']          = $this->request->data['Reserindividuale']['cliente_id'];
                    $auditoria['Auditoria']['factura_id']          = 0;
                    $auditoria['Auditoria']['hora']                = DboSource::expression('NOW()');
                    $auditoria['Auditoria']['motivo']              = 'N/A';
                    $auditoria['Auditoria']['tipo_operacion']      = 'EDICION DE RESERVA';
                    $auditoria['Auditoria']['especifico']          = 'EDITADA';

                    $this->loadModel('Auditoria');
                    $this->Auditoria->save($auditoria);*/

                    $this->Flash->success(__('Registro Guardado.'));
                    return $this->redirect('/Reserindivistatuses/add_2/'.$id);
                } else {
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                }
            } else {
                $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
            }
        } else {
            $options = array('conditions' => array('Reserindividuale.' . $this->Reserindividuale->primaryKey => $id));
            $this->request->data = $this->Reserindividuale->find('first', $options);
            $this->set('data', $this->request->data);
        }
        $tipoclientes = $this->Reserindividuale->Tipocliente->find('all', array('conditions'=>array('Tipocliente.id'=>$this->request->data['Reserindividuale']['tipocliente_id'])));
        $tipoclientesubs = $this->Reserindividuale->Tipoclientesub->find('all', array('conditions'=>array('Tipoclientesub.tipocliente_id'=>$this->request->data['Reserindividuale']['tipocliente_id'], 'Tipoclientesub.id'=>$this->request->data['Reserindividuale']['tipoclientesub_id'])));
        $clientes = $this->Reserindividuale->Cliente->find('all', array('conditions'=>array('Cliente.id'=>$this->request->data['Reserindividuale']['cliente_id'])));
        $tipohabitaciones = $this->Reserindividuale->Tipohabitacione->find('list');
        $habitaciones = $this->Reserindividuale->Habitacione->find('all', array('conditions'=>array('Habitacione.tipohabitacione_id'=>$this->request->data['Reserindividuale']['tipohabitacione_id'], 'Habitacione.id'=>$this->request->data['Reserindividuale']['habitacione_id'])));
        $reserstatusindividuales = $this->Reserindividuale->Reserstatusindividuale->find('list');
        $resermultiples = $this->Reserindividuale->Resermultiple->find('list');
        $this->set(compact('tipoclientes', 'tipoclientesubs', 'clientes', 'tipohabitaciones', 'habitaciones', 'reserstatusindividuales', 'resermultiples'));
    }






    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit_4($id = null) {

        $this->set('id',$id);

        if (!$this->Reserindividuale->exists($id)) {
            throw new NotFoundException(__('Invalid reserindividuale'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Reserindividuale->save($this->request->data)) {
                $this->Reserindividualeextra->deleteAll(array('Reserindividualeextra.reserindividuale_id'=>$id));
                $stop = 0;
                if(!empty($this->request->data["Reserindividuale"]["Nombre"])){
                    foreach($this->request->data["Reserindividuale"]["Nombre"] as $nombre){
                        $this->request->data["Reserindividualeextra"]["reserindividuale_id"] = $id;
                        $this->request->data["Reserindividualeextra"]["nombres"] = $nombre;
                        $this->Reserindividualeextra->create();
                        if ($this->Reserindividualeextra->save($this->request->data)){

                        }else{
                            $stop = 1;
                        }
                    }
                }
                if($stop==0){

                    /*$auditoria['Auditoria']['user_id']             = $this->Session->read('USUARIO_ID');
                    $auditoria['Auditoria']['habitacione_id']      = $this->request->data['Reserindividuale']['habitacione_id'];
                    $auditoria['Auditoria']['reserindividuale_id'] = $id;
                    $auditoria['Auditoria']['cliente_id']          = $this->request->data['Reserindividuale']['cliente_id'];
                    $auditoria['Auditoria']['factura_id']          = 0;
                    $auditoria['Auditoria']['hora']                = DboSource::expression('NOW()');
                    $auditoria['Auditoria']['motivo']              = 'N/A';
                    $auditoria['Auditoria']['tipo_operacion']      = 'EDICION DE RESERVA';
                    $auditoria['Auditoria']['especifico']          = 'EDITADA';

                    $this->loadModel('Auditoria');
                    $this->Auditoria->save($auditoria);*/

                    $this->Flash->success(__('Registro Guardado.'));
                    return $this->redirect('/Reserindivistatuses/add_2/'.$id);
                } else {
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                }
            } else {
                $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
            }
        } else {
            $options = array('conditions' => array('Reserindividuale.' . $this->Reserindividuale->primaryKey => $id));
            $this->request->data = $this->Reserindividuale->find('first', $options);
            $this->set('data', $this->request->data);
        }
        $tipoclientes = $this->Reserindividuale->Tipocliente->find('all', array('conditions'=>array('Tipocliente.id'=>$this->request->data['Reserindividuale']['tipocliente_id'])));
        $tipoclientesubs = $this->Reserindividuale->Tipoclientesub->find('all', array('conditions'=>array('Tipoclientesub.tipocliente_id'=>$this->request->data['Reserindividuale']['tipocliente_id'], 'Tipoclientesub.id'=>$this->request->data['Reserindividuale']['tipoclientesub_id'])));
        $clientes = $this->Reserindividuale->Cliente->find('all', array('conditions'=>array('Cliente.id'=>$this->request->data['Reserindividuale']['cliente_id'])));
        $tipohabitaciones = $this->Reserindividuale->Tipohabitacione->find('list');
        $habitaciones = $this->Reserindividuale->Habitacione->find('all', array('conditions'=>array('Habitacione.tipohabitacione_id'=>$this->request->data['Reserindividuale']['tipohabitacione_id'], 'Habitacione.id'=>$this->request->data['Reserindividuale']['habitacione_id'])));
        $reserstatusindividuales = $this->Reserindividuale->Reserstatusindividuale->find('list');
        $resermultiples = $this->Reserindividuale->Resermultiple->find('list');
        $this->set(compact('tipoclientes', 'tipoclientesubs', 'clientes', 'tipohabitaciones', 'habitaciones', 'reserstatusindividuales', 'resermultiples'));
    }



    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit_2($id = null) {
        if (!$this->Reserindividuale->exists($id)) {
            throw new NotFoundException(__('Invalid reserindividuale'));
        }
        if ($this->request->is(array('post', 'put'))) {

            $this->request->data['Reserindividuale']['fecha_entrada'] = !empty($this->request->data['Reserindividuale']['fecha_entrada1']) ? formatfecha($this->request->data['Reserindividuale']['fecha_entrada1']) : '';
            $this->request->data['Reserindividuale']['fecha_salida'] = !empty($this->request->data['Reserindividuale']['fecha_salida1']) ? formatfecha($this->request->data['Reserindividuale']['fecha_salida1']) : '';

            if ($this->Reserindividuale->save($this->request->data)) {
                $this->Reserindividualeextra->deleteAll(array('Reserindividualeextra.reserindividuale_id'=>$id));
                $stop = 0;
                if(!empty($this->request->data["Reserindividuale"]["Nombre"])){
                    foreach($this->request->data["Reserindividuale"]["Nombre"] as $nombre){
                        $this->request->data["Reserindividualeextra"]["reserindividuale_id"] = $id;
                        $this->request->data["Reserindividualeextra"]["nombres"] = $nombre;
                        $this->Reserindividualeextra->create();
                        if ($this->Reserindividualeextra->save($this->request->data)){

                        }else{
                            $stop = 1;
                        }
                    }
                }
                if($stop==0){

                    /*$auditoria['Auditoria']['user_id']             = $this->Session->read('USUARIO_ID');
                    $auditoria['Auditoria']['habitacione_id']      = $this->request->data['Reserindividuale']['habitacione_id'];
                    $auditoria['Auditoria']['reserindividuale_id'] = $id;
                    $auditoria['Auditoria']['cliente_id']          = $this->request->data['Reserindividuale']['cliente_id'];
                    $auditoria['Auditoria']['factura_id']          = 0;
                    $auditoria['Auditoria']['hora']                = DboSource::expression('NOW()');
                    $auditoria['Auditoria']['motivo']              = 'N/A';
                    $auditoria['Auditoria']['tipo_operacion']      = 'EDICION DE RESERVA';
                    $auditoria['Auditoria']['especifico']          = 'EDITADA';

                    $this->loadModel('Auditoria');
                    $this->Auditoria->save($auditoria);*/

                    $this->Flash->success(__('Registro Guardado.'));
                    $this->redirect('/reservas/reservas');
                } else {
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                }
            } else {
                $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
            }
        } else {
            $options = array('conditions' => array('Reserindividuale.' . $this->Reserindividuale->primaryKey => $id));
            $this->request->data = $this->Reserindividuale->find('first', $options);
            $this->request->data['Reserindividuale']['fecha_entrada1'] = formatdmy($this->request->data['Reserindividuale']['fecha_entrada']);
            $this->request->data['Reserindividuale']['fecha_salida1'] = formatdmy($this->request->data['Reserindividuale']['fecha_salida']);
            $this->set('data', $this->request->data);
        }
        $tipoclientes = $this->Reserindividuale->Tipocliente->find('list');
        $tipoclientesubs = $this->Reserindividuale->Tipoclientesub->find('all', array('conditions'=>array('Tipoclientesub.tipocliente_id'=>$this->request->data['Reserindividuale']['tipocliente_id'], 'Tipoclientesub.id'=>$this->request->data['Reserindividuale']['tipoclientesub_id'])));
        $clientes = $this->Reserindividuale->Cliente->find('all', array('conditions'=>array('Cliente.id'=>$this->request->data['Reserindividuale']['cliente_id'])));
        $tipohabitaciones = $this->Reserindividuale->Tipohabitacione->find('list');
        $habitaciones = $this->Reserindividuale->Habitacione->find('all', array('conditions'=>array('Habitacione.tipohabitacione_id'=>$this->request->data['Reserindividuale']['tipohabitacione_id'], 'Habitacione.id'=>$this->request->data['Reserindividuale']['habitacione_id'])));
        $reserstatusindividuales = $this->Reserindividuale->Reserstatusindividuale->find('list');
        $resermultiples = $this->Reserindividuale->Resermultiple->find('list');
        $this->set(compact('tipoclientes', 'tipoclientesubs', 'clientes', 'tipohabitaciones', 'habitaciones', 'reserstatusindividuales', 'resermultiples'));
    }





    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null, $motivo = null) {

        $this->Reserindividuale->id = $id;

        if (!$this->Reserindividuale->exists()) {
            throw new NotFoundException(__('Invalid reserindividuale'));
        }

        $res = $this->Reserindividuale->find('first', ['conditions' => ['Reserindividuale.id' => $id] ]);

        $auditoria['Auditoria']['user_id']             = $this->Session->read('USUARIO_ID');
        $auditoria['Auditoria']['habitacione_id']      = $res['Reserindividuale']['habitacione_id'];
        $auditoria['Auditoria']['reserindividuale_id'] = $res['Reserindividuale']['id'];
        $auditoria['Auditoria']['resermultiple_id']    = 0;
        $auditoria['Auditoria']['cliente_id']          = $res['Reserindividuale']['cliente_id'];
        $auditoria['Auditoria']['factura_id']          = !empty($res['Factura']['id']) ? $res['Factura']['id'] : 0;
        $auditoria['Auditoria']['hora']                = DboSource::expression('NOW()');
        $auditoria['Auditoria']['motivo']              = $motivo;
        $auditoria['Auditoria']['tipo_operacion']      = 'ELIMINADO DE RESERVA';
        $auditoria['Auditoria']['especifico']          = 'ELIMINADO';
        $auditoria['Auditoria']['fecha_entrada']       = $res['Reserindividuale']['fecha_entrada'];
        $auditoria['Auditoria']['fecha_salida']        = $res['Reserindividuale']['fecha_salida'];

        $auditoria['Auditoria']['num_habitacion']      = $this->requestAction('habitaciones/get_numero/'.$res['Reserindividuale']['habitacione_id']);
        $auditoria['Auditoria']['capacidad']           = $this->requestAction('habitaciones/get_capacidad/'.$res['Reserindividuale']['habitacione_id']);
        $auditoria['Auditoria']['detalle_reserva']     = !empty($res['Reserindividuale']['detallereserva']) ? $res['Reserindividuale']['detallereserva'] : '';
        $auditoria['Auditoria']['costo_total']         = $res['Reserindividuale']['total'];
        $auditoria['Auditoria']['forma_pago']          = '-';
        $auditoria['Auditoria']['num_factura']         = ($res['Reserindividuale']['factura_id'] != 0) ? $res['Factura']['numero'] : 0;

        $this->loadModel('Auditoria');
        $this->Auditoria->save($auditoria);


        $rol = $this->Session->read('ROL');
        if($rol!=1){
            $this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
        }else{

            $this->request->allowMethod('post', 'delete', 'get');
            if ($this->Reserindividuale->delete()) {
                $this->Flash->success(__('El Registro fue eliminado.'));
            } else {
                $this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
            }
            return $this->redirect(array('action' => 'index'));
        }
    }


    /**
     * habitacion method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function habitacion($id = null) {
        $this->layout = 'ajax';
        $habitaciones = $this->Reserindividuale->Habitacione->find('list', array('fields'=>array('Habitacione.id', 'Habitacione.numhabitacion'), 'conditions'=>array('Habitacione.tipohabitacione_id'=>$id)));
        $this->set(compact('habitaciones'));
        $this->set('id', $id);

    }


    /**
     * fecha_entrada method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function fecha_entrada($id = null, $id2 = null) {
        $this->layout = 'ajax';
        $this->set('id', $id);
    }


    /**
     * precioxdia method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function precioxdia($id = null, $id2 = null) {
        $this->layout       = 'ajax';
        $conuntemp          = $this->Tipotemporada->find('count', array('conditions'=>array('Tipotemporada.fechadesde <='=>$id2, 'Tipotemporada.fechahasta >='=>$id2)));
        $tipotemporadas     = $this->Tipotemporada->find('all', array('conditions'=>array('Tipotemporada.fechadesde <='=>$id2, 'Tipotemporada.fechahasta >='=>$id2)));
        $tipohabitacione_id = $id;
        if($conuntemp>1){
            $listo = 0;
            foreach($tipotemporadas as $data){
                if($listo==0){
                    $listo      = $this->Pstemporada->find('count',   array('conditions'=>array('Pstemporada.tipohabitacione_id'=>$tipohabitacione_id, 'Pstemporada.tipotemporada_id'=>$data['Tipotemporada']['id'])));
                    $precios    = $this->Pstemporada->find('all',   array('conditions'=>array('Pstemporada.tipohabitacione_id'=>$tipohabitacione_id, 'Pstemporada.tipotemporada_id'=>$data['Tipotemporada']['id'])));
                }
            }
        }else{
            $tipotemporada_id   = isset($tipotemporadas[0]['Tipotemporada']['id'])?$tipotemporadas[0]['Tipotemporada']['id']:0;
            $precios            = $this->Pstemporada->find('all',   array('conditions'=>array('Pstemporada.tipohabitacione_id'=>$tipohabitacione_id, 'Pstemporada.tipotemporada_id'=>$tipotemporada_id)));
        }

        $this->set('value', isset($precios[0]['Pstemporada']['precio'])?$precios[0]['Pstemporada']['precio']:0);
    }

    /**
     * precioxdia method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function preciopordia($fecha_entrada = null, $fecha_salida = null,  $numhabitacion = null) {
        $this->layout = 'ajax';

        // Caso 1
        // Cuando la fecha de inicio esta dentro de una temporada y su fecha de salida esta fuera
        $caso1 = $this->Tipotemporada->find('all',
            array('conditions'=>array('Tipotemporada.fechadesde <= ' => $fecha_entrada,
                'Tipotemporada.fechahasta >= ' => $fecha_entrada),'limit'=>1));
        //var_dump($caso1);

        //Dentro
        $caso2 = $this->Tipotemporada->find('all',
            array('conditions'=>array('Tipotemporada.fechadesde <= ' => $fecha_salida,
                'Tipotemporada.fechahasta >= ' => $fecha_salida),'limit'=>1));
        //var_dump($caso2);

        $caso3 = $this->Tipotemporada->find('all',
            array('conditions'=>array('Tipotemporada.fechadesde = ' => $fecha_salida,
                'Tipotemporada.fechahasta = ' => $fecha_salida),'limit'=>1));
        //

        $conuntemp = $this->Tipotemporada->find('count',
            array('conditions'=>array('Tipotemporada.fechadesde >='=>$fecha_entrada,
                'Tipotemporada.fechahasta <='  => $fecha_salida)));
        //pr($todastemporadas);

        $auxfechadeentrada = $fecha_entrada;
        $auxfechasalidacaso2 = $fecha_salida;
        //Determino el tipo de habitacion



        $tipohabitacione_id = $this->Habitacione->find('all',
            array('conditions'=>array('Habitacione.id ' => $numhabitacion)));

        $tipohabitacione_id = !empty($tipohabitacione_id[0]['Habitacione']['tipohabitacione_id']) ? $tipohabitacione_id[0]['Habitacione']['tipohabitacione_id'] : '';


        $costototal = 0;
        $total = 0;

        $table	= "<table class='table'>";
        $table .= "<thead>";
        $table .= "<tr><th>Tipo Temporada</th>";
        $table .= "<th>Dias</th>";
        $table .= "<th>Costo</th><tr></thead>";
        $table .= "<tbody>";



        if ($caso1[0]['Tipotemporada']['id'] == 12 && $caso2[0]['Tipotemporada']['id'] == 12 ) {


            $todastemporadas = $this->Tipotemporada->find('all',
                array('conditions'=>array('Tipotemporada.fechadesde >='=>$fecha_entrada,
                    'Tipotemporada.fechahasta <='=>$fecha_salida) ) );

        }
        //Evitar que el caso se repita para temporada baja
        if ($caso1[0]['Tipotemporada']['id'] != 12 ) {
            //En este caso, la fecha inicial de la reserva cayo sobre una fecha festiva
            //tomare el ultimo dia de esa fecha festiva y le sumo un dia
            //lo tomare como el primer dia de la temporada normal
            $fecha = $caso1[0]['Tipotemporada']['fechahasta'];
            $nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
            $fecha_entrada = date ( 'Y-m-j' , $nuevafecha );


            //Calculo cuantas fechas hay feriados
            $fecha1 = explode('-', $auxfechadeentrada);
            $fecha2 = explode('-', $fecha);

            $dia1 = $fecha1[2];
            $mes1 = $fecha1[1];
            $anho1 = $fecha1[0];


            $dia2 = $fecha2[2];
            $mes2 = $fecha2[1];
            $anho2 = $fecha2[0];

            $timestamp1 = mktime(0,0,0,$mes1,$dia1,$anho1);

            $timestamp2 = mktime(0,0,0,$mes2,$dia2,$anho2);


            $segundos_diferencia =  ($timestamp1 - $timestamp2);

            $segundos_diferencia = abs($segundos_diferencia);


            $cantidadcaso1 =  ($segundos_diferencia / (60 * 60 * 24));

            $idtemporadacaso1 = $caso1[0]['Tipotemporada']['id'];

            $preciocaso1 = $this->Pstemporada->find('all',
                array('conditions'=>array('Pstemporada.tipotemporada_id'=>$idtemporadacaso1,
                    'Pstemporada.tipohabitacione_id'=>$tipohabitacione_id)));



            $table .= '<tr><td>'.$caso1[0]['Tipotemporada']['denominacion'].'</td>';
            $table .= '<td>'.$cantidadcaso1.'</td>';
            @$table .= '<td>'.($cantidadcaso1 * $preciocaso1[0]['Pstemporada']['precio']).'</td>';



            @$total += $cantidadcaso1 * $preciocaso1[0]['Pstemporada']['precio'];

            $todastemporadas = $this->Tipotemporada->find('all',
                array('conditions'=>array('Tipotemporada.fechadesde >='=>$fecha_entrada,
                    'Tipotemporada.fechahasta <='=>$fecha_salida) ) );

            $auxfechadeentrada = $fecha_entrada;
        }


        if ($caso2[0]['Tipotemporada']['id'] != 12 ) {
            //En este caso, la fecha inicial de la reserva cayo sobre una fecha festiva
            //tomare el ultimo dia de esa fecha festiva y le sumo un dia
            //lo tomare como el primer dia de la temporada normal
            $fecha = $caso2[0]['Tipotemporada']['fechadesde'];
            $nuevafecha = strtotime ( '-1 day' , strtotime ( $fecha ) ) ;
            $fecha_salida = date ( 'Y-m-j' , $nuevafecha );
            $fecha_salida = $fecha;




            //Calculo cuantas fechas hay feriados
            $fecha1 = explode('-', $fecha);
            $fecha2 = explode('-', $auxfechasalidacaso2);

            $dia1 = $fecha1[2];
            $mes1 = $fecha1[1];
            $anho1 = $fecha1[0];


            $dia2 = $fecha2[2];
            $mes2 = $fecha2[1];
            $anho2 = $fecha2[0];

            $timestamp1 = mktime(0,0,0,$mes1,$dia1,$anho1);

            $timestamp2 = mktime(0,0,0,$mes2,$dia2,$anho2);


            $segundos_diferencia =  ($timestamp1 - $timestamp2);

            $segundos_diferencia = abs($segundos_diferencia);


            $cantidadcaso2 =  ($segundos_diferencia / (60 * 60 * 24));

            $idtemporadacaso2 = $caso2[0]['Tipotemporada']['id'];

            $preciocaso2 = $this->Pstemporada->find('all',
                array('conditions'=>array('Pstemporada.tipotemporada_id'=>$idtemporadacaso2,
                    'Pstemporada.tipohabitacione_id'=>$tipohabitacione_id)));


            $auxtable = '<tr><td>'.$caso2[0]['Tipotemporada']['denominacion'].'</td>';
            $auxtable .= '<td>'.$cantidadcaso2.'</td>';
            @$auxtable .= '<td>'.($cantidadcaso2 * $preciocaso2[0]['Pstemporada']['precio']).'</td>';

            @$total += $cantidadcaso2 * @$preciocaso2[0]['Pstemporada']['precio'];

            // echo "nuevafecha".$fecha_entrada;

            $todastemporadas = $this->Tipotemporada->find('all',
                array('conditions'=>array('Tipotemporada.fechadesde >='=>$fecha_entrada,
                    'Tipotemporada.fechahasta <= ' => $fecha_salida) ) );

        }




        //		var_dump($todastemporadas);

        foreach ($todastemporadas as $tod) {


            $auxfechasalida = $tod['Tipotemporada']['fechadesde'];
            //echo $auxfechasalida;

            $conuntemp = $this->Tipotemporada->find('all',
                array('conditions'=>array('Tipotemporada.fechadesde <='=>$auxfechadeentrada,
                    'Tipotemporada.fechahasta >='=>$auxfechasalida)));

            //Saco el id de la temporada 1
            $idtemporada1 = $conuntemp[0]['Tipotemporada']['id'];
            $table .= "<tr><td>".$conuntemp[0]['Tipotemporada']['denominacion']."</td>";

            //Determino el costo
            $precio1 = $this->Pstemporada->find('all',
                array('conditions'=>array('Pstemporada.tipotemporada_id'=>$idtemporada1,
                    'Pstemporada.tipohabitacione_id'=>$tipohabitacione_id)));

            $precio1 = $precio1[0]['Pstemporada']['precio'];

            //Determino la cantidad de dias que dura esta fecha
            //echo $auxfechadeentrada.' - '.$auxfechasalida.'<br>';


            $fecha1 = explode('-', $auxfechadeentrada);
            $fecha2 = explode('-', $auxfechasalida);

            $dia1 = $fecha1[2];
            $mes1 = $fecha1[1];
            $anho1 = $fecha1[0];


            $dia2 = $fecha2[2];
            $mes2 = $fecha2[1];
            $anho2 = $fecha2[0];

            $timestamp1 = mktime(0,0,0,$mes1,$dia1,$anho1);

            $timestamp2 = mktime(0,0,0,$mes2,$dia2,$anho2);


            $segundos_diferencia =  ($timestamp1 - $timestamp2);

            $segundos_diferencia = abs($segundos_diferencia);
            $cantidad =  ($segundos_diferencia / (60 * 60 * 24));

            $table .= "<td>".$cantidad."</td>";
            $table .= "<td>".$precio1."</td></tr>";

            //echo "cant ".$cantidad.'<br>';
            $total += $cantidad * $precio1;
            /////////*****************************************************************************
            $auxfechadeentrada = $tod['Tipotemporada']['fechadesde'];
            $auxfechasalida    = $tod['Tipotemporada']['fechahasta'];


            $conuntemp = $this->Tipotemporada->find('all',
                array('conditions'=>array('Tipotemporada.fechadesde >='=>$auxfechadeentrada,
                    'Tipotemporada.fechahasta <='=>$auxfechasalida)));
            //Saco el id de la temporada 2
            $idtemporada2 = $conuntemp[0]['Tipotemporada']['id'];
            $table .= "<tr><td>".$conuntemp[0]['Tipotemporada']['denominacion']."</td>";
            //Determino el costo
            //
            //
            $precio2 = $this->Pstemporada->find('all',
                array('conditions'=>array('Pstemporada.tipotemporada_id'=>$idtemporada2,
                    'Pstemporada.tipohabitacione_id'=>$tipohabitacione_id)));



            $precio2 = $precio2[0]['Pstemporada']['precio'];


            //Determino la cantidad de dias que dura esta fecha
            //echo $auxfechadeentrada.' - '.$auxfechasalida.'<br>';
            $fecha1 = explode('-', $auxfechadeentrada);
            $fecha2 = explode('-', $auxfechasalida);

            $dia1 = $fecha1[2];
            $mes1 = $fecha1[1];
            $anho1 = $fecha1[0];


            $dia2 = $fecha2[2];
            $mes2 = $fecha2[1];
            $anho2 = $fecha2[0];

            $timestamp1 = mktime(0,0,0,$mes1,$dia1,$anho1);

            $timestamp2 = mktime(0,0,0,$mes2,$dia2,$anho2);


            $segundos_diferencia =  ($timestamp1 - $timestamp2);

            $segundos_diferencia = abs($segundos_diferencia);
            $cantidad =  ($segundos_diferencia / (60 * 60 * 24));
            //echo "cant ".$cantidad.'<br>';
            $total += $cantidad * $precio2;
            $table .= "<td>".$cantidad."</td>";
            $table .= "<td>".number_format($precio2,'2',',','.')."</td></tr>";


            $auxfechadeentrada = $tod['Tipotemporada']['fechahasta'];


        }

        //Se calculan los dias restante en temporada normal
        if ($conuntemp > 0) {
            # code...
            $conuntemp = $this->Tipotemporada->find('all',
                array('conditions'=>array('Tipotemporada.fechadesde <='=>$auxfechadeentrada,
                    'Tipotemporada.fechahasta >='=>$fecha_salida)));
            //Saco el id de la temporada 2
            $idtemporada2 = $conuntemp[0]['Tipotemporada']['id'];
            //Determino el costo
            $precio2 = $this->Pstemporada->find('all',
                array('conditions'=>array('Pstemporada.tipotemporada_id'=>$idtemporada2,
                    'Pstemporada.tipohabitacione_id'=>$tipohabitacione_id)));

            $precio2 = $precio2[0]['Pstemporada']['precio'];
            $table .= "<tr><td>".$conuntemp[0]['Tipotemporada']['denominacion']."</td>";
            //Determino la cantidad de dias que dura esta fecha
            //echo $auxfechadeentrada.' - '.$fecha_salida.'<br>';
            $fecha1 = explode('-', $auxfechadeentrada);
            $fecha2 = explode('-', $fecha_salida);

            $dia1 = $fecha1[2];
            $mes1 = $fecha1[1];
            $anho1 = $fecha1[0];


            $dia2 = $fecha2[2];
            $mes2 = $fecha2[1];
            $anho2 = $fecha2[0];

            $timestamp1 = mktime(0,0,0,$mes1,$dia1,$anho1);

            $timestamp2 = mktime(0,0,0,$mes2,$dia2,$anho2);


            $segundos_diferencia =  ($timestamp1 - $timestamp2);

            $segundos_diferencia = abs($segundos_diferencia);
            $cantidad =  ($segundos_diferencia / (60 * 60 * 24));
            //echo "cant ".$cantidad.'<br>';
            $total += $cantidad * $precio2;
            $table .= "<td>".$cantidad."</td>";
            $table .= "<td>".number_format($precio2,'2',',','.')."</td></tr>";

        }


        if ($conuntemp == 0) {

            $conuntemp = $this->Tipotemporada->find('all',
                array('conditions'=>array('Tipotemporada.fechadesde <='=>$fecha_entrada,
                    'Tipotemporada.fechahasta >='=>$fecha_salida)));
            //Saco el id de la temporada 1

            $tipohabitacione_id = $this->Habitacione->find('all',
                array('conditions'=>array('Habitacione.id ' => $numhabitacion)));
            $tipohabitacione_id = $tipohabitacione_id[0]['Habitacione']['tipohabitacione_id'];
            $idtemporada1 = $conuntemp[0]['Tipotemporada']['id'];
            $table .= "<tr><td>".$conuntemp[0]['Tipotemporada']['denominacion']."</td>";



            //Determino el costo
            $precio1 = $this->Pstemporada->find('all',
                array('conditions'=>array('Pstemporada.tipotemporada_id'=>$idtemporada1,
                    'Pstemporada.tipohabitacione_id'=>$tipohabitacione_id)));


            @$precio1 = $precio1[0]['Pstemporada']['precio'];



            //Determino la cantidad de dias que dura esta fecha
            //echo $auxfechadeentrada.' - '.$auxfechasalida.'<br>';


            $fecha1 = explode('-', $fecha_entrada);
            $fecha2 = explode('-', $fecha_salida);

            $dia1 = $fecha1[2];
            $mes1 = $fecha1[1];
            $anho1 = $fecha1[0];


            $dia2 = $fecha2[2];
            $mes2 = $fecha2[1];
            $anho2 = $fecha2[0];

            $timestamp1 = mktime(0,0,0,$mes1,$dia1,$anho1);

            $timestamp2 = mktime(0,0,0,$mes2,$dia2,$anho2);


            $segundos_diferencia =  ($timestamp1 - $timestamp2);

            $segundos_diferencia = abs($segundos_diferencia);
            $cantidad =  ($segundos_diferencia / (60 * 60 * 24));

            $table .= "<td>".$cantidad."</td>";
            $table .= "<td>".number_format($precio1,'2',',','.')."</td></tr>";

            //echo "cant ".$cantidad.'<br>';
            if($cantidad == 0){
                $cantidad = 1;
            }
            $total += $cantidad * $precio1;

        }

        if ($caso2[0]['Tipotemporada']['id'] != 12 ) {
            $table .= $auxtable;
        }

        $table .= '</table>';


        $this->set('value', $total);
        $this->set('table', $table);
    }



    /**
     * sub_cliente method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function sub_cliente($id = null) {
        $this->layout = 'ajax';
        $subclientes = $this->Reserindividuale->Tipoclientesub->find('list', array('fields'=>array('Tipoclientesub.id', 'Tipoclientesub.nombre'), 'conditions'=>array('Tipoclientesub.tipocliente_id'=>$id)));
        $this->set(compact('subclientes'));
        $this->set('id', $id);

    }

    /**
     * cliente method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function cliente($id = null, $id2 = null) {
        $this->layout = 'ajax';
        $clientes = $this->Reserindividuale->Cliente->find('list', array('fields'=>array('Cliente.id', 'Cliente.nombre_completo'), 'conditions'=>array('Cliente.tipocliente_id'=>$id, 'Cliente.tipoclientesub_id'=>$id2)));
        $this->set(compact('clientes'));

    }


    /**
     * cliente method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function cliente2() {
        $this->layout = 'ajax';
        $id = $this->Session->read('LAST_ID_CLIENT');
        $clientes = $this->Reserindividuale->Cliente->find('list', array('conditions'=>array('Cliente.id'=>$id)));
        $this->set(compact('clientes'));

    }

    /**
     * disponibidad method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */

    public function disponibidad($a = null, $b = null) {
        $this->layout = 'ajax';
        $a = $this->request->data['fecha_a'];
        $b = $this->request->data['fecha_b'];
        $c = $this->request->data['tipohabitacione_id'];
        $d = $this->request->data['habitacione_id'];
        $tipotemporadas_e = $this->Reserindividuale->find('count', array('conditions'=>array('Reserindividuale.tipohabitacione_id'=>$c, 'Reserindividuale.habitacione_id'=>$d, 'Reserindividuale.fecha_entrada <='=>$a, 'Reserindividuale.fecha_salida >'=>$a, 'Reserindividuale.reserstatusindividuale_id !='=>5)));
        $tipotemporadas_s = $this->Reserindividuale->find('count', array('conditions'=>array('Reserindividuale.tipohabitacione_id'=>$c, 'Reserindividuale.habitacione_id'=>$d, 'Reserindividuale.fecha_entrada <'=>$b, 'Reserindividuale.fecha_salida >='=>$b, 'Reserindividuale.reserstatusindividuale_id !='=>5)));
        $tipotemporadas_c = $this->Reserindividuale->find('count', array('conditions'=>array('Reserindividuale.tipohabitacione_id'=>$c, 'Reserindividuale.habitacione_id'=>$d, 'Reserindividuale.fecha_entrada >='=>$a, 'Reserindividuale.fecha_salida <='=>$b, 'Reserindividuale.reserstatusindividuale_id !='=>5)));

        if($tipotemporadas_e==0){
            if($tipotemporadas_s==0){
                if($tipotemporadas_c==0){
                    //RESERVA MULTIPLE
                    $tipotemporadas_e_m = $this->Resermulhabitacione->find('count', array('conditions'=>array('Resermulhabitacione.habitacione_id'=>$d, 'Resermultiple.tipohabitacione_id'=>$c, 'Resermultiple.fecha_entrada <='=>$a, 'Resermultiple.fecha_salida >'=>$a, 'Reserindividuale.reserstatusindividuale_id !='=>5)));
                    $tipotemporadas_s_m = $this->Resermulhabitacione->find('count', array('conditions'=>array('Resermulhabitacione.habitacione_id'=>$d, 'Resermultiple.tipohabitacione_id'=>$c, 'Resermultiple.fecha_entrada <'=>$b, 'Resermultiple.fecha_salida >='=>$b, 'Reserindividuale.reserstatusindividuale_id !='=>5)));
                    $tipotemporadas_c_m = $this->Resermulhabitacione->find('count', array('conditions'=>array('Resermulhabitacione.habitacione_id'=>$d, 'Resermultiple.tipohabitacione_id'=>$c, 'Resermultiple.fecha_entrada >='=>$a, 'Resermultiple.fecha_salida <='=>$b, 'Reserindividuale.reserstatusindividuale_id !='=>5)));

                    if($tipotemporadas_e_m==0){
                        if($tipotemporadas_s_m==0){
                            if($tipotemporadas_c_m==0){
                                echo "1";
                            }else{
                                echo "0";
                            }
                        }else{
                            echo "0";
                        }
                    }else{
                        echo "0";
                    }
                    //FIN RESERVA MULTIPLE
                    //echo "1";
                }else{
                    echo "0";
                }
            }else{
                echo "0";
            }
        }else{
            echo "0";
        }

    }












    public function disponibidadhabitaciones($fecha_entrada = null, $fecha_salida = null, $idTipoHabitacion = null) {
        $this->layout = 'ajax';
        $array1[] = '0';
        $array2[] = '0';
        $array3[] = '0';
        $array4[] = '0';

        //RESERVA MULTIPLE
        #SQL CASO 1
        #No traer estatus
        #6
        #5
        #3
        $CASO1 = $this->Reserindividuale->find('all', array('conditions'=>array('Reserindividuale.tipohabitacione_id'=>$idTipoHabitacion, 'Reserindividuale.fecha_entrada <='=>$fecha_entrada, 'Reserindividuale.fecha_salida >='=>$fecha_salida,'or'=>array('Reserindividuale.reserstatusindividuale_id !='=>array(6,5,3)))));
        $array = '';

        foreach ($CASO1 as $key) {
            $array1[] = $key['Habitacione']['numhabitacion'];

        }

        #SQL CASO 2
        $CASO2 = $this->Reserindividuale->find('all', array('conditions'=>array('Reserindividuale.tipohabitacione_id'=>$idTipoHabitacion, 'Reserindividuale.fecha_entrada >='=>$fecha_entrada, 'Reserindividuale.fecha_salida <='=>$fecha_salida,'or'=>array('Reserindividuale.reserstatusindividuale_id !='=>array(6,5,3)))));
        $array = '';

        foreach ($CASO2 as $key) {
            $array2[] = $key['Habitacione']['numhabitacion'];

        }



        #SQL CASO 3
        $CASO3 = $this->Reserindividuale->find('all', array('conditions'=>array('Reserindividuale.tipohabitacione_id'=>$idTipoHabitacion, 'Reserindividuale.fecha_entrada <='=>$fecha_entrada, 'Reserindividuale.fecha_salida >'=>$fecha_entrada, 'Reserindividuale.fecha_salida <='=>$fecha_salida,'or'=>array('Reserindividuale.reserstatusindividuale_id !='=>array(6,5,3)))));
        $array = '';

        foreach ($CASO3 as $key) {
            $array3[] = $key['Habitacione']['numhabitacion'];

        }

        #SQL CASO 4
        $CASO4 = $this->Reserindividuale->find('all', array('conditions'=>array('Reserindividuale.tipohabitacione_id'=>$idTipoHabitacion, 'Reserindividuale.fecha_entrada >='=>$fecha_entrada, 'Reserindividuale.fecha_entrada < '=>$fecha_salida, 'Reserindividuale.fecha_salida >='=>$fecha_salida,'or'=>array('Reserindividuale.reserstatusindividuale_id !='=>array(6,5,3)))));
        $array = '';

        foreach ($CASO4 as $key) {
            $array4[] = $key['Habitacione']['numhabitacion'];

        }




        $array = array_merge($array1, $array2, $array3, $array4);


        $this->set('habitaciones', $this->Habitacione->find('all',array('conditions'=>array('Habitacione.numhabitacion !='=>$array,'Habitacione.tipohabitacione_id '=>$idTipoHabitacion))));


        /**
         * ESTATUS DE HABITACIONES
         */
        $this->set('estatushabitaciones', $this->Habitacione->find('all'));

    }

    public function disponibidadhabitacionesmultiples($fecha_entrada = null, $fecha_salida = null, $idTipoHabitacion = null) {
        $this->layout = 'ajax';
        $array1[] = '0';
        $array2[] = '0';
        $array3[] = '0';
        $array4[] = '0';
        $array5[] = '0';
        $array6[] = '0';
        $array7[] = '0';
        $array8[] = '0';
        $arrayhab[] = '0';



        #SQL CASO 1
        $CASO1 = $this->Reserindividuale->find('all', array('conditions'=>array('Reserindividuale.tipohabitacione_id'=>$idTipoHabitacion, 'Reserindividuale.fecha_entrada <='=>$fecha_entrada, 'Reserindividuale.fecha_salida >='=>$fecha_salida,'or'=>array('Reserindividuale.reserstatusindividuale_id !='=>array(6,5,3)))));
        $array = '';

        foreach ($CASO1 as $key) {
            $array1[] = $key['Habitacione']['numhabitacion'];

        }

        #SQL CASO 2
        $CASO2 = $this->Reserindividuale->find('all', array('conditions'=>array('Reserindividuale.tipohabitacione_id'=>$idTipoHabitacion, 'Reserindividuale.fecha_entrada >='=>$fecha_entrada, 'Reserindividuale.fecha_salida <='=>$fecha_salida,'or'=>array('Reserindividuale.reserstatusindividuale_id !='=>array(6,5,3)))));
        $array = '';

        foreach ($CASO2 as $key) {
            $array2[] = $key['Habitacione']['numhabitacion'];

        }



        #SQL CASO 3
        $CASO3 = $this->Reserindividuale->find('all', array('conditions'=>array('Reserindividuale.tipohabitacione_id'=>$idTipoHabitacion, 'Reserindividuale.fecha_entrada <='=>$fecha_entrada, 'Reserindividuale.fecha_salida >'=>$fecha_entrada, 'Reserindividuale.fecha_salida <='=>$fecha_salida,'or'=>array('Reserindividuale.reserstatusindividuale_id !='=>array(6,5,3)))));
        $array = '';

        foreach ($CASO3 as $key) {
            $array3[] = $key['Habitacione']['numhabitacion'];

        }

        #SQL CASO 4
        $CASO4 = $this->Reserindividuale->find('all', array('conditions'=>array('Reserindividuale.tipohabitacione_id'=>$idTipoHabitacion, 'Reserindividuale.fecha_entrada >='=>$fecha_entrada, 'Reserindividuale.fecha_entrada < '=>$fecha_salida, 'Reserindividuale.fecha_salida >='=>$fecha_salida,'or'=>array('Reserindividuale.reserstatusindividuale_id !='=>array(6,5,3)))));
        $array = '';

        foreach ($CASO4 as $key) {
            $array4[] = $key['Habitacione']['numhabitacion'];

        }





        #SQL CASO 5

        $CASO5 =  $this->Resermultiple->find('all', array('conditions'=>array('Resermultiple.fecha_entrada <='=>$fecha_entrada, 'Resermultiple.fecha_salida >='=>$fecha_salida)));


        foreach ($CASO5 as $key) {
            foreach ($key['Resermulhabitacione'] as $data) {
                $array5[] = $data['habitacione_id'].'<br>';
            }
        }



        $CASO6 = $this->Resermultiple->find('all', array('conditions'=>array('Resermultiple.fecha_entrada >='=>$fecha_entrada, 'Resermultiple.fecha_salida <='=>$fecha_salida)));

        foreach ($CASO6 as $key) {
            foreach ($key['Resermulhabitacione'] as $data) {
                $array6[] = $data['habitacione_id'].'<br>';
            }

        }


        $CASO7 =  $this->Resermultiple->find('all', array('conditions'=>array('Resermultiple.fecha_salida <='=>$fecha_entrada, 'Resermultiple.fecha_salida > '=>$fecha_entrada)));

        foreach ($CASO7 as $key) {
            foreach ($key['Resermulhabitacione'] as $data) {
                $array7[] = $data['habitacione_id'].'<br>';
            }

        }



        $CASO8 =  $this->Resermultiple->find('all', array('conditions'=>array('Resermultiple.fecha_entrada >='=>$fecha_entrada, 'Resermultiple.fecha_entrada <'=>$fecha_salida)));

        foreach ($CASO8 as $key) {
            foreach ($key['Resermulhabitacione'] as $data) {
                $array8[] = $data['habitacione_id'].'<br>';
            }

        }

        $arrayhab = array_merge($array5, $array6, $array7, $array8);




        $HAB1 = $this->Habitacione->find('all', array('conditions'=>array('Habitacione.id'=>$arrayhab)));

        foreach ( $HAB1 as $key ) {

            $arrayhab[] = $key['Habitacione']['numhabitacion'];

        }



        $array = array_merge($array1, $array2, $array3, $array4, $arrayhab);




        $this->set('habitaciones', $this->Habitacione->find('all',array('conditions'=>array('Habitacione.tipohabitacione_id '=>$idTipoHabitacion, 'Habitacione.numhabitacion !='=>$array))));



    }









    /**
     * status method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function status($id = null) {
        return $this->redirect('/Reserindivistatuses/add/'.$id);
    }


    /**
     * reporte method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */

    public function reporte($id=null){
        $this->layout = 'pdf';
        App::import('Vendor', 'Fpdf', array('file' => 'fpdf181/fpdf.php'));
        $this->set('name', 'planilla_confirmacion_entrada_'.date('Y_m_d').".pdf");

    }


    public function precios($fechae = null, $fechas = null, $idtipohabitacion = null, $paxadicional = 0){
        $this->layout = 'ajax';

        //Obtengo el precio por defecto
        $preciodef = $this->Setting->find('all');
        $preciodefpax = $preciodef[0]['Setting']['preciopax'];
        $preciodefpaxad = $preciodef[0]['Setting']['preciopaxadicional'];


        $totalpaxadicional = $paxadicional * $preciodefpaxad;

        $preciopornoche = $preciodefpax + $totalpaxadicional;

        $totalprecio = 0;



        $fecha1 = new DateTime($fechae);
        $fecha2 = new DateTime($fechas);
        $resultado = $fecha1->diff($fecha2);
        //echo $resultado->format('%a');
        $cantdias =  $resultado->format('%a');

        $nuevafecha = strtotime ( '+'.$cantdias.' day' , strtotime ( $fechae ) ) ;

        for ($i=1; $i <= $cantdias ; $i++) {
            //Sumando dia a dia la fecha
            $nuevafecha1 = strtotime ( '+'.$i.' day' , strtotime ( $fechae ) ) ;
            $nuevafecha = date ( 'Y-m-d' , $nuevafecha1 );
            //echo $idtipohabitacion;


            // $evaluaprecio = $this->Cupo->find('all', array('conditions'=>array('Cupo.fecha'=>'2018-09-01', 'Cupo.tipohabitacione_id'=>'9')));
            // $fechae = parse_str($fechae);
            $evaluaprecio = $this->Cupo->find('all', array('conditions'=> array('Cupo.fecha'=> $nuevafecha, 'Cupo.tipohabitacione_id' => $idtipohabitacion )));

            if (count($evaluaprecio) == 0) {
                //Al no estar definido un precio en cupo, tomo el precio por defecto desde la tabla settings
                $totalprecio += $preciodefpax + $totalpaxadicional;

            }else{
                $totalprecio += $evaluaprecio[0]['Cupo']['precio'] + ( $paxadicional * $evaluaprecio[0]['Cupo']['preciopaxadicional']);
            }


        }

        $this->set('precio', $totalprecio);
        $this->set('preciopornoche', $preciopornoche);

    }

}

