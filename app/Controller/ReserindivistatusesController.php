<?php
App::uses('AppController', 'Controller');
/**
 * Reserindivistatuses Controller
 *
 * @property Reserindivistatus $Reserindivistatus
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ReserindivistatusesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

	var $uses = array('Cliente','Reserindivistatus', 'Reserindividuale', 'Habitacione','Tarjetacredito','Resermultiple','Reserindividualeextra','Bodymail', 'Auditoria');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(6);
	}

/**
 * index method
 *
 * @return void
 */
	public function index($id=null) {
		//$this->Reserindivistatus->recursive = 0;
		//$this->set('reserindivistatuses', $this->Paginator->paginate());
		  $this->set('reserindivistatuses', $this->Reserindivistatus->find('all'));
		  if($id!=null && $id!='0'){
		  	return $this->redirect('/Reserindividuales/index');
		  }
	}

	public function index_2($id=null) {
		//$this->Reserindivistatus->recursive = 0;
		//$this->set('reserindivistatuses', $this->Paginator->paginate());
		  $this->set('reserindivistatuses', $this->Reserindivistatus->find('all'));
		  if($id!=null){
		  	return $this->redirect('/Reservas/reservas');
		  }
	}
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Reserindivistatus->exists($id)) {
			throw new NotFoundException(__('Invalid reserindivistatus'));
		}
		$options = array('conditions' => array('Reserindivistatus.' . $this->Reserindivistatus->primaryKey => $id));
		$this->set('reserindivistatus', $this->Reserindivistatus->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($id=null) {
		if ($this->request->is('post')) {


			if (strpos($this->request->data['Reserindivistatus']['fecha'], '/')) {
				
				$acomodafecha = explode('/', $this->request->data['Reserindivistatus']['fecha']);
				$this->request->data['Reserindivistatus']['fecha'] = $acomodafecha[2].'-'.$acomodafecha[1].'-'.$acomodafecha[0];
				
			}
			 


			$emailingresada = 0;
			$emailegresada  = 0;
			$fechainicio = '';
			$fechasalida = '';
			$email = 0;
			$entrada = 0;

			$datasource = $this->Reserindivistatus->getDataSource();
			try{
			    $datasource->begin();

			    	
					if (!$this->Reserindivistatus->save($this->request->data)) {
						throw new Exception("Error Saving Reserindivistatus", 1);
					}else{
			              //$id   =  $this->Reserindividuale->id;
						   $stop = 0;
						   if( !empty($this->request->data["Reserindividuale"]["Nombre"][0]) ){
								foreach($this->request->data["Reserindividuale"]["Nombre"] as $nombre){
									    $this->request->data["Reserindividualeextra"]["reserindividuale_id"] = $id;
									    $this->request->data["Reserindividualeextra"]["nombres"]             = $nombre;
									    $this->Reserindividualeextra->create();
									if (!$this->Reserindividualeextra->save($this->request->data)){
										throw new Exception("Error Saving Reserindividualeextra", 1);
									}else{
										$stop = 1;
									}
								}
						   }

						   $this->request->data['Reserindividuale']['id'] = $this->request->data['Reserindivistatus']['reserindividuale_id'];
						   $this->request->data['Reserindividuale']['reserstatusindividuale_id'] = $this->request->data['Reserindivistatus']['reserstatusindividuale_id'];
		                  
		                   if($this->request->data['Reserindividuale']['reserstatusindividuale_id']==2){
							   $this->request->data['Reserindividuale']['pagado'] = $this->request->data['Reserindivistatus']['pago'];
							}
					}

	               	if (!$this->Reserindividuale->save($this->request->data)){
	               		throw new Exception("Reserindividuale", 1);
	               	}else{
	               		
	                    $this->request->data['Habitacione']['id'] = $this->request->data['Reserindivistatus']['habitacione_id'];
	                    if($this->request->data['Reserindividuale']['reserstatusindividuale_id']==2){
	                          	 $this->request->data['Habitacione']['habistatu_id'] = 2;
	                          	 $email = 1;
	                    }else if($this->request->data['Reserindividuale']['reserstatusindividuale_id']==4){
	                    	     $this->request->data['Habitacione']['habistatu_id'] = 2;
	                    	     $entrada = $this->request->data['Reserindividuale']['id'];

	                    	     $emailingresada = 1;

	                    }else if($this->request->data['Reserindividuale']['reserstatusindividuale_id']==3){
	                    	     $this->request->data['Habitacione']['habistatu_id'] = 1;
	                    }else if($this->request->data['Reserindividuale']['reserstatusindividuale_id']==5){
	                    	     $this->request->data['Habitacione']['habistatu_id'] = 1;
	                    	     $emailegresada = 1;

	                    }else if($this->request->data['Reserindividuale']['reserstatusindividuale_id']==6){
	                             $this->request->data['Habitacione']['habistatu_id'] = 1;
	                    }
	                }

                    if (!$this->Habitacione->save($this->request->data)) {
                    	throw new Exception("Error Saving Habitacione", 1);                    	
                    }else{

                    	$idR = $this->request->data['Reserindivistatus']['reserindividuale_id'];

                    	$cliente = $this->Reserindividuale->find('all', array('conditions'=>array('Reserindividuale.id'=>$idR)));

                    	$idcliente = $cliente[0]['Reserindividuale']['cliente_id'];

                    	$id_factura = !empty($cliente[0]['Factura'][0]['id']) ? $cliente[0]['Factura'][0]['id'] : 0; 
                    	
						$datacliente = $this->Cliente->find('all', array('conditions'=>array('Cliente.id'=>$idcliente)));

						$email   = !empty($datacliente[0]['Cliente']['correo'])          ? $datacliente[0]['Cliente']['correo']          : '';
						$nombres = !empty($datacliente[0]['Cliente']['nombre_completo']) ? $datacliente[0]['Cliente']['nombre_completo'] : '';
						
						/**
						 * Extraemos el cuerpo de el mensaje
						 */						

						$cabeceras  = "MIME-Version: 1.0\r\n";
						$cabeceras .= "Content-Transfer-Encoding: 8Bit\r\n";
						$cabeceras .= "Content-Type: text/html; charset=\"utf-8\"\r\n";
						$cabeceras .= "Reply-to: \r\n";
						$cabeceras .= "From: serviciodereservas \r\n";
						$cabeceras .= "Errors-To: \r\n";


						/**
						 * Mail para reserva iniciada
						 */
						
                    	 //Si el estatus cambia a ingresada mando mail
                    	 //
                    	if ($emailingresada == 1) {
                    	 	$bodymail = $this->Bodymail->find('all', array('conditions'=>array('Bodymail.id'=>2)));

                    	 	$subject = $bodymail[0]['Bodymail']['denominacion'];

                    	 	$bodymail = $bodymail[0]['Bodymail']['cuerpoemail'];

                    	 	$cuerpo = str_replace('_nombres_', $nombres, $bodymail);

                    	 	$cuerpo = str_replace('_fechainicio_', $fechainicio, $cuerpo);

                    	 	$cuerpo = str_replace('_fechasalida_', $fechasalida, $cuerpo);
							mail($email,$subject,$cuerpo, $cabeceras);
                    	 	
                    	}

                    	if ($emailegresada) {
                    	 	$bodymail = $this->Bodymail->find('all', array('conditions'=>array('Bodymail.id'=>3)));

                    	 	$subject = $bodymail[0]['Bodymail']['denominacion'];

                    	 	$bodymail = $bodymail[0]['Bodymail']['cuerpoemail'];

                    	 	$cuerpo = str_replace('_nombres_', $nombres, $bodymail);

                    	 	$cuerpo = str_replace('_fechainicio_', $fechainicio, $cuerpo);

                    	 	$cuerpo = str_replace('_fechasalida_', $fechasalida, $cuerpo);
                    	 	mail($email,$subject,$cuerpo, $cabeceras);
                    	}



                    	 
	                }

	                $this->loadModel('Auditoria');
	                $auditoria = $this->Auditoria->find('first', [
	                	'Auditoria.reserindividuale_id' => $id
	                ]);

	                

	                
	               

	                if(!empty($auditoria)){


		                $estatus = $this->request->data['Reserindivistatus']['reserstatusindividuale_id'];

		                $especifico = $this->requestAction('reserstatusindividuales/get_estatus/'.$estatus);

		                $auditoria['Auditoria']['id']                  = $auditoria['Auditoria']['id'];
		                $auditoria['Auditoria']['user_id']             = $this->Session->read('USUARIO_ID');
		                $auditoria['Auditoria']['cliente_id']          = $idcliente;
		                if(!empty($id_factura)){
		                	$auditoria['Auditoria']['factura_id']      = $id_factura;
		                }
		                $auditoria['Auditoria']['hora']                = DboSource::expression('NOW()');
		                $auditoria['Auditoria']['motivo']              = !empty($this->request->data['Auditoria']['motivo']) ? $this->request->data['Auditoria']['motivo'] : '';
		                $auditoria['Auditoria']['tipo_operacion']      = 'ACTUALIZACION DE ESTATUS';
		                $auditoria['Auditoria']['especifico']          = $especifico;

					
				        if(!empty($this->request->data['Reserindivistatus']['total'])){
				        	$auditoria['Auditoria']['costo_total']     = $this->request->data['Reserindivistatus']['total'];	
				        }
				        
				        $auditoria['Auditoria']['forma_pago']          = !empty($this->request->data['Reserindivistatus']['conftipopagoreserva_id']) ? $this->requestAction('conftipopagoreservas/get_tipo_pago/'.$this->request->data['Reserindivistatus']['conftipopagoreserva_id']) : '-';
				        

				    }   

	                if(!$this->Auditoria->save($auditoria)){
	                	throw new Exception("Error Saving Auditoria", 1);	                	
	                }




	                $datasource->commit();
	                $this->Flash->success(__('Registro Guardado.'.$this->request->data['Reserindivistatus']['observaciones']));

	                
	            	if ($estatus == 5) {
	            	 	return $this->redirect(array('controller'=>'Facturas','action' => 'index'));
	            	}

	                if ($entrada == 0) {
					 	return $this->redirect('/Reserindividuales/index/');
            		}else{	
					 	return $this->redirect('/Reserindividuales/index/'.$entrada);
            		}

            	}catch(Exception $e){
            		$datasource->rollback();
            		$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo...'));
            	}
		         
		}
		$reserindividuales       = $this->Reserindivistatus->Reserindividuale->find('list');
		$reserindividuales2      = $this->Reserindivistatus->Reserindividuale->find('all', array('conditions'=>array('Reserindividuale.id'=>$id)));
		$reserstatusindividuales = $this->Reserindivistatus->Reserstatusindividuale->find('list');
		$conftipopagoreservas    = $this->Reserindivistatus->Conftipopagoreserva->find('list');
		$idCliente				 = $this->Reserindivistatus->Reserindividuale->find('all', array('conditions'=>array('Reserindividuale.id'=>$id)));

		if ($idCliente[0]['Reserindividuale']['resermultiple_id'] == 0) {
			$idCliente = $idCliente[0]['Reserindividuale']['cliente_id'];
			
		}else{
			$idRMultiple = $idCliente[0]['Reserindividuale']['resermultiple_id'];
			$idCliente				 = $this->Resermultiple->find('all', array('conditions'=>array('Resermultiple.id'=>$idRMultiple)));
			$idCliente = $idCliente[0]['Resermultiple']['cliente_id'];
		}


		$tarjetacreditos = $this->Tarjetacredito->find('list', array('conditions'=>array('Tarjetacredito.cliente_id' => $idCliente)));
		
		$this->set(compact('reserindividuales', 'reserstatusindividuales', 'conftipopagoreservas', 'reserindividuales2','tarjetacreditos'));
		$this->set('id',$id);
	}



/**
 * add method
 *
 * @return void
 */
	public function add_2($id=null) {
		if ($this->request->is('post')) {
			$this->Reserindividuale->begin();
			$this->Reserindivistatus->create();
			if ($this->Reserindivistatus->save($this->request->data)) {
				//$id   =  $this->Reserindividuale->id;
				   $stop = 0;
				   if(!empty($this->request->data["Reserindividuale"]["Nombre"])){
						foreach($this->request->data["Reserindividuale"]["Nombre"] as $nombre){
							    $this->request->data["Reserindividualeextra"]["reserindividuale_id"] = $id;
							    $this->request->data["Reserindividualeextra"]["nombres"]             = $nombre;
							    $this->Reserindividualeextra->create();
							if ($this->Reserindividualeextra->save($this->request->data)){

							}else{
								$stop = 1;
							}
						}
				   }
				   $this->request->data['Reserindividuale']['id']                        = $this->request->data['Reserindivistatus']['reserindividuale_id'];
				   $this->request->data['Reserindividuale']['reserstatusindividuale_id'] = $this->request->data['Reserindivistatus']['reserstatusindividuale_id'];
                   if($this->request->data['Reserindividuale']['reserstatusindividuale_id']==2){
					   $this->request->data['Reserindividuale']['pagado'] = $this->request->data['Reserindivistatus']['pago'];
					}
               if ($this->Reserindividuale->save($this->request->data)){
               	$entrada = 0;
                         	 $this->request->data['Habitacione']['id'] = $this->request->data['Reserindivistatus']['habitacione_id'];
                          if($this->request->data['Reserindividuale']['reserstatusindividuale_id']==2){
                          	 $this->request->data['Habitacione']['habistatu_id'] = 2;
                    }else if($this->request->data['Reserindividuale']['reserstatusindividuale_id']==4){
                    	     $this->request->data['Habitacione']['habistatu_id'] = 2;
                    	     $entrada = $this->request->data['Reserindividuale']['id'];
                    }else if($this->request->data['Reserindividuale']['reserstatusindividuale_id']==3){
                    	     $this->request->data['Habitacione']['habistatu_id'] = 1;
                    }else if($this->request->data['Reserindividuale']['reserstatusindividuale_id']==5){
                    	     $this->request->data['Habitacione']['habistatu_id'] = 1;
                    }else if($this->request->data['Reserindividuale']['reserstatusindividuale_id']==6){
                             $this->request->data['Habitacione']['habistatu_id'] = 1;
                    }
                    if ($this->Habitacione->save($this->request->data)) {
                   		 $this->Reserindividuale->commit();
                		 $this->Flash->success(__('Registro Guardado.'));
						 return $this->redirect('/Reservas/reservas/'.$entrada);
	                }else{
	                	$this->Reserindividuale->rollback();
	                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
	                }
               }else{
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
               }
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
		$reserindividuales       = $this->Reserindivistatus->Reserindividuale->find('list');
		$reserindividuales2      = $this->Reserindivistatus->Reserindividuale->find('all', array('conditions'=>array('Reserindividuale.id'=>$id)));
		$reserstatusindividuales = $this->Reserindivistatus->Reserstatusindividuale->find('list');
		$conftipopagoreservas    = $this->Reserindivistatus->Conftipopagoreserva->find('list');
		$idCliente				 = $this->Reserindivistatus->Reserindividuale->find('all', array('conditions'=>array('Reserindividuale.id'=>$id)));

		if ($idCliente[0]['Reserindividuale']['resermultiple_id'] == 0) {
			$idCliente = $idCliente[0]['Reserindividuale']['cliente_id'];
			
		}else{
			$idRMultiple = $idCliente[0]['Reserindividuale']['resermultiple_id'];
			$idCliente	 = $this->Resermultiple->find('all', array('conditions'=>array('Resermultiple.id'=>$idRMultiple)));
			$idCliente   = $idCliente[0]['Resermultiple']['cliente_id'];
		}
		$tarjetacreditos 		 = $this->Tarjetacredito->find('list', array('conditions'=>array('Tarjetacredito.cliente_id' => $idCliente)));
		$this->set(compact('tarjetacreditos','reserindividuales', 'reserstatusindividuales', 'conftipopagoreservas', 'reserindividuales2'));
		$this->set('id',$id);
	}	

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Reserindivistatus->exists($id)) {
			throw new NotFoundException(__('Invalid reserindivistatus'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Reserindivistatus->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Reserindivistatus.' . $this->Reserindivistatus->primaryKey => $id));
			$this->request->data = $this->Reserindivistatus->find('first', $options);
		}
		$reserindividuales = $this->Reserindivistatus->Reserindividuale->find('list');
		$reserstatusindividuales = $this->Reserindivistatus->Reserstatusindividuale->find('list');
		$conftipopagoreservas = $this->Reserindivistatus->Conftipopagoreserva->find('list');
		$this->set(compact('reserindividuales', 'reserstatusindividuales', 'conftipopagoreservas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Reserindivistatus->id = $id;
		if (!$this->Reserindivistatus->exists()) {
			throw new NotFoundException(__('Invalid reserindivistatus'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Reserindivistatus->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


	/**
	 * reporte method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */

	public function reporte($id=null){
	        $this->layout = 'ajax';
	        $this->Reserindivistatus->Reserindividuale->recursive = 2;
	       // App::import('Vendor', 'Fpdf', array('file' => 'fpdf181/fpdf.php'));
	        $reserindividuales = $this->Reserindivistatus->Reserindividuale->find('all', array('conditions'=>array('Reserindividuale.id'=>$id)  ));
	       // $this->set('reserindividuales', $reserindividuales);
        //    $this->set('name', 'planilla_confirmacion_entrada_'.date('Y_m_d').".pdf");
        

        $email = $reserindividuales[0]['Cliente']['correo'];

        $nombres = $reserindividuales[0]['Cliente']['nombre_completo'];



        
        
        App::import('Vendor', 'PHPMailer', array('file' => 'phpmailer/phpmailer.php'));
        App::import('Vendor', 'PHPMailer', array('file' => 'phpmailer/class.phpmailer.php'));
        
        // ENVIO MAIL 2:  DECLARO LA VARIABLE
        $mail = new PHPMailer();

        // ENVIO MAIL 3: ESTABLEZCO PARAMETROS
        $mail->From       = "no-reply@centraldereservas.com";
        $mail->FromName   = "Central de Reservas";

        $mail->Subject    = "Datos de su reservación.";


        $nombres = ucwords(strtolower($nombres));

        $subject = "Su reserva a pasado a estatus: Ingresada.";



        $body = templateMail($nombres,$subject,'Bienvenido y que disfrute de su estadía.');
        $body = preg_replace("[\]",'',$body);

        $mail->MsgHTML($body);


        $mail->AddAddress($email,$nombres);


        if(!$mail->Send()) {
          $this->log('Error en el envio de mensaje: '.$mail->ErrorInfo);
        } else {
          $this->log('Mensaje enviado con exito.');
        }

	}
}
