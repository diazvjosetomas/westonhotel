<?php
App::uses('AppController', 'Controller');
/**
 * Proproductos Controller
 *
 * @property Proproducto $Proproducto
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ProproductosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');

	var $uses = array('Proproducto', 'Protipo', 'Promarca', 'Proiva');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(3);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Proproducto->recursive = 0;
		//$this->set('proproductos', $this->Paginator->paginate());
		  $this->set('proproductos', $this->Proproducto->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Proproducto->exists($id)) {
			throw new NotFoundException(__('Invalid proproducto'));
		}
		$options = array('conditions' => array('Proproducto.' . $this->Proproducto->primaryKey => $id));
		$this->set('proproducto', $this->Proproducto->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Proproducto->create();
			if ($this->Proproducto->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
		$protipos = $this->Proproducto->Protipo->find('list');
		$promarcas = array();
		$proivas = $this->Proiva->find('all');
		$this->set(compact('protipos', 'promarcas', 'proivas'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Proproducto->exists($id)) {
			throw new NotFoundException(__('Invalid proproducto'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Proproducto->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Proproducto.' . $this->Proproducto->primaryKey => $id));
			$this->request->data = $this->Proproducto->find('first', $options);
		}
		$protipos = $this->Proproducto->Protipo->find('list');
		$promarcas = array();
		$proivas = $this->Proiva->find('all');
		$this->set(compact('protipos', 'promarcas', 'proivas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Proproducto->id = $id;
		if (!$this->Proproducto->exists()) {
			throw new NotFoundException(__('Invalid proproducto'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Proproducto->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}





 /**
 * sub_cliente method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function marca($id = null) {
		$this->layout = 'ajax';
	    $promarcas = $this->Proproducto->Promarca->find('list', array('fields'=>array('Promarca.id', 'Promarca.denominacion'), 'conditions'=>array('Promarca.protipo_id'=>$id)));
	    $this->set(compact('promarcas'));

	}
}
