<?php
App::uses('AppController', 'Controller');
/**
 * Promarcas Controller
 *
 * @property Promarca $Promarca
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PromarcasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(3);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Promarca->recursive = 0;
		//$this->set('promarcas', $this->Paginator->paginate());
		  $this->set('promarcas', $this->Promarca->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Promarca->exists($id)) {
			throw new NotFoundException(__('Invalid promarca'));
		}
		$options = array('conditions' => array('Promarca.' . $this->Promarca->primaryKey => $id));
		$this->set('promarca', $this->Promarca->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Promarca->create();
			if ($this->Promarca->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
		$protipos = $this->Promarca->Protipo->find('list');
		$proproductos = array();
		$this->set(compact('protipos', 'proproductos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Promarca->exists($id)) {
			throw new NotFoundException(__('Invalid promarca'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Promarca->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Promarca.' . $this->Promarca->primaryKey => $id));
			$this->request->data = $this->Promarca->find('first', $options);
		}
		$protipos     = $this->Promarca->Protipo->find('list');
		$proproductos = $this->Promarca->Proproducto->find('list', array('conditions'=>array('Proproducto.id'=>$this->request->data['Promarca']['proproducto_id'])));
		$this->set(compact('protipos', 'proproductos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Promarca->id = $id;
		if (!$this->Promarca->exists()) {
			throw new NotFoundException(__('Invalid promarca'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Promarca->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


/**
 * producto method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function producto($id = null) {
		$this->layout = "ajax";
		$proproductos = $this->Promarca->Proproducto->find('list', array('conditions'=>array('Proproducto.id'=>$id)));
		$this->set(compact('proproductos'));
	}
}
