<?php
App::uses('AppController', 'Controller');
/**
 * Conftipopagoreservas Controller
 *
 * @property Conftipopagoreserva $Conftipopagoreserva
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ConftipopagoreservasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(13);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Conftipopagoreserva->recursive = 0;
		//$this->set('conftipopagoreservas', $this->Paginator->paginate());
		  $this->set('conftipopagoreservas', $this->Conftipopagoreserva->find('all'));
	}

	public function get_tipo_pago($id_conftipogagoreserva = null) {
		$data = $this->Conftipopagoreserva->find('first', ['conditions' => $id_conftipogagoreserva]);
		return $data['Conftipopagoreserva']['denominacion'];
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Conftipopagoreserva->exists($id)) {
			throw new NotFoundException(__('Invalid conftipopagoreserva'));
		}
		$options = array('conditions' => array('Conftipopagoreserva.' . $this->Conftipopagoreserva->primaryKey => $id));
		$this->set('conftipopagoreserva', $this->Conftipopagoreserva->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Conftipopagoreserva->create();
			if ($this->Conftipopagoreserva->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Conftipopagoreserva->exists($id)) {
			throw new NotFoundException(__('Invalid conftipopagoreserva'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Conftipopagoreserva->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Conftipopagoreserva.' . $this->Conftipopagoreserva->primaryKey => $id));
			$this->request->data = $this->Conftipopagoreserva->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Conftipopagoreserva->id = $id;
		if (!$this->Conftipopagoreserva->exists()) {
			throw new NotFoundException(__('Invalid conftipopagoreserva'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Conftipopagoreserva->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
