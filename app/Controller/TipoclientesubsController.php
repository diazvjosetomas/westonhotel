<?php
App::uses('AppController', 'Controller');
/**
 * Tipoclientesubs Controller
 *
 * @property Tipoclientesub $Tipoclientesub
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class TipoclientesubsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(1);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Tipoclientesub->recursive = 0;
		//$this->set('tipoclientesubs', $this->Paginator->paginate());
		  $this->set('tipoclientesubs', $this->Tipoclientesub->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Tipoclientesub->exists($id)) {
			throw new NotFoundException(__('Invalid tipoclientesub'));
		}
		$options = array('conditions' => array('Tipoclientesub.' . $this->Tipoclientesub->primaryKey => $id));
		$this->set('tipoclientesub', $this->Tipoclientesub->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Tipoclientesub->create();
			if ($this->Tipoclientesub->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
		$tipoclientes = $this->Tipoclientesub->Tipocliente->find('list');
		$this->set(compact('tipoclientes'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Tipoclientesub->exists($id)) {
			throw new NotFoundException(__('Invalid tipoclientesub'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Tipoclientesub->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Tipoclientesub.' . $this->Tipoclientesub->primaryKey => $id));
			$this->request->data = $this->Tipoclientesub->find('first', $options);
		}
		$tipoclientes = $this->Tipoclientesub->Tipocliente->find('list');
		$this->set(compact('tipoclientes'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Tipoclientesub->id = $id;
		if (!$this->Tipoclientesub->exists()) {
			throw new NotFoundException(__('Invalid tipoclientesub'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Tipoclientesub->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
