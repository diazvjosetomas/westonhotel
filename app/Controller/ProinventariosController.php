<?php
App::uses('AppController', 'Controller');
/**
 * Proinventarios Controller
 *
 * @property Proinventario $Proinventario
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ProinventariosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession();
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Proinventario->recursive = 0;
		//$this->set('proinventarios', $this->Paginator->paginate());
		  $this->set('proinventarios', $this->Proinventario->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Proinventario->exists($id)) {
			throw new NotFoundException(__('Invalid proinventario'));
		}
		$options = array('conditions' => array('Proinventario.' . $this->Proinventario->primaryKey => $id));
		$this->set('proinventario', $this->Proinventario->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Proinventario->create();
			if ($this->Proinventario->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
		$protipos = $this->Proinventario->Protipo->find('list');
		$promarcas = array();
		$proproductos = array();
		$protipopisos = $this->Proinventario->Protipopiso->find('list');
		$proalmacenes = array();
		$this->set(compact('protipos', 'promarcas', 'proproductos','protipopisos','proalmacenes'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Proinventario->exists($id)) {
			throw new NotFoundException(__('Invalid proinventario'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Proinventario->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Proinventario.' . $this->Proinventario->primaryKey => $id));
			$this->request->data = $this->Proinventario->find('first', $options);
		}
		$protipos     = $this->Proinventario->Protipo->find('list');
		$promarcas    = $this->Proinventario->Promarca->find('list',    array('conditions' => array('Promarca.protipo_id'   =>$this->request->data["Proinventario"]["protipo_id"])));
		$proproductos = $this->Proinventario->Proproducto->find('list', array('conditions' => array('Proproducto.protipo_id'=>$this->request->data["Proinventario"]["protipo_id"],  'Proproducto.promarca_id'=>$this->request->data["Proinventario"]["promarca_id"])));
		$protipopisos = $this->Proinventario->Protipopiso->find('list');
		$proalmacenes = $this->Proinventario->Proalmacene->find('list', array('conditions' => array('Proalmacene.protipopiso_id'=>$this->request->data["Proinventario"]["protipopiso_id"])));
		$this->set(compact('protipos', 'promarcas', 'proproductos','protipopisos','proalmacenes'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Proinventario->id = $id;
		if (!$this->Proinventario->exists()) {
			throw new NotFoundException(__('Invalid proinventario'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Proinventario->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


/**
 * almacen method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function almacen($id = null) {
		$this->layout = 'ajax';
	    $proalmacenes = $this->Proinventario->Proalmacene->find('list', array('fields'=>array('Proalmacene.id', 'Proalmacene.denominacion'), 'conditions'=>array('Proalmacene.protipopiso_id'=>$id)));
	    $this->set(compact('proalmacenes'));

	}
/**
 * tipoproducto method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function tipoproducto($id = null) {
		$this->layout = 'ajax';
	    $promarcas = $this->Proinventario->Promarca->find('list', array('fields'=>array('Promarca.id', 'Promarca.denominacion'), 'conditions'=>array('Promarca.protipo_id'=>$id)));
	    $this->set(compact('promarcas'));
	    $this->set('var', $id);

	}
/*
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function producto($id = null, $id2 = null) {
		$this->layout = 'ajax';
	    $proproductos = $this->Proinventario->Proproducto->find('list', array('fields'=>array('Proproducto.id', 'Proproducto.denominacion'), 'conditions'=>array('Proproducto.protipo_id'=>$id, 'Proproducto.promarca_id'=>$id2)));
	    $this->set(compact('proproductos'));

	}














}
