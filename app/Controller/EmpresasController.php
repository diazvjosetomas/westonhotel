<?php
App::uses('AppController', 'Controller');
/**
 * Empresas Controller
 *
 * @property Empresa $Empresa
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class EmpresasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(13);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Empresa->recursive = 0;
		//$this->set('empresas', $this->Paginator->paginate());
		  $this->set('empresas', $this->Empresa->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Empresa->exists($id)) {
			throw new NotFoundException(__('Invalid empresa'));
		}
		$options = array('conditions' => array('Empresa.' . $this->Empresa->primaryKey => $id));
		$this->set('empresa', $this->Empresa->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			if (!empty($this->request->data['Empresa']['imagen']) && imagenType($this->request->data['Empresa']['imagen']['type']) != false) {
				$ds = '/';
				$storeFolder = 'upload';
				$tempFile    = $this->request->data['Empresa']['imagen']['tmp_name'];
				$targetPath  = ROOT . $ds .'app'. $ds .'webroot'. $ds .'img'. $ds . $storeFolder . $ds;
				$extencion   = imagenType($this->request->data['Empresa']['imagen']['type']);
				$renameFile  = 'a_'.date('dmY_his').$extencion;
				$targetFile  = $targetPath.$renameFile;
				if(move_uploaded_file($tempFile,$targetFile)){
					$tipo_imagen = $this->request->data['Empresa']['imagen']['type'];
					$nombre_imagen = $renameFile;;
					$this->request->data['Empresa']['carpeta_imagen'] = $storeFolder;
					$this->request->data['Empresa']['nombre_imagen'] = $nombre_imagen;
					$this->request->data['Empresa']['tipo_imagen'] = $tipo_imagen;
					$this->request->data['Empresa']['ruta_imagen'] = $storeFolder.'/'.$nombre_imagen;
				}else{
					$this->request->data['Empresa']['carpeta_imagen']  = "_";
					$this->request->data['Empresa']['nombre_imagen']   = "_";
					$this->request->data['Empresa']['tipo_imagen']     = "_";
					$this->request->data['Empresa']['ruta_imagen'] = "_";
				}

		    }else{
		    	    $this->request->data['Empresa']['carpeta_imagen']  = "_";
					$this->request->data['Empresa']['nombre_imagen']   = "_";
					$this->request->data['Empresa']['tipo_imagen']     = "_";
					$this->request->data['Empresa']['ruta_imagen'] = "_";
			}
			$this->Empresa->create();
			if ($this->Empresa->save($this->request->data)) {
				$this->Flash->success(__('The empresa has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The empresa could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Empresa->exists($id)) {
			throw new NotFoundException(__('Invalid empresa'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if (!empty($this->request->data['Empresa']['imagen']) && imagenType($this->request->data['Empresa']['imagen']['type']) != false) {
				$ds = '/';
				$storeFolder = 'upload';
				$tempFile    = $this->request->data['Empresa']['imagen']['tmp_name'];
				$targetPath  = ROOT . $ds .'app'. $ds .'webroot'. $ds .'img'. $ds . $storeFolder . $ds;
				$extencion   = imagenType($this->request->data['Empresa']['imagen']['type']);
				$renameFile  = 'a_'.date('dmY_his').$extencion;
				$targetFile  = $targetPath.$renameFile;
				if(move_uploaded_file($tempFile,$targetFile)){
					$tipo_imagen = $this->request->data['Empresa']['imagen']['type'];
					$nombre_imagen = $renameFile;;
					$this->request->data['Empresa']['carpeta_imagen'] = $storeFolder;
					$this->request->data['Empresa']['nombre_imagen']  = $nombre_imagen;
					$this->request->data['Empresa']['tipo_imagen']    = $tipo_imagen;
					$this->request->data['Empresa']['ruta_imagen']    = $storeFolder.'/'.$nombre_imagen;
				}else{
					$this->request->data['Empresa']['carpeta_imagen']  = "_";
					$this->request->data['Empresa']['nombre_imagen']   = "_";
					$this->request->data['Empresa']['tipo_imagen']     = "_";
					$this->request->data['Empresa']['ruta_imagen'] = "_";
				}

		    }else{
		    	    $this->request->data['Empresa']['carpeta_imagen']  = "_";
					$this->request->data['Empresa']['nombre_imagen']   = "_";
					$this->request->data['Empresa']['tipo_imagen']     = "_";
					$this->request->data['Empresa']['ruta_imagen'] = "_";
			}
			if ($this->Empresa->save($this->request->data)) {
				$this->Flash->success(__('The empresa has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The empresa could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Empresa.' . $this->Empresa->primaryKey => $id));
			$this->request->data = $this->Empresa->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Empresa->id = $id;
		if (!$this->Empresa->exists()) {
			throw new NotFoundException(__('Invalid empresa'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Empresa->delete()) {
			$this->Flash->success(__('The empresa has been deleted.'));
		} else {
			$this->Flash->error(__('The empresa could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


 


}
