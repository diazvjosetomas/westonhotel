<?php
App::uses('AppController', 'Controller');
/**
 * Tarjetacreditos Controller
 *
 * @property Tarjetacredito $Tarjetacredito
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class TarjetacreditosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(1);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Tarjetacredito->recursive = 0;
		//$this->set('tarjetacreditos', $this->Paginator->paginate());
		  $this->set('tarjetacreditos', $this->Tarjetacredito->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Tarjetacredito->exists($id)) {
			throw new NotFoundException(__('Invalid tarjetacredito'));
		}
		$options = array('conditions' => array('Tarjetacredito.' . $this->Tarjetacredito->primaryKey => $id));
		$this->set('tarjetacredito', $this->Tarjetacredito->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->request->data['Tarjetacredito']['vencimiento'] = !empty($this->request->data['Tarjetacredito']['vencimiento1']) ? formatym($this->request->data['Tarjetacredito']['vencimiento1']) : '';
			$this->Tarjetacredito->create();
			if ($this->Tarjetacredito->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
		$clientes = $this->Tarjetacredito->Cliente->find('list');
		$tipotarjetas = $this->Tarjetacredito->Tipotarjeta->find('list');
		$this->set(compact('clientes', 'tipotarjetas'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Tarjetacredito->exists($id)) {
			throw new NotFoundException(__('Invalid tarjetacredito'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Tarjetacredito']['vencimiento'] = !empty($this->request->data['Tarjetacredito']['vencimiento1']) ? formatym($this->request->data['Tarjetacredito']['vencimiento1']) : '';
			if ($this->Tarjetacredito->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Tarjetacredito.' . $this->Tarjetacredito->primaryKey => $id));
			$this->request->data = $this->Tarjetacredito->find('first', $options);
			$this->request->data['Tarjetacredito']['vencimiento'] = formatmy($this->request->data['Tarjetacredito']['vencimiento']);
		}
		$clientes = $this->Tarjetacredito->Cliente->find('list');
		$tipotarjetas = $this->Tarjetacredito->Tipotarjeta->find('list');
		$this->set(compact('clientes', 'tipotarjetas'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Tarjetacredito->id = $id;
		if (!$this->Tarjetacredito->exists()) {
			throw new NotFoundException(__('Invalid tarjetacredito'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Tarjetacredito->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
