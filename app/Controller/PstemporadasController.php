<?php
App::uses('AppController', 'Controller');
/**
 * Pstemporadas Controller
 *
 * @property Pstemporada $Pstemporada
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PstemporadasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(2);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Pstemporada->recursive = 0;
		//$this->set('pstemporadas', $this->Paginator->paginate());
		  $this->set('pstemporadas', $this->Pstemporada->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Pstemporada->exists($id)) {
			throw new NotFoundException(__('Invalid pstemporada'));
		}
		$options = array('conditions' => array('Pstemporada.' . $this->Pstemporada->primaryKey => $id));
		$this->set('pstemporada', $this->Pstemporada->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Pstemporada->create();
			if ($this->Pstemporada->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
		$tipotemporadas = $this->Pstemporada->Tipotemporada->find('list');
		$tipohabitaciones = $this->Pstemporada->Tipohabitacione->find('list');
		$this->set(compact('tipotemporadas', 'tipohabitaciones'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Pstemporada->exists($id)) {
			throw new NotFoundException(__('Invalid pstemporada'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Pstemporada->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Pstemporada.' . $this->Pstemporada->primaryKey => $id));
			$this->request->data = $this->Pstemporada->find('first', $options);
		}
		$tipotemporadas = $this->Pstemporada->Tipotemporada->find('list');
		$tipohabitaciones = $this->Pstemporada->Tipohabitacione->find('list');
		$this->set(compact('tipotemporadas', 'tipohabitaciones'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Pstemporada->id = $id;
		if (!$this->Pstemporada->exists()) {
			throw new NotFoundException(__('Invalid pstemporada'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Pstemporada->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
