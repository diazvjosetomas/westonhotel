<?php
App::uses('AppController', 'Controller');
/**
 * Pais Controller
 *
 * @property Pai $Pai
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PaisController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(13);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Pai->recursive = 0;
		//$this->set('pais', $this->Paginator->paginate());
		  $this->set('pais', $this->Pai->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Pai->exists($id)) {
			throw new NotFoundException(__('Invalid pai'));
		}
		$options = array('conditions' => array('Pai.' . $this->Pai->primaryKey => $id));
		$this->set('pai', $this->Pai->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Pai->create();
			if ($this->Pai->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Pai->exists($id)) {
			throw new NotFoundException(__('Invalid pai'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Pai->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Pai.' . $this->Pai->primaryKey => $id));
			$this->request->data = $this->Pai->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Pai->id = $id;
		if (!$this->Pai->exists()) {
			throw new NotFoundException(__('Invalid pai'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Pai->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
