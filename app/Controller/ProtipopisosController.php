<?php
App::uses('AppController', 'Controller');
/**
 * Protipopisos Controller
 *
 * @property Protipopiso $Protipopiso
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ProtipopisosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession();
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Protipopiso->recursive = 0;
		//$this->set('protipopisos', $this->Paginator->paginate());
		  $this->set('protipopisos', $this->Protipopiso->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Protipopiso->exists($id)) {
			throw new NotFoundException(__('Invalid protipopiso'));
		}
		$options = array('conditions' => array('Protipopiso.' . $this->Protipopiso->primaryKey => $id));
		$this->set('protipopiso', $this->Protipopiso->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Protipopiso->create();
			if ($this->Protipopiso->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Protipopiso->exists($id)) {
			throw new NotFoundException(__('Invalid protipopiso'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Protipopiso->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Protipopiso.' . $this->Protipopiso->primaryKey => $id));
			$this->request->data = $this->Protipopiso->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Protipopiso->id = $id;
		if (!$this->Protipopiso->exists()) {
			throw new NotFoundException(__('Invalid protipopiso'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Protipopiso->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
