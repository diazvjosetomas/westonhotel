<?php
App::uses('AppController', 'Controller');
/**
 * Facturatipopagos Controller
 *
 * @property Facturatipopago $Facturatipopago
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class FacturatipopagosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(8);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Facturatipopago->recursive = 0;
		//$this->set('facturatipopagos', $this->Paginator->paginate());
		  $this->set('facturatipopagos', $this->Facturatipopago->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Facturatipopago->exists($id)) {
			throw new NotFoundException(__('Invalid facturatipopago'));
		}
		$options = array('conditions' => array('Facturatipopago.' . $this->Facturatipopago->primaryKey => $id));
		$this->set('facturatipopago', $this->Facturatipopago->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Facturatipopago->create();
			if ($this->Facturatipopago->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Facturatipopago->exists($id)) {
			throw new NotFoundException(__('Invalid facturatipopago'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Facturatipopago->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Facturatipopago.' . $this->Facturatipopago->primaryKey => $id));
			$this->request->data = $this->Facturatipopago->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Facturatipopago->id = $id;
		if (!$this->Facturatipopago->exists()) {
			throw new NotFoundException(__('Invalid facturatipopago'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Facturatipopago->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
