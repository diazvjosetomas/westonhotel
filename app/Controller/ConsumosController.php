<?php
App::uses('AppController', 'Controller');
/**
 * Consumos Controller
 *
 * @property Consumo $Consumo
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ConsumosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

	var $uses = array('Consumo', 'Consumoproducto', 'Reserindividuale', 'Pstemporada', 'Proproductos',
		              'Tipotemporada', 'Habitacione', 'Resermulhabitacione', 'Resermultiple');
/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(7);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Consumo->recursive = 3;
		//$this->set('consumos', $this->Paginator->paginate());
		  $this->set('consumos', $this->Consumo->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Consumo->exists($id)) {
			throw new NotFoundException(__('Invalid consumo'));
		}
		$options = array('conditions' => array('Consumo.' . $this->Consumo->primaryKey => $id));
		$this->set('consumo', $this->Consumo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->request->data['Consumo']['fecha_consumo'] = !empty($this->request->data['Consumo']['fecha_consumo1']) ? formatfecha($this->request->data['Consumo']['fecha_consumo1']) : '';
			$this->Consumo->create();
			$this->Consumo->begin();
			if ($this->Consumo->save($this->request->data)) {
				$id   =  $this->Consumo->id;
				$stop = 0;
				if(!empty($this->request->data["Consumo"]["Productos"])){
					foreach($this->request->data["Consumo"]["Productos"] as $producto){
						$this->request->data["Consumoproducto"]["consumo_id"] = $id;
						$this->request->data["Consumoproducto"]["proproducto_id"] = $producto['pro'];
						$this->request->data["Consumoproducto"]["cantidad"]       = $producto['cant'];
						$this->request->data["Consumoproducto"]["precio"]         = $producto['pre'];
						$this->request->data["Consumoproducto"]["total"]          = $producto['total'];
						$this->Consumoproducto->create();
						if ($this->Consumoproducto->save($this->request->data)){

						}else{
							$stop = 1;
						}
					}
				}
				if($stop==0){
                    $this->Consumo->commit();
                	$this->Flash->success(__('Registro Guardado.'));
					return $this->redirect(array('action' => 'index'));

                }else{
                	$this->Consumo->rollback();
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                }
			} else {
				$this->Consumo->rollback();
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
		$tipohabitaciones = $this->Consumo->Tipohabitacione->find('list');
		$habitaciones = $this->Consumo->Habitacione->find('list');
		$reserindividuales = $this->Consumo->Reserindividuale->find('list');
		$proproductos = $this->Proproductos->find('all');
		$this->set(compact('tipohabitaciones', 'habitaciones', 'reserindividuales', 'proproductos'));
	}


	/**
	 * add method
	 *
	 * @return void
	 */
		public function add_2($tipohabitacione, $habitacione, $reservacione) {
			if ($this->request->is('post')) {
				$this->Consumo->create();
				$this->Consumo->begin();
				if ($this->Consumo->save($this->request->data)) {
					$id   =  $this->Consumo->id;
					$stop = 0;
					if(!empty($this->request->data["Consumo"]["Productos"])){
						foreach($this->request->data["Consumo"]["Productos"] as $producto){
							$this->request->data["Consumoproducto"]["consumo_id"] = $id;
							$this->request->data["Consumoproducto"]["proproducto_id"] = $producto['pro'];
							$this->request->data["Consumoproducto"]["cantidad"]       = $producto['cant'];
							$this->request->data["Consumoproducto"]["precio"]         = $producto['pre'];
							$this->request->data["Consumoproducto"]["total"]          = $producto['total'];
							$this->Consumoproducto->create();
							if ($this->Consumoproducto->save($this->request->data)){

							}else{
								$stop = 1;
							}
						}
					}
					if($stop==0){
	                    $this->Consumo->commit();
	                	$this->Flash->success(__('Registro Guardado.'));
						return $this->redirect(array('action' => 'index','controller' =>'Reserindividuales'));

	                }else{
	                	$this->Consumo->rollback();
	                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
	                }
				} else {
					$this->Consumo->rollback();
					$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
				}
			}

			/**
			 * Busco el ID de la habitacion
			 */
			

			$sql = "SELECT * FROM consumos WHERE reserindividuale_id = '".$reservacione."' ORDER BY num_consumo DESC LIMIT 1";

			
			$data2hab = $this->Consumo->query($sql);

			

			if (isset($data2hab[0]['consumos']['num_consumo'])) {
				$data2hab = $data2hab[0]['consumos']['num_consumo'];

				$data2hab++;

				$this->set('num_consumo', $data2hab);
			}else{
					

				$this->set('num_consumo', 1);
			}

			



			$tipohabitaciones = $this->Consumo->Tipohabitacione->find('list',array('conditions'=>array('Tipohabitacione.id'=>$tipohabitacione)));
			$habitaciones = $this->Consumo->Habitacione->find('list',array('conditions'=>array('Habitacione.id'=>$habitacione)));
			$reserindividuales = $this->Consumo->Reserindividuale->find('list',array('conditions'=>array('Reserindividuale.id'=>$reservacione)));
			$proproductos = $this->Proproductos->find('all');
			$this->set(compact('tipohabitaciones', 'habitaciones', 'reserindividuales', 'proproductos'));
		}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Consumo->exists($id)) {
			throw new NotFoundException(__('Invalid consumo'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Consumo']['fecha_consumo'] = !empty($this->request->data['Consumo']['fecha_consumo1']) ? formatfecha($this->request->data['Consumo']['fecha_consumo1']) : '';
			$this->Consumo->begin();
			if ($this->Consumo->save($this->request->data)) {
				$this->Consumoproducto->deleteAll(array('Consumoproducto.consumo_id'=>$id));
				$stop = 0;
				if(!empty($this->request->data["Consumo"]["Productos"])){
					foreach($this->request->data["Consumo"]["Productos"] as $producto){
						$this->request->data["Consumoproducto"]["consumo_id"] = $id;
						$this->request->data["Consumoproducto"]["proproducto_id"] = $producto['pro'];
						$this->request->data["Consumoproducto"]["cantidad"]       = $producto['cant'];
						$this->request->data["Consumoproducto"]["precio"]         = $producto['pre'];
						$this->request->data["Consumoproducto"]["total"]          = $producto['total'];
						$this->Consumoproducto->create();
						if ($this->Consumoproducto->save($this->request->data)){

						}else{
							$stop = 1;
						}
					}
				}
				if($stop==0){
                    $this->Consumo->commit();
                	$this->Flash->success(__('Registro Guardado.'));
					return $this->redirect(array('action' => 'index'));

                }else{
                	$this->Consumo->rollback();
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                    return $this->redirect(array('action' => 'edit/'.$id));
                }
			} else {
				$this->Consumo->rollback();
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Consumo.' . $this->Consumo->primaryKey => $id));
			$this->request->data = $this->Consumo->find('first', $options);

			$this->request->data['Consumo']['fecha_consumo1'] = formatdmy($this->request->data['Consumo']['fecha_consumo']);

		}
		$consumoproductos = $this->Consumoproducto->find('all', array('conditions'=>array('consumo_id'=>$id)));
		$tipohabitaciones = $this->Consumo->Tipohabitacione->find('list', array('conditions'=>array('Tipohabitacione.id'=>$this->request->data['Consumo']['tipohabitacione_id'])));
		$habitaciones = $this->Consumo->Habitacione->find('list', array('conditions'=>array('Habitacione.id'=>$this->request->data['Consumo']['habitacione_id'])));
		$reserindividuales = $this->Consumo->Reserindividuale->find('all', array('conditions'=>array('Reserindividuale.id'=>$this->request->data['Consumo']['reserindividuale_id'])));
		$proproductos    = $this->Proproductos->find('all');
		$this->set(compact('consumoproductos','proproductos', 'tipohabitaciones', 'habitaciones', 'reserindividuales'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null, $motivo = null) {

		$this->Consumo->id = $id;
		
		if (!$this->Consumo->exists()) {
			throw new NotFoundException(__('Invalid Consumo'));
		}

		$res = $this->Consumo->find('first', ['conditions' => ['Consumo.id' => $id], 'recursive' => 1 ]);

		

        $auditoria['Auditoria']['user_id']             = $this->Session->read('USUARIO_ID');
        $auditoria['Auditoria']['num_consumo']         = $res['Consumo']['num_consumo'];
        $auditoria['Auditoria']['habitacione_id']      = $res['Consumo']['habitacione_id'];
        $auditoria['Auditoria']['reserindividuale_id'] = $res['Consumo']['reserindividuale_id'];
        $auditoria['Auditoria']['resermultiple_id']    = 0;
        $auditoria['Auditoria']['cliente_id']          = !empty($res['Reserindividuale']['cliente_id']) ? $res['Reserindividuale']['cliente_id'] : 0;
        $auditoria['Auditoria']['factura_id']          = !empty($res['Factura']['id']) ? $res['Factura']['id'] : 0;
        $auditoria['Auditoria']['hora']                = DboSource::expression('NOW()');
        $auditoria['Auditoria']['motivo']              = $motivo;
        $auditoria['Auditoria']['tipo_operacion']      = 'ELIMINADO DE CONSUMO';
        $auditoria['Auditoria']['especifico']          = 'ELIMINADO';

        $this->loadModel('Auditoria');
        $this->Auditoria->save($auditoria);

        $rol = $this->Session->read('ROL');
        if($rol!=1){
                $this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
        }else{

				
				$this->request->allowMethod('post', 'delete', 'get');
				if ($this->Consumo->delete()) {
					$this->Flash->success(__('El Registro fue eliminado.'));
				} else {
					$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
				}
				return $this->redirect(array('action' => 'index'));
		}
	}


/**
 * habitacion method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function habitacion($id = null) {
		$this->layout = 'ajax';
	    $habitaciones = $this->Habitacione->find('list', array('fields'=>array('Habitacione.id', 'Habitacione.numhabitacion'), 'conditions'=>array('Habitacione.tipohabitacione_id'=>$id, 'Habitacione.habistatu_id'=>2)));
	    $this->set(compact('habitaciones'));
	    $this->set('id', $id);

	}

/**
 * cliente method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cliente($id = null, $id2 = null) {
		$this->layout = 'ajax';
	    $clientes = $this->Reserindividuale->find('all', array('conditions'=>array('Reserindividuale.tipohabitacione_id'=>$id, 'Reserindividuale.habitacione_id'=>$id2, 'Reserindividuale.reserstatusindividuale_id'=>4)));
	    $this->set(compact('clientes'));

	}


/**
 * precio_p method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function precio(){
		$this->layout = "ajax";
		$proproductos = $this->Proproductos->find('all', array('conditions'=>array('Proproductos.id'=>$this->request->data['id'])));
		$this->set('precio', isset($proproductos[0]['Proproductos']['pvp'])?$proproductos[0]['Proproductos']['pvp']:0);
	}


}
