<?php
App::uses('AppController', 'Controller');
/**
 * Habitaciones Controller
 *
 * @property Habitacione $Habitacione
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class HabitacionesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*** var de layout
*
*/
	public $layout = "dashbord";
	var $uses = array('Reserindividuale', 'Habitacione', 'Cliente', 'Consumo', 'Factura','Facturapago', 'Pai', 'Habistatu');

/*
*  *  beforeFilter check de session
*
*/	

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->set('habitaciones', $this->Paginator->paginate());
		$this->set('habitaciones', $this->Habitacione->find('all', array('order'=>array('Habitacione.numhabitacion'=>'asc'))));
		  
	}


/**
 * index method
 *
 * @return void
 */
	public function status() {

		$this->Habitacione->recursive = 1;
		 // $this->layout = 'ajax';
		
		 $this->set('habitaciones', $this->Habitacione->find('all', array('order'=>array('Habitacione.numhabitacion'=>'asc'))));
		 $this->set('habistatus', $this->Habistatu->find('all'));
		

		 $hab =  $this->Habitacione->find('all', array('order'=>array('Habitacione.numhabitacion'=>'asc')));

		 foreach ($hab as $key) {
		 	$array[] =  $key['Habitacione']['id'];
		 }

		 

		 
		 $fecha1 = date('Y-m-d');
		 $fecha2 = date('Y-m-d');


		$this->set('allhabitaciones',  $this->Reserindividuale->find('all',array('conditions'=>array('Reserindividuale.fecha_entrada <=' =>$fecha1,'Reserindividuale.fecha_salida >=' =>$fecha1))));
	}

	public function get_numero($idhab = null) {
		$r =  $this->Habitacione->find('first', [ 'conditions' => [ 'Habitacione.id' => $idhab ] ] );
		return !empty($r['Habitacione']['numhabitacion']) ? $r['Habitacione']['numhabitacion'] : '';
	}

	public function get_capacidad($idhab = null) {
		$r =  $this->Habitacione->find('first', [ 'conditions' => [ 'Habitacione.id' => $idhab ] ] );
		return $r['Habitacione']['capacidad'];
	}	


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function editstatus($id = null) {
		if (!$this->Habitacione->exists($id)) {
			throw new NotFoundException(__('Invalid habitacione'));
		}
		if ($this->request->is(array('post', 'put'))) {


			

			$desde = strpos($this->request->data['Habitacione']['desde'], '/');
			$hasta = strpos($this->request->data['Habitacione']['hasta'], '/');

			

			if ($desde > 0) {
				$convert = explode('/',$this->request->data['Habitacione']['desde']);
				$this->request->data['Habitacione']['desde'] = $convert[2].'-'.$convert[1].'-'.$convert[0];

				echo $this->request->data['Habitacione']['desde'];
			}

			if ($hasta > 0) {
				$convert = explode('/',$this->request->data['Habitacione']['hasta']);
				$this->request->data['Habitacione']['hasta'] = $convert[2].'-'.$convert[1].'-'.$convert[0];
				echo $this->request->data['Habitacione']['hasta'];
			}


			


			if ($this->Habitacione->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'status'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Habitacione.' . $this->Habitacione->primaryKey => $id));
			$this->request->data = $this->Habitacione->find('first', $options);
		}
		$tipohabitaciones = $this->Habitacione->Tipohabitacione->find('list', array('conditions'=>array('Tipohabitacione.id'=>$this->request->data['Habitacione']['tipohabitacione_id'] )) );
		$habistatus = $this->Habitacione->Habistatus->find('list');
		$this->set(compact('tipohabitaciones', 'habistatus'));

		$this->set('datafecha', $this->Habitacione->find('all', array('conditions'=>array('Habitacione.id'=>$id))));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Habitacione->exists($id)) {
			throw new NotFoundException(__('Invalid habitacione'));
		}
		$options = array('conditions' => array('Habitacione.' . $this->Habitacione->primaryKey => $id));
		$this->set('habitacione', $this->Habitacione->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {

			$c = $this->Habitacione->find('all', array('conditions'=>array('Habitacione.ref'=>$this->request->data['Habitacione']['ref'])));

			if (count($c) > 0) {
				$this->Flash->error(__('Esta referencia ya esta siendo usada, indica otra'));
			}else{





					$this->Habitacione->create();
					if ($this->Habitacione->save($this->request->data)) {
						$this->Flash->success(__('Registro Guardado.'));
						return $this->redirect(array('action' => 'index'));
					} else {
						$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
					}
			}
		}
		$tipohabitaciones = $this->Habitacione->Tipohabitacione->find('list');
		$this->set(compact('tipohabitaciones'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Habitacione->exists($id)) {
			throw new NotFoundException(__('Invalid habitacione'));
		}
		if ($this->request->is(array('post', 'put'))) {



			$c = $this->Habitacione->find('all', array('conditions'=>array('Habitacione.ref'=>$this->request->data['Habitacione']['ref'])));

			if (count($c) > 0) {
				$this->Flash->error(__('Esta referencia ya esta siendo usada, indica otra'));
			}else{





					$this->Habitacione->create();
					if ($this->Habitacione->save($this->request->data)) {
						$this->Flash->success(__('Registro Guardado.'));
						return $this->redirect(array('action' => 'index'));
					} else {
						$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
					}
			}



			
		} else {
			$options = array('conditions' => array('Habitacione.' . $this->Habitacione->primaryKey => $id));
			$this->request->data = $this->Habitacione->find('first', $options);
		}
		$tipohabitaciones = $this->Habitacione->Tipohabitacione->find('list');
		$this->set(compact('tipohabitaciones'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Habitacione->id = $id;
		if (!$this->Habitacione->exists()) {
			throw new NotFoundException(__('Invalid habitacione'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Habitacione->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
