<?php
App::uses('AppController', 'Controller');
/**
 * Banners Controller
 *
 * @property Banner $Banner
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class BannersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');
	public $layout = 'dashbord';
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Banner->recursive = 0;
		$this->set('banners', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Banner->exists($id)) {
			throw new NotFoundException(__('Invalid banner'));
		}
		$options = array('conditions' => array('Banner.' . $this->Banner->primaryKey => $id));
		$this->set('banner', $this->Banner->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {



			$this->Banner->create();


			if (!empty($this->request->data['Banner']['file']) && imagenType($this->request->data['Banner']['file']['type']) != false) {
				               
				               $ds = '/';
				               $storeFolder = 'upload';
				               $tempFile    = $this->request->data['Banner']['file']['tmp_name'];
				               $targetPath  = ROOT . $ds .'app'. $ds .'webroot'. $ds .'img'. $ds . $storeFolder . $ds;
				               $extencion   = imagenType($this->request->data['Banner']['file']['type']);


				               $renameFile  = 'a_'.date('dmY_his').$extencion;
				               $targetFile  = $targetPath.$renameFile;
				               if(move_uploaded_file($tempFile,$targetFile)){
				                   $tipo_file = $this->request->data['Banner']['file']['type'];
				                   $nombre_file = $renameFile;;
				                   
				                   $this->request->data['Banner']['file']    = $nombre_file;
				               }else{
				                   
				                   $this->request->data['Banner']['file'] = "_";
				               }
				               if ($extencion != '.jpeg') {
					               	if ($extencion != '.png') {
					               		if ( $extencion != '.jpg') {
					               			
					               		$this->Flash->error(__('Registro no Guardado. Por favor agregue una file con extención JPG, JPEG o PNG.'.$extencion));
					                  	return $this->redirect(array('controller' => 'Banners','action' => 'index'));			 			
					               		}else{

					               		}
					               		
					               	}else{

				               	}

				               }else{

				               }

				           }else{
				           	$this->request->data['Banner']['file'] = '*';


				           }





			if ($this->Banner->save($this->request->data)) {
				$this->Flash->success(__('The banner has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The banner could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Banner->exists($id)) {
			throw new NotFoundException(__('Invalid banner'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Banner->save($this->request->data)) {
				$this->Flash->success(__('The banner has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The banner could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Banner.' . $this->Banner->primaryKey => $id));
			$this->request->data = $this->Banner->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Banner->id = $id;
		if (!$this->Banner->exists()) {
			throw new NotFoundException(__('Invalid banner'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Banner->delete()) {
			$this->Flash->success(__('The banner has been deleted.'));
		} else {
			$this->Flash->error(__('The banner could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
