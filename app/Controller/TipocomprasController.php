<?php
App::uses('AppController', 'Controller');
/**
 * Tipocompras Controller
 *
 * @property Tipocompra $Tipocompra
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class TipocomprasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(5);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Tipocompra->recursive = 0;
		//$this->set('tipocompras', $this->Paginator->paginate());
		  $this->set('tipocompras', $this->Tipocompra->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Tipocompra->exists($id)) {
			throw new NotFoundException(__('Invalid tipocompra'));
		}
		$options = array('conditions' => array('Tipocompra.' . $this->Tipocompra->primaryKey => $id));
		$this->set('tipocompra', $this->Tipocompra->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Tipocompra->create();
			if ($this->Tipocompra->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Tipocompra->exists($id)) {
			throw new NotFoundException(__('Invalid tipocompra'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Tipocompra->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Tipocompra.' . $this->Tipocompra->primaryKey => $id));
			$this->request->data = $this->Tipocompra->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Tipocompra->id = $id;
		if (!$this->Tipocompra->exists()) {
			throw new NotFoundException(__('Invalid tipocompra'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Tipocompra->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
