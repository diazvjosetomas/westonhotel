<?php
App::uses('AppController', 'Controller');
/**
 * Impresiones Controller
 *
 * @property Impresione $Impresione
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ImpresionesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(13);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Impresione->recursive = 0;
		//$this->set('impresiones', $this->Paginator->paginate());
		  $this->set('impresiones', $this->Impresione->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Impresione->exists($id)) {
			throw new NotFoundException(__('Invalid impresione'));
		}
		$options = array('conditions' => array('Impresione.' . $this->Impresione->primaryKey => $id));
		$this->set('impresione', $this->Impresione->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Impresione->create();
			if ($this->Impresione->save($this->request->data)) {
				$this->Flash->success(__('The impresione has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The impresione could not be saved. Please, try again.'));
			}
		}
		$tipoimpresoras = $this->Impresione->Tipoimpresora->find('list');
		$this->set(compact('tipoimpresoras'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Impresione->exists($id)) {
			throw new NotFoundException(__('Invalid impresione'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Impresione->save($this->request->data)) {
				$this->Flash->success(__('The impresione has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The impresione could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Impresione.' . $this->Impresione->primaryKey => $id));
			$this->request->data = $this->Impresione->find('first', $options);
		}
		$tipoimpresoras = $this->Impresione->Tipoimpresora->find('list');
		$this->set(compact('tipoimpresoras'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Impresione->id = $id;
		if (!$this->Impresione->exists()) {
			throw new NotFoundException(__('Invalid impresione'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Impresione->delete()) {
			$this->Flash->success(__('The impresione has been deleted.'));
		} else {
			$this->Flash->error(__('The impresione could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
