<?php
App::uses('AppController', 'Controller');
/**
 * Reserindivistatuses Controller
 *
 * @property Reserindivistatus $Reserindivistatus
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ReservasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

	var $uses = array('Reserindivistatuses','Reserindivistatus', 'Reserindividuale', 'Habitacione','Resermultiple', 'Pstemporada', 'Tipotemporada', 'Habitacione','Resermulhabitacione', 'Reserindividuale', 'Pai','Empresa','Cliente');



/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(6);
	}


	public function reservas($id=null) {
		$this->layout ='dashbord';
		$this->set('resermultiples', $this->Resermultiple->find('all'));
		if($id!=null && $id!=0){$this->set('id_reporte', $id);}
	}

	public function baja_reservas() {
		$this->layout ='dashbord';
		$this->loadModel('Auditoria');
		$this->set('reservas', $this->Auditoria->find('all', [
					'conditions' => [
						'OR' => [
							[ 'Auditoria.especifico' => 'EGRESADA' ],
							[ 'Auditoria.tipo_operacion like' => '%ELIMINADO%' ],
							[ 'Auditoria.especifico like' => '%ELIMINADO' ]

						],
						'Auditoria.tipo_operacion not' => 'ELIMINADO DE CONSUMO'
					]
				]
			)
		);
	}

	public function baja_consumos() {
		$this->layout ='dashbord';
		$this->loadModel('Auditoria');
		$this->set('consumos', $this->Auditoria->find('all', [
					'conditions' => [
						 'Auditoria.tipo_operacion' => 'ELIMINADO DE CONSUMO' ,

					]
				]
			)
		);
	}

	public function reservasajax($id=null) {
		$this->layout ='ajax';
		$this->set('resermultiples', $this->Resermultiple->find('all'));
		if($id!=null && $id!=0){$this->set('id_reporte', $id);}
	}


	public function mes_($dia, $mes, $anio){

		set_time_limit(600);
		ini_set('memory_limit', '-1');
		
		$array[] ='0';

		$this->layout ='ajax';
		$this->set('dia', $dia);
		$this->set('mes', $mes);
		$this->set('anio', $anio);

		$a = $anio.'-'.$mes.'-01';
		$b = $anio.'-'.$mes.'-31';

		$this->Reserindividuale->recursive = 1;
		/*$entrada1 =  $this->Reserindividuale->find('all', array('conditions' => array(
			 				'OR' => array(
			 					array('Reserindividuale.fecha_entrada >=' => $a), 
			 					array('Reserindividuale.fecha_entrada <=' => $b)
			 				)
			 			),
			 			'order' => array(
			 				'Reserindividuale.fecha_entrada'=>'ASC'
			 				)
			 			)
			 		);*/
		$entrada1 = $this->Reserindividuale->find('all', array('conditions' => array('Reserindividuale.fecha_entrada >= ' => $a, 'Reserindividuale.fecha_salida <= '.$b )));
		//var_dump($entrada1);

		foreach ($entrada1 as $key) {
			$array[] = $key['Reserindividuale']['id'];
		}

		//echo $array[0];

		
		if ($array[0] == '0') {
			$entrada2 = $this->Reserindividuale->find('all', 
			 		array('conditions'=>array('Reserindividuale.fecha_salida >='=>$a, 
			 			'Reserindividuale.fecha_salida <='=>$b), 'order'=>array('Reserindividuale.fecha_entrada'=>'ASC') )  );
		}else{

		$entrada2 = $this->Reserindividuale->find('all', 
			 		array('conditions'=>array('Reserindividuale.fecha_salida >='=>$a, 
			 			'Reserindividuale.fecha_salida <='=>$b, 'Reserindividuale.id != '=>$array), 'order'=>array('Reserindividuale.fecha_entrada'=>'ASC')));

		}


		$habitaciones = $this->Habitacione->find('all', array('conditions'=>array('Habitacione.desde >='.$a, 'Habitacione.hasta <= '.$b)));

		$this->set('entrada1', array_merge($entrada1, $entrada2, $habitaciones));


		//Trayendo todas las habitaciones
		$this->Habitacione->recursive = 2;
		$this->set('habitaciones', $this->Habitacione->find('all', array('order'=>'numhabitacion ASC')));

		$this->set('mes_actual', $mes);

		/*$mes_a = (($mes < 10) ? '0'.$mes : $mes);

		$multiples = $this->Resermultiple->find('all', [
			'conditions' => [
				'OR' =>
					[
						[ 'Resermultiple.fecha_entrada like' => $anio.'-'.$mes_a.'%' ],
						[ 'Resermultiple.fecha_salida like'  => $anio.'-'.$mes_a.'%' ]
					]
				]
			]
		);*/

		
		
		/*foreach ($multiples as $key => $value) {
			$resermulthabs = $multiples[$key]['Resermulhabitacione'];
			foreach ($resermulthabs as $key2 => $value) {
				$dia_e = explode('-', $multiples[$key]['Resermultiple']['fecha_entrada']);
				$dia_e = $dia_e[2];

				$dia_s = explode('-', $multiples[$key]['Resermultiple']['fecha_salida']);
				$dia_s = $dia_s[2];

				$reserva_multiple[$key]['Resermultiple']['id']             = !empty($resermulthabs[$key2]['resermultiple_id']) ? $resermulthabs[$key2]['resermultiple_id'] : '';
				$reserva_multiple[$key]['Resermultiple']['habitacione_id'] = !empty($resermulthabs[$key2]['habitacione_id']) ? $resermulthabs[$key2]['habitacione_id'] : '';
				$reserva_multiple[$key]['Resermultiple']['numerohab']      = $this->requestAction('habitaciones/get_numero/'.$resermulthabs[$key2]['habitacione_id']);
				$reserva_multiple[$key]['Resermultiple']['dia_entrada']    = $dia_e;
				$reserva_multiple[$key]['Resermultiple']['dia_salida']     = $dia_s;
			}
			
		}*/

		//pr($reserva_multiple);
		//exit();

		// $this->set(compact('reserva_multiple'));

		

		// $this->set('entrada2', $this->Reserindividuale->find('all', array('conditions'=>array('Reserindividuale.fecha_salida >='=>$a, 'Reserindividuale.fecha_salida <='=>$b))));		
		// $this->set('entrada3', $this->Resermultiple->find('all', array('conditions'=>array('Resermultiple.fecha_salida >='=>$a, 'Resermultiple.fecha_salida <='=>$b))));
		// $this->set('entrada4', $this->Resermultiple->find('all', array('conditions'=>array('Resermultiple.fecha_salida >='=>$a, 'Resermultiple.fecha_salida <='=>$b))));
			


		//	$tipotemporadas_e = $this->Reserindividuale->find('all', array('conditions'=>array('Reserindividuale.fecha_entrada <='=>$a, 'Reserindividuale.fecha_entrada >='=>$b)));



	//$tipotemporadas_e_m = $this->Resermulhabitacione->find('all', array('conditions'=>array('Resermultiple.fecha_salida <='=>$a, 'Resermultiple.fecha_salida >='=>$b)));
	}



	public function mes($dia, $mes, $anio){

		set_time_limit(600);
		ini_set('memory_limit', '-1');
		
		$array[] ='0';

		$this->layout ='ajax';
		$this->set('dia', $dia);
		$this->set('mes', $mes);
		$this->set('anio', $anio);

		$a = $anio.'-'.$mes.'-01';
		$b = $anio.'-'.$mes.'-31';

		//$this->Reserindividuale->recursive = 1;

		$habitaciones = $this->Habitacione->find('all', array('conditions'=>array('Habitacione.desde >='.$a, 'Habitacione.hasta <= '.$b)));



		//$response = $this->Login->find('all', array('order'=>array('Login.profile_type'=>'desc')));


		// INSIDE 
		$inside = $this->Reserindividuale->find('all', array(
		'conditions' => array(
			'Reserindividuale.reserstatusindividuale_id != ' => 6,
		'Reserindividuale.fecha_entrada >=' => $a, 
		'Reserindividuale.fecha_salida <='  => $b
		),
		'order' => array( 'Reserindividuale.fecha_entrada' => 'asc' )
		));
		

		// OUTSIDE
		$outside = $this->Reserindividuale->find('all', 
			array('conditions'=>array(
				'Reserindividuale.reserstatusindividuale_id != ' => 6,
				'Reserindividuale.fecha_entrada < '=>$a, 
				'Reserindividuale.fecha_salida > '=>$b),
		'order' => array( 'Reserindividuale.fecha_entrada' => 'asc' )
		));
		
		// LEFTSIDE
		$leftside = $this->Reserindividuale->find('all', 
			array('conditions'=>array(
				'Reserindividuale.reserstatusindividuale_id != ' => 6,
				'Reserindividuale.fecha_entrada < '=>$a, 
				'Reserindividuale.fecha_salida <= '=>$b),
		'order' => array( 'Reserindividuale.fecha_entrada' => 'asc' )
		));
		

		// RIGHTSIDE
		$rightside = $this->Reserindividuale->find('all', 
			array('conditions'=>array(
				'Reserindividuale.reserstatusindividuale_id != ' => 6,
				'Reserindividuale.fecha_entrada >= '=>$a, 
				'Reserindividuale.fecha_salida > '=>$b),
		'order' => array( 'Reserindividuale.fecha_entrada' => 'asc' )
		));
		




		$this->set('entrada1', array_merge($inside, $outside, $leftside, $rightside));
		$var = array_merge($inside);



		//Trayendo todas las habitaciones
		$this->Habitacione->recursive = 2;
		$this->set('habitaciones', $this->Habitacione->find('all', array('order'=>'numhabitacion ASC')));

		$this->set('mes_actual', $mes);


		$this->set('controlfechas', $this->Habitacione->query('select r.id, r.fecha_entrada, h.numhabitacion from reserindividuales r, habitaciones h where r.habitacione_id = h.id order by r.fecha_entrada ASC'));
	}




















	public function agregar($id = null){
				if ($this->request->is('post')) {
					$this->Reserindividuale->create();
					$this->Reserindividuale->begin();
					if ($this->Reserindividuale->save($this->request->data)) {
		                   // $this->request->data['Habitacione']['id']           = $this->request->data['Reserindividuale']['habitacione_id'];
		                   // $this->request->data['Habitacione']['habistatu_id'] = 2;
		                //if ($this->Habitacione->save($this->request->data)) {
						$id   =  $this->Reserindividuale->id;
						$stop = 0;
						if(!empty($this->request->data["Reserindividuale"]["Nombre"])){
							foreach($this->request->data["Reserindividuale"]["Nombre"] as $nombre){
								    $this->request->data["Reserindividualeextra"]["reserindividuale_id"] = $id;
								    $this->request->data["Reserindividualeextra"]["nombres"] = $nombre;
								    $this->Reserindividualeextra->create();
								if ($this->Reserindividualeextra->save($this->request->data)){

								}else{
									$stop = 1;
								}
							}
						}
						if($stop==0){
		                    $this->Reserindividuale->commit();
		                    $empresas = $this->Empresa->find('all');
							$clientes = $this->Cliente->find('all', array('conditions'=>array('Cliente.id'=>$this->request->data['Reserindividuale']['cliente_id'])));
							if(isset($empresas[0]['Empresa']['correo_smtp'])){
			                    //EMAIL AQUI
				                App::import('Vendor', 'PHPMailer', array('file' => 'phpmailer/class.phpmailer.php'));
						        $mail_r = new PHPMailer();
						        $mail_r->IsHTML(true);
						        $mail_r->IsSMTP();
								$mail_r->SMTPAuth   = true;                  // enable SMTP authentication
								$mail_r->SMTPSecure = "ssl";                 // sets the prefix to the servier
								$mail_r->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
								$mail_r->Port       = 465;                   // set the SMTP port for the GMAIL server
								$mail_r->Username   = $empresas[0]['Empresa']['correo_smtp'];  // GMAIL username
								$mail_r->Password   = $empresas[0]['Empresa']['clave_smtp'];   // GMAIL password
						        $mail_r->From       = $empresas[0]['Empresa']['correo_smtp'];
							    $mail_r->FromName   = $empresas[0]['Empresa']['razon_social'];
							    $mail_r->AddAddress($clientes[0]['Cliente']['correo'], $clientes[0]['Cliente']['nombre_completo']);
							    $mail_r->Subject  = "Confirmacion de la reserva";
							    $email_a_revista  = "<table class='shop-item-selections'>
							      <tr>
							          <td width='100%'>
		                                  Saludos Estimado ".$clientes[0]['Cliente']['nombre_completo']." 
		                                  <br>  
		                                  El siguiente correo es un enlace de confirmación por el registro de su reserva  en ".$empresas[0]['Empresa']['razon_social']."
		                                  con fecha de entrada ".$this->request->data['Reserindividuale']['fecha_entrada']." y fecha de salida ".$this->request->data['Reserindividuale']['fecha_salida']."
		                                  <br>
		                                  Gracias por Preferirnos!!!               
							          </td>
							        </tr>
							      </table>";
							    $mail_r->MsgHTML($email_a_revista);
							    $mail_r->Send();
						    }	
		                	$this->Flash->success(__('Registro Guardado.'));
							return $this->redirect('Reservas/reservas');
						}else{
							$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
						}
		                /*}else{
		                	$this->Reserindividuale->rollback();
		                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
		                }*/
					} else {
						$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
					}
				}



				$tipoclientes = $this->Reserindividuale->Tipocliente->find('list');
				$tipoclientesubs = $this->Reserindividuale->Tipoclientesub->find('list');
				$clientes = $this->Reserindividuale->Cliente->find('list');
				$habitaciones = $this->Reserindividuale->Habitacione->find('list', array('conditions'=>array('Habitacione.id'=> $id)));
				$habitacion = $this->Habitacione->find('all', array('conditions'=>array('Habitacione.id'=> $id)));
				
				$idTipoHab = $habitacion[0]['Habitacione']['tipohabitacione_id'];

				$tipohabitaciones = $this->Reserindividuale->Tipohabitacione->find('list', array('conditions'=>array('Tipohabitacione.id'=>$idTipoHab)));

				$reserstatusindividuales = $this->Reserindividuale->Reserstatusindividuale->find('list');
				$resermultiples = $this->Reserindividuale->Resermultiple->find('list');
				$pais = $this->Pai->find('list');
				$this->set(compact('pais','tipoclientes', 'tipoclientesubs', 'clientes', 'tipohabitaciones', 'habitaciones', 'reserstatusindividuales', 'resermultiples'));

	}



	public function status($id = null) {
		return $this->redirect('/Reserindivistatuses/add_2/'.$id);
	}

}