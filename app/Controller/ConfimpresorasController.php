<?php
App::uses('AppController', 'Controller');
/**
 * Confimpresoras Controller
 *
 * @property Confimpresora $Confimpresora
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ConfimpresorasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(1);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Confimpresora->recursive = 0;
		//$this->set('confimpresoras', $this->Paginator->paginate());
		  $this->set('confimpresoras', $this->Confimpresora->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Confimpresora->exists($id)) {
			throw new NotFoundException(__('Invalid confimpresora'));
		}
		$options = array('conditions' => array('Confimpresora.' . $this->Confimpresora->primaryKey => $id));
		$this->set('confimpresora', $this->Confimpresora->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Confimpresora->create();
			if ($this->Confimpresora->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Confimpresora->exists($id)) {
			throw new NotFoundException(__('Invalid confimpresora'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Confimpresora->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Confimpresora.' . $this->Confimpresora->primaryKey => $id));
			$this->request->data = $this->Confimpresora->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Confimpresora->id = $id;
		if (!$this->Confimpresora->exists()) {
			throw new NotFoundException(__('Invalid confimpresora'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Confimpresora->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
