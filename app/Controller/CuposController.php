<?php
App::uses('AppController', 'Controller');
/**
 * Cupos Controller
 *
 * @property Cupo $Cupo
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class CuposController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');
	public $layout = "dashbord";
	public $uses = array('Tipohabitacione','Cupo','Tipotemporada','Setting');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Cupo->recursive = 0;
		$this->set('cupos', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Cupo->exists($id)) {
			throw new NotFoundException(__('Invalid cupo'));
		}
		$options = array('conditions' => array('Cupo.' . $this->Cupo->primaryKey => $id));
		$this->set('cupo', $this->Cupo->find('first', $options));
	}
    
    
    public function rangofechas(){
        $this->layout = "ajax";
        
        $preciopaxadicfec = $this->request->data['preciopaxadicfec'];
        $idtipohabitacion = $this->request->data['idtipohabitacion'];
        $idtipotemporada  = $this->request->data['idtipotemporada'];
        $cantidadcupos    = $this->request->data['cantidadcupos'];
        $diainicio        = $this->request->data['diainicio'];
        $preciopax        = $this->request->data['preciopax'];
        $diafin           = $this->request->data['diafin'];
        $anhio            = $this->request->data['anhio'];
        $mes              = $this->request->data['mes'];
        
        
        //Parseando la data 
        
        
        

        
        for($i = $diainicio; $i <= $diafin; $i++){
            
            
            
            //Consultamos si una fecha ya esta colocada
            $fechacompuesta = $anhio.'-'.$mes.'-'.$i;
            
            $c = $this->Cupo->find('all', ['conditions'=>[ 'Cupo.fecha'              => $fechacompuesta,
                                                           'Cupo.tipohabitacione_id' => $idtipohabitacion,
                                                           'Cupo.tipotemporada_id'   => $idtipotemporada
                                                         ] ]);
                
                
            if(count($c) > 0 ){

            	$c[0]['Cupo']['id'];
                $this->Cupo->create();
                            
                                $this->request->data['Cupo']['id']                 = $c[0]['Cupo']['id'];
                                $this->request->data['Cupo']['tipohabitacione_id'] = $idtipohabitacion;
                                $this->request->data['Cupo']['preciopaxadicional'] = $preciopaxadicfec;
                                $this->request->data['Cupo']['tipotemporada_id']   = $idtipotemporada;
                                $this->request->data['Cupo']['cantidad']           = $cantidadcupos;
                                $this->request->data['Cupo']['precio']             = $preciopax;
                            
                            
                            
                            
                                $this->request->data['Cupo']['fecha'] = $anhio.'-'.$mes.'-'.$i;
                            
                                $this->Cupo->save($this->request->data);
                
            }else if(count($c) == 0){
                
                $this->Cupo->create();
            
                $this->request->data['Cupo']['tipohabitacione_id'] = $idtipohabitacion;
                $this->request->data['Cupo']['preciopaxadicional'] = $preciopaxadicfec;
                $this->request->data['Cupo']['tipotemporada_id']   = $idtipotemporada;
                $this->request->data['Cupo']['cantidad']           = $cantidadcupos;
                $this->request->data['Cupo']['precio']             = $preciopax;
            
            
            
            
                $this->request->data['Cupo']['fecha'] = $anhio.'-'.$mes.'-'.$i;
            
                $this->Cupo->save($this->request->data);
                
            }
            
            
        }
    }

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Cupo->create();
			if ($this->Cupo->save($this->request->data)) {
				$this->Flash->success(__('The cupo has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The cupo could not be saved. Please, try again.'));
			}
		}
		$tipohabitaciones = $this->Cupo->Tipohabitacione->find('list');
		$this->set(compact('tipohabitaciones'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Cupo->exists($id)) {
			throw new NotFoundException(__('Invalid cupo'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Cupo->save($this->request->data)) {
				$this->Flash->success(__('The cupo has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The cupo could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Cupo.' . $this->Cupo->primaryKey => $id));
			$this->request->data = $this->Cupo->find('first', $options);
		}
		$tipohabitaciones = $this->Cupo->Tipohabitacione->find('list');
		$this->set(compact('tipohabitaciones'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Cupo->id = $id;
		if (!$this->Cupo->exists()) {
			throw new NotFoundException(__('Invalid cupo'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Cupo->delete()) {
			$this->Flash->success(__('The cupo has been deleted.'));
		} else {
			$this->Flash->error(__('The cupo could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


	public function modografico(){

		if ($this->request->data['mes'] < 10) {
			$mes = '0'.$this->request->data['mes'];
		}else{
			$mes = $this->request->data['mes'];
		}

		$this->set('mescontroller'  , $mes);
		$this->set('anhocontroller' , $this->request->data['anhoselect']);
		$this->set('selecttipotemporadas' , $this->request->data['tipotemporadas']);




		$this->set('tipohab', $this->Tipohabitacione->find('all'));
		$this->set('tipotemporadas', $this->Tipotemporada->find('all'));

	}

	public function getdata($mes = null, $anho = null, $idtemporada = null){

		$this->layout = "ajax";

		if ($mes < 10) {
			$mes = '0'.$mes;
		}
			
		if ( ( $mes == null ) AND  ( $anho == null ) ) {
			$fechainicio = date('Y').'-'.date('m').'-'.'01';
			$fechafin    = date('Y').'-'.date('m').'-'.'31';

		}else{
			
			$fechainicio = $anho."-".$mes."-"."01";	
			$fechafin    = $anho."-".$mes."-"."31";	

		}
		$this->set('data',  $this->Cupo->find('all', 
												array('conditions'=>
													array('Cupo.fecha >= ' 	       => $fechainicio, 
														  'Cupo.fecha <= ' 		   => $fechafin,
														  'Cupo.tipotemporada_id ' => $idtemporada))));


	}


	public function addcupos(){
		$this->layout = "ajax";
		echo  $this->request->data['cupo'];
		$fecha = $this->request->data['fecha'];


		//* Consulto si no esta guardado ese dato para esa fecha
		

		$tipohabitacione_id = $this->request->data['th'];
		$cupo  				= $this->request->data['cupo'];
		$idtemporada   		= $this->request->data['idtemporada'];

		$c  = $this->Cupo->find('all', 
								array('conditions'=> 
									array(
										  'Cupo.tipohabitacione_id' => $tipohabitacione_id, 
										  'Cupo.fecha'			    => $fecha, 
										  'Cupo.tipotemporada_id'   => $idtemporada)));


		if (count($c) == 0) {
			
			//Caso de no encontrar datos para este id y fecha

			//Seteamos los datos para pasarlos a la base de datos
			$this->Cupo->create();

			$this->request->data['Cupo']['fecha']              = $fecha;	
			$this->request->data['Cupo']['tipohabitacione_id'] = $tipohabitacione_id;
			$this->request->data['Cupo']['cantidad'] 		   = $cupo;
			$this->request->data['Cupo']['precio'] 			   = 0.0;
			$this->request->data['Cupo']['tipotemporada_id']   = $idtemporada;

			//var_dump($this->request->data);
			//Salvamos los datos
			if ( $this->Cupo->save($this->request->data) ){
				return 1;
				
			}else{
				return 0;
				
			}
			
		}else if (count($c) > 0) {
			$datos = $this->Cupo->find('all', 
											array('conditions'=> 
												array(
													  'Cupo.tipohabitacione_id' => $tipohabitacione_id, 
													  'Cupo.fecha'	            => $fecha, 
													  'Cupo.tipotemporada_id'   => $idtemporada 
													)
											)
										);



			$id    = $datos[0]['Cupo']['id'];

			$sql   = "UPDATE cupos SET cantidad = '$cupo' WHERE id = $id ";

			$this->Cupo->query($sql);



		}


}	


		public function addprecios(){
			$this->layout = "ajax";

			//* Consulto si no esta guardado ese dato para esa fecha		


			$tipohabitacione_id     = $this->request->data['th'];
			$precio  				= $this->request->data['precio'];
			$precioadicional        = $this->request->data['preciopaxadicional'];
			$fecha 					= $this->request->data['fecha'];
			$idtemporada   		    = $this->request->data['idtemporada'];


			$c  = $this->Cupo->find('all', 
									array('conditions'=> 
										array(
												'Cupo.fecha' 			  => $this->request->data['fecha'], 
												'Cupo.tipohabitacione_id' => $tipohabitacione_id, 
												'Cupo.tipotemporada_id'   => $idtemporada )));


			if (count($c) == 0) {
				//Caso de no encontrar datos para este id y fecha

				//Seteamos los datos para pasarlos a la base de datos
				$this->Cupo->create();

				$this->request->data['Cupo']['fecha']              = $fecha;	
				$this->request->data['Cupo']['tipohabitacione_id'] = $tipohabitacione_id;
				$this->request->data['Cupo']['cantidad'] 		   = 0;
				$this->request->data['Cupo']['precio'] 			   = $precio;
				$this->request->data['Cupo']['precioadicional']    = $precioadicional;
				$this->request->data['Cupo']['tipotemporada_id']   = $idtemporada;


				//var_dump($this->request->data);
				//Salvamos los datos
				if ( $this->Cupo->save($this->request->data) ){
					return 1;
					
				}else{
					return 0;
					
				}
				
			}else if (count($c) > 0) {
				$datos = $this->Cupo->find('all', array('conditions'=> 
													array(
														'Cupo.tipohabitacione_id' => $tipohabitacione_id, 
														'Cupo.fecha'			  => $fecha,
														'Cupo.tipotemporada_id'   => $idtemporada  )));
				$id    = $datos[0]['Cupo']['id'];
				$sql   = "UPDATE cupos SET precio = '$precio' , preciopaxadicional = '$precioadicional' WHERE id = $id ";

				$this->Cupo->query($sql);



			}




		
		

	}



	
	public function getdatadefault(){

		$this->layout = "ajax";

	
		$this->set('data',  $this->Setting->find('all'));


	}

	public function updatedefault(){
		$this->layout = "ajax";

		$cupodef          = $this->request->data['cupodef'];
		$preciopaxdef     = $this->request->data['preciopaxdef'];
		$preciopaxadicdef = $this->request->data['preciopaxadicdef'];

		$sqlupdate = "UPDATE settings SET cupos =  ".$cupodef.", preciopax = ".$preciopaxdef.", preciopaxadicional = ".$preciopaxadicdef."  ";


		if ($cupodef != '') {
			$sqlupdate = "UPDATE settings SET cupos =  ".$cupodef." ";
			$this->Setting->query($sqlupdate);
			
		}

		if ($preciopaxdef != '') {
			$sqlupdate = "UPDATE settings SET preciopax =  ".$preciopaxdef." ";
			$this->Setting->query($sqlupdate);
			
		}

		if ($preciopaxadicdef != '') {
			$sqlupdate = "UPDATE settings SET preciopaxadicional =  ".$preciopaxadicdef." ";
			$this->Setting->query($sqlupdate);
			
		}





	}

	}
