<?php
App::uses('AppController', 'Controller');
/**
 * Reserstatusmultiples Controller
 *
 * @property Reserstatusmultiple $Reserstatusmultiple
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ReserstatusmultiplesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(6);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Reserstatusmultiple->recursive = 0;
		//$this->set('reserstatusmultiples', $this->Paginator->paginate());
		  $this->set('reserstatusmultiples', $this->Reserstatusmultiple->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Reserstatusmultiple->exists($id)) {
			throw new NotFoundException(__('Invalid reserstatusmultiple'));
		}
		$options = array('conditions' => array('Reserstatusmultiple.' . $this->Reserstatusmultiple->primaryKey => $id));
		$this->set('reserstatusmultiple', $this->Reserstatusmultiple->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Reserstatusmultiple->create();
			if ($this->Reserstatusmultiple->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Reserstatusmultiple->exists($id)) {
			throw new NotFoundException(__('Invalid reserstatusmultiple'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Reserstatusmultiple->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Reserstatusmultiple.' . $this->Reserstatusmultiple->primaryKey => $id));
			$this->request->data = $this->Reserstatusmultiple->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Reserstatusmultiple->id = $id;
		if (!$this->Reserstatusmultiple->exists()) {
			throw new NotFoundException(__('Invalid reserstatusmultiple'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Reserstatusmultiple->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}



}
