<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class DashboardController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');
	

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array('Empresa','Reserindividuale');

	public function beforeFilter() {
		$this->checkSession(100);
	}





	function index($mes = null, $anho = null){


		$value = $this->request->data['Ocupacion']['estatus'];






		$this->layout = "dashbord";
		
		


		$data1[1]  = "Enero";
		$data1[2]  = "Febrero";
		$data1[3]  = "Marzo";
		$data1[4]  = "Abril";
		$data1[5]  = "Mayo";
		$data1[6]  = "Junio";
		$data1[7]  = "Julio";
		$data1[8]  = "Agosto";
		$data1[9]  = "Septiembre";
		$data1[10] = "Octubre";
		$data1[11] = "Noviembre";
		$data1[12] = "Diciembre";





		if ($mes == null && $anho == null) {
			# code...

			$nummes = is_integer(date('m'));
			$titulo = " Grafica Ocupaciones Mes: ".$data1[date('m')];
			
			$numero = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
			$var = '';

			$this->set('mes', date('m'));
			$fechaa = date('Y-m-').'01';
			$fechab = date('Y-m-').$numero;
		}else{


			$nummes = is_integer($mes);
			$titulo = " Grafica Ocupaciones Mes: ".$data1[$mes];
			
			$numero = cal_days_in_month(CAL_GREGORIAN, $mes, $anho);
			$var = '';



			$this->set('mes', $mes);

			$fechaa = $anho.'-'.$mes.'-01';
			$fechab = $anho.'-'.$mes.$numero;

		}

	

		for ($i=1; $i <= $numero ; $i++) { 

			$arraycont[$i] = 0;

		}

			$a = explode('-', $fechaa);
			$this->set('primerdia', $a[2]);
			$primerdia = $a[2];

			$b = explode('-', $fechab);
			$this->set('ultimodia', $b[2]);
			$ultimodia = $b[2];



			for ($i=1; $i <= $numero ; $i++) { 


				$fecha_a = $a[0].'-'.$a[1].'-0'.$i;


				$result1 = $this->Reserindividuale->find('all', 
					[
						'conditions'=>[
							'Reserindividuale.reserstatusindividuale_id' =>$value,
							'Reserindividuale.fecha_entrada <= ' => $fecha_a, 'Reserindividuale.fecha_salida  >= ' => $fecha_a
						],
						'order'=> ['Habitacione.numhabitacion'=>'asc']

					]);

				
				foreach ($result1 as $key) {
					$arraycont[$i] = $arraycont[$i] + 1;
				}

			} 

		
		$cat = '[';
		$dat = '[';

		for ($i=1; $i <= $numero ; $i++) { 
			

			$cat .= $i.','; 
			$dat .= $arraycont[$i].',';
			//$var .= '{y: '.$arraycont[$i].', label: '.$i.'},';
		}

		$cat .= ']'; 
		$dat .= ']'; 

		$this->set('titulo', $titulo);
        $this->set('cate', $cat);
        $this->set('data', $dat);



			    		
	}
}
?>