<?php
App::uses('AppController', 'Controller');
/**
 * Compraabonas Controller
 *
 * @property Compraabona $Compraabona
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CompraabonasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(5);
	}

/**
 * index method
 *
 * @return void
 */
	public function index($id=null) {
		$this->Compraabona->recursive = 2;
		//$this->set('reserindivistatuses', $this->Paginator->paginate());
		  $this->set('compraabonas', $this->Compraabona->find('all'));
		  if($id!=null){
		  	return $this->redirect('/Compras/cuentaxpagar');
		  }
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Compraabona->exists($id)) {
			throw new NotFoundException(__('Invalid compraabona'));
		}
		$options = array('conditions' => array('Compraabona.' . $this->Compraabona->primaryKey => $id));
		$this->set('compraabona', $this->Compraabona->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($id=null) {
		if ($this->request->is('post')) {
			$this->Compraabona->begin();
			$this->Compraabona->create();
			if ($this->Compraabona->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				//return $this->redirect(array('action' => 'index'));
                        $this->request->data['Compra']['id']     = $this->request->data['Compraabona']['compra_id'];
                        $this->request->data['Compra']['deuda']  = $this->request->data['Compraabona']['deuda'];
                        $this->request->data['Compra']['pagado'] = $this->request->data['Compraabona']['total']-$this->request->data['Compraabona']['deuda'];
				    if ($this->Compraabona->Compra->save($this->request->data)) {
                   		 $this->Compraabona->commit();
                		 $this->Flash->success(__('Registro Guardado.'));
						 return $this->redirect('/Compras/cuentaxpagar');
	                }else{
	                	$this->Compraabona->rollback();
	                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
	                }

			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
		$compras_d     = $this->Compraabona->Compra->find('all', array('conditions'=>array('Compra.id'=>$id)));
		$num_pago      = $this->Compraabona->find('count', array('conditions'=>array('Compraabona.compra_id'=>$id)));
		$compraabonas  = $this->Compraabona->find('all', array('conditions'=>array('Compraabona.compra_id'=>$id)));

		$this->set('total', $compras_d[0]['Compra']['total']);
		$this->set('deuda', $compras_d[0]['Compra']['deuda']);
		$this->set('compras_d', $compras_d);
		$this->set('num_pago', $num_pago+1);

		$compras = $this->Compraabona->Compra->find('list', array('conditions'=>array('Compra.id'=>$id)));
		$compratipopagos = $this->Compraabona->Compratipopago->find('list');
		$this->set(compact('compras', 'compratipopagos', 'compraabonas'));
		$this->set('id',$id);
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Compraabona->exists($id)) {
			throw new NotFoundException(__('Invalid compraabona'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Compraabona->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Compraabona.' . $this->Compraabona->primaryKey => $id));
			$this->request->data = $this->Compraabona->find('first', $options);
		}
		$compras = $this->Compraabona->Compra->find('list');
		$compratipopagos = $this->Compraabona->Compratipopago->find('list');
		$this->set(compact('compras', 'compratipopagos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Compraabona->id = $id;
		if (!$this->Compraabona->exists()) {
			throw new NotFoundException(__('Invalid compraabona'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Compraabona->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
