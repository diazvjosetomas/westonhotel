<?php
App::uses('AppController', 'Controller');
/**
 * Reserindividualeextras Controller
 *
 * @property Reserindividualeextra $Reserindividualeextra
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ReserindividualeextrasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession();
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Reserindividualeextra->recursive = 0;
		//$this->set('reserindividualeextras', $this->Paginator->paginate());
		  $this->set('reserindividualeextras', $this->Reserindividualeextra->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Reserindividualeextra->exists($id)) {
			throw new NotFoundException(__('Invalid reserindividualeextra'));
		}
		$options = array('conditions' => array('Reserindividualeextra.' . $this->Reserindividualeextra->primaryKey => $id));
		$this->set('reserindividualeextra', $this->Reserindividualeextra->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Reserindividualeextra->create();
			if ($this->Reserindividualeextra->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Reserindividualeextra->exists($id)) {
			throw new NotFoundException(__('Invalid reserindividualeextra'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Reserindividualeextra->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Reserindividualeextra.' . $this->Reserindividualeextra->primaryKey => $id));
			$this->request->data = $this->Reserindividualeextra->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Reserindividualeextra->id = $id;
		if (!$this->Reserindividualeextra->exists()) {
			throw new NotFoundException(__('Invalid reserindividualeextra'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Reserindividualeextra->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
