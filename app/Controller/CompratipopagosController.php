<?php
App::uses('AppController', 'Controller');
/**
 * Compratipopagos Controller
 *
 * @property Compratipopago $Compratipopago
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CompratipopagosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(5);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Compratipopago->recursive = 0;
		//$this->set('compratipopagos', $this->Paginator->paginate());
		  $this->set('compratipopagos', $this->Compratipopago->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Compratipopago->exists($id)) {
			throw new NotFoundException(__('Invalid compratipopago'));
		}
		$options = array('conditions' => array('Compratipopago.' . $this->Compratipopago->primaryKey => $id));
		$this->set('compratipopago', $this->Compratipopago->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Compratipopago->create();
			if ($this->Compratipopago->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Compratipopago->exists($id)) {
			throw new NotFoundException(__('Invalid compratipopago'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Compratipopago->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Compratipopago.' . $this->Compratipopago->primaryKey => $id));
			$this->request->data = $this->Compratipopago->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Compratipopago->id = $id;
		if (!$this->Compratipopago->exists()) {
			throw new NotFoundException(__('Invalid compratipopago'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Compratipopago->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
