<?php
App::uses('AppController', 'Controller');
/**
 * Proivas Controller
 *
 * @property Proiva $Proiva
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ProivasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(3);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Proiva->recursive = 0;
		//$this->set('proivas', $this->Paginator->paginate());
		  $this->set('proivas', $this->Proiva->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Proiva->exists($id)) {
			throw new NotFoundException(__('Invalid proiva'));
		}
		$options = array('conditions' => array('Proiva.' . $this->Proiva->primaryKey => $id));
		$this->set('proiva', $this->Proiva->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Proiva->create();
			if ($this->Proiva->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Proiva->exists($id)) {
			throw new NotFoundException(__('Invalid proiva'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Proiva->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Proiva.' . $this->Proiva->primaryKey => $id));
			$this->request->data = $this->Proiva->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Proiva->id = $id;
		if (!$this->Proiva->exists()) {
			throw new NotFoundException(__('Invalid proiva'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Proiva->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
