<?php
App::uses('AppController', 'Controller');
/**
 * Cajaturnos Controller
 *
 * @property Cajaturno $Cajaturno
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CajaturnosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(9);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Cajaturno->recursive = 0;
		//$this->set('cajaturnos', $this->Paginator->paginate());
		  $this->set('cajaturnos', $this->Cajaturno->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Cajaturno->exists($id)) {
			throw new NotFoundException(__('Invalid cajaturno'));
		}
		$options = array('conditions' => array('Cajaturno.' . $this->Cajaturno->primaryKey => $id));
		$this->set('cajaturno', $this->Cajaturno->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Cajaturno->create();
			if ($this->Cajaturno->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Cajaturno->exists($id)) {
			throw new NotFoundException(__('Invalid cajaturno'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Cajaturno->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Cajaturno.' . $this->Cajaturno->primaryKey => $id));
			$this->request->data = $this->Cajaturno->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Cajaturno->id = $id;
		if (!$this->Cajaturno->exists()) {
			throw new NotFoundException(__('Invalid cajaturno'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Cajaturno->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
