<?php
App::uses('AppController', 'Controller');
/**
 * Reserstatusindividuales Controller
 *
 * @property Reserstatusindividuale $Reserstatusindividuale
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ReserstatusindividualesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(6);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Reserstatusindividuale->recursive = 0;
		//$this->set('reserstatusindividuales', $this->Paginator->paginate());
		  $this->set('reserstatusindividuales', $this->Reserstatusindividuale->find('all'));
	}

	public function get_estatus($id_reserstatusinidividuales = null){

		if (!$this->Reserstatusindividuale->exists($id_reserstatusinidividuales)) {
			throw new NotFoundException(__('Invalid reserstatusindividuale'));
		}

		$result = $this->Reserstatusindividuale->find('first', [
			'conditions' => [
				'Reserstatusindividuale.id' => $id_reserstatusinidividuales
				]
			]
		);

		$estatus = $result['Reserstatusindividuale']['denominacion'];
		return $estatus;
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Reserstatusindividuale->exists($id)) {
			throw new NotFoundException(__('Invalid reserstatusindividuale'));
		}
		$options = array('conditions' => array('Reserstatusindividuale.' . $this->Reserstatusindividuale->primaryKey => $id));
		$this->set('reserstatusindividuale', $this->Reserstatusindividuale->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Reserstatusindividuale->create();
			if ($this->Reserstatusindividuale->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));

				
				return $this->redirect(array('action' => 'index'));


			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Reserstatusindividuale->exists($id)) {
			throw new NotFoundException(__('Invalid reserstatusindividuale'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Reserstatusindividuale->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Reserstatusindividuale.' . $this->Reserstatusindividuale->primaryKey => $id));
			$this->request->data = $this->Reserstatusindividuale->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Reserstatusindividuale->id = $id;
		if (!$this->Reserstatusindividuale->exists()) {
			throw new NotFoundException(__('Invalid reserstatusindividuale'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Reserstatusindividuale->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
