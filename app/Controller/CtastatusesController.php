<?php
App::uses('AppController', 'Controller');
/**
 * Ctastatuses Controller
 *
 * @property Ctastatus $Ctastatus
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CtastatusesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(4);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Ctastatus->recursive = 0;
		//$this->set('ctastatuses', $this->Paginator->paginate());
		  $this->set('ctastatuses', $this->Ctastatus->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Ctastatus->exists($id)) {
			throw new NotFoundException(__('Invalid ctastatus'));
		}
		$options = array('conditions' => array('Ctastatus.' . $this->Ctastatus->primaryKey => $id));
		$this->set('ctastatus', $this->Ctastatus->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Ctastatus->create();
			if ($this->Ctastatus->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Ctastatus->exists($id)) {
			throw new NotFoundException(__('Invalid ctastatus'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Ctastatus->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Ctastatus.' . $this->Ctastatus->primaryKey => $id));
			$this->request->data = $this->Ctastatus->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Ctastatus->id = $id;
		if (!$this->Ctastatus->exists()) {
			throw new NotFoundException(__('Invalid ctastatus'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Ctastatus->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
