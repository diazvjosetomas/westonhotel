<?php
App::uses('AppController', 'Controller');
/**
 * Reserindividuales Controller
 *
 * @property Reserindividuale $Reserindividuale
 * 
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ReserindividualespubController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session', 'Flash');
    //public $uses = array('Reserindividuale');


    var $uses = array('Terminoscondicione',
                      'Reserindividuale', 
                      'Pstemporada', 
                      'Empresa', 
                      'Cliente', 
                      'Tipotemporada', 
                      'Habitacione', 
                      'Reserindividualeextra', 
                      'Resermulhabitacione', 
                      'Resermultiple', 
                      'Pai',
                      'Bodymail',
                      'Reserindivistatu',
                      'Setting',
                      'Cupo', 
                      'Tipohabitacione',
                      'Auditoria',
                      'Conteocupo',
                      'Tmpreserva',
                      'Descuento');
    /*
    ** var de layout
    *
    */
    public $layout = 'contenttour/tour_modulo';
    
    /*
    *  *  beforeFilter check de session
    *
    */
    /*  public function beforeFilter() {
            $this->checkSession(6);
        }*/

    public function denominaciontiptemporada(){
      $this->layout  = 'ajax';

      $this->set('denominacion', $this->Tipotemporada->find('all', [ 'conditions'=>[ 'Tipotemporada.id ' => $this->request->data['id'] ] ] ));
    }


/*   public function enabledbox(){
    $this->layout='ajax';

    $fentrada = $this->Session->read('FECHAENTRADA');
    $fsalida  = $this->Session->read('FECHASALIDA');

    $this->set('idtipotemporada', $this->Cupo->find('all', [
                                  'conditions'=>[

                                                'Cupo.fecha           >= ' => $fentrada,
                                                'Cupo.fecha           <= ' => $fsalida,
                                                'Cupo.tipotemporada_id   ' => $this->request->data['idtipotemporada'],
                                                'Cupo.tipohabitacione_id ' => $this->request->data['inputhiddenidtipohab']

                                                ]
                                ]));

   }*/


   public function enabledbox(){
    $this->layout='ajax';

    $fentrada = $this->Session->read('FECHAENTRADA');
    $fsalida  = $this->Session->read('FECHASALIDA');

    $this->set('idtipotemporada', $this->Cupo->find('all', [
                                  'conditions'=>[

                                                'Cupo.fecha           >= ' => $fentrada,
                                                'Cupo.fecha            < ' => $fsalida,
                                                'Cupo.tipotemporada_id   ' => $this->request->data['idtipotemporada'],
                                                'Cupo.tipohabitacione_id ' => $this->request->data['inputhiddenidtipohab']

                                                ]
                                ]));

   $this->set('fechainicio' , $fentrada );
   $this->set('fechafin'    , $fsalida );


   $date1 = new DateTime($fentrada);
   $date2 = new DateTime($fsalida);

   $diff = $date1->diff($date2);
   // will output 2 days
   

   $this->set('cant_dias', $diff->days );


   }



   public function removertmp(){

    $this->layout = 'ajax';


    $idtipohabitacion = $this->request->data['idtipohabitacion'];

    $idtipohabitacion = explode('|', $idtipohabitacion);

    $idtipohabitacion = $idtipohabitacion[1];

    $idvisitante      = $this->Session->read('VISITANTE');
  





    $sqlDelete = "DELETE FROM tmpreservas WHERE identificador = '$idvisitante' AND tipotemporada_id = '$idtipohabitacion'";

    $this->Tmpreserva->query($sqlDelete);

   }



   public function newremove($id = null){

    $this->layout = 'ajax';



    $sqlDelete = "DELETE FROM tmpreservas WHERE id = '$id'";

    $this->Tmpreserva->query($sqlDelete);

   }




    public function getdatatmpreservas(){
        $this->layout = 'ajax';

        if ($this->request->is('post')) {

        
            $this->set('identificador', $this->Session->Read('VISITANTE'));

            $this->set('getdata', 
                $this->Tmpreserva->find('all', 
                                [ 'conditions' => 
                                        ['Tmpreserva.identificador'=>  $this->Session->Read('VISITANTE')] 
                                    ]) 
            );

        }


    }


    public function time(){

      $this->layout='ajax';

      $fentrada = $this->Session->read('FECHAENTRADA');
      $fsalida  = $this->Session->read('FECHASALIDA');






      if ( isset( $fentrada ) AND isset( $fsalida ) ) {

          $fentrada_aux = explode('-', $fentrada);
          $fentrada     = $fentrada_aux[1].'-'.$fentrada_aux[2].'-'.$fentrada_aux[0];

          $fsalida_aux = explode('-', $fsalida);
          $fsalida     = $fsalida_aux[1].'-'.$fsalida_aux[2].'-'.$fsalida_aux[0];
        
          $this->set('isset', 1                                           );
          $this->set('fechaentrada', $fentrada );
          $this->set('fechasalida' , $fsalida  );

      }else{

          $this->set('isset', 0);
      }


    }


    public function consulta_habitacion() {
        $this->layout='ajax';
        $a = $this->request->data['fechai'];
        $b = $this->request->data['fechas'];
        $c = $this->request->data['personas'];

        $fecha = explode('-', $a);
        $a = $fecha[2].'-'.$fecha[0].'-'.$fecha[1];

        

        $dispcupos = $this->Cupo->find('all', 
                                              [

                                                'conditions'=>[

                                                    'Cupo.fecha'=>$a


                                                ]
                                              

                                              ]);

        

        foreach ($dispcupos as $key) {
          $id[] = $key['Cupo']['tipohabitacione_id'];
        }







        $opciones= array('conditions'=> array('Tipohabitacione.cantninos >= ' => $c, 'Tipohabitacione.id'=>$id));



        $data = $this->Tipohabitacione->find('all',$opciones);


        //Buscar en la tabla cupo disponibilidad





        if (count($data) > 0 ){
            $this->set('data', $data);
        }else{
            $this->set('data', 0);

        }

    }
    
    
    
    

  public function tmpreservas(){
    $this->layout='ajax';    

    if ($this->request->is('post')) {


        $auxfechae = $this->request->data['fecha_entrada'];
        $auxfechas = $this->request->data['fecha_salida'];


        $auxfechae = explode('/', $auxfechae);
        $auxfechas = explode('/', $auxfechas);

        if ($auxfechae[0] < 10) {
         
          $fechae = $auxfechae[2].'-0'.$auxfechae[0].'-'.$auxfechae[1];
          
        }else{

          $fechae = $auxfechae[2].'-'.$auxfechae[0].'-'.$auxfechae[1];
          
        }


        if ($auxfechas[0] < 10) {
            
          $fechas = $auxfechas[2].'-0'.$auxfechas[0].'-'.$auxfechas[1];

            
        }else{

          $fechas = $auxfechas[2].'-'.$auxfechas[0].'-'.$auxfechas[1];

        }





        // Evaluar si hay cupo disponibles en la fecha solicitada



        //Verificar si se ha ingresado el mismo tipodehabitacion
        $tipohabverf = $this->Tmpreserva->find('all', 
                                ['conditions'=>

                                  [ 

                                    'Tmpreserva.tipohabitacione_id ' => $this->request->data['tipohabitacione_id'] ,

                                    'Tmpreserva.tipotemporada_id '   => $this->request->data['temporada_id'] ,

                                    'Tmpreserva.identificador'       => $this->Session->Read('VISITANTE')

                                                ]]
                                            );

        if(count($tipohabverf) > 0){

        echo " fe ".$fechae." fs ".$fechas;
            $tipohabitacione_id = $this->request->data['tipohabitacione_id'];
            $cantidadreservada  = $this->request->data['cant_tipotemp'];
            $cant_personas      = $this->request->data['cant_personas'];
            $temporada_id       = $this->request->data['temporada_id'];
            $cupo_id            = $this->request->data['cupo_id'];
            


            $sqlupdate = "UPDATE tmpreservas SET tipotemporada_id         = $temporada_id, 
                                                 cant_personas            = $cant_personas,
                                                 cupo_id                  = $cupo_id,
                                                 fecha_entrada            = '$fechae',
                                                 fecha_salida             = '$fechas',
                                                 cantidadseleccionada     = $cantidadreservada
                                                 WHERE tipohabitacione_id = $tipohabitacione_id";
            
            $this->Tmpreserva->query($sqlupdate);

        }else{




            $this->request->data['Tmpreserva']['tipohabitacione_id']   = $this->request->data['tipohabitacione_id'];
            $this->request->data['Tmpreserva']['cantidadseleccionada'] = $this->request->data['cant_tipotemp'];
            $this->request->data['Tmpreserva']['tipotemporada_id']     = $this->request->data['temporada_id'];
            $this->request->data['Tmpreserva']['cant_personas']        = $this->request->data['cant_personas'];
            $this->request->data['Tmpreserva']['identificador']        = $this->Session->Read('VISITANTE');
            $this->request->data['Tmpreserva']['cupo_id']              = $this->request->data['cupo_id'];
            $this->request->data['Tmpreserva']['fecha_entrada']        = $fechae;
            $this->request->data['Tmpreserva']['fecha_salida']         = $fechas;

        
            $this->Tmpreserva->create();

            if ($this->Tmpreserva->save($this->request->data)) {
           
            } else {
            

            }
            
        }

        
    }
      
  }
    
    
    
  public function temporadas_id(){
        
      $this->layout='ajax';

      $this->Cupo->recursive = 3;

      $fecha = explode('/', $this->request->data['fechaentrada']);
      $fechaentrada = $fecha[2].'-'.$fecha[0].'-'.$fecha[1];


      $fecha = explode('/', $this->request->data['fechasalida']);
      $fechasalida = $fecha[2].'-'.$fecha[0].'-'.$fecha[1];

    



      $idtipotemporada =  $this->Cupo->find('all', [
                                    'conditions'=>[

                                                  'Cupo.fecha                   >= ' => $fechaentrada,
                                                  'Cupo.fecha                   <= ' => $fechasalida
                                                  

                                                  ]
                                  ]);


      foreach ($idtipotemporada as $key =>$value) {
        
        $id[] = $value['Cupo']['tipotemporada_id'];
          
      }


      $this->set('idtemporada',
            $this->Tipotemporada->find('all', [
                                                  'conditions'=>[
                                                    'Tipotemporada.id'=>$id
                                                  ]
            ])
          );
      

      
      
  }    


  public function temporadas(){
      $this->layout='ajax';

      $this->Cupo->recursive = 3;

      $fecha = explode('/', $this->request->data['fechaentrada']);
      $fechaentrada = $fecha[2].'-'.$fecha[0].'-'.$fecha[1];


      $fecha = explode('/', $this->request->data['fechasalida']);
      $fechasalida = $fecha[2].'-'.$fecha[0].'-'.$fecha[1];


      $fecha1 = new DateTime($fechaentrada);
      $fecha2 = new DateTime($fechasalida);
      $resultado = $fecha1->diff($fecha2);
      //echo $resultado->format('%a');
      $cantnoches =  $resultado->format('%a');


    
      $this->Session->write('FECHAENTRADA', $fechaentrada);
      $this->Session->write('FECHASALIDA', $fechasalida);


      $this->set('fechae',   $fechaentrada);
      $this->set('fechas',   $fechasalida);
      $this->set('cantnoches', $cantnoches);


      $idtipotemporada =  $this->Cupo->find('all', [
                                    'conditions'=>[

                                                  'Cupo.fecha                   >= ' => $fechaentrada,
                                                  'Cupo.fecha                   <= ' => $fechasalida,
                                                  'Cupo.tipohabitacione_id         ' => $this->request->data['idtipohabitacion']

                                                  ]
                                  ]);


      foreach ($idtipotemporada as $key =>$value) {
        
        $id[] = $value['Cupo']['tipotemporada_id'];
      }
      
      
      

      


      $this->set('tipotemporada',
            $this->Tipotemporada->find('all', [
                                                  'conditions'=>[
                                                    'Tipotemporada.id'=>$id
                                                  ]
            ])
          );




      $this->set('datacupos', 
        $this->Cupo->find('all', [
                                    'conditions'=>[

                                                  'Cupo.fecha                   >= ' => $fechaentrada,
                                                  'Cupo.fecha                   <= ' => $fechasalida,
                                                  'Cupo.tipohabitacione_id         ' => $this->request->data['idtipohabitacion']

                                                  ]
                                  ])
     );
      $datacupos = $this->Cupo->find('all', [
          'conditions'=>[

              'Cupo.fecha                   >= ' => $fechaentrada,
              'Cupo.fecha                   <= ' => $fechasalida,
              'Cupo.tipohabitacione_id         ' => $this->request->data['idtipohabitacion']

          ]
      ]);

      ///////////consulta de cupos
      /// //Esta busqueda entra en las reservaciones a buscar ese tipo de reservaciones
      //Esta busqueda entra en las reservaciones a buscar ese tipo de reservaciones

      $reservas = $this->Reserindividuale->find('all',
          array('conditions'=>
              array('Reserindividuale.fecha_entrada'=>$fechaentrada )));

      $tipohabitaciones = $this->Tipohabitacione->find('all',
          ['conditions'=>[ 'Tipohabitacione.id'=>$this->request->data['idtipohabitacion']]]);

      $control = 0;
      $cant = 0;


      foreach ($tipohabitaciones as $id) { // Tipos de habitaciones
          //echo $d['Tipohabitacione']['id'];


          foreach ($reservas as $data) { // Reservas
              $arraydata = $data["Reserindividuale"]["tipohabitaciones"];

              $arraydata =  substr($arraydata, 0, -1);
              $arraydata =  substr($arraydata, 1);

              $explode1 = explode(',', $arraydata);

              for ($i=0; $i <= count($explode1) - 1; $i++) { // 11


                  $aux = substr($explode1[$i], 0, -1);
                  $aux = substr($aux, 1);
                  //echo '->'.$aux.'<br>';


                  $arraydata2 = explode('|', $aux);

                  for ($j=0; $j <= count($arraydata2) - 2 ; $j++) { // 12

                      if ($arraydata2[0] == $this->request->data['idtipohabitacion']) {

                          $cant += $arraydata2[1];

                      }

                  } // 12 END

              }  // 11 END

          } // Reservas END

      }// Tipos de habitaciones END



      // Buscar todas las cantidades
      $tipohabitaciones2 = $this->Tipohabitacione->find('all');



      $datatotal = '{';

      foreach ($tipohabitaciones2 as $id) { // Tipos de habitaciones
          //echo $d['Tipohabitacione']['id'];


          $datatotal .= '"'.$id['Tipohabitacione']['id'].'":';


          foreach ($reservas as $data) { // Reservas
              $arraydata = $data["Reserindividuale"]["tipohabitaciones"];

              $arraydata =  substr($arraydata, 0, -1);
              $arraydata =  substr($arraydata, 1);

              $explode1 = explode(',', $arraydata);

              for ($i=0; $i <= count($explode1) - 1; $i++) { // 11


                  $aux = substr($explode1[$i], 0, -1);
                  $aux = substr($aux, 1);
                  //echo '->'.$aux.'<br>';

                  $arraydata2 = explode('|', $aux);

                  for ($j=0; $j <= count($arraydata2) - 2 ; $j++) { // 12

                      if ($id['Tipohabitacione']['id'] == $arraydata2[0]) {

                          $arrayTipoHab[ $id['Tipohabitacione']['id'] ] += $arraydata2[1];


                      }

                  } // 12 END

              }  // 11 END

          } // Reservas END

          if ($arrayTipoHab[ $id['Tipohabitacione']['id'] ]== '') {

              $datatotal .= '0,';

          }else{

              $datatotal .= $arrayTipoHab[ $id['Tipohabitacione']['id'] ].',';

          }


      }// Tipos de habitaciones END


      $datatotal  = substr($datatotal, 0, -1 );
      $datatotal .= '}';


      // Consultando la tabla cupos
      $data = $this->Cupo->find('all', [
          'conditions' => [
              'Cupo.fecha'             => $this->request->data['fechae'],
              'Cupo.tipohabitacione_id'=> $this->request->data['idtipohabitacion']
          ]
      ]);



      /*if (count($data) == 0) {
          $this->set('result',0);
      }else{

          if ( $data[0]['Cupo']['cantidad'] == 0 ) {
              $this->set('result',0);
          }

          if ( $data[0]['Cupo']['cantidad'] > 0 ) {


              $this->set('cantidadreservadas', $cant);
              $this->set('cantidadrestante',$data[0]['Cupo']['cantidad'] -  $cant);

          }
      }*/

      $this->set('cantidadrestante',  $datacupos[0]['Cupo']['cantidad'] - $cant );
      //$this->set('cantidadrestante',  $cant);


///////


  }

  public function cupostipohabitacionesprincipal(){


      $this->layout='ajax';


      $reservas = $this->Reserindividuale->find('all', 
                                            array('conditions'=>
                                              array('Reserindividuale.fecha_entrada'=>
                                                $this->request->data['fechae'] )));
     

      $tipohabitaciones = $this->Tipohabitacione->find('all');

      $control = 0;
      $cant    = 0;


      foreach ($tipohabitaciones as $id) { // Tipos de habitaciones
          //echo $d['Tipohabitacione']['id'];


          foreach ($reservas as $data) { // Reservas
            $arraydata = $data["Reserindividuale"]["tipohabitaciones"];

            $arraydata =  substr($arraydata, 0, -1);
            $arraydata =  substr($arraydata, 1);
            
            $explode1 = explode(',', $arraydata);

            for ($i=0; $i <= count($explode1) - 1; $i++) { // 11
                

                $aux = substr($explode1[$i], 0, -1);
                $aux = substr($aux, 1);
                //echo '->'.$aux.'<br>'; 


                $arraydata2 = explode('|', $aux);

                for ($j=0; $j <= count($arraydata2) - 2 ; $j++) { // 12

                  if ($arraydata2[0] == $this->request->data['idtipohabitacion']) {
                    
                      $cant += $arraydata2[1];
                      
                  }

               } // 12 END

            }  // 11 END

          } // Reservas END
          
      }// Tipos de habitaciones END

          

      // Buscar todas las cantidades 
      $tipohabitaciones2 = $this->Tipohabitacione->find('all');



      $datatotal = '{';

      foreach ($tipohabitaciones2 as $id) { // Tipos de habitaciones
          //echo $d['Tipohabitacione']['id'];

      
          $datatotal .= '"'.$id['Tipohabitacione']['id'].'":'; 


          foreach ($reservas as $data) { // Reservas
            $arraydata = $data["Reserindividuale"]["tipohabitaciones"];

            $arraydata =  substr($arraydata, 0, -1);
            $arraydata =  substr($arraydata, 1);
            
            $explode1 = explode(',', $arraydata);

            for ($i=0; $i <= count($explode1) - 1; $i++) { // 11
                

                $aux = substr($explode1[$i], 0, -1);
                $aux = substr($aux, 1);
                //echo '->'.$aux.'<br>'; 


                $arraydata2 = explode('|', $aux);

                for ($j=0; $j <= count($arraydata2) - 2 ; $j++) { // 12

                  if ($id['Tipohabitacione']['id'] == $arraydata2[0]) {
                      
                      $arrayTipoHab[ $id['Tipohabitacione']['id'] ] += $arraydata2[1];                            

                      
                  }

               } // 12 END

            }  // 11 END

          } // Reservas END

          if ($arrayTipoHab[ $id['Tipohabitacione']['id'] ]== '') {
              
              $datatotal .= '0,';
              
          }else{

              $datatotal .= $arrayTipoHab[ $id['Tipohabitacione']['id'] ].',';

          }

          
      }// Tipos de habitaciones END


      $datatotal  = substr($datatotal, 0, -1 );
      $datatotal .= '}';


      // Consultando la tabla cupos    
      $data = $this->Cupo->find('all', [
          'conditions' => [
              'Cupo.fecha'             => $this->request->data['fechae']
          ]
      ]);

      

      if (count($data) == 0) {
          $this->set('result',0);
      }else{

          if ( $data[0]['Cupo']['cantidad'] == 0 ) {
              $this->set('result',0);                
          }

          if ( $data[0]['Cupo']['cantidad'] > 0 ) {

              $this->set('result',1);
              $this->set('cantidadreservadas', $cant);
              $this->set('cantidadrestante',$data[0]['Cupo']['cantidad'] -  $cant);   
              $this->set('datatotal', $datatotal);      
          }
      }

  
  }

  public function cupostipohabitacionesview(){

        $this->layout='ajax';

        $reservas = $this->Reserindividuale->find('all', 
                                                  array('conditions'=>
                                                    array('Reserindividuale.id'=>$this->request->data['idreserva'] )));

        $control = 0;
        $cant = 0;


        // Buscar todas las cantidades 
        $tipohabitaciones2 = $this->Tipohabitacione->find('all');



        $datatotal = '';

        foreach ($tipohabitaciones2 as $id) { // Tipos de habitaciones
            //echo $d['Tipohabitacione']['id'];

        
            $datatotal .= ''.$id['Tipohabitacione']['denominacion'].':'; 


            foreach ($reservas as $data) { // Reservas
              $arraydata = $data["Reserindividuale"]["tipohabitaciones"];

              $arraydata =  substr($arraydata, 0, -1);
              $arraydata =  substr($arraydata, 1);
              
              $explode1 = explode(',', $arraydata);

              for ($i=0; $i <= count($explode1) - 1; $i++) { // 11
                  

                  $aux = substr($explode1[$i], 0, -1);
                  $aux = substr($aux, 1);
                  //echo '->'.$aux.'<br>'; 


                  $arraydata2 = explode('|', $aux);


                  



                  for ($j=0; $j <= count($arraydata2) - 2 ; $j++) { // 12

                    if ($id['Tipohabitacione']['id'] == $arraydata2[0]) {
                        
                        $arrayTipoHab[ $id['Tipohabitacione']['id'] ] += $arraydata2[1];                            

                        
                    }

                 } // 12 END

              }  // 11 END

            } // Reservas END

            if ($arrayTipoHab[ $id['Tipohabitacione']['id'] ]== '') {
                
                $datatotal .= '0,';
                
            }else{

                $datatotal .= $arrayTipoHab[ $id['Tipohabitacione']['id'] ].', ';

            }

            
        }// Tipos de habitaciones END


        $datatotal  = substr($datatotal, 0, -1 );
        $datatotal .= '';


        $this->set('datatotal', $datatotal);

        
  }

  public function cupostipohabitaciones(){
       


        $this->layout='ajax';


        $reservas = $this->Reserindividuale->find('all', 
                                              array('conditions'=>
                                                array('Reserindividuale.fecha_entrada'=>$this->request->data['fechae'] )));

        


        //Esta busqueda entra en las reservaciones a buscar ese tipo de reservaciones
        $tipohabitaciones = $this->Tipohabitacione->find('all', 
                            ['conditions'=>[ 'Tipohabitacione.id'=>$this->request->data['idtipohabitacion']]]);

        $control = 0;
        $cant = 0;


        foreach ($tipohabitaciones as $id) { // Tipos de habitaciones
            //echo $d['Tipohabitacione']['id'];


            foreach ($reservas as $data) { // Reservas
              $arraydata = $data["Reserindividuale"]["tipohabitaciones"];

              $arraydata =  substr($arraydata, 0, -1);
              $arraydata =  substr($arraydata, 1);
              
              $explode1 = explode(',', $arraydata);

              for ($i=0; $i <= count($explode1) - 1; $i++) { // 11
                  

                  $aux = substr($explode1[$i], 0, -1);
                  $aux = substr($aux, 1);
                  //echo '->'.$aux.'<br>'; 


                  $arraydata2 = explode('|', $aux);

                  for ($j=0; $j <= count($arraydata2) - 2 ; $j++) { // 12

                    if ($arraydata2[0] == $this->request->data['idtipohabitacion']) {
                      
                        $cant += $arraydata2[1];
                        
                    }

                 } // 12 END

              }  // 11 END

            } // Reservas END
            
        }// Tipos de habitaciones END

            

        // Buscar todas las cantidades 
        $tipohabitaciones2 = $this->Tipohabitacione->find('all');



        $datatotal = '{';

        foreach ($tipohabitaciones2 as $id) { // Tipos de habitaciones
            //echo $d['Tipohabitacione']['id'];

        
            $datatotal .= '"'.$id['Tipohabitacione']['id'].'":'; 


            foreach ($reservas as $data) { // Reservas
              $arraydata = $data["Reserindividuale"]["tipohabitaciones"];

              $arraydata =  substr($arraydata, 0, -1);
              $arraydata =  substr($arraydata, 1);
              
              $explode1 = explode(',', $arraydata);

              for ($i=0; $i <= count($explode1) - 1; $i++) { // 11
                  

                  $aux = substr($explode1[$i], 0, -1);
                  $aux = substr($aux, 1);
                  //echo '->'.$aux.'<br>'; 


                  $arraydata2 = explode('|', $aux);


                  



                  for ($j=0; $j <= count($arraydata2) - 2 ; $j++) { // 12

                    if ($id['Tipohabitacione']['id'] == $arraydata2[0]) {
                        
                        $arrayTipoHab[ $id['Tipohabitacione']['id'] ] += $arraydata2[1];                            

                        
                    }

                 } // 12 END

              }  // 11 END

            } // Reservas END

            if ($arrayTipoHab[ $id['Tipohabitacione']['id'] ]== '') {
                
                $datatotal .= '0,';
                
            }else{

                $datatotal .= $arrayTipoHab[ $id['Tipohabitacione']['id'] ].',';

            }

            
        }// Tipos de habitaciones END


        $datatotal  = substr($datatotal, 0, -1 );
        $datatotal .= '}';


        // Consultando la tabla cupos    
        $data = $this->Cupo->find('all', [
            'conditions' => [
                'Cupo.fecha'             => $this->request->data['fechae'],
                'Cupo.tipohabitacione_id'=> $this->request->data['idtipohabitacion']
            ]
        ]);

       

        if (count($data) == 0) {
            $this->set('result',0);
        }else{

            if ( $data[0]['Cupo']['cantidad'] == 0 ) {
                $this->set('result',0);                
            }

            if ( $data[0]['Cupo']['cantidad'] > 0 ) {

                $this->set('result',1);
                $this->set('cantidadreservadas', $cant);
                $this->set('cantidadrestante',$data[0]['Cupo']['cantidad'] -  $cant);   
                $this->set('datatotal', $datatotal);      
            }
        }



  }

    public function index($id=null) {
        //$this->Reserindividuale->recursive = 0;
        //$this->set('reserindividuales', $this->Paginator->paginate());
        $this->set('reserindividuales', $this->Reserindividuale->find('all'));
        if($id!=null){$this->set('id_reporte', $id);}
    }

    public function inicio($id=null) {
        //$this->Reserindividuale->recursive = 0;
        //$this->set('reserindividuales', $this->Paginator->paginate());
        $this->set('reserindividuales', $this->Reserindividuale->find('all'));
        if($id!=null){$this->set('id_reporte', $id);}
    }


    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Reserindividuale->exists($id)) {
            throw new NotFoundException(__('Invalid reserindividuale'));
        }


        $this->set('detallesextra', $this->Reserindivistatu->find('all', array('conditions'=>array('Reserindivistatu.reserindividuale_id = ' => $id))));
        $options = array('conditions' => array('Reserindividuale.' . $this->Reserindividuale->primaryKey => $id));
        $this->set('reserindividuale', $this->Reserindividuale->find('first', $options));
    }

    public function get_previous($fecha_salida = null, $habitacione_id = null) {

        $nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha_salida ) ) ;
        $nuevafecha = date ( 'Y-m-d' , $nuevafecha );

        $data = $this->Reserindividuale->find('first', [
            'conditions' => [
                'Reserindividuale.fecha_entrada'  => $nuevafecha,
                'Reserindividuale.habitacione_id' => $habitacione_id
            ]
        ]);

        if ( !empty($data) ){
            return true;
        }

        return false;
    }




    public function datatipohabitacion($idtipohabitacion){
        $this->layout = 'ajax';




        $this->Tipohabitacione->recursive = 0;
        $this->set('datatipohabitacion', $this->Tipohabitacione->find('all', array('conditions'=>array('Tipohabitacione.id'=>$idtipohabitacion))));
    }

    public function buscardatos(){
        $this->layout = 'ajax';
        
        $this->set('datacliente',  $this->Cliente->find('all', array('conditions'=>array('Cliente.dni'=>$this->request->data['dni']))));



    }

    public function buscarcupos($id = null){
        $this->layout = 'ajax';
        $this->set('Ocupados',  $this->Cupo->find('all', array('conditions'=>array('Cupo.fecha'=>$this->request->data['Reserindividualefecha_entrada'],'Cupo.tipohabitacione_id'=>$id))));
    }





    public function add( $fechai = null , $fechas = null ) {
        
        if ($this->request->is('post')) {


            if  ($fechai == null)
            {
                $fechai = $this->Session->read('FECHAENTRADA');
            }

            if  ($fechas == null)
            {
                $fechas = $this->Session->read('FECHASALIDA');
            }

            


            $this->request->data['Reserindividuale']['tipohabitaciones'] = json_encode($this->request->data['Reserindividuale']['tipohabitaciones']);


            $this->request->data['Reserindividuale']['cantidad_personas'] = json_encode($this->request->data['Reserindividuale']['cantidad_personas']);

            
            

            $this->request->data['Reserindividuale']['fecha_entrada'] = !empty($this->request->data['Reserindividuale']['fecha_entrada1']) ? formatfecha($this->request->data['Reserindividuale']['fecha_entrada1']) : '';
            
            $this->request->data['Reserindividuale']['fecha_salida'] = !empty($this->request->data['Reserindividuale']['fecha_salida1']) ? formatfecha($this->request->data['Reserindividuale']['fecha_salida1']) : '';
            
            $cliente = $this->Cliente->find('first', [ [ 'conditions' => [ 'Cliente.id' => $this->request->data['Reserindividuale']['cliente_id'] ] ] ]);
            



            if(!empty($cliente)){
                $this->request->data['Reserindividuale']['tipocliente_id']    = $cliente['Tipocliente']['id'];
                $this->request->data['Reserindividuale']['tipoclientesub_id'] = $cliente['Cliente']['tipoclientesub_id'];
                $this->request->data['Reserindividuale']['resermultiple_id']  = 0;
            }else{
                $this->request->data['Reserindividuale']['tipocliente_id']    = 0;
                $this->request->data['Reserindividuale']['tipoclientesub_id'] = 0;
                $this->request->data['Reserindividuale']['resermultiple_id']  = 0;
            }


            $this->Reserindividuale->create();
            $this->Reserindividuale->begin();


            if ($this->Reserindividuale->save($this->request->data)) {
         

                $preferencias = $this->request->data['Reserindividuale']['observaciones_cliente'];
                $idclienter   = $this->request->data['Reserindividuale']['cliente_id'];
                $sqlUpdateCliente = "UPDATE clientes SET preferencias = '".$preferencias."' WHERE id =  $idclienter";

                $this->Cliente->query($sqlUpdateCliente);



                $last_id = $this->Reserindividuale->getLastInsertID();
                $id   =  $this->Reserindividuale->id;
                $stop = 0;



                if(!empty($this->request->data["Reserindividuale"]["Nombre"])){
                    foreach($this->request->data["Reserindividuale"]["Nombre"] as $nombre){
                        $this->request->data["Reserindividualeextra"]["reserindividuale_id"] = $id;
                        $this->request->data["Reserindividualeextra"]["nombres"] = $nombre;
                        $this->Reserindividualeextra->create();
                        if ($this->Reserindividualeextra->save($this->request->data)){

                        }else{
                            $stop = 1;
                        }
                    }
                }


                if($stop==0){
                    $this->Reserindividuale->commit();
                    $empresas = $this->Empresa->find('all');
                    $clientes = $this->Cliente->find('all', array('conditions'=>array('Cliente.id'=>$this->request->data['Reserindividuale']['cliente_id'])));

                    /**
                     * Enviamos mail
                     */
                    $idcliente = $this->request->data["Reserindividuale"]["cliente_id"];
                    $datacliente = $this->Cliente->find('all', array('conditions'=>array('Cliente.id'=>$idcliente)));


                    $email       = $datacliente[0]['Cliente']['correo'];
                    $nombres     = $datacliente[0]['Cliente']['nombre_completo'];
                    $fechainicio = $this->request->data["Reserindividuale"]["fecha_entrada"];
                    $fechasalida = $this->request->data["Reserindividuale"]["fecha_salida"];
                    $total       = $this->request->data["Reserindividuale"]["total"];
                    $idreserva   = $this->request->data["Reserindividuale"]["localizador"];
                  

                    $idlocalizador = $this->request->data["Reserindividuale"]["localizador"];
                    /**
                     * Extraemos el cuerpo de el mensaje
                     */

                    $cabeceras  = "MIME-Version: 1.0\r\n";
                    $cabeceras .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
                    $cabeceras .= "Reply-to: serviciodereservas@hoteleria.tk\r\n";
                    $cabeceras .= "From: serviciodereservas@hoteleria.tk \r\n";
                    

                    /**
                     * Mail para reserva iniciada
                     */
                    $terminos_condiciones = $this->Terminoscondicione->find('all', array('conditions'=>array('Terminoscondicione.id'=>1)));

                    $bodymail = $this->Bodymail->find('all', array('conditions'=>array('Bodymail.id'=>1)));

                    $subject = $bodymail[0]['Bodymail']['denominacion'];

                    $bodymail = $bodymail[0]['Bodymail']['cuerpoemail'];


                    $cuerpo = str_replace('_nombres_', $nombres, $bodymail);

                    $cuerpo = str_replace('_fechainicio_', $fechainicio, $cuerpo);

                    $cuerpo = str_replace('_fechasalida_', $fechasalida, $cuerpo);

                    $cuerpo = str_replace('_valorreserva_', $total, $cuerpo);

                    

                    $cuerpo = str_replace('_terminoscondiciones_', $terminos_condiciones[0]['Terminoscondicione']['condiciones_generales'] , $cuerpo);

                    $cuerpo = str_replace('_condicionescancelacion_', $terminos_condiciones[0]['Terminoscondicione']['condiciones_cancelacion'] , $cuerpo);




                    //Traemos el detalle de la reserva


                    $reservas = $this->Reserindividuale->find('all', 
                                                                    array('conditions'
                                                                      =>array('Reserindividuale.localizador'
                                                                      =>$idreserva )));

                    


                    $dias   = $reservas[0]['Reserindividuale']['dias'];
                    $cuerpo = str_replace('_cant_noches_', $dias, $cuerpo);



                    $ppn    = $reservas[0]['Reserindividuale']['precioxdia'];
                    $cuerpo = str_replace('_ppn_', $ppn, $cuerpo);

                    $ptp    = $reservas[0]['Reserindividuale']['total'];
                    $cuerpo = str_replace('_ptp_', $ptp, $cuerpo);
                    

                    //porcentaje 18%
                    $porc18 = $ptp * ( 18 / 100 );
                    $cuerpo = str_replace('_iva_', $porc18, $cuerpo);

                    //porcentaje 10%
                    $porc30 = $ptp * ( 10 / 100 );
                    $cuerpo = str_replace('_porcen_servicio_', $porc30, $cuerpo);

                    

                    //Observaciones cliente
                    $obser  = $reservas[0]['Reserindividuale']['obseraciones'];
                    $cuerpo = str_replace('_observaciones_cliente_', $obser, $cuerpo);




                    //Sacando los datos del tipo de habitaciones

                    $tipoha = $reservas[0]['Reserindividuale']['tipohabitaciones'];

                    $tipoha = str_replace('[','', $tipoha);
                    $tipoha = str_replace(']','', $tipoha);

                    


                    $auxtipohab = explode(',', $tipoha);

                    $b = count($auxtipohab);

                    $denominacionTipoH .= '';


                    for ($i=0; $i <= $b - 1 ; $i++) { 
                                         
                      $clear = str_replace('"', '', $auxtipohab[$i]);
                      

                      $auxclear = explode('|', $clear);

                      //Consultamos el tipo de habitacion
                      $idtipoh = $auxclear[0];

                      
                      


                      $den = $this->Tipohabitacione->find('all', ['conditions'=>['Tipohabitacione.id'=>$idtipoh[0]]]);

                      $denominacionTipoH .= 'Denominacion : '.$den[0]['Tipohabitacione']['denominacion'].'. <br>';
                      $denominacionTipoH .= 'Descripcion  : '.$den[0]['Tipohabitacione']['descripcion'].'. <br><br>';




                    }


                    


                    $cuerpo = str_replace('_det_habitacion_', $denominacionTipoH, $cuerpo);



                    // Sacando los datos del pax


                    $cp = $reservas[0]['Reserindividuale']['cantidad_personas'];

                    $cp = str_replace('[','', $cp);
                    $cp = str_replace(']','', $cp);

                    


                    $aux_cp = explode(',', $cp);

                    $b = count($aux_cp);

                    $cant_personas_ .= '';


                    for ($i=0; $i <= $b - 1 ; $i++) { 
                                         
                      $clear = str_replace('"', '', $aux_cp[$i]);
                      

                      $auxclear = explode('|', $clear);

                      //Consultamos el tipo de habitacion
                      $idtipoh = $auxclear[0];

                      
                      


                      $den = $this->Tipohabitacione->find('all', ['conditions'=>['Tipohabitacione.id'=>$idtipoh[0]]]);

                      $cant_personas_ .= $den[0]['Tipohabitacione']['denominacion'].': '.$auxclear[1];
                      




                    }



                    
                    $cuerpo = str_replace('_pax_',$cant_personas_, $cuerpo);

















                    $control = 0;
                    $cant = 0;


                    // Buscar todas las cantidades 
                    $tipohabitaciones2 = $this->Tipohabitacione->find('all');



                    $datatotal = '';

                    foreach ($tipohabitaciones2 as $id) { // Tipos de habitaciones
                        //echo $d['Tipohabitacione']['id'];

                    
                        $datatotal .= ''.$id['Tipohabitacione']['denominacion'].':'; 


                        foreach ($reservas as $data) { // Reservas
                          $arraydata = $data["Reserindividuale"]["tipohabitaciones"];

                          $arraydata =  substr($arraydata, 0, -1);
                          $arraydata =  substr($arraydata, 1);
                          
                          $explode1 = explode(',', $arraydata);

                          for ($i=0; $i <= count($explode1) - 1; $i++) { // 11
                              

                              $aux = substr($explode1[$i], 0, -1);
                              $aux = substr($aux, 1);
                              //echo '->'.$aux.'<br>'; 


                              $arraydata2 = explode('|', $aux);


                              



                              for ($j=0; $j <= count($arraydata2) - 2 ; $j++) { // 12

                                if ($id['Tipohabitacione']['id'] == $arraydata2[0]) {
                                    
                                    $arrayTipoHab[ $id['Tipohabitacione']['id'] ] += $arraydata2[1];                            

                                    
                                }

                             } // 12 END

                          }  // 11 END

                        } // Reservas END

                        if ($arrayTipoHab[ $id['Tipohabitacione']['id'] ]== '') {
                            
                            $datatotal .= ' 0 <br>';
                            
                        }else{

                            $datatotal .= ' '.$arrayTipoHab[ $id['Tipohabitacione']['id'] ].' <br> ';

                        }

                        
                    }// Tipos de habitaciones END


                    $datatotal  = substr($datatotal, 0, -1 );
                    $datatotal .= '';


                    //-----------------------------------> FIN DETALLE RESERVAS









					$linkanular = 'Si desea anular la reservacion por favor vaya a  http:&#47;&#47;hoteleria.tk&#47;Reserindividualespub&#47;anulate_reserva&#47;_idreserva_';

					$linkeditar = 'Si desea editar la reservacion por favor vaya a http:&#47;&#47;hoteleria.tk&#47;Reserindividualespub&#47;edit_basic&#47;_idreserva_';

					$cuerpo = str_replace('_linkanular_', $linkanular , $cuerpo);
					$cuerpo = str_replace('_linkeditar_', $linkeditar , $cuerpo);
	

					
					$cuerpo = str_replace('_idreserva_', $idreserva, $cuerpo);

          

          $cuerpo = str_replace('_detallereservas_', $datatotal , $cuerpo);

                   

//                    

                    

					mail($email,$subject,$cuerpo, $cabeceras);


					//Envio de copia a towers
				 mail('reservation@westonsuiteshotel.com','Nueva Reserva',$cuerpo, $cabeceras);








                    $this->Flash->success(__('Registro Guardado.'));   


                    //Descontamos los cupos
                    $idvisitante      = $this->Session->read('VISITANTE');

                    //Descontar cupos
                    $descontar = $this->Tmpreserva->find('all', ['conditions'=>['Tmpreserva.identificador'=>$idvisitante]]);


                    foreach ($descontar as $key) {
                          # code...

                        // Vaciar las variables
                        $tipohabitacione_id = $key['Tmpreserva']['tipohabitacione_id'];
                        $cantidad           = $key['Tmpreserva']['cantidadseleccionada'];
                        $fechae             = $key['Tmpreserva']['fecha_entrada'];
                        $fechas             = $key['Tmpreserva']['fecha_salida'];

                        //Sacar la cantidad de dias
                        $fecha1 = new DateTime($fechae);
                        $fecha2 = new DateTime($fechas);
                        $resultado = $fecha1->diff($fecha2);
                        //echo $resultado->format('%a');
                        $cantdias =  $resultado->format('%a');



                         $fecha1 = date ( 'Y-m-d' , $fecha1);
                         




                        for ($i=1; $i <= $cantdias ; $i++) { 



                          $cupos = $this->Cupo->find('all', ['conditions'=>
                                                              [
                                                               'Cupo.fecha'=>$fechae, 
                                                               'Cupo.tipohabitacione_id'=>$tipohabitacione_id
                                                              ]
                                                            ]);



                          $cantidadcupos = $cupos[0]['Cupo']['cantidad'];

                          $ncantidad = $cantidadcupos - $cantidad;

                          

                          $sqlUpdateCupos = "UPDATE cupos SET cantidad = '$ncantidad' WHERE fecha = '$fechae' AND tipohabitacione_id = $tipohabitacione_id";




                          $this->Cupo->query($sqlUpdateCupos); 



                          $nuevafecha = strtotime ( '+1 day' , strtotime ( $fechae ) ) ;
                          $fechae = date ( 'Y-m-d' , $nuevafecha );
                          
                        }


                    }
                    //Fin descontar cantidad de cupos


                    if(!empty($this->request->data['Reserindividuale']['inicio'])){
                        
                        return $this->redirect('/Hoteleria/index');
                    }else{
                      

                        return $this->redirect('/Hoteleria/index');
                    }
                }else{
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                }
                /*}else{
                    $this->Reserindividuale->rollback();
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                }*/
            } else {
                $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
            }

        }
        $tipoclientes     = $this->Reserindividuale->Tipocliente->find('list');
        $tipoclientesubs  = $this->Reserindividuale->Tipoclientesub->find('list');
        $clientes         = $this->Reserindividuale->Cliente->find('list');


        $tipohabitaciones = $this->Reserindividuale->Tipohabitacione->find('list', array('conditions'=>array('Tipohabitacione.cantninos >= ' => $personas)));


        $habitaciones = $this->Reserindividuale->Habitacione->find('list');
      

        $reserstatusindividuales = $this->Reserindividuale->Reserstatusindividuale->find('list');
        $resermultiples = $this->Reserindividuale->Resermultiple->find('list');
        $pais = $this->Pai->find('list');
        $fechai = explode("-",$fechai);
        $fechas = explode("-",$fechas);
        $fechai= $fechai[1]."/".$fechai[0]."/".$fechai[2];
        $fechas= $fechas[1]."/".$fechas[0]."/".$fechas[2];




        $aux_descuento = $this->Descuento->find('all');


        $this->set( 'estatus_descuento'    ,  $aux_descuento[0]['Descuento']['estatus'] );
        $this->set( 'porcentaje_descuento' ,  $aux_descuento[0]['Descuento']['monto']   );


        $this->Session->write('PORCENTAJE_DESCUENTO', $aux_descuento[0]['Descuento']['monto']);





        $this->set('id', $id);

        $this->set('terminos', $this->Terminoscondicione->find('all', array('conditions'=>array('Terminoscondicione.id'=>1))));
        

        $this->set('tphabit',  $this->Reserindividuale->Tipohabitacione->find('all') ) ;
          
        $this->set(compact('pais','tipoclientes', 'tipoclientesubs', 'clientes', 'tipohabitaciones', 'habitaciones', 'reserstatusindividuales', 'resermultiples', 'fechai', 'fechas', 'personas'));
    }



    public function aplicacardescuento(){

      $this->layout  = 'ajax';

      $monto = $this->Session->read('PRECIO');
      $porcentaje_descuento = $this->Session->read('PORCENTAJE_DESCUENTO');

      

      

      $descuentoaplicado = $this->Session->read('DESCUENTOAPLICADO');
      

      if ($descuentoaplicado == 0 ) {



        $consultaestatus = $this->Descuento->find('all');


        $estatus = $consultaestatus[0]['Descuento']['estatus'];


        if ($estatus == 1) {
          # code...
        
        


        $totaldescuento = $monto * ($porcentaje_descuento / 100);

        $totaldescuento = $monto - $totaldescuento;

        $this->set('porcentajeaplicado', 1);
        $this->set('totaldescuento', $totaldescuento);

         $this->Session->write('DESCUENTOAPLICADO', 1);


        }else{
         $this->set('porcentajeaplicado', 0);
          
        }




      }else{
         $this->set('porcentajeaplicado', 0);

      }


    }



    


  /**
     * Anular habitacion
     * 
     */
    public function anulate_reserva($localizador = null) {

        $this->Reserindividuale->updateAll(array('reserstatusindividuale_id'=>100), array('Reserindividuale.localizador'=>$localizador));



        $this->set('localizador', $localizador);
                   
                   
    }



    public function editardatoscliente(){

      if ($this->request->is('post')) 
      {

        

        $nombre_completo = $this->request->data['Cliente']['nombres'];
        $tdc             = $this->request->data['Cliente']['tdc'];
        $codtdc          = $this->request->data['Cliente']['codtdc'];
        $id              = $this->request->data['Cliente']['id'];


        //$this->Cliente->updateAll( [ 'nombre_completo' => '$nombre_completo', 'tdc' => $tdc, 'codtdc' => $codtdc ], [ 'Cliente.id' => $id ]  );

        $sqlupdate = "UPDATE clientes SET nombre_completo = '$nombre_completo',  tdc = $tdc, codtdc = $codtdc WHERE id = $id";

        $this->Cliente->query($sqlupdate);
    

      }
      else
      {

      }



    }



    public function edit_basic($localizador = null){

      // Consultamos los datos del cliente a traves del localizador

      $this->Reserindividuale->recursive = 0;

      $data = $this->Reserindividuale->find('all', [ 'conditions'=>['Reserindividuale.localizador'=>$localizador ] ]  );

      

      $this->set('encontrado',1);


      if (count($data) > 0)
      {
        
        $this->set('nombre_completo', $data[0]['Cliente']['nombre_completo']);
        $this->set('tdc', $data[0]['Cliente']['tdc']);
        $this->set('cod_tdc', $data[0]['Cliente']['codtdc']);
        $this->set('id', $data[0]['Cliente']['id']);
        
        $this->set('localizador', $localizador);


        if ($data[0]['Reserindividuale']['reserstatusindividuale_id'] == 100) {

          
          $this->set('encontrado',0);


        }



      }else{


        $this->set('encontrado',0);
      }

      
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Reserindividuale->exists($id)) {
            throw new NotFoundException(__('Invalid reserindividuale'));
        }
        if ($this->request->is(array('post', 'put'))) {

            $this->request->data['Reserindividuale']['tipohabitaciones'] = json_encode($this->request->data['Reserindividuale']['tipohabitaciones']);

            $this->request->data['Reserindividuale']['fecha_entrada'] = !empty($this->request->data['Reserindividuale']['fecha_entrada1']) ? formatfecha($this->request->data['Reserindividuale']['fecha_entrada1']) : '';
            $this->request->data['Reserindividuale']['fecha_salida']  = !empty($this->request->data['Reserindividuale']['fecha_salida1']) ? formatfecha($this->request->data['Reserindividuale']['fecha_salida1']) : '';


            if ($this->Reserindividuale->save($this->request->data)) {

                $this->Reserindividualeextra->deleteAll(array('Reserindividualeextra.reserindividuale_id'=>$id));
                $stop = 0;
                if(!empty($this->request->data["Reserindividuale"]["Nombre"])){
                    foreach($this->request->data["Reserindividuale"]["Nombre"] as $nombre){
                        $this->request->data["Reserindividualeextra"]["reserindividuale_id"] = $id;
                        $this->request->data["Reserindividualeextra"]["nombres"] = $nombre;
                        $this->Reserindividualeextra->create();
                        if ($this->Reserindividualeextra->save($this->request->data)){

                        }else{
                            $stop = 1;
                        }
                    }
                }
                if($stop==0){
                    $this->Flash->success(__('Registro Guardado.'));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                }
            } else {
                $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
            }
        } else {
            $options = array('conditions' => array('Reserindividuale.' . $this->Reserindividuale->primaryKey => $id));
            $this->request->data = $this->Reserindividuale->find('first', $options);
            $this->request->data['Reserindividuale']['fecha_entrada1'] = formatdmy($this->request->data['Reserindividuale']['fecha_entrada']);
            $this->request->data['Reserindividuale']['fecha_salida1'] = formatdmy($this->request->data['Reserindividuale']['fecha_salida']);
            $this->set('data', $this->request->data);
        }
        $tipoclientes = $this->Reserindividuale->Tipocliente->find('list');
        $tipoclientesubs = $this->Reserindividuale->Tipoclientesub->find('list', array('conditions'=>array('Tipoclientesub.tipocliente_id'=>$this->request->data['Reserindividuale']['tipocliente_id'], 'Tipoclientesub.id'=>$this->request->data['Reserindividuale']['tipoclientesub_id'])));
        $clientes = $this->Reserindividuale->Cliente->find('list', array('conditions'=>array('Cliente.id'=>$this->request->data['Reserindividuale']['cliente_id'])));
        $tipohabitaciones = $this->Reserindividuale->Tipohabitacione->find('list');
        $habitaciones = $this->Reserindividuale->Habitacione->find('all', array('conditions'=>array('Habitacione.tipohabitacione_id'=>$this->request->data['Reserindividuale']['tipohabitacione_id'], 'Habitacione.id'=>$this->request->data['Reserindividuale']['habitacione_id'])));
        $reserstatusindividuales = $this->Reserindividuale->Reserstatusindividuale->find('list');
        $resermultiples = $this->Reserindividuale->Resermultiple->find('list');
        $this->set(compact('tipoclientes', 'tipoclientesubs', 'clientes', 'tipohabitaciones', 'habitaciones', 'reserstatusindividuales', 'resermultiples'));
    }











    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit_3($id = null) {

        $this->set('id',$id);

        if (!$this->Reserindividuale->exists($id)) {
            throw new NotFoundException(__('Invalid reserindividuale'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Reserindividuale->save($this->request->data)) {
                $this->Reserindividualeextra->deleteAll(array('Reserindividualeextra.reserindividuale_id'=>$id));
                $stop = 0;
                if(!empty($this->request->data["Reserindividuale"]["Nombre"])){
                    foreach($this->request->data["Reserindividuale"]["Nombre"] as $nombre){
                        $this->request->data["Reserindividualeextra"]["reserindividuale_id"] = $id;
                        $this->request->data["Reserindividualeextra"]["nombres"] = $nombre;
                        $this->Reserindividualeextra->create();
                        if ($this->Reserindividualeextra->save($this->request->data)){

                        }else{
                            $stop = 1;
                        }
                    }
                }
                if($stop==0){

                    /*$auditoria['Auditoria']['user_id']             = $this->Session->read('USUARIO_ID');
                    $auditoria['Auditoria']['habitacione_id']      = $this->request->data['Reserindividuale']['habitacione_id'];
                    $auditoria['Auditoria']['reserindividuale_id'] = $id;
                    $auditoria['Auditoria']['cliente_id']          = $this->request->data['Reserindividuale']['cliente_id'];
                    $auditoria['Auditoria']['factura_id']          = 0;
                    $auditoria['Auditoria']['hora']                = DboSource::expression('NOW()');
                    $auditoria['Auditoria']['motivo']              = 'N/A';
                    $auditoria['Auditoria']['tipo_operacion']      = 'EDICION DE RESERVA';
                    $auditoria['Auditoria']['especifico']          = 'EDITADA';

                    $this->loadModel('Auditoria');
                    $this->Auditoria->save($auditoria);*/

                    $this->Flash->success(__('Registro Guardado.'));
                    return $this->redirect('/Reserindivistatuses/add_2/'.$id);
                } else {
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                }
            } else {
                $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
            }
        } else {
            $options = array('conditions' => array('Reserindividuale.' . $this->Reserindividuale->primaryKey => $id));
            $this->request->data = $this->Reserindividuale->find('first', $options);
            $this->set('data', $this->request->data);
        }
        $tipoclientes = $this->Reserindividuale->Tipocliente->find('all', array('conditions'=>array('Tipocliente.id'=>$this->request->data['Reserindividuale']['tipocliente_id'])));
        $tipoclientesubs = $this->Reserindividuale->Tipoclientesub->find('all', array('conditions'=>array('Tipoclientesub.tipocliente_id'=>$this->request->data['Reserindividuale']['tipocliente_id'], 'Tipoclientesub.id'=>$this->request->data['Reserindividuale']['tipoclientesub_id'])));
        $clientes = $this->Reserindividuale->Cliente->find('all', array('conditions'=>array('Cliente.id'=>$this->request->data['Reserindividuale']['cliente_id'])));
        $tipohabitaciones = $this->Reserindividuale->Tipohabitacione->find('list');
        $habitaciones = $this->Reserindividuale->Habitacione->find('all', array('conditions'=>array('Habitacione.tipohabitacione_id'=>$this->request->data['Reserindividuale']['tipohabitacione_id'], 'Habitacione.id'=>$this->request->data['Reserindividuale']['habitacione_id'])));
        $reserstatusindividuales = $this->Reserindividuale->Reserstatusindividuale->find('list');
        $resermultiples = $this->Reserindividuale->Resermultiple->find('list');
        $this->set(compact('tipoclientes', 'tipoclientesubs', 'clientes', 'tipohabitaciones', 'habitaciones', 'reserstatusindividuales', 'resermultiples'));
    }






    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit_4($id = null) {

        $this->set('id',$id);

        if (!$this->Reserindividuale->exists($id)) {
            throw new NotFoundException(__('Invalid reserindividuale'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Reserindividuale->save($this->request->data)) {
                $this->Reserindividualeextra->deleteAll(array('Reserindividualeextra.reserindividuale_id'=>$id));
                $stop = 0;
                if(!empty($this->request->data["Reserindividuale"]["Nombre"])){
                    foreach($this->request->data["Reserindividuale"]["Nombre"] as $nombre){
                        $this->request->data["Reserindividualeextra"]["reserindividuale_id"] = $id;
                        $this->request->data["Reserindividualeextra"]["nombres"] = $nombre;
                        $this->Reserindividualeextra->create();
                        if ($this->Reserindividualeextra->save($this->request->data)){

                        }else{
                            $stop = 1;
                        }
                    }
                }
                if($stop==0){

                    /*$auditoria['Auditoria']['user_id']             = $this->Session->read('USUARIO_ID');
                    $auditoria['Auditoria']['habitacione_id']      = $this->request->data['Reserindividuale']['habitacione_id'];
                    $auditoria['Auditoria']['reserindividuale_id'] = $id;
                    $auditoria['Auditoria']['cliente_id']          = $this->request->data['Reserindividuale']['cliente_id'];
                    $auditoria['Auditoria']['factura_id']          = 0;
                    $auditoria['Auditoria']['hora']                = DboSource::expression('NOW()');
                    $auditoria['Auditoria']['motivo']              = 'N/A';
                    $auditoria['Auditoria']['tipo_operacion']      = 'EDICION DE RESERVA';
                    $auditoria['Auditoria']['especifico']          = 'EDITADA';

                    $this->loadModel('Auditoria');
                    $this->Auditoria->save($auditoria);*/

                    $this->Flash->success(__('Registro Guardado.'));
                    return $this->redirect('/Reserindivistatuses/add_2/'.$id);
                } else {
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                }
            } else {
                $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
            }
        } else {
            $options = array('conditions' => array('Reserindividuale.' . $this->Reserindividuale->primaryKey => $id));
            $this->request->data = $this->Reserindividuale->find('first', $options);
            $this->set('data', $this->request->data);
        }
        $tipoclientes = $this->Reserindividuale->Tipocliente->find('all', array('conditions'=>array('Tipocliente.id'=>$this->request->data['Reserindividuale']['tipocliente_id'])));
        $tipoclientesubs = $this->Reserindividuale->Tipoclientesub->find('all', array('conditions'=>array('Tipoclientesub.tipocliente_id'=>$this->request->data['Reserindividuale']['tipocliente_id'], 'Tipoclientesub.id'=>$this->request->data['Reserindividuale']['tipoclientesub_id'])));
        $clientes = $this->Reserindividuale->Cliente->find('all', array('conditions'=>array('Cliente.id'=>$this->request->data['Reserindividuale']['cliente_id'])));
        $tipohabitaciones = $this->Reserindividuale->Tipohabitacione->find('list');
        $habitaciones = $this->Reserindividuale->Habitacione->find('all', array('conditions'=>array('Habitacione.tipohabitacione_id'=>$this->request->data['Reserindividuale']['tipohabitacione_id'], 'Habitacione.id'=>$this->request->data['Reserindividuale']['habitacione_id'])));
        $reserstatusindividuales = $this->Reserindividuale->Reserstatusindividuale->find('list');
        $resermultiples = $this->Reserindividuale->Resermultiple->find('list');
        $this->set(compact('tipoclientes', 'tipoclientesubs', 'clientes', 'tipohabitaciones', 'habitaciones', 'reserstatusindividuales', 'resermultiples'));
    }



    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit_2($id = null) {
        if (!$this->Reserindividuale->exists($id)) {
            throw new NotFoundException(__('Invalid reserindividuale'));
        }
        if ($this->request->is(array('post', 'put'))) {

            $this->request->data['Reserindividuale']['fecha_entrada'] = !empty($this->request->data['Reserindividuale']['fecha_entrada1']) ? formatfecha($this->request->data['Reserindividuale']['fecha_entrada1']) : '';
            $this->request->data['Reserindividuale']['fecha_salida'] = !empty($this->request->data['Reserindividuale']['fecha_salida1']) ? formatfecha($this->request->data['Reserindividuale']['fecha_salida1']) : '';

            if ($this->Reserindividuale->save($this->request->data)) {
                $this->Reserindividualeextra->deleteAll(array('Reserindividualeextra.reserindividuale_id'=>$id));
                $stop = 0;
                if(!empty($this->request->data["Reserindividuale"]["Nombre"])){
                    foreach($this->request->data["Reserindividuale"]["Nombre"] as $nombre){
                        $this->request->data["Reserindividualeextra"]["reserindividuale_id"] = $id;
                        $this->request->data["Reserindividualeextra"]["nombres"] = $nombre;
                        $this->Reserindividualeextra->create();
                        if ($this->Reserindividualeextra->save($this->request->data)){

                        }else{
                            $stop = 1;
                        }
                    }
                }
                if($stop==0){

                    /*$auditoria['Auditoria']['user_id']             = $this->Session->read('USUARIO_ID');
                    $auditoria['Auditoria']['habitacione_id']      = $this->request->data['Reserindividuale']['habitacione_id'];
                    $auditoria['Auditoria']['reserindividuale_id'] = $id;
                    $auditoria['Auditoria']['cliente_id']          = $this->request->data['Reserindividuale']['cliente_id'];
                    $auditoria['Auditoria']['factura_id']          = 0;
                    $auditoria['Auditoria']['hora']                = DboSource::expression('NOW()');
                    $auditoria['Auditoria']['motivo']              = 'N/A';
                    $auditoria['Auditoria']['tipo_operacion']      = 'EDICION DE RESERVA';
                    $auditoria['Auditoria']['especifico']          = 'EDITADA';

                    $this->loadModel('Auditoria');
                    $this->Auditoria->save($auditoria);*/

                    $this->Flash->success(__('Registro Guardado.'));
                    $this->redirect('/reservas/reservas');
                } else {
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                }
            } else {
                $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
            }
        } else {
            $options = array('conditions' => array('Reserindividuale.' . $this->Reserindividuale->primaryKey => $id));
            $this->request->data = $this->Reserindividuale->find('first', $options);
            $this->request->data['Reserindividuale']['fecha_entrada1'] = formatdmy($this->request->data['Reserindividuale']['fecha_entrada']);
            $this->request->data['Reserindividuale']['fecha_salida1'] = formatdmy($this->request->data['Reserindividuale']['fecha_salida']);
            $this->set('data', $this->request->data);
        }
        $tipoclientes = $this->Reserindividuale->Tipocliente->find('list');
        $tipoclientesubs = $this->Reserindividuale->Tipoclientesub->find('all', array('conditions'=>array('Tipoclientesub.tipocliente_id'=>$this->request->data['Reserindividuale']['tipocliente_id'], 'Tipoclientesub.id'=>$this->request->data['Reserindividuale']['tipoclientesub_id'])));
        $clientes = $this->Reserindividuale->Cliente->find('all', array('conditions'=>array('Cliente.id'=>$this->request->data['Reserindividuale']['cliente_id'])));
        $tipohabitaciones = $this->Reserindividuale->Tipohabitacione->find('list');
        $habitaciones = $this->Reserindividuale->Habitacione->find('all', array('conditions'=>array('Habitacione.tipohabitacione_id'=>$this->request->data['Reserindividuale']['tipohabitacione_id'], 'Habitacione.id'=>$this->request->data['Reserindividuale']['habitacione_id'])));
        $reserstatusindividuales = $this->Reserindividuale->Reserstatusindividuale->find('list');
        $resermultiples = $this->Reserindividuale->Resermultiple->find('list');
        $this->set(compact('tipoclientes', 'tipoclientesubs', 'clientes', 'tipohabitaciones', 'habitaciones', 'reserstatusindividuales', 'resermultiples'));
    }





    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null, $motivo = null) {

        $this->Reserindividuale->id = $id;

        if (!$this->Reserindividuale->exists()) {
            throw new NotFoundException(__('Invalid reserindividuale'));
        }

        $res = $this->Reserindividuale->find('first', ['conditions' => ['Reserindividuale.id' => $id] ]);

        $auditoria['Auditoria']['user_id']             = $this->Session->read('USUARIO_ID');
        $auditoria['Auditoria']['habitacione_id']      = $res['Reserindividuale']['habitacione_id'];
        $auditoria['Auditoria']['reserindividuale_id'] = $res['Reserindividuale']['id'];
        $auditoria['Auditoria']['resermultiple_id']    = 0;
        $auditoria['Auditoria']['cliente_id']          = $res['Reserindividuale']['cliente_id'];
        $auditoria['Auditoria']['factura_id']          = !empty($res['Factura']['id']) ? $res['Factura']['id'] : 0;
        $auditoria['Auditoria']['hora']                = DboSource::expression('NOW()');
        $auditoria['Auditoria']['motivo']              = $motivo;
        $auditoria['Auditoria']['tipo_operacion']      = 'ELIMINADO DE RESERVA';
        $auditoria['Auditoria']['especifico']          = 'ELIMINADO';
        $auditoria['Auditoria']['fecha_entrada']       = $res['Reserindividuale']['fecha_entrada'];
        $auditoria['Auditoria']['fecha_salida']        = $res['Reserindividuale']['fecha_salida'];

        $auditoria['Auditoria']['num_habitacion']      = $this->requestAction('habitaciones/get_numero/'.$res['Reserindividuale']['habitacione_id']);
        $auditoria['Auditoria']['capacidad']           = $this->requestAction('habitaciones/get_capacidad/'.$res['Reserindividuale']['habitacione_id']);
        $auditoria['Auditoria']['detalle_reserva']     = !empty($res['Reserindividuale']['detallereserva']) ? $res['Reserindividuale']['detallereserva'] : '';
        $auditoria['Auditoria']['costo_total']         = $res['Reserindividuale']['total'];
        $auditoria['Auditoria']['forma_pago']          = '-';
        $auditoria['Auditoria']['num_factura']         = ($res['Reserindividuale']['factura_id'] != 0) ? $res['Factura']['numero'] : 0;

        $this->loadModel('Auditoria');
        $this->Auditoria->save($auditoria);


        $rol = $this->Session->read('ROL');
        if($rol!=1){
            $this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
        }else{

            $this->request->allowMethod('post', 'delete', 'get');
            if ($this->Reserindividuale->delete()) {
                $this->Flash->success(__('El Registro fue eliminado.'));
            } else {
                $this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
            }
            return $this->redirect(array('action' => 'index'));
        }
    }


    /**
     * habitacion method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function habitacion($id = null) {
        $this->layout = 'ajax';
        $habitaciones = $this->Reserindividuale->Habitacione->find('list', array('fields'=>array('Habitacione.id', 'Habitacione.numhabitacion'), 'conditions'=>array('Habitacione.tipohabitacione_id'=>$id)));
        $this->set(compact('habitaciones'));
        $this->set('id', $id);

    }


    /**
     * fecha_entrada method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function fecha_entrada($id = null, $id2 = null) {
        $this->layout = 'ajax';
        $this->set('id', $id);
    }


    /**
     * precioxdia method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function precioxdia($id = null, $id2 = null) {
        $this->layout       = 'ajax';
        $conuntemp          = $this->Tipotemporada->find('count', array('conditions'=>array('Tipotemporada.fechadesde <='=>$id2, 'Tipotemporada.fechahasta >='=>$id2)));
        $tipo     = $this->Tipotemporada->find('all', array('conditions'=>array('Tipotemporada.fechadesde <='=>$id2, 'Tipotemporada.fechahasta >='=>$id2)));
        $tipohabitacione_id = $id;
        if($conuntemp>1){
            $listo = 0;
            foreach($tipotemporadas as $data){
                if($listo==0){
                    $listo      = $this->Pstemporada->find('count',   array('conditions'=>array('Pstemporada.tipohabitacione_id'=>$tipohabitacione_id, 'Pstemporada.tipotemporada_id'=>$data['Tipotemporada']['id'])));
                    $precios    = $this->Pstemporada->find('all',   array('conditions'=>array('Pstemporada.tipohabitacione_id'=>$tipohabitacione_id, 'Pstemporada.tipotemporada_id'=>$data['Tipotemporada']['id'])));
                }
            }
        }else{
            $tipotemporada_id   = isset($tipotemporadas[0]['Tipotemporada']['id'])?$tipotemporadas[0]['Tipotemporada']['id']:0;
            $precios            = $this->Pstemporada->find('all',   array('conditions'=>array('Pstemporada.tipohabitacione_id'=>$tipohabitacione_id, 'Pstemporada.tipotemporada_id'=>$tipotemporada_id)));
        }

        $this->set('value', isset($precios[0]['Pstemporada']['precio'])?$precios[0]['Pstemporada']['precio']:0);
    }

    /**
     * precioxdia method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function preciopordia($fecha_entrada = null, $fecha_salida = null,  $numhabitacion = null) {
        $this->layout = 'ajax';

        // Caso 1
        // Cuando la fecha de inicio esta dentro de una temporada y su fecha de salida esta fuera
        $caso1 = $this->Tipotemporada->find('all',
            array('conditions'=>array('Tipotemporada.fechadesde <= ' => $fecha_entrada,
                'Tipotemporada.fechahasta >= ' => $fecha_entrada),'limit'=>1));
        //var_dump($caso1);

        //Dentro
        $caso2 = $this->Tipotemporada->find('all',
            array('conditions'=>array('Tipotemporada.fechadesde <= ' => $fecha_salida,
                'Tipotemporada.fechahasta >= ' => $fecha_salida),'limit'=>1));
        //var_dump($caso2);

        $caso3 = $this->Tipotemporada->find('all',
            array('conditions'=>array('Tipotemporada.fechadesde = ' => $fecha_salida,
                'Tipotemporada.fechahasta = ' => $fecha_salida),'limit'=>1));
        //

        $conuntemp = $this->Tipotemporada->find('count',
            array('conditions'=>array('Tipotemporada.fechadesde >='=>$fecha_entrada,
                'Tipotemporada.fechahasta <='  => $fecha_salida)));
        //pr($todastemporadas);

        $auxfechadeentrada = $fecha_entrada;
        $auxfechasalidacaso2 = $fecha_salida;
        //Determino el tipo de habitacion



        $tipohabitacione_id = $this->Habitacione->find('all',
            array('conditions'=>array('Habitacione.id ' => $numhabitacion)));

        $tipohabitacione_id = !empty($tipohabitacione_id[0]['Habitacione']['tipohabitacione_id']) ? $tipohabitacione_id[0]['Habitacione']['tipohabitacione_id'] : '';


        $costototal = 0;
        $total = 0;

        $table  = "<table class='table'>";
        $table .= "<thead>";
        $table .= "<tr><th>Tipo Temporada</th>";
        $table .= "<th>Dias</th>";
        $table .= "<th>Costo</th><tr></thead>";
        $table .= "<tbody>";



        if ($caso1[0]['Tipotemporada']['id'] == 12 && $caso2[0]['Tipotemporada']['id'] == 12 ) {


            $todastemporadas = $this->Tipotemporada->find('all',
                array('conditions'=>array('Tipotemporada.fechadesde >='=>$fecha_entrada,
                    'Tipotemporada.fechahasta <='=>$fecha_salida) ) );

        }
        //Evitar que el caso se repita para temporada baja
        if ($caso1[0]['Tipotemporada']['id'] != 12 ) {
            //En este caso, la fecha inicial de la reserva cayo sobre una fecha festiva
            //tomare el ultimo dia de esa fecha festiva y le sumo un dia
            //lo tomare como el primer dia de la temporada normal
            $fecha = $caso1[0]['Tipotemporada']['fechahasta'];
            $nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
            $fecha_entrada = date ( 'Y-m-j' , $nuevafecha );


            //Calculo cuantas fechas hay feriados
            $fecha1 = explode('-', $auxfechadeentrada);
            $fecha2 = explode('-', $fecha);

            $dia1 = $fecha1[2];
            $mes1 = $fecha1[1];
            $anho1 = $fecha1[0];


            $dia2 = $fecha2[2];
            $mes2 = $fecha2[1];
            $anho2 = $fecha2[0];

            $timestamp1 = mktime(0,0,0,$mes1,$dia1,$anho1);

            $timestamp2 = mktime(0,0,0,$mes2,$dia2,$anho2);


            $segundos_diferencia =  ($timestamp1 - $timestamp2);

            $segundos_diferencia = abs($segundos_diferencia);


            $cantidadcaso1 =  ($segundos_diferencia / (60 * 60 * 24));

            $idtemporadacaso1 = $caso1[0]['Tipotemporada']['id'];

            $preciocaso1 = $this->Pstemporada->find('all',
                array('conditions'=>array('Pstemporada.tipotemporada_id'=>$idtemporadacaso1,
                    'Pstemporada.tipohabitacione_id'=>$tipohabitacione_id)));



            $table .= '<tr><td>'.$caso1[0]['Tipotemporada']['denominacion'].'</td>';
            $table .= '<td>'.$cantidadcaso1.'</td>';
            @$table .= '<td>'.($cantidadcaso1 * $preciocaso1[0]['Pstemporada']['precio']).'</td>';



            @$total += $cantidadcaso1 * $preciocaso1[0]['Pstemporada']['precio'];

            $todastemporadas = $this->Tipotemporada->find('all',
                array('conditions'=>array('Tipotemporada.fechadesde >='=>$fecha_entrada,
                    'Tipotemporada.fechahasta <='=>$fecha_salida) ) );

            $auxfechadeentrada = $fecha_entrada;
        }


        if ($caso2[0]['Tipotemporada']['id'] != 12 ) {
            //En este caso, la fecha inicial de la reserva cayo sobre una fecha festiva
            //tomare el ultimo dia de esa fecha festiva y le sumo un dia
            //lo tomare como el primer dia de la temporada normal
            $fecha = $caso2[0]['Tipotemporada']['fechadesde'];
            $nuevafecha = strtotime ( '-1 day' , strtotime ( $fecha ) ) ;
            $fecha_salida = date ( 'Y-m-j' , $nuevafecha );
            $fecha_salida = $fecha;




            //Calculo cuantas fechas hay feriados
            $fecha1 = explode('-', $fecha);
            $fecha2 = explode('-', $auxfechasalidacaso2);

            $dia1 = $fecha1[2];
            $mes1 = $fecha1[1];
            $anho1 = $fecha1[0];


            $dia2 = $fecha2[2];
            $mes2 = $fecha2[1];
            $anho2 = $fecha2[0];

            $timestamp1 = mktime(0,0,0,$mes1,$dia1,$anho1);

            $timestamp2 = mktime(0,0,0,$mes2,$dia2,$anho2);


            $segundos_diferencia =  ($timestamp1 - $timestamp2);

            $segundos_diferencia = abs($segundos_diferencia);


            $cantidadcaso2 =  ($segundos_diferencia / (60 * 60 * 24));

            $idtemporadacaso2 = $caso2[0]['Tipotemporada']['id'];

            $preciocaso2 = $this->Pstemporada->find('all',
                array('conditions'=>array('Pstemporada.tipotemporada_id'=>$idtemporadacaso2,
                    'Pstemporada.tipohabitacione_id'=>$tipohabitacione_id)));


            $auxtable = '<tr><td>'.$caso2[0]['Tipotemporada']['denominacion'].'</td>';
            $auxtable .= '<td>'.$cantidadcaso2.'</td>';
            @$auxtable .= '<td>'.($cantidadcaso2 * $preciocaso2[0]['Pstemporada']['precio']).'</td>';

            @$total += $cantidadcaso2 * @$preciocaso2[0]['Pstemporada']['precio'];

            // echo "nuevafecha".$fecha_entrada;

            $todastemporadas = $this->Tipotemporada->find('all',
                array('conditions'=>array('Tipotemporada.fechadesde >='=>$fecha_entrada,
                    'Tipotemporada.fechahasta <= ' => $fecha_salida) ) );

        }




        //      var_dump($todastemporadas);

        foreach ($todastemporadas as $tod) {


            $auxfechasalida = $tod['Tipotemporada']['fechadesde'];
            //echo $auxfechasalida;

            $conuntemp = $this->Tipotemporada->find('all',
                array('conditions'=>array('Tipotemporada.fechadesde <='=>$auxfechadeentrada,
                    'Tipotemporada.fechahasta >='=>$auxfechasalida)));

            //Saco el id de la temporada 1
            $idtemporada1 = $conuntemp[0]['Tipotemporada']['id'];
            $table .= "<tr><td>".$conuntemp[0]['Tipotemporada']['denominacion']."</td>";

            //Determino el costo
            $precio1 = $this->Pstemporada->find('all',
                array('conditions'=>array('Pstemporada.tipotemporada_id'=>$idtemporada1,
                    'Pstemporada.tipohabitacione_id'=>$tipohabitacione_id)));

            $precio1 = $precio1[0]['Pstemporada']['precio'];

            //Determino la cantidad de dias que dura esta fecha
            //echo $auxfechadeentrada.' - '.$auxfechasalida.'<br>';


            $fecha1 = explode('-', $auxfechadeentrada);
            $fecha2 = explode('-', $auxfechasalida);

            $dia1 = $fecha1[2];
            $mes1 = $fecha1[1];
            $anho1 = $fecha1[0];


            $dia2 = $fecha2[2];
            $mes2 = $fecha2[1];
            $anho2 = $fecha2[0];

            $timestamp1 = mktime(0,0,0,$mes1,$dia1,$anho1);

            $timestamp2 = mktime(0,0,0,$mes2,$dia2,$anho2);


            $segundos_diferencia =  ($timestamp1 - $timestamp2);

            $segundos_diferencia = abs($segundos_diferencia);
            $cantidad =  ($segundos_diferencia / (60 * 60 * 24));

            $table .= "<td>".$cantidad."</td>";
            $table .= "<td>".$precio1."</td></tr>";

            //echo "cant ".$cantidad.'<br>';
            $total += $cantidad * $precio1;
            /////////*****************************************************************************
            $auxfechadeentrada = $tod['Tipotemporada']['fechadesde'];
            $auxfechasalida    = $tod['Tipotemporada']['fechahasta'];


            $conuntemp = $this->Tipotemporada->find('all',
                array('conditions'=>array('Tipotemporada.fechadesde >='=>$auxfechadeentrada,
                    'Tipotemporada.fechahasta <='=>$auxfechasalida)));
            //Saco el id de la temporada 2
            $idtemporada2 = $conuntemp[0]['Tipotemporada']['id'];
            $table .= "<tr><td>".$conuntemp[0]['Tipotemporada']['denominacion']."</td>";
            //Determino el costo
            //
            //
            $precio2 = $this->Pstemporada->find('all',
                array('conditions'=>array('Pstemporada.tipotemporada_id'=>$idtemporada2,
                    'Pstemporada.tipohabitacione_id'=>$tipohabitacione_id)));



            $precio2 = $precio2[0]['Pstemporada']['precio'];


            //Determino la cantidad de dias que dura esta fecha
            //echo $auxfechadeentrada.' - '.$auxfechasalida.'<br>';
            $fecha1 = explode('-', $auxfechadeentrada);
            $fecha2 = explode('-', $auxfechasalida);

            $dia1 = $fecha1[2];
            $mes1 = $fecha1[1];
            $anho1 = $fecha1[0];


            $dia2 = $fecha2[2];
            $mes2 = $fecha2[1];
            $anho2 = $fecha2[0];

            $timestamp1 = mktime(0,0,0,$mes1,$dia1,$anho1);

            $timestamp2 = mktime(0,0,0,$mes2,$dia2,$anho2);


            $segundos_diferencia =  ($timestamp1 - $timestamp2);

            $segundos_diferencia = abs($segundos_diferencia);
            $cantidad =  ($segundos_diferencia / (60 * 60 * 24));
            //echo "cant ".$cantidad.'<br>';
            $total += $cantidad * $precio2;
            $table .= "<td>".$cantidad."</td>";
            $table .= "<td>".number_format($precio2,'2',',','.')."</td></tr>";


            $auxfechadeentrada = $tod['Tipotemporada']['fechahasta'];


        }

        //Se calculan los dias restante en temporada normal
        if ($conuntemp > 0) {
            # code...
            $conuntemp = $this->Tipotemporada->find('all',
                array('conditions'=>array('Tipotemporada.fechadesde <='=>$auxfechadeentrada,
                    'Tipotemporada.fechahasta >='=>$fecha_salida)));
            //Saco el id de la temporada 2
            $idtemporada2 = $conuntemp[0]['Tipotemporada']['id'];
            //Determino el costo
            $precio2 = $this->Pstemporada->find('all',
                array('conditions'=>array('Pstemporada.tipotemporada_id'=>$idtemporada2,
                    'Pstemporada.tipohabitacione_id'=>$tipohabitacione_id)));

            $precio2 = $precio2[0]['Pstemporada']['precio'];
            $table .= "<tr><td>".$conuntemp[0]['Tipotemporada']['denominacion']."</td>";
            //Determino la cantidad de dias que dura esta fecha
            //echo $auxfechadeentrada.' - '.$fecha_salida.'<br>';
            $fecha1 = explode('-', $auxfechadeentrada);
            $fecha2 = explode('-', $fecha_salida);

            $dia1 = $fecha1[2];
            $mes1 = $fecha1[1];
            $anho1 = $fecha1[0];


            $dia2 = $fecha2[2];
            $mes2 = $fecha2[1];
            $anho2 = $fecha2[0];

            $timestamp1 = mktime(0,0,0,$mes1,$dia1,$anho1);

            $timestamp2 = mktime(0,0,0,$mes2,$dia2,$anho2);


            $segundos_diferencia =  ($timestamp1 - $timestamp2);

            $segundos_diferencia = abs($segundos_diferencia);
            $cantidad =  ($segundos_diferencia / (60 * 60 * 24));
            //echo "cant ".$cantidad.'<br>';
            $total += $cantidad * $precio2;
            $table .= "<td>".$cantidad."</td>";
            $table .= "<td>".number_format($precio2,'2',',','.')."</td></tr>";

        }


        if ($conuntemp == 0) {

            $conuntemp = $this->Tipotemporada->find('all',
                array('conditions'=>array('Tipotemporada.fechadesde <='=>$fecha_entrada,
                    'Tipotemporada.fechahasta >='=>$fecha_salida)));
            //Saco el id de la temporada 1

            $tipohabitacione_id = $this->Habitacione->find('all',
                array('conditions'=>array('Habitacione.id ' => $numhabitacion)));
            $tipohabitacione_id = $tipohabitacione_id[0]['Habitacione']['tipohabitacione_id'];
            $idtemporada1 = $conuntemp[0]['Tipotemporada']['id'];
            $table .= "<tr><td>".$conuntemp[0]['Tipotemporada']['denominacion']."</td>";



            //Determino el costo
            $precio1 = $this->Pstemporada->find('all',
                array('conditions'=>array('Pstemporada.tipotemporada_id'=>$idtemporada1,
                    'Pstemporada.tipohabitacione_id'=>$tipohabitacione_id)));


            @$precio1 = $precio1[0]['Pstemporada']['precio'];



            //Determino la cantidad de dias que dura esta fecha
            //echo $auxfechadeentrada.' - '.$auxfechasalida.'<br>';


            $fecha1 = explode('-', $fecha_entrada);
            $fecha2 = explode('-', $fecha_salida);

            $dia1 = $fecha1[2];
            $mes1 = $fecha1[1];
            $anho1 = $fecha1[0];


            $dia2 = $fecha2[2];
            $mes2 = $fecha2[1];
            $anho2 = $fecha2[0];

            $timestamp1 = mktime(0,0,0,$mes1,$dia1,$anho1);

            $timestamp2 = mktime(0,0,0,$mes2,$dia2,$anho2);


            $segundos_diferencia =  ($timestamp1 - $timestamp2);

            $segundos_diferencia = abs($segundos_diferencia);
            $cantidad =  ($segundos_diferencia / (60 * 60 * 24));

            $table .= "<td>".$cantidad."</td>";
            $table .= "<td>".number_format($precio1,'2',',','.')."</td></tr>";

            //echo "cant ".$cantidad.'<br>';
            if($cantidad == 0){
                $cantidad = 1;
            }
            $total += $cantidad * $precio1;

        }

        if ($caso2[0]['Tipotemporada']['id'] != 12 ) {
            $table .= $auxtable;
        }

        $table .= '</table>';


        $this->set('value', $total);
        $this->set('table', $table);
    }



    /**
     * sub_cliente method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function sub_cliente($id = null) {
        $this->layout = 'ajax';
        $subclientes = $this->Reserindividuale->Tipoclientesub->find('list', array('fields'=>array('Tipoclientesub.id', 'Tipoclientesub.nombre'), 'conditions'=>array('Tipoclientesub.tipocliente_id'=>$id)));
        $this->set(compact('subclientes'));
        $this->set('id', $id);

    }

    /**
     * cliente method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function cliente($id = null, $id2 = null) {
        $this->layout = 'ajax';
        $clientes = $this->Reserindividuale->Cliente->find('list', array('fields'=>array('Cliente.id', 'Cliente.nombre_completo'), 'conditions'=>array('Cliente.tipocliente_id'=>$id, 'Cliente.tipoclientesub_id'=>$id2)));
        $this->set(compact('clientes'));

    }


    /**
     * cliente method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function cliente2() {
        $this->layout = 'ajax';

        $status_save = $this->Session->read('STATUS_SAVE');


        if ($status_save == 1) {
            # code...

            $idcliente = $this->Session->read('LAST_ID_CLIENT');

            $this->set('cliente', $this->Cliente->find('all', array('conditions'=>array('Cliente.id'=>$idcliente))));
            
        }else if($status_save == 0){

            $dni = $this->Session->read('DNI');

            $this->set('cliente', $this->Cliente->find('all', array('conditions'=>array('Cliente.dni'=> $dni))));

        }

        

    }




    /**
     * status method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function status($id = null) {
        return $this->redirect('/Reserindivistatuses/add/'.$id);
    }


    /**
     * reporte method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */

    public function reporte($id=null){
        $this->layout = 'pdf';
        App::import('Vendor', 'Fpdf', array('file' => 'fpdf181/fpdf.php'));
        $this->set('name', 'planilla_confirmacion_entrada_'.date('Y_m_d').".pdf");

    }


    public function precios($fechae = null, $fechas = null, $idtipohabitacion = null, $paxadicional = 0, $canthabitaciones = null, $tipotemporada_id = null){



        $this->layout = 'ajax';

    


        


        //Obtengo el precio por defecto
        $preciodef = $this->Setting->find('all');

        $preciodefpax = $preciodef[0]['Setting']['preciopax'];
        $preciodefpaxad = $preciodef[0]['Setting']['preciopaxadicional'];


        $totalpaxadicional = $paxadicional * $preciodefpaxad;

        $preciopornoche = $preciodefpax + $totalpaxadicional;

        $totalprecio = 0;



        $fecha1 = new DateTime($fechae);
        $fecha2 = new DateTime($fechas);
        $resultado = $fecha1->diff($fecha2);
        //echo $resultado->format('%a');
        $cantdias =  $resultado->format('%a');

        //$nuevafecha = strtotime ( '+'.$cantdias.' day' , strtotime ( $fechae ) ) ;
        $nuevafecha = date ( 'Y-m-d' , $fechae );

        for ($i=1; $i <= $cantdias ; $i++) {

            $evaluaprecio = '';
            //Sumando dia a dia la fecha
            


            // $evaluaprecio = $this->Cupo->find('all', array('conditions'=>array('Cupo.fecha'=>'2018-09-01', 'Cupo.tipohabitacione_id'=>'9')));
            // $fechae = parse_str($fechae);


            if ($i == 1) {


            $evaluaprecio = $this->Cupo->find('all', array('conditions'=> 
                                                     array('Cupo.fecha'=>$fechae, 
                                                           'Cupo.tipohabitacione_id' => $idtipohabitacion,
                                                           'Cupo.tipotemporada_id' => $tipotemporada_id

                                                            )));

            
            }
            
            if ($i > 1) {

            $evaluaprecio = $this->Cupo->find('all', array('conditions'=> 
                                                     array('Cupo.fecha'=> $fechae, 
                                                           'Cupo.tipohabitacione_id' => $idtipohabitacion,
                                                           'Cupo.tipotemporada_id' => $tipotemporada_id


                                                         )));
                      
            //echo "fechanf".$fechae;
            
            }          


           // echo $fechae." f ". $evaluaprecio[0]['Cupo']['precio'];
            
            $nuevafecha1 = strtotime ( '+1 day' , strtotime ( $fechae ) ) ;
            $fechae = date ( 'Y-m-d' , $nuevafecha1 );
            


            if (count($evaluaprecio) == 0) {
                //Al no estar definido un precio en cupo, tomo el precio por defecto desde la tabla settings
                $totalprecio += $preciodefpax + $totalpaxadicional;

            }else{
                //echo 'fecha '. $nuevafecha.' precio '.$evaluaprecio[0]['Cupo']['precio'];

                if ($paxadicional > 0) {
                  
                  $totalprecio = $totalprecio + $evaluaprecio[0]['Cupo']['precio'] + $evaluaprecio[0]['Cupo']['preciopaxadicional'];
                  
                }else if ($paxadicional <= 0) {
                  
                  $totalprecio = $totalprecio + $evaluaprecio[0]['Cupo']['precio'];
                  # code...
                }
               
            }





        }


        $this->Session->write('PRECIO', $totalprecio * $canthabitaciones);

        $this->Session->write('DESCUENTOAPLICADO', 0);

        $this->set('precio', $totalprecio * $canthabitaciones);
        $this->set('preciopornoche', $preciopornoche);

    }

}

