<?php
App::uses('AppController', 'Controller');
/**
 * Auditorias Controller
 *
 * @property Auditoria $Auditoria
 * @property PaginatorComponent $Paginator
 */
class HoteleriaController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	public $layout = 'contenttour/tour';
	public $uses = array('tipohabitacione','banner','Tmpreserva');
	



	public function index()
	 {
		$this->set('tipohabitacione', $this->tipohabitacione->find('all'));
		$this->set('banner', $this->banner->find('all'));		

		
    
    	if( $this->Session->Read('VISITANTE') )
    		{
        		$idvisitante = $this->Session->Read('VISITANTE');
    			$sqlDeleteTmp = "DELETE FROM tmpreservas WHERE identificador = $idvisitante";
    			$this->Tmpreserva->query($sqlDeleteTmp);


    		} else {
		
				$this->Session->Write('VISITANTE', time());
    		}	



	}



	
}
