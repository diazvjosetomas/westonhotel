<?php
App::uses('AppController', 'Controller');
/**
 * Facturapagos Controller
 *
 * @property Facturapago $Facturapago
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class FacturapagosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(8);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Facturapago->recursive = 0;
		//$this->set('facturapagos', $this->Paginator->paginate());
		  //$this->set('facturapagos', $this->Facturapago->find('all'));
		  return $this->redirect("/Facturas/pago/");	
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Facturapago->exists($id)) {
			throw new NotFoundException(__('Invalid facturapago'));
		}
		$options = array('conditions' => array('Facturapago.' . $this->Facturapago->primaryKey => $id));
		$this->set('facturapago', $this->Facturapago->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($id=null) {
		if ($this->request->is('post')) {
			$this->request->data['Cliente']['fecha_nacimiento'] = !empty($this->request->data['Cliente']['fecha_nacimiento1']) ? formatfecha($this->request->data['Cliente']['fecha_nacimiento1']) : '';
			$this->Facturapago->create();
			$user = $this->Session->read('USUARIO_ID');
			$this->request->data['Facturapago']['user_id'] = $user;
			if ($this->Facturapago->save($this->request->data)) {
				$id       = $this->request->data['Facturapago']['factura_id'];
				$facturas = $this->Facturapago->Factura->find('all', array('conditions'=>array('Factura.id'=>$id)));
				$pago     = $facturas[0]['Factura']['pagado'] + $this->request->data['Facturapago']['pago'];
				$this->request->data['Factura']['id'] = $id;
				$this->request->data['Factura']['pagado'] = $pago;
				if ($this->Facturapago->Factura->save($this->request->data)) {
				  $this->Flash->success(__('Registro Guardado.'));
				//return $this->redirect(array('action' => 'index'));
				  return $this->redirect("/Facturas/pago/");
				}else{
				  $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
				}
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
		$facturas  = $this->Facturapago->Factura->find('list', array('conditions'=>array('Factura.id'=>$id)));
		$facturas2 = $this->Facturapago->Factura->find('all', array('conditions'=>array('Factura.id'=>$id)));
		$facturatipopagos = $this->Facturapago->Facturatipopago->find('list');
		$this->set(compact('facturas', 'facturatipopagos', 'facturas2'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Facturapago->exists($id)) {
			throw new NotFoundException(__('Invalid facturapago'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Facturapago->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Facturapago.' . $this->Facturapago->primaryKey => $id));
			$this->request->data = $this->Facturapago->find('first', $options);
		}
		$facturas = $this->Facturapago->Factura->find('list');
		$facturatipopagos = $this->Facturapago->Facturatipopago->find('list');
		$this->set(compact('facturas', 'facturatipopagos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Facturapago->id = $id;
		if (!$this->Facturapago->exists()) {
			throw new NotFoundException(__('Invalid facturapago'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Facturapago->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
