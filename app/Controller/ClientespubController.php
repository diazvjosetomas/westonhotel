<?php
App::uses('AppController', 'Controller');
/**
 * Clientes Controller
 *
 * @property Cliente $Cliente
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ClientespubController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";
	public $uses = array('Cliente');

/*
*  *  beforeFilter check de session
*
*/	
	/*public function beforeFilter() {
		$this->checkSession(1);
	}*/

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Cliente->recursive = 0;
		//$this->set('clientes', $this->Paginator->paginate());
		  $this->set('clientes', $this->Cliente->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Cliente->exists($id)) {
			throw new NotFoundException(__('Invalid cliente'));
		}
		$options = array('conditions' => array('Cliente.' . $this->Cliente->primaryKey => $id));
		$this->set('cliente', $this->Cliente->find('first', $options));
	}



public function preferencias($idcliente = null){
	$this->layout = 'ajax';

	$this->set('preferencias', $this->Cliente->find('all', 
									[
										'conditions'=>[ 

											'Cliente.id ' => $idcliente

										    ]
										] 
									)
								);



}
public function descuento($idcliente = null){
	$this->layout = 'ajax';

	$this->set('preferencias', $this->Cliente->find('all', 
									[
										'conditions'=>[ 

											'Cliente.id ' => $idcliente

										    ]
										] 
									)
								);



}



/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'ajax';

		if ($this->request->is('post')) {

			$this->request->data['Cliente']['fvencimiento'] = $this->request->data['Cliente']['fvencimientoanho']." / ".$this->request->data['Cliente']['fvencimientomes'];
			


			$countcliente = $this->Cliente->find('all', array('conditions'=>array('Cliente.dni'=>$this->request->data['Cliente']['dni'])));
			$dni 		  = $this->request->data['Cliente']['dni'];
			$this->Session->write('DNI', $this->request->data['Cliente']['dni']);
			$this->Cliente->create();
			$this->request->data['Cliente']['fecha_nacimiento'] = !empty($this->request->data['Cliente']['fecha_nacimiento1']) ? formatfecha($this->request->data['Cliente']['fecha_nacimiento1']) : '1993/03/11';

			if (count($countcliente) == 0) {
				if ($this->Cliente->save($this->request->data)) {
				    $this->Session->write('STATUS_SAVE',1);
					//Guardamos el ultimo id ingresado en la base de datos en una sesion
				    $this->Session->write('LAST_ID_CLIENT', $this->Cliente->getLastInsertID());


					$this->Flash->success(__('Registro Guardado.'));


				//	return $this->redirect(array('action' => 'index'));
				} else {

					$this->Session->write('STATUS_SAVE',0);
					$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                
				}
			}else{

				$nombre_completo = $this->request->data['Cliente']['nombre_completo'];
				$direccion       = $this->request->data['Cliente']['direccion'];
				$ciudad          = $this->request->data['Cliente']['ciudad'];
				$telf            = $this->request->data['Cliente']['telf'];
				$correo			 = $this->request->data['Cliente']['correo'];
				$preferencias    = $this->request->data['Cliente']['preferencias'];
				$tdc    		 = $this->request->data['Cliente']['tdc'];
				$codtdc    		 = $this->request->data['Cliente']['codtdc'];
				$fvencimiento    = $this->request->data['Cliente']['fvencimientoanho']." / ".$this->request->data['Cliente']['fvencimientomes'];

				$sql = "UPDATE clientes SET nombre_completo = '$nombre_completo', 
				 							direccion       = '$direccion', 
				 							ciudad          = '$ciudad',
				 							telf			= '$telf',
				 							correo			= '$correo',
				 							preferencias	= '$preferencias',
				 							tdc				= '$tdc',
				 							codtdc 			= '$codtdc',
				 							fvencimiento    = '$fvencimiento'
				 							WHERE dni = $dni";
				
				$this->Cliente->query($sql);
				$this->Session->write('STATUS_SAVE',0);


			}








		}
		$tipoclientes = $this->Cliente->Tipocliente->find('list');
		$tipoclientesubs = $this->Cliente->Tipoclientesub->find('list');
		$pais = $this->Cliente->Pai->find('list');

		$this->set(compact('tipoclientes', 'tipoclientesubs','pais'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Cliente->exists($id)) {
			throw new NotFoundException(__('Invalid cliente'));
		}

		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Cliente']['fecha_nacimiento'] = !empty($this->request->data['Cliente']['fecha_nacimiento1']) ? formatfecha($this->request->data['Cliente']['fecha_nacimiento1']) : '1993/03/11';
			if ($this->Cliente->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Cliente.' . $this->Cliente->primaryKey => $id));
			$this->request->data = $this->Cliente->find('first', $options);
			$this->request->data['Cliente']['fecha_nacimiento1'] = formatdmy($this->request->data['Cliente']['fecha_nacimiento']);
		}
		$tipoclientes = $this->Cliente->Tipocliente->find('list');
		$tipoclientesubs = $this->Cliente->Tipoclientesub->find('list', array('conditions' => array('Tipoclientesub.tipocliente_id'=>$this->request->data["Cliente"]["tipocliente_id"])));
		$pais = $this->Cliente->Pai->find('list');
		$this->set(compact('tipoclientes', 'tipoclientesubs','pais'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Cliente->id = $id;
		if (!$this->Cliente->exists()) {
			throw new NotFoundException(__('Invalid cliente'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Cliente->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}




/**
 * sub_cliente method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function sub_cliente($id = null) {
		$this->layout = 'ajax';
	    $subclientes = $this->Cliente->Tipoclientesub->find('list', array('fields'=>array('Tipoclientesub.id', 'Tipoclientesub.nombre'), 'conditions'=>array('Tipoclientesub.tipocliente_id'=>$id)));
	    $this->set(compact('subclientes'));

	}
}
