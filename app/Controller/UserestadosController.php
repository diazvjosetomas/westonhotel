<?php
App::uses('AppController', 'Controller');
/**
 * Userestados Controller
 *
 * @property Userestado $Userestado
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class UserestadosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession();
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Userestado->recursive = 0;
		//$this->set('userestados', $this->Paginator->paginate());
		  $this->set('userestados', $this->Userestado->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Userestado->exists($id)) {
			throw new NotFoundException(__('Invalid userestado'));
		}
		$options = array('conditions' => array('Userestado.' . $this->Userestado->primaryKey => $id));
		$this->set('userestado', $this->Userestado->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Userestado->create();
			if ($this->Userestado->save($this->request->data)) {
				$this->Flash->success(__('The userestado has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The userestado could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Userestado->exists($id)) {
			throw new NotFoundException(__('Invalid userestado'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Userestado->save($this->request->data)) {
				$this->Flash->success(__('The userestado has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The userestado could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Userestado.' . $this->Userestado->primaryKey => $id));
			$this->request->data = $this->Userestado->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Userestado->id = $id;
		if (!$this->Userestado->exists()) {
			throw new NotFoundException(__('Invalid userestado'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Userestado->delete()) {
			$this->Flash->success(__('The userestado has been deleted.'));
		} else {
			$this->Flash->error(__('The userestado could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
