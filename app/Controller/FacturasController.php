<?php
App::uses('AppController', 'Controller');
/**
 * Facturas Controller
 *
 * @property Factura $Factura
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class FacturasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

	var $uses = array('Factura', 'Reserindividuale', 'Consumo', 'Consumoproducto', 'Pstemporada', 'Proproductos',
		              'Tipotemporada', 'Habitacione', 'Resermulhabitacione', 'Resermultiple');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(8);
	}




/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Factura->recursive = 0;
		//$this->set('facturas', $this->Paginator->paginate());
		  $this->set('facturas', $this->Factura->find('all'));
	}


	public function listados() {
		$this->Factura->recursive = 1;
		//$this->set('facturas', $this->Paginator->paginate());
		$sql = "SELECT * FROM facturas Factura, 
							  reserindividuales Reserindividuale,
							  facturapagos Facturapago,
							  facturatipopagos Facturatipopago

							  WHERE Factura.reserindividuale_id = Reserindividuale.id
							  AND Factura.id = Facturapago.factura_id
							  AND Facturapago.facturatipopago_id = Facturatipopago.id
							  ";

		  $this->set('facturas', $this->Factura->find('all'));
		  $this->set('facturas', $this->Factura->query($sql));
	}
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Factura->exists($id)) {
			throw new NotFoundException(__('Invalid factura'));
		}
		$options = array('conditions' => array('Factura.' . $this->Factura->primaryKey => $id));
		$this->set('factura', $this->Factura->find('first', $options));
	}

	public function impresion($id = null, $monto = null){
		$this->layout = 'pdf';
		App::import('Vendor', 'Fpdf', array('file' => 'fpdf181/fpdf.php'));

		$this->Factura->recursive = 1;
		$data = $this->Factura->find('all', array('conditions'=>array('Factura.id'=>$id)));
		$this->set('datafactura', $data);
		//pr($data);
		$id_reser = $data[0]['Factura']['reserindividuale_id'];

		$this->Reserindividuale->recursive = 1;
		$this->set('detallereserva', 
					$this->Reserindividuale->find('all', array('Reserindividuale.id'=>$id_reser)));


		$this->Consumo->recursive = 2;
		$this->set('data_printer', $this->Consumo->find('all',array('conditions'=>array('Consumo.reserindividuale_id'=>$id_reser))));
	}


	public function fiscal($id = null){
		$this->layout = 'ajax';
		$this->Consumo->recursive = 2;
		$data = $this->Factura->find('all', array('conditions'=>array('Factura.id'=>$id)));
		$id_reser = $data[0]['Factura']['reserindividuale_id'];

		$this->set('data_printer', $this->Consumo->find('all',array('conditions'=>array('Consumo.reserindividuale_id'=>$id_reser))));
	}




/**
 * add method
 *
 * @return void
 */
	public function add() {
		$r = $this->Factura->find('first', ['order' => ['Factura.id' => 'DESC']] );
		$number = !empty($r) ? $r['Factura']['numero'] : 0;
		if ($this->request->is('post')) {

			$this->request->data['Factura']['facturatipopago_id'] = 1; //TIPO DE PAGO CUENTA CORRIENTE
			$this->request->data['Factura']['fecha'] = !empty($this->request->data['Factura']['fecha1']) ? formatfecha($this->request->data['Factura']['fecha1']) : '';
			$this->Factura->create();
			$this->Factura->begin();
			if ($this->Factura->save($this->request->data)) {
				$id = $this->request->data['Factura']['cliente_id'];
				$reserindividuales = $this->Reserindividuale->find('all', array('conditions'=>array('Reserindividuale.cliente_id'=>$id,'Reserindividuale.reserstatusindividuale_id'=>5, 'Reserindividuale.facturado'=>0)));
	    		$resermultiples    = $this->Resermultiple->find('all',    array('conditions'=>array('Resermultiple.cliente_id'=>$id,   'Resermultiple.reserstatusmultiple_id'=>2,       'Resermultiple.facturado'=>0)));
	            $stop = 0;
	            foreach ($reserindividuales as $reserindividuale){
	                    $this->request->data['Reserindividuale']['id']        = $reserindividuale['Reserindividuale']['id'];
	                    $this->request->data['Reserindividuale']['facturado'] = 1;
	            	if ($this->Reserindividuale->save($this->request->data)){

					}else{
						$stop = 1;
					}
	           
	            }
	            foreach ($resermultiples as $resermultiple){
	            		$this->request->data['Resermultiple']['id']        = $resermultiple['Resermultiple']['id'];
	                    $this->request->data['Resermultiple']['facturado'] = 1;
	            	if ($this->Resermultiple->save($this->request->data)){

					}else{
						$stop = 1;
					}
	           
	            }
				if($stop==0){

                    $this->Factura->commit();
                	$this->Flash->success(__('Registro Guardado.'));
					return $this->redirect(array('action' => 'index'));

                }else{
                	$this->Factura->rollback();
                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
                }

			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
		$tipoclientes = $this->Factura->Tipocliente->find('list');
		$tipoclientesubs = $this->Factura->Tipoclientesub->find('list');
		$clientes = $this->Factura->Cliente->find('list');
		$this->set(compact('tipoclientes', 'tipoclientesubs', 'clientes', 'number'));
	}



	/**
	 * add method
	 *
	 * @return void
	 */
		public function add_2($tipocliente,$tiposubcliente,$cliente,$id_reserva, $tipo = null) {
			$r = $this->Factura->find('first', ['order' => ['Factura.id' => 'DESC']] );
			$number = !empty($r) ? $r['Factura']['numero'] : 0;
			if ($this->request->is('post')) {
				$this->request->data['Factura']['facturatipopago_id'] = 1;
				$this->request->data['Factura']['fecha'] = !empty($this->request->data['Factura']['fecha1']) ? formatfecha($this->request->data['Factura']['fecha1']) : '';
				$this->Factura->create();
				$this->Factura->begin();
				if ($this->Factura->save($this->request->data)) {
					$last_factura = $this->Factura->getLastInsertID();
					$id = $this->request->data['Factura']['cliente_id'];
					$reserindividuales = $this->Reserindividuale->find('all', array('conditions'=>array('Reserindividuale.cliente_id'=>$id,'Reserindividuale.reserstatusindividuale_id'=>5, 'Reserindividuale.facturado'=>0)));
		    		$resermultiples    = $this->Resermultiple->find('all',    array('conditions'=>array('Resermultiple.cliente_id'=>$id,   'Resermultiple.reserstatusmultiple_id'=>2,       'Resermultiple.facturado'=>0)));
		            $stop = 0;
		            foreach ($reserindividuales as $reserindividuale){

		                    $this->request->data['Reserindividuale']['id']        = $reserindividuale['Reserindividuale']['id'];
		                    $this->request->data['Reserindividuale']['facturado'] = 1;
		            	if ($this->Reserindividuale->save($this->request->data)){

						}else{
							$stop = 1;
						}
		           
		            }
		            foreach ($resermultiples as $resermultiple){
		            		$this->request->data['Resermultiple']['id']        = $resermultiple['Resermultiple']['id'];
		                    $this->request->data['Resermultiple']['facturado'] = 1;
		            	if ($this->Resermultiple->save($this->request->data)){

						}else{
							$stop = 1;
						}
		           
		            }
					if($stop==0){

						$this->loadModel('Auditoria');
						if($tipo == 'individual'){
							$datos = $this->Auditoria->find('first', [ 'conditions' => [ 
									 'Auditoria.reserindividuale_id' => $id_reserva 
								]
							]);
						}else{
							$datos = $this->Auditoria->find('first', [ 'conditions' => [ 
									 'Auditoria.reserstatusmultiple_id' => $id_reserva 
								]
							]);
						}

						$auditoria['Auditoria']['id'] = $datos['Auditoria']['id'];
						$auditoria['Auditoria']['factura_id'] = $last_factura;
						$auditoria['Auditoria']['num_factura'] = $this->request->data['Factura']['numero'];
						$auditoria['Auditoria']['total'] = $this->request->data['Factura']['total'];

						
						$this->Auditoria->save($auditoria);

	                    $this->Factura->commit();
	                	$this->Flash->success(__('Registro Guardado.'));
						return $this->redirect(array('action' => 'index','controller'=>'Facturas'));

	                }else{
	                	$this->Factura->rollback();
	                    $this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
	                }

				} else {
					$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
				}
			}
			$tipoclientes = $this->Factura->Tipocliente->find('list',array('conditions'=>array('Tipocliente.id '=>$tipocliente)));
			$tipoclientesubs = $this->Factura->Tipoclientesub->find('list',array('conditions'=>array('Tipoclientesub.id'=>$tiposubcliente)));
			$clientes = $this->Factura->Cliente->find('list',array('conditions'=>array('Cliente.id'=>$cliente)));
			$this->set('id_cliente', $cliente);
			$this->set('id_reserva', $id_reserva);
			$this->set(compact('tipoclientes', 'tipoclientesubs', 'clientes', 'number'));
		}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Factura->exists($id)) {
			throw new NotFoundException(__('Invalid factura'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Factura']['fecha'] = !empty($this->request->data['Factura']['fecha1']) ? formatfecha($this->request->data['Factura']['fecha1']) : '';
			if ($this->Factura->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Factura.' . $this->Factura->primaryKey => $id));
			$this->request->data = $this->Factura->find('first', $options);
			$this->request->data['Factura']['fecha1'] = formatdmy($this->request->data['Factura']['fecha']);
		}
		$tipoclientes = $this->Factura->Tipocliente->find('list');
		$tipoclientesubs = $this->Factura->Tipoclientesub->find('list');
		$clientes = $this->Factura->Cliente->find('list');
		$this->set(compact('tipoclientes', 'tipoclientesubs', 'clientes'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Factura->id = $id;
		if (!$this->Factura->exists()) {
			throw new NotFoundException(__('Invalid factura'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Factura->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * sub_cliente method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function sub_cliente($id = null) {
		$this->layout = 'ajax';
	    $subclientes = $this->Factura->Tipoclientesub->find('list', array('fields'=>array('Tipoclientesub.id', 'Tipoclientesub.nombre'), 'conditions'=>array('Tipoclientesub.tipocliente_id'=>$id)));
	    $this->set(compact('subclientes'));
	    $this->set('tipocliente_id',$id);
	}

/**
 * cliente method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cliente($id = null, $id2 = null) {
		$this->layout = 'ajax';
	    $clientes = $this->Factura->Cliente->find('list', array('conditions'=>array('Cliente.tipocliente_id'=>$id, 'Cliente.tipoclientesub_id'=>$id2)));
	    $this->set(compact('clientes'));
	}


/**
 * factura method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function factura($id = null, $id2 = null, $id3 = null) {
		$this->layout      = 'ajax';
        $reserindividuales = $this->Reserindividuale->find('all', array('conditions'=>array('Reserindividuale.cliente_id'=>$id,'Reserindividuale.reserstatusindividuale_id'=>5, 'Reserindividuale.facturado'=>0)));
        $resermultiples    = $this->Resermultiple->find('all',    array('conditions'=>array('Resermultiple.cliente_id'=>$id,   'Resermultiple.reserstatusmultiple_id'=>2,       'Resermultiple.facturado'=>0)));
        $or=array();
        $contar = 0;
        foreach ($reserindividuales as $key){
          $or[]['Consumo.reserindividuale_id'] = $key['Reserindividuale']['id'];
          $contar++;
        }
        $this->Consumo->recursive = 2;
        if($contar!=0){
           $consumos          = $this->Consumo->find('all', array('conditions'=>array ('OR' => $or )));
        }else{
           $consumos          = array();
        }
		$proproductos    = $this->Proproductos->find('all');
	    $this->set(compact('reserindividuales', 'consumos', 'proproductos', 'resermultiples'));
	}




/**
 * pago method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function pago($id = null, $id2 = null) {
		$this->set('facturas', $this->Factura->find('all', array('conditions'=>array('Factura.total !='=>'Factura.pagado') )));
	}


/**
 * pagar method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function pagar($id = null) {
      return $this->redirect("/Facturapagos/add/".$id);	
	}


   

}//FIN CLASS
?>
