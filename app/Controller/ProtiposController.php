<?php
App::uses('AppController', 'Controller');
/**
 * Protipos Controller
 *
 * @property Protipo $Protipo
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ProtiposController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(3);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Protipo->recursive = 0;
		//$this->set('protipos', $this->Paginator->paginate());
		  $this->set('protipos', $this->Protipo->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Protipo->exists($id)) {
			throw new NotFoundException(__('Invalid protipo'));
		}
		$options = array('conditions' => array('Protipo.' . $this->Protipo->primaryKey => $id));
		$this->set('protipo', $this->Protipo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Protipo->create();
			if ($this->Protipo->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Protipo->exists($id)) {
			throw new NotFoundException(__('Invalid protipo'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Protipo->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Protipo.' . $this->Protipo->primaryKey => $id));
			$this->request->data = $this->Protipo->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Protipo->id = $id;
		if (!$this->Protipo->exists()) {
			throw new NotFoundException(__('Invalid protipo'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Protipo->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
