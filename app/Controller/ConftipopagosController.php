<?php
App::uses('AppController', 'Controller');
/**
 * Conftipopagos Controller
 *
 * @property Conftipopago $Conftipopago
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ConftipopagosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(13);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Conftipopago->recursive = 0;
		//$this->set('conftipopagos', $this->Paginator->paginate());
		  $this->set('conftipopagos', $this->Conftipopago->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Conftipopago->exists($id)) {
			throw new NotFoundException(__('Invalid conftipopago'));
		}
		$options = array('conditions' => array('Conftipopago.' . $this->Conftipopago->primaryKey => $id));
		$this->set('conftipopago', $this->Conftipopago->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Conftipopago->create();
			if ($this->Conftipopago->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Conftipopago->exists($id)) {
			throw new NotFoundException(__('Invalid conftipopago'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Conftipopago->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Conftipopago.' . $this->Conftipopago->primaryKey => $id));
			$this->request->data = $this->Conftipopago->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Conftipopago->id = $id;
		if (!$this->Conftipopago->exists()) {
			throw new NotFoundException(__('Invalid conftipopago'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Conftipopago->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
