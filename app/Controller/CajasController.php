<?php
App::uses('AppController', 'Controller');
/**
 * Cajas Controller
 *
 * @property Caja $Caja
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CajasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(9);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Caja->recursive = 0;
		//$this->set('cajas', $this->Paginator->paginate());
		  $this->set('cajas', $this->Caja->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Caja->exists($id)) {
			throw new NotFoundException(__('Invalid caja'));
		}
		$options = array('conditions' => array('Caja.' . $this->Caja->primaryKey => $id));
		$this->set('caja', $this->Caja->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Caja->create();
			if ($this->Caja->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Caja->exists($id)) {
			throw new NotFoundException(__('Invalid caja'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Caja->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Caja.' . $this->Caja->primaryKey => $id));
			$this->request->data = $this->Caja->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Caja->id = $id;
		if (!$this->Caja->exists()) {
			throw new NotFoundException(__('Invalid caja'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Caja->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
