<?php
App::uses('AppController', 'Controller');
/**
 * Auditorias Controller
 *
 * @property Auditoria $Auditoria
 * @property PaginatorComponent $Paginator
 */
class AuditoriasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	public $layout = 'dashbord';

	public function beforeFilter() {
		$this->checkSession(11);
	}


	public function index() {
		$auditorias = $this->Auditoria->find('all', [
				/*'conditions' => 
				[
					'Auditoria.especifico NOT LIKE' => '%ELIMINADO%',
					'Auditoria.especifico NOT LIKE' => '%EGRESADA%',
				]*/
			]
		);
		$this->set(compact('auditorias'));
	}

	public function promedios(){
		if($this->request->is('post')){
			$dias = 0;

			$fecha_1 = formatfecha($this->request->data['Auditoria']['fecha_1']);
			$fecha_2 = formatfecha($this->request->data['Auditoria']['fecha_2']);
			//pr($this->request->data);


			if( $fecha_1 != '' && $fecha_2 != '' ){

				$date1 = new DateTime($fecha_1);
				$date2 = new DateTime($fecha_2);
				$diff = $date1->diff($date2);
				$dias = $diff->days;

				$conditions1 = array('Auditoria.fecha_entrada BETWEEN ? and ?' => array($fecha_1, $fecha_2));
				$conditions2 = array('Auditoria.fecha_salida BETWEEN ? and ?' => array($fecha_1, $fecha_2));

				$auditorias = $this->Auditoria->find('all', [
					'conditions' => [
						'OR' =>[
							[ $conditions1 ],
							[ $conditions2 ]
						]
					]
				]);
			}else{

				$this->Flash->error(__('Debe seleccionar ambas fechas!'));
			}

			if($dias == 0){
				$dias = 1;
			}
			

			
		}

		$this->set(compact('auditorias', 'dias', 'fecha_1', 'fecha_2'));
	}



	
}
