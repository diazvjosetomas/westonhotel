<?php
App::uses('AppController', 'Controller');
/**
 * Conteocupos Controller
 *
 * @property Conteocupo $Conteocupo
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ConteocuposController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Conteocupo->recursive = 0;
		$this->set('conteocupos', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Conteocupo->exists($id)) {
			throw new NotFoundException(__('Invalid conteocupo'));
		}
		$options = array('conditions' => array('Conteocupo.' . $this->Conteocupo->primaryKey => $id));
		$this->set('conteocupo', $this->Conteocupo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Conteocupo->create();
			if ($this->Conteocupo->save($this->request->data)) {
				$this->Flash->success(__('The conteocupo has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The conteocupo could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Conteocupo->exists($id)) {
			throw new NotFoundException(__('Invalid conteocupo'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Conteocupo->save($this->request->data)) {
				$this->Flash->success(__('The conteocupo has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The conteocupo could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Conteocupo.' . $this->Conteocupo->primaryKey => $id));
			$this->request->data = $this->Conteocupo->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Conteocupo->id = $id;
		if (!$this->Conteocupo->exists()) {
			throw new NotFoundException(__('Invalid conteocupo'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Conteocupo->delete()) {
			$this->Flash->success(__('The conteocupo has been deleted.'));
		} else {
			$this->Flash->error(__('The conteocupo could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
