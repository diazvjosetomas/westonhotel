<?php
App::uses('AppController', 'Controller');
/**
 * Ctacorrientes Controller
 *
 * @property Ctacorriente $Ctacorriente
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CtacorrientesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(4);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Ctacorriente->recursive = 0;
		//$this->set('ctacorrientes', $this->Paginator->paginate());
		  $this->set('ctacorrientes', $this->Ctacorriente->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Ctacorriente->exists($id)) {
			throw new NotFoundException(__('Invalid ctacorriente'));
		}
		$options = array('conditions' => array('Ctacorriente.' . $this->Ctacorriente->primaryKey => $id));
		$this->set('ctacorriente', $this->Ctacorriente->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Ctacorriente->create();
			if ($this->Ctacorriente->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
		$tipoclientes = $this->Ctacorriente->Tipocliente->find('list');
		$tipoclientesubs = $this->Ctacorriente->Tipoclientesub->find('list');
		$this->set(compact('tipoclientes', 'tipoclientesubs'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Ctacorriente->exists($id)) {
			throw new NotFoundException(__('Invalid ctacorriente'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Ctacorriente->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Ctacorriente.' . $this->Ctacorriente->primaryKey => $id));
			$this->request->data = $this->Ctacorriente->find('first', $options);
		}
		$tipoclientes = $this->Ctacorriente->Tipocliente->find('list', array('conditions' => array('Tipocliente.id'=>$this->request->data["Ctacorriente"]["tipocliente_id"])));
		$tipoclientesubs = $this->Ctacorriente->Tipoclientesub->find('list', array('conditions' => array('Tipoclientesub.tipocliente_id'=>$this->request->data["Ctacorriente"]["tipocliente_id"], 'Tipoclientesub.id'=>$this->request->data["Ctacorriente"]["tipoclientesub_id"])));
		$clientes = $this->Ctacorriente->Cliente->find('list', array('conditions' => array('Cliente.tipocliente_id'=>$this->request->data["Ctacorriente"]["tipocliente_id"], 'Cliente.tipoclientesub_id'=>$this->request->data["Ctacorriente"]["tipoclientesub_id"], 'Cliente.id'=>$this->request->data["Ctacorriente"]["cliente_id"])));
		$this->set(compact('tipoclientes', 'tipoclientesubs', 'clientes'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Ctacorriente->id = $id;
		if (!$this->Ctacorriente->exists()) {
			throw new NotFoundException(__('Invalid ctacorriente'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Ctacorriente->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * sub_cliente method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function sub_cliente($id = null) {
		$this->layout = 'ajax';
	    $subclientes = $this->Ctacorriente->Tipoclientesub->find('list', array('fields'=>array('Tipoclientesub.id', 'Tipoclientesub.nombre'), 'conditions'=>array('Tipoclientesub.tipocliente_id'=>$id)));
	    $this->set(compact('subclientes'));
	    $this->set('tipocliente_id',$id);
	}

/**
 * cliente method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cliente($id = null, $id2 = null) {
		$this->layout = 'ajax';
	    $clientes = $this->Ctacorriente->Cliente->find('list', array('conditions'=>array('Cliente.tipocliente_id'=>$id, 'Cliente.tipoclientesub_id'=>$id2)));
	    $this->set(compact('clientes'));
	}
}
