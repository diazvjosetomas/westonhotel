<?php
App::uses('AppController', 'Controller');
/**
 * Terminoscondiciones Controller
 *
 * @property Terminoscondicione $Terminoscondicione
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class TerminoscondicionesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

	/*
	** var de layout
	*
	*/
		public $layout = "dashbord";


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Terminoscondicione->recursive = 0;
		$this->set('terminoscondiciones', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Terminoscondicione->exists($id)) {
			throw new NotFoundException(__('Invalid terminoscondicione'));
		}
		$options = array('conditions' => array('Terminoscondicione.' . $this->Terminoscondicione->primaryKey => $id));
		$this->set('terminoscondicione', $this->Terminoscondicione->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Terminoscondicione->create();
			if ($this->Terminoscondicione->save($this->request->data)) {
				$this->Flash->success(__('The terminoscondicione has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The terminoscondicione could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Terminoscondicione->exists($id)) {
			throw new NotFoundException(__('Invalid terminoscondicione'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Terminoscondicione->save($this->request->data)) {
				$this->Flash->success(__('The terminoscondicione has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The terminoscondicione could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Terminoscondicione.' . $this->Terminoscondicione->primaryKey => $id));
			$this->request->data = $this->Terminoscondicione->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Terminoscondicione->id = $id;
		if (!$this->Terminoscondicione->exists()) {
			throw new NotFoundException(__('Invalid terminoscondicione'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Terminoscondicione->delete()) {
			$this->Flash->success(__('The terminoscondicione has been deleted.'));
		} else {
			$this->Flash->error(__('The terminoscondicione could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
