<?php
App::uses('AppController', 'Controller');
/**
 * Cajaacierreturnos Controller
 *
 * @property Cajaacierreturno $Cajaacierreturno
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CajaacierreturnosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/*
** var de layout
*
*/
	public $layout = "dashbord";

/*
*  *  beforeFilter check de session
*
*/	
	public function beforeFilter() {
		$this->checkSession(9);
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		//$this->Cajaacierreturno->recursive = 0;
		//$this->set('cajaacierreturnos', $this->Paginator->paginate());
		  $this->set('cajaacierreturnos', $this->Cajaacierreturno->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Cajaacierreturno->exists($id)) {
			throw new NotFoundException(__('Invalid cajaacierreturno'));
		}
		$options = array('conditions' => array('Cajaacierreturno.' . $this->Cajaacierreturno->primaryKey => $id));
		$this->set('cajaacierreturno', $this->Cajaacierreturno->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Cajaacierreturno->create();
			if ($this->Cajaacierreturno->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		}
		$cajas = $this->Cajaacierreturno->Caja->find('list');
		$cajaturnos = $this->Cajaacierreturno->Cajaturno->find('list');
		$users = $this->Cajaacierreturno->User->find('list');
		$this->set(compact('cajas', 'cajaturnos', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Cajaacierreturno->exists($id)) {
			throw new NotFoundException(__('Invalid cajaacierreturno'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Cajaacierreturno->save($this->request->data)) {
				$this->Flash->success(__('Registro Guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('Registro no Guardado. Por favor, inténtelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('Cajaacierreturno.' . $this->Cajaacierreturno->primaryKey => $id));
			$this->request->data = $this->Cajaacierreturno->find('first', $options);
		}
		$cajas = $this->Cajaacierreturno->Caja->find('list');
		$cajaturnos = $this->Cajaacierreturno->Cajaturno->find('list');
		$users = $this->Cajaacierreturno->User->find('list');
		$this->set(compact('cajas', 'cajaturnos', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Cajaacierreturno->id = $id;
		if (!$this->Cajaacierreturno->exists()) {
			throw new NotFoundException(__('Invalid cajaacierreturno'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Cajaacierreturno->delete()) {
			$this->Flash->success(__('El Registro fue eliminado.'));
		} else {
			$this->Flash->error(__('El Registro no fue eliminado. Por favor, inténtelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
