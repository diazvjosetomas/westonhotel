<?php
App::uses('AppController', 'Controller');
/**
 * Tipohabitaciones Controller
 *
 * @property Tipohabitacione $Tipohabitacione
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class TipohabitacionesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');
    public $layout     = 'dashbord';

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Tipohabitacione->recursive = 0;
		$this->set('tipohabitaciones', $this->Paginator->paginate());
	}


	/**
	 * index method
	 *
	 * @return void
	 */
		public function consulta($id = null) {
			$this->layout = 'ajax';
			$this->Tipohabitacione->recursive = 0;
			$this->set('data', $this->Tipohabitacione->find('all', array('conditions'=>array('Tipohabitacione.id'=>$id))));
		}


	/**
	 * index method
	 *
	 * @return void
	 */
		public function consultaespecial($id = null, $cantpersonas) {
			$this->layout = 'ajax';
			$this->Tipohabitacione->recursive = 0;
			$this->set('data', $this->Tipohabitacione->find('all', array('conditions'=>array('Tipohabitacione.id'=>$id))));
			$this->set('cantpersonas', $cantpersonas);
		}
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Tipohabitacione->exists($id)) {
			throw new NotFoundException(__('Invalid tipohabitacione'));
		}
		$options = array('conditions' => array('Tipohabitacione.' . $this->Tipohabitacione->primaryKey => $id));
		$this->set('tipohabitacione', $this->Tipohabitacione->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {

			$c = $this->Tipohabitacione->find('all', array('conditions'=>array('Tipohabitacione.ref'=>$this->request->data['Tipohabitacione']['ref'])));

			if (count($c) > 0) {
				$this->Flash->error(__('Esta referencia ya esta siendo usada, indica otra'));
			}else{
				


				$this->Tipohabitacione->create();


			if (!empty($this->request->data['Tipohabitacione']['file']) && imagenType($this->request->data['Tipohabitacione']['file']['type']) != false) {

				               $storeFolder = 'upload';
				               $tempFile    = $this->request->data['Tipohabitacione']['file']['tmp_name'];
				               $targetPath  = ROOT . DS .'app'. DS .'webroot'. DS .'img'. DS . $storeFolder . DS;
				               $extencion   = imagenType($this->request->data['Tipohabitacione']['file']['type']);


				               $renameFile  = 'a_'.date('dmY_his').$extencion;
				               $targetFile  = $targetPath.$renameFile;
				               if(move_uploaded_file($tempFile,$targetFile)){
				                   $tipo_file = $this->request->data['Tipohabitacione']['file']['type'];
				                   $nombre_file = $renameFile;;
				                   
				                   $this->request->data['Tipohabitacione']['file']    = $nombre_file;
				               }else{
				                   
				                   $this->request->data['Tipohabitacione']['file'] = "_";
				               }
				               if ($extencion != '.jpeg') {
				               	if ($extencion != '.png') {
				               		if ( $extencion != '.jpg') {
				               		$this->Flash->error(__('Registro no Guardado. Por favor agregue una file con extención JPG, JPEG o PNG.'.$extencion));
				                  	return $this->redirect(array('controller' => 'Tipohabitaciones','action' => 'index'));			 			
				               		}else{}
				               		
				               	}else{}

				               }else{}

				           }else{
				           	$this->request->data['Tipohabitacione']['file'] = '*';


				           }



				if ($this->Tipohabitacione->save($this->request->data)) {
					$this->Flash->success(__('The tipohabitacione has been saved.'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Flash->error(__('The tipohabitacione could not be saved. Please, try again.'));
				}
			}


			
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        if (!$this->Tipohabitacione->exists($id)) {
            throw new NotFoundException(__('Invalid tipohabitacione'));
        }
        if ($this->request->is(array('post', 'put'))) {

            $this->Tipohabitacione->create();

            if (isset($this->request->data['Tipohabitacione']['file'])) {

                if (!empty($this->request->data['Tipohabitacione']['file']) && imagenType($this->request->data['Tipohabitacione']['file']['type']) != false) {

                    $storeFolder = 'upload';
                    $tempFile    = $this->request->data['Tipohabitacione']['file']['tmp_name'];
                    $targetPath  = ROOT . DS .'app'. DS .'webroot'. DS .'img'. DS . $storeFolder . DS;
                    $extencion   = imagenType($this->request->data['Tipohabitacione']['file']['type']);


                    $renameFile  = 'a_'.date('dmY_his').$extencion;
                    $targetFile  = $targetPath.$renameFile;
                    if(move_uploaded_file($tempFile,$targetFile)){
                        $tipo_file = $this->request->data['Tipohabitacione']['file']['type'];
                        $nombre_file = $renameFile;

                        $this->request->data['Tipohabitacione']['file']    = $nombre_file;
                    }else{

                        $this->request->data['Tipohabitacione']['file'] = "_";
                    }
                    if ($extencion != '.jpeg') {
                        if ($extencion != '.png') {
                            if ( $extencion != '.jpg') {
                                $this->Flash->error(__('Registro no Guardado. Por favor agregue una file con extención JPG, JPEG o PNG.'.$extencion));
                                return $this->redirect(array('controller' => 'Tipohabitaciones','action' => 'index'));
                            }else{}

                        }else{}

                    }else{}

                }else{
                    $imagen = $this->Tipohabitacione->find('all', ['conditions'=>['Tipohabitacione.id'=>$id]]);


                    $this->request->data['Tipohabitacione']['file'] = $imagen[0]['Tipohabitacione']['file'];


                }

            }

            if ($this->Tipohabitacione->save($this->request->data)) {
                $this->Flash->success(__('The tipohabitacione has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The tipohabitacione could not be saved. Please, try again.'));
            }

        } else {
            $options = array('conditions' => array('Tipohabitacione.' . $this->Tipohabitacione->primaryKey => $id));
            $this->request->data = $this->Tipohabitacione->find('first', $options);


        }
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Tipohabitacione->id = $id;
		if (!$this->Tipohabitacione->exists()) {
			throw new NotFoundException(__('Invalid tipohabitacione'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Tipohabitacione->delete()) {
			$this->Flash->success(__('The tipohabitacione has been deleted.'));
		} else {
			$this->Flash->error(__('The tipohabitacione could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}