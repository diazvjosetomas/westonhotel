<section class="content-header">
  <h1>
    Sistema de Encuestas
    <small><?php echo __('Terminoscondiciones'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Terminoscondiciones'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Edit Terminoscondicione'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Terminoscondicione', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
			
echo $this->Form->input('id', array('class'=>'form-horizontal'));	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Terminoscondicionecondiciones_generales">condiciones_generales</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('condiciones_generales', array('id'=>'Terminoscondicionecondiciones_generales', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Terminoscondicionecondiciones_cancelacion">condiciones_cancelacion</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('condiciones_cancelacion', array('id'=>'Terminoscondicionecondiciones_cancelacion', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Terminoscondicionecondiciones_pago">condiciones_pago</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('condiciones_pago', array('id'=>'Terminoscondicionecondiciones_pago', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Terminoscondicionetexto_aceptar_reserva">texto_aceptar_reserva</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('texto_aceptar_reserva', array('id'=>'Terminoscondicionetexto_aceptar_reserva', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Terminoscondicionetexto_sin_licencia_disponibilidad">texto_sin_licencia_disponibilidad</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('texto_sin_licencia_disponibilidad', array('id'=>'Terminoscondicionetexto_sin_licencia_disponibilidad', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Terminoscondicionepolitica_privacidad">politica_privacidad</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('politica_privacidad', array('id'=>'Terminoscondicionepolitica_privacidad', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Terminoscondicione.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Terminoscondicione.id'))); ?></li>
				<li><?php echo $this->Html->link(__('List Terminoscondiciones'), array('action' => 'index')); ?></li>
			</ul>
	</div>
<?php */ ?>