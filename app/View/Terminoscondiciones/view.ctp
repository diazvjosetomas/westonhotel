<section class="content-header">
  <h1>
    Sistema de Encuestas
    <small><?php echo __('Terminoscondiciones'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Terminoscondiciones'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Terminoscondiciones'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($terminoscondicione['Terminoscondicione']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Condiciones Generales'); ?></dt>
		<dd>
			<?php echo h($terminoscondicione['Terminoscondicione']['condiciones_generales']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Condiciones Cancelacion'); ?></dt>
		<dd>
			<?php echo h($terminoscondicione['Terminoscondicione']['condiciones_cancelacion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Condiciones Pago'); ?></dt>
		<dd>
			<?php echo h($terminoscondicione['Terminoscondicione']['condiciones_pago']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Texto Aceptar Reserva'); ?></dt>
		<dd>
			<?php echo h($terminoscondicione['Terminoscondicione']['texto_aceptar_reserva']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Texto Sin Licencia Disponibilidad'); ?></dt>
		<dd>
			<?php echo h($terminoscondicione['Terminoscondicione']['texto_sin_licencia_disponibilidad']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Politica Privacidad'); ?></dt>
		<dd>
			<?php echo h($terminoscondicione['Terminoscondicione']['politica_privacidad']); ?>
			&nbsp;
		</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $terminoscondicione['Terminoscondicione']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Terminoscondicione'), array('action' => 'edit', $terminoscondicione['Terminoscondicione']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Terminoscondicione'), array('action' => 'delete', $terminoscondicione['Terminoscondicione']['id']), array(), __('Are you sure you want to delete # %s?', $terminoscondicione['Terminoscondicione']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Terminoscondiciones'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Terminoscondicione'), array('action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>