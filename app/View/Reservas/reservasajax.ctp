<html>



<body>

<!-- container -->
<div class="container example">

        <style type="text/css">

            td{
                width: 260px;
                height: 10px;
            }

            table{
                font-size:12px;
            }
            
        </style>

    <div class="row" style="position: fixed;width: 92%;" >   
    <div class="panel panel-heading">
      Calendario
      <span class="pull-right">
      <a style="cursor: pointer;" onclick="ocultar()">
        Mostrar / Ocultar
        <i class="fa fa-caret-down"></i>
      </a>
      </span>
    </div>      
    <div class="row" id="contentCalendario">
      
        <div class="col-md-4">
            <div id="my-calendar1"></div>
        </div>

        <div class="col-md-4">
            <div class="m2" id="my-calendar2"></div>
        </div>
          
        <div class="col-md-4">
            <div class="m3" id="my-calendar3"></div>
        </div>        
    </div>
    </div>

    <div  style="padding-top: 5%" id="mesSeleccionado"></div>

    <input type="hidden" id="contentCalendarioInput" value="1">


    <script type="text/javascript">
      function ocultar(){

        var valueInput= $('#contentCalendarioInput').val();
        console.log('------>'+valueInput);

        if(valueInput == 1){
          $('#contentCalendarioInput').val(0);         
          $('#contentCalendario').fadeOut();

        }

        if(valueInput == 0){
          $('#contentCalendarioInput').val(1);         
          $('#contentCalendario').fadeIn();

        }



        
      }

      ocultar();
    </script>

	

</body>
</html>
  <script type="application/javascript">


                function mesSeleccionado(dia,mes,anio){

                    $.ajax({
                        url:"/Reservas/mes/"+dia+"/"+mes+"/"+anio,
                        type:"post",
                        data:{
                              
                            },
                        success:function(response){
                          if(response != 0){
                                $("#mesSeleccionado").html(response);
                          }else{
                            console.log('no ok');
                          }
                        }
                    });
                }





                var f = new Date();
                
                mesSeleccionado(f.getDate(),f.getMonth() +1 ,f.getFullYear());

              

                $(document).ready(function () {
                    $("#date-popover").popover({html: true, trigger: "manual"});
                    $("#date-popover").hide();
                    $("#date-popover").click(function (e) {
                        $(this).hide();
                    });

                    $("#my-calendar1").zabuto_calendar({
                        nav_icon: { 
                        prev: '<i class="fa fa-angle-double-left" aria-hidden="true"></i>', 
                        next: '<i class="fa fa-angle-double-right" aria-hidden="true"></i>' 
                        },
                        year: <?=date('Y')?>,
                        language: 'es',
                        show_previous: true,
                        show_next: true  ,  
                        today: true,
                        action: function () {
                            return myDateFunction(this.id, false);
                            console.log('->'+this.id);
                        },
                        action_nav: function () {
                            return myNavFunction(this.id);
                            console.log('->'+this.id);
                        }
                        
                        
                    });

                    var f = new Date();
                    //document.write(f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear());

                    mes2(f.getFullYear(),f.getMonth() + 1);
                    mes3(f.getFullYear(),f.getMonth() + 1);


                    
                  

                });

                function myDateFunction(id, fromModal) {

                    var date = $("#" + id).data("date");
                    $("#diaSelect").val(date);
                    console.log(date);

                    var d_y = date.split('-');
              
                    mesSeleccionado(d_y[2],d_y[1],d_y[0]);

                    var hasEvent = $("#" + id).data("hasEvent");
                    $("#date-popover").hide();
                    if (fromModal) {
                        $("#" + id + "_modal").modal("hide");
                    }
                    var date = $("#" + id).data("date");
                    var hasEvent = $("#" + id).data("hasEvent");
                    if (hasEvent && !fromModal) {
                        return false;
                    }
                    //$("#date-popover-content").html('You clicked on date ' + date);
                    //$("#date-popover").show();


                }
                //humaniza fecha
                function convierteFecha(fecha){
                    var newFecha = fecha.split("-");
                    $("#fecha").html(newFecha[2]+'/'+newFecha[1]+'/'+newFecha[0]);
                }

                function myNavFunction(id) {
                    $("#date-popover").hide();
                    var nav = $("#" + id).data("navigation");
                    var to = $("#" + id).data("to");
                    console.log('nav ' + nav + ' to/: ' + to.month + '/' + to.year);
                    mes2(to.year, to.month);
                    mes3(to.year, to.month);

                    //mesSeleccionado(to.month,to.year);
                }

                  function mes2(anio, mes){

                    mes++;


                    var id =   $('.m2').attr('id');
                    console.log(anio+' * '+mes+' / '+id);

                    $("#"+id).html('');
                    $("#"+id).zabuto_calendar({                          
                          year: anio,
                          month:mes,
                          language: 'es',
                          show_previous: false,
                          show_next: false  ,  
                          today: true,
                          action: function () {
                              return myDateFunction(this.id, false);
                          },
                          action_nav: function () {
                              return myNavFunction(this.id);
                          }                                            
                    });
                  }



                  

                  function mes3(anio, mes){    
                    mes++;mes++;
                  var id =   $('.m3').attr('id');                    
                  $("#"+id).html('');
                  $("#"+id).zabuto_calendar({                      
                      year: anio,
                      month:mes,
                      language: 'es',
                      show_previous: false,
                      show_next: false  ,  
                      today: true,
                      action: function () {
                          return myDateFunction(this.id, false);
                      },
                      action_nav: function () {
                          return myNavFunction(this.id);
                      }
                      
                      
                  });
                }
            </script>
<?php 
if(isset($id_reporte)){
?>
<script type="text/javascript">
  //generar_reporte_reserva('<?= $id_reporte ?>');
  window.open("<?= $this->Html->url('/Reserindivistatuses/reporte/'.$id_reporte.' ')?> ",'_blank');
</script>
<?php
}
?>