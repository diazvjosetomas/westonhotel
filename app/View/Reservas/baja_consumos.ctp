<section class="content-header">
<h1>
Sistema de gestión
<small><?php echo __('Consumos'); ?></small>
</h1>
<ol class="breadcrumb">
<li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
<li class="active"><?php echo __('Consumos'); ?></li>
</ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Consumos'); ?> dados de baja</h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">

						<table id="data" class="table table-striped table-bordered" width="100%" cellspacing="0">
						<thead>
						<tr>
							<th><?php echo __('Consumo'); ?></th>
							<th><?php echo __('Cliente'); ?></th>
							<th><?php echo __('Hora'); ?></th>
							<th><?php echo __('Motivo'); ?></th>
							<th><?php echo __('Usuario'); ?></th>
							<th><?php echo __('Operaci&oacute;n'); ?></th>
														
						</tr>
						</thead>
						<tbody>
						<?php foreach ($consumos as $consumo): ?>
							<tr>
								<td><?php echo h( $consumo['Auditoria']['num_consumo'] ); ?></td>
								<td><?php echo h($consumo['Cliente']['dni'].' '.$consumo['Cliente']['nombre_completo']); ?></td>
								<td><?php echo h(  formatdmy( substr($consumo['Auditoria']['hora'], 0, 10)).' - '. substr($consumo['Auditoria']['hora'], 11) ); ?>&nbsp;</td>
								<td><?php echo h($consumo['Auditoria']['motivo']); ?>&nbsp;</td>
								<td><?php echo h($consumo['User']['username']); ?>&nbsp;</td>
								<td><?php echo h($consumo['Auditoria']['especifico']); ?>&nbsp;</td>
								
							</tr>
						<?php endforeach; ?>
						</tbody>
						</table>
					</div>
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->


<?php /* ?><div class="actions">
<h3><?php echo __('Actions'); ?></h3>
<ul>
	<li><?php echo $this->Html->link(__('New Promarca'), array('action' => 'add')); ?></li>
	<li><?php echo $this->Html->link(__('List Protipos'), array('controller' => 'protipos', 'action' => 'index')); ?> </li>
	<li><?php echo $this->Html->link(__('New Protipo'), array('controller' => 'protipos', 'action' => 'add')); ?> </li>
</ul>
</div>
<?php */ ?><script type="text/javascript">
//$(document).ready(function() {
    $('#data').DataTable( {
    	dom: 'Bfrtlip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "language": 
        {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
    } );
//} );
</script>
