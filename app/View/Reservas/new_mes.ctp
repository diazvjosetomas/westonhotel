<?php
$numero = cal_days_in_month(CAL_GREGORIAN, $mes, $anio); // 31

$num_mes = $mes;

if ($mes == 1) {
	$mes = 'Enero del ';
}else if ($mes == 2) {
	$mes = 'Febrero del ';
}else if ($mes == 3) {
	$mes = 'Marzo del ';
}else if ($mes == 4) {
	$mes = 'Abril del ';
}else if ($mes == 5) {
	$mes = 'Mayo del ';
}else if ($mes == 6) {
	$mes = 'Junio del ';
}else if ($mes == 7) {
	$mes = 'Julio del ';
}else if ($mes == 8) {
	$mes = 'Agosto del ';
}else if ($mes == 9) {
	$mes = 'Septiembre del ';
}else if ($mes == 10) {
	$mes = 'Octubre del ';
}else if ($mes == 11) {
	$mes = 'Noviembre del ';
}else if ($mes == 12) {
	$mes = 'Diciembre del ';
}

?>

<h3 style="text-align: center;">  <?=$mes.' '.$anio ?>

<span class="pull-right" style="font-size: 12px;"> <a href="<?=$this->Html->url('/Reserindividuales/add_from_calendar')?>" class="btn btn-primary"> Nueva Reserva <i class='fa fa-plus'></i> </a> </span>
</h3>
<br>

<style type="text/css">
	
	.diaOcupado{
		
		background: rgba(254,182,69,1);
		background: -moz-linear-gradient(top, rgba(254,182,69,1) 0%, rgba(254,183,69,1) 100%);
		background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(254,182,69,1)), color-stop(100%, rgba(254,183,69,1)));
		background: -webkit-linear-gradient(top, rgba(254,182,69,1) 0%, rgba(254,183,69,1) 100%);
		background: -o-linear-gradient(top, rgba(254,182,69,1) 0%, rgba(254,183,69,1) 100%);
		background: -ms-linear-gradient(top, rgba(254,182,69,1) 0%, rgba(254,183,69,1) 100%);
		background: linear-gradient(to bottom, rgba(254,182,69,1) 0%, rgba(254,183,69,1) 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#feb645', endColorstr='#feb745', GradientType=0 );

	}

	.th-color{
		background-color: #3c8dbc;
		color: white;
		font-size: 11px;
	}
	td{
		background-color: white;	
		font-size: 11px;

	}

	.border_td{
		border-style: solid ;
		border-width: 1px;
		border-color:silver;
		max-width: 21px !important;

	}

	.primer_dia{

		background: rgba(255,255,255,1);
		background: -moz-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(255,255,255,1) 45%, rgba(254,182,69,1) 100%);
		background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(255,255,255,1)), color-stop(45%, rgba(255,255,255,1)), color-stop(100%, rgba(254,182,69,1)));
		background: -webkit-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(255,255,255,1) 45%, rgba(254,182,69,1) 100%);
		background: -o-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(255,255,255,1) 45%, rgba(254,182,69,1) 100%);
		background: -ms-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(255,255,255,1) 45%, rgba(254,182,69,1) 100%);
		background: linear-gradient(to bottom, rgba(255,255,255,1) 0%, rgba(255,255,255,1) 45%, rgba(254,182,69,1) 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#feb645', GradientType=0 );

	}

	.ultimo_dia{

		background: rgba(254,182,69,1);
		background: -moz-linear-gradient(top, rgba(254,182,69,1) 0%, rgba(255,255,255,1) 55%, rgba(255,255,255,1) 100%);
		background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(254,182,69,1)), color-stop(55%, rgba(255,255,255,1)), color-stop(100%, rgba(255,255,255,1)));
		background: -webkit-linear-gradient(top, rgba(254,182,69,1) 0%, rgba(255,255,255,1) 55%, rgba(255,255,255,1) 100%);
		background: -o-linear-gradient(top, rgba(254,182,69,1) 0%, rgba(255,255,255,1) 55%, rgba(255,255,255,1) 100%);
		background: -ms-linear-gradient(top, rgba(254,182,69,1) 0%, rgba(255,255,255,1) 55%, rgba(255,255,255,1) 100%);
		background: linear-gradient(to bottom, rgba(254,182,69,1) 0%, rgba(255,255,255,1) 55%, rgba(255,255,255,1) 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#feb645', endColorstr='#ffffff', GradientType=0 );

	}
/*	thead{
		padding-right: 0px;
	}
	thead, tbody { display: block; }
	#tbody {
	    height: 800px;
	    overflow-y: auto;
	    
	}*/

	.tr_show_number{

	}

	.tr_show_number:hover{
		color:red !important;

	}


</style>
<?php $auxNumero = $numero + 1; ?>
<table class="table tablesorter" >
	<thead >
		<tr>	
			<th class="th-color">Hab.</th>
			<th class="th-color">Cant.</th>
			<th class="th-color">Desc.</th>
			<th class="th-color">Estatus</th>
			<th class="th-color" colspan="<?=$numero?>" style="text-align: center;">Días del Mes</th>
		</tr>
		<tr>
		<td>#</td>
		<td></td>
		<td></td>
		<td></td>
		
		<?php 
			for ($i=1; $i <= $numero ; $i++) { 
				echo "<td style='width:2%;' class='border_td' style='text-align: center;'>".$i."</td>";					
			}
		?>
		
		</tr>
	</thead>
	<tbody id='tbody'>
		

<?php

foreach ($habitaciones as $key) {
	
	$inicio_ciclo = 0;




	$posicion = 1;
	foreach ($entrada1 as $entradas) {
		

		if ($key['Habitacione']['id'] == $entradas['Reserindividuale']['habitacione_id']) {
			//Entrada				
		$inicio_ciclo = 1;
		$dia_entrada_ex = explode('-',$entradas['Reserindividuale']['fecha_entrada']);
		$dia_entrada = $dia_entrada_ex[2];
		//$dia_entrada = '3';
		//Salida
		$dia_salida_ex = explode('-',$entradas['Reserindividuale']['fecha_salida']);		
		$dia_salida = $dia_salida_ex[2];
		//$dia_salida = '10';

		if ($dia_salida < $dia_entrada ) {
			
				$dia_salida = $numero;				
			
		}


		#Posicion N1 inicio de la habitacion
		if ($posicion == 1) {
			# code...
		echo "<tr class='tr_show_number'><td>".$entradas['Habitacione']['numhabitacion']."</td>";
		//echo "<td style='text-align:center;'>".$entradas['Habitacione']['capacidad']."</td>";
		echo "<td style='text-align:center;'>".$entradas['Reserindividuale']['cantidad_personas']."</td>";
		echo "<td>".ucwords(strtolower($entradas['Habitacione']['descripcion']))."</td>";
		echo "<td>";

		if ($entradas['Reserstatusindividuale']['id'] == 1) {
			$EstatusClass = 'btn btn-calendar-nueva btn-sm';
		}

		if ($entradas['Reserstatusindividuale']['id'] == 2) {
			$EstatusClass = 'btn btn-calendar-confirmada btn-sm';
		}

		if ($entradas['Reserstatusindividuale']['id'] == 3) {
			$EstatusClass = 'btn btn-calendar-auditada btn-sm';
		}

		if ($entradas['Reserstatusindividuale']['id'] == 4) {
			$EstatusClass = 'btn btn-calendar-entrada btn-sm';
		}

		if ($entradas['Reserstatusindividuale']['id'] == 5) {
			$EstatusClass = 'btn btn-calendar-salida btn-sm';
		}

		echo $this->Html->link(__(h($entradas['Reserstatusindividuale']['denominacion'])), array('action' => 'status', $entradas['Reserindividuale']['id']), array('class'=>$EstatusClass));
		echo "</td>";
		}


		for ($i=$posicion; $i <= $numero ; $i++) { 


			if ($i >= $dia_entrada AND $i <= $dia_salida) {


				if ($i == $dia_entrada) {
					echo "<td title='Editar' style='cursor:pointer;width:2% !important;' onclick='openEdit(".$entradas['Reserindividuale']['id'].")' class='primer_dia border_td'>$i</td>";
						$posicion++;

				}if ($i > $dia_entrada && $i < $dia_salida) {
					echo "<td title='Editar' style='cursor:pointer;width:2% !important;' onclick='openEdit(".$entradas['Reserindividuale']['id'].")' class='diaOcupado border_td'>$i</td>";			
						$posicion++;		

				}else if ($i == $dia_salida) {
					echo "<td title='Editar' style='cursor:pointer;width:2% !important;' onclick='openEdit(".$entradas['Reserindividuale']['id'].")' class='ultimo_dia border_td'>$i</td>";	
						$posicion++;
						break;
				}
			}else{
				#Cuadro en blanco
				$posicion++;
				echo "<td style='width:2% !important; color:white;' class='border_td tr_show_number'>- $i</td>";				
			}
	
	
		}
			if ($i == $numero) {
				echo "</tr>";
			}
		}




	}



	
	if ($inicio_ciclo != 0) {

		for ($i=$posicion; $i <= $numero ; $i++) { 		
			echo "<td style='width:2%;color:white;' class='border_td tr_show_number'>. $i</td>";				
		}
			echo "</tr>";
		
	}

	if ($inicio_ciclo == 0) {
			
			echo "<tr><td>".$key['Habitacione']['numhabitacion']."</td>";
			echo "<td>".$key['Habitacione']['capacidad']."</td>";
			echo "<td>".ucwords(strtolower($key['Tipohabitacione']['denominacion']))."</td>";

			echo "<td style='width:2%;'>";
			echo $this->Html->link(__(h('Nueva R.')), array('action' => 'agregar', $key['Habitacione']['id']), array('class'=>'btn btn-calendar-nueva btn-sm'));
			echo "</td>";
			

			for ($i=1; $i <= $numero ; $i++) { 
				
				echo "<td style='width:2%;color:white;' class='border_td tr_show_number'>$i</td>";				
			}
			echo "</tr>";	
		}

	




	

}







?>

<?php //pr($habitaciones); ?>
	</tbody>
</table>

<script type="text/javascript">
	$("body").addClass('sidebar-collapse');
	function openEdit(idReserva){
		window.location.href = '/Reserindividuales/edit_2/'+idReserva;
	}
</script>

<script>

		$(document).ready(function () {
			// initialize stickyTableHeaders _after_ tablesorter
			$(".tablesorter").tablesorter();
			$("table").stickyTableHeaders();
		});
	</script>
</script>