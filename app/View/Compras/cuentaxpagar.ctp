<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Cuentas x pagar'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Cuentas x pagar'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Cuentas x pagar'); ?></h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                    <p><?php echo $this->Html->link(__('Crear Nuevo'), array('action' => 'add'),  array('class'=>'btn btn-primary')); ?></p>

							<table id="data" class="table table-striped table-bordered" width="100%" cellspacing="0">
							<thead>
							<tr>
															<th><?php echo __('Proveedor'); ?></th>
															<th><?php echo __('Tipo Compra'); ?></th>
															<th><?php echo __('Num compra'); ?></th>
															<th><?php echo __('Fecha compra'); ?></th>
															<th><?php echo __('Num factura'); ?></th>
															<th><?php echo __('Total'); ?></th>
															<th><?php echo __('Pagado'); ?></th>
															<th><?php echo __('Deduda'); ?></th>
															<th class="actions"><?php echo __('Acción'); ?></th>
							</tr>
							</thead>
							<tbody>
							<?php foreach ($compras as $compra): ?>
	<tr>
		<td><?php echo h($compra['Proveedore']['razon_social']); ?></td>
		<td><?php echo h($compra['Tipocompra']['denominacion']); ?></td>
		<td><?php echo h($compra['Compra']['num_compra']); ?>&nbsp;</td>
		<td><?php echo h($compra['Compra']['fecha_compra']); ?>&nbsp;</td>
		<td><?php echo h($compra['Compra']['num_factura']); ?>&nbsp;</td>
		<td><?php echo h($compra['Compra']['total']); ?>&nbsp;</td>
		<td><?php echo h($compra['Compra']['pagado']); ?>&nbsp;</td>
		<td><?php echo h($compra['Compra']['deuda']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Contabilizar Pago'), array('action' => 'abonar', $compra['Compra']['id']), array('class'=>'')); ?>
		</td>
	</tr>
<?php endforeach; ?>
							</tbody>
							</table>
						</div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->


<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Compra'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Proveedores'), array('controller' => 'proveedores', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proveedore'), array('controller' => 'proveedores', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipocompras'), array('controller' => 'tipocompras', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipocompra'), array('controller' => 'tipocompras', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?><script type="text/javascript">
	//$(document).ready(function() {
	    $('#data').DataTable( {
	    	dom: 'Bfrtlip',
	        buttons: [
	            'copy', 'csv', 'excel', 'pdf', 'print'
	        ],
	        "language": 
	        {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
	    } );
	//} );
</script>
