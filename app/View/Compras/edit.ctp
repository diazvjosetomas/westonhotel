<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Compras'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Compras'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Edit Compras'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Compra', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
									echo $this->Form->input('id', array('class'=>'form-horizontal'));	
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Compraproveedore_id">Proveedor</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('proveedore_id', array('id'=>'Compraproveedore_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'empty'=>'--Seleccione--'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Compratipocompra_id">Tipo Compra</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('tipocompra_id', array('id'=>'Compratipocompra_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'empty'=>'--Seleccione--'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Compraano_compra">Año compra</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('ano_compra', array('id'=>'Compraano_compra', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'value'=>date("Y"), 'readonly'=>false));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Compranum_compra">Númer compra</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('num_compra', array('id'=>'Compranum_compra', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Comprafecha_compra">Fecha compra</label>';		
									echo'<div class="col-md-4">';			
									echo $this->Form->input('fecha_compra1', array('id'=>'Comprafecha_compra', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly' => true, 'value' => $this->request->data['Compra']['fecha_compra1']));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Compranum_factura">Número factura</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('num_factura', array('id'=>'Compranum_factura', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
							?>
							</div>
							</div>
			                <div class="row">
			                <label class="control-label col-md-2" for="Compranum_factura">Productos</label>
			                <div class="col-md-9 col-sm-9 col-xs-12">
			                    <div class="box box-solid box-default">
			                        <div class="box-header with-border">
			                            <h3 class="box-title">Productos</h3>
			                            <div class="box-tools pull-right">
			                                <button id="newProductoBtn" class="btn btn-box-tool" data-toggle="tooltip" title="Agregar otro producto" data-widget="chat-pane-toggle"><i class="glyphicon glyphicon-plus"></i></button>
			                            </div><!-- /.box-tools -->
			                        </div><!-- /.box-header -->
			                        <div class="box-body">
			                            <div id="productos-container">
			                            <?php $c = 0;foreach($compraproductos as $compraproducto){ ?>
			                                <div id="productoscount">
			                                    <div class="form-group pro-con" id="producto_<?= $c ?>">
			                                        <label class="control-label col-md-1">Producto</label>
			                                        <div class="col-md-2">
			                                            <select class="form-control tel-<?= $c ?>" id="Productos_<?= $c ?>__Pro" name="data[Compra][Productos][<?= $c ?>][pro]" required="required"  onchange="Javascript:precio_p(<?= $c ?>);">
														<option value="">--Seleccione--</option>
														<?php
				            	                             foreach($proproductos as $proproducto){
				            	                             	if($compraproducto['Compraproducto']['proproducto_id']==$proproducto['Proproductos']['id']){
				            	                             		echo "  '<option value=\"".$proproducto['Proproductos']['id']."\" selected>".$proproducto['Proproductos']['denominacion']."</option>'+   ";
				            	                             	}else{
                                                                    echo "  '<option value=\"".$proproducto['Proproductos']['id']."\">".$proproducto['Proproductos']['denominacion']."</option>'+   ";
				            	                             	}
				            	                             }
			            	                             ?>
														</select>
			                                        </div>
			                                        <label class="control-label col-md-1">Cantidad</label>
			                                        <div class="col-md-1">
														<input class="form-control pro-<?= $c ?>-ext text-box single-line"  id="Productos_<?= $c ?>__Cantidad" name="data[Compra][Productos][<?= $c ?>][cant]" required="required"  type="text" value="<?= $compraproducto['Compraproducto']['cantidad'] ?>"  onchange="Javascript:total_p(<?= $c ?>);"/>
			                                        </div>
			                                        <label class="control-label col-md-1">Pre. U.</label>
			                                        <div class="col-md-2">
			                                            <input class="form-control pro-<?= $c ?>-ext text-box single-line"  id="Productos_<?= $c ?>__Precio"   name="data[Compra][Productos][<?= $c ?>][pre]"  required="required"  type="text" value="<?= $compraproducto['Compraproducto']['precio'] ?>" readonly=true />
			                                        </div>
			                                        <label class="control-label col-md-1">Total</label>
			                                        <div class="col-md-2">
			                                            <input class="form-control pro-<?= $c ?>-ext text-box single-line" id="Productos_<?= $c ?>__Total" name="data[Compra][Productos][<?= $c ?>][total]"  required="required"  type="text" value="<?= $compraproducto['Compraproducto']['total'] ?>" readonly=true />
			                                        </div>
			                                        <div class="col-md-1">
			                                            <button class="btn btn-danger btn-sm" onclick="$('#producto_<?= $c ?>').remove(); total_p(<?= $c ?>); return false;"><span class="glyphicon glyphicon-trash"></span></button>
			                                        </div>
			                                    </div>
			                                </div>
			                            <?php $c++;} ?>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			           		</div>
				            <div class="row">
				            <div class="col-md-12">
								<?php
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Compraobservaciones">Observaciones</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('observaciones', array('id'=>'Compraobservaciones', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Compraobservaciones">Total</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('total', array('id'=>'Compratotal', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly'=>true, 'type'=>'text'));		
									echo '</div>';	
									echo '</div>';
								?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    
	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<script type="text/javascript">
$(document).ready(function() {
    $("#newProductoBtn").click(function (e) {
        var MaxInputs       = 100;
        var x = $("#productos-container #productoscount").length + 1;
        var FieldCount = x-1;
        if(x <= MaxInputs) //max input box allowed
        {
            FieldCount++;
            $("#productos-container").append('<div class="form-group pro-con" id="producto_'+FieldCount+'">'+
			                                        '<label class="control-label col-md-1">Producto</label>'+
			                                        '<div class="col-md-2">'+
			                                            '<select class="form-control tel-'+FieldCount+'" id="Productos_'+FieldCount+'__Pro" name="data[Compra][Productos]['+FieldCount+'][pro]" required="required"  onchange="Javascript:precio_p('+FieldCount+');">'+
														'<option value="">--Seleccione--</option>'+
														<?php
				            	                             foreach($proproductos as $proproducto){
				            	                             	echo "  '<option value=\"".$proproducto['Proproductos']['id']."\">".$proproducto['Proproductos']['denominacion']."</option>'+   ";
				            	                             }
			            	                             ?>
														'</select>'+
			                                        '</div>'+
			                                        '<label class="control-label col-md-1">Cantidad</label>'+
			                                        '<div class="col-md-1">'+
														'<input class="form-control pro-'+FieldCount+'-ext text-box single-line"  id="Productos_'+FieldCount+'__Cantidad" required="required"  name="data[Compra][Productos]['+FieldCount+'][cant]" type="text" value="0" onchange="Javascript:total_p('+FieldCount+');" />'+
			                                        '</div>'+
			                                        '<label class="control-label col-md-1">Pre. U.</label>'+
			                                        '<div class="col-md-2">'+
			                                            '<input class="form-control producto-mask text-box single-line" id="Productos_'+FieldCount+'__Precio" required="required"  name="data[Compra][Productos]['+FieldCount+'][pre]"  type="text" value="0" readonly=true />'+
			                                        '</div>'+
			                                        '<label class="control-label col-md-1">Total</label>'+
			                                        '<div class="col-md-2">'+
			                                            '<input class="form-control producto-mask text-box single-line" id="Productos_'+FieldCount+'__Total" required="required"  name="data[Compra][Productos]['+FieldCount+'][total]"  type="text" value="0" readonly=true />'+
			                                        '</div>'+
			                                        '<div class="col-md-1">'+
			                                            '<button class="btn btn-danger btn-sm" onclick="$(\'#producto_'+FieldCount+'\').remove(); total_p('+FieldCount+'); return false;"><span class="glyphicon glyphicon-trash"></span></button>'+
			                                        '</div>'+
			                                    '</div>');
            x++; //text box increment
        }
        return false;
    });
    
    $('#Comprafecha_compra').datepicker();
});
</script>