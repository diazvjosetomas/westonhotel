<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Compras'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Compras'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Compras'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
							<dt><?php echo __('Proveedor'); ?></dt>
							<dd>
								<?php echo h($compra['Proveedore']['razon_social']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Tipo de compra'); ?></dt>
							<dd>
								<?php echo h($compra['Tipocompra']['denominacion']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Año Compra'); ?></dt>
							<dd>
								<?php echo h($compra['Compra']['ano_compra']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Número Compra'); ?></dt>
							<dd>
								<?php echo h($compra['Compra']['num_compra']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Fecha Compra'); ?></dt>
							<dd>
								<?php echo h( formatdmy($compra['Compra']['fecha_compra']) ); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Número Factura'); ?></dt>
							<dd>
								<?php echo h($compra['Compra']['num_factura']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Observaciones'); ?></dt>
							<dd>
								<?php echo h($compra['Compra']['observaciones']); ?>
								&nbsp;
							</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $compra['Compra']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Compra'), array('action' => 'edit', $compra['Compra']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Compra'), array('action' => 'delete', $compra['Compra']['id']), array(), __('Are you sure you want to delete # %s?', $compra['Compra']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Compras'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Compra'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proveedores'), array('controller' => 'proveedores', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proveedore'), array('controller' => 'proveedores', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipocompras'), array('controller' => 'tipocompras', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipocompra'), array('controller' => 'tipocompras', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>