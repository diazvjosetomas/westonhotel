<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Consumo Resto'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Consumo Resto'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Consumo Resto'); ?> Registrados</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class='row'>
                            <div class='col-md-12'>
                                <!-- LINE CHART -->
                                    <div class="box-body chart-responsive">
                                      <div class="chart" id="line-chart" style="height: 300px;"></div>
                                    </div><!-- /.box-body -->
                                    <script>
                                    $(function () {
                                     "use strict";
                                        // LINE CHART
                                        var line = new Morris.Line({
                                          element: 'line-chart',
                                          resize: true,
                                          data: [
                                            {y: '2011 Q1', item1: 2666},
                                            {y: '2011 Q2', item1: 2778},
                                            {y: '2011 Q3', item1: 4912},
                                            {y: '2011 Q4', item1: 3767},
                                            {y: '2012 Q1', item1: 6810},
                                            {y: '2012 Q2', item1: 5670},
                                            {y: '2012 Q3', item1: 4820},
                                            {y: '2012 Q4', item1: 15073},
                                            {y: '2013 Q1', item1: 10687},
                                            {y: '2013 Q2', item1: 8432}
                                          ],
                                          xkey: 'y',
                                          ykeys: ['item1'],
                                          labels: ['Item 1'],
                                          lineColors: ['#3c8dbc'],
                                          hideHover: 'auto',
                                          exporting: {
                                                buttons: {
                                                    contextButton: {
                                                        text:'Imprimir Grafica'
                                                    }
                                                }
                                            }
                                        });
                                    });
                                    </script>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <?php echo $this->Html->link(__('Volver a selección'), array('action' => 'factura')); ?>                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
