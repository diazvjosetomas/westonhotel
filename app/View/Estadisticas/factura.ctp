<?php echo $this->Html->script('highcharts/highstock.js'); ?>

<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Facturado'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Facturado'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Facturado'); ?> Registrados</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class='row'>
                            <div class='col-md-12'>
                                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                                <script type="text/javascript">
                                // Create the chart
                                     $.getJSON('/Estadisticas/facturagrafica', function (data) {
                                        // Create the chart
                                        Highcharts.stockChart('container', {
	                                            rangeSelector: {
	                                                selected: 1
	                                            },

	                                            title: {
	                                                text: 'Grafica Facturado'
	                                            },

	                                            series: [{
	                                            	type:'line',
	                                                name: 'Monto',
	                                                //data: data,
	                                                data:[<?php
	                                                foreach($factura as $fac){
	                                                	$datatime = strtotime($fac['Factura']['created']);
	                                                	$datatime *= 1000;
	                                                	echo "[".$datatime.", ".$fac['Factura']['total']."],";
												    }
	                                                ?>],
	                                                tooltip: {
	                                                    valueDecimals: 2
	                                                }
	                                            }],
	                                            exporting: {
	                                            	enabled:true,
			                                        buttons: {
			                                            contextButton: {
			                                            	enabled:true,
			                                                text:'Imprimir Grafica'
			                                            }
			                                        }
			                                    }
	                                        });
                                        });
                                </script>
                                </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-12">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
