<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Ocupación por fecha'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Ocupación por fecha'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Ocupación por fecha'); ?> Registrados</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                    <?php echo $this->Form->create('Grafica', array('class'=>'form-horizontal')); ?>
						<div class='row'>
								<div class='col-md-12'>
									<?php
	                                    echo'<div class="form-group">';	
	                                    echo'<label class="control-label col-md-2" for="Ocupacionfechadesde">Fecha Desde</label>';	
	                                    echo'<div class="col-md-4">';			
	                                    echo $this->Form->input('Grafica.fecha_desde1', array('id'=>'Ocupacionfechadesde', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly' => true )); 	
	                                    echo '</div>';	
	                                    echo '</div>';

	                                    echo'<div class="form-group">';
	                                    echo'<label class="control-label col-md-2" for="Ocupacionfechahasta">Fecha Hasta</label>';	
	                                    echo'<div class="col-md-4" id="div-tipo">';			
	                                    echo $this->Form->input('Grafica.fecha_hasta1', array('id'=>'Ocupacionfechahasta', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly' => true ));		
	                                    echo '</div>';	
	                                    echo '</div>';
	                                ?>
	                                <div class="form-group">
	                                	<label class="control-label col-md-2">
	                                		Estatus
	                                	</label>
	                                	<div class="col-md-9">
	                                		<select id="Ocupacion.estatus" name="data[Grafica][estatus]" class="form-control">
	                                			<option value="1"> Iniciada </option>
	                                			<option value="2"> Confirmada </option>
	                                			<option value="3"> Auditada </option>
	                                			<option value="4"> Ingresada </option>
	                                			<option value="5"> Egresada </option>
	                                			<option value="6"> No Show </option>
	                                			<option value="7"> Todas</option>
	                                		</select>
	                                	</div>
	                                </div>
								</div>
								
									
								
								<div class="col-md-12">
									<div class="form-group">
		                                <div class="col-md-12">
		                                    <input value="Ver" class="btn btn-primary pull-right" type="submit">
		                                </div>
		                            </div>
	                            </div>
						</div>
					</form>                
					</div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<script type="text/javascript">
	$('#Ocupacionfechadesde').datepicker();
	$('#Ocupacionfechahasta').datepicker();
</script>