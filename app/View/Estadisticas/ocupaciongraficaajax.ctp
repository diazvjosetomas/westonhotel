

<section class="content-header">
  <h1>
    Ocupación Gráfica del Día
    <small><?php echo __('Ocupación'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Ocupación'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Ocupación'); ?> Registrados</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                    <div class='row'>
                                <div class='col-md-12'>
                                <div id="container3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                                <script type="text/javascript">
                                // Create the chart
                                $('#container3').highcharts({
                                    chart: {
                                        type: 'column'
                                    },
                                    title: {
                                        text: '<?= $titulo ?>'
                                    },
                                    subtitle: {
                                        text: ''
                                    },
                                    xAxis: {
                                        type: 'category'
                                    },
                                    yAxis: {
                                        title: {
                                            text: 'Total porcentual'
                                        }

                                    },
                                    legend: {
                                        enabled: false
                                    },
                                    plotOptions: {
                                        series: {
                                            borderWidth: 0,
                                            dataLabels: {
                                                enabled: true,
                                                format: '{point.y:f} Habitacion(es) ocupada(s)'
                                            }
                                        }
                                    },

                                    tooltip: {
                                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> Habitacion(es) ocupada(s)<br/>'
                                    },

                                    series: [{
                                        name: 'Porcentaje de Ocupación',
                                        colorByPoint: true,
                                        data: [<?php 
                                                
                                                echo "{
                                                        name: 'Habitaciones (".$cantidad_hbitaciones.")',
                                                        y: ".$cantidad.",
                                                        drilldown: null
                                                       },
                                                      ";
                                        ?>]
                                    }],exporting: {
                                        buttons: {
                                            contextButton: {
                                                align: "right",
                                                enabled: true,
                                                height: 20,
                                                menuItems: undefined,
                                                onclick: undefined,
                                                symbol: "menu",
                                                symbolFill: "#666666",
                                                symbolSize: 14,
                                                symbolStroke: "#666666",
                                                symbolStrokeWidth: 1,
                                                symbolX: 12.5,
                                                symbolY: 10.5,
                                                text: 'Erportar',
                                                theme:'',
                                                verticalAlign: "top",
                                                width: 24,
                                                x: -10,
                                                y: 0
                                            }
                                        }
                                    }
                                });
                                </script>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <?php echo $this->Html->link(__('Volver a selección'), array('action' => 'ocupacion')); ?>                                       
                                        </div>
                                    </div>
                                </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
