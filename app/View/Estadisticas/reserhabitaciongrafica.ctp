<?php echo $this->Html->script('highcharts/highcharts.js'); ?>
<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Reservaciones x habitación'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Reservaciones x habitación'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Reservaciones x habitación'); ?> Registrados</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                    <div class='row'>
                                <div id="container2" class="col-md-12">
                                    
                                </div>

                                <div class='col-md-12'>
                                <div id="container3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                                <script type="text/javascript">
                                // Create the chart
                                $('#container3').highcharts({
                                    chart: {
                                        type: 'column'
                                    },
                                    title: {
                                        text: '<?= $titulo ?>'
                                    },
                                    subtitle: {
                                        text: ''
                                    },
                                    xAxis: {
                                        type: 'category'
                                    },
                                    yAxis: {
                                        title: {
                                            text: 'Total de reservaciones'
                                        }

                                    },
                                    legend: {
                                        enabled: false
                                    },
                                    plotOptions: {
                                        series: {
                                            borderWidth: 0,
                                            dataLabels: {
                                                enabled: true,
                                                format: '{point.y:f} Rervaciones'
                                            }
                                        }
                                    },

                                    tooltip: {
                                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> Reservaciones hechas para esta habitación.<br/>'
                                    },

                                    series: [{
                                        name: 'Brands',
                                        colorByPoint: true,
                                        data: [<?php 
                                                $por = ($cantidad * 100) / $cantidad_hbitaciones;
                                                echo "{
                                                        name: 'Reservaciones (".$cantidad.")',
                                                        y: ".$cantidad.",
                                                        drilldown: null
                                                       },
                                                      ";
                                        ?>]
                                    }],
                                    exporting: {
                                        buttons: {
                                            contextButton: {
                                                text:'Imprimir Grafica'
                                            }
                                        }
                                    }
                                });
                                </script>


                                <?php if ($tipografica == 2): ?>
                                    
                                
                                <script>


                                      var chart = Highcharts.chart('container2', {

                                        title: {
                                            text: '<?= $titulo ?>'
                                        },

                                        subtitle: {
                                            text: 'Relacion Mes/Ocupacion'
                                        },

                                        xAxis: {
                                            categories: <?=$cat?>
                                        },

                                        series: [{
                                            type: 'column',
                                            colorByPoint: true,
                                            data: <?=$dat?>,
                                            showInLegend: false
                                        }]

                                    });




                                
                                    </script>


                                <?php endif ?>


                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <?php echo $this->Html->link(__('Volver a selección'), array('action' => 'reserhabitacion')); ?>                                       
                                        </div>
                                    </div>
                                </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
