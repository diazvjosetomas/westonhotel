<div id="colorlib-services">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-md-3 animate-box text-center aside-stretch">
                <div class="services">
							<span class="icon">
								<i> <img style="width: 120px;" src="<?= $this->Html->templateinclude('/fonts/hotel-and-service/png/reception.png', 'tour');?>"></i>
							</span>
                    <h3>Servicio Increible</h3>
                    <p></p>
                </div>
            </div>
            <div class="col-md-3 animate-box text-center ">
                <div class="services">
							<span class="icon">
								<i> <img style="width: 120px;" src="<?= $this->Html->templateinclude('/fonts/hotel-and-service/png/hotel-6.png', 'tour');?>"></i>
							</span>
                    <h3>Nuestros Planes</h3>
                    <p></p>
                </div>
            </div>
            <div class="col-md-3 animate-box text-center">
                <div class="services">
							<span class="icon">
								<i> <img style="width: 120px;" src="<?= $this->Html->templateinclude('/fonts/hotel-and-service/png/room-key-1.png', 'tour');?>"></i>
							</span>
                    <h3>Reserva tu Habitacion</h3>
                    <p></p>
                </div>
            </div>
            <div class="col-md-3 animate-box text-center">
                <div class="services">
							<span class="icon">
								<i> <img style="width: 120px;" src="<?= $this->Html->templateinclude('/fonts/hotel-and-service/png/woman.png', 'tour');?>"></i>
							</span>
                    <h3>Buen Apoyo</h3>
                    <p></p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="colorlib-tour colorlib-light-grey">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center colorlib-heading animate-box">
                <h2 style="color: " id="marca_habitaciontipo"><strong>Disponiblidad</strong></h2>
                <h3>Escoge la habitacion que se ajusta a tus necesidades.</h3>
            </div>
        </div>
    </div>
    <div  class="tour-wrap">
        <div id="habitaciontipo"></div>
    </div>
</div>


