<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Reservaciones'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Reservaciones'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Reservaciones en rango de fechas'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Auditoria', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-4'>
								<?php
									
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-6">Fecha inicial</label>';		
									echo'<div class="col-md-6" id="div-fechaentrada">';			
									echo $this->Form->input('fecha_1', array('div'=>false, 'id'=>'fecha_1', 'readonly' => 'readonly' ,'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									

									
								?>
								
							</div>

							<div class='col-md-4'>
								<?php
									
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-6">Fecha final</label>';		
									echo'<div class="col-md-6" id="div-fechaentrada">';			
									echo $this->Form->input('fecha_2', array('id' => 'fecha_2','div'=>false, 'readonly' => 'readonly','label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									

									
								?>
								
							</div>

							<div class="col-md-2">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    
	                                    <input value="Consultar" id="Reserindividualesubmit" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>

                            <?php
                            if(!empty($auditorias)){
                            	?>
                            		<table id="data" class="table table-striped table-bordered" width="100%" cellspacing="0">
							<thead>
							<tr>
								
								<th><?php echo __('Habitacion'); ?></th>
								<th><?php echo __('Cliente'); ?></th>
								
								<th><?php echo __('Fecha entrada'); ?></th>
								<th><?php echo __('Fecha salida'); ?></th>
								<th><?php echo __('Total'); ?></th>
								
							</tr>
							</thead>
							<tbody>
							<?php
								$sum = 0;
							foreach ($auditorias as $key => $auditoria):
								$sum += $auditoria['Auditoria']['costo_total'];
							?>
							<tr>
								
								<td><?php echo h($auditoria['Habitacione']['numhabitacion']); ?></td>
								<td><?php echo h($auditoria['Cliente']['nombre_completo']); ?></td>
								<td><?php echo h( formatdmy($auditoria['Auditoria']['fecha_entrada'])); ?>&nbsp;</td>
								<td><?php echo h( formatdmy($auditoria['Auditoria']['fecha_salida'])); ?>&nbsp;</td>
								<td><?php echo h($auditoria['Auditoria']['costo_total']); ?>&nbsp;</td>
								
								
							</tr>
							<?php endforeach; ?>
							</tbody>
							<tr>
								<th colspan="4" style="text-align: right;">Promedio por d&iacute;a</th>
								<th><?php echo number_format(($sum / ($dias) ), 2, ',', '.'); ?></th>
							</tr>
							</table>
							<?php 
                            	}else{
                            		echo '<br><br><h1 style="text-align:center">No hay resultados!</h1>';
                            	}
                            
                            ?>

					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<!-- Modal -->

<script type="text/javascript">

$("#fecha_1").datepicker();
$("#fecha_2").datepicker();

</script>

<? //pr($allclientes) ?>