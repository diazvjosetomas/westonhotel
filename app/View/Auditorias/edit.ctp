<div class="auditorias form">
<?php echo $this->Form->create('Auditoria'); ?>
	<fieldset>
		<legend><?php echo __('Edit Auditoria'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('habitacione_id');
		echo $this->Form->input('reserindividuale_id');
		echo $this->Form->input('resermultiple_id');
		echo $this->Form->input('cliente_id');
		echo $this->Form->input('factura_id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('hora');
		echo $this->Form->input('motivo');
		echo $this->Form->input('tipo_operacion');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Auditoria.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Auditoria.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Auditorias'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Habitaciones'), array('controller' => 'habitaciones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Habitacione'), array('controller' => 'habitaciones', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Reserindividuales'), array('controller' => 'reserindividuales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reserindividuale'), array('controller' => 'reserindividuales', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resermultiples'), array('controller' => 'resermultiples', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resermultiple'), array('controller' => 'resermultiples', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Clientes'), array('controller' => 'clientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cliente'), array('controller' => 'clientes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Facturas'), array('controller' => 'facturas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Factura'), array('controller' => 'facturas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
