<div class="auditorias view">
<h2><?php echo __('Auditoria'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($auditoria['Auditoria']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Habitacione'); ?></dt>
		<dd>
			<?php echo $this->Html->link($auditoria['Habitacione']['numhabitacion'], array('controller' => 'habitaciones', 'action' => 'view', $auditoria['Habitacione']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Reserindividuale'); ?></dt>
		<dd>
			<?php echo $this->Html->link($auditoria['Reserindividuale']['id'], array('controller' => 'reserindividuales', 'action' => 'view', $auditoria['Reserindividuale']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Resermultiple'); ?></dt>
		<dd>
			<?php echo $this->Html->link($auditoria['Resermultiple']['id'], array('controller' => 'resermultiples', 'action' => 'view', $auditoria['Resermultiple']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cliente'); ?></dt>
		<dd>
			<?php echo $this->Html->link($auditoria['Cliente']['da-ci'], array('controller' => 'clientes', 'action' => 'view', $auditoria['Cliente']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Factura'); ?></dt>
		<dd>
			<?php echo $this->Html->link($auditoria['Factura']['numero'], array('controller' => 'facturas', 'action' => 'view', $auditoria['Factura']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($auditoria['User']['username'], array('controller' => 'users', 'action' => 'view', $auditoria['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Hora'); ?></dt>
		<dd>
			<?php echo h($auditoria['Auditoria']['hora']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Motivo'); ?></dt>
		<dd>
			<?php echo h($auditoria['Auditoria']['motivo']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tipo Operacion'); ?></dt>
		<dd>
			<?php echo h($auditoria['Auditoria']['tipo_operacion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($auditoria['Auditoria']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($auditoria['Auditoria']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Auditoria'), array('action' => 'edit', $auditoria['Auditoria']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Auditoria'), array('action' => 'delete', $auditoria['Auditoria']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $auditoria['Auditoria']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Auditorias'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Auditoria'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Habitaciones'), array('controller' => 'habitaciones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Habitacione'), array('controller' => 'habitaciones', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Reserindividuales'), array('controller' => 'reserindividuales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reserindividuale'), array('controller' => 'reserindividuales', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resermultiples'), array('controller' => 'resermultiples', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resermultiple'), array('controller' => 'resermultiples', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Clientes'), array('controller' => 'clientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cliente'), array('controller' => 'clientes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Facturas'), array('controller' => 'facturas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Factura'), array('controller' => 'facturas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
