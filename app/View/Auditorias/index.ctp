<section class="content-header">
<h1>
Sistema de gestión
<small><?php echo __('Auditoria'); ?></small>
</h1>
<ol class="breadcrumb">
<li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
<li class="active"><?php echo __('Auditoria'); ?></li>
</ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Auditoria reservas'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">

						<table id="data" class="table table-striped table-bordered" width="100%" cellspacing="0">
						<thead>
						<tr>
							<th><?php echo __('Reserva'); ?></th>
							<th><?php echo __('Factura'); ?></th>
							<th><?php echo __('N&uacute;mero Habitacion'); ?></th>
							<th><?php echo __('Capacidad'); ?></th>
							<th><?php echo __('Cliente'); ?></th>
							<th><?php echo __('Operaci&oacute;n'); ?></th>
							<th><?php echo __('Especifico'); ?></th>
							
							<th><?php echo __('Detalle reserva'); ?></th>
							<th><?php echo __('Total'); ?></th>
							<th><?php echo __('Forma de pago'); ?></th>
							<th><?php echo __('Usuario'); ?></th>
							
														
						</tr>
						</thead>
						<tbody>
						<?php foreach ($auditorias as $auditoria): ?>
							<tr>
								<td><?php echo h(($auditoria['Auditoria']['reserindividuale_id'] == 0 ) ? 'MULTIPLE' : 'INDIVIDUAL') ; ?>&nbsp;</td>
								<td><?php echo h(($auditoria['Auditoria']['num_factura'] != 0 ) ? $auditoria['Auditoria']['num_factura'] : '-') ; ?>&nbsp;</td>
								<td><?php echo h($auditoria['Auditoria']['num_habitacion']); ?>&nbsp;</td>
								<td><?php echo h($auditoria['Auditoria']['capacidad']); ?>&nbsp;</td>
								<td><?php echo h($auditoria['Cliente']['dni'].' '.$auditoria['Cliente']['nombre_completo']); ?></td>
								<td><?php echo h($auditoria['Auditoria']['tipo_operacion']); ?>&nbsp;</td>
								<td><?php echo h($auditoria['Auditoria']['especifico']); ?>&nbsp;</td>
								
								<td><?php echo h($auditoria['Auditoria']['detalle_reserva']); ?>&nbsp;</td>
								<td><?php echo h($auditoria['Auditoria']['costo_total']); ?>&nbsp;</td>
								<td><?php echo h(@$auditoria['Auditoria']['forma_pago']); ?>&nbsp;</td>
								
								<td><?php echo h($auditoria['User']['username']); ?>&nbsp;</td>
								
							</tr>
						<?php endforeach; ?>
						</tbody>
						</table>
					</div>
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->


<?php /* ?><div class="actions">
<h3><?php echo __('Actions'); ?></h3>
<ul>
	<li><?php echo $this->Html->link(__('New Promarca'), array('action' => 'add')); ?></li>
	<li><?php echo $this->Html->link(__('List Protipos'), array('controller' => 'protipos', 'action' => 'index')); ?> </li>
	<li><?php echo $this->Html->link(__('New Protipo'), array('controller' => 'protipos', 'action' => 'add')); ?> </li>
</ul>
</div>
<?php */ ?><script type="text/javascript">
//$(document).ready(function() {
    $('#data').DataTable( {
    	dom: 'Bfrtlip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "language": 
        {
			"sProcessing":     "Procesando...",
			"sLengthMenu":     "Mostrar _MENU_ registros",
			"sZeroRecords":    "No se encontraron resultados",
			"sEmptyTable":     "Ningún dato disponible en esta tabla",
			"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix":    "",
			"sSearch":         "Buscar:",
			"sUrl":            "",
			"sInfoThousands":  ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst":    "Primero",
				"sLast":     "Último",
				"sNext":     "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		}
    } );
//} );
</script>
