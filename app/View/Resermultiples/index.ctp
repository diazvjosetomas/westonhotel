<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Reservación Multiple'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Reservación Multiple'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Reservación Multiple'); ?> Registrados</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                    <p><?php echo $this->Html->link(__('Crear Nuevo'), array('action' => 'add'),  array('class'=>'btn btn-primary')); ?></p>

							<table id="data" class="table table-striped table-bordered" width="100%" cellspacing="0">
							<thead>
							<tr>							<th>Reservas</th>
															<th><?php echo __('Cliente '); ?></th>
															<th><?php echo __('Tipo habitación'); ?></th>
															<th><?php echo __('Cantidad'); ?></th>
															<th><?php echo __('Fecha entrada'); ?></th>
															<th><?php echo __('Fecha salida'); ?></th>
															<th><?php echo __('Fecha tope'); ?></th>
															<th><?php echo __('Observaciones'); ?></th>
															<th><?php echo __('Status'); ?></th>
															<th class="actions"><?php echo __('Acción'); ?></th>
							</tr>
							</thead>
							<tbody>
							<?php foreach ($resermultiples as $resermultiple): ?>
	<tr>
		<td><?php echo h($resermultiple['Resermultiple']['id']); ?></td>
		<td><?php echo h($resermultiple['Cliente']['nombre_completo']); ?></td>
		<td><?php echo h($resermultiple['Tipohabitacione']['denominacion']); ?></td>
		<td><?php echo h($resermultiple['Resermultiple']['cantidad']); ?>&nbsp;</td>
		<td><?php echo h( formatdmy($resermultiple['Resermultiple']['fecha_entrada']) ); ?>&nbsp;</td>
		<td><?php echo h( formatdmy($resermultiple['Resermultiple']['fecha_salida']) ); ?>&nbsp;</td>
		<td><?php echo h( formatdmy($resermultiple['Resermultiple']['fecha_tope']) ); ?>&nbsp;</td>
		<td><?php echo h( $resermultiple['Resermultiple']['obseraciones']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link(__(h($resermultiple['Reserstatusmultiple']['denominacion'])), array('action' => 'status', $resermultiple['Resermultiple']['id']), array('class'=>'btn btn-warning')); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $resermultiple['Resermultiple']['id']), array('class'=>'')); ?>
			| <?php echo $this->Html->link(__('Ver'), array('action' => 'view', $resermultiple['Resermultiple']['id']),   array('class'=>'')); ?>
			 <?php  
	            $rol = $this->Session->read('ROL');
	            if($rol==1){
            ?>
			| 
			<?php echo $this->Html->link(__('Eliminar'), array(), array('onclick' => "eliminar(".$resermultiple['Resermultiple']['id']."); return false;", 'escape' => false));
			}
			?>
		</td>
	</tr>
<?php endforeach; ?>
							</tbody>
							</table>
						</div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->


<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Resermultiple'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Tipoclientes'), array('controller' => 'tipoclientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipocliente'), array('controller' => 'tipoclientes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipoclientesubs'), array('controller' => 'tipoclientesubs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipoclientesub'), array('controller' => 'tipoclientesubs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Clientes'), array('controller' => 'clientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cliente'), array('controller' => 'clientes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipohabitaciones'), array('controller' => 'tipohabitaciones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipohabitacione'), array('controller' => 'tipohabitaciones', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Reserstatusmultiples'), array('controller' => 'reserstatusmultiples', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reserstatusmultiple'), array('controller' => 'reserstatusmultiples', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Reserindividuales'), array('controller' => 'reserindividuales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reserindividuale'), array('controller' => 'reserindividuales', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resermulhabitaciones'), array('controller' => 'resermulhabitaciones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resermulhabitacione'), array('controller' => 'resermulhabitaciones', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?><script type="text/javascript">
	//$(document).ready(function() {
	    $('#data').DataTable( {
	    	dom: 'Bfrtlip',
	        buttons: [
	            'copy', 'csv', 'excel', 'pdf', 'print'
	        ],
	        "language": 
	        {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
	    } );


	    function eliminar(reserva){
	    	bootbox.prompt({ 
			  title: "<h4><i class='fa fa-trash'></i> Desea eliminar el registro "+reserva+ "? Si es asi, Favor especifique un motivo!</h4>", 
			  callback: function(result){ 
			  	if( result === null  || result === ''){
			  		bootbox.alert({
    					message: "<h4><i class='fa fa-remove'></i> El registro no ha sido eliminado. Debe especificar un MOTIVO!</h4>",
    				});
			  	}else{
			  		document.location.href="/resermultiples/delete/"+reserva+'/'+result;
			  	}
			   }
			});
		}

	//} );
</script>
