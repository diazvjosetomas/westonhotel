<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Resermultiples'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Resermultiples'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Add Resermultiple'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Resermultiple', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
							<?php
	
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Resermultipleetipocliente_id">Tipo de cliente</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('tipocliente_id', array('id'=>'Resermultipleetipocliente_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', "onChange"=>"selectTagRemote('".$this->Html->url('/Resermultiples/sub_cliente')."', 'div-subcliente', this.value);", 'empty'=>'--Seleccione--')); 
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Resermultipleetipoclientesub_id">Tipo de sub cliente</label>';		
									echo'<div class="col-md-9" id="div-subcliente">';			
									echo $this->Form->input('tipoclientesub_id', array('id'=>'Resermultipleetipoclientesub_id', 'div'=>false, 'label'=>false, 'class'=>'form-control','options'=>array()));
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Resermultipleecliente_id">Cliente/ Añadir <img title="Agregar Cliente" style="icon:pointer;" data-toggle="modal" data-target="#myModal" src="'.$this->webroot.'img/adduser.png'.'"> </label>';			
									echo'<div class="col-md-9" id="div-cliente">';			
									echo $this->Form->input('cliente_id', array('id'=>'Resermultipleecliente_id', 'div'=>false, 'label'=>false, 'class'=>'form-control','options'=>array()));
									echo '</div>';	
									echo '</div>';
	
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Resermultipletipohabitacione_id">Tipo habitación</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('tipohabitacione_id', array('id'=>'Resermultipletipohabitacione_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', "onChange"=>"selectTagRemote('".$this->Html->url('/Resermultiples/fecha_entrada')."', 'div-fechaentrada', this.value);", 'empty'=>'--Seleccione--')); 	
									echo '</div>';	
									echo '</div>';
								?>
								<div id="div-habitacion-conten">
	                                <div class="form-group">
	                                	<label class="control-label col-md-2" for="Resermultiplehabitacione_id">Habitación</label>
		                                <div class="col-md-10">
				                                <div class="row">
				                                	<div class="col-md-4">
						                                <div id="div-habitacion">
															<select name="origen[]" id="origen" multiple="multiple" size="8" class="select_">
															</select>
														</div>
													</div>
													<div class="col-md-4">
														<div>
															<input type="button" class="pasar izq input_" value="Pasar »"><input type="button" class="quitar der input_" value="« Quitar"><br />
															<input type="button" class="pasartodos izq input_" value="Todos »"><input type="button" class="quitartodos der input_" value="« Todos">
														</div>
													</div>
													<div class="col-md-4">
														<div class="">
															<select name="destino[]" id="destino" multiple="multiple" size="8" class="select_"></select>
														</div>
													</div>
												</div>
										</div>
									</div>
								</div>
								<?php
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Resermultiplecantidad">Cantidad</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('cantidad',     array('value'=>0, 'readonly'=>true, 'id'=>'Resermultiplecantidad', 'div'=>false, 'label'=>false, 'class'=>'form-control',"onChange"=>'javascript:calcular_multiple_habitacion();'));		
									echo $this->Form->input('habitaciones', array('value'=>0, 'readonly'=>true, 'id'=>'Resermultiplehabitaciones', 'div'=>false, 'label'=>false, 'class'=>'form-control',"type"=>'hidden'));		
									echo '</div>';	
									echo '</div>';
	
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Resermultiplefecha_entrada">Fecha entrada</label>';		
									echo'<div class="col-md-9" id="div-fechaentrada">';			
									echo $this->Form->input('fecha_entrada', array('id'=>'Resermultiplefecha_entrada', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
	
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Resermultiplefecha_salida">Fecha salida</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('fecha_salida', array('id'=>'Resermultiplefecha_salida', 'div'=>false, 'label'=>false, 'class'=>'form-control',"onChange"=>'javascript:calcular_multiple_habitacion();'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Resermultiplefecha_tope">Fecha tope</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('fecha_tope', array('id'=>'Resermultiplefecha_tope', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Resermultipleobseraciones">Observaciones</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('obseraciones', array('id'=>'Resermultipleobseraciones', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Resermultipledias">Dias</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('dias', array('value'=>0, 'id'=>'Resermultipledias', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>false, "onChange"=>'recalcular_multiple();'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Resermultipleprecioxdia">Precio x Habitación x dia</label>';		
									echo'<div class="col-md-9" id="div-precioxdia">';			
									echo $this->Form->input('precioxdia', array('value'=>0, 'id'=>'Resermultipleprecioxdia', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>false, "onChange"=>'recalcular_multiple();'));			
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Resermultipledescuento">Descuento (%)</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('descuento', array('value'=>0, 'id'=>'Resermultipledescuento', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>false, "onChange"=>'recalcular_multiple();'));	
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Resermultipletotal">Total</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('total', array('value'=>0, 'id'=>'Resermultipletotal', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>true));	
									echo '</div>';	
									echo '</div>';
							?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agregar Cliente</h4>
      </div>
      <div class="modal-body">

      		                <div class="box-body">
      							<?php echo $this->Form->create('Cliente', array('class'=>'form-horizontal','url'=>'/Clientes/add')); ?>
      							<div class='row'>
      									<div class='col-md-12'>
      										<?php
      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientetipocliente_id">Tipo cliente</label>';		
      											echo'<div class="col-md-9">';			
      											echo $this->Form->input('tipocliente_id', array('id'=>'Clientetipocliente_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', "onChange"=>"selectTagRemote('".$this->Html->url('/Clientes/sub_cliente')."', 'div-subcliente2', this.value);", 'empty'=>'--Seleccione--')); 
      											echo '</div>';	
      											echo '</div>';
      												
      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientetipoclientesub_id">Tipo subcliente</label>';		
      											echo'<div class="col-md-9" id="div-subcliente2">';			
      											echo $this->Form->input('tipoclientesub_id', array('id'=>'Clientetipoclientesub_id', 'div'=>false, 'label'=>false, 'class'=>'form-control','options'=>array()));		
      											echo '</div>';	
      											echo '</div>';
      												
      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientedni">Dni</label>';		
      											echo'<div class="col-md-9">';			
      											echo $this->Form->input('dni', array('id'=>'Clientedni', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
      											echo '</div>';	
      											echo '</div>';
      												
      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientenombre_completo">Nombre</label>';		
      											echo'<div class="col-md-9">';			
      											echo $this->Form->input('nombre_completo', array('id'=>'Clientenombre_completo', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
      											echo '</div>';	
      											echo '</div>';
      												
      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientedireccion">Dirección</label>';		
      											echo'<div class="col-md-9">';			
      											echo $this->Form->input('direccion', array('id'=>'Clientedireccion', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
      											echo '</div>';	
      											echo '</div>';
      												
      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientetelf">Telf</label>';		
      											echo'<div class="col-md-9">';			
      											echo $this->Form->input('telf', array('id'=>'Clientetelf', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
      											echo '</div>';	
      											echo '</div>';
      												
      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientecorreo">Email</label>';		
      											echo'<div class="col-md-9">';			
      											echo $this->Form->input('correo', array('id'=>'Clientecorreo', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
      											echo '</div>';	
      											echo '</div>';
      												
      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientecod_pos">Cod pos</label>';		
      											echo'<div class="col-md-9">';			
      											echo $this->Form->input('cod_pos', array('id'=>'Clientecod_pos', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
      											echo '</div>';	
      											echo '</div>';
      										?>
      									</div>
      									<div class="col-md-12">
      										<div class="form-group">
      			                                <div class="col-md-12">
      			                                <a href="#" onclick="guardar()" class="btn btn-primary pull-right">Guardar</a>    
      			                                <!--<input  value="Guardar" class="btn btn-primary pull-right" type="submit">-->
      			                                </div>
      			                            </div>
      		                            </div>
      							</div></form>                </div>

      </div>
      <div class="modal-footer">
        
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
	function guardar(){
		$.post('/Clientes/add',$("#ClienteAddForm").serialize(),function(response){
			window.location.reload();
		});
		// $.ajax({
		//     url:"'/Clientes/add'"+$("#ClienteAdd2Form").serialize(),
		//     type:"post",
		//     data:{
		          
		//         },
		//     success:function(response){
		//       if(response != 0){
		//         window.location.reload();
		//       }else{
		//         console.log('Ocurrio un error!');
		//       }
		//     }
		// });
	}
</script>