<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Reservación Multiple'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Reservación Multiple'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Reservación Multiple'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
							<dt><?php echo __('Tipo de cliente'); ?></dt>
							<dd>
								<?php echo h($resermultiple['Tipocliente']['denominacion']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Tipo de sub cliente'); ?></dt>
							<dd>
								<?php echo h($resermultiple['Tipoclientesub']['nombre']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Cliente'); ?></dt>
							<dd>
								<?php echo h($resermultiple['Cliente']['nombre_completo']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Tipo de habitación'); ?></dt>
							<dd>
								<?php echo h($resermultiple['Tipohabitacione']['denominacion']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Cantidad'); ?></dt>
							<dd>
								<?php echo h($resermultiple['Resermultiple']['cantidad']); ?>
								&nbsp;
							</dd>
                           





							<dt><?php echo __('Status'); ?></dt>
							<dd>
								<?php echo h($resermultiple['Reserstatusmultiple']['denominacion']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Fecha Entrada'); ?></dt>
							<dd>
								<?php echo h( formatdmy($resermultiple['Resermultiple']['fecha_entrada']) ); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Fecha Salida'); ?></dt>
							<dd>
								<?php echo h( formatdmy($resermultiple['Resermultiple']['fecha_salida']) ); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Fecha Tope'); ?></dt>
							<dd>
								<?php echo h( formatdmy($resermultiple['Resermultiple']['fecha_tope']) ); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Obseraciones'); ?></dt>
							<dd>
								<?php echo h($resermultiple['Resermultiple']['obseraciones']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Dias'); ?></dt>
							<dd>
								<?php echo h($resermultiple['Resermultiple']['dias']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Precioxdia'); ?></dt>
							<dd>
								<?php echo h($resermultiple['Resermultiple']['precioxdia']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Descuento al total'); ?></dt>
							<dd>
								<?php echo h($resermultiple['Resermultiple']['descuento']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Total'); ?></dt>
							<dd>
								<?php echo h($resermultiple['Resermultiple']['total']); ?>
								&nbsp;
							</dd>
						</dl>
						
						<table id="data" class="table table-striped table-bordered" width="100%" cellspacing="0">
						<tr>
								<th><?php echo __('Tipo de habitación');?></th>
								<th><?php echo __('Número de habitación');?></th>
								<th><?php echo __('Asignar');?></th>
						</tr>
						<?php
						$i = 0;
						$total = 0;
						foreach ($resermultiple["Resermulhabitacione"] as $resermultiple_): ?>
						<tr>
						    <td><?php echo h($resermultiple['Tipohabitacione']['denominacion']); ?>&nbsp;</td>
							<td><?php echo h($resermultiple_['Habitacione']['numhabitacion']); ?>&nbsp;</td>
							<td>
							<?php 
								if($resermultiple_['ocupada']==0){
								    echo $this->Html->link(__('Asignar'), '/Reserindividuales/add_m/'.$resermultiple['Resermultiple']['id'].'/'.$resermultiple_['habitacione_id']);
								}else{
									echo "Ocupada";
								}
							?>&nbsp;</td>
						</tr>
						<?php endforeach; ?>
						</table>
							
	                    <br>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $resermultiple['Resermultiple']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->

<?php pr($resermultiple) ?>