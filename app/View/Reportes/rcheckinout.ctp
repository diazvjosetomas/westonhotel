
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Check In / Out '); ?> </h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                    <!-- <input type="button" onclick="buscar(<?=$tipo?>)"  value="PDF"> -->
                   <!-- <input type="button" onclick="buscarExcell(<?=$tipo?>)"  value="Excel"> -->
             

	<table id="data" class="table table-striped table-bordered" width="100%" cellspacing="0">
		<thead>
			<tr>
			<th><?php echo __('HABITACION'); ?></th>
			<th><?php echo __('RESERVA'); ?></th>
			<th><?php echo __('PASAJERO'); ?></th>
			<th><?php echo __('CANT PERSONAS'); ?></th>
			<th><?php echo __('DESAYUNO'); ?></th>
			<th><?php echo __('ALMUERZO'); ?></th>
			<th><?php echo __('CENA'); ?></th>
			
			</tr>
		</thead>
		<tbody>
		<?php foreach ($reserindividuales as $key ): ?>	
		<?php 
			$fecha = '';

			foreach ($key['Reserindivistatus'] as $kfecha) {
				if($kfecha['reserstatusindividuale_id']==4){
			       $fecha = $kfecha['created'];
				}
			}

		 ?>				
		<tr>
			<td><?php echo $key['Habitacione']['numhabitacion']; ?></td>
			<td><?php echo $key['Reserindividuale']['id']; ?></td>
			<td><?php echo $key['Cliente']['nombre_completo']; ?></td>
			<td><?php echo $key['Reserindividuale']['cantidad_personas']; ?></td>
			<td>
				<?php 
				if ($key['Reserindividuale']['desayuno'] == 1) {
				 	echo "Desayuno";
				 } ?>
			</td>
			<td>
				<?php 
				if ($key['Reserindividuale']['almuerzo'] == 1) {
				 	echo "Almuerzo";
				 } ?>
			</td>
			<td>
				<?php 
				if ($key['Reserindividuale']['cena'] == 1) {
				 	echo "Cena";
				 } ?>
			</td>
		</tr>
		<?php endforeach ?>
			

		</tbody>
	</table>
						</div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->


<script type="text/javascript">
	//$(document).ready(function() {
	    $('#data').DataTable( {
	    	dom: 'Bfrtlip',
	        buttons: [
	          'copy', 'csv', 'excel', 'pdf', 'print' 
	        ],
	        "language": 
	        {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				}
			}
	    } );
	//} );
</script>
