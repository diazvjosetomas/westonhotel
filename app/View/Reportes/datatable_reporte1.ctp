<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Pasajeros Alojados'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Pasajeros Alojados'); ?> </h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
             

	<table id="data" class="table table-striped table-bordered" width="100%" cellspacing="0">
		<thead>
			<tr>
			<th><?php echo __('Reserva'); ?></th>
			<th><?php echo __('Tipo habitación'); ?></th>
			<th><?php echo __('Habitación'); ?></th>
			<th><?php echo __('Cliente'); ?></th>
			<th><?php echo __('Fecha entrada'); ?></th>
			<th><?php echo __('Fecha salida'); ?></th>
			<th><?php echo __('Status'); ?></th>
			<th class="actions"><?php echo __('Acción'); ?></th>
			</tr>
		</thead>
		<tbody>
						
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>
				
			</td>
			<td class="actions">
				
			</td>
		</tr>

		</tbody>
	</table>
						</div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->


<script type="text/javascript">
	//$(document).ready(function() {
	    $('#data').DataTable( {
	    	dom: 'Bfrtlip',
	        buttons: [
	            'copy', 'csv', 'excel', 'pdf', 'print'
	        ],
	        "language": 
	        {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
	    } );
	//} );
</script>
