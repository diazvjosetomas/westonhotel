<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Ingresos / Egresos'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Pasajeros último Pago'); ?> </h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                    <input type="button" onclick="buscar(<?=$idCliente?>)"  value="PDF">
                    <input type="button" onclick="buscarExcell(<?=$idCliente?>)"  value="Excel">
             

	<table id="data" class="table table-striped table-bordered" width="100%" cellspacing="0">
		<thead>
			<tr>
			<th><?php echo __('DOCUMENTO'); ?></th>
			<th><?php echo __('NOMBRE'); ?></th>
			<th><?php echo __('NACIONALIDAD'); ?></th>
			<th><?php echo __('TOTAL'); ?></th>
			<th><?php echo __('PAGADO'); ?></th>
			<th><?php echo __('TIPO PAGO'); ?></th>
			<th><?php echo __('FECHA DE PAGO'); ?></th>
			
			
			</tr>
		</thead>
		<tbody>
		<?php 
			$cantidadHab = 0;
			$cantidadPersonas = 0;

		 ?>	
		<?php foreach ($facturas as $key ): ?>	
				
		<tr>
			<td><?php echo 'DU '.$key['Factura']['Cliente']['dni']; ?></td>
			<td><?php echo $key['Factura']['Cliente']['nombre_completo'];?></td>
			<td></td>
			<td><?php echo $key['Factura']['total'];?></td>
			<td><?php echo $key['Factura']['pagado'];?></td>
			<td><?php echo $key['Facturatipopago']['denominacion'];?></td>
			<td><?php echo $key['Facturapago']['fecha_pago'];?></td>
		</tr>


		
		<?php endforeach ?>
			

		</tbody>
	</table>




						</div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->


<script type="text/javascript">
	//$(document).ready(function() {
	    $('#data').DataTable( {
	    	dom: 'Bfrtlip',
	        buttons: [
	           
	        ],
	        "language": 
	        {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				}
			}
	    } );
	//} );
</script>


