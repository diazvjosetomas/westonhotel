<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Ingresos / Egresos'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Pasajeros Alojados'); ?> </h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                    <input type="button" onclick="buscar()"  value="PDF">
                    <input type="button" onclick="buscarExcell()"  value="Excel">
             

	<table id="data" class="table table-striped table-bordered" width="100%" cellspacing="0">
		<thead>
			<tr>
			<th><?php echo __('DOCUMENTO'); ?></th>
			<th><?php echo __('NOMBRE'); ?></th>
			<th><?php echo __('NACIONALIDAD'); ?></th>
			<th><?php echo __('INGRESO/HORA'); ?></th>
			<th><?php echo __('EGRESO/HORA'); ?></th>
			<th><?php echo __('HABITACION'); ?></th>
			<th><?php echo __('RESERVA'); ?></th>
			
			</tr>
		</thead>
		<tbody>
		<?php 
			$cantidadHab = 0;
			$cantidadPersonas = 0;

		 ?>	
		<?php foreach ($reserindividuales as $key ): ?>	

		
			
				
		<tr>
			<td><?php echo 'DU '.$key['Cliente']['dni']; ?></td>
			<td><?php echo $key['Cliente']['nombre_completo']; ?></td>
			<td><?php isset($key['Cliente']['Pai']['denominacion'])?$key['Cliente']['Pai']['denominacion']:''; ?></td>
			<td><?php echo $key['Reserindividuale']['fecha_entrada']; ?></td>
			<td><?php echo $key['Reserindividuale']['fecha_salida']; ?></td>
			<td><?php echo $key['Habitacione']['numhabitacion']; ?></td>
			<td><?php echo $key['Reserindividuale']['id']; ?></td>
		</tr>


		<?php 
			$cantidadPersonas += $key['Reserindividuale']['cantidad_personas'];

			$cantidadHab++;
		 ?>
		<?php endforeach ?>
			

		</tbody>
	</table>
	<?php 
		$cant_entrada_data = 0;
		$cant_salida_data = 0;
		foreach ($cant_entrada as $key) {
			$cant_entrada_data += $key['Reserindividuale']['cantidad_personas'];
		}
		foreach ($cant_salida as $key) {
			$cant_salida_data += $key['Reserindividuale']['cantidad_personas'];
		}

	 ?>

	<h3>Cantidad de Habitaciones: <?php echo $cantidadHab ?></h3>
	<h3>Cantidad de Personas: <?php echo $cantidadPersonas ?></h3>
	<h3>Cantidad de Personas Entrada: <?php echo $cant_entrada_data ?></h3>
	<h3>Cantidad de Personas Salida: <?php echo $cant_salida_data ?></h3>

						</div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->


<script type="text/javascript">
	//$(document).ready(function() {
	    $('#data').DataTable( {
	    	dom: 'Bfrtlip',
	        buttons: [
	           
	        ],
	        "language": 
	        {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				}
			}
	    } );
	//} );
</script>

