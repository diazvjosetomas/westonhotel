
<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Reportes'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Reportes'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Pasajero último pago'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Reserindividuale', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualetipocliente_id">Tipo de cliente</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('tipocliente_id', array('id'=>'Reserindividualetipocliente_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', "onChange"=>"selectTagRemote('".$this->Html->url('/Reportes/sub_cliente')."', 'div-subcliente', this.value);", 'empty'=>'--Seleccione--')); 
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualetipoclientesub_id">Tipo de sub cliente</label>';		
									echo'<div class="col-md-9" id="div-subcliente">';			
									echo $this->Form->input('tipoclientesub_id', array('id'=>'Reserindividualetipoclientesub_id', 'div'=>false, 'label'=>false, 'class'=>'form-control','options'=>array()));
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualecliente_id">Cliente</label>';			
									echo'<div class="col-md-9" id="div-cliente">';			
									echo $this->Form->input('cliente_id', array('id'=>'Reserindividualecliente_id', 'div'=>false, 'label'=>false, 'class'=>'form-control','options'=>array()));
									echo '</div>';	
									echo '</div>';
								?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                     <input type="button" onclick="buscarAjax()"  value="Ver Reporte">
                                		 
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<section id="volcadatos"></section>
<script type="text/javascript">

	function buscarAjax(){
		var idCliente = $("#ReserindividualeClienteId").val();
			
			if (idCliente == '') {
				alert('Debe seleccionar un cliente');
				return false;
			}


			$.ajax({
			    url:'/Reportes/reporte6ajax/'+idCliente,
			    type:'post',
			    data:{},
			    success:function(response){

			        $("#volcadatos").html(response);

			    }
			});
	}

	function buscar(idCliente){
		
		window.open('/Reportes/reporte6/'+idCliente,'_self');
	}

	function buscarExcell(idCliente){
		
		window.open('/Reportes/reporte6_excell/'+idCliente,'_self');

	}
</script>