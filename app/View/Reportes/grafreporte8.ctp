<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Reporte Check In / Check Out : '.date('d-m-Y')); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Reporte Check In / Check Out'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Protipo', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class="col-md-12">
                            <form action="/Reportes/reporte1/" method="post" >

                                    <div class="row">
                                        <div class="col-md-4">
                                            <input id="fecha1" type="text" name="fecha1" class="form-control hasDatePicker" placeholder="Fecha de Entrada" value="<?=date('Y-m-d')?>">
                                        </div>
                                        <div class="col-md-4">
                                            <input id="fecha2" type="text" name="fecha1" class="form-control hasDatePicker" placeholder="Fecha de Salida" value="<?=date('Y-m-d')?>">
                                        </div>
                                        <div class="col-md-4">
                                            <select id="estados" class="form-control">
                                                <option value="1"> INICIADO </option>
                                                <option value="2"> CONFIRMADO </option>
                                                <option value="3"> AUDITADA </option>
                                                <option value="4"> INGRESADA </option>
                                                <option value="5"> EGRESADA </option>
                                                <option value="6"> NO SHOW </option>
                                            </select>
                                        </div>
                                    </div>

                                    <br>
                                    
                                  <input type="button" onclick="buscarAjax()"  value="Ver Reporte">
                             </form>
                                
                            </div>
							 





					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<section id="volcadatos"></section>
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Html->link(__('List Protipos'), array('action' => 'index')); ?></li>
			</ul>
	</div>
<?php */ ?>


<script type="text/javascript">
$("#fecha1").datepicker();
$("#fecha2").datepicker();


    
    function buscarAjax(){
        var f1 = $("#fecha1").val();
        var f2 = $("#fecha2").val();
        var estados = $("#estados").val();

        if (f1 == '' || f2 == '') {
            alert('Debe ingresar ambas fechas');
            return false;
        }

        var fe1 = f1.split("/");
        var fecha1 = fe1[2] + "-" + fe1[1] + "-" + fe1[0];

        var fe2 = f2.split("/");
        var fecha2 = fe2[2] + "-" + fe2[1] + "-" + fe2[0];

        $.ajax({
            url:'/Reportes/rcheckinout/'+fecha1+'/'+fecha2+'/'+estados,
            type:'post',
            data:{},
            success:function(response){

                $("#volcadatos").html(response);

            }
        });
    }


    function buscar(tipo){

        var fecha1 = $("#fecha1").val();
        var fecha2 = $("#fecha2").val();

        if (fecha1 == '' || fecha2 == '') {
            alert('Debe ingresar ambas fechas');
            return false;
        }

        var estados = $("#estados").val();

        window.open('/Reportes/rcheckinoutpdf/'+fecha1+'/'+fecha2+'/'+estados,'_self');
    }


    function buscarExcell(tipo){

        var fecha1 = $("#fecha1").val();
        var fecha2 = $("#fecha2").val();

        if (fecha1 == '' || fecha2 == '') {
            alert('Debe ingresar ambas fechas');
            return false;
        }

        var estados = $("#estados").val();

        window.open('/Reportes/reporte3_excell/'+fecha1+'/'+fecha2+'/'+estados,'_self');
    }



</script>