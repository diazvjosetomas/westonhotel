<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Reporte pasajeros Ingresos'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Pasajeros Ingresos'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Pasajeros Ingresos'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Protipo', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class="col-md-12">
                            <form action="/Reportes/reporte1/" method="post" >

                                    <div class="row">
                                        <div class="col-md-6">
                                            <input id="fecha1" type="text" name="fecha1" class="form-control hasDatePicker" placeholder="Fecha de Entrada">
                                        </div>
                                        <div class="col-md-6">
                                            <input id="fecha2" type="text" name="fecha1" class="form-control hasDatePicker" placeholder="Fecha de Salida">
                                        </div>
                                    </div>

                                    <br>
                                    
                                 <input type="button" onclick="buscar()"  value="Ver Reporte PDF">
                                 <input type="button" onclick="buscarExcell()"  value="Ver Reporte Excel">
                             </form>
                                
                            </div>
							 





					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Html->link(__('List Protipos'), array('action' => 'index')); ?></li>
			</ul>
	</div>
<?php */ ?>


<script type="text/javascript">
$("#fecha1").datepicker();
$("#fecha2").datepicker();



    function buscar(){

        var f1 = $("#fecha1").val();
        var f2 = $("#fecha2").val();

        if (f1 == '' || f2 == '') {
            alert('Debe ingresar ambas fechas');
            return false;
        }

        var fe1 = f1.split("/");
        var fecha1 = fe1[2] + "-" + fe1[1] + "-" + fe1[0];

        var fe2 = f2.split("/");
        var fecha2 = fe2[2] + "-" + fe2[1] + "-" + fe2[0];

        window.open('/Reportes/reporte7/'+fecha1+'/'+fecha2,'_self');
        

    }

    function buscarExcell(){

        var fecha1 = $("#fecha1").val();
        var fecha2 = $("#fecha2").val();

        if (fecha1 == '' || fecha2 == '') {
            alert('Debe ingresar ambas fechas');
            return false;
        }

        window.open('/Reportes/reporte7_excell/'+fecha1+'/'+fecha2,'_self');
        

    }
</script>