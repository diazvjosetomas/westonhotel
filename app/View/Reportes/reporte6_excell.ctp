<?php 
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("JCloudTechonologies")
							 ->setLastModifiedBy("JOSE & JUAN")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("");
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')
                                          ->setSize(10);
	
	// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'DOCUMENTO')
            ->setCellValue('B1', 'NOMBRE')
            ->setCellValue('C1', 'NACIONALIDAD')
            ->setCellValue('D1', 'TOTAL')
            ->setCellValue('E1', 'PAGADO')
            ->setCellValue('F1', 'TIPO PAGO')
            ->setCellValue('G1', 'FECHA DE PAGO');
$cont = 2;

foreach ($facturas as $key) {
	

	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$cont,'DU '.$key['Factura']['Cliente']['dni'])
				->setCellValue('B'.$cont,$key['Factura']['Cliente']['nombre_completo'])
				->setCellValue('C'.$cont,'','')
				->setCellValue('D'.$cont,$key['Factura']['total'])
				->setCellValue('E'.$cont,$key['Factura']['pagado'])
				->setCellValue('F'.$cont,$key['Facturatipopago']['denominacion'])
				->setCellValue('G'.$cont,$key['Facturapago']['fecha_pago']);
	$cont++;
}


// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle($name);


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);


// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$name.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;



?>
 		
