<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Reporte '); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Pasajeros Ingresos / Egresos'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Habitaciones'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Protipo', array('class'=>'form-horizontal')); ?>
					<div class='row'>
                            <div class="col-md-12">
                            <form action="/Reportes/reporte1/" method="post" >

                                    <div class="row">
                                        <div class="col-md-6">
                                            <input id="fecha1" type="text" name="fecha1" class="form-control hasDatePicker" placeholder="Fecha">
                                        </div>
                                      
                                  </div>

                                  <br>
                                    
                                  <input type="button" onclick="buscarAjax()"  value="Ver Reporte">


                             </form>
                                
                            </div>
                             





                    </div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<section id="volcadatos"></section>
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Html->link(__('List Protipos'), array('action' => 'index')); ?></li>
			</ul>
	</div>
<?php */ ?>


<script type="text/javascript">
$("#fecha1").datepicker();




    function buscarAjax(){
        var f1 = $("#fecha1").val();
     

        if (f1 == '') {
            alert('Debe ingresar ambas fechas');
            return false;
        }

        var fe1 = f1.split("/");
        var fecha1 = fe1[2] + "-" + fe1[1] + "-" + fe1[0];

        $.ajax({
            url:'/Reportes/reporte5ajax/'+fecha1,
            type:'post',
            data:{},
            success:function(response){

                $("#volcadatos").html(response);

            }
        });

    }





    function buscar(){

        var fecha1 = $("#fecha1").val();
        var fecha2 = $("#fecha2").val();


        

        

        window.open('/Reportes/reporte5/','_self');
        

    }

    function buscarExcell(){

        var fecha1 = $("#fecha1").val();
        var fecha2 = $("#fecha2").val();

        if (fecha1 == '' || fecha2 == '') {
            alert('Debe ingresar ambas fechas');
            return false;
        }

        window.open('/Reportes/reporte5_excell/','_self');
        

    }
</script>