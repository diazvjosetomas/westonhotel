<?php 
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("JCloudTechonologies")
							 ->setLastModifiedBy("JOSE & JUAN")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("");
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')
                                          ->setSize(10);
	
	// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'DOCUMENTO')
            ->setCellValue('B1', 'NOMBRE')
            ->setCellValue('C1', 'AUTOMOVIL')
            ->setCellValue('D1', 'INGRESO / HORA')
            ->setCellValue('E1', 'HABITACION')
            ->setCellValue('F1', 'RESERVA')
            ->setCellValue('G1', 'CANTIDAD DE HABITACIONES')
            ->setCellValue('H1', 'CANTIDAD DE PERSONAS');
$cont = 2;
$cantidadHab = 0;
$cantidadPersonas = 0;

foreach ($reserindividuales as $value) {
	$fecha = "";
    foreach ($value['Reserindivistatus'] as $key) {
    	if($key['reserstatusindividuale_id']==4){
           $fecha = $key['created'];
    	}
    }
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$cont,'DU '.$value['Cliente']['dni'])
				->setCellValue('B'.$cont,$value['Cliente']['nombre_completo'])
				->setCellValue('C'.$cont,$value['Reserindividuale']['automovil'])
				->setCellValue('D'.$cont,$fecha)
				->setCellValue('E'.$cont,$value['Habitacione']['numhabitacion'])
				->setCellValue('F'.$cont,$value['Reserindividuale']['id']);
	$cont++;
	$cantidadPersonas += $value['Reserindividuale']['cantidad_personas'];

	$cantidadHab++;
}


$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('G2', $cantidadHab)
			->setCellValue('H2', $cantidadPersonas);





// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle($name);


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);


// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$name.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;



?>
 		
