<?php
class fpdfview extends FPDF{
	function Footer(){}
	function Header(){

		
	}
}
$fpdf = new fpdfview('P','mm','A4');
$fpdf->AddPage();

$fpdf->Ln(5);
$fpdf->SetFont('Arial','B',22);
$fpdf->Cell(0,5,"INGRESOS/EGRESOS DIARIOS ".date('d/m/Y'),'',1,'C');

$fpdf->Ln(2);
$fpdf->SetFont('Arial','',8);
$fpdf->Cell(0,5,"ORDENADOS POR NOMBRES",'',1,'C');

$fpdf->Ln(3);
$fpdf->SetFont('Arial','',6);
$fpdf->Cell(25,5,"DOCUMENTO",'LTB',0,'C');
$fpdf->Cell(60,5,"NOMBRE",'TB',0,'C');
$fpdf->Cell(25,5,"NACIONALIDAD",'TB',0,'C');
$fpdf->Cell(25,5,"INGRESO/HORA",'TB',0,'C');
$fpdf->Cell(25,5,"EGRESO/HORA",'TB',0,'C');
$fpdf->Cell(10,5,"HABITACIÓN",'TB',0,'C');
$fpdf->Cell(0,5,"RESERVA",'TBR',1,'C');
//pr($reserindividuales);
$cantidadHab = 0;
$cantidadPersonas = 0;
foreach ($reserindividuales as $value) {
	$fpdf->Cell(5,5,"DU",'',0,'L');
	$fpdf->Cell(20,5,$value['Cliente']['dni'],'',0,'R');
	$fpdf->Cell(60,5,$value['Cliente']['nombre_completo'],'',0,'L');
	$fpdf->Cell(25,5,isset($value['Cliente']['Pai']['denominacion'])?$value['Cliente']['Pai']['denominacion']:'','',0,'C');
	$fecha = "";
    foreach ($value['Reserindivistatus'] as $key) {
    	if($key['reserstatusindividuale_id']==4){
           $fecha = $key['created'];
    	}
    }
	$fpdf->Cell(25,5,$value['Reserindividuale']['fecha_entrada'],'',0,'C');
	$fpdf->Cell(25,5,$value['Reserindividuale']['fecha_salida'],'',0,'C');
	$fpdf->Cell(15,5,$value['Habitacione']['numhabitacion'],'',0,'C');
	$fpdf->Cell(0,5,$value['Reserindividuale']['id'],'',1,'C');

		$cantidadPersonas += $value['Reserindividuale']['cantidad_personas'];

	$cantidadHab++;
}

 
		$cant_entrada_data = 0;
		$cant_salida_data = 0;
		foreach ($cant_entrada as $key) {
			$cant_entrada_data += $key['Reserindividuale']['cantidad_personas'];
		}
		foreach ($cant_salida as $key) {
			$cant_salida_data += $key['Reserindividuale']['cantidad_personas'];
		}

	 

$fpdf->Ln(5);

$fpdf->SetFont('Arial','B',12);
$fpdf->Cell(5,5,ucwords($info),'',0,'L');
$fpdf->Ln(5);
$fpdf->Cell(5,5,ucwords('Cantidad de habitaciones: '.$cantidadHab),'',0,'L');
$fpdf->Ln(5);
$fpdf->Cell(5,5,ucwords('Cantidad de Personas: '.$cantidadPersonas),'',0,'L');
$fpdf->Ln(5);
$fpdf->Cell(5,5,ucwords('Cantidad de Personas Entrada: '.$cant_entrada_data),'',0,'L');
$fpdf->Ln(5);
$fpdf->Cell(5,5,ucwords('Cantidad de Personas Salida: '.$cant_salida_data),'',0,'L');
$fpdf->Output('D',$name);
?>