<?php 
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("JCloudTechonologies")
							 ->setLastModifiedBy("JOSE & JUAN")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("");
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')
                                          ->setSize(10);
	
	// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'DOCUMENTO')
            ->setCellValue('B1', 'NOMBRE')
            ->setCellValue('C1', 'NACIONALIDAD')
            ->setCellValue('D1', 'INGRESO / HORA')
            ->setCellValue('E1', 'EGRESO / HORA')
            ->setCellValue('F1', 'HABITACION')
            ->setCellValue('G1', 'RESERVA')
            ->setCellValue('H1', 'CANTIDAD DE HABITACIONES')
            ->setCellValue('I1', 'CANTIDAD DE PERSONAS')
            ->setCellValue('J1', 'CANTIDAD DE PERSONAS (Entrada)')
            ->setCellValue('K1', 'CANTIDAD DE PERSONAS (Salida)');
$cont = 2;
$cantidadHab = 0;
$cantidadPersonas = 0;

foreach ($reserindividuales as $value) {
	$fecha = "";
    foreach ($value['Reserindivistatus'] as $key) {
    	if($key['reserstatusindividuale_id']==4){
           $fecha = $key['created'];
    	}
    }
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$cont,'DU '.$value['Cliente']['dni'])
				->setCellValue('B'.$cont,$value['Cliente']['nombre_completo'])
				->setCellValue('C'.$cont,isset($value['Cliente']['Pai']['denominacion'])?$value['Cliente']['Pai']['denominacion']:'','')
				->setCellValue('D'.$cont,$value['Reserindividuale']['fecha_entrada'])
				->setCellValue('E'.$cont,$value['Reserindividuale']['fecha_salida'])
				->setCellValue('F'.$cont,$value['Habitacione']['numhabitacion'])
				->setCellValue('G'.$cont,$value['Reserindividuale']['id']);
	$cont++;
	$cantidadPersonas += $value['Reserindividuale']['cantidad_personas'];

	$cantidadHab++;
}


		$cant_entrada_data = 0;
		$cant_salida_data = 0;
		foreach ($cant_entrada as $key) {
			$cant_entrada_data += $key['Reserindividuale']['cantidad_personas'];
		}
		foreach ($cant_salida as $key) {
			$cant_salida_data += $key['Reserindividuale']['cantidad_personas'];
		}

	



$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('H2', $cantidadHab)
			->setCellValue('I2', $cantidadPersonas)
			->setCellValue('J2', $cant_entrada_data)
			->setCellValue('K2', $cant_salida_data);




// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle($name);


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);


// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$name.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;



?>
 		
