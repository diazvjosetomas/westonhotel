<?php 
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("JCloudTechonologies")
							 ->setLastModifiedBy("JOSE & JUAN")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("");
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')
                                          ->setSize(10);
	
	// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'HABITACION')
            ->setCellValue('B1', 'RESERVA')
            ->setCellValue('C1', 'PASAJEROS')
            ->setCellValue('D1', 'DESAYUNO')
            ->setCellValue('E1', 'ALMERZO')
            ->setCellValue('F1', 'CENA')
            ->setCellValue('G1', 'CANTIDAD DE HABITACIONES')
            ->setCellValue('H1', 'CANTIDAD DE PERSONAS');
$cont = 2;
$cantidadHab = 0;
$cantidadPersonas = 0;

foreach ($reserindividuales as $value) {
	$fecha = "";
    foreach ($value['Reserindivistatus'] as $key) {
    	if($key['reserstatusindividuale_id']==4){
           $fecha = $key['created'];
    	}
    }
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$cont,$value['Habitacione']['numhabitacion'])
				->setCellValue('B'.$cont,$value['Reserindividuale']['id'])
				->setCellValue('C'.$cont,$value['Habitacione']['capacidad'])
				->setCellValue('D'.$cont,$value['Reserindividuale']['desayuno']==1?'Incluye Desayuno':'')
				->setCellValue('E'.$cont,$value['Reserindividuale']['almuerzo']==1?'Incluye Almuerzo':'')
				->setCellValue('F'.$cont,$value['Reserindividuale']['cena']==1?'Incluye Cena':'');
	$cont++;

	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$cont,'')
				->setCellValue('B'.$cont,'')
				->setCellValue('C'.$cont,$value['Cliente']['nombre_completo'])
				->setCellValue('D'.$cont,'___desayuno')
				->setCellValue('E'.$cont,'___almorzo')
				->setCellValue('F'.$cont,'___ceno');

				    foreach($value['Reserindividualeextra'] as $reserindividualeextra){
						$objPHPExcel->setActiveSheetIndex(0)
									->setCellValue('A'.$cont,'')
									->setCellValue('B'.$cont,'')
									->setCellValue('C'.$cont,$reserindividualeextra['nombres'])
									->setCellValue('D'.$cont,'___desayuno')
									->setCellValue('E'.$cont,'___almorzo')
									->setCellValue('F'.$cont,'___ceno');
					}
	$cont++;
	$cantidadPersonas += $value['Reserindividuale']['cantidad_personas'];

	$cantidadHab++;

}


$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('G2', $cantidadHab)
			->setCellValue('H2', $cantidadPersonas);





// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle($name);


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);


// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$name.'"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;



?>
 		
