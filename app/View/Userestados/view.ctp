<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Userestados'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Userestados'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Userestados'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
		<dt><?php echo __('Estado'); ?></dt>
		<dd>
			<?php echo h($userestado['Userestado']['estado']); ?>
			&nbsp;
		</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $userestado['Userestado']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Userestado'), array('action' => 'edit', $userestado['Userestado']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Userestado'), array('action' => 'delete', $userestado['Userestado']['id']), array(), __('Are you sure you want to delete # %s?', $userestado['Userestado']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Userestados'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Userestado'), array('action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>