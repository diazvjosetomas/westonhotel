<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Userestados'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Userestados'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Userestados'); ?> Registrados</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                    <p><?php echo $this->Html->link(__('Crear Nuevo'), array('action' => 'add'), array('class'=>'btn btn-primary')); ?></p>
						<table id="data" class="table table-striped table-bordered" width="100%" cellspacing="0">
						<thead>
						<tr>
							<th><?php echo __('Estado'); ?></th>
							<th class="actions"><?php echo __('Acción'); ?></th>
						</tr>
						</thead>
						<tbody>
						<?php foreach ($userestados as $userestado): ?>
						<tr>
							<td><?php echo h($userestado['Userestado']['estado']); ?>&nbsp;</td>
							<td class="actions">
								<?php echo $this->Html->link('Ver', array('action' => 'view', $userestado['Userestado']['id']), array('class'=>'', 'escapeTitle'=>false)); ?>
								| <?php echo $this->Html->link('Editar', array('action' => 'edit', $userestado['Userestado']['id']), array('class'=>'', 'escapeTitle'=>false)); ?>
								| <?php echo $this->Form->postLink('Eliminar', array('action' => 'delete', $userestado['Userestado']['id']), array('class'=>'', 'confirm' => __('Esta seguro que desea eliminar el registro # %s?', $userestado['Userestado']['id']), 'escapeTitle'=>false)); ?>
							</td>
						</tr>
						<?php endforeach; ?>
						</tbody>
						</table>
					</div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->

<script type="text/javascript">
	//$(document).ready(function() {
	    $('#data').DataTable( {
	    	dom: 'Bfrtlip',
	        buttons: [
	            'copy', 'csv', 'excel', 'pdf', 'print'
	        ],
	        "language": 
	        {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "NingÃºn dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Ãšltimo",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
	    } );
	//} );
</script>

<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Userestado'), array('action' => 'add')); ?></li>
	</ul>
</div>
<?php */ ?>