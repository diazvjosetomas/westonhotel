<script src="https://cdn.ckeditor.com/4.7.0/standard/ckeditor.js"></script>
<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Bodymails'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Bodymails'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Agregar Cuerpo de Mail'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Bodymail', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Bodymaildenominacion">Subject</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('denominacion', array('id'=>'Bodymaildenominacion', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Bodymailcuerpoemail">Cuerpo de Email</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('cuerpoemail', array('id'=>'Bodymailcuerpoemail', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
    <div class="container">

		<h3>Lista de Abreviaturas</h3>

    	<table class="table">
    		<tr>
    			<th>Dato</th>
    			<th>Token</th>
    		</tr>

    		<tr>
    			<td>Nombres</td>
    			<td>_nombres_</td>
    		</tr>
    		
    		<tr>
    			<td>Fecha de Inicio Reserva</td>
    			<td>_fechainicio_</td>
    		</tr>
    		<tr>
    			<td>Fecha de Salida de Reserva</td>
    			<td>_fechasalida_</td>
    		</tr>

            <tr>
                <td>Valor de la reserva</td>
                <td>_valorreserva_</td>
            </tr>

			<tr>
                <td>Identificador de la reserva</td>
                <td>_idreserva_</td>
            </tr>

            <tr>
                <td>Condiciones Generales</td>
                <td>_terminoscondiciones_</td>
            </tr>
            <tr>
                <td>Condiciones Cancelacion</td>
                <td>_condicionescancelacion_</td>
            </tr>


    	</table>
    	
    </div>
            </div><!-- /.box -->
        </div><!-- /.col -->
    








    </div><!-- /.row -->


</section><!-- /.content -->



<script>
            CKEDITOR.replace( 'data[Bodymail][cuerpoemail]' );
        </script>