<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Tipo status individuales'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Tipo status individuales'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Tipo status individuales'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								<dt><?php echo __('Denominación'); ?></dt>
		<dd>
			<?php echo h($reserstatusindividuale['Reserstatusindividuale']['denominacion']); ?>
			&nbsp;
		</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $reserstatusindividuale['Reserstatusindividuale']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Reserstatusindividuale'), array('action' => 'edit', $reserstatusindividuale['Reserstatusindividuale']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Reserstatusindividuale'), array('action' => 'delete', $reserstatusindividuale['Reserstatusindividuale']['id']), array(), __('Are you sure you want to delete # %s?', $reserstatusindividuale['Reserstatusindividuale']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Reserstatusindividuales'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reserstatusindividuale'), array('action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>