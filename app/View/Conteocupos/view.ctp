<section class="content-header">
  <h1>
    Sistema de Encuestas
    <small><?php echo __('Conteocupos'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Conteocupos'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Conteocupos'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($conteocupo['Conteocupo']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha'); ?></dt>
		<dd>
			<?php echo h($conteocupo['Conteocupo']['fecha']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cantcupos'); ?></dt>
		<dd>
			<?php echo h($conteocupo['Conteocupo']['cantcupos']); ?>
			&nbsp;
		</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $conteocupo['Conteocupo']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Conteocupo'), array('action' => 'edit', $conteocupo['Conteocupo']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Conteocupo'), array('action' => 'delete', $conteocupo['Conteocupo']['id']), array(), __('Are you sure you want to delete # %s?', $conteocupo['Conteocupo']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Conteocupos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Conteocupo'), array('action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>