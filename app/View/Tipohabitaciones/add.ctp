<section class="content-header">
  <h1>
    Sistema de Encuestas
    <small><?php echo __('Tipo Habitaciones'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Tipo Habitaciones'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Agregar'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Tipohabitacione', array('class'=>'form-horizontal', 'enctype'=>'multipart/form-data')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" >Referencia</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('ref', array('id'=>'Tipohabitacioneref', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';


	
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Tipohabitacionedenominacion">Denominacion</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('denominacion', array('id'=>'Tipohabitacionedenominacion', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Tipohabitacionecantadultos">Cantidad Adultos (min)</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('cantadultos', array('id'=>'Tipohabitacionecantadultos', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Tipohabitacionecantninos"> Cantidad Adultos (max)</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('cantninos', array('id'=>'Tipohabitacionecantninos', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Tipohabitacionedescripcion"> Descripcion </label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('descripcion', array('id'=>'Tipohabitacionedescripcion', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';


									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Rimagen">Imagen Referencial</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('file', array('id'=>'Rimagen', 'div'=>false, 'label'=>false, 'class'=>'form-control',  'type'=>'file'));		
									echo '</div>';	
									echo '</div>';
										
									// echo'<div class="form-group">';	
									// echo'<label class="control-label col-md-2" for="Tipohabitacionecantbebes">Cantidad Bebes</label>';		
									// echo'<div class="col-md-9">';			
									// echo $this->Form->input('cantbebes', array('id'=>'Tipohabitacionecantbebes', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									// echo '</div>';	
									// echo '</div>';
										?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Html->link(__('List Tipohabitaciones'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('List Consumos'), array('controller' => 'consumos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Consumo'), array('controller' => 'consumos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Habitaciones'), array('controller' => 'habitaciones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Habitacione'), array('controller' => 'habitaciones', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Pstemporadas'), array('controller' => 'pstemporadas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pstemporada'), array('controller' => 'pstemporadas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Reserindividuales'), array('controller' => 'reserindividuales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reserindividuale'), array('controller' => 'reserindividuales', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resermultiples'), array('controller' => 'resermultiples', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resermultiple'), array('controller' => 'resermultiples', 'action' => 'add')); ?> </li>
		</ul>
	</div>
<?php */ ?>