<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Tipohabitaciones'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Tipohabitaciones'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Tipohabitaciones'); ?> Registrados</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                    <p><?php echo $this->Html->link(__('Crear Nuevo'), array('action' => 'add'),  array('class'=>'btn btn-primary')); ?></p>

							<table id="data" class="table table-striped table-bordered" width="100%" cellspacing="0">
							<thead>
							<tr>														
								<th><?php echo __('Referencia'); ?></th>
								<th><?php echo __('Denominacion'); ?></th>
								<th><?php echo __('Cant Min Adultos'); ?></th>
								<th><?php echo __('Cant Max Adultos'); ?></th>
								<th class="actions"><?php echo __('Acción'); ?></th>
							</tr>
							</thead>
							<tbody>
							<?php foreach ($tipohabitaciones as $tipohabitacione): ?>
	<tr>
		
		<td><?php echo h($tipohabitacione['Tipohabitacione']['ref']); ?>&nbsp;</td>
		<td><?php echo h($tipohabitacione['Tipohabitacione']['denominacion']); ?>&nbsp;</td>

		<td><?php echo h($tipohabitacione['Tipohabitacione']['cantadultos']); ?>&nbsp;</td>
		<td><?php echo h($tipohabitacione['Tipohabitacione']['cantninos']); ?>&nbsp;</td>
		
		<td class="actions">
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $tipohabitacione['Tipohabitacione']['id']), array('class'=>'btn btn-primary')); ?>
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $tipohabitacione['Tipohabitacione']['id']),   array('class'=>'btn btn-success')); ?>
			<?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $tipohabitacione['Tipohabitacione']['id']), array('class'=>'btn btn-danger', 'confirm'=>__('Esta seguro que desea eliminar el registro # %s?', $tipohabitacione['Tipohabitacione']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
							</tbody>
							</table>
						</div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->


<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Tipohabitacione'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Consumos'), array('controller' => 'consumos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Consumo'), array('controller' => 'consumos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Habitaciones'), array('controller' => 'habitaciones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Habitacione'), array('controller' => 'habitaciones', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Pstemporadas'), array('controller' => 'pstemporadas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pstemporada'), array('controller' => 'pstemporadas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Reserindividuales'), array('controller' => 'reserindividuales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reserindividuale'), array('controller' => 'reserindividuales', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resermultiples'), array('controller' => 'resermultiples', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resermultiple'), array('controller' => 'resermultiples', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?><script type="text/javascript">
	//$(document).ready(function() {
	    $('#data').DataTable( {
	    	dom: 'Bfrtlip',
	        buttons: [
	            'copy', 'csv', 'excel', 'pdf', 'print'
	        ],
	        "language": 
	        {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
	    } );
	//} );
</script>
