<section class="content-header">
  <h1>
    Sistema de Encuestas
    <small><?php echo __('Tipohabitaciones'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Tipohabitaciones'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Tipohabitaciones'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($tipohabitacione['Tipohabitacione']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Denominacion'); ?></dt>
		<dd>
			<?php echo h($tipohabitacione['Tipohabitacione']['denominacion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($tipohabitacione['Tipohabitacione']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($tipohabitacione['Tipohabitacione']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cantadultos'); ?></dt>
		<dd>
			<?php echo h($tipohabitacione['Tipohabitacione']['cantadultos']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cantninos'); ?></dt>
		<dd>
			<?php echo h($tipohabitacione['Tipohabitacione']['cantninos']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cantbebes'); ?></dt>
		<dd>
			<?php echo h($tipohabitacione['Tipohabitacione']['cantbebes']); ?>
			&nbsp;
		</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $tipohabitacione['Tipohabitacione']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Tipohabitacione'), array('action' => 'edit', $tipohabitacione['Tipohabitacione']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Tipohabitacione'), array('action' => 'delete', $tipohabitacione['Tipohabitacione']['id']), array(), __('Are you sure you want to delete # %s?', $tipohabitacione['Tipohabitacione']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipohabitaciones'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipohabitacione'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Consumos'), array('controller' => 'consumos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Consumo'), array('controller' => 'consumos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Habitaciones'), array('controller' => 'habitaciones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Habitacione'), array('controller' => 'habitaciones', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Pstemporadas'), array('controller' => 'pstemporadas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pstemporada'), array('controller' => 'pstemporadas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Reserindividuales'), array('controller' => 'reserindividuales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reserindividuale'), array('controller' => 'reserindividuales', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resermultiples'), array('controller' => 'resermultiples', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resermultiple'), array('controller' => 'resermultiples', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Consumos'); ?></h3>
	<?php if (!empty($tipohabitacione['Consumo'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Tipohabitacione Id'); ?></th>
		<th><?php echo __('Habitacione Id'); ?></th>
		<th><?php echo __('Reserindividuale Id'); ?></th>
		<th><?php echo __('Ano Consumo'); ?></th>
		<th><?php echo __('Num Consumo'); ?></th>
		<th><?php echo __('Fecha Consumo'); ?></th>
		<th><?php echo __('Observaciones'); ?></th>
		<th><?php echo __('Total'); ?></th>
		<th><?php echo __('Pagado'); ?></th>
		<th><?php echo __('Deuda'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($tipohabitacione['Consumo'] as $consumo): ?>
		<tr>
			<td><?php echo $consumo['id']; ?></td>
			<td><?php echo $consumo['tipohabitacione_id']; ?></td>
			<td><?php echo $consumo['habitacione_id']; ?></td>
			<td><?php echo $consumo['reserindividuale_id']; ?></td>
			<td><?php echo $consumo['ano_consumo']; ?></td>
			<td><?php echo $consumo['num_consumo']; ?></td>
			<td><?php echo $consumo['fecha_consumo']; ?></td>
			<td><?php echo $consumo['observaciones']; ?></td>
			<td><?php echo $consumo['total']; ?></td>
			<td><?php echo $consumo['pagado']; ?></td>
			<td><?php echo $consumo['deuda']; ?></td>
			<td><?php echo $consumo['created']; ?></td>
			<td><?php echo $consumo['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'consumos', 'action' => 'view', $consumo['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'consumos', 'action' => 'edit', $consumo['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'consumos', 'action' => 'delete', $consumo['id']), array(), __('Are you sure you want to delete # %s?', $consumo['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Consumo'), array('controller' => 'consumos', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Habitaciones'); ?></h3>
	<?php if (!empty($tipohabitacione['Habitacione'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Tipohabitacione Id'); ?></th>
		<th><?php echo __('Numhabitacion'); ?></th>
		<th><?php echo __('Habistatu Id'); ?></th>
		<th><?php echo __('Desde'); ?></th>
		<th><?php echo __('Hasta'); ?></th>
		<th><?php echo __('Descripcion'); ?></th>
		<th><?php echo __('Capacidad'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($tipohabitacione['Habitacione'] as $habitacione): ?>
		<tr>
			<td><?php echo $habitacione['id']; ?></td>
			<td><?php echo $habitacione['tipohabitacione_id']; ?></td>
			<td><?php echo $habitacione['numhabitacion']; ?></td>
			<td><?php echo $habitacione['habistatu_id']; ?></td>
			<td><?php echo $habitacione['desde']; ?></td>
			<td><?php echo $habitacione['hasta']; ?></td>
			<td><?php echo $habitacione['descripcion']; ?></td>
			<td><?php echo $habitacione['capacidad']; ?></td>
			<td><?php echo $habitacione['created']; ?></td>
			<td><?php echo $habitacione['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'habitaciones', 'action' => 'view', $habitacione['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'habitaciones', 'action' => 'edit', $habitacione['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'habitaciones', 'action' => 'delete', $habitacione['id']), array(), __('Are you sure you want to delete # %s?', $habitacione['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Habitacione'), array('controller' => 'habitaciones', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Pstemporadas'); ?></h3>
	<?php if (!empty($tipohabitacione['Pstemporada'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Tipotemporada Id'); ?></th>
		<th><?php echo __('Tipohabitacione Id'); ?></th>
		<th><?php echo __('Precio'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($tipohabitacione['Pstemporada'] as $pstemporada): ?>
		<tr>
			<td><?php echo $pstemporada['id']; ?></td>
			<td><?php echo $pstemporada['tipotemporada_id']; ?></td>
			<td><?php echo $pstemporada['tipohabitacione_id']; ?></td>
			<td><?php echo $pstemporada['precio']; ?></td>
			<td><?php echo $pstemporada['created']; ?></td>
			<td><?php echo $pstemporada['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'pstemporadas', 'action' => 'view', $pstemporada['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'pstemporadas', 'action' => 'edit', $pstemporada['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'pstemporadas', 'action' => 'delete', $pstemporada['id']), array(), __('Are you sure you want to delete # %s?', $pstemporada['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Pstemporada'), array('controller' => 'pstemporadas', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Reserindividuales'); ?></h3>
	<?php if (!empty($tipohabitacione['Reserindividuale'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Tipocliente Id'); ?></th>
		<th><?php echo __('Tipoclientesub Id'); ?></th>
		<th><?php echo __('Cliente Id'); ?></th>
		<th><?php echo __('Tipohabitacione Id'); ?></th>
		<th><?php echo __('Habitacione Id'); ?></th>
		<th><?php echo __('Reserstatusindividuale Id'); ?></th>
		<th><?php echo __('Fecha Entrada'); ?></th>
		<th><?php echo __('Fecha Salida'); ?></th>
		<th><?php echo __('Obseraciones'); ?></th>
		<th><?php echo __('Observaciones Cliente'); ?></th>
		<th><?php echo __('Dias'); ?></th>
		<th><?php echo __('Precioxdia'); ?></th>
		<th><?php echo __('Total'); ?></th>
		<th><?php echo __('Pagado'); ?></th>
		<th><?php echo __('Facturado'); ?></th>
		<th><?php echo __('Descuento'); ?></th>
		<th><?php echo __('Resermultiple Id'); ?></th>
		<th><?php echo __('Desayuno'); ?></th>
		<th><?php echo __('Almuerzo'); ?></th>
		<th><?php echo __('Cena'); ?></th>
		<th><?php echo __('Automovil'); ?></th>
		<th><?php echo __('Cantidad Personas'); ?></th>
		<th><?php echo __('Detallereservas'); ?></th>
		<th><?php echo __('Localizador'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($tipohabitacione['Reserindividuale'] as $reserindividuale): ?>
		<tr>
			<td><?php echo $reserindividuale['id']; ?></td>
			<td><?php echo $reserindividuale['tipocliente_id']; ?></td>
			<td><?php echo $reserindividuale['tipoclientesub_id']; ?></td>
			<td><?php echo $reserindividuale['cliente_id']; ?></td>
			<td><?php echo $reserindividuale['tipohabitacione_id']; ?></td>
			<td><?php echo $reserindividuale['habitacione_id']; ?></td>
			<td><?php echo $reserindividuale['reserstatusindividuale_id']; ?></td>
			<td><?php echo $reserindividuale['fecha_entrada']; ?></td>
			<td><?php echo $reserindividuale['fecha_salida']; ?></td>
			<td><?php echo $reserindividuale['obseraciones']; ?></td>
			<td><?php echo $reserindividuale['observaciones_cliente']; ?></td>
			<td><?php echo $reserindividuale['dias']; ?></td>
			<td><?php echo $reserindividuale['precioxdia']; ?></td>
			<td><?php echo $reserindividuale['total']; ?></td>
			<td><?php echo $reserindividuale['pagado']; ?></td>
			<td><?php echo $reserindividuale['facturado']; ?></td>
			<td><?php echo $reserindividuale['descuento']; ?></td>
			<td><?php echo $reserindividuale['resermultiple_id']; ?></td>
			<td><?php echo $reserindividuale['desayuno']; ?></td>
			<td><?php echo $reserindividuale['almuerzo']; ?></td>
			<td><?php echo $reserindividuale['cena']; ?></td>
			<td><?php echo $reserindividuale['automovil']; ?></td>
			<td><?php echo $reserindividuale['cantidad_personas']; ?></td>
			<td><?php echo $reserindividuale['detallereservas']; ?></td>
			<td><?php echo $reserindividuale['localizador']; ?></td>
			<td><?php echo $reserindividuale['created']; ?></td>
			<td><?php echo $reserindividuale['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'reserindividuales', 'action' => 'view', $reserindividuale['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'reserindividuales', 'action' => 'edit', $reserindividuale['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'reserindividuales', 'action' => 'delete', $reserindividuale['id']), array(), __('Are you sure you want to delete # %s?', $reserindividuale['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Reserindividuale'), array('controller' => 'reserindividuales', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Resermultiples'); ?></h3>
	<?php if (!empty($tipohabitacione['Resermultiple'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Tipocliente Id'); ?></th>
		<th><?php echo __('Tipoclientesub Id'); ?></th>
		<th><?php echo __('Cliente Id'); ?></th>
		<th><?php echo __('Tipohabitacione Id'); ?></th>
		<th><?php echo __('Cantidad'); ?></th>
		<th><?php echo __('Cantidad Ocupada'); ?></th>
		<th><?php echo __('Reserstatusmultiple Id'); ?></th>
		<th><?php echo __('Fecha Entrada'); ?></th>
		<th><?php echo __('Fecha Salida'); ?></th>
		<th><?php echo __('Fecha Tope'); ?></th>
		<th><?php echo __('Obseraciones'); ?></th>
		<th><?php echo __('Dias'); ?></th>
		<th><?php echo __('Precioxdia'); ?></th>
		<th><?php echo __('Total'); ?></th>
		<th><?php echo __('Pagado'); ?></th>
		<th><?php echo __('Facturado'); ?></th>
		<th><?php echo __('Descuento'); ?></th>
		<th><?php echo __('Localizador'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($tipohabitacione['Resermultiple'] as $resermultiple): ?>
		<tr>
			<td><?php echo $resermultiple['id']; ?></td>
			<td><?php echo $resermultiple['tipocliente_id']; ?></td>
			<td><?php echo $resermultiple['tipoclientesub_id']; ?></td>
			<td><?php echo $resermultiple['cliente_id']; ?></td>
			<td><?php echo $resermultiple['tipohabitacione_id']; ?></td>
			<td><?php echo $resermultiple['cantidad']; ?></td>
			<td><?php echo $resermultiple['cantidad_ocupada']; ?></td>
			<td><?php echo $resermultiple['reserstatusmultiple_id']; ?></td>
			<td><?php echo $resermultiple['fecha_entrada']; ?></td>
			<td><?php echo $resermultiple['fecha_salida']; ?></td>
			<td><?php echo $resermultiple['fecha_tope']; ?></td>
			<td><?php echo $resermultiple['obseraciones']; ?></td>
			<td><?php echo $resermultiple['dias']; ?></td>
			<td><?php echo $resermultiple['precioxdia']; ?></td>
			<td><?php echo $resermultiple['total']; ?></td>
			<td><?php echo $resermultiple['pagado']; ?></td>
			<td><?php echo $resermultiple['facturado']; ?></td>
			<td><?php echo $resermultiple['descuento']; ?></td>
			<td><?php echo $resermultiple['localizador']; ?></td>
			<td><?php echo $resermultiple['created']; ?></td>
			<td><?php echo $resermultiple['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'resermultiples', 'action' => 'view', $resermultiple['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'resermultiples', 'action' => 'edit', $resermultiple['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'resermultiples', 'action' => 'delete', $resermultiple['id']), array(), __('Are you sure you want to delete # %s?', $resermultiple['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Resermultiple'), array('controller' => 'resermultiples', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<?php */ ?>