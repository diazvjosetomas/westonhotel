<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Tipo de compras'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Tipo de compras'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Tipo de compras'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								
		<dt><?php echo __('Denominación'); ?></dt>
		<dd>
			<?php echo h($tipocompra['Tipocompra']['denominacion']); ?>
			&nbsp;
		</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $tipocompra['Tipocompra']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Tipocompra'), array('action' => 'edit', $tipocompra['Tipocompra']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Tipocompra'), array('action' => 'delete', $tipocompra['Tipocompra']['id']), array(), __('Are you sure you want to delete # %s?', $tipocompra['Tipocompra']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipocompras'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipocompra'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Compras'), array('controller' => 'compras', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Compra'), array('controller' => 'compras', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Compras'); ?></h3>
	<?php if (!empty($tipocompra['Compra'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Proveedore Id'); ?></th>
		<th><?php echo __('Tipocompra Id'); ?></th>
		<th><?php echo __('Ano Compra'); ?></th>
		<th><?php echo __('Num Compra'); ?></th>
		<th><?php echo __('Fecha Compra'); ?></th>
		<th><?php echo __('Num Factura'); ?></th>
		<th><?php echo __('Observaciones'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($tipocompra['Compra'] as $compra): ?>
		<tr>
			<td><?php echo $compra['id']; ?></td>
			<td><?php echo $compra['proveedore_id']; ?></td>
			<td><?php echo $compra['tipocompra_id']; ?></td>
			<td><?php echo $compra['ano_compra']; ?></td>
			<td><?php echo $compra['num_compra']; ?></td>
			<td><?php echo $compra['fecha_compra']; ?></td>
			<td><?php echo $compra['num_factura']; ?></td>
			<td><?php echo $compra['observaciones']; ?></td>
			<td><?php echo $compra['created']; ?></td>
			<td><?php echo $compra['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'compras', 'action' => 'view', $compra['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'compras', 'action' => 'edit', $compra['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'compras', 'action' => 'delete', $compra['id']), array(), __('Are you sure you want to delete # %s?', $compra['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Compra'), array('controller' => 'compras', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<?php */ ?>