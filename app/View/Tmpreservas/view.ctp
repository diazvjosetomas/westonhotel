<section class="content-header">
  <h1>
    Sistema de Encuestas
    <small><?php echo __('Tmpreservas'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Tmpreservas'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Tmpreservas'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($tmpreserva['Tmpreserva']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Identificador'); ?></dt>
		<dd>
			<?php echo h($tmpreserva['Tmpreserva']['identificador']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tipotemporada'); ?></dt>
		<dd>
			<?php echo $this->Html->link($tmpreserva['Tipotemporada']['denominacion'], array('controller' => 'tipotemporadas', 'action' => 'view', $tmpreserva['Tipotemporada']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tipohabitacione'); ?></dt>
		<dd>
			<?php echo $this->Html->link($tmpreserva['Tipohabitacione']['denominacion'], array('controller' => 'tipohabitaciones', 'action' => 'view', $tmpreserva['Tipohabitacione']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cupo'); ?></dt>
		<dd>
			<?php echo $this->Html->link($tmpreserva['Cupo']['fecha'], array('controller' => 'cupos', 'action' => 'view', $tmpreserva['Cupo']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cant Personas'); ?></dt>
		<dd>
			<?php echo h($tmpreserva['Tmpreserva']['cant_personas']); ?>
			&nbsp;
		</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $tmpreserva['Tmpreserva']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Tmpreserva'), array('action' => 'edit', $tmpreserva['Tmpreserva']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Tmpreserva'), array('action' => 'delete', $tmpreserva['Tmpreserva']['id']), array(), __('Are you sure you want to delete # %s?', $tmpreserva['Tmpreserva']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Tmpreservas'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tmpreserva'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipotemporadas'), array('controller' => 'tipotemporadas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipotemporada'), array('controller' => 'tipotemporadas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipohabitaciones'), array('controller' => 'tipohabitaciones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipohabitacione'), array('controller' => 'tipohabitaciones', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cupos'), array('controller' => 'cupos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cupo'), array('controller' => 'cupos', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>