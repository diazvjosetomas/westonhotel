<section class="content-header">
  <h1>
    Sistema de Encuestas
    <small><?php echo __('Tmpreservas'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Tmpreservas'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Edit Tmpreserva'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Tmpreserva', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
			
echo $this->Form->input('id', array('class'=>'form-horizontal'));	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Tmpreservaidentificador">identificador</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('identificador', array('id'=>'Tmpreservaidentificador', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Tmpreservatipotemporada_id">tipotemporada_id</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('tipotemporada_id', array('id'=>'Tmpreservatipotemporada_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Tmpreservatipohabitacione_id">tipohabitacione_id</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('tipohabitacione_id', array('id'=>'Tmpreservatipohabitacione_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Tmpreservacupo_id">cupo_id</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('cupo_id', array('id'=>'Tmpreservacupo_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Tmpreservacant_personas">cant_personas</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('cant_personas', array('id'=>'Tmpreservacant_personas', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Tmpreserva.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Tmpreserva.id'))); ?></li>
				<li><?php echo $this->Html->link(__('List Tmpreservas'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('List Tipotemporadas'), array('controller' => 'tipotemporadas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipotemporada'), array('controller' => 'tipotemporadas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipohabitaciones'), array('controller' => 'tipohabitaciones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipohabitacione'), array('controller' => 'tipohabitaciones', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cupos'), array('controller' => 'cupos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cupo'), array('controller' => 'cupos', 'action' => 'add')); ?> </li>
		</ul>
	</div>
<?php */ ?>