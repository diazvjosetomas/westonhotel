<!--  
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div >
                <?php
					if(isset($empresas[0]["Empresa"]["carpeta_imagen"])){
					       $img = $empresas[0]["Empresa"]["ruta_imagen"];
					}else{

					}
					echo $this->Html->image($img, array('alt' => 'Imagen', 'class'=>'img-responsive', 'style'=>'high:80%; width:100%;'));
				?>

            </div>
        </div>
    </div>
</section> -->

<head>
    <title>Zabuto | Calendar | Action</title>
    <meta name="robots" content="noindex, nofollow">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <!-- jQuery CDN -->    
    <script type="text/javascript" src="<?= $this->Html->templateinclude('jquery.min.js', 'reservas');?>"></script>

    <!-- Bootstrap CDN -->
    
    

    <!-- Zabuto -->
    <link rel="stylesheet" type="text/css" href="<?= $this->Html->templateinclude('examples.css', 'reservas');?>">    
    <link rel="stylesheet" type="text/css" href="<?= $this->Html->templateinclude('style.css', 'reservas');?>">        

    <!-- Zabuto Calendar -->    
    <script type="text/javascript" src="<?= $this->Html->templateinclude('zabuto_calendar.min.js', 'reservas');?>"></script> 
    <link rel="stylesheet" type="text/css" href="<?= $this->Html->templateinclude('zabuto_calendar.min.css', 'reservas');?>">            
    
    <link rel="stylesheet" type="text/css" href="<?= $this->Html->templateinclude('awesome/css/font-awesome.min.css', 'reservas');?>">              

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
<?php echo $this->Html->script('jquery.canvasjs.min.js'); ?>
</head>




<div class="form-group">
    <label class="control-label col-md-2">
        Estatus
    </label>
    <div class="col-md-9">

        

        <form action="/Dashboard/index" method="post">
            
        <select  name="data[Ocupacion][estatus]" class="form-control">
            <option > --SELECCIONA--</option>
            <option value="1"> Iniciada </option>
            <option value="2"> Confirmada </option>
            <option value="3"> Auditada </option>
            <option value="4"> Ingresada </option>
            <option value="5"> Egresada </option>
            <option value="6"> No Show </option>
            <option value="7"> Todas </option>
        </select>
        <br>
        <br>
        <input class="btn btn-success" type="submit" value=" Ver Status" >
        </form>
    </div>
</div>


<br>
<br>
<div id="container2"></div>


<script type="text/javascript">
      <?php 

        
        if ($mes == 1) {
            $nommes = 'Enero';
        }
        if ($mes == 2) {
            $nommes = 'Febrero';
        }
        if ($mes == 3) {
            $nommes = 'Marzo';
        }
        if ($mes == 4) {
            $nommes = 'Abril';
        }
        if ($mes == 5) {
            $nommes = 'Mayo';
        }
        if ($mes == 6) {
            $nommes = 'Junio';
        }
        if ($mes == 7) {
            $nommes = 'Julio';
        }
        if ($mes == 8) {
            $nommes = 'Agosto';
        }
        if ($mes == 9) {
            $nommes = 'Septiembre';
        }
        if ($mes == 10) {
            $nommes = 'Octubre';
        }
        if ($mes == 11) {
            $nommes = 'Noviembre';
        }
        if ($mes == 12) {
            $nommes = 'Diciembre';
        }

       ?>





 




          var chart = Highcharts.chart('container2', {

            title: {
                text: 'Ocupacion del Mes <?=$nommes?>'
            },

            subtitle: {
                text: 'Relacion Mes/Ocupacion'
            },

            xAxis: {
                categories: <?=$cate?>
            },

            series: [{
                type: 'column',
                colorByPoint: true,
                data: <?=$data?>,
                showInLegend: false
            }]

        });



  
</script>


