<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Personales'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Personales'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Personales'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
							<dt><?php echo __('N° identidad'); ?></dt>
							<dd>
								<?php echo h($personale['Personale']['nidentidad']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Nombres'); ?></dt>
							<dd>
								<?php echo h($personale['Personale']['nombres']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Apellidos'); ?></dt>
							<dd>
								<?php echo h($personale['Personale']['apellidos']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Teléfono'); ?></dt>
							<dd>
								<?php echo h($personale['Personale']['telefono']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Email'); ?></dt>
							<dd>
								<?php echo h($personale['Personale']['email']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Direccion'); ?></dt>
							<dd>
								<?php echo h($personale['Personale']['direccion']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Fecha Ingreso'); ?></dt>
							<dd>
								<?php echo h( formatdmy($personale['Personale']['fecha_ingreso'])); ?>
								&nbsp;
							</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $personale['Personale']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Personale'), array('action' => 'edit', $personale['Personale']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Personale'), array('action' => 'delete', $personale['Personale']['id']), array(), __('Are you sure you want to delete # %s?', $personale['Personale']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Personales'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Personale'), array('action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>