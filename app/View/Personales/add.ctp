<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Personales'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Personales'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Add Personale'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Personale', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Personalenidentidad">N° identidad</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('nidentidad', array('id'=>'Personalenidentidad', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Personalenombres">Nombres</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('nombres', array('id'=>'Personalenombres', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Personaleapellidos">Apellidos</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('apellidos', array('id'=>'Personaleapellidos', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Personaletelefono">Teléfono</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('telefono', array('id'=>'Personaletelefono', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Personaleemail">Email</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('email', array('id'=>'Personaleemail', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Personaledireccion">Dirección</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('direccion', array('id'=>'Personaledireccion', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Personalefecha_ingreso">Fecha ingreso</label>';		
									echo'<div class="col-md-4">';			
									echo $this->Form->input('fecha_ingreso1', array('id'=>'Personalefecha_ingreso', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly' => true ));		
									echo '</div>';	
									echo '</div>';
								?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Html->link(__('List Personales'), array('action' => 'index')); ?></li>
			</ul>
	</div>
<?php */ ?>

<script type="text/javascript">
	$(function(){
		$('#Personalefecha_ingreso').datepicker();
	});
</script>