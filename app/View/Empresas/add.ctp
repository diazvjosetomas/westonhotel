<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Empresas'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Empresas'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Add Empresa'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Empresa', array('class'=>'form-horizontal','enctype'=>'multipart/form-data')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
	
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Empresacuit">Cuit</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('cuit', array('id'=>'Empresacuit', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Empresarazon_social">Razon social</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('razon_social', array('id'=>'Empresarazon_social', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Empresaemail">Email</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('email', array('id'=>'Empresaemail', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Empresatelefono">Teléfono</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('telefono', array('id'=>'Empresatelefono', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Empresawebsite">Website</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('website', array('id'=>'Empresawebsite', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Empresaprovincia">Provincia</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('provincia', array('id'=>'Empresaprovincia', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Empresadireccion">Direccion</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('direccion', array('id'=>'Empresadireccion', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="logo">Logo</label>';		
									echo'<div class="col-md-10">';	
									echo $this->Form->input('imagen', array('id'=>'logo','label'=>false, 'type'=>'file'));
									echo'<p for="" class="help-block text-red">* El logo debe estar en formato .png</p>';
									echo '</div>';
									echo '</div>';


									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Empresacorreo_smtp">Correo smtp</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('correo_smtp', array('id'=>'Empresacorreo_smtp', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Empresaclave_smtp">Clave smtp</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('clave_smtp', array('id'=>'Empresaclave_smtp', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';


								?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Html->link(__('List Empresas'), array('action' => 'index')); ?></li>
			</ul>
	</div>
<?php */ ?>