<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Tipo de sub-clientes'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Tipo de sub-clientes'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Tipo de sub-clientes'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								
		<dt><?php echo __('Tipo de cliente'); ?></dt>
		<dd>
			<?php echo h($tipoclientesub['Tipocliente']['denominacion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nombre'); ?></dt>
		<dd>
			<?php echo h($tipoclientesub['Tipoclientesub']['nombre']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cuit'); ?></dt>
		<dd>
			<?php echo h($tipoclientesub['Tipoclientesub']['cuit']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Contacto Ref'); ?></dt>
		<dd>
			<?php echo h($tipoclientesub['Tipoclientesub']['contacto_ref']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Correo'); ?></dt>
		<dd>
			<?php echo h($tipoclientesub['Tipoclientesub']['correo']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Telf Fijo'); ?></dt>
		<dd>
			<?php echo h($tipoclientesub['Tipoclientesub']['telf_fijo']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Telf Celular'); ?></dt>
		<dd>
			<?php echo h($tipoclientesub['Tipoclientesub']['telf_celular']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dirección'); ?></dt>
		<dd>
			<?php echo h($tipoclientesub['Tipoclientesub']['direccion']); ?>
			&nbsp;
		</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $tipoclientesub['Tipoclientesub']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Tipoclientesub'), array('action' => 'edit', $tipoclientesub['Tipoclientesub']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Tipoclientesub'), array('action' => 'delete', $tipoclientesub['Tipoclientesub']['id']), array(), __('Are you sure you want to delete # %s?', $tipoclientesub['Tipoclientesub']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipoclientesubs'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipoclientesub'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipoclientes'), array('controller' => 'tipoclientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipocliente'), array('controller' => 'tipoclientes', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>