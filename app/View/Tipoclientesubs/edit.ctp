<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Tipo de sub-clientes'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Tipo de sub-clientes'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Edit Tipo de sub-clientes'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Tipoclientesub', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
			
									echo $this->Form->input('id', array('class'=>'form-horizontal'));	
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Tipoclientesubtipocliente_id">Tipo de Cliente</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('tipocliente_id', array('id'=>'Tipoclientesubtipocliente_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Tipoclientesubnombre">Nombre</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('nombre', array('id'=>'Tipoclientesubnombre', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Tipoclientesubcuit">Cuit</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('cuit', array('id'=>'Tipoclientesubcuit', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Tipoclientesubcontacto_ref">Contacto ref</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('contacto_ref', array('id'=>'Tipoclientesubcontacto_ref', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Tipoclientesubcorreo">Correo</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('correo', array('id'=>'Tipoclientesubcorreo', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Tipoclientesubtelf_fijo">Telf fijo</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('telf_fijo', array('id'=>'Tipoclientesubtelf_fijo', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Tipoclientesubtelf_celular">Telf celular</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('telf_celular', array('id'=>'Tipoclientesubtelf_celular', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Tipoclientesubdireccion">Dirección</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('direccion', array('id'=>'Tipoclientesubdireccion', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Tipoclientesub.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Tipoclientesub.id'))); ?></li>
				<li><?php echo $this->Html->link(__('List Tipoclientesubs'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('List Tipoclientes'), array('controller' => 'tipoclientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipocliente'), array('controller' => 'tipoclientes', 'action' => 'add')); ?> </li>
		</ul>
	</div>
<?php */ ?>