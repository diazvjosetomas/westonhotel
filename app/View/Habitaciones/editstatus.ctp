<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('habitación///'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('habitación'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Edit habitación'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Habitacione', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
			
							echo $this->Form->input('id', array('class'=>'form-horizontal'));	
							echo'<div class="form-group">';	
							echo'<label class="control-label col-md-2" for="Habitacionetipohabitacione_id">Tipo habitación</label>';		
							echo'<div class="col-md-9">';			
							echo $this->Form->input('tipohabitacione_id', array('id'=>'Habitacionetipohabitacione_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
							echo '</div>';	
							echo '</div>';
								
							echo'<div class="form-group">';	
							echo'<label class="control-label col-md-2" for="Habitacionenumhabitacion">Num habitación</label>';		
							echo'<div class="col-md-9">';			
							echo $this->Form->input('numhabitacion', array('readonly'=>true, 'id'=>'Habitacionenumhabitacion', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
							echo '</div>';	
							echo '</div>';
								
							echo'<div class="form-group">';	
							echo'<label class="control-label col-md-2" for="Habitacionehabistatu_id">Status</label>';		
							echo'<div class="col-md-9">';			
							echo $this->Form->input('habistatu_id', array('id'=>'Habitacionehabistatu_id', 'div'=>false, 'label'=>false, 'class'=>'form-control','onchange'=>'rangofecha()'));		
							echo '</div>';	
							echo '</div>';


							
							
								?>


							<div  >

							<div class="form-group">
								<label  for="desde" class="control-label col-md-2">
									Bloqueada desde
								</label>
								<div class="col-md-9">
									<input type="text" name="data[Habitacione][desde]" id="desde" class="form-control" value="<?=$datafecha[0]['Habitacione']['desde']?>">
								</div>
								
							</div>


							<div class="form-group">
								<label  for="desde" class="control-label col-md-2">
									Bloqueada hasta
								</label>
								<div class="col-md-9">
									<input type="text" name="data[Habitacione][hasta]" id="hasta" class="form-control" value="<?=$datafecha[0]['Habitacione']['hasta']?>">
								</div>
								
							</div>

							<?php 

							/*echo $datafecha[0]['Habitacione']['desde'];

							echo'<div class="form-group">';	
							echo'<label class="control-label col-md-2" for="desde">Bloqueada Desde: </label>';		
							echo'<div class="col-md-9">';			
							echo $this->Form->input('desde', array('id'=>'desde', 
																   'div'=>false, 
																   'label'=>false, 
																   'class'=>'form-control', 
																   'value'=>'as'));		
							echo '</div>';	
							echo '</div>';



							echo'<div class="form-group">';	
							echo'<label class="control-label col-md-2" for="hasta">Bloqueada Hasta</label>';		
							echo'<div class="col-md-9">';			
							echo $this->Form->input('hasta', array('id'=>'hasta', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'value'=>$datafecha[0]['Habitacione']['hasta']));		
							echo '</div>';	
							echo '</div>';
*/






							 ?>
								
							</div>	
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'status')); ?>	                                    
	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<script>
	
	function rangofecha(){

		var idestatus = $("#Habitacionehabistatu_id").val();

		console.log(idestatus);

		if (idestatus == 8) {
			$("#rangofecha").show();
		}else{
			$("#rangofecha").hide();	
		}

	}
	//rangofecha();

	$("#desde").datepicker();
	$("#hasta").datepicker();
</script>