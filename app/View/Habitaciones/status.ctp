<?

	foreach ($habistatus as $key) {

		$estatus[$key['Habistatu']['id']] = $key['Habistatu']['denominacion'];
		
	}

	

?>



<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('habitación'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('habitación'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Habitaciones'); ?> Registrados</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                    <p><?php echo $this->Html->link(__('Crear Nuevo'), array('action' => 'add'),  array('class'=>'btn btn-primary')); ?></p>

							<table id="data" class="table table-striped table-bordered" width="100%" cellspacing="0">
							<thead>
							<tr>
								<th><?php echo __('Tipo habitación'); ?></th>
								<th><?php echo __('Num habitación'); ?></th>
								<th><?php echo __('Status'); ?></th>
								<th> Nombre Pasajero   </th>
								<th> Numero de Reserva </th>
								<th class="actions"><?php echo __('Acción'); ?></th>
							</tr>
							</thead>
							<tbody>
							<?php foreach ($habitaciones as $habitacione): ?>
	<tr>
		<td><?php echo h($habitacione['Tipohabitacione']['denominacion']); ?></td>
		<td><?php echo h($habitacione['Habitacione']['numhabitacion']); ?>&nbsp;</td>
		<td><?php

		$msg = 0;


		foreach ($allhabitaciones as $key) {
			if ($habitacione['Habitacione']['id'] == $key['Habitacione']['id']) {

			
					
						echo $key['Reserstatusindividuale']['denominacion'];
						$msg++;	
				
				
			}

		}

		if ($msg == 0) {

			if (isset($estatus[$habitacione['Habitacione']['habistatu_id']])) {
				
				echo $estatus[$habitacione['Habitacione']['habistatu_id']];
				
			}else{
				echo "INDEFINIDO";
			}
		}

		  ?>



		&nbsp;</td>

		<td>
			<?php 

				foreach ($allhabitaciones as $key) {
					if ($habitacione['Habitacione']['id'] == $key['Habitacione']['id']) {

					
							
								echo $key['Cliente']['nombre_completo'];
								$msg++;	
						
						
					}

				}
			 ?>
		</td>

		<td>
			<?php 

				foreach ($allhabitaciones as $key) {
					if ($habitacione['Habitacione']['id'] == $key['Habitacione']['id']) {

					
							
								echo $key['Reserindividuale']['id'];
								$msg++;	
						
						
					}

				}
			 ?>

		</td>


		<td class="actions">
			<?php echo $this->Html->link(__('Editar'), array('action' => 'editstatus', $habitacione['Habitacione']['id']), array('class'=>'')); ?>
		</td>
	</tr>
<?php endforeach; ?>
							</tbody>
							</table>
						</div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->

<?php pr($habitaciones) ?>

<script type="text/javascript">



	//$(document).ready(function() {
	    $('#data').DataTable( {
	    	dom: 'Bfrtlip',
	        buttons: [
	            'copy', 'csv', 'excel', 'pdf', 'print'
	        ],
	        "language": 
	        {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
	    } );
	//} );
</script>
