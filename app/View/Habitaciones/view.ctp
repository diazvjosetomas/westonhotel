<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('habitación'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('habitación'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('habitación'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								
		<dt><?php echo __('Tipo habitación'); ?></dt>
		<dd>
			<?php echo h($habitacione['Tipohabitacione']['denominacion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Num habitacion'); ?></dt>
		<dd>
			<?php echo h($habitacione['Habitacione']['numhabitacion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Descripción'); ?></dt>
		<dd>
			<?php echo h($habitacione['Habitacione']['descripcion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Capacidad'); ?></dt>
		<dd>
			<?php echo h($habitacione['Habitacione']['capacidad']); ?>
			&nbsp;
		</dd>

						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $habitacione['Habitacione']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Habitacione'), array('action' => 'edit', $habitacione['Habitacione']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Habitacione'), array('action' => 'delete', $habitacione['Habitacione']['id']), array(), __('Are you sure you want to delete # %s?', $habitacione['Habitacione']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Habitaciones'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Habitacione'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipohabitaciones'), array('controller' => 'tipohabitaciones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipohabitacione'), array('controller' => 'tipohabitaciones', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>