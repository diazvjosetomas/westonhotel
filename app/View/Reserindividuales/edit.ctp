<style>
	.tipo_habitacion{
		border-radius: 10px 10px 10px 10px;
		-moz-border-radius: 10px 10px 10px 10px;
		-webkit-border-radius: 10px 10px 10px 10px;
		border: 1px solid #000000;

		padding: 5px;
		padding-top: 15px;
	}
</style>

<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Reservación Individual'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Reservación Individual'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Agregar Reservación Individual'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Reserindividuale', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
								echo $this->Form->input('id', array('class'=>'form-horizontal'));



								   echo'<div class="form-group">';	
								   echo'<label class="control-label col-md-2" for="Reserindividualecliente_id">Cliente/ Añadir <img title="Agregar Cliente" style="icon:pointer;" data-toggle="modal" data-target="#myModal" src="'.$this->webroot.'img/adduser.png'.'"> </label>';		


								   echo'<div class="col-md-9" id="div-cliente">';			
									echo $this->Form->input('cliente_id', array('id'=>'Reserindividualecliente_id', 'div'=>false, 'label'=>false, 'class'=>'form-control selectpicker',"data-live-search"=>"true", "onChange"=>"selectTagRemoteCliente('".$this->Html->url('/Clientes/preferencias')."','".$this->Html->url('/Clientes/descuento')."', 'preferencias','descuento', this.value, this.value);", 'empty'=>'--Seleccione--')); 
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualefecha_entrada">Fecha entrada</label>';		
									echo'<div class="col-md-4" id="div-fechaentrada">';			
									echo $this->Form->input('fecha_entrada1', array('id'=>'Reserindividualefecha_entrada', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly' => true));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualefecha_salida">Fecha salida</label>';		
									echo'<div class="col-md-4">';			
									echo $this->Form->input('fecha_salida1', array('id'=>'Reserindividualefecha_salida', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly' => true, "onChange"=>'javascript:tipo_habitacion();'));		
									echo '</div>';	
									echo '</div>';

									//,"onChange"=>'javascript:calcular_individual_habitacion();


									//...............
									echo "<h3> <b> Agregar Tipos de Habitaciones </b> </h3>";



									echo "<div class='tipo_habitacion' id='id_tipo_habitacion'>";

											echo'<div class="form-group">';	
												echo'<label class="control-label col-md-2" for="Reserindividualetipohabitacione_id">
													<h4>Tipo Habitación </h4> </label>';		
												echo'<div class="col-md-4">';			
												echo $this->Form->input('tipohabitacione_id', array('id'=>'Reserindividualetipohabitacione_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', "onChange"=>'javascript:tipo_habitacion();', 'empty'=>'--Seleccione--')); 
												echo '</div>';	



												echo'<div class="col-md-2">';			
												echo $this->Form->input('cantidad_personas', array('id'=>'Reserindividualecantidad_personas', 'div'=>false, 'label'=>false, 'class'=>'form-control','placeholder'=>'Cantidad de Personas'));
												echo '</div>';



												echo "<div class='col-md-2'>";
												echo "<h4> <a  onclick='cantpersonas()'>  <i class='fa fa-plus-square' ></i> Agregar</a> </h4>";
												echo "</div>";


											echo '</div>';
										echo "<hr>";



									echo "</div>";

									echo "<hr>";
									echo "<div id='eliminar'>";
									echo $this->Form->input('tipohabitaciones', array('id'=>'Reserindividualetipohabitaciones', 'div'=>false, 'label'=>false, 'class'=>'form-control','type'=>'hidden'));
									echo "</div>";

									
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualeobseraciones">Observaciones</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('obseraciones', array('id'=>'Reserindividualeobseraciones', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualeobseraciones">Preferencias</label>';		
									echo'<div class="col-md-9" id="preferencias">';			
									echo $this->Form->input('obseraciones_cliente', array('id'=>'Reserindividualeobseracionescliente', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'name'=>'data[Reserindividuale][observaciones_cliente]'));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualelocalizador">Localizador</label>';		
									echo'<div class="col-md-9" id="div-descuento">';			
									echo $this->Form->input('localizador', array('id'=>'Reserindividualelocalizador', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>false ));		
									echo '</div>';	
									echo '</div>';



									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualeautomovil">Patente</label>';		
									echo'<div class="col-md-9" id="div-total">';			
									echo $this->Form->input('automovil', array('type'=>'text', 'id'=>'Reserindividualeautomovil', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
									
									echo'<div class="form-group" >';	
									echo'<label class="control-label col-md-2" for="Reserindividualedias">Cant Noches</label>';		
									echo'<div class="col-md-9" id="div-dia">';			
									echo $this->Form->input('dias', array( 'id'=>'Reserindividualedias', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly'=>false, "onChange"=>'cambiarfecha();','type'=>'number'));		
									echo '</div>';	
									echo '</div>';
										

									echo'<div class="form-group" >';	
									echo'<label class="control-label col-md-2" for="Reserindividualedescuento">Descuento (%)</label>';		
									echo'<div class="col-md-9" id="descuento">';			
									echo $this->Form->input('descuento', array('id'=>'Reserindividualedescuento', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>false, "onChange"=>'recalcular_individual();', 'disabled'=>'true'));		
									echo '</div>';	
									echo '</div>';



									echo'<div class="form-group" >';	
									echo'<label class="control-label col-md-2" for="Reserindividualeprecioxdia">Precio x noche</label>';		
									echo'<div class="col-md-9" id="div-precioxdia">';			
									echo $this->Form->input('precioxdia', array('id'=>'Reserindividualeprecioxdia', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>false, "onChange"=>'recalcular_individual();'));		
									echo '</div>';	
									echo '</div>';


									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualetotal">Total</label>';		
									echo'<div class="col-md-9" id="div-total">';			
									echo $this->Form->input('total', array('id'=>'Reserindividualetotal', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text'));		


									echo '</div>';	
									echo '</div>';


									echo $this->Form->input('detallereservas', array('id'=>'detallereservastable', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'hidden', 'readonly'=>true));	
								?>
								
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    
	                                    <input onclick="remover()" value="Guardar" id="Reserindividualesubmit" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog" style="z-index: 3">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agregar Cliente</h4>
      </div>
      <div class="modal-body">

      		                <div class="box-body">
      							<?php echo $this->Form->create('Cliente', array('class'=>'form-horizontal','url'=>'/Clientes/add')); ?>
      							<div class='row'>
      									<div class='col-md-12'>
      										<?php
      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientetipocliente_id">Tipo cliente</label>';		
      											echo'<div class="col-md-9">';			
      											echo $this->Form->input('tipocliente_id', array('id'=>'Clientetipocliente_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', "onChange"=>"selectTagRemote('".$this->Html->url('/Clientes/sub_cliente')."', 'div-subcliente2', this.value);", 'empty'=>'--Seleccione--')); 
      											echo '</div>';	
      											echo '</div>';
      												
      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientetipoclientesub_id">Tipo subcliente</label>';		
      											echo'<div class="col-md-9" id="div-subcliente2">';			
      											echo $this->Form->input('tipoclientesub_id', array('id'=>'Clientetipoclientesub_id', 'div'=>false, 'label'=>false, 'class'=>'form-control','options'=>array()));		
      											echo '</div>';	
      											echo '</div>';
      												
      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientedni">Dni</label>';		
      											echo'<div class="col-md-9">';			
      											echo $this->Form->input('dni', array('id'=>'Clientedni', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
      											echo '</div>';	
      											echo '</div>';
      												
      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientenombre_completo">Nombre completo</label>';		
      											echo'<div class="col-md-9">';			
      											echo $this->Form->input('nombre_completo', array('id'=>'Clientenombre_completo', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
      											echo '</div>';	
      											echo '</div>';

      											echo'<div class="form-group">';	
												echo'<label class="control-label col-md-2" for="Clientefecha_nacimiento">Fecha nacimiento</label>';		
												echo'<div class="col-md-9">';			
												echo $this->Form->input('fecha_nacimiento', array('id'=>'Clientefecha_nacimiento', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
												echo '</div>';	
												echo '</div>';

												echo'<div class="form-group">';	
												echo'<label class="control-label col-md-2" for="Clientepai_id">País de Origen</label>';		
												echo'<div class="col-md-9">';			
												echo $this->Form->input('pai_id', array('empty'=>'--Seleccione--', 'id'=>'Clientepai_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
												echo '</div>';	
												echo '</div>';

												echo'<div class="form-group">';	
												echo'<label class="control-label col-md-2" for="Clienteciudad">Ciudad</label>';		
												echo'<div class="col-md-9">';			
												echo $this->Form->input('ciudad', array('id'=>'Clienteciudad', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
												echo '</div>';	
												echo '</div>';

      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientedireccion">Dirección</label>';		
      											echo'<div class="col-md-9">';			
      											echo $this->Form->input('direccion', array('id'=>'Clientedireccion', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
      											echo '</div>';	
      											echo '</div>';
      												
      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientetelf">Teléfono</label>';		
      											echo'<div class="col-md-9">';			
      											echo $this->Form->input('telf', array('id'=>'Clientetelf', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
      											echo '</div>';	
      											echo '</div>';

      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientetelf">Movil</label>';		
      											echo'<div class="col-md-9">';			
      											echo $this->Form->input('movil', array('id'=>'Clientemovil', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
      											echo '</div>';	
      											echo '</div>';



      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientecorreo">Email</label>';		
      											echo'<div class="col-md-9">';			
      											echo $this->Form->input('correo', array('id'=>'Clientecorreo', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
      											echo '</div>';	
      											echo '</div>';
      												
      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientecod_pos">Cod pos</label>';		
      											echo'<div class="col-md-9">';			
      											echo $this->Form->input('cod_pos', array('id'=>'Clientecod_pos', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
      											echo '</div>';	
      											echo '</div>';



      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Preferencias">Preferencias</label>';		
      											echo'<div class="col-md-9">';			
      											echo $this->Form->input('preferencias', array('id'=>'Clientepreferencias', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
      											echo '</div>';	
      											echo '</div>';
      										?>
      									</div>
      									<div class="col-md-12">
      										<div class="form-group">
      			                                <div class="col-md-12">
      			                                <a href="#" onclick="guardar()" class="btn btn-primary pull-right">Guardar</a>    
      			                                <!--<input  value="Guardar" class="btn btn-primary pull-right" type="submit">-->
      			                                </div>
      			                            </div>
      		                            </div>
      							</div></form>                </div>





      </div>
      <div class="modal-footer">
        
      </div>
    </div>

  </div>
</div>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>





<input type="hidden" value='1' id='control-th'>
<input type="hidden" id='lastth'>


<input type="hidden" id="ctrladd" value="0">
								

<script type="text/javascript">


	function preferenciascliente(){
		var idcliente = $("#Reserindividualecliente_id").val();

		$.ajax({
			url:"/Clientes/descuento/"+idcliente,
			type:"post",
			data:{},
			success:function(response){
				$("#descuento").html(response);
			}
		});

		$.ajax({
			url:"/Clientes/preferencias/"+idcliente,
			type:"post",
			data:{},
			success:function(response){
				$("#preferencias").html(response);
			}
		});

	}

	function montartipohabitaciones(){
		//Eliminar el primero y el ultimo corchete de la variable
		var tipohabitaciones = $("#Reserindividualetipohabitaciones").val()
		var long = tipohabitaciones.length;
		long = long - 2;
		tipohabitaciones = tipohabitaciones.substr(1, long);
		//console.log(tipohabitaciones)

		//hacemos split por la coma
		var data1 = tipohabitaciones.split(",");
		
		for (var i = 0; i <= data1.length - 1; i++) {
			var variable = data1[i];
			var long = data1[i].length;
			long = long - 2;
			var datadetalle = data1[i].substr(1, long)
			//console.log(datadetalle)

			// hacemos split por |
			var data2 = datadetalle.split("|");

			var cantidadpersonas = data2[1]

			//+++++++++++++++++++++++++++++++++++++++++++++++++++++ 

				console.log(data2[1])

				$.ajax({
				  url:"/Tipohabitaciones/consultaespecial/"+data2[0]+"/"+data2[1],
				  type:"post",
				  data:{},
				  success:function(response){
				    
				   
				    var data = JSON.parse(response);
				    
				    if (data.resp == true) {
					




				      nombreth = data.datath[0].Tipohabitacione.denominacion;
				      //console.log(nombreth)
				      $("#lastth").val(nombreth)

				       var cth = $("#control-th").val();
				       var varlastth = $("#lastth").val();
				      

				       var addhtml = '';
				       addhtml += "<div id='div_addhtml"+cth+"' class='row'>";
				      
				       addhtml += "<label class='control-label col-md-4'>Tipo Habitacion</label>"
				      
				       addhtml += "<div class='col-md-2'>"
				       addhtml += varlastth+'<input name="data[Reserindividuale][tipohabitaciones][]" type="hidden" id="input_'+cth+'" value="'+data2[0]+'|'+data.cantpersonas+'">'
				       addhtml += "</div>"
				      
				       addhtml += "<div class='col-md-2'>"
				       addhtml += "<span id='spantptemp"+data.cantpersonas+"'></span>"
				       
				       addhtml += "</div>"
				       
				       addhtml += "<div class='col-md-2'>"
				       addhtml += "<a onclick='remove("+cth+")' ><i class='fa fa-trash-o'></i></a>"
				       addhtml += "</div>"
				      
				       addhtml += "</div>"

				      $("#id_tipo_habitacion").append(addhtml)
				      cth++;
				      $("#control-th").val(cth);

				      
				    }

				    denominaciontiptemporada( data.cantpersonas )

				  }
				});



				 


			}


		

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++


	}


	preferenciascliente();	
	//cambiarfecha();
	montartipohabitaciones();


	function remover(){
		$("#eliminar").remove();
	}









	function guardar(){

		$.post('/Clientes/add',$("#ClienteAddForm").serialize(),function(response){
			cargacliente();
		});
	}


	function cargacliente(){
		$("#myModal").modal('hide');
		
		$.ajax({
		    url:"/Reserindividuales/cliente2",
		    type:"post",
		    data:{},
		    success:function(response){
		    	console.log(response);

		    	$("#div-cliente").html(response);

					      

		      
		    }
		});
	}


$("#Reserindividualefecha_entrada").datepicker();
$("#Reserindividualefecha_salida").datepicker();
$("#Clientefecha_nacimiento").datepicker();


$(document).ready(function() {
    $("#newNombresBtn").click(function (e) {
        var MaxInputs       = 8;
        var x = $("#nombres-container #emailcount").length + 1;
        var FieldCount = x-1;
        if(x <= MaxInputs) //max input box allowed
        {
            FieldCount++;
            $("#nombres-container").append('<div id="nombrecount"><div class="form-group nombre-con" id="nombre_'+ FieldCount +'"><label class="control-label col-md-3">Nombre Completo</label><div class="col-md-8"><div class="input-group col-md-12"><input class="form-control text-box single-line" id="Nombres_'+ FieldCount +'__Nombre" name="data[Reserindividuale][Nombre][]" placeholder="Ej: Juan Antonio Pacheco Lugo" require="require" type="text" value="" /></div></div><div class="col-md-1"><button class="btn btn-danger btn-sm" onclick="if(confirm(\'Seguro que desea eliminar el registro\')){ $(\'#nombre_'+ FieldCount +'\').remove(); $(\'#Nombres_'+ FieldCount +'__Nombre\').val(\'\'); $(\'#Nombres_'+ FieldCount +'__Nombre\').removeAttr(\'required\'); } return false;"><span class="glyphicon glyphicon-trash"></span></button></div></div></div>');
            x++; //text box increment
        }
        return false;
    });
});
</script>

<? //pr($allclientes) ?>