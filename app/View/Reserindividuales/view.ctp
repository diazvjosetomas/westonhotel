<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Reservación Individial'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Reservación Individial'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Reservación Individual'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                      
                        <div class="col-xs-9">
						<dl class="dl-horizontal">
							<dt><?php echo __('Tipo de cliente'); ?></dt>
							<dd>
								<?php echo h($reserindividuale['Tipocliente']['denominacion']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Tipo de sub cliente'); ?></dt>
							<dd>
								<?php echo h($reserindividuale['Tipoclientesub']['nombre']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Cliente'); ?></dt>
							<dd>
								<?php echo h($reserindividuale['Cliente']['nombre_completo']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Tipo de habitación'); ?></dt>
							<dd id="datahtml">
								
							</dd>
							<dt><?php echo __('Habitación'); ?></dt>
							<dd>
								<?php echo h($reserindividuale['Habitacione']['numhabitacion']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Status'); ?></dt>
							<dd>
								<?php echo h($reserindividuale['Reserstatusindividuale']['denominacion']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Fecha Entrada'); ?></dt>
							<dd>
								<?php echo h( formatdmy($reserindividuale['Reserindividuale']['fecha_entrada']) ); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Fecha Salida'); ?></dt>
							<dd>
								<?php echo h( formatdmy($reserindividuale['Reserindividuale']['fecha_salida']) ); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Observaciones'); ?></dt>
							<dd>
								<?php echo h($reserindividuale['Reserindividuale']['obseraciones']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Preferencias'); ?></dt>
							<dd>
								<?php echo h($reserindividuale['Reserindividuale']['observaciones_cliente']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Localizador'); ?></dt>
							<dd>
								<?php echo h($reserindividuale['Reserindividuale']['localizador']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Dias'); ?></dt>
							<dd>
								<?php echo h($reserindividuale['Reserindividuale']['dias']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Precio x dia'); ?></dt>
							<dd>
								<?php echo h($reserindividuale['Reserindividuale']['precioxdia']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Incluye desayuno'); ?></dt>
							<dd>
								<?php echo ($reserindividuale['Reserindividuale']['desayuno'] == 1) ? 'SI' : 'NO'; ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Incluye almuerzo'); ?></dt>
							<dd>
								<?php echo ($reserindividuale['Reserindividuale']['almuerzo'] == 1) ? 'SI' : 'NO'; ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Incluye cena'); ?></dt>
							<dd>
								<?php echo ($reserindividuale['Reserindividuale']['cena'] == 1) ? 'SI' : 'NO'; ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Automovil'); ?></dt>
							<dd>
								<?php echo h($reserindividuale['Reserindividuale']['automovil']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Descuento al total'); ?></dt>
							<dd>
								<?php echo h($reserindividuale['Reserindividuale']['descuento']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Total'); ?></dt>
							<dd>
								<?php echo h($reserindividuale['Reserindividuale']['total']); ?>
								&nbsp;
							</dd>


						</dl>
                        </div>
                    </div>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $reserindividuale['Reserindividuale']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
					
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->

<script>
	$.ajax({
	    url:"/Reserindividualespub/cupostipohabitacionesview/",
	    type:"post",
	    data:{
	      idreserva : <?=$reserindividuale['Reserindividuale']['id']; ?>,
	      
	    },

	    success:function(response){

	    	var data = JSON.parse(response);
	    	var data2 = data.datatotal;
	    	var html = ''
	    	


	    	$("#datahtml").html(data2);
	    	


	    }


	});
</script>