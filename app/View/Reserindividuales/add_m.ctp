<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Reservación Individial'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Reservación Individial'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Add Reservación Individial'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Reserindividuale', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualetipocliente_id">Tipo de cliente</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('tipocliente_id', array('id'=>'Reserindividualetipocliente_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', "onChange"=>"selectTagRemote('".$this->Html->url('/Reserindividuales/sub_cliente')."', 'div-subcliente', this.value);", 'empty'=>'--Seleccione--')); 
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualetipoclientesub_id">Tipo de sub cliente</label>';		
									echo'<div class="col-md-9" id="div-subcliente">';			
									echo $this->Form->input('tipoclientesub_id', array('id'=>'Reserindividualetipoclientesub_id', 'div'=>false, 'label'=>false, 'class'=>'form-control','options'=>array()));
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualecliente_id">Cliente</label>';		
									echo'<div class="col-md-9" id="div-cliente">';			
									echo $this->Form->input('cliente_id', array('id'=>'Reserindividualecliente_id', 'div'=>false, 'label'=>false, 'class'=>'form-control','options'=>array()));
									echo '</div>';	
									echo '</div>';
									/*
									?>

									<!--------- EMAIL -------->
									<div class="form-group">
									<label class="control-label col-md-2" for="Reserindividualecliente_id">Acompañantes </label>
				                    <div class="col-md-9">
				                        <div class="box box-solid box-default">
				                            <div class="box-header with-border">
				                                <h3 class="box-title">Nombre Completo</h3>
				                                <div class="box-tools pull-right">
				                                    <button id="newNombresBtn" class="btn btn-box-tool" data-toggle="tooltip" title="Agregar otro email" data-widget="chat-pane-toggle"><i class="glyphicon glyphicon-plus"></i></button>
				                                </div><!-- /.box-tools -->
				                            </div><!-- /.box-header -->
				                            <div class="box-body">
				                                <div id="nombres-container">
				                                        <div id="nombrecount">
					                                        <div class="form-group nombre-con" id="nombre_0">
					                                            <label class="control-label col-md-3">
					                                                Nombre Completo
					                                            </label>
					                                            <div class="col-md-8">
					                                                <div class='input-group col-md-12'>
					                                               		<input class="form-control text-box single-line" id="Nombres_0__Nombre" name="data[Reserindividuale][Nombre][]" placeholder="Ej: Juan Antonio Pacheco Lugo" require="require" type="text" value="" />
					                                                 </div>
					                                            </div>
					                                            <div class="col-md-1">
					                                                <button class="btn btn-danger btn-sm" onclick="if(confirm('Seguro que desea eliminar el registro')){ $('#nombre_0').remove(); $('#Nombres_0__Nombre').val(''); $('#Nombres_0__Nombre').removeAttr('required'); } return false;"><span class="glyphicon glyphicon-trash"></span></button>
					                                            </div>
					                                        </div>
				                                        </div>
				                                </div>
				                            </div>
				                        </div>
				                    </div>
				                    </div>
								    <?php
										*/
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualetipohabitacione_id">Tipo Habitación</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('tipohabitacione_id', array('id'=>'Reserindividualetipohabitacione_id', 'div'=>false, 'label'=>false, 'class'=>'form-control')); 
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualehabitacione_id">Habitación</label>';		
									echo'<div class="col-md-9" id="div-habitacion">';			
									echo $this->Form->input('habitacione_id', array('id'=>'Reserindividualehabitacione_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualecantidad_personas">Cantidad personas</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('cantidad_personas', array('id'=>'Reserindividualecantidad_personas', 'div'=>false, 'label'=>false, 'class'=>'form-control'));
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualefecha_entrada">Fecha entrada</label>';		
									echo'<div class="col-md-9" id="div-fechaentrada">';			
									echo $this->Form->input('fecha_entrada', array('id'=>'Reserindividualefecha_entrada', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>true, 'value'=>$resermultiples2[0]['Resermultiple']['fecha_entrada']));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualefecha_salida">Fecha salida</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('fecha_salida', array('id'=>'Reserindividualefecha_salida', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>true, 'value'=>$resermultiples2[0]['Resermultiple']['fecha_entrada']));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualeobseraciones">Obseraciones</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('obseraciones', array('id'=>'Reserindividualeobseraciones', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualedesayuno">Incluye Desayuno</label>';		
									echo'<div class="col-md-9" id="div-total">';			
									echo $this->Form->input('desayuno', array('value'=>1, 'type'=>'checkbox', 'id'=>'Reserindividualedesayuno', 'div'=>false, 'label'=>false, 'class'=>''));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualealmuerzo">Incluye Almuerzo</label>';		
									echo'<div class="col-md-9" id="div-total">';			
									echo $this->Form->input('almuerzo', array('value'=>1, 'type'=>'checkbox', 'id'=>'Reserindividualealmuerzo', 'div'=>false, 'label'=>false, 'class'=>''));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualecena">Incluye Cena</label>';		
									echo'<div class="col-md-9" id="div-total">';			
									echo $this->Form->input('cena', array('value'=>1, 'type'=>'checkbox', 'id'=>'Reserindividualecena', 'div'=>false, 'label'=>false, 'class'=>''));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualeautomovil">Automovil</label>';		
									echo'<div class="col-md-9" id="div-total">';			
									echo $this->Form->input('automovil', array('type'=>'text', 'id'=>'Reserindividualeautomovil', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group" style="display:none;">';	
									echo'<label class="control-label col-md-2" for="Reserindividualedias">Dias</label>';		
									echo'<div class="col-md-9" id="div-dia">';			
									echo $this->Form->input('dias', array('value'=>0, 'id'=>'Reserindividualedias', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>true));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group" style="display:none;">';	
									echo'<label class="control-label col-md-2" for="Reserindividualeprecioxdia">Precio x dia</label>';		
									echo'<div class="col-md-9" id="div-precioxdia">';			
									echo $this->Form->input('precioxdia', array('value'=>0,'id'=>'Reserindividualeprecioxdia', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>true));		
									echo '</div>';	
									echo '</div>';
									
										
									echo'<div class="form-group" style="display:none;">';	
									echo'<label class="control-label col-md-2" for="Reserindividualetotal">Total</label>';		
									echo'<div class="col-md-9" id="div-total">';			
									echo $this->Form->input('total', array('value'=>0,'id'=>'Reserindividualetotal', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>true));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group" style="display:none;">';	
									echo'<label class="control-label col-md-2" for="Reserindividualeresermultiple_id">resermultiple_id</label>';		
									echo'<div class="col-md-9" id="div-resermultiple_id">';			
									echo $this->Form->input('resermultiple_id', array('value'=>$resermultiple_id,'id'=>'Reserindividualeresermultiple_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>true));		
									echo '</div>';	
									echo '</div>';
								?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    
	                                    <input value="Guardar" id="Reserindividualesubmit" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog" style="z-index: 3">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agregar Cliente</h4>
      </div>
      <div class="modal-body">

      		                <div class="box-body">
      							<?php echo $this->Form->create('Cliente', array('class'=>'form-horizontal','url'=>'/Clientes/add')); ?>
      							<div class='row'>
      									<div class='col-md-12'>
      										<?php
      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientetipocliente_id">Tipo cliente</label>';		
      											echo'<div class="col-md-9">';			
      											echo $this->Form->input('tipocliente_id', array('id'=>'Clientetipocliente_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', "onChange"=>"selectTagRemote('".$this->Html->url('/Clientes/sub_cliente')."', 'div-subcliente2', this.value);", 'empty'=>'--Seleccione--')); 
      											echo '</div>';	
      											echo '</div>';
      												
      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientetipoclientesub_id">Tipo subcliente</label>';		
      											echo'<div class="col-md-9" id="div-subcliente2">';			
      											echo $this->Form->input('tipoclientesub_id', array('id'=>'Clientetipoclientesub_id', 'div'=>false, 'label'=>false, 'class'=>'form-control','options'=>array()));		
      											echo '</div>';	
      											echo '</div>';
      												
      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientedni">Dni</label>';		
      											echo'<div class="col-md-9">';			
      											echo $this->Form->input('dni', array('id'=>'Clientedni', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
      											echo '</div>';	
      											echo '</div>';
      												
      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientenombre_completo">Nombre completo</label>';		
      											echo'<div class="col-md-9">';			
      											echo $this->Form->input('nombre_completo', array('id'=>'Clientenombre_completo', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
      											echo '</div>';	
      											echo '</div>';

      											echo'<div class="form-group">';	
												echo'<label class="control-label col-md-2" for="Clientefecha_nacimiento">Fecha nacimiento</label>';		
												echo'<div class="col-md-9">';			
												echo $this->Form->input('fecha_nacimiento', array('id'=>'Clientefecha_nacimiento', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
												echo '</div>';	
												echo '</div>';

												echo'<div class="form-group">';	
												echo'<label class="control-label col-md-2" for="Clientepai_id">Nacionalidad</label>';		
												echo'<div class="col-md-9">';			
												echo $this->Form->input('pai_id', array('empty'=>'--Seleccione--', 'id'=>'Clientepai_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
												echo '</div>';	
												echo '</div>';

												echo'<div class="form-group">';	
												echo'<label class="control-label col-md-2" for="Clienteciudad">Ciudad</label>';		
												echo'<div class="col-md-9">';			
												echo $this->Form->input('ciudad', array('id'=>'Clienteciudad', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
												echo '</div>';	
												echo '</div>';
      												
      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientedireccion">Dirección</label>';		
      											echo'<div class="col-md-9">';			
      											echo $this->Form->input('direccion', array('id'=>'Clientedireccion', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
      											echo '</div>';	
      											echo '</div>';
      												
      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientetelf">Telf</label>';		
      											echo'<div class="col-md-9">';			
      											echo $this->Form->input('telf', array('id'=>'Clientetelf', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
      											echo '</div>';	
      											echo '</div>';
      												
      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientecorreo">Email</label>';		
      											echo'<div class="col-md-9">';			
      											echo $this->Form->input('correo', array('id'=>'Clientecorreo', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
      											echo '</div>';	
      											echo '</div>';
      												
      											echo'<div class="form-group">';	
      											echo'<label class="control-label col-md-2" for="Clientecod_pos">Cod pos</label>';		
      											echo'<div class="col-md-9">';			
      											echo $this->Form->input('cod_pos', array('id'=>'Clientecod_pos', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
      											echo '</div>';	
      											echo '</div>';
      										?>
      									</div>
      									<div class="col-md-12">
      										<div class="form-group">
      			                                <div class="col-md-12">
      			                                <a href="#" onclick="guardar()" class="btn btn-primary pull-right">Guardar</a>    
      			                                <!--<input  value="Guardar" class="btn btn-primary pull-right" type="submit">-->
      			                                </div>
      			                            </div>
      		                            </div>
      							</div></form>                </div>





      </div>
      <div class="modal-footer">
        
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
	function guardar(){
		$.post('/Clientes/add',$("#ClienteAddMForm").serialize(),function(response){
			window.location.reload();
		});
	}
</script>
<script type="text/javascript">
$(document).ready(function() {
    $("#newNombresBtn").click(function (e) {
        var MaxInputs       = 8;
        var x = $("#nombres-container #emailcount").length + 1;
        var FieldCount = x-1;
        if(x <= MaxInputs) //max input box allowed
        {
            FieldCount++;
            $("#nombres-container").append('<div id="nombrecount"><div class="form-group nombre-con" id="nombre_'+ FieldCount +'"><label class="control-label col-md-3">Nombre Completo</label><div class="col-md-8"><div class="input-group col-md-12"><input class="form-control text-box single-line" id="Nombres_'+ FieldCount +'__Nombre" name="data[Reserindividuale][Nombre][]" placeholder="Ej: Juan Antonio Pacheco Lugo" require="require" type="text" value="" /></div></div><div class="col-md-1"><button class="btn btn-danger btn-sm" onclick="if(confirm(\'Seguro que desea eliminar el registro\')){ $(\'#nombre_'+ FieldCount +'\').remove(); $(\'#Nombres_'+ FieldCount +'__Nombre\').val(\'\'); $(\'#Nombres_'+ FieldCount +'__Nombre\').removeAttr(\'required\'); } return false;"><span class="glyphicon glyphicon-trash"></span></button></div></div></div>');
            x++; //text box increment
        }
        return false;
    });
});
</script>