<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Reservación Individial'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Reservación Individial'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Edit Reservación Individial'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Reserindividuale', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
								    echo $this->Form->input('id', array('class'=>'form-horizontal'));	
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualetipocliente_id">Tipo de cliente</label>';		
									echo'<div class="col-md-9">';			
								
									echo $this->Form->input('tipocliente_id2', array('id'=>'Reserindividualetipocliente_id2', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly'=>true, 'type'=>'text', 'value'=>$tipoclientes[0]['Tipocliente']['denominacion']));
								

									echo $this->Form->input('tipocliente_id',  array('id'=>'Reserindividualetipocliente_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly'=>true, 'type'=>'hidden'));
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualetipoclientesub_id">Tipo de sub cliente</label>';		
									echo'<div class="col-md-9" id="div-subcliente">';			
									echo $this->Form->input('tipoclientesub_id2', array('id'=>'Reserindividualetipoclientesub_id2', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly'=>true, 'type'=>'text', 'value'=>$tipoclientesubs[0]['Tipoclientesub']['nombre']));
									echo $this->Form->input('tipoclientesub_id',  array('id'=>'Reserindividualetipoclientesub_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly'=>true, 'type'=>'hidden'));
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualecliente_id">Cliente</label>';		
									echo'<div class="col-md-9" id="div-cliente">';			
									echo $this->Form->input('cliente_id2', array('id'=>'Reserindividualecliente_id2', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly'=>true, 'type'=>'text', 'value'=>$clientes[0]['Cliente']['nombre_completo']));
									echo $this->Form->input('cliente_id',  array('id'=>'Reserindividualecliente_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly'=>true, 'type'=>'hidden'));
									echo '</div>';	
									echo '</div>';
									/*
									?>
									<!--------- EMAIL -------->
									<div class="form-group">
									<label class="control-label col-md-2" for="Reserindividualecliente_id">Acompañantes </label>
				                    <div class="col-md-9">
				                        <div class="box box-solid box-default">
				                            <div class="box-header with-border">
				                                <h3 class="box-title">Nombre Completo</h3>
				                                <div class="box-tools pull-right">
				                                    <button id="newNombresBtn" class="btn btn-box-tool" data-toggle="tooltip" title="Agregar otro nombre" data-widget="chat-pane-toggle"><i class="glyphicon glyphicon-plus"></i></button>
				                                </div><!-- /.box-tools -->
				                            </div><!-- /.box-header -->
				                            <div class="box-body">
				                                <div id="nombres-container">
				                                 <?php $c = 0;foreach($data['Reserindividualeextra'] as $reserindividualeextra){ ?>
				                                        <div id="nombrecount">
					                                        <div class="form-group nombre-con" id="nombre_<?= $c ?>">
					                                            <label class="control-label col-md-3">
					                                                Nombre Completo
					                                            </label>
					                                            <div class="col-md-8">
					                                                <div class='input-group col-md-12'>
					                                               		<input class="form-control text-box single-line" id="Nombres_<?= $c ?>__Nombre" name="data[Reserindividuale][Nombre][]" placeholder="Ej: Juan Antonio Pacheco Lugo" require="require" type="text" value="<?= $reserindividualeextra['nombres'] ?>" />
					                                                 </div>
					                                            </div>
					                                            <div class="col-md-1">
					                                                <button class="btn btn-danger btn-sm" onclick="if(confirm('Seguro que desea eliminar el registro')){ $('#nombre_<?= $c ?>').remove(); $('#Nombres_<?= $c ?>__Nombre').val(''); $('#Nombres_<?= $c ?>__Nombre').removeAttr('required'); } return false;"><span class="glyphicon glyphicon-trash"></span></button>
					                                            </div>
					                                        </div>
				                                        </div>
				                                <?php $c++;} ?>
				                                </div>
				                            </div>
				                        </div>
				                    </div>
				                    </div>
								    <?php
								    */
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualetipohabitacione_id">Tipo Habitación</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('tipohabitacione_id', array('id'=>'Reserindividualetipohabitacione_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', "onChange"=>"selectTagRemote('".$this->Html->url('/Reserindividuales/habitacion')."', 'div-habitacion', this.value);", 'empty'=>'--Seleccione--')); 
									echo '</div>';	
									echo '</div>';
										
									



									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualehabitacione_id">Habitación</label>';		
									echo'<div class="col-md-9" id="div-habitacion">';			
									echo $this->Form->input('habitacione_id2', array('id'=>'Reserindividualehabitacione_id2', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly'=>true, 'type'=>'text', 'value'=>$habitaciones[0]['Habitacione']['numhabitacion']));
									echo $this->Form->input('habitacione_id',  array('id'=>'Reserindividualehabitacione_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly'=>true, 'type'=>'hidden'));
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualefecha_entrada">Fecha entrada</label>';		
									echo'<div class="col-md-9" id="div-fechaentrada">';			
									echo $this->Form->input('fecha_entrada', array('id'=>'Reserindividualefecha_entrada', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualefecha_salida">Fecha salida</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('fecha_salida', array('id'=>'Reserindividualefecha_salida', 'div'=>false, 'label'=>false, 'class'=>'form-control',"onChange"=>'javascript:calcular_individual_habitacion();'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualeobseraciones">Observaciones</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('obseraciones', array('id'=>'Reserindividualeobseraciones', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualeobseraciones">Observaciones del Cliente</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('obseraciones_cliente', array('id'=>'Reserindividualeobseracionescliente', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualedesayuno">Incluye Desayuno</label>';		
									echo'<div class="col-md-9" id="div-total">';			
									echo $this->Form->input('desayuno', array('value'=>1, 'type'=>'checkbox', 'id'=>'Reserindividualedesayuno', 'div'=>false, 'label'=>false, 'class'=>''));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualealmuerzo">Incluye Almuerzo</label>';		
									echo'<div class="col-md-9" id="div-total">';			
									echo $this->Form->input('almuerzo', array('value'=>1, 'type'=>'checkbox', 'id'=>'Reserindividualealmuerzo', 'div'=>false, 'label'=>false, 'class'=>''));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualecena">Incluye Cena</label>';		
									echo'<div class="col-md-9" id="div-total">';			
									echo $this->Form->input('cena', array('value'=>1, 'type'=>'checkbox', 'id'=>'Reserindividualecena', 'div'=>false, 'label'=>false, 'class'=>''));		
									echo '</div>';	
									echo '</div>';


									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualecantidad_personas">Cantidad Personas</label>';		
									echo'<div class="col-md-9" id="div-total">';			
									echo $this->Form->input('cantidad_personas', array('type'=>'text', 'id'=>'Reserindividualecantidad_personas', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualeautomovil">Automovil</label>';		
									echo'<div class="col-md-9" id="div-total">';			
									echo $this->Form->input('automovil', array('type'=>'text', 'id'=>'Reserindividualeautomovil', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualedias">Dias</label>';		
									echo'<div class="col-md-9" id="div-dia">';			
									echo $this->Form->input('dias', array('id'=>'Reserindividualedias', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>false, "onChange"=>'recalcular_individual();'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualeprecioxdia">Precio x dia</label>';		
									echo'<div class="col-md-9" id="div-precioxdia">';			
									echo $this->Form->input('precioxdia', array('id'=>'Reserindividualeprecioxdia', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>false, "onChange"=>'recalcular_individual();'));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualedescuento">Descuento (%)</label>';		
									echo'<div class="col-md-9" id="div-descuento">';			
									echo $this->Form->input('descuento', array('id'=>'Reserindividualedescuento', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>false, "onChange"=>'recalcular_individual();'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindividualetotal">Total</label>';		
									echo'<div class="col-md-9" id="div-total">';			
									echo $this->Form->input('total', array('id'=>'Reserindividualetotal', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>true));		
									echo '</div>';	
									echo '</div>';
								?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <a href="<?=$this->Html->url('/Reserindivistatuses/add/'.$id."")?>"><i class="fa fa-circle-o"></i>Volver</a>
	                                    <input value="Guardar" id="Reserindividualesubmit" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Reserindividuale.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Reserindividuale.id'))); ?></li>
				<li><?php echo $this->Html->link(__('List Reserindividuales'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('List Tipoclientes'), array('controller' => 'tipoclientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipocliente'), array('controller' => 'tipoclientes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipoclientesubs'), array('controller' => 'tipoclientesubs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipoclientesub'), array('controller' => 'tipoclientesubs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Clientes'), array('controller' => 'clientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cliente'), array('controller' => 'clientes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipohabitaciones'), array('controller' => 'tipohabitaciones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipohabitacione'), array('controller' => 'tipohabitaciones', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Habitaciones'), array('controller' => 'habitaciones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Habitacione'), array('controller' => 'habitaciones', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Reserstatusindividuales'), array('controller' => 'reserstatusindividuales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reserstatusindividuale'), array('controller' => 'reserstatusindividuales', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resermultiples'), array('controller' => 'resermultiples', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resermultiple'), array('controller' => 'resermultiples', 'action' => 'add')); ?> </li>
		</ul>
	</div>
<?php */ ?>
<script type="text/javascript">
$(document).ready(function() {
    $("#newNombresBtn").click(function (e) {
        var MaxInputs       = 8;
        var x = $("#nombres-container #emailcount").length + 1;
        var FieldCount = x-1;
        if(x <= MaxInputs) //max input box allowed
        {
            FieldCount++;
            $("#nombres-container").append('<div id="nombrecount"><div class="form-group nombre-con" id="nombre_'+ FieldCount +'"><label class="control-label col-md-3">Nombre Completo</label><div class="col-md-8"><div class="input-group col-md-12"><input class="form-control text-box single-line" id="Nombres_'+ FieldCount +'__Nombre" name="data[Reserindividuale][Nombre][]" placeholder="Ej: Juan Antonio Pacheco Lugo" require="require" type="text" value="" /></div></div><div class="col-md-1"><button class="btn btn-danger btn-sm" onclick="if(confirm(\'Seguro que desea eliminar el registro\')){ $(\'#nombre_'+ FieldCount +'\').remove(); $(\'#Nombres_'+ FieldCount +'__Nombre\').val(\'\'); $(\'#Nombres_'+ FieldCount +'__Nombre\').removeAttr(\'required\'); } return false;"><span class="glyphicon glyphicon-trash"></span></button></div></div></div>');
            x++; //text box increment
        }
        return false;
    });
});
</script>