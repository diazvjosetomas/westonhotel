
<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Reservación Individial'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Reservación Individial'); ?></li>
  </ol>
</section>

<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Reservación Individial'); ?> Registrados</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                    <p><?php echo $this->Html->link(__('Crear Nuevo'), array('action' => 'add'),  array('class'=>'btn btn-primary')); ?></p>

							<table id="data" class="table table-striped table-bordered" width="100%" cellspacing="0">
							<thead>
							<tr>
								<th style="width: 30px;"><?php echo __('Reserva'); ?></th>
								<th style="width: 40px;"><?php echo __('Tipo habitación'); ?></th>
								<th style="width: 40px;"><?php echo __('Habitación'); ?></th>
								<th style="width: 60px;"><?php echo __('Cliente'); ?></th>
								<th style="width: 60px;"><?php echo __('Fecha entrada'); ?></th>
								<th style="width: 60px;"> <?php echo __('Fecha salida'); ?></th>
								<th style="width: 60px;"> <?php echo __('Observación'); ?></th>
								<th><?php echo __('Status'); ?></th>
								<th class="actions"><?php echo __('Acción'); ?></th>
							</tr>
							</thead>
							<tbody>
							<?php foreach ($reserindividuales as $reserindividuale): ?>
	<tr>
		<td><?php echo h('R'.$reserindividuale['Reserindividuale']['id']); ?></td>
		<td><?php echo h($reserindividuale['Tipohabitacione']['denominacion']); ?></td>
		<td><?php echo h('H'.$reserindividuale['Habitacione']['numhabitacion']); ?></td>
		<td><?php echo h($reserindividuale['Cliente']['nombre_completo']); ?></td>
		<td><?php echo h( formatdmy($reserindividuale['Reserindividuale']['fecha_entrada']) ); ?>&nbsp;</td>
		<td><?php echo h( formatdmy($reserindividuale['Reserindividuale']['fecha_salida']) ); ?>&nbsp;</td>
		<td><?php echo h($reserindividuale['Reserindividuale']['obseraciones']); ?>&nbsp;</td>
		<td>
			<?php 

			if ($reserindividuale['Reserstatusindividuale']['id'] == 5) {
				
			echo '<strong class="btn btn-warning">'.$reserindividuale['Reserstatusindividuale']['denominacion']."</strong>"; 
			}else{
				echo $this->Html->link(__(h($reserindividuale['Reserstatusindividuale']['denominacion'])), array('action' => 'status', $reserindividuale['Reserindividuale']['id']), array('class'=>'btn btn-warning'));
			}


			?>

			<?php 

			$tipoclienteid = !empty($reserindividuale['Tipocliente']['id']) ? $reserindividuale['Tipocliente']['id'] : 0;
			$tipoclientesubid = !empty($reserindividuale['Tipoclientesub']['id']) ? $reserindividuale['Tipoclientesub']['id'] : 0;
			$clienteid = !empty($reserindividuale['Cliente']['id']) ? $reserindividuale['Cliente']['id'] : 0;
			$reservaindividualeid = !empty($reserindividuale['Reserindividuale']['id']) ? $reserindividuale['Reserindividuale']['id'] : 0;

			$tipohabitacioneid  = !empty($reserindividuale['Tipohabitacione']['id']) ? $reserindividuale['Tipohabitacione']['id'] : 0;
			$habitacioneid = !empty($reserindividuale['Habitacione']['id']) ? $reserindividuale['Habitacione']['id'] : 0;


			if ($reserindividuale['Reserstatusindividuale']['id'] == 4) {
				echo $this->Html->link(__(h('Agg Consumo')), array('action' => 'add_2/'.$tipohabitacioneid.'/'.$habitacioneid.'/'.$reservaindividualeid.'/individual','controller'=>'Consumos'), array('class'=>'btn btn-success'));
			}

			if ($reserindividuale['Reserstatusindividuale']['id'] == 5) {
				echo $this->Html->link(__(h('Facturar')), array('action' => 'add_2/'.$tipoclienteid.'/'.$tipoclientesubid.'/'.$clienteid.'/'.$reservaindividualeid.'/individual','controller'=>'Facturas'), array('class'=>'btn btn-danger'));
			}



			?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $reserindividuale['Reserindividuale']['id']), array('class'=>'')); ?>
			| <?php echo $this->Html->link(__('Ver'), array('action' => 'view', $reserindividuale['Reserindividuale']['id']),   array('class'=>'')); ?>
            <?php  
	            $rol = $this->Session->read('ROL');
	            if($rol==1){
            ?>
				 
				<?php

				if ($reserindividuale['Reserstatusindividuale']['id'] != 5) {
					echo " | ";
					echo $this->Html->link(__('Eliminar'), 1, array('onclick' => "eliminar(".$reserindividuale['Reserindividuale']['id']."); return false;", 'escape' => false));
				}
			}
			?>
		</td>
	</tr>
<?php endforeach; ?>
							</tbody>
							</table>
						</div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<!-- <?php 
if(isset($id_reporte)){
?>
<script type="text/javascript">
	generar_reporte_reserva('<?= $id_reporte ?>');
</script>
<?php
}
?> -->

<script type="text/javascript">
	//$(document).ready(function() {
	    $('#data').DataTable( {
	    	dom: 'Bfrtlip',
	        buttons: [
	            'copy', 'csv', 'excel', 'pdf', 'print'
	        ],
	        "language": 
	        {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
	    } );

	  	function eliminar(reserva){
	    	bootbox.prompt({ 
			  title: "<h4><i class='fa fa-trash'></i> Desea eliminar el registro "+reserva+ "? Si es asi, Favor especifique un motivo!</h4>", 
			  callback: function(result){ 
			  	if( result === null  || result === ''){
			  		bootbox.alert({
    					message: "<h4><i class='fa fa-remove'></i> El registro no ha sido eliminado. Debe especificar un MOTIVO!</h4>",
    				});
			  	}else{
			  		document.location.href="/reserindividuales/delete/"+reserva+'/'+result;
			  	}
			   }
			});
		}  
	//} );
</script>

