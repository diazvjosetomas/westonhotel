<style>
    .tipo_habitacion{
        border-radius: 10px 10px 10px 10px;
        -moz-border-radius: 10px 10px 10px 10px;
        -webkit-border-radius: 10px 10px 10px 10px;
        border: 2px solid #84c400;

        padding: 5px;
        padding-top: 15px;
    }
</style>


<section class="content">
    <div id="google_translate_element"></div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">

                    <div class="col-xs-12">

                            <div style='text-align: center;'>
                        <h3 class="box-title"><?php echo __('Agregar Reservación'); ?></h3>
                            </div>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?php echo $this->Form->create('Reserindividuale', array('class'=>'form-horizontal')); ?>
                    <div class='row'>
                        <div class='col-md-2'>
                        </div>
                        <div class='col-md-8'>
                            <?php
                            
                            echo'<div class="form-group hidden">';
                            echo'<label class="control-label col-md-2" for=""></label>';
                            echo'<div class="col-md-4" id="">';
                            echo $this->Form->input('inicio', array('id'=>'inicio', 'div'=>false, 'label'=>false, 'class'=>'form-control hidden', 'readonly' => true, 'value' => 'inicio'));
                            echo '</div>';
                            echo '</div>';


                            echo'<div class="form-group">';
                            echo'<label class="control-label col-md-4" for="Reserindividualefecha_entrada">Fecha entrada</label>';
                            echo'<div class="col-md-2" id="div-fechaentrada">';
                            echo $this->Form->input('fecha_entrada1', array('id'=>'Reserindividualefecha_entrada', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly' => true, 'value' => $fechai, 'style' => 'border:none; backgroud:none;'));
                            echo '</div>';

                            echo'<label class="control-label col-md-2" for="Reserindividualefecha_salida">Fecha salida</label>';
                            echo'<div class="col-md-2">';
                            echo $this->Form->input('fecha_salida1', array('id'=>'Reserindividualefecha_salida', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly' => true, "onChange"=>'javascript:tipo_habitacion();', 'value' => $fechas, 'style' => 'border:none; backgroud:none;'));
                            echo '</div>';
                            echo '</div>';

                            //,"onChange"=>'javascript:calcular_individual_habitacion();


                            //...............
                            echo "<div class='col-md-12 col-xs-12 col-lg-12 col-sm-12'>";
                            echo "<div style='text-align: center;'>";
                            echo "<h3> <b> Tipos de Habitaciones  Seleccionadas </b> </h3>";
                            echo "</div>";
                            echo "</div>";
                            echo "<div class='col-md-1 col-xs-1 col-lg-1 col-sm-1'>";
                            echo "</div>";
                            echo "<div class='col-md-10 col-xs-10 col-lg-10 col-sm-10 tipo_habitacion' id='id_tipo_habitacion'>";
                            // echo'<div class="form-group">';
                            //  echo'<label class="control-label col-md-2" for="Reserindividualetipohabitacione_id"><h4>Tipo Habitación </h4> </label>';
                            //     echo'<div class="col-md-4">';
                            //     echo $this->Form->input('tipohabitacione_id', array('id'=>'Reserindividualetipohabitacione_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', "onChange"=>'javascript:tipo_habitacion();', 'value'=>$id));
                            //     echo '</div>';

                            //     echo'<div class="col-md-2">';
                            //     echo $this->Form->input('cantidad_personas', array('id'=>'Reserindividualecantidad_personas', 'div'=>false, 'label'=>false, 'class'=>'form-control','placeholder'=>'Cantidad de Personas', 'value' => $personas));
                            //     echo '</div>';

                            //     echo "<div class='col-md-2'>";
                            //     echo "<h4> <a id='autoclick' href='#'  onclick='cantpersonas()'>  <i class='fa fa-plus-square' ></i> Agregar</a> </h4>";
                            //     echo "</div>";

                            // echo '</div>';

                            echo "</div>";

                            echo"<br>";
                            echo '</div>';
                            // echo'<div class="form-group" >';
                            // echo'<label class="control-label col-md-2" >Cant Tipo Habitaciones</label>';
                            // echo'<div class="col-md-9" id="div-canttipohabitaciones">';
                            // echo $this->Form->input('canttipohabitaciones', array('value'=>0,'id'=>'Reserindividualecanttipohabitaciones', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>false));
                            // echo '</div>';
                            // echo '</div>';
                            echo '<div class="container     ">';
                            echo "<div class='tipo_habitacion' >";

                            echo "<hr>";


                            echo'<div class="form-group">';

                            echo'<label class="control-label col-md-2" for="Reserindividualeobseraciones">Preferencias</label>';
                            echo'<div class="col-md-4" id="preferencias">';
                            echo $this->Form->input('obseraciones_cliente', array('id'=>'Reserindividualeobseracionescliente', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'name'=>'data[Reserindividuale][observaciones_cliente]'));
                            echo '</div>';


                            echo'<label class="control-label col-md-2" for="Reserindividualedescuento">Descuento (%)</label>';
                            echo'<div class="col-md-4" id="descuento">';
                            echo $this->Form->input('descuento', array('value'=>0,'id'=>'Reserindividualedescuento', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>false, "onChange"=>'recalcular_individual();', 'disabled'=>'true'));
                            echo '</div>';
                            echo '</div>';


                            echo'<div class="form-group">';
                            echo'<label class="control-label col-md-2" for="Reserindividualeautomovil">Patente</label>';
                            echo'<div class="col-md-4" id="div-total">';
                            echo $this->Form->input('automovil', array('type'=>'text', 'id'=>'Reserindividualeautomovil', 'div'=>false, 'label'=>false, 'class'=>'form-control'));
                            echo '</div>';

                            echo'<label class="control-label col-md-2" for="Reserindividualedias">Cant Noches</label>';
                            echo'<div class="col-md-4" id="div-dia">';
                            echo $this->Form->input('dias', array('value'=>0, 'id'=>'Reserindividualedias', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly'=>false, "onChange"=>'cambiarfecha();','type'=>'number'));
                            echo '</div>';
                            echo '</div>';

                            echo'<div class="form-group" >';
                            echo'<label class="control-label col-md-2" for="Reserindividualelocalizador">Localizador</label>';
                            echo'<div class="col-md-4" id="div-descuento">';
                            echo $this->Form->input('localizador', array('id'=>'Reserindividualelocalizador', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>true, 'value'=>time() ));
                            echo '</div>';

                            echo'<label class="control-label col-md-2" for="Reserindividualeprecioxdia">Precio x noche</label>';
                            echo'<div class="col-md-4" id="div-precioxdia">';
                            echo $this->Form->input('precioxdia', array('value'=>0,'id'=>'Reserindividualeprecioxdia', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>false, "onChange"=>'recalcular_individual();', 'readonly' => 'true'));
                            echo '</div>';
                            echo '</div>';

                            echo'<div class="form-group">';
                            echo'<label class="control-label col-md-2" for="Reserindividualeobseraciones">Observaciones</label>';
                            echo'<div class="col-md-4">';
                            echo $this->Form->input('obseraciones', array('id'=>'Reserindividualeobseraciones', 'div'=>false, 'label'=>false, 'class'=>'form-control'));
                            echo '</div>';

                            echo'<label class="control-label col-md-2" for="Reserindividualetotal">Total</label>';

                            echo'<div class="col-md-4" id="div-total">';
                            echo $this->Form->input('total', array('id'=>'Reserindividualetotal', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'number', 'readonly' => 'true', 'value'=>'0'));

                            echo '</div>';
                            echo '</div>';

                            echo'<div  class="form-group" >';
                            echo'<label class="control-label col-md-2" for=""></label>';

                            echo'<div class="col-md-3" id="">';

                            echo '</div>';


                            echo'<label class="control-label col-md-2" for="Reserindividualetotal">Agregar Datos Cliente: <i data-toggle="modal" data-target="#myModal" class="fa fa-plus-circle" ></i> </label>';
                            echo'<div class="col-md-4" id="div-cliente">';


                            echo $this->Form->input('', array('style'=>'border: none;','type'=>'text','required'=>'true','onclick'=>'modalcliente()','autocomplete'=>'off'));


                            echo '</div>';

                            echo '</div>';


                            echo'<div  id="div-cliente">';

                            echo '</div>';

                            
                            echo $this->Form->input('detallereservas', array('id'=>'detallereservastable', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'hidden', 'readonly'=>true));
                            
                            ?>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <?php echo $this->Html->link(__('Volver al Inicio'), array('controller' => 'Hoteleria', 'action' => 'index')); ?>
                                  

                                    <button style="display: none;" id="btnaddform" value="Enviar"  class="btn btn-primary pull-right" type="submit">Enviar</button>



                                    <label class="pull-right" for="">  <input id="micheckbox" onclick="acepto()" type="checkbox" checked="true"> <div onclick="modalterminos()"  id="terminos"> Acepto los Términos y Condiciones</div></label> &nbsp;&nbsp;&nbsp;&nbsp;
                                </div>
                            </div>
                        </div>

                        <br>
                        <br>



                        </div>
                        </div>
                        


    
                       
                    </div></form></div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog" style="">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Agregar Cliente</h4>
            </div>
            <div class="modal-body">

                <div class="box-body">
                    <?php echo $this->Form->create('Cliente', array('class'=>'form-horizontal','url'=>'/Clientes/add')); ?>
                    <div class='row'>
                        <div class='col-md-12'>

                            <!--

                                echo'<div class="form-group">';
                                echo'<label class="control-label col-md-2" for="Clientecod_pos">Cod pos</label>';
                                echo'<div class="col-md-9">';
                                echo $this->Form->input('cod_pos', array('id'=>'Clientecod_pos', 'div'=>false, 'label'=>false, 'class'=>'form-control'));
                                echo '</div>';
                                echo '</div>';
                                -->
                            <?php
                            echo'<div class="hidden">';
                            echo'<div class="form-group">';
                            echo'<label class="control-label col-md-2" for="Clientetipocliente_id">Tipo cliente</label>';
                            echo'<div class="col-md-4">';
                            echo $this->Form->input('tipocliente_id', array('value'=>'1', 'id'=>'Clientetipocliente_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', "onChange"=>"selectTagRemote('".$this->Html->url('/Clientespub/sub_cliente')."', 'div-subcliente2', this.value);", 'empty'=>'--Seleccione--'));
                            echo '</div>';
                            echo '</div>';

                            echo'<div class="form-group">';
                            echo'<label class="control-label col-md-2" for="Clientetipoclientesub_id">Tipo subcliente</label>';
                            echo'<div class="col-md-4" id="div-subcliente2">';
                            echo $this->Form->input('tipoclientesub_id', array('value'=>'145', 'id'=>'Clientetipoclientesub_id', 'div'=>false, 'label'=>false, 'class'=>'form-control','options'=>array()));
                            echo '</div>';
                            echo '</div>';
                            echo '</div>';


                            echo'<div class="form-group">';
                            echo'<label class="control-label col-md-2" for="Clientedni">Nº Pasaporte</label>';
                            echo'<div class="col-md-4">';
                            echo $this->Form->input('dni', array('id'=>'Clientedni', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'required' => true,'onBlur'=>'buscadato()'));
                            echo '</div>';

                            echo'<label class="control-label col-md-2" for="Clientenombre_completo">Nombre completo</label>';
                            echo'<div class="col-md-4">';
                            echo $this->Form->input('nombre_completo', array('id'=>'Clientenombre_completo', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'required' => true));
                            echo '</div>';

                            echo '</div>';


                            echo'<div class="form-group">';
                            echo'<label class="control-label col-md-2" for="Clientepai_id">País de Origen</label>';
                            echo'<div class="col-md-4">';
                            echo $this->Form->input('pai_id', array('empty'=>'--Seleccione--', 'id'=>'Clientepai_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'required' => true));
                            echo '</div>';



                            echo'<label class="control-label col-md-2" for="Clienteciudad">Ciudad</label>';
                            echo'<div class="col-md-4">';
                            echo $this->Form->input('ciudad', array('id'=>'Clienteciudad', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'required' => true));
                            echo '</div>';

                            echo '</div>';


                            echo'<div class="form-group">';
                            echo'<label class="control-label col-md-2" for="Clientedireccion">Dirección</label>';
                            echo'<div class="col-md-4">';
                            echo $this->Form->input('direccion', array('id'=>'Clientedireccion', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'required' => true));
                            echo '</div>';

                            echo'<label class="control-label col-md-2" for="Clientetelf">Teléfono</label>';
                            echo'<div class="col-md-4">';
                            echo $this->Form->input('telf', array('id'=>'Clientetelf', 'div'=>false, 'label'=>false, 'class'=>'form-control' , 'required' => true));
                            echo '</div>';

                            echo '</div>';

                            echo'<div class="form-group">';
                            echo'<label class="control-label col-md-2" for="Clientetelf">Movil</label>';
                            echo'<div class="col-md-4">';
                            echo $this->Form->input('movil', array('id'=>'Clientemovil', 'div'=>false, 'label'=>false, 'class'=>'form-control' , 'required' => true));
                            echo '</div>';

                            echo'<label class="control-label col-md-2" for="Clientecorreo">Email</label>';
                            echo'<div class="col-md-4">';
                            echo $this->Form->input('correo', array('id'=>'Clientecorreo', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'required' => true));
                            echo '</div>';

                            echo '</div>';
                            ##############
                            echo'<div class="form-group">';
                            echo'<label class="control-label col-md-2" for="Clientetdc">Num Tarjeta de Crédito</label>';
                            echo'<div class="col-md-4">';
                            echo $this->Form->input('tdc', array('id'=>'Clientetdc', 'div'=>false, 'label'=>false, 'class'=>'form-control' , 'required' => true));
                            echo '</div>';

                            echo'<label class="control-label col-md-2" for="Clientecod">Cod Tdc</label>';
                            echo'<div class="col-md-4">';
                            echo $this->Form->input('codtdc', array('id'=>'Clientecodtdc', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'required' => true));
                            echo '</div>';

                            echo '</div>';



                            ###############


                            $anho = array(
                                              '2019'=>'2019',
                                              '2020'=>'2020',
                                              '2021'=>'2021',
                                              '2022'=>'2022',
                                              '2023'=>'2023',
                                              '2024'=>'2024',
                                              '2025'=>'2025',
                                              '2026'=>'2026',
                                              '2027'=>'2027',
                                              '2028'=>'2028',
                                              '2029'=>'2029',
                                              '2030'=>'2030',

                                            );

                            $mes = array(
                              '1'=>'1',
                              '2'=>'2',
                              '3'=>'3',
                              '4'=>'4',
                              '5'=>'5',
                              '6'=>'6',
                              '7'=>'7',
                              '8'=>'8',
                              '9'=>'9',
                              '10'=>'10',
                              '11'=>'11',
                              '12'=>'12',

                            );


                            echo'<div class="form-group">';
                            echo'<label class="control-label col-md-4" for="Preferencias">Fecha de Vencimiento Tarjeta</label>';

                            echo'<div class="col-md-4">';
                            echo $this->Form->input('fvencimientoanho', array('id'=>'Clientefvencimientoanho', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'required' => true, 'options'=>$anho));
                            echo '</div>';

                            echo'<div class="col-md-4">';
                            echo $this->Form->input('fvencimientomes', array('id'=>'Clientefvencimientomes', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'required' => true, 'options'=>$mes));
                            echo '</div>';
                            echo '</div>';


                            echo'<div class="form-group">';
                            echo'<label class="control-label col-md-2" for="Preferencias">Preferencias</label>';
                            echo'<div class="col-md-9">';
                            echo $this->Form->input('preferencias', array('id'=>'Clientepreferencias', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'required' => true));
                            echo '</div>';
                            echo '</div>';


                            echo'<hr><div class="form-group" id="div_descuento" style="display:none;">';
                            echo'<label class="control-label col-md-8" for="Preferencias">Estimado <span id="nombre_cliente_descuento"></span>, ha ganado un '.$porcentaje_descuento.'% de descuento para esta reserva!</label>';
                            echo'<div class="col-md-4">';
                            echo '<a onclick="aplicardescuento()" class="btn btn-success">Aplicar '.$porcentaje_descuento.'% de descuento!</a>';
                            echo '</div>';
                            echo '</div><hr>';


                            ?>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <a href="#" onclick="guardar()" class="btn btn-primary pull-right">Guardar</a>
                                        <!--<input  value="Guardar" class="btn btn-primary pull-right" type="submit">-->
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    </form>
                </div>





            </div>
            <div class="modal-footer">

            </div>
        </div>

    </div>
</div>

<div id="myModalacepto" class="modal fade" role="dialog" style="">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Términos y Condiciones</h4>
            </div>
            <div class="modal-body">

                <div class="box-body">
                    
                    <input type="checkbox" onchange="acepto()" id="acepto">
                    <?=$terminos[0]['Terminoscondicione']['condiciones_generales']; ?>
                </div>

            </div>
            <div class="modal-footer">

            </div>
        </div>

    </div>
</div>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

<input type="hidden" value='1' id='control-th'>
<input type="hidden" id='lastth'>

<input type="hidden" id="ctrladd" value="0">


<input type="hidden" id="nadaquemostrar" value="1">


<input type="hidden" id="aplicadescuento" value="<?=$estatus_descuento?>">



<?php foreach ($tphabit as $key ): ?>
    

    <input type="hidden" value="0" id="controltphab<?=$key['Tipohabitacione']['id'] ?>">



<?php endforeach ?>



<script type="text/javascript">


    function aplicardescuento(){
        var dni = $("#Clientedni").val();
        $.ajax({
            url:"/Reserindividualespub/aplicacardescuento",
            type:"post",
            data:{dni:dni},
            success:function(response){

                console.log(response);

                var data = JSON.parse(response);

                if (data.resp == true) {
                    console.log(data.descuentototal)

                    $("#Reserindividualetotal").val(data.descuentototal);
                    alert('Se le ha aplicado el % de descuento indicado para esta reserva. Felicidades!');
                }
            }}



            );
    }

    function prevent(e) {
       e.preventDefault();
    }
    

    function buscadato(){
        var dni = $("#Clientedni").val();
        $.ajax({
            url:"/Reserindividualespub/buscardatos",
            type:"post",
            data:{dni:dni},
            success:function(response){

                var collection = JSON.parse(response);

                console.log(collection);

                if (collection.resp == true) {

                    var estatus_descuento = $("#aplicadescuento").val();


                    if (estatus_descuento == 1) {



                        $("#div_descuento").show();


                    }

                    $("#nombre_cliente_descuento").html(collection.valores[0]['Cliente']['nombre_completo']);
                    $("#Clientenombre_completo") .val(collection.valores[0]['Cliente']['nombre_completo'])
                    $("#Clientepai_id")          .val(collection.valores[0]['Cliente']['pai_id'])
                    $("#Clienteciudad")          .val(collection.valores[0]['Cliente']['ciudad'])
                    $("#Clientedireccion")       .val(collection.valores[0]['Cliente']['direccion'])
                    $("#Clientetelf")            .val(collection.valores[0]['Cliente']['telf'])
                    $("#Clientemovil")           .val(collection.valores[0]['Cliente']['movil'])
                    $("#Clientecorreo")          .val(collection.valores[0]['Cliente']['correo'])
                    $("#Clientepreferencias")    .val(collection.valores[0]['Cliente']['preferencias'])
                    $("#Clientetdc")             .val(collection.valores[0]['Cliente']['tdc'])
                    $("#Clientecodtdc")          .val(collection.valores[0]['Cliente']['codtdc'])
                    

                }

                if (collection.resp == false) {
                    $("#Clientenombre_completo") .val('');
                    $("#Clientepai_id")	         .val('');
                    $("#Clienteciudad")			 .val('');
                    $("#Clientedireccion")		 .val('');
                    $("#Clientetelf")			 .val('');
                    $("#Clientemovil")		     .val('');
                    $("#Clientecorreo")			 .val('');
                    $("#Clientepreferencias")    .val('');
                    $("#Clientetdc")             .val('');
                    $("#Clientecodtdc")          .val('');
                }






            }
        });

    }



    function modalcliente(){
        $("#myModal").modal('show');
    }

    function modalterminos(){
        $("#myModalacepto").modal('show');
    }



    function guardar(){
        $.post('/Clientespub/add',$("#ClienteAddForm").serialize(),function(response){
            console.log(response);
            cargacliente();
        });
    }


    function cargacliente(){
        $("#myModal").modal('hide');

        $.ajax({
            url:"/Reserindividualespub/cliente2",
            type:"post",
            data:{},
            success:function(response){
                //console.log(response);

                $("#div-cliente").html(response);




            }
        });
    }

   


    function acepto(){
        if($('#acepto:checkbox').prop('checked')){
            $('#btnaddform').show();
        }else{
            $('#btnaddform').hide();
            alert('Para reservar debe aceptar los terminos!');

        }
    }









</script>



