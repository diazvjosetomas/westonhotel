<style>
    .tipo_habitacion{
        border-radius: 10px 10px 10px 10px;
        -moz-border-radius: 10px 10px 10px 10px;
        -webkit-border-radius: 10px 10px 10px 10px;
        border: 2px solid #84c400;

        padding: 5px;
        padding-top: 15px;
    }
</style>


<section class="content">
    <div id="google_translate_element"></div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="col-xs-2">
                    </div>
                    <!--<div class="col-xs-10">
                        <h3 class="box-title"><?php echo __('Anular Reservación'); ?></h3>
                    </div>-->
                </div><!-- /.box-header -->
                <div class="box-body">

                    <?php if ($encontrado == 0): ?>
                        <h3>
                            Esta reserva se encuentra anulada o no existe.
                        </h3>
                    <?php endif ?>

                    <?php if ($encontrado ==1): ?>
                        


                    <div class='row'>
                        <div class='col-md-12'>
                            <div style="align-content: center; align-items: center; text-align: center">
                                <h3><strong>Edición de Datos para Reservación Nº: <?=($localizador);  ?></strong></h3>
                            

                                <form method="post" class="form-horizontal" action="/Reserindividualespub/editardatoscliente">
                                <fieldset>

                                <!-- Form Name -->
                                <legend>Editar Datos de <?=$nombre_completo?></legend>

                                <!-- Text input-->
                                <div class="form-group">
                                  <label class="col-md-4 control-label" for="Label">Nombres y Apellidos</label>  
                                  <div class="col-md-5">
                                  <input  name="data[Cliente][nombres]" type="text"  class="form-control input-md" required="" value="<?=$nombre_completo?>">
                                  <span class="help-block">Nombres y Apellidos</span>  
                                  </div>
                                </div>


                                <!-- Text input-->
                                <div class="form-group">
                                  <label class="col-md-4 control-label" for="textinput">Num de Tarjeta</label>  
                                  <div class="col-md-5">
                                  <input name="data[Cliente][tdc]" type="text" placeholder="" class="form-control input-md" value="<?=$tdc?>">
                                  <span class="help-block">Ingresa el número de la TDC</span>  
                                  </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                  <label class="col-md-4 control-label" for="textinput">Codigo de Tarjeta</label>  
                                  <div class="col-md-5">
                                  <input  name="data[Cliente][codtdc]" type="text" placeholder="" class="form-control input-md" value="<?=$cod_tdc?>">
                                  <span class="help-block">Ingresa el codigo de tarjeta que se encuentra en la parte reversa de la tarjeta</span>  
                                  </div>
                                </div>

                                <input type="hidden" name="data[Cliente][id]" value="<?=$id?>">

                                <!-- Button -->
                                <div class="form-group">
                                  <label class="col-md-4 control-label" for="singlebutton"></label>
                                  <div class="col-md-4">
                                    <input type="submit" name="Actualizar" value="Actualizar" class="btn btn-success" >
                                  </div>
                                </div>

                                </fieldset>
                                </form>




                            </div>
                        </div>

                        
                    </div></form>

                    <?php endif ?>


                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<!-- Modal -->


<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>





<input type="hidden" value='1' id='control-th'>
<input type="hidden" id='lastth'>


<input type="hidden" id="ctrladd" value="0">




<? //pr($allclientes) ?>