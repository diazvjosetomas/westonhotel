<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Caja aperturas'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Caja aperturas'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Edit Caja aperturas'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Cajaapertura', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
			
									echo $this->Form->input('id', array('class'=>'form-horizontal'));	
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Cajaaperturacaja_id">Caja</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('caja_id', array('id'=>'Cajaaperturacaja_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Cajaaperturacajaturno_id">Turno</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('cajaturno_id', array('id'=>'Cajaaperturacajaturno_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Cajaaperturauser_id">User</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('user_id', array('id'=>'Cajaaperturauser_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Cajaaperturamonto">Monto</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('monto', array('id'=>'Cajaaperturamonto', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Cajaaperturamontocierre">Monto Cierre</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('montocierre', array('id'=>'Cajaaperturamontocierre', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
								?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Cajaapertura.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Cajaapertura.id'))); ?></li>
				<li><?php echo $this->Html->link(__('List Cajaaperturas'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('List Cajas'), array('controller' => 'cajas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Caja'), array('controller' => 'cajas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cajaturnos'), array('controller' => 'cajaturnos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cajaturno'), array('controller' => 'cajaturnos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		</ul>
	</div>
<?php */ ?>