<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Caja aperturas'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Caja aperturas'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Caja aperturas'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
		<dt><?php echo __('Caja'); ?></dt>
		<dd>
			<?php echo h($cajaapertura['Caja']['denominacion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cajaturno'); ?></dt>
		<dd>
			<?php echo h($cajaapertura['Cajaturno']['denominacion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo h($cajaapertura['User']['username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Monto'); ?></dt>
		<dd>
			<?php echo h($cajaapertura['Cajaapertura']['monto']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha'); ?></dt>
		<dd>
			<?php echo h($cajaapertura['Cajaapertura']['created']); ?>
			&nbsp;
		</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $cajaapertura['Cajaapertura']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Cajaapertura'), array('action' => 'edit', $cajaapertura['Cajaapertura']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Cajaapertura'), array('action' => 'delete', $cajaapertura['Cajaapertura']['id']), array(), __('Are you sure you want to delete # %s?', $cajaapertura['Cajaapertura']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Cajaaperturas'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cajaapertura'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cajas'), array('controller' => 'cajas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Caja'), array('controller' => 'cajas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cajaturnos'), array('controller' => 'cajaturnos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cajaturno'), array('controller' => 'cajaturnos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>