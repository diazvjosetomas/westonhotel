<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Impresiones'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Impresiones'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Impresiones'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
		<dt><?php echo __('Tipo de impresora'); ?></dt>
		<dd>
			<?php echo $this->Html->link($impresione['Tipoimpresora']['denominacion'], array('controller' => 'tipoimpresoras', 'action' => 'view', $impresione['Tipoimpresora']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Puerto'); ?></dt>
		<dd>
			<?php echo h($impresione['Impresione']['puerto']); ?>
			&nbsp;
		</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $impresione['Impresione']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Impresione'), array('action' => 'edit', $impresione['Impresione']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Impresione'), array('action' => 'delete', $impresione['Impresione']['id']), array(), __('Are you sure you want to delete # %s?', $impresione['Impresione']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Impresiones'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Impresione'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipoimpresoras'), array('controller' => 'tipoimpresoras', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipoimpresora'), array('controller' => 'tipoimpresoras', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>