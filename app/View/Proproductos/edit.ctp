<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Productos'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Productos'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Edit Productos'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Proproducto', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
							    <input type="hidden" name="iva" id="Proiva" value="<?= $proivas[0]['Proiva']['denominacion'] ?>">
								<?php
			                        echo $this->Form->input('id', array('class'=>'form-horizontal'));	
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Proproductoprotipo_id">Tipo producto</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('protipo_id', array('id'=>'Proproductoprotipo_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'empty'=>'--Seleccione--'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Proproductocod_producto">Cod producto</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('cod_producto', array('id'=>'Proproductocod_producto', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Proproductodenominacion">Denominación</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('denominacion', array('id'=>'Proproductodenominacion', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Proproductoplan_cuenta">Plan cuenta</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('plan_cuenta', array('id'=>'Proproductoplan_cuenta', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Proproductoprecio_compra">Precio compra</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('precio_compra', array('id'=>'Proproductoprecio_compra', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'onkeyup'=>"fncSumar();"));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Proproductoganancia">Ganancia (%)</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('ganancia', array('id'=>'Proproductoganancia', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'onkeyup'=>"fncSumar();"));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Proproductoprecio_neto">Precio neto</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('precio_neto', array('id'=>'Proproductoprecio_neto', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly'=>true));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Proproductoimpuesto">Impuesto (%)</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('impuesto', array('id'=>'Proproductoimpuesto', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly'=>false, 'onkeyup'=>"fncSumar();"));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Proproductoiva">Monto Iva</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('iva', array('id'=>'Proproductoiva', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly'=>true));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Proproductopvp">P.v.p</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('pvp', array('id'=>'Proproductopvp', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly'=>true));		
									echo '</div>';	
									echo '</div>';


									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Proproductocantidad">Cantidad</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('cantidad', array('id'=>'Proproductocantidad', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly'=>false));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Proproductocantidadminimo">Cantidad minimo</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('cantidad_minimo', array('id'=>'Proproductocantidadminimo', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly'=>false));		
									echo '</div>';	
									echo '</div>';
							?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Proproducto.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Proproducto.id'))); ?></li>
				<li><?php echo $this->Html->link(__('List Proproductos'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('List Protipos'), array('controller' => 'protipos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Protipo'), array('controller' => 'protipos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Promarcas'), array('controller' => 'promarcas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Promarca'), array('controller' => 'promarcas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proinventarios'), array('controller' => 'proinventarios', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proinventario'), array('controller' => 'proinventarios', 'action' => 'add')); ?> </li>
		</ul>
	</div>
<?php */ ?>