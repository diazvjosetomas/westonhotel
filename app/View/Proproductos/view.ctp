<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Productos'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Productos'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Productos'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
		<dt><?php echo __('Tipo producto'); ?></dt>
		<dd>
			<?php echo h($proproducto['Protipo']['denominacion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cod Producto'); ?></dt>
		<dd>
			<?php echo h($proproducto['Proproducto']['cod_producto']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Denominación'); ?></dt>
		<dd>
			<?php echo h($proproducto['Proproducto']['denominacion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Plan Cuenta'); ?></dt>
		<dd>
			<?php echo h($proproducto['Proproducto']['plan_cuenta']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Precio Compra'); ?></dt>
		<dd>
			<?php echo h($proproducto['Proproducto']['precio_compra']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ganancia (%)'); ?></dt>
		<dd>
			<?php echo h($proproducto['Proproducto']['ganancia']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Precio Neto'); ?></dt>
		<dd>
			<?php echo h($proproducto['Proproducto']['precio_neto']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Impuesto (%)); ?></dt>
		<dd>
			<?php echo h($proproducto['Proproducto']['impuesto']." %"); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Monto Iva'); ?></dt>
		<dd>
			<?php echo h($proproducto['Proproducto']['iva']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('P.v.p'); ?></dt>
		<dd>
			<?php echo h($proproducto['Proproducto']['pvp']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cantidad'); ?></dt>
		<dd>
			<?php echo h($proproducto['Proproducto']['cantidad']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cantidad minima'); ?></dt>
		<dd>
			<?php echo h($proproducto['Proproducto']['cantidad_minimo']); ?>
			&nbsp;
		</dd>
		</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $proproducto['Proproducto']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Proproducto'), array('action' => 'edit', $proproducto['Proproducto']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Proproducto'), array('action' => 'delete', $proproducto['Proproducto']['id']), array(), __('Are you sure you want to delete # %s?', $proproducto['Proproducto']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Proproductos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proproducto'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Protipos'), array('controller' => 'protipos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Protipo'), array('controller' => 'protipos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Promarcas'), array('controller' => 'promarcas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Promarca'), array('controller' => 'promarcas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proinventarios'), array('controller' => 'proinventarios', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proinventario'), array('controller' => 'proinventarios', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Proinventarios'); ?></h3>
	<?php if (!empty($proproducto['Proinventario'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Protipo Id'); ?></th>
		<th><?php echo __('Promarca Id'); ?></th>
		<th><?php echo __('Proproducto Id'); ?></th>
		<th><?php echo __('Cantidad'); ?></th>
		<th><?php echo __('Cantidadminima'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($proproducto['Proinventario'] as $proinventario): ?>
		<tr>
			<td><?php echo $proinventario['id']; ?></td>
			<td><?php echo $proinventario['protipo_id']; ?></td>
			<td><?php echo $proinventario['promarca_id']; ?></td>
			<td><?php echo $proinventario['proproducto_id']; ?></td>
			<td><?php echo $proinventario['cantidad']; ?></td>
			<td><?php echo $proinventario['cantidadminima']; ?></td>
			<td><?php echo $proinventario['created']; ?></td>
			<td><?php echo $proinventario['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'proinventarios', 'action' => 'view', $proinventario['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'proinventarios', 'action' => 'edit', $proinventario['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'proinventarios', 'action' => 'delete', $proinventario['id']), array(), __('Are you sure you want to delete # %s?', $proinventario['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Proinventario'), array('controller' => 'proinventarios', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<?php */ ?>