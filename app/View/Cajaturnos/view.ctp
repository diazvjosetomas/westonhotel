<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Caja turnos'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Caja turnos'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Caja turnos'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								
		<dt><?php echo __('Denominación'); ?></dt>
		<dd>
			<?php echo h($cajaturno['Cajaturno']['denominacion']); ?>
			&nbsp;
		</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $cajaturno['Cajaturno']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Cajaturno'), array('action' => 'edit', $cajaturno['Cajaturno']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Cajaturno'), array('action' => 'delete', $cajaturno['Cajaturno']['id']), array(), __('Are you sure you want to delete # %s?', $cajaturno['Cajaturno']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Cajaturnos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cajaturno'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cajaacierreturnos'), array('controller' => 'cajaacierreturnos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cajaacierreturno'), array('controller' => 'cajaacierreturnos', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Cajaacierreturnos'); ?></h3>
	<?php if (!empty($cajaturno['Cajaacierreturno'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Caja Id'); ?></th>
		<th><?php echo __('Cajaturno Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Monto'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($cajaturno['Cajaacierreturno'] as $cajaacierreturno): ?>
		<tr>
			<td><?php echo $cajaacierreturno['id']; ?></td>
			<td><?php echo $cajaacierreturno['caja_id']; ?></td>
			<td><?php echo $cajaacierreturno['cajaturno_id']; ?></td>
			<td><?php echo $cajaacierreturno['user_id']; ?></td>
			<td><?php echo $cajaacierreturno['monto']; ?></td>
			<td><?php echo $cajaacierreturno['created']; ?></td>
			<td><?php echo $cajaacierreturno['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'cajaacierreturnos', 'action' => 'view', $cajaacierreturno['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'cajaacierreturnos', 'action' => 'edit', $cajaacierreturno['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'cajaacierreturnos', 'action' => 'delete', $cajaacierreturno['id']), array(), __('Are you sure you want to delete # %s?', $cajaacierreturno['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Cajaacierreturno'), array('controller' => 'cajaacierreturnos', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<?php */ ?>