<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Impuesto'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Impuesto'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Impuesto'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
						<dt><?php echo __('Denominación'); ?></dt>
						<dd>
							<?php echo h($proiva['Proiva']['denominacion']." %"); ?>
							&nbsp;
						</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $proiva['Proiva']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Proiva'), array('action' => 'edit', $proiva['Proiva']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Proiva'), array('action' => 'delete', $proiva['Proiva']['id']), array(), __('Are you sure you want to delete # %s?', $proiva['Proiva']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Proivas'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proiva'), array('action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>