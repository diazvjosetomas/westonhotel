<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Carga Stock'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Carga Stock'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Carga Stock'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
							<dt><?php echo __('Tipo Almacen'); ?></dt>
							<dd>
								<?php echo h($proinventario['Protipopiso']['denominacion']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Almacen'); ?></dt>
							<dd>
								<?php echo h($proinventario['Proalmacene']['denominacion']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Tipo Producto'); ?></dt>
							<dd>
								<?php echo h($proinventario['Protipo']['denominacion']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Marca'); ?></dt>
							<dd>
								<?php echo h($proinventario['Promarca']['denominacion']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Producto'); ?></dt>
							<dd>
								<?php echo h($proinventario['Proproducto']['denominacion']); ?>
								&nbsp;
							</dd>
							
							<dt><?php echo __('Cantidad'); ?></dt>
							<dd>
								<?php echo h($proinventario['Proinventario']['cantidad']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Cantidad minima'); ?></dt>
							<dd>
								<?php echo h($proinventario['Proinventario']['cantidadminima']); ?>
								&nbsp;
							</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $proinventario['Proinventario']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Proinventario'), array('action' => 'edit', $proinventario['Proinventario']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Proinventario'), array('action' => 'delete', $proinventario['Proinventario']['id']), array(), __('Are you sure you want to delete # %s?', $proinventario['Proinventario']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Proinventarios'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proinventario'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Protipos'), array('controller' => 'protipos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Protipo'), array('controller' => 'protipos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Promarcas'), array('controller' => 'promarcas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Promarca'), array('controller' => 'promarcas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proproductos'), array('controller' => 'proproductos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proproducto'), array('controller' => 'proproductos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Protipopisos'), array('controller' => 'protipopisos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Protipopiso'), array('controller' => 'protipopisos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proalmacenes'), array('controller' => 'proalmacenes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proalmacene'), array('controller' => 'proalmacenes', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>