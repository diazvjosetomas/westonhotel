<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Carga Stock'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Carga Stock'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Carga Stock'); ?> Registrados</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                    <p><?php echo $this->Html->link(__('Crear Nuevo'), array('action' => 'add'),  array('class'=>'btn btn-primary')); ?></p>

							<table id="data" class="table table-striped table-bordered" width="100%" cellspacing="0">
							<thead>
							<tr>
															<th><?php echo __('Tipo Almacen'); ?></th>
															<th><?php echo __('Almacen'); ?></th>
															<th><?php echo __('Producto'); ?></th>
															<th><?php echo __('Cantidad'); ?></th>
															<th><?php echo __('Cantidad minima'); ?></th>
															<th class="actions"><?php echo __('Acción'); ?></th>
							</tr>
							</thead>
							<tbody>
							<?php foreach ($proinventarios as $proinventario): ?>
	<tr>
	    <td><?php echo h($proinventario['Protipopiso']['denominacion']); ?></td>
		<td><?php echo h($proinventario['Proalmacene']['denominacion']); ?></td>
		<td><?php echo h($proinventario['Proproducto']['denominacion']); ?></td>
		<td><?php echo h($proinventario['Proinventario']['cantidad']); ?>&nbsp;</td>
		<td><?php echo h($proinventario['Proinventario']['cantidadminima']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $proinventario['Proinventario']['id']), array('class'=>'')); ?>
			| <?php echo $this->Html->link(__('Ver'), array('action' => 'view', $proinventario['Proinventario']['id']),   array('class'=>'')); ?>
			| <?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $proinventario['Proinventario']['id']), array('class'=>'', 'confirm'=>__('Esta seguro que desea eliminar el registro # %s?', $proinventario['Proinventario']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
							</tbody>
							</table>
						</div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->


<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Proinventario'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Protipos'), array('controller' => 'protipos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Protipo'), array('controller' => 'protipos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Promarcas'), array('controller' => 'promarcas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Promarca'), array('controller' => 'promarcas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proproductos'), array('controller' => 'proproductos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proproducto'), array('controller' => 'proproductos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Protipopisos'), array('controller' => 'protipopisos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Protipopiso'), array('controller' => 'protipopisos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proalmacenes'), array('controller' => 'proalmacenes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proalmacene'), array('controller' => 'proalmacenes', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?><script type="text/javascript">
	//$(document).ready(function() {
	    $('#data').DataTable( {
	    	dom: 'Bfrtlip',
	        buttons: [
	            'copy', 'csv', 'excel', 'pdf', 'print'
	        ],
	        "language": 
	        {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
	    } );
	//} );
</script>
