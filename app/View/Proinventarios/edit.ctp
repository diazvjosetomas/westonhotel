<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Carga Stock'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Carga Stock'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Edit Carga Stock'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Proinventario', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
			
										echo $this->Form->input('id', array('class'=>'form-horizontal'));	
										echo'<div class="form-group">';	
										echo'<label class="control-label col-md-2" for="Proinventarioprotipopiso_id">Tipo Almacen</label>';		
										echo'<div class="col-md-9">';			
										echo $this->Form->input('protipopiso_id', array('id'=>'Proinventarioprotipopiso_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', "onChange"=>"selectTagRemote('".$this->Html->url('/Proinventarios/almacen')."', 'div-almacen', this.value);", 'empty'=>'--Seleccione--'));		
										echo '</div>';	
										echo '</div>';
											
										echo'<div class="form-group">';	
										echo'<label class="control-label col-md-2" for="Proinventarioproalmacene_id">Almacen</label>';		
										echo'<div class="col-md-9" id="div-almacen">';			
										echo $this->Form->input('proalmacene_id', array('id'=>'Proinventarioproalmacene_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
										echo '</div>';	
										echo '</div>';

										echo'<div class="form-group">';	
										echo'<label class="control-label col-md-2" for="Proinventarioprotipo_id">Tipo producto</label>';		
										echo'<div class="col-md-9">';			
										echo $this->Form->input('protipo_id', array('id'=>'Proinventarioprotipo_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', "onChange"=>"selectTagRemote('".$this->Html->url('/Proinventarios/tipoproducto')."', 'div-tipoproducto', this.value);", 'empty'=>'--Seleccione--'));		
										echo '</div>';	
										echo '</div>';
											
										echo'<div class="form-group">';	
										echo'<label class="control-label col-md-2" for="Proinventariopromarca_id">Marca</label>';		
										echo'<div class="col-md-9" id="div-tipoproducto">';			
										echo $this->Form->input('promarca_id', array('id'=>'Proinventariopromarca_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', "onChange"=>"selectTagRemote('".$this->Html->url('/Proinventarios/producto/'.$this->request->data["Proinventario"]["protipo_id"])."', 'div-producto', this.value);", 'empty'=>'--Seleccione--'));		
										echo '</div>';	
										echo '</div>';
											
										echo'<div class="form-group">';	
										echo'<label class="control-label col-md-2" for="Proinventarioproproducto_id">Producto</label>';		
										echo'<div class="col-md-9" id="div-producto">';			
										echo $this->Form->input('proproducto_id', array('id'=>'Proinventarioproproducto_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
										echo '</div>';	
										echo '</div>';
											
										echo'<div class="form-group">';	
										echo'<label class="control-label col-md-2" for="Proinventariocantidad">Cantidad</label>';		
										echo'<div class="col-md-9">';			
										echo $this->Form->input('cantidad', array('id'=>'Proinventariocantidad', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
										echo '</div>';	
										echo '</div>';
											
										echo'<div class="form-group">';	
										echo'<label class="control-label col-md-2" for="Proinventariocantidadminima">Cantidad minima</label>';		
										echo'<div class="col-md-9">';			
										echo $this->Form->input('cantidadminima', array('id'=>'Proinventariocantidadminima', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
										echo '</div>';	
										echo '</div>';
								?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Proinventario.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Proinventario.id'))); ?></li>
				<li><?php echo $this->Html->link(__('List Proinventarios'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('List Protipos'), array('controller' => 'protipos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Protipo'), array('controller' => 'protipos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Promarcas'), array('controller' => 'promarcas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Promarca'), array('controller' => 'promarcas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proproductos'), array('controller' => 'proproductos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proproducto'), array('controller' => 'proproductos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Protipopisos'), array('controller' => 'protipopisos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Protipopiso'), array('controller' => 'protipopisos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Proalmacenes'), array('controller' => 'proalmacenes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proalmacene'), array('controller' => 'proalmacenes', 'action' => 'add')); ?> </li>
		</ul>
	</div>
<?php */ ?>