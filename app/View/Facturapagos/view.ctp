<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Facturapagos'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Facturapagos'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Facturapagos'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($facturapago['Facturapago']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Factura'); ?></dt>
		<dd>
			<?php echo $this->Html->link($facturapago['Factura']['numero'], array('controller' => 'facturas', 'action' => 'view', $facturapago['Factura']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Facturatipopago'); ?></dt>
		<dd>
			<?php echo $this->Html->link($facturapago['Facturatipopago']['denominacion'], array('controller' => 'facturatipopagos', 'action' => 'view', $facturapago['Facturatipopago']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha Pago'); ?></dt>
		<dd>
			<?php echo h($facturapago['Facturapago']['fecha_pago']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Numero Pago'); ?></dt>
		<dd>
			<?php echo h($facturapago['Facturapago']['numero_pago']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pago'); ?></dt>
		<dd>
			<?php echo h($facturapago['Facturapago']['pago']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($facturapago['Facturapago']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($facturapago['Facturapago']['modified']); ?>
			&nbsp;
		</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $facturapago['Facturapago']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Facturapago'), array('action' => 'edit', $facturapago['Facturapago']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Facturapago'), array('action' => 'delete', $facturapago['Facturapago']['id']), array(), __('Are you sure you want to delete # %s?', $facturapago['Facturapago']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Facturapagos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Facturapago'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Facturas'), array('controller' => 'facturas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Factura'), array('controller' => 'facturas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Facturatipopagos'), array('controller' => 'facturatipopagos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Facturatipopago'), array('controller' => 'facturatipopagos', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>