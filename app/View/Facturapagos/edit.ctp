<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Facturapagos'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Facturapagos'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Edit Facturapago'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Facturapago', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
			
echo $this->Form->input('id', array('class'=>'form-horizontal'));	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Facturapagofactura_id">factura_id</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('factura_id', array('id'=>'Facturapagofactura_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Facturapagofacturatipopago_id">facturatipopago_id</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('facturatipopago_id', array('id'=>'Facturapagofacturatipopago_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Facturapagofecha_pago">fecha_pago</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('fecha_pago', array('id'=>'Facturapagofecha_pago', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Facturapagonumero_pago">numero_pago</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('numero_pago', array('id'=>'Facturapagonumero_pago', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Facturapagopago">pago</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('pago', array('id'=>'Facturapagopago', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Facturapago.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Facturapago.id'))); ?></li>
				<li><?php echo $this->Html->link(__('List Facturapagos'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('List Facturas'), array('controller' => 'facturas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Factura'), array('controller' => 'facturas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Facturatipopagos'), array('controller' => 'facturatipopagos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Facturatipopago'), array('controller' => 'facturatipopagos', 'action' => 'add')); ?> </li>
		</ul>
	</div>
<?php */ ?>