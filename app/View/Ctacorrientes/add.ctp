<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Cta corriente'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Cta corriente'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Add Cta corriente'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Ctacorriente', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
	
										echo'<div class="form-group">';	
										echo'<label class="control-label col-md-2" for="Ctacorrientetipocliente_id">Tipo cliente</label>';		
										echo'<div class="col-md-9">';			
										echo $this->Form->input('tipocliente_id', array('id'=>'Ctacorrientetipocliente_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', "onChange"=>"selectTagRemote('".$this->Html->url('/Ctacorrientes/sub_cliente')."', 'div-subcliente', this.value);", 'empty'=>'--Seleccione--')); 
										echo '</div>';	
										echo '</div>';
											
										echo'<div class="form-group">';	
										echo'<label class="control-label col-md-2" for="Ctacorrientetipoclientesub_id">Tipo subcliente</label>';		
										echo'<div class="col-md-9" id="div-subcliente">';			
										echo $this->Form->input('tipoclientesub_id', array('id'=>'Ctacorrientetipoclientesub_id', 'div'=>false, 'label'=>false, 'class'=>'form-control','options'=>array()));
										echo '</div>';	
										echo '</div>';

										echo'<div class="form-group">';	
										echo'<label class="control-label col-md-2" for="Ctacorrientecliente_id">Cliente</label>';		
										echo'<div class="col-md-9" id="div-cliente">';			
										echo $this->Form->input('cliente_id', array('id'=>'Ctacorrientecliente_id', 'div'=>false, 'label'=>false, 'class'=>'form-control','options'=>array()));
										echo '</div>';	
										echo '</div>';
											
										echo'<div class="form-group">';	
										echo'<label class="control-label col-md-2" for="Ctacorrientedescripcion">Descripción</label>';		
										echo'<div class="col-md-9">';			
										echo $this->Form->input('descripcion', array('id'=>'Ctacorrientedescripcion', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
										echo '</div>';	
										echo '</div>';
											
										echo'<div class="form-group">';	
										echo'<label class="control-label col-md-2" for="Ctacorrientelimitecredito">limite de credito</label>';		
										echo'<div class="col-md-9">';			
										echo $this->Form->input('limitecredito', array('id'=>'Ctacorrientelimitecredito', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
										echo '</div>';	
										echo '</div>';
								?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Html->link(__('List Ctacorrientes'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('List Tipoclientes'), array('controller' => 'tipoclientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipocliente'), array('controller' => 'tipoclientes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipoclientesubs'), array('controller' => 'tipoclientesubs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipoclientesub'), array('controller' => 'tipoclientesubs', 'action' => 'add')); ?> </li>
		</ul>
	</div>
<?php */ ?>