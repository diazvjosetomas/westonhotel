<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Cta corriente'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Cta corriente'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Cta corriente'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($ctacorriente['Ctacorriente']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tipo cliente'); ?></dt>
		<dd>
			<?php echo h($ctacorriente['Tipocliente']['denominacion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tipo subcliente'); ?></dt>
		<dd>
			<?php echo h($ctacorriente['Tipoclientesub']['nombre']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cliente'); ?></dt>
		<dd>
			<?php echo h($ctacorriente['Cliente']['dni']." - ".$ctacorriente['Cliente']['nombre_completo']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Descripción'); ?></dt>
		<dd>
			<?php echo h($ctacorriente['Ctacorriente']['descripcion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Limite de credito'); ?></dt>
		<dd>
			<?php echo h($ctacorriente['Ctacorriente']['limitecredito']); ?>
			&nbsp;
		</dd>

						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $ctacorriente['Ctacorriente']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ctacorriente'), array('action' => 'edit', $ctacorriente['Ctacorriente']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ctacorriente'), array('action' => 'delete', $ctacorriente['Ctacorriente']['id']), array(), __('Are you sure you want to delete # %s?', $ctacorriente['Ctacorriente']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Ctacorrientes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ctacorriente'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipoclientes'), array('controller' => 'tipoclientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipocliente'), array('controller' => 'tipoclientes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipoclientesubs'), array('controller' => 'tipoclientesubs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipoclientesub'), array('controller' => 'tipoclientesubs', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>