<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Facturas'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Facturas'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Listado de Facturas'); ?> Registradas</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                  

							<table id="data" class="table table-striped table-bordered" width="100%" cellspacing="0">
							<thead>
							<tr>
								
								<th><?php echo __('Número de Factura'); ?></th>
								<th><?php echo __('Cantidad de Noches'); ?></th>
								<th><?php echo __('Forma de Pago'); ?></th>
								<th><?php echo __('Tarifa Diaria'); ?></th>
								<th><?php echo __('Valor Real'); ?></th>
								

							</tr>
							</thead>
							<tbody>
							<?php foreach ($facturas as $factura): ?>
							<tr>
								
								<td><?php echo h($factura['Factura']['numero']); ?>&nbsp;</td>
								<td><?=cantidadnoches($factura['Reserindividuale']['fecha_entrada'], $factura['Reserindividuale']['fecha_salida'])?>&nbsp;</td>
								<td><?=$factura['Facturatipopago']['denominacion'] ?></td>
								<td><?php echo h($factura['Factura']['total']/cantidadnoches($factura['Reserindividuale']['fecha_entrada'], $factura['Reserindividuale']['fecha_salida'])); ?>&nbsp;</td>
								<td>?</td>

							</tr>
							<?php endforeach; ?>
							</tbody>
							</table>
						</div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<input type="hidden" id="id_impresion" >

<script type="text/javascript">
		function printFactura(id){

			$("#modalFacturacion").modal('show');
			$("#id_impresion").val(id);

		}
	//$(document).ready(function() {
	    $('#data').DataTable( {
	    	dom: 'Bfrtlip',
	        buttons: [
	            'copy', 'csv', 'excel', 'pdf', 'print'
	        ],
	        "language": 
	        {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
	    } );
	//} );
</script>


