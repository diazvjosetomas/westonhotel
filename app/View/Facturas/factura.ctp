<?php
	$total = 0;
?>
<?php
//pr($reserindividuales);
$c = 0;
foreach ($reserindividuales as $reserindividuale){
	if($reserindividuale['Reserindividuale']['resermultiple_id']=='0'){
?>
<div class="row">
<label class="control-label col-md-2" for="Consumonum_factura">Reservación Individuales Deuda</label>
<div class="col-md-9 col-sm-9 col-xs-12">
    <div class="box box-solid box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Habitación</h3>
            <div class="box-tools pull-right">
            </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            <div id="productos-container">
                <div id="productoscount">
                    <div class="form-group pro-con" id="producto_<?= $c ?>">
                        <label class="control-label col-md-1">N° Reserva</label>
                        <div class="col-md-2">
                            <input class="form-control pro-<?= $c ?>-ext text-box single-line"  id="Productos_<?= $c ?>__Cantidad" name="data[Consumo][Productos][<?= $c ?>][cant]" required="required" type="text" value="<?= $reserindividuale['Reserindividuale']['id'] ?>"  readonly=true/>
                        </div>
                        <label class="control-label col-md-1">Habitación</label>
                        <div class="col-md-2">
							<input class="form-control pro-<?= $c ?>-ext text-box single-line"  id="Productos_<?= $c ?>__Cantidad" name="data[Consumo][Productos][<?= $c ?>][cant]" required="required" type="text" value="<?= $reserindividuale['Habitacione']['numhabitacion'] ?>"  readonly=true/>
                        </div>
                        <label class="control-label col-md-1">Total</label>
                        <div class="col-md-2">
                            <input class="form-control pro-<?= $c ?>-ext text-box single-line"  id="Productos_<?= $c ?>__Precio"   name="data[Consumo][Productos][<?= $c ?>][pre]"  required="required" type="text" value="<?= $reserindividuale['Reserindividuale']['total'] ?>" readonly=true />
                        </div>
                        <label class="control-label col-md-1">Pagado</label>
                        <div class="col-md-2">
                            <input class="form-control pro-<?= $c ?>-ext text-box single-line" id="Productos_<?= $c ?>__Total" name="data[Consumo][Productos][<?= $c ?>][total]"  required="required"  type="text" value="<?= $reserindividuale['Reserindividuale']['pagado'] ?>" readonly=true />
                        </div>
                        <div class="col-md-1">
                        	<?php $total = $total + ($reserindividuale['Reserindividuale']['total']-$reserindividuale['Reserindividuale']['pagado']); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php
     $c++;
	}
}
?>
<?php
//pr($reserindividuales);
$c = 0;
foreach ($resermultiples as $resermultiple){
?>
<div class="row">
<label class="control-label col-md-2" for="Consumonum_factura">Reservación Multiples Deuda</label>
<div class="col-md-9 col-sm-9 col-xs-12">
    <div class="box box-solid box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Habitación</h3>
            <div class="box-tools pull-right">
            </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            <div id="productos-container">
                <div id="productoscount">
                    <div class="form-group pro-con" id="producto_<?= $c ?>">
                        <label class="control-label col-md-1">N° Reserva</label>
                        <div class="col-md-2">
                            <input class="form-control pro-<?= $c ?>-ext text-box single-line"  id="Productos_<?= $c ?>__Cantidad" name="data[Consumo][Productos][<?= $c ?>][cant]" required="required" type="text" value="<?= $resermultiple['Resermultiple']['id'] ?>"  readonly=true/>
                        </div>
                        <label class="control-label col-md-1">Cantidad Habitación</label>
                        <div class="col-md-2">
                            <input class="form-control pro-<?= $c ?>-ext text-box single-line"  id="Productos_<?= $c ?>__Cantidad" name="data[Consumo][Productos][<?= $c ?>][cant]" required="required" type="text" value="<?= $resermultiple['Resermultiple']['cantidad'] ?>"  readonly=true/>
                        </div>
                        <label class="control-label col-md-1">Total</label>
                        <div class="col-md-2">
                            <input class="form-control pro-<?= $c ?>-ext text-box single-line"  id="Productos_<?= $c ?>__Precio"   name="data[Consumo][Productos][<?= $c ?>][pre]"  required="required" type="text" value="<?= $resermultiple['Resermultiple']['total'] ?>" readonly=true />
                        </div>
                        <label class="control-label col-md-1">Pagado</label>
                        <div class="col-md-2">
                            <input class="form-control pro-<?= $c ?>-ext text-box single-line" id="Productos_<?= $c ?>__Total" name="data[Consumo][Productos][<?= $c ?>][total]"  required="required"  type="text" value="<?= $resermultiple['Resermultiple']['pagado'] ?>" readonly=true />
                        </div>
                        <div class="col-md-1">
                            <?php $total = $total + ($resermultiple['Resermultiple']['total']-$resermultiple['Resermultiple']['pagado']); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php
     $c++;
}
?>
<div class="row">
<label class="control-label col-md-2" for="Consumonum_factura">Consumo Resto</label>
<div class="col-md-9 col-sm-9 col-xs-12">
    <div class="box box-solid box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Productos</h3>
            <div class="box-tools pull-right">
            </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            <div id="productos-container">
            <?php $c = 0;
            if(isset($consumos[0]['Consumoproducto'])){
                foreach($consumos[0]['Consumoproducto'] as $consumoproducto){ ?>
                <div id="productoscount">
                    <div class="form-group pro-con" id="producto_<?= $c ?>">
                        <label class="control-label col-md-1">Producto</label>
                        <div class="col-md-2">
                            <select disabled="true" class="form-control tel-<?= $c ?>" id="Productos_<?= $c ?>__Pro" name="data[Consumo][Productos][<?= $c ?>][pro]" required="required" >
							<option value="">--Seleccione--</option>
							<?php
	                             foreach($proproductos as $proproducto){
	                             	if($consumoproducto['proproducto_id']==$proproducto['Proproductos']['id']){
	                             		echo "  '<option value=\"".$proproducto['Proproductos']['id']."\" selected>".$proproducto['Proproductos']['denominacion']."</option>'+   ";
	                             	}else{
                                        echo "  '<option value=\"".$proproducto['Proproductos']['id']."\">".$proproducto['Proproductos']['denominacion']."</option>'+   ";
	                             	}
	                             }
                             ?>
							</select>
                        </div>
                        <label class="control-label col-md-1">Cantidad</label>
                        <div class="col-md-2">
							<input class="form-control pro-<?= $c ?>-ext text-box single-line"  id="Productos_<?= $c ?>__Cantidad" name="data[Consumo][Productos][<?= $c ?>][cant]" required="required" type="text" value="<?= $consumoproducto['cantidad'] ?>"  readonly=true/>
                        </div>
                        <label class="control-label col-md-1">Pre. U.</label>
                        <div class="col-md-2">
                            <input class="form-control pro-<?= $c ?>-ext text-box single-line"  id="Productos_<?= $c ?>__Precio"   name="data[Consumo][Productos][<?= $c ?>][pre]"  required="required" type="text" value="<?= $consumoproducto['precio'] ?>" readonly=true />
                        </div>
                        <label class="control-label col-md-1">Total</label>
                        <div class="col-md-2">
                            <input class="form-control pro-<?= $c ?>-ext text-box single-line" id="Productos_<?= $c ?>__Total" name="data[Consumo][Productos][<?= $c ?>][total]"  required="required"  type="text" value="<?= $consumoproducto['total'] ?>" readonly=true />
                        </div>
                        <div class="col-md-1">
                        	<?php $total = $total + $consumoproducto['total']; ?>
                        </div>
                    </div>
                </div>
            <?php 
            $c++;
                }
            } 
            ?>
            </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
	$('#Facturatotal').val('<?= $total ?>');
</script>


