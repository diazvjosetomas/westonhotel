<?php
class fpdfview extends FPDF{
	function Footer(){}
	function Header(){

		
	}
}
$fpdf = new fpdfview('P','mm','A4');
$fpdf->AddPage();



$fpdf->Ln(5);
$fpdf->SetFont('Arial','B',22);
$fpdf->Cell(0,5,"Factura ",'',1,'C');

$fpdf->Ln(2);

$fpdf->Cell(0,5,"Detalles de la Reserva ",'',1,'C');


$fpdf->Ln(5);
$fpdf->SetFont('Arial','B',8);
$fpdf->Cell(45,5,'Datos de Cliente: '.ucwords(strtolower($datafactura[0]['Cliente']['nombre_completo'])),'',0,'C');
$fpdf->Ln(4);
$fpdf->SetFont('Arial','B',8);
$fpdf->Cell(38,5,'Fecha de Entrada: '.conviertefecha($detallereserva[0]['Reserindividuale']['fecha_entrada']),'',0,'C');
$fpdf->Ln(4);
$fpdf->SetFont('Arial','B',8);
$fpdf->Cell(36,5,'Fecha de Salida: '.conviertefecha($detallereserva[0]['Reserindividuale']['fecha_salida']),'',0,'C');
$fpdf->Ln(4);
$fpdf->SetFont('Arial','B',8);
$fpdf->Cell(45,5,'Observaciones: '.$detallereserva[0]['Reserindividuale']['obseraciones'],'',0,'C');


$fpdf->Ln(10);
$fpdf->SetFont('Arial','',6);
$fpdf->Cell(15,5,"Num de Factura",'LTB',0,'C');
$fpdf->Cell(20,5,"Descripcion",'TB',0,'C');
$fpdf->Cell(75,5,"Precio Neto",'TB',0,'C');
$fpdf->Cell(30,5,"IVA",'TB',0,'C');
$fpdf->Cell(30,5,"Subtotal",'TB',0,'C');
$fpdf->Cell(0,5,"Total",'TBR',1,'C');

$fpdf->Ln(2);
foreach ($datafactura as $key) {
	$fpdf->Cell(15,5,$key['Factura']['numero'],'',0,'C');
	$fpdf->Cell(20,5,$key['Factura']['concepto'],'',0,'C');
	$fpdf->Cell(75,5,number_format($key['Factura']['total'],'2',',','.'),'',0,'C');
	$fpdf->Cell(30,5,number_format( $key['Factura']['total'] * 0.12  ,'2',',','.'),'',0,'C');
	$fpdf->Cell(30,5,number_format( $key['Factura']['total'] - ($key['Factura']['total'] * 0.12) ,'2',',','.'),'',0,'C');
	$fpdf->Cell(0,5,number_format($key['Factura']['total'],'2',',','.'),'',1,'C');

}

$fpdf->Output('D','');
?>

