<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Pagos'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Pagos'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Pagos'); ?> Pendientes</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                    <p>
                    <?php 
                    	//echo $this->Html->link(__('Crear Nuevo'), array('action' => 'add'),  array('class'=>'btn btn-primary')); 
                    ?>
                    </p>

							<table id="data" class="table table-striped table-bordered" width="100%" cellspacing="0">
							<thead>
							<tr>
								<th><?php echo __('Número'); ?></th>
								<th><?php echo __('Fecha'); ?></th>
								<th><?php echo __('Cliente'); ?></th>
								<th><?php echo __('Total'); ?></th>
								<th><?php echo __('Pagado'); ?></th>
								<th><?php echo __('Deuda'); ?></th>
								<th class="actions"><?php echo __('Acción'); ?></th>
							</tr>
							</thead>
							<tbody>
							<?php foreach ($facturas as $factura): ?>
							<tr>


							<?php 
								$pagado = false;
								if ($factura['Factura']['total'] <= $factura['Factura']['pagado']) {
									$pagado = true;
								}

							 ?>

								<td><?php echo h($factura['Factura']['numero']); ?>&nbsp;</td>
								<td><?php echo h( formatdmy($factura['Factura']['fecha'])); ?>&nbsp;</td>
								<td><?php echo h($factura['Cliente']['nombre_completo']); ?></td>
								<td><?php echo h($factura['Factura']['total']); ?>&nbsp;</td>
								<td><?php echo h($factura['Factura']['pagado']); ?>&nbsp;</td>
								<td><?php echo h($factura['Factura']['total']-$factura['Factura']['pagado']); ?>&nbsp;</td>
								<td class="actions">
									<?php 
									//echo $this->Html->link(__('Editar'), array('action' => 'edit', $factura['Factura']['id']), array('class'=>'')); 
									?>
									<?php
									if (!$pagado) {
										echo $this->Html->link(__('Pagar'), array('action' => 'pagar', $factura['Factura']['id']),   array('class'=>''));
									}else{
										echo "<a onclick='printFactura(".$factura['Factura']['id'].")' class='btn btn-success'>Imprimir Factura</a>";
									}


									 ?>
								</td>
							</tr>
							<?php endforeach; ?>
							</tbody>
							</table>
						</div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->

<!-- Modal -->
<div id="modalFacturacion" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detalles de Facturación</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



<script type="text/javascript">

		function printFactura(){

			$("#modalFacturacion").modal('show');

		}

	//$(document).ready(function() {
	    $('#data').DataTable( {
	    	dom: 'Bfrtlip',
	        buttons: [
	            'copy', 'csv', 'excel', 'pdf', 'print'
	        ],
	        "language": 
	        {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
	    } );
	//} );
</script>
