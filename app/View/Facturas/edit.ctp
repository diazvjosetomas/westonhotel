<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Facturas'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Facturas'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Edit Factura'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Factura', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
			
echo $this->Form->input('id', array('class'=>'form-horizontal'));	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Facturanumero">numero</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('numero', array('id'=>'Facturanumero', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Facturafecha">fecha</label>';		
echo'<div class="col-md-4">';			
echo $this->Form->input('fecha1', array('id'=>'Facturafecha', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly' => true, 'value' => $this->request->data['Factura']['fecha1']));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Facturatipocliente_id">tipocliente_id</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('tipocliente_id', array('id'=>'Facturatipocliente_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Facturatipoclientesub_id">tipoclientesub_id</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('tipoclientesub_id', array('id'=>'Facturatipoclientesub_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Facturacliente_id">cliente_id</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('cliente_id', array('id'=>'Facturacliente_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Facturaconcepto">concepto</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('concepto', array('id'=>'Facturaconcepto', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Facturatotal">total</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('total', array('id'=>'Facturatotal', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Facturapagado">pagado</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('pagado', array('id'=>'Facturapagado', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Factura.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Factura.id'))); ?></li>
				<li><?php echo $this->Html->link(__('List Facturas'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('List Tipoclientes'), array('controller' => 'tipoclientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipocliente'), array('controller' => 'tipoclientes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipoclientesubs'), array('controller' => 'tipoclientesubs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipoclientesub'), array('controller' => 'tipoclientesubs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Clientes'), array('controller' => 'clientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cliente'), array('controller' => 'clientes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Facturadetalles'), array('controller' => 'facturadetalles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Facturadetalle'), array('controller' => 'facturadetalles', 'action' => 'add')); ?> </li>
		</ul>
	</div>
<?php */ ?>


<script type="text/javascript">
	$(function(){
		$('#Facturafecha').datepicker();
	});
</script>