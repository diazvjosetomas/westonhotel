<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Facturas'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Facturas'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Add Factura'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Factura', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>

								<?php
									echo $this->Form->input('reserindividuale_id', array('type'=>'hidden', 'value'=>$id_reserva));	

									if($number != 0){
										echo'<div class="form-group">';	
										echo'<label class="control-label col-md-2" for="Facturanumero">Número</label>';		
										echo'<div class="col-md-9">';			
										echo $this->Form->input('numero', array('id'=>'Facturanumero', 'div'=>false, 'label'=>false, 'readonly' => 'readonly', 'value' => ($number + 1), 'class'=>'form-control'));		
										echo '</div>';	
										echo '</div>';
									}else{
										echo'<div class="form-group">';	
										echo'<label class="control-label col-md-2" for="Facturanumero">Número</label>';		
										echo'<div class="col-md-9">';			
										echo $this->Form->input('numero', array('id'=>'Facturanumero', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
										echo '</div>';	
										echo '</div>';
									}
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Facturafecha">Fecha</label>';		
									echo'<div class="col-md-4">';			
									echo $this->Form->input('fecha1', array('id'=>'Facturafecha', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly' => true ));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="numero_control">N&uacute;mero de control</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('numero_control', array('id'=>'numero_control', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Facturatipocliente_id">Tipo cliente</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('tipocliente_id', array('id'=>'Facturatipocliente_id', 'div'=>false, 'label'=>false, 'class'=>'form-control' )); 
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Facturatipoclientesub_id">Tipo subcliente</label>';		
									echo'<div class="col-md-9" id="div-subcliente">';			
									echo $this->Form->input('tipoclientesub_id', array('id'=>'Facturatipoclientesub_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Facturacliente_id">Cliente</label>';		
									echo'<div class="col-md-9" id="div-cliente">';		
									echo $this->Form->input('cliente_id', array('id'=>'Facturacliente_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));
									echo '</div>';	
									echo '</div>';
									?>
									<div id="div-factura">

									</div>
									<script type="text/javascript">
										selectTagRemote('/Facturas/factura','div-factura',<?=$id_cliente?>);
									</script>
									<?php
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Facturatotal">Total</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('total', array('value'=>0, 'readonly'=>true, 'id'=>'Facturatotal', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';

										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Facturaconcepto">Concepto</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('concepto', array('id'=>'Facturaconcepto', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									
								?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Html->link(__('List Facturas'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('List Tipoclientes'), array('controller' => 'tipoclientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipocliente'), array('controller' => 'tipoclientes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipoclientesubs'), array('controller' => 'tipoclientesubs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipoclientesub'), array('controller' => 'tipoclientesubs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Clientes'), array('controller' => 'clientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cliente'), array('controller' => 'clientes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Facturadetalles'), array('controller' => 'facturadetalles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Facturadetalle'), array('controller' => 'facturadetalles', 'action' => 'add')); ?> </li>
		</ul>
	</div>
<?php */ ?>

<script type="text/javascript">
	$(function(){
		$('#Facturafecha').datepicker();
	});
</script>