<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Facturas'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Facturas'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Facturas'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								
							<dt><?php echo __('Número'); ?></dt>
							<dd>
								<?php echo h($factura['Factura']['numero']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Fecha'); ?></dt>
							<dd>
								<?php echo h( formatdmy($factura['Factura']['fecha']) ); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Tipo cliente'); ?></dt>
							<dd>
								<?php echo h($factura['Tipocliente']['denominacion']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Tipo cliente sub'); ?></dt>
							<dd>
								<?php echo h($factura['Tipoclientesub']['nombre']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Cliente'); ?></dt>
							<dd>
								<?php echo h($factura['Cliente']['nombre_completo']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Concepto'); ?></dt>
							<dd>
								<?php echo h($factura['Factura']['concepto']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Total'); ?></dt>
							<dd>
								<?php echo h($factura['Factura']['total']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Tipo pago'); ?></dt>
							<dd>
								<?php echo h($factura['Facturatipopago']['denominacion']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Pagado'); ?></dt>
							<dd>
								<?php echo h($factura['Factura']['pagado']); ?>
								&nbsp;
							</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $factura['Factura']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Factura'), array('action' => 'edit', $factura['Factura']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Factura'), array('action' => 'delete', $factura['Factura']['id']), array(), __('Are you sure you want to delete # %s?', $factura['Factura']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Facturas'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Factura'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipoclientes'), array('controller' => 'tipoclientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipocliente'), array('controller' => 'tipoclientes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipoclientesubs'), array('controller' => 'tipoclientesubs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipoclientesub'), array('controller' => 'tipoclientesubs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Clientes'), array('controller' => 'clientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cliente'), array('controller' => 'clientes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Facturadetalles'), array('controller' => 'facturadetalles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Facturadetalle'), array('controller' => 'facturadetalles', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Facturadetalles'); ?></h3>
	<?php if (!empty($factura['Facturadetalle'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Factura Id'); ?></th>
		<th><?php echo __('Facturatipoproducto Id'); ?></th>
		<th><?php echo __('Total'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($factura['Facturadetalle'] as $facturadetalle): ?>
		<tr>
			<td><?php echo $facturadetalle['id']; ?></td>
			<td><?php echo $facturadetalle['factura_id']; ?></td>
			<td><?php echo $facturadetalle['facturatipoproducto_id']; ?></td>
			<td><?php echo $facturadetalle['total']; ?></td>
			<td><?php echo $facturadetalle['created']; ?></td>
			<td><?php echo $facturadetalle['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'facturadetalles', 'action' => 'view', $facturadetalle['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'facturadetalles', 'action' => 'edit', $facturadetalle['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'facturadetalles', 'action' => 'delete', $facturadetalle['id']), array(), __('Are you sure you want to delete # %s?', $facturadetalle['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Facturadetalle'), array('controller' => 'facturadetalles', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<?php */ ?>