<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Clientes'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Clientes'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Edit Cliente'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Cliente', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
												
									echo $this->Form->input('id', array('class'=>'form-horizontal'));	
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Clientetipocliente_id">Tipo cliente</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('tipocliente_id', array('id'=>'Clientetipocliente_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', "onChange"=>"selectTagRemote('".$this->Html->url('/Clientes/sub_cliente')."', 'div-subcliente', this.value);", 'empty'=>'--Seleccione--')); 
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Clientetipoclientesub_id">Tipo subcliente</label>';		
									echo'<div class="col-md-9" id="div-subcliente">';			
									echo $this->Form->input('tipoclientesub_id', array('id'=>'Clientetipoclientesub_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Clientedni">Dni</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('dni', array('id'=>'Clientedni', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Clientenombre_completo">Nombre</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('nombre_completo', array('id'=>'Clientenombre_completo', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Clientefecha_nacimiento">Fecha nacimiento</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('fecha_nacimiento1', array('id'=>'Clientefecha_nacimiento', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'required' => true, 'readonly' => true));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Clientepai_id">País de Origen</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('pai_id', array('id'=>'Clientepai_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Clienteciudad">Ciudad</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('ciudad', array('id'=>'Clienteciudad', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Clientedireccion">Dirección</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('direccion', array('id'=>'Clientedireccion', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Clientetelf">Teléfono</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('telf', array('id'=>'Clientetelf', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
									
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Clientetelf">Móvil</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('movil', array('id'=>'Clientemovil', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Clientecorreo">Email</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('correo', array('id'=>'Clientecorreo', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Clientecod_pos">Cod pos</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('cod_pos', array('id'=>'Clientecod_pos', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Preferencias">Preferencias</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('preferencias', array('id'=>'Clientepreferencias', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';

									$options = array('1'=>'Activo', '0'=>'Inactivo');

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" >Estatus</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('estatus', array('id'=>'Clienteestatus', 'div'=>false, 'label'=>false, 'class'=>'form-control','options'=>$options));		
									echo '</div>';	
									echo '</div>';

									$porcentajes = array('1'=>'Si', '0'=>'No');

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" >Aplica Porcentaje</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('apliporcent', array('id'=>'Clienteapliporcent', 'div'=>false, 'label'=>false, 'class'=>'form-control','options'=>$porcentajes));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" >Monto Porcentaje</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('montoporcent', array('id'=>'Clientemontoporcent', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
							?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<script>
	$("#Clientefecha_nacimiento").datepicker();
</script>