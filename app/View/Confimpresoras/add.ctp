<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Confimpresoras'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Confimpresoras'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Add Confimpresora'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Confimpresora', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Confimpresoradenominacion">Denominación</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('denominacion', array('id'=>'Confimpresoradenominacion', 'div'=>false, 'label'=>false, 'class'=>'form-control','placeholder'=>'Denominación de la impresora, para reconocerla. Ej: Epson TMUA220 II'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Confimpresorapuerto">Puerto</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('puerto', array('id'=>'Confimpresorapuerto', 'div'=>false, 'label'=>false, 'class'=>'form-control','options'=>array('1'=>'Com 1','2'=>'Com 2','3'=>'Com 3','4'=>'Com 4','5'=>'Com 5')));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Confimpresoratipodocumento">Tipo de Documento</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('tipodocumento', array('id'=>'Confimpresoratipodocumento', 'div'=>false, 'label'=>false, 'class'=>'form-control','options'=>array('T'=>'Ticket Fiscal','M'=>'Notas de Crédito')));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Confimpresorasalidaimpresora">Salida Impresora</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('salidaimpresora', array('id'=>'Confimpresorasalidaimpresora', 'div'=>false, 'label'=>false, 'class'=>'form-control','options'=>array('C'=>'Formulario Continuo','F'=>'Hoja Suelta')));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Confimpresoraletradedocumento">Letra de Documento</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('letradedocumento', array('id'=>'Confimpresoraletradedocumento', 'div'=>false, 'label'=>false, 'class'=>'form-control','options'=>array('A'=>'A ','B'=>'B','C'=>'C')));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Confimpresoracantcopias">Cantidad de Copias</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('cantcopias', array('id'=>'Confimpresoracantcopias', 'div'=>false, 'label'=>false, 'class'=>'form-control','placeholder'=>'Recomendado 1'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Confimpresoratipodeformulario">Tipo de Formulario</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('tipodeformulario', array('id'=>'Confimpresoratipodeformulario', 'div'=>false, 'label'=>false, 'class'=>'form-control','options'=>array('F'=>'Pre Impreso','P'=>'Dibuja la Impresora (*)','A'=>'Auto Impreso')));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Confimpresorasizecaracter">Tamaño de Letra</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('sizecaracter', array('id'=>'Confimpresorasizecaracter', 'div'=>false, 'label'=>false, 'class'=>'form-control','options'=>array('10'=>'10 (*)','12'=>'12','17'=>'17')));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Confimpresorarespvendedoriva">Resp. Iva (Vendedor)</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('respvendedoriva', array('id'=>'Confimpresorarespvendedoriva', 'div'=>false, 'label'=>false, 'class'=>'form-control','options'=>array('I'=>'Responsable Inscrito',
								   'R'=>'Responsable No Inscrito',
								   'N'=>'No Responsable',
								   'E'=>'Exento',
								   'M'=>'Monotributo')));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Confimpresorarespclienteiva">Resp. Iva (Cliente)</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('respclienteiva', array('id'=>'Confimpresorarespclienteiva', 'div'=>false, 'label'=>false, 'class'=>'form-control','options'=>array('I'=>'Responsable Inscrito',
								   'R'=>'Responsable No Inscrito',
								   'N'=>'No Responsable',
								   'E'=>'Exento',
								   'M'=>'Monotributo')));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Confimpresoraformalmacenadatos">Forma de Almacenar Datos</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('formalmacenadatos', array('id'=>'Confimpresoraformalmacenadatos', 'div'=>false, 'label'=>false, 'class'=>'form-control','options'=>array('C'=>'No se va a realizar un DFH para Farmacia (*)',
								    'G'=>'Unicamente cuando se va a emitir un DFH para Farmacia')));		
echo '</div>';	
echo '</div>';
	?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Html->link(__('List Confimpresoras'), array('action' => 'index')); ?></li>
			</ul>
	</div>
<?php */ ?>