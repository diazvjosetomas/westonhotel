<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Confimpresoras'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Confimpresoras'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Confimpresoras'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($confimpresora['Confimpresora']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Denominacion'); ?></dt>
		<dd>
			<?php echo h($confimpresora['Confimpresora']['denominacion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Puerto'); ?></dt>
		<dd>
			<?php echo h($confimpresora['Confimpresora']['puerto']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tipodocumento'); ?></dt>
		<dd>
			<?php echo h($confimpresora['Confimpresora']['tipodocumento']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Salidaimpresora'); ?></dt>
		<dd>
			<?php echo h($confimpresora['Confimpresora']['salidaimpresora']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Letradedocumento'); ?></dt>
		<dd>
			<?php echo h($confimpresora['Confimpresora']['letradedocumento']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cantcopias'); ?></dt>
		<dd>
			<?php echo h($confimpresora['Confimpresora']['cantcopias']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tipodeformulario'); ?></dt>
		<dd>
			<?php echo h($confimpresora['Confimpresora']['tipodeformulario']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sizecaracter'); ?></dt>
		<dd>
			<?php echo h($confimpresora['Confimpresora']['sizecaracter']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Respvendedoriva'); ?></dt>
		<dd>
			<?php echo h($confimpresora['Confimpresora']['respvendedoriva']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Respclienteiva'); ?></dt>
		<dd>
			<?php echo h($confimpresora['Confimpresora']['respclienteiva']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Formalmacenadatos'); ?></dt>
		<dd>
			<?php echo h($confimpresora['Confimpresora']['formalmacenadatos']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($confimpresora['Confimpresora']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($confimpresora['Confimpresora']['modified']); ?>
			&nbsp;
		</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $confimpresora['Confimpresora']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Confimpresora'), array('action' => 'edit', $confimpresora['Confimpresora']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Confimpresora'), array('action' => 'delete', $confimpresora['Confimpresora']['id']), array(), __('Are you sure you want to delete # %s?', $confimpresora['Confimpresora']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Confimpresoras'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Confimpresora'), array('action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>