<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sistema de Reservaciones</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/bootstrap/css/bootstrap.min.css', 'AdminLTE-2.3.0');?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/bootstrap/css/ionicons.min.css', 'AdminLTE-2.3.0');?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/dist/css/AdminLTE.css', 'AdminLTE-2.3.0');?>">
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/dist/css/calendar.css', 'AdminLTE-2.3.0');?>">

    <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/dist/css/skins/_all-skins.min.css', 'AdminLTE-2.3.0');?>">
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/plugins/morris/morris.css', 'AdminLTE-2.3.0');?>">



    <!-- jQuery 2.1.4 -->
    <script src="<?= $this->Html->templateinclude('/plugins/jQuery/jQuery-2.1.4.min.js', 'AdminLTE-2.3.0');?>"></script>
    <script src="<?= $this->Html->templateinclude('/dist/js/tablesorter.js', 'AdminLTE-2.3.0');?>"></script>
    <script src="<?= $this->Html->templateinclude('/dist/js/sticktableheaders.js', 'AdminLTE-2.3.0');?>"></script>
    
    <!-- DataTable CSS -->
    <?php 
    //echo $this->Html->css('datatable/jquery.dataTables.css'); 
    ?>
    <?php echo $this->Html->css('datatable/buttons.dataTables.min.css'); ?>
    <?php echo $this->Html->css('datatable/dataTables.bootstrap.min.css'); ?>

    <!-- DataTable JS -->
    <?php echo $this->Html->script('datatable/jquery.dataTables.js'); ?>
    <?php echo $this->Html->script('datatable/dataTables.bootstrap.min.js'); ?>
    <?php echo $this->Html->script('datatable/dataTables.buttons.min.js'); ?>
    <?php echo $this->Html->script('datatable/dataTables.buttons.flash.min.js'); ?>
    <?php echo $this->Html->script('datatable/jszip.min.js'); ?>
    <?php echo $this->Html->script('datatable/pdfmake.min.js'); ?>
    <?php echo $this->Html->script('datatable/vfs_fonts.js'); ?>
    <?php echo $this->Html->script('datatable/buttons.html5.min.js'); ?>
    <?php echo $this->Html->script('datatable/buttons.print.min.js'); ?>
    

    <!-- Calendar JS -->
    <?php echo $this->Html->css('jquery-ui.css'); ?>
    <?php echo $this->Html->script('jquery-ui.min.js'); ?>
    <script type="text/javascript">
      jQuery(function($){
          $.datepicker.regional['es'] = {
              closeText: 'Cerrar',
              prevText: '&#x3c;Ant',
              nextText: 'Sig&#x3e;',
              currentText: 'Hoy',
              monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
              'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
              monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
              'Jul','Ago','Sep','Oct','Nov','Dic'],
              dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
              dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
              dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
              weekHeader: 'Sm',
              dateFormat: 'dd/mm/yy',
              changeMont: true,
              changeYear: true,
              firstDay: 1,
              isRTL: false,
              showMonthAfterYear: true,
              yearRange: '-100:+30',
              yearSuffix: ''};
          $.datepicker.setDefaults($.datepicker.regional['es']);
      });
  </script>
  
  <script src="<?= $this->Html->templateinclude('/plugins/morris/raphael-min.js', 'AdminLTE-2.3.0');?>"></script>
  <script src="<?= $this->Html->templateinclude('/plugins/morris/morris.js', 'AdminLTE-2.3.0');?>"></script>
  <?= $this->Html->script('bootbox.min'); ?>></script>
  <?php echo $this->Html->script('functions.js'); ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue sidebar-mini fixed">
    <!-- Site wrapper -->
    <div class="wrapper">
      <header class="main-header">
        <!-- Logo -->
        <a target="_blank" href="/Hoteleria" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>SR</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Reservaciones</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!--<img src="<?= $this->Html->templateinclude('/dist/img/user2-160x160.jpg', 'AdminLTE-2.3.0');?>" class="user-image" alt="User Image">-->
                  <span class="hidden-xs">Administrador</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <p>
                      Administrador
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body">
                    <!--<div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>-->
                    <div class="pull-right">
                      <a href="<?= $this->Html->url('/Login/logout')?>" class="btn btn-default btn-flat">Cerrar Sesión</a>
                    </div>
                  </li>
                  <!-- Menu Footer-->
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <!--<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>-->
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- =============================================== -->
      <!-- Left side column. contains the sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Opciones del sistema</li>
            <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-home"></i><span>Inicio</span></a></li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-users"></i> <span>Clientes</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu"> 
                <li><a href="<?= $this->Html->url('/Clientes/')?>"><i class="fa fa-circle-o"></i>Clientes</a></li>
                <li><a href="<?= $this->Html->url('/Tarjetacreditos/')?>"><i class="fa fa-circle-o"></i>Tarjeta de credito</a></li>
                <li><a href="<?= $this->Html->url('/Tipoclientes/')?>"><i class="fa fa-circle-o"></i>Tipos de clientes</a></li>
                <li><a href="<?= $this->Html->url('/Tipoclientesubs/')?>"><i class="fa fa-circle-o"></i>Tipos de Sub clientes</a></li>
                <li><a href="<?= $this->Html->url('/Tipotarjetas/')?>"><i class="fa fa-circle-o"></i>Tipos de tarjetas</a></li>
              </ul>
            </li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-bed"></i> <span>Habitaciones</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu"> 
                <li><a href="<?= $this->Html->url('/Tipohabitaciones/')?>"><i class="fa fa-circle-o"></i>Tipo habitación</a></li>
                <li><a href="<?= $this->Html->url('/Habistatuses/')?>"><i class="fa fa-circle-o"></i>Tipo status habitación</a></li>
                <li><a href="<?= $this->Html->url('/Habitaciones/')?>"><i class="fa fa-circle-o"></i>Habitaciones</a></li>
                <li><a href="<?= $this->Html->url('/Habitaciones/status')?>"><i class="fa fa-circle-o"></i>Status habitaciones</a></li>
                <li><a href="<?= $this->Html->url('/Tipotemporadas/')?>"><i class="fa fa-circle-o"></i>Tipo temporadas</a></li>
                <!-- <li><a href="<?= $this->Html->url('/Pstemporadas/')?>"><i class="fa fa-circle-o"></i>Precio segun temporada</a></li> -->
              </ul>
            </li>
     

            <li class="treeview">
              <a href="#">
                <i class="fa fa-calendar"></i> <span>Reservaciones</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu"> 
                <li><a href="<?= $this->Html->url('/Cupos/modografico/')?>"><i class="fa fa-circle-o"></i>Cupos Diarios</a></li>
                <li><a href="<?= $this->Html->url('/Reserstatusindividuales/')?>"><i class="fa fa-circle-o"></i>Tipo status individual</a></li>
                <!-- <li><a href="<?= $this->Html->url('/Reserstatusmultiples/')?>"><i class="fa fa-circle-o"></i>Tipo status multiples</a></li> -->
                <li><a href="<?= $this->Html->url('/Reserindividuales/')?>"><i class="fa fa-circle-o"></i>Individuales</a></li>
                <!-- <li><a href="<?= $this->Html->url('/Resermultiples/')?>"><i class="fa fa-circle-o"></i>Multiples</a></li> -->
                <!-- <li><a href="<?= $this->Html->url('/Reservas/reservas')?>"><i class="fa fa-circle-o"></i>Calendario</a></li>   -->
                <li><a href="<?= $this->Html->url('/Bodymails')?>"><i class="fa fa-circle-o"></i>Cuerpo de Mail</a></li> 
                <li><a href="<?= $this->Html->url('/Terminoscondiciones')?>"><i class="fa fa-circle-o"></i>Terminos y Condiciones</a></li>   
                <!-- <li><a href="<?= $this->Html->url('/Reservas/baja_reservas')?>"><i class="fa fa-circle-o"></i>Reservas de Baja</a></li>  -->
                <!-- <li><a href="<?= $this->Html->url('/auditorias/')?>"><i class="fa fa-circle-o"></i>Auditoria reservas</a></li>  -->
                <!-- <li><a href="<?= $this->Html->url('/auditorias/promedios')?>"><i class="fa fa-circle-o"></i>Promedios</a></li>  -->

              </ul>
            </li>
 
            <li class="treeview">
              <a href="#">
                <i class="fa fa-user-o"></i> <span>Usuarios</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu"> 
                <li><a href="<?= $this->Html->url('/Roles/')?>"><i class="fa fa-circle-o"></i>Roles</a></li>
                <li><a href="<?= $this->Html->url('/Modulos/')?>"><i class="fa fa-circle-o"></i>Modulos</a></li>
                <li><a href="<?= $this->Html->url('/Rolesmodulos/')?>"><i class="fa fa-circle-o"></i>Roles modulos</a></li>
                <li><a href="<?= $this->Html->url('/Users/')?>"><i class="fa fa-circle-o"></i>Usuarios</a></li>
              </ul>
            </li>
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-gears"></i> <span>Configuraciones</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu"> 
                <li><a href="<?= $this->Html->url('/Descuentos')?>"><i class="fa fa-circle-o"></i>Descuentos</a></li>
              </ul>
            </li>



        </section>
        <!-- /.sidebar -->
      </aside>
    </div><!-- ./wrapper -->
      <!-- =============================================== -->
      <div class="wrapper">
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <?php echo $this->Flash->render(); ?>
        <?php echo $this->fetch('content'); ?>
      </div><!-- /.content-wrapper -->
    </div><!-- ./wrapper -->

    
    <!-- Bootstrap 3.3.5 -->
    <script src="<?= $this->Html->templateinclude('/bootstrap/js/bootstrap.min.js', 'AdminLTE-2.3.0');?>"></script>
    <!-- SlimScroll -->
    <script src="<?= $this->Html->templateinclude('/plugins/slimScroll/jquery.slimscroll.min.js', 'AdminLTE-2.3.0');?>"></script>
    <!-- FastClick -->
    <script src="<?= $this->Html->templateinclude('/plugins/fastclick/fastclick.min.js', 'AdminLTE-2.3.0');?>"></script>
    <!-- AdminLTE App -->
    <script src="<?= $this->Html->templateinclude('/dist/js/app.min.js', 'AdminLTE-2.3.0');?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?= $this->Html->templateinclude('/dist/js/demo.js', 'AdminLTE-2.3.0');?>"></script>

    <!-- highcharts JS -->
    
    
    <?php echo $this->Html->script('highcharts/exporting.js'); ?>
    <?php echo $this->Html->script('highcharts/drilldown.js'); ?>
    <?php echo $this->Html->script('highcharts/data.js'); ?>

  </body>
</html>
