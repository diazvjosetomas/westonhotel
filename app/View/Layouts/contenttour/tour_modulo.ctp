<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>!Weston</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />

    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:description" content=""/>
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />

    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">

    <!-- Animate.css -->
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/css/animate.css', 'tour');?>">

    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/css/icomoon.css', 'tour');?>">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/css/bootstrap.css', 'tour');?>">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/css/magnific-popup.css', 'tour');?>">
    <!-- Flexslider  -->
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/css/flexslider.css', 'tour');?>">

    <!-- Owl Carousel -->
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/css/owl.carousel.min.css', 'tour');?>">
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/css/owl.theme.default.min.css', 'tour');?>">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/css/bootstrap-datepicker.css', 'tour');?>">

    <!-- Flaticons  -->
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/fonts/flaticon/font/flaticon.css', 'tour');?>">

    <!-- Theme style  -->
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/css/style.css', 'tour');?>">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">



    <!-- Modernizr JS -->
    <script src="<?= $this->Html->templateinclude('/js/modernizr-2.6.2.min.js', 'tour');?>"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="js/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<div class="colorlib-loader"></div>

<div id="page">
<div	class="row">
    <?php include('nav_modulo.ctp'); ?>
</div>
<br>
<br>
<br>
    
  

<?php echo $this->Flash->render(); ?>
<?php echo $this->fetch('content'); ?>



    <footer id="colorlib-footer" role="contentinfo">
        <div class="container">
            <div class="row row-pb-md">
                <div class="col-md-3 colorlib-widget">
                </div>
                <div style="text-align: center;" class="col-md-6 colorlib-widget">
                    <h4><img style="width: 300px;" src="<?= $this->Html->estructurainclude('logo_weston.png');?>" alt=""></h4>
                    <p></p>
                    <p>
                    <ul class="colorlib-social-icons">
                        <li><a href="#"><i class="icon-twitter"></i></a></li>
                        <li><a href="#"><i class="icon-facebook"></i></a></li>
                        <li><a href="#"><i class="icon-linkedin"></i></a></li>
                        <li><a href="#"><i class="icon-dribbble"></i></a></li>
                    </ul>
                    </p>
                </div>
                <div class="col-md-3 colorlib-widget">
                    <h4>Gracias por Contactarnos</h4>
                    <p>
                        <ul class="colorlib-footer-links">


                    <p class="address-item">
                        <span class="icon"><i class="fa fa-map-marker"></i></span>
                        Ave. 27 de Febrero #194 Santo Domingo, Dominican Republic

                    </p>
                    <p class="address-item">
                        <span class="icon"><i class="glyphicon glyphicon-earphone"></i></span>
                        Telefono: +1 829 567 8888 <br>

                    </p>
                    <p class="address-item">
                        <span class="icon"><i class="fa fa-envelope"></i></span>
                        Reserva: <a href="mailto:sales@westonsuiteshotel.com">sales@westonsuiteshotel.com</a> <a href="mailto:reservation@westonsuiteshotel.com">reservation@westonsuiteshotel.com</a><br>
                    </p>
                    <p class="address-item">
                        <span class="icon"><i class="fa fa-globe"></i></span>
                        Soporte: <a href="mailto:operations@westonsuiteshotel.com">operations@westonsuiteshotel.com</a> <br>
                    </p>


                    </ul>
                    </p>
                </div>
     
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        <!--Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart2" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>-->
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0.</span> -->
                        <!--<span class="block">Demo Images: <a href="#" target="_blank">Unsplash</a> , <a href="http://pexels.com/" target="_blank">Pexels.com</a></span>-->
                    </p>
                </div>
            </div>
        </div>
    </footer>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<!-- jQuery -->
<script src="<?= $this->Html->templateinclude('/js/jquery.min.js', 'tour');?>"></script>


<!-- jQuery Easing -->
<script src="<?= $this->Html->templateinclude('/js/jquery.easing.1.3.js', 'tour');?>"></script>
<!-- Bootstrap -->
<script src="<?= $this->Html->templateinclude('/js/bootstrap.min.js', 'tour');?>"></script>
<!-- Waypoints -->
<script src="<?= $this->Html->templateinclude('/js/jquery.waypoints.min.js', 'tour');?>"></script>
<!-- Flexslider -->
<script src="<?= $this->Html->templateinclude('/js/jquery.flexslider-min.js', 'tour');?>"></script>
<!-- Owl carousel -->
<script src="<?= $this->Html->templateinclude('/js/owl.carousel.min.js', 'tour');?>"></script>
<!-- Magnific Popup -->
<script src="<?= $this->Html->templateinclude('/js/jquery.magnific-popup.min.js', 'tour');?>"></script>
<script src="<?= $this->Html->templateinclude('/js/magnific-popup-options.js', 'tour');?>"></script>
<!-- Date Picker -->
<script src="<?= $this->Html->templateinclude('/js/bootstrap-datepicker.js', 'tour');?>"></script>
<!-- Stellar Parallax -->
<script src="<?= $this->Html->templateinclude('/js/jquery.stellar.min.js', 'tour');?>"></script>
<!-- Main -->
<script src="<?= $this->Html->templateinclude('/js/main.js', 'tour');?>"></script>


<script type="text/javascript">


    $('#consultar').click(function(e){
        e.preventDefault();
        $('#habitaciontipo').empty();
        var fechai = $('#fecha_ingreso').val();
        var fechas = $('#fecha_salida').val();
        var personas = $('#cant_personas').val();
        var fecha_i_1 = fechai.replace("/","-");
        var fecha_s_1 = fechas.replace("/","-");
        var fecha_i = fecha_i_1.replace("/","-");
        var fecha_s = fecha_s_1.replace("/","-");

        if(fechai != "" && fechas != "" && personas != ""){
            $.ajax({
                url:"/Reserindividuales/consulta_habitacion/",
                type:"post",
                data:{
                    fechai: fechai,
                    fechas: fechas,
                    personas:personas
                },
                success:function(n){
                    console.log(n);
                    var data = JSON.parse(n);

                    if (data.resp == true) {


                        data.data.forEach(datos => {
                            $('#habitaciontipo').append(
                                "<a href='/Reserindividualespub/add/"+datos.Tipohabitacione.id+"/"+fecha_i+"/"+fecha_s+"/"+personas+"' class='tour-entry'>"+
                                "<div class='tour-img' style='background-image: url(img/upload/"+datos.Tipohabitacione.file+")'>"+
                                "</div>"+
                                "<span class='desc'>"+
                                "<p class='star'><span><i class='icon-star-full'></i><i class='icon-star-full'></i><i class='icon-star-full'></i><i class='icon-star-full'></i><i class='icon-star-full'></i></span> 545 Reviews</p>"+
                                "<h2>"+datos.Tipohabitacione.denominacion+"</h2>"+
                                "<span class='price'>Oferta</span>"+
                                "</span>"+
                                "</a>");

                        });

                    }
                }
            });
            $('html,body').animate({
                scrollTop: $("#habitaciontipo").offset().top
            }, 2000);
        }else{
            alert("Debe setear los campos de fechas y ocupantes");
        }


        /*$.get("/Reserindividuales/consulta_habitacion/".fechai."/".fechas."/".personas, function( data ) {
         console.log( data );
          //alert(personas);

        });*/
    });



</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<script type="text/javascript">
    function googleTranslateElementInit() {
        new google.translate.TranslateElement({pageLanguage: 'es',autoDisplay: false, layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
    }

  

         
      $(document).ready(function() {

            


          $("#newNombresBtn").click(function (e) {
              var MaxInputs       = 8;
              var x = $("#nombres-container #emailcount").length + 1;
              var FieldCount = x-1;
              if(x <= MaxInputs) //max input box allowed
              {
                  FieldCount++;
                  $("#nombres-container").append('<div id="nombrecount"><div class="form-group nombre-con" id="nombre_'+ FieldCount +'"><label class="control-label col-md-3">Nombre Completo</label><div class="col-md-8"><div class="input-group col-md-12"><input class="form-control text-box single-line" id="Nombres_'+ FieldCount +'__Nombre" name="data[Reserindividuale][Nombre][]" placeholder="Ej: Juan Antonio Pacheco Lugo" require="require" type="text" value="" /></div></div><div class="col-md-1"><button class="btn btn-danger btn-sm" onclick="if(confirm(\'Seguro que desea eliminar el registro\')){ $(\'#nombre_'+ FieldCount +'\').remove(); $(\'#Nombres_'+ FieldCount +'__Nombre\').val(\'\'); $(\'#Nombres_'+ FieldCount +'__Nombre\').removeAttr(\'required\'); } return false;"><span class="glyphicon glyphicon-trash"></span></button></div></div></div>');
                  x++; //text box increment
              }
              return false;
          });





          


      });

</script>
<?php echo $this->Html->script('functions.js'); ?>


</body>
</html>
