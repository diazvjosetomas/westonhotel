<nav class="navbar navbar-inverse ">
  <div class="container-fluid">
    <div class="navbar-header">
    
      <div class="navbar-brand" id="colorlib-logo"><a  href="index.html"><img style="width: 120px;" src="<?= $this->Html->estructurainclude('logo_weston.png');?>" alt=""></a></div>
    </div>
   
    <ul class="nav navbar-nav">

    <?php if(!empty($this->Session->read('USUARIO_STATUS'))){ ?>
              <li   class="active"><a id="logout" href="<?= $this->Html->url('/LoginUser/logout')?>" data-toggle="modal" data-target="#exampleModal">Logout</a></li>
             <?php }else{ ?>
							<li  class="active"><a id="login" href="#" data-toggle="modal" data-target="#exampleModal">Login</a></li>
              <?php } ?>


    </ul>
    
  </div>
</nav>



<div class="modal fade" style="background: rgb(0, 0, 0, 0.6);" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="background:rgb(255, 255, 255, 0.4);" role="document">
    <div class="modal-content" style="background:none;">
      <div class="modal-header">
        <h5  class="modal-title" style="color: #ffffff;" id="exampleModalLabel">Inicio Sesion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="background:none;">
        <div class="container">
        <div class="row">
        	<div class="col-md-12">
        <form class="form-2" method="post" action="/LoginUser/ValidateLoginUser">
          <h1 style="color: #ffffff ;"><span class="log-in">Iniciar Sesion</span> </h1>
          </div>
          </div>
          <div class="row">
          	<div class="col-md-1">
          	</div>
          	<div class="col-md-1">
            <label style="color: #ffffff ;" for="usuario"><i class="icon-user"></i>Usuario:</label>
           </div>
           <div class="col-md-4">
            <input type="text" id="usuario" style="border: 1px; height: 20px; background:none; color: #ffffff " name="data[Admin][user]"   placeholder="Usuario">
            </div>
          </div>
          <div class="row">
          	<div class="col-md-1">
          	</div>
          	<div class="col-md-1">
            <label style="color: #ffffff ;" for="password"><i class="icon-lock"></i>Contraseña:</label>
        </div>
            <div class="col-md-4">
            <input style="border: 1px; height: 20px; background:none; color: #ffffff " id="password" type="password" name="data[Admin][password]" placeholder="Contraseña" class="">
        	</div>
          </div>   
          <div class="row">  
              <div class="col-md-2" style="align-content: center;">
              </div>
              <div class="col-md-4">
            <input class="btn btn-primary" type="submit" name="submit" value="Iniciar Sesion">
            </div>
        </div>
         
        </form>​​
        </div>
      
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>