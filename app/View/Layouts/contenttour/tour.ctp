
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>!WestonHotel</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />

    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:description" content=""/>
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />

    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">

    <!-- Animate.css -->
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/css/animate.css', 'tour');?>">

    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/css/icomoon.css', 'tour');?>">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/css/bootstrap.css', 'tour');?>">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/css/magnific-popup.css', 'tour');?>">
    <!-- Flexslider  -->
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/css/flexslider.css', 'tour');?>">

    <!-- Owl Carousel -->
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/css/owl.carousel.min.css', 'tour');?>">
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/css/owl.theme.default.min.css', 'tour');?>">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/css/bootstrap-datepicker.css', 'tour');?>">

    <!-- Flaticons  -->
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/fonts/flaticon/font/flaticon.css', 'tour');?>">

    <!-- Theme style  -->
    <link rel="stylesheet" href="<?= $this->Html->templateinclude('/css/style.css', 'tour');?>">

    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    

    <!-- Modernizr JS -->
    <script src="<?= $this->Html->templateinclude('/js/modernizr-2.6.2.min.js', 'tour');?>"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <style>
        .skiptranslate{
            background:rgba(0,0,0,0.4);
        }
        .oculto{
            display: none;
        }
    </style>
</head>
<body>
<div id="google_translate_element"></div>
    <div class="colorlib-loader"></div>

    <div id="page">

        <?php include('nav.ctp'); ?>

        <aside id="colorlib-hero">

            <div class="flexslider">
                <ul class="slides">
                    <?php foreach ($banner as $key => $value): ?>
                        <li style="background-image: url(<?= $this->Html->imginclude('/upload/'.$value['banner']['file']);?>)">
                            <div class="overlay"></div>
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
                                        <div class="slider-text-inner text-center">
                                            <h2><?= $value['banner']['linea_1'] ?></h2>
                                            <h1><?= $value['banner']['linea_2'] ?></h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endforeach ?>


                </ul>
            </div>
        </aside>
        <div id="colorlib-reservation">
            <!-- <div class="container"> -->
            <div class="row">
                <div class="search-wrap">
                    <div class="container">
                        <ul class="nav nav-tabs">
                            <!--<li class="active"><a data-toggle="tab" href="#flight"><i class="flaticon-plane"></i> Flight</a></li>-->
                            <li class=""><a data-toggle="tab" href="#hotel"><i class="flaticon-resort"></i> Hotel</a></li>
                            <!--<li><a data-toggle="tab" href="#car"><i class="flaticon-car"></i> Car Rent</a></li>-->
                            <!--<li><a data-toggle="tab" href="#cruises"><i class="flaticon-boat"></i> Cruises</a></li>-->
                        </ul>
                    </div>
                    <div  class="tab-content" >
                      
                        <div id="hotel" class="tab-pane fade in active">
                            <form method="post" class="colorlib-form" autocomplete="off">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="booknow">
                                            <h2>RESERVAR AHORA</h2>
                                            <span>El mejor precio en línea</span>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="date">Fecha de Entrada:</label>
                                            <div class= "form-field">
                                                <i class="icon icon-calendar2"></i>
                                                <input type="text" id="fecha_ingreso" class="form-control date" placeholder="Fecha Registrarse">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="date">Fecha de Salida:</label>
                                            <div class="form-field">
                                                <i class="icon icon-calendar2"></i>
                                                <input type="text" id="fecha_salida" class="form-control date" placeholder="Fecha Partida">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="guests">Cantidad de Huéspedes</label>
                                            <div class="form-field">
                                                <i class="icon icon-arrow-down3"></i>
                                                <input name="people" id="cant_personas" class="form-control" placeholder="Huésped">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <button  name="" id="consultar" value="Desplazar" class="btn btn-primary btn-block">Buscar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="cuerpo oculto">
    <?php echo $this->Flash->render(); ?>
    <?php echo $this->fetch('content'); ?>


    <footer id="colorlib-footer" role="contentinfo">
        <div class="container">
            <div class="row row-pb-md">
                <div class="col-md-3 colorlib-widget">
                </div>
                <div style="text-align: center;" class="col-md-6 colorlib-widget">
                    <h4><img style="width: 300px;" src="<?= $this->Html->estructurainclude('logo_weston.png');?>" alt=""></h4>
                    <p></p>
                    <p>
                    <ul class="colorlib-social-icons">
                        <li><a href="#"><i class="icon-twitter"></i></a></li>
                        <li><a href="#"><i class="icon-facebook"></i></a></li>
                        <li><a href="#"><i class="icon-linkedin"></i></a></li>
                        <li><a href="#"><i class="icon-dribbble"></i></a></li>
                    </ul>
                    </p>
                </div>
                <div class="col-md-3 colorlib-widget">
                    <h4>Gracias por Contactarnos</h4>
                    <p>
                        <ul class="colorlib-footer-links">


                    <p class="address-item">
                        <span class="icon"><i class="fa fa-map-marker"></i></span>
                        Ave. 27 de Febrero #194 Santo Domingo, Dominican Republic

                    </p>
                    <p class="address-item">
                        <span class="icon"><i class="glyphicon glyphicon-earphone"></i></span>
                        Telefono: +1 829 567 8888 <br>

                    </p>
                    <p class="address-item">
                        <span class="icon"><i class="fa fa-envelope"></i></span>
                        Reserva: <a href="mailto:sales@westonsuiteshotel.com">sales@westonsuiteshotel.com</a> <a href="mailto:reservation@westonsuiteshotel.com">reservation@westonsuiteshotel.com</a><br>
                    </p>
                    <p class="address-item">
                        <span class="icon"><i class="fa fa-globe"></i></span>
                        Soporte: <a href="mailto:operations@westonsuiteshotel.com">operations@westonsuiteshotel.com</a> <br>
                    </p>


                    </ul>
                    </p>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <p>
                   
                    </p>
                </div>
            </div>
        </div>
    </footer>
    </div>
</div>


<div id="btncart" class="gototop js-top">
    <a onclick="gotop()"  class="js-gotop"><i class="fa fa-shopping-cart"></i></a>
</div>


<input type="hidden" id="controlcupoid">
<input type="hidden" id="controltipohabid">
<input type="hidden" id="controltipotempid">
<input type="hidden" id="inputhiddenidtipohab">


<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Tipo Habitacion: <b> <span id="tipohabdenom"></span> </b> Cantidad de Personas <b id="cantidadpersonas"></b> </h4>
        
      </div>

      <!-- Modal body -->
      <div class="modal-body">

        

        
        
    

        <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;}
        .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
        .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
        .tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
        </style>
        

        <style>
        

        /* Style the tab */
        .tab {
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
        }

        /* Style the buttons inside the tab */
        .tab button {
            background-color: inherit;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
            font-size: 17px;
        }

        /* Change background color of buttons on hover */
        .tab button:hover {
            background-color: #ddd;
        }

        /* Create an active/current tablink class */
        .tab button.active {
            background-color: #ccc;
        }

        /* Style the tab content */
        .tabcontent {
            display: none;
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
        }
        </style>




        <table class="tg">
          <tr  >
            <th class="tg-0pky" style="height: 120px !important; width: 120px !important"> <div   id="imgdiv"> </div> </th>
            <th class="tg-0pky"> <h2>Descripcion:</h2> <b> <p id="tipohabdescripcion"></p> </b>  </th>

          </tr>
            <tr>
                <th class="tg-0pky"> <h4>Cupos Disponibles:</h4>   </th>
                <th class="tg-0pky"> <h2  id="ccuposdisponible"></h2>   <input id="ccuposdisponibleOculto" type="text" style="display: none;" value="0"></th>
            </tr>
          <tr style="height: 410px !important">
            <td class="tg-0pky" colspan="2">
                <h3>Seleccione un tipo de hospedaje:   </h3>
                

                <div class="tab" id="temporadas"></div>

                <div id="contenidotabs"> </div>
            </td>
          </tr>
        </table>
        
        


        <button id="cesta" type="button" class="btn btn-success " onclick="siguiente()" >Agregar a la cesta!</button>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        
        <button  type="button" class="btn btn-danger" data-dismiss="modal">Close / Cerrar</button>
      
      
      </div>

    </div>
  </div>
</div>












<!-- jQuery -->
<script src="<?= $this->Html->templateinclude('/js/jquery.min.js', 'tour');?>"></script>


<!-- jQuery Easing -->
<script src="<?= $this->Html->templateinclude('/js/jquery.easing.1.3.js', 'tour');?>"></script>
<!-- Bootstrap -->
<script src="<?= $this->Html->templateinclude('/js/bootstrap.min.js', 'tour');?>"></script>
<!-- Waypoints -->
<script src="<?= $this->Html->templateinclude('/js/jquery.waypoints.min.js', 'tour');?>"></script>
<!-- Flexslider -->
<script src="<?= $this->Html->templateinclude('/js/jquery.flexslider-min.js', 'tour');?>"></script>
<!-- Owl carousel -->
<script src="<?= $this->Html->templateinclude('/js/owl.carousel.min.js', 'tour');?>"></script>
<!-- Magnific Popup -->
<script src="<?= $this->Html->templateinclude('/js/jquery.magnific-popup.min.js', 'tour');?>"></script>
<script src="<?= $this->Html->templateinclude('/js/magnific-popup-options.js', 'tour');?>"></script>
<!-- Date Picker -->
<script src="<?= $this->Html->templateinclude('/js/bootstrap-datepicker.js', 'tour');?>"></script>
<!-- Stellar Parallax -->
<script src="<?= $this->Html->templateinclude('/js/jquery.stellar.min.js', 'tour');?>"></script>
<!-- Main -->
<script src="<?= $this->Html->templateinclude('/js/main.js', 'tour');?>"></script>


<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>


<script type="text/javascript">
    function googleTranslateElementInit() {
        new google.translate.TranslateElement({pageLanguage: 'es',autoDisplay: false, layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');

       


    }
        



    </script>

<?php echo $this->Html->script('functions.js'); ?>




</body>
</html>
