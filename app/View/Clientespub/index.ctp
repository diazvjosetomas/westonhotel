<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Clientes'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Clientes'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Clientes'); ?> Registrados</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                    <p><?php echo $this->Html->link(__('Crear Nuevo'), array('action' => 'add'),  array('class'=>'btn btn-primary')); ?></p>

							<table id="data" class="table table-striped table-bordered" width="100%" cellspacing="0">
							<thead>
							<tr>
															<th style="width: 15px !important;"><?php echo __('Dni'); ?></th>
															<th><?php echo __('Nombre'); ?></th>
															<th><?php echo __('Dirección'); ?></th>
															<th><?php echo __('Telf'); ?></th>
															<th><?php echo __('Email'); ?></th>
															<th><?php echo __('cod pos'); ?></th>
															<th><?php echo __('Estatus'); ?></th>
															<th><?php echo __('Ap. %'); ?></th>
															<th><?php echo __('Monto %'); ?></th>
															<th class="actions"><?php echo __('Acción'); ?></th>
							</tr>
							</thead>
							<tbody>
							<?php foreach ($clientes as $cliente): ?>
	<tr>
		<td ><?php echo h($cliente['Cliente']['dni']); ?>&nbsp;</td>
		<td><?php echo h($cliente['Cliente']['nombre_completo']); ?>&nbsp;</td>
		<td><?php echo h($cliente['Cliente']['direccion']); ?>&nbsp;</td>
		<td><?php echo h($cliente['Cliente']['telf']); ?>&nbsp;</td>
		<td><?php echo h($cliente['Cliente']['correo']); ?>&nbsp;</td>
		<td><?php echo h($cliente['Cliente']['cod_pos']); ?>&nbsp;</td>
		<td><?php echo $cliente['Cliente']['estatus']?'Activo':'Inactivo'; ?>&nbsp;</td>
		<td><?php echo $cliente['Cliente']['apliporcent']?'Si':'No'; ?>&nbsp;</td>
		<td><?php echo $cliente['Cliente']['montoporcent']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $cliente['Cliente']['id']), array('class'=>'')); ?>
			| <?php echo $this->Html->link(__('Ver'), array('action' => 'view', $cliente['Cliente']['id']),   array('class'=>'')); ?>
			| <?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $cliente['Cliente']['id']), array('class'=>'', 'confirm'=>__('Esta seguro que desea eliminar el registro # %s?', $cliente['Cliente']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
							</tbody>
							</table>
						</div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->


<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Cliente'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Tipoclientes'), array('controller' => 'tipoclientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipocliente'), array('controller' => 'tipoclientes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipoclientesubs'), array('controller' => 'tipoclientesubs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipoclientesub'), array('controller' => 'tipoclientesubs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tarjetacreditos'), array('controller' => 'tarjetacreditos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tarjetacredito'), array('controller' => 'tarjetacreditos', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?><script type="text/javascript">
	//$(document).ready(function() {
	    $('#data').DataTable( {
	    	dom: 'Bfrtlip',
	        buttons: [
	            'copy', 'csv', 'excel', 'pdf', 'print'
	        ],
	        "language": 
	        {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
	    } );
	//} );
</script>
