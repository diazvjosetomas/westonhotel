<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Clientes'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Clientes'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Clientes'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
	
		<dt><?php echo __('Tipo de cliente'); ?></dt>
		<dd>
			<?php echo h($cliente['Tipocliente']['denominacion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tipo de sub cliente'); ?></dt>
		<dd>
			<?php echo h($cliente['Tipoclientesub']['nombre']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dni'); ?></dt>
		<dd>
			<?php echo h($cliente['Cliente']['dni']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nombre'); ?></dt>
		<dd>
			<?php echo h($cliente['Cliente']['nombre_completo']); ?>
			&nbsp;
		</dd>

		<dt><?php echo __('Fecha nacimiento'); ?></dt>
		<dd>
			<?php echo h($cliente['Cliente']['fecha_nacimiento']); ?>
			&nbsp;
		</dd>
        <dt><?php echo __('Ciudad'); ?></dt>
		<dd>
			<?php echo h($cliente['Cliente']['ciudad']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Dirección'); ?></dt>
		<dd>
			<?php echo h($cliente['Cliente']['direccion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nacionalidad'); ?></dt>
		<dd>
			<?php echo h($cliente['Pai']['denominacion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Telf'); ?></dt>
		<dd>
			<?php echo h($cliente['Cliente']['telf']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($cliente['Cliente']['correo']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cod Pos'); ?></dt>
		<dd>
			<?php echo h($cliente['Cliente']['cod_pos']); ?>
			&nbsp;
		</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $cliente['Cliente']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Cliente'), array('action' => 'edit', $cliente['Cliente']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Cliente'), array('action' => 'delete', $cliente['Cliente']['id']), array(), __('Are you sure you want to delete # %s?', $cliente['Cliente']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Clientes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cliente'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipoclientes'), array('controller' => 'tipoclientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipocliente'), array('controller' => 'tipoclientes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipoclientesubs'), array('controller' => 'tipoclientesubs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipoclientesub'), array('controller' => 'tipoclientesubs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tarjetacreditos'), array('controller' => 'tarjetacreditos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tarjetacredito'), array('controller' => 'tarjetacreditos', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Tarjetacreditos'); ?></h3>
	<?php if (!empty($cliente['Tarjetacredito'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Cliente Id'); ?></th>
		<th><?php echo __('Tipotarjeta Id'); ?></th>
		<th><?php echo __('Num Tajeta'); ?></th>
		<th><?php echo __('Vencimiento'); ?></th>
		<th><?php echo __('Nombre Segun Tarjeta'); ?></th>
		<th><?php echo __('Cuatro Ultimos'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($cliente['Tarjetacredito'] as $tarjetacredito): ?>
		<tr>
			<td><?php echo $tarjetacredito['id']; ?></td>
			<td><?php echo $tarjetacredito['cliente_id']; ?></td>
			<td><?php echo $tarjetacredito['tipotarjeta_id']; ?></td>
			<td><?php echo $tarjetacredito['num_tajeta']; ?></td>
			<td><?php echo $tarjetacredito['vencimiento']; ?></td>
			<td><?php echo $tarjetacredito['nombre_segun_tarjeta']; ?></td>
			<td><?php echo $tarjetacredito['cuatro_ultimos']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'tarjetacreditos', 'action' => 'view', $tarjetacredito['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'tarjetacreditos', 'action' => 'edit', $tarjetacredito['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'tarjetacreditos', 'action' => 'delete', $tarjetacredito['id']), array(), __('Are you sure you want to delete # %s?', $tarjetacredito['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Tarjetacredito'), array('controller' => 'tarjetacreditos', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<?php */ ?>