<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Tipo temporadas'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Tipo temporadas'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Tipo temporadas'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								
		<dt><?php echo __('Denominación'); ?></dt>
		<dd>
			<?php echo h( $tipotemporada['Tipotemporada']['denominacion'] ); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha desde'); ?></dt>
		<dd>
			<?php echo h( formatdmy($tipotemporada['Tipotemporada']['fechadesde']) ); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha hasta'); ?></dt>
		<dd>
			<?php echo h( formatdmy($tipotemporada['Tipotemporada']['fechahasta']) ); ?>
			&nbsp;
		</dd>

						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $tipotemporada['Tipotemporada']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Tipotemporada'), array('action' => 'edit', $tipotemporada['Tipotemporada']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Tipotemporada'), array('action' => 'delete', $tipotemporada['Tipotemporada']['id']), array(), __('Are you sure you want to delete # %s?', $tipotemporada['Tipotemporada']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipotemporadas'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipotemporada'), array('action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>