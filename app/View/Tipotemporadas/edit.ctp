<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Tipo temporadas'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Tipo temporadas'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Edit Tipo temporadas'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Tipotemporada', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
			
                                echo $this->Form->input('id', array('class'=>'form-horizontal'));	
                                echo'<div class="form-group">';	
                                echo'<label class="control-label col-md-2" for="Tipotemporadadenominacion">Denominación</label>';		
                                echo'<div class="col-md-9">';			
                                echo $this->Form->input('denominacion', array('id'=>'Tipotemporadadenominacion', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
                                echo '</div>';	
                                echo '</div>';

                                echo'<div class="form-group">'; 
                                echo'<label class="control-label col-md-2" for="Tipotemporadadescripcion">Descripcion</label>';       
                                echo'<div class="col-md-9">';           
                                echo $this->Form->input('descripcion', array('id'=>'Tipotemporadadescripcion', 'div'=>false, 'label'=>false, 'class'=>'form-control','type'=>'textarea'));       
                                echo '</div>';  
                                echo '</div>';

                                echo'<div class="form-group">'; 
                                echo'<label class="control-label col-md-2" for="Tipotemporadaterminos">Términos y Condiciones</label>';       
                                echo'<div class="col-md-9">';           
                                echo $this->Form->input('terminos', array('id'=>'Tipotemporadaterminos', 'div'=>false, 'label'=>false, 'class'=>'form-control','type'=>'textarea'));       
                                echo '</div>';  
                                echo '</div>';

                                echo'<div class="form-group">'; 
                                echo'<label class="control-label col-md-2" for="Tipotemporadaminimonoches">Mínimo Noches</label>';       
                                echo'<div class="col-md-9">';           
                                echo $this->Form->input('minimonoches', array('id'=>'Tipotemporadaminimonoches', 'div'=>false, 'label'=>false, 'class'=>'form-control'));       
                                echo '</div>';  
                                echo '</div>';

                                
                                    echo'<div class="form-group">'; 
                                    echo'<label class="control-label col-md-2" for="Tipotemporadadiasanticipacion">Dias anticipacion para anular</label>';       
                                    echo'<div class="col-md-9">';           
                                    echo $this->Form->input('diasanticipacion', array('id'=>'Tipotemporadadiasanticipacion', 'div'=>false, 'label'=>false, 'class'=>'form-control'));       
                                    echo '</div>';  
                                    echo '</div>';

	

	?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<script>
    $("#Tipotemporadafechadesde").datepicker();
    $("#Tipotemporadafechahasta").datepicker();
</script>