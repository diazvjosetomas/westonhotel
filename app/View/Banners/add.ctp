<section class="content-header">
  <h1>
    Sistema de Encuestas
    <small><?php echo __('Banners'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Banners'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Add Banner'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Banner', array('class'=>'form-horizontal', 'enctype'=>'multipart/form-data')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
	
								echo'<div class="form-group">';	
								echo'<label class="control-label col-md-2" for="Bannerfile">file</label>';		
								echo'<div class="col-md-9">';			
								echo $this->Form->input('file', array('id'=>'Bannerfile', 'div'=>false, 'label'=>false, 'class'=>'form-control',  'type'=>'file'));		
								echo '</div>';	
								echo '</div>';
									
								echo'<div class="form-group">';	
								echo'<label class="control-label col-md-2" for="Bannerlinea_1">linea_1</label>';		
								echo'<div class="col-md-9">';			
								echo $this->Form->input('linea_1', array('id'=>'Bannerlinea_1', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
								echo '</div>';	
								echo '</div>';
									
								echo'<div class="form-group">';	
								echo'<label class="control-label col-md-2" for="Bannerlinea_2">linea_2</label>';		
								echo'<div class="col-md-9">';			
								echo $this->Form->input('linea_2', array('id'=>'Bannerlinea_2', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
								echo '</div>';	
								echo '</div>';
									?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Html->link(__('List Banners'), array('action' => 'index')); ?></li>
			</ul>
	</div>
<?php */ ?>