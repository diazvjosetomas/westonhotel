<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Factura tipo pagos'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Factura tipo pagos'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Factura tipo pagos'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
							<dt><?php echo __('Denominación'); ?></dt>
							<dd>
								<?php echo h($facturatipopago['Facturatipopago']['denominacion']); ?>
								&nbsp;
							</dd>		
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $facturatipopago['Facturatipopago']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Facturatipopago'), array('action' => 'edit', $facturatipopago['Facturatipopago']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Facturatipopago'), array('action' => 'delete', $facturatipopago['Facturatipopago']['id']), array(), __('Are you sure you want to delete # %s?', $facturatipopago['Facturatipopago']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Facturatipopagos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Facturatipopago'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Facturapagos'), array('controller' => 'facturapagos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Facturapago'), array('controller' => 'facturapagos', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Facturapagos'); ?></h3>
	<?php if (!empty($facturatipopago['Facturapago'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Factura Id'); ?></th>
		<th><?php echo __('Facturatipopago Id'); ?></th>
		<th><?php echo __('Fecha Pago'); ?></th>
		<th><?php echo __('Numero Pago'); ?></th>
		<th><?php echo __('Pago'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($facturatipopago['Facturapago'] as $facturapago): ?>
		<tr>
			<td><?php echo $facturapago['id']; ?></td>
			<td><?php echo $facturapago['factura_id']; ?></td>
			<td><?php echo $facturapago['facturatipopago_id']; ?></td>
			<td><?php echo $facturapago['fecha_pago']; ?></td>
			<td><?php echo $facturapago['numero_pago']; ?></td>
			<td><?php echo $facturapago['pago']; ?></td>
			<td><?php echo $facturapago['created']; ?></td>
			<td><?php echo $facturapago['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'facturapagos', 'action' => 'view', $facturapago['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'facturapagos', 'action' => 'edit', $facturapago['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'facturapagos', 'action' => 'delete', $facturapago['id']), array(), __('Are you sure you want to delete # %s?', $facturapago['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Facturapago'), array('controller' => 'facturapagos', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<?php */ ?>