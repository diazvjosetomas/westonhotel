<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Almacenes'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Almacenes'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Almacenes'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								
		<dt><?php echo __('tipo piso'); ?></dt>
		<dd>
			<?php echo h($proalmacene['Protipopiso']['denominacion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Denominación'); ?></dt>
		<dd>
			<?php echo h($proalmacene['Proalmacene']['denominacion']); ?>
			&nbsp;
		</dd>
		
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $proalmacene['Proalmacene']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Proalmacene'), array('action' => 'edit', $proalmacene['Proalmacene']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Proalmacene'), array('action' => 'delete', $proalmacene['Proalmacene']['id']), array(), __('Are you sure you want to delete # %s?', $proalmacene['Proalmacene']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Proalmacenes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Proalmacene'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Protipopisos'), array('controller' => 'protipopisos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Protipopiso'), array('controller' => 'protipopisos', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>