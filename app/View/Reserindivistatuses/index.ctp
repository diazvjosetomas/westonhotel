<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Reserindivistatuses'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Reserindivistatuses'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Reserindivistatuses'); ?> Registrados</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                    <p><?php echo $this->Html->link(__('Crear Nuevo'), array('action' => 'add'),  array('class'=>'btn btn-primary')); ?></p>

							<table id="data" class="table table-striped table-bordered" width="100%" cellspacing="0">
							<thead>
							<tr>
															<th><?php echo __('id'); ?></th>
															<th><?php echo __('reserindividuale_id'); ?></th>
															<th><?php echo __('reserstatusindividuale_id'); ?></th>
															<th><?php echo __('conftipopagoreserva_id'); ?></th>
															<th><?php echo __('fecha'); ?></th>
															<th><?php echo __('monto_penalidad'); ?></th>
															<th><?php echo __('observaciones'); ?></th>
															<th><?php echo __('created'); ?></th>
															<th><?php echo __('modified'); ?></th>
															<th class="actions"><?php echo __('Acción'); ?></th>
							</tr>
							</thead>
							<tbody>
							<?php foreach ($reserindivistatuses as $reserindivistatus): ?>
	<tr>
		<td><?php echo h($reserindivistatus['Reserindivistatus']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($reserindivistatus['Reserindividuale']['id'], array('controller' => 'reserindividuales', 'action' => 'view', $reserindivistatus['Reserindividuale']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($reserindivistatus['Reserstatusindividuale']['denominacion'], array('controller' => 'reserstatusindividuales', 'action' => 'view', $reserindivistatus['Reserstatusindividuale']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($reserindivistatus['Conftipopagoreserva']['denominacion'], array('controller' => 'conftipopagoreservas', 'action' => 'view', $reserindivistatus['Conftipopagoreserva']['id'])); ?>
		</td>
		<td><?php echo h($reserindivistatus['Reserindivistatus']['fecha']); ?>&nbsp;</td>
		<td><?php echo h($reserindivistatus['Reserindivistatus']['monto_penalidad']); ?>&nbsp;</td>
		<td><?php echo h($reserindivistatus['Reserindivistatus']['observaciones']); ?>&nbsp;</td>
		<td><?php echo h($reserindivistatus['Reserindivistatus']['created']); ?>&nbsp;</td>
		<td><?php echo h($reserindivistatus['Reserindivistatus']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $reserindivistatus['Reserindivistatus']['id']), array('class'=>'')); ?>
			| <?php echo $this->Html->link(__('Ver'), array('action' => 'view', $reserindivistatus['Reserindivistatus']['id']),   array('class'=>'')); ?>
			| <?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $reserindivistatus['Reserindivistatus']['id']), array('class'=>'', 'confirm'=>__('Esta seguro que desea eliminar el registro # %s?', $reserindivistatus['Reserindivistatus']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
							</tbody>
							</table>
						</div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->


<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Reserindivistatus'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Reserindividuales'), array('controller' => 'reserindividuales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reserindividuale'), array('controller' => 'reserindividuales', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Reserstatusindividuales'), array('controller' => 'reserstatusindividuales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reserstatusindividuale'), array('controller' => 'reserstatusindividuales', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Conftipopagoreservas'), array('controller' => 'conftipopagoreservas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Conftipopagoreserva'), array('controller' => 'conftipopagoreservas', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?><script type="text/javascript">
	//$(document).ready(function() {
	    $('#data').DataTable( {
	    	dom: 'Bfrtlip',
	        buttons: [
	            'copy', 'csv', 'excel', 'pdf', 'print'
	        ],
	        "language": 
	        {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
	    } );
	//} );
</script>
