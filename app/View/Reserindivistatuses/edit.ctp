<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Reserindivistatuses'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Reserindivistatuses'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Edit Reserindivistatus'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Reserindivistatus', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
			
echo $this->Form->input('id', array('class'=>'form-horizontal'));	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Reserindivistatusreserindividuale_id">reserindividuale_id</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('reserindividuale_id', array('id'=>'Reserindivistatusreserindividuale_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Reserindivistatusreserstatusindividuale_id">reserstatusindividuale_id</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('reserstatusindividuale_id', array('id'=>'Reserindivistatusreserstatusindividuale_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Reserindivistatusconftipopagoreserva_id">conftipopagoreserva_id</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('conftipopagoreserva_id', array('id'=>'Reserindivistatusconftipopagoreserva_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Reserindivistatusfecha">fecha</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('fecha', array('id'=>'Reserindivistatusfecha', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Reserindivistatusmonto_penalidad">monto_penalidad</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('monto_penalidad', array('id'=>'Reserindivistatusmonto_penalidad', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Reserindivistatusobservaciones">observaciones</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('observaciones', array('id'=>'Reserindivistatusobservaciones', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Reserindivistatus.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Reserindivistatus.id'))); ?></li>
				<li><?php echo $this->Html->link(__('List Reserindivistatuses'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('List Reserindividuales'), array('controller' => 'reserindividuales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reserindividuale'), array('controller' => 'reserindividuales', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Reserstatusindividuales'), array('controller' => 'reserstatusindividuales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reserstatusindividuale'), array('controller' => 'reserstatusindividuales', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Conftipopagoreservas'), array('controller' => 'conftipopagoreservas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Conftipopagoreserva'), array('controller' => 'conftipopagoreservas', 'action' => 'add')); ?> </li>
		</ul>
	</div>
<?php */ ?>