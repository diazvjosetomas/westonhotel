<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Cambiar status de reserva'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Cambiar status de reserva'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Add Cambiar status de reserva'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Reserindivistatus', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php 
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindivistatusreserindividuale_id">Habitación

										<a href="'.$this->Html->url("/Reserindividuales/edit_4/".$reserindividuales2[0]['Reserindividuale']['id']).'">								
										<i class="fa fa-refresh" aria-hidden="true"></i>
										</a>

									</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('habitacione_id',       array('id'=>'Reserindivistatusreserindividuale_id3', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'hidden', 'readonly'=>true, 'value'=>$reserindividuales2[0]['Habitacione']['id']));		
									echo $this->Form->input('reserindividuale_id2', array('id'=>'Reserindivistatusreserindividuale_id2', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text',   'readonly'=>true, 'value'=>$reserindividuales2[0]['Habitacione']['numhabitacion'].' - DNI:'.$reserindividuales2[0]['Cliente']['dni'].' '.$reserindividuales2[0]['Cliente']['nombre_completo']));		
									echo $this->Form->input('reserindividuale_id',  array('id'=>'Reserindivistatusreserindividuale_id',  'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'hidden', 'readonly'=>true, 'value'=>$reserindividuales2[0]['Reserindividuale']['id']));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Reserindivistatusreserstatusindividuale_id">Status</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('reserstatusindividuale_id', array('id'=>'Reserindivistatusreserstatusindividuale_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'empty'=>'--Seleccione--' , "onChange"=>'javascript:status_individual_habitacion();'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group" id="Reserindivistatusconftipopagoreserva_id_v" style="display:none;">';	
									echo'<label class="control-label col-md-2" for="Reserindivistatusconftipopagoreserva_id">Tipo de Pago</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('conftipopagoreserva_id', array('id'=>'Reserindivistatusconftipopagoreserva_id', 'div'=>false, 'label'=>false, 'class'=>'form-control','empty'=>'--Seleccione--',"onChange"=>'javascript:select_tipo_pago();'));		
									echo '</div>';	
									echo '</div>';
									//Tarjetas Guardadas
									echo'<div class="form-group"  id="tarjetascredito" style="display:none;">';	
									echo'<label class="control-label col-md-2" for="tarjetascredito">Tarjetas</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('tarjetacredito_id', array('id'=>'Tarjetascredito', 'div'=>false, 'label'=>false, 'class'=>'form-control','type'=>'text'));		
									echo '</div>';	
									echo '</div>';
									?>
									<!--------- PASAJEROS -------->
									<div class="form-group" id="pasajerosextras" style="display:none;">
										<label class="control-label col-md-2" for="Reserindividualecliente_id">Acompañantes </label>
					                    <div class="col-md-9">
					                        <div class="box box-solid box-default">
					                            <div class="box-header with-border">
					                                <h3 class="box-title">Nombre Completo</h3>
					                                <div class="box-tools pull-right">
					                                    <button id="newNombresBtn" class="btn btn-box-tool" data-toggle="tooltip" title="Agregar otro nombre" data-widget="chat-pane-toggle"><i class="glyphicon glyphicon-plus"></i></button>
					                                </div><!-- /.box-tools -->
					                            </div><!-- /.box-header -->
					                            <div class="box-body">
					                                <div id="nombres-container">
					                                        <div id="nombrecount">
						                                        <div class="form-group nombre-con" id="nombre_0">
						                                            <label class="control-label col-md-3">
						                                                Nombre Completo
						                                            </label>
						                                            <div class="col-md-8">
						                                                <div class='input-group col-md-12'>
						                                               		<input class="form-control text-box single-line" id="Nombres_0__Nombre" name="data[Reserindividuale][Nombre][]" placeholder="Ej: Juan Antonio Pacheco Lugo" require="require" type="text" value="" />
						                                                 </div>
						                                            </div>
						                                            <div class="col-md-1">
						                                                <button class="btn btn-danger btn-sm" onclick="if(confirm('Seguro que desea eliminar el registro')){ $('#nombre_0').remove(); $('#Nombres_0__Nombre').val(''); $('#Nombres_0__Nombre').removeAttr('required'); } return false;"><span class="glyphicon glyphicon-trash"></span></button>
						                                            </div>
						                                        </div>
					                                        </div>
					                                </div>
					                            </div>
					                        </div>
					                    </div>
				                    </div>

				                    <div class="form-group" id="motivoegreso_v" style="display:none;">
										<label class="control-label col-md-2" for="Reserindividualecliente_id">Motivo</label>
					                    <div class="col-md-9">
					                        <div class="box box-solid box-default">
					                            <div class="box-header with-border">
					                                <h3 class="box-title">Motivo del Egreso</h3>
					                                <div class="box-tools pull-right">
					                                    
					                                </div><!-- /.box-tools -->
					                            </div><!-- /.box-header -->
					                            <div class="box-body">
			                                        <div class="form-group nombre-con">
			                                            <div class="col-md-12">
			                                                <div class='input-group col-md-12'>
			                                               		<input class="form-control text-box single-line" id="motivoegreso" name="data[Auditoria][motivo]" placeholder="Motivo del egreso" require="require" type="text" value="" />
			                                                 </div>
			                                            </div>
			                                        </div>
					                            </div>
					                        </div>
					                    </div>
				                    </div>
								    <?php
									echo'<div class="form-group" id="Reserindivistatusfecha_v" style="display:none;">';	
									echo'<label class="control-label col-md-2" for="Reserindivistatusfecha">Fecha</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('fecha', array('id'=>'Reserindivistatusfecha', 'div'=>false ,'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group" id="Reserindivistatusmonto_penalidad_v" style="display:none;">';	
									echo'<label class="control-label col-md-2" for="Reserindivistatusmonto_penalidad">Monto penalidad</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('monto_penalidad', array('value'=>0, 'id'=>'Reserindivistatusmonto_penalidad', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group" id="Reserindivistatustotal_v" style="display:none;">';	
									echo'<label class="control-label col-md-2" for="Reserindivistatustotal">Total</label>';		
									echo'<div class="col-md-9" id="div-total">';			
									echo $this->Form->input('total', array('value'=>$reserindividuales2[0]['Reserindividuale']['total'], 'id'=>'Reserindivistatustotal', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group" id="Reserindivistatuspago_v" style="display:none;">';	
									echo'<label class="control-label col-md-2" for="Reserindivistatuspago">Pago</label>';		
									echo'<div class="col-md-9" id="div-pago">';			
									echo $this->Form->input('pago', array('value'=>0, 'id'=>'Reserindivistatuspago', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text',"onChange"=>'javascript:pago_individual_habitacion();'));			
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group" id="Reserindivistatusdebe_v" style="display:none;">';	
									echo'<label class="control-label col-md-2" for="Reserindivistatusdebe">Debe</label>';		
									echo'<div class="col-md-9" id="div-debe">';			
									echo $this->Form->input('debe', array('value'=>0, 'id'=>'Reserindivistatusdebe', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>true));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group" id="Reserindivistatusobservaciones_v" style="display:none;">';	
									echo'<label class="control-label col-md-2" for="Reserindivistatusobservaciones">Observaciones</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('observaciones', array('id'=>'Reserindivistatusobservaciones', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';

									
								?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index/'.$id)); ?>
	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                
					</div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->

<script type="text/javascript">

$("#Reserindivistatusfecha").datepicker();

</script>

