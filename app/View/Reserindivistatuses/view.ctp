<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Reserindivistatuses'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Reserindivistatuses'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Reserindivistatuses'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($reserindivistatus['Reserindivistatus']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Reserindividuale'); ?></dt>
		<dd>
			<?php echo $this->Html->link($reserindivistatus['Reserindividuale']['id'], array('controller' => 'reserindividuales', 'action' => 'view', $reserindivistatus['Reserindividuale']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Reserstatusindividuale'); ?></dt>
		<dd>
			<?php echo $this->Html->link($reserindivistatus['Reserstatusindividuale']['denominacion'], array('controller' => 'reserstatusindividuales', 'action' => 'view', $reserindivistatus['Reserstatusindividuale']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Conftipopagoreserva'); ?></dt>
		<dd>
			<?php echo $this->Html->link($reserindivistatus['Conftipopagoreserva']['denominacion'], array('controller' => 'conftipopagoreservas', 'action' => 'view', $reserindivistatus['Conftipopagoreserva']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha'); ?></dt>
		<dd>
			<?php echo h($reserindivistatus['Reserindivistatus']['fecha']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Monto Penalidad'); ?></dt>
		<dd>
			<?php echo h($reserindivistatus['Reserindivistatus']['monto_penalidad']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Observaciones'); ?></dt>
		<dd>
			<?php echo h($reserindivistatus['Reserindivistatus']['observaciones']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($reserindivistatus['Reserindivistatus']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($reserindivistatus['Reserindivistatus']['modified']); ?>
			&nbsp;
		</dd>
	
		</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $reserindivistatus['Reserindivistatus']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Reserindivistatus'), array('action' => 'edit', $reserindivistatus['Reserindivistatus']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Reserindivistatus'), array('action' => 'delete', $reserindivistatus['Reserindivistatus']['id']), array(), __('Are you sure you want to delete # %s?', $reserindivistatus['Reserindivistatus']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Reserindivistatuses'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reserindivistatus'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Reserindividuales'), array('controller' => 'reserindividuales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reserindividuale'), array('controller' => 'reserindividuales', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Reserstatusindividuales'), array('controller' => 'reserstatusindividuales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reserstatusindividuale'), array('controller' => 'reserstatusindividuales', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Conftipopagoreservas'), array('controller' => 'conftipopagoreservas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Conftipopagoreserva'), array('controller' => 'conftipopagoreservas', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>