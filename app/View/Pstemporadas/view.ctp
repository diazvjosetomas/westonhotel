<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Precio según temporadas'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Precio según temporadas'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Precio según temporadas'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								
		<dt><?php echo __('Tipo temporada'); ?></dt>
		<dd>
			<?php echo h($pstemporada['Tipotemporada']['denominacion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tipo habitación'); ?></dt>
		<dd>
			<?php echo h($pstemporada['Tipohabitacione']['denominacion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Precio'); ?></dt>
		<dd>
			<?php echo h($pstemporada['Pstemporada']['precio']); ?>
			&nbsp;
		</dd>

						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $pstemporada['Pstemporada']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Pstemporada'), array('action' => 'edit', $pstemporada['Pstemporada']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Pstemporada'), array('action' => 'delete', $pstemporada['Pstemporada']['id']), array(), __('Are you sure you want to delete # %s?', $pstemporada['Pstemporada']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Pstemporadas'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pstemporada'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipotemporadas'), array('controller' => 'tipotemporadas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipotemporada'), array('controller' => 'tipotemporadas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipohabitaciones'), array('controller' => 'tipohabitaciones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipohabitacione'), array('controller' => 'tipohabitaciones', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>