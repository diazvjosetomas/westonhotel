<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Consumos'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Consumos'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Consumos'); ?> Registrados</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                    <p><?php echo $this->Html->link(__('Crear Nuevo'), array('action' => 'add'),  array('class'=>'btn btn-primary')); ?></p>

							<table id="data" class="table table-striped table-bordered" width="100%" cellspacing="0">
							<thead>
							<tr>
															<th><?php echo __('Tipo habitación'); ?></th>
															<th><?php echo __('Habitación'); ?></th>
															<th><?php echo __('Cliente'); ?></th>
															<th><?php echo __('Num consumo'); ?></th>
															<th><?php echo __('Fecha consumo'); ?></th>
															<th><?php echo __('Total'); ?></th>
															<th class="actions"><?php echo __('Acción'); ?></th>
							</tr>
							</thead>
							<tbody>
							<?php foreach ($consumos as $consumo): ?>
	<tr>
		<td><?php echo h($consumo['Tipohabitacione']['denominacion']); ?></td>
		<td><?php echo h($consumo['Habitacione']['numhabitacion']); ?></td>
		<td><?php echo h($consumo['Reserindividuale']['Cliente']['nombre_completo']); ?></td>
		<td><?php echo h($consumo['Consumo']['num_consumo']); ?>&nbsp;</td>
		<td><?php echo h( formatdmy($consumo['Consumo']['fecha_consumo']) ); ?>&nbsp;</td>
		<td><?php echo h($consumo['Consumo']['total']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $consumo['Consumo']['id']), array('class'=>'')); ?>
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $consumo['Consumo']['id']),   array('class'=>'')); ?>
			 <?php  
	            $rol = $this->Session->read('ROL');
	            if($rol==1){
            ?>
				<?php echo $this->Html->link(__('Eliminar'), 1, array('onclick' => "eliminar(".$consumo['Consumo']['id']."); return false;", 'escape' => false));
			}
			?>
		</td>
	</tr>
<?php endforeach; ?>
							</tbody>
							</table>
						</div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->



<script type="text/javascript">
	//$(document).ready(function() {
	    $('#data').DataTable( {
	    	dom: 'Bfrtlip',
	        buttons: [
	            'copy', 'csv', 'excel', 'pdf', 'print'
	        ],
	        "language": 
	        {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
	    } );


	    function eliminar(consumo){
	    	bootbox.prompt({ 
			  title: "<h4><i class='fa fa-trash'></i> Desea eliminar el registro "+consumo+ "? Si es asi, Favor especifique un motivo!</h4>", 
			  callback: function(result){ 
			  	if( result === null  || result === ''){
			  		bootbox.alert({
    					message: "<h4><i class='fa fa-remove'></i> El registro no ha sido eliminado. Debe especificar un MOTIVO!</h4>",
    				});
			  	}else{
			  		document.location.href="/consumos/delete/"+consumo+'/'+result;
			  	}
			   }
			});
		}
	//} );
</script>



