<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Consumos'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Consumos'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Add Consumo'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Consumo', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
									
										echo'<div class="form-group" style="display:none !important;">';	
										echo'<label class="control-label col-md-2" for="Consumotipohabitacione_id">Tipo Habitación</label>';		
										echo'<div class="col-md-9">';			
										echo $this->Form->input('tipohabitacione_id', array('id'=>'Consumotipohabitacione_id', 'div'=>false, 'label'=>false, 'class'=>'form-control')); 	
										echo '</div>';	
										echo '</div>';
											
										echo'<div class="form-group" style="display:none !important;">';	
										echo'<label class="control-label col-md-2" for="Consumohabitacione_id">Habitación</label>';		
										echo'<div class="col-md-9" id="div-habitacion">';			
										echo $this->Form->input('habitacione_id', array('id'=>'Consumohabitacione_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));	
										echo '</div>';	
										echo '</div>';
											
										echo'<div class="form-group" style="display:none !important;">';	
										echo'<label class="control-label col-md-2" for="Consumoreserindividuale_id">Cliente</label>';		
										echo'<div class="col-md-9" id="div-cliente">';			
										echo $this->Form->input('reserindividuale_id', array('id'=>'Consumoreserindividuale_id', 'div'=>false, 'label'=>false,'class'=>'form-control'));	
										
										echo '</div>';	
										echo '</div>';
											
										echo'<div class="form-group">';	
										echo'<label class="control-label col-md-2" for="Consumoano_consumo">Año consumo</label>';		
										echo'<div class="col-md-9">';			
										echo $this->Form->input('ano_consumo', array('id'=>'Consumoano_consumo', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'value'=>date('Y'), 'readonly'=>true));		
										echo '</div>';	
										echo '</div>';
											
										// echo'<div class="form-group" >';	
										// echo'<label class="control-label col-md-2" for="Consumonum_consumo">Número consumo</label>';		
										// echo'<div class="col-md-9">';			
										echo $this->Form->input('num_consumo', array('id'=>'Consumonum_consumo','type'=>'hidden', 'value'=>$num_consumo));		
										// echo '</div>';	
										// echo '</div>';
											
										echo'<div class="form-group">';	
										echo'<label class="control-label col-md-2" for="Consumofecha_consumo">Fecha consumo</label>';		
										echo'<div class="col-md-9">';			
										echo $this->Form->input('fecha_consumo', array('id'=>'Consumofecha_consumo', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
										echo '</div>';	
										echo '</div>';
										?>
										</div>
										</div>
						                <div class="row">
						                <label class="control-label col-md-2" for="Consumonum_factura">Productos</label>
						                <div class="col-md-9 col-sm-9 col-xs-12">
						                    <div class="box box-solid box-default">
						                        <div class="box-header with-border">
						                            <h3 class="box-title">Productos</h3>
						                            <div class="box-tools pull-right">
						                                <button id="newProductoBtn" class="btn btn-box-tool" data-toggle="tooltip" title="Agregar otro producto" data-widget="chat-pane-toggle"><i class="glyphicon glyphicon-plus"></i></button>
						                            </div><!-- /.box-tools -->
						                        </div><!-- /.box-header -->
						                        <div class="box-body">
						                            <div id="productos-container">
						                                <div id="productoscount">
						                                    <div class="form-group pro-con" id="producto_0">
						                                        <label class="control-label col-md-1">Producto</label>
						                                        <div class="col-md-2">
						                                            <select class="form-control tel-0" id="Productos_0__Pro" name="data[Consumo][Productos][0][pro]" required="required" onchange="Javascript:precio_p(0);">
																	<option value="">--Seleccione--</option>
																	<?php

							            	                             foreach($proproductos as $proproducto){
							            	                             	echo "  '<option value=\"".$proproducto['Proproductos']['id']."\">".$proproducto['Proproductos']['denominacion']."</option>'+   ";
							            	                             }
						            	                             ?>
																	</select>
						                                        </div>
						                                        <label class="control-label col-md-1">Cantidad</label>
						                                        <div class="col-md-1">
																	<input class="form-control pro-0-ext text-box single-line"  id="Productos_0__Cantidad" name="data[Consumo][Productos][0][cant]" required="required" type="text" value="0"  onchange="Javascript:total_p(0);"/>
						                                        </div>
						                                        <label class="control-label col-md-1">Pre. U.</label>
						                                        <div class="col-md-2">
						                                            <input class="form-control pro-0-ext text-box single-line"  id="Productos_0__Precio"   name="data[Consumo][Productos][0][pre]" required="required" type="text" value="0" readonly=true />
						                                        </div>
						                                        <label class="control-label col-md-1">Total</label>
						                                        <div class="col-md-2">
						                                            <input class="form-control pro-0-ext text-box single-line" id="Productos_0__Total" name="data[Consumo][Productos][0][total]"  required="required" type="text" value="0" readonly=true />
						                                        </div>
						                                        <div class="col-md-1">
						                                            <button class="btn btn-danger btn-sm" onclick="return false;"><span class="glyphicon glyphicon-trash"></span></button>
						                                        </div>
						                                    </div>
						                                </div>
						                            </div>
						                        </div>
						                    </div>
						                </div>
						           		</div>
							            <div class="row">
							            <div class="col-md-12">
								<?php
											
										echo'<div class="form-group">';	
										echo'<label class="control-label col-md-2" for="Consumoobservaciones">Observaciones</label>';		
										echo'<div class="col-md-9">';			
										echo $this->Form->input('observaciones', array('id'=>'Consumoobservaciones', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
										echo '</div>';	
										echo '</div>';
											
										echo'<div class="form-group">';	
										echo'<label class="control-label col-md-2" for="Consumototal">Total</label>';		
										echo'<div class="col-md-9">';			
										echo $this->Form->input('total', array('value'=>0, 'id'=>'Compratotal', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly'=>true, 'type'=>'text'));		
										echo '</div>';	
										echo '</div>';
								?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    
	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<script type="text/javascript">
$(document).ready(function() {
    $("#newProductoBtn").click(function (e) {
        var MaxInputs       = 100;
        var x = $("#productos-container #productoscount").length + 1;
        var FieldCount = x-1;
        if(x <= MaxInputs) //max input box allowed
        {
            FieldCount++;
            $("#productos-container").append('<div class="form-group pro-con" id="producto_'+FieldCount+'">'+
			                                        '<label class="control-label col-md-1">Producto</label>'+
			                                        '<div class="col-md-2">'+
			                                            '<select class="form-control tel-'+FieldCount+'" id="Productos_'+FieldCount+'__Pro" name="data[Consumo][Productos]['+FieldCount+'][pro]" onchange="Javascript:precio_p('+FieldCount+');">'+
														'<option value="">--Seleccione--</option>'+
														<?php
				            	                             foreach($proproductos as $proproducto){
				            	                             	echo "  '<option value=\"".$proproducto['Proproductos']['id']."\">".$proproducto['Proproductos']['denominacion']."</option>'+   ";
				            	                             }
			            	                             ?>
														'</select>'+
			                                        '</div>'+
			                                        '<label class="control-label col-md-1">Cantidad</label>'+
			                                        '<div class="col-md-1">'+
														'<input class="form-control pro-'+FieldCount+'-ext text-box single-line"  id="Productos_'+FieldCount+'__Cantidad" name="data[Consumo][Productos]['+FieldCount+'][cant]" type="text" value="0" onchange="Javascript:total_p('+FieldCount+');" />'+
			                                        '</div>'+
			                                        '<label class="control-label col-md-1">Pre. U.</label>'+
			                                        '<div class="col-md-2">'+
			                                            '<input class="form-control producto-mask text-box single-line" id="Productos_'+FieldCount+'__Precio" name="data[Consumo][Productos]['+FieldCount+'][pre]"  type="text" value="0" readonly=true />'+
			                                        '</div>'+
			                                        '<label class="control-label col-md-1">Total</label>'+
			                                        '<div class="col-md-2">'+
			                                            '<input class="form-control producto-mask text-box single-line" id="Productos_'+FieldCount+'__Total" name="data[Consumo][Productos]['+FieldCount+'][total]"  type="text" value="0" readonly=true />'+
			                                        '</div>'+
			                                        '<div class="col-md-1">'+
			                                            '<button class="btn btn-danger btn-sm" onclick="$(\'#producto_'+FieldCount+'\').remove(); total_p('+FieldCount+'); return false;"><span class="glyphicon glyphicon-trash"></span></button>'+
			                                        '</div>'+
			                                    '</div>');
            x++; //text box increment
        }
        return false;
    });
});
</script>