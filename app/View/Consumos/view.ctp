<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Consumos'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Consumos'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Consumos'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
							<dt><?php echo __('Tipo de habitación'); ?></dt>
							<dd>
								<?php echo h($consumo['Tipohabitacione']['denominacion']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Habitación'); ?></dt>
							<dd>
								<?php echo h($consumo['Habitacione']['numhabitacion']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Cliente'); ?></dt>
							<dd>
								<?php echo h($consumo['Reserindividuale']['id']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Año Consumo'); ?></dt>
							<dd>
								<?php echo h($consumo['Consumo']['ano_consumo']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Número Consumo'); ?></dt>
							<dd>
								<?php echo h($consumo['Consumo']['num_consumo']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Fecha Consumo'); ?></dt>
							<dd>
								<?php echo h( formatdmy($consumo['Consumo']['fecha_consumo'])); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Observaciones'); ?></dt>
							<dd>
								<?php echo h($consumo['Consumo']['observaciones']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Total'); ?></dt>
							<dd>
								<?php echo h($consumo['Consumo']['total']); ?>
								&nbsp;
							</dd>
		
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $consumo['Consumo']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Consumo'), array('action' => 'edit', $consumo['Consumo']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Consumo'), array('action' => 'delete', $consumo['Consumo']['id']), array(), __('Are you sure you want to delete # %s?', $consumo['Consumo']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Consumos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Consumo'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipohabitaciones'), array('controller' => 'tipohabitaciones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipohabitacione'), array('controller' => 'tipohabitaciones', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Habitaciones'), array('controller' => 'habitaciones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Habitacione'), array('controller' => 'habitaciones', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Reserindividuales'), array('controller' => 'reserindividuales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reserindividuale'), array('controller' => 'reserindividuales', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Consumoproductos'), array('controller' => 'consumoproductos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Consumoproducto'), array('controller' => 'consumoproductos', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Consumoproductos'); ?></h3>
	<?php if (!empty($consumo['Consumoproducto'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Consumo Id'); ?></th>
		<th><?php echo __('Proproducto Id'); ?></th>
		<th><?php echo __('Cantidad'); ?></th>
		<th><?php echo __('Precio'); ?></th>
		<th><?php echo __('Total'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($consumo['Consumoproducto'] as $consumoproducto): ?>
		<tr>
			<td><?php echo $consumoproducto['id']; ?></td>
			<td><?php echo $consumoproducto['consumo_id']; ?></td>
			<td><?php echo $consumoproducto['proproducto_id']; ?></td>
			<td><?php echo $consumoproducto['cantidad']; ?></td>
			<td><?php echo $consumoproducto['precio']; ?></td>
			<td><?php echo $consumoproducto['total']; ?></td>
			<td><?php echo $consumoproducto['created']; ?></td>
			<td><?php echo $consumoproducto['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'consumoproductos', 'action' => 'view', $consumoproducto['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'consumoproductos', 'action' => 'edit', $consumoproducto['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'consumoproductos', 'action' => 'delete', $consumoproducto['id']), array(), __('Are you sure you want to delete # %s?', $consumoproducto['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Consumoproducto'), array('controller' => 'consumoproductos', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<?php */ ?>