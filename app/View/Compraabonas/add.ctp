<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Contabilizar pago'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Contabilizar pago'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Add Contabilizar pago'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Compraabona', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Compraabonacompra_id">Orden de Compra</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('compra_id2', array('id'=>'Compraabonacompra_id2', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'readonly'=>true,  'value'=>$compras_d[0]['Compra']['num_compra'].' - Proveedor: '.$compras_d[0]['Proveedore']['razon_social']));		
									echo $this->Form->input('compra_id', array( 'id'=>'Compraabonacompra_id',  'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'hidden', 'value'=>$id));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Compraabonacompratipopago_id">Tipo de Pago</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('compratipopago_id', array('id'=>'Compraabonacompratipopago_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'empty'=>'--Seleccione--',"onChange"=>'javascript:compra_pago();'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Compraabonanum_pago">Número de pago</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('num_pago', array('value'=>$num_pago, 'id'=>'Compraabonanum_pago', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Compraabonafecha_pago">Fecha de pago</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('fecha_pago', array('value'=>date('Y-m-d'),'id'=>'Compraabonafecha_pago', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
									?>
	                                	<div class="form-group">
		                                	<label class="control-label col-md-2" for="Compraabonahistorial">Historial</label>
			                                <div class="col-md-9">
			                                    <table id="data" class="table table-striped table-bordered" width="100%" cellspacing="0">
												<tr>
												        <th><?php echo __('Tipo de Pago');?></th>
														<th><?php echo __('Número de Pago');?></th>
														<th><?php echo __('Fecha de Pago');?></th>
														<th><?php echo __('Monto de Pago');?></th>
														<th><?php echo __('Núm Cheque');?></th>
														<th><?php echo __('Fecha cobro cheque');?></th>
														<th><?php echo __('Núm transferencia');?></th>
												</tr>
												<?php
												foreach ($compraabonas as $compraabona): ?>
												<tr>
												    <td><?php echo h($compraabona['Compratipopago']['denominacion']); ?>&nbsp;</td>
												    <td><?php echo h($compraabona['Compraabona']['num_pago']); ?>&nbsp;</td>
													<td><?php echo h($compraabona['Compraabona']['fecha_pago']); ?>&nbsp;</td>
													<td><?php echo h($compraabona['Compraabona']['cancelado']); ?>&nbsp;</td>
													<td><?php echo h($compraabona['Compraabona']['num_cheque']); ?>&nbsp;</td>
													<td><?php echo h($compraabona['Compraabona']['fecha_cobro_cheque']); ?>&nbsp;</td>
													<td><?php echo h($compraabona['Compraabona']['num_transferencia']); ?>&nbsp;</td>
												</tr>
												<?php endforeach; ?>
												</table>
											</div>
                                   		</div>
									<?php
                                    
									echo'<div class="form-group" id="Reserindivistatustransferencia_v" style="display:none;">';	
									echo'<label class="control-label col-md-2" for="Compraabonanum_transferencia">Número transferencia</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('num_transferencia', array('id'=>'Compraabonanum_transferencia', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group" id="Reserindivistatuscheque_v" style="display:none;">';	
									echo'<label class="control-label col-md-2" for="Compraabonanum_cheque">Número cheque</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('num_cheque', array('id'=>'Compraabonanum_cheque', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group" id="Reserindivistatuscheque2_v" style="display:none;">';	
									echo'<label class="control-label col-md-2" for="Compraabonafecha_cobro_cheque">Fecha cobro cheque</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('fecha_cobro_cheque', array('id'=>'Compraabonafecha_cobro_cheque', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Compraabonatotal">Total Compra</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('total', array('value'=>$total,'readonly'=>true, 'id'=>'Compraabonatotal', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text'));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Compraabonadeuda2">Monto Deuda</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('deuda2', array('value'=>$deuda, 'readonly'=>true, 'id'=>'Compraabonadeuda2', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Compraabonacancelado">Monto Cancelar</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('cancelado', array('value'=>'0','id'=>'Compraabonacancelado', 'div'=>false, 'label'=>false, 'class'=>'form-control',"onChange"=>'javascript:compra_pago_deuda();', 'type'=>'text'));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Compraabonadeuda">Monto Saldo</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('deuda',  array('value'=>$deuda, 'readonly'=>true, 'id'=>'Compraabonadeuda', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Compraabonadescripcion">Descripción</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('descripcion', array('id'=>'Compraabonadescripcion', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
								?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index/'.$id)); ?>
	                                     <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Html->link(__('List Compraabonas'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('List Compras'), array('controller' => 'compras', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Compra'), array('controller' => 'compras', 'action' => 'add')); ?> </li>
		</ul>
	</div>
<?php */ ?>