<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Contabilizar pago'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Contabilizar pago'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Contabilizar pago'); ?> Registrados</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">

							<table id="data" class="table table-striped table-bordered" width="100%" cellspacing="0">
							<thead>
							<tr>
															<th><?php echo __('Compra'); ?></th>
															<th><?php echo __('Proveedor'); ?></th>
															<th><?php echo __('Núm pago'); ?></th>
															<th><?php echo __('Fecha pago'); ?></th>
															<th><?php echo __('Núm cheque'); ?></th>
															<th><?php echo __('Fecha cobro cheque'); ?></th>
															<th><?php echo __('Total'); ?></th>
															<th><?php echo __('Cancelado'); ?></th>
															<th><?php echo __('Deuda'); ?></th>
															<th class="actions"><?php echo __('Acción'); ?></th>
							</tr>
							</thead>
							<tbody>
							<?php 

							foreach ($compraabonas as $compraabona): ?>
	<tr>
		<td><?php echo h($compraabona['Compra']['num_compra']); ?></td>
		<td><?php echo h($compraabona['Compra']['Proveedore']['razon_social']); ?></td>
		<td><?php echo h($compraabona['Compraabona']['num_pago']); ?>&nbsp;</td>
		<td><?php echo h( formatdmy($compraabona['Compraabona']['fecha_pago']) ); ?>&nbsp;</td>
		<td><?php echo h($compraabona['Compraabona']['num_cheque']); ?>&nbsp;</td>
		<td><?php echo h($compraabona['Compraabona']['fecha_cobro_cheque']); ?>&nbsp;</td>
		<td><?php echo h($compraabona['Compraabona']['total']); ?>&nbsp;</td>
		<td><?php echo h($compraabona['Compraabona']['cancelado']); ?>&nbsp;</td>
		<td><?php echo h($compraabona['Compraabona']['deuda']); ?>&nbsp;</td>
		<td class="actions">
			<?php 
			//echo $this->Html->link(__('Editar'), array('action' => 'edit', $compraabona['Compraabona']['id']), array('class'=>'')); 
			?>
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $compraabona['Compraabona']['id']),   array('class'=>'')); ?>
			<?php 
			//echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $compraabona['Compraabona']['id']), array('class'=>'', 'confirm'=>__('Esta seguro que desea eliminar el registro # %s?', $compraabona['Compraabona']['id']))); 
			?>
		</td>
	</tr>
<?php endforeach; ?>
							</tbody>
							</table>
						</div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->


<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Compraabona'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Compras'), array('controller' => 'compras', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Compra'), array('controller' => 'compras', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?><script type="text/javascript">
	//$(document).ready(function() {
	    $('#data').DataTable( {
	    	dom: 'Bfrtlip',
	        buttons: [
	            'copy', 'csv', 'excel', 'pdf', 'print'
	        ],
	        "language": 
	        {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
	    } );
	//} );
</script>
