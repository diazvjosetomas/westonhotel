<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Contabilizar pago'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Contabilizar pago'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Edit Contabilizar pago'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Compraabona', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
									echo $this->Form->input('id', array('class'=>'form-horizontal'));	
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Compraabonacompra_id">compra_id</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('compra_id', array('id'=>'Compraabonacompra_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Compraabonanum_pago">num_pago</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('num_pago', array('id'=>'Compraabonanum_pago', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Compraabonafecha_pago">fecha_pago</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('fecha_pago', array('id'=>'Compraabonafecha_pago', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Compraabonanum_cheque">num_cheque</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('num_cheque', array('id'=>'Compraabonanum_cheque', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Compraabonafecha_cobro_cheque">fecha_cobro_cheque</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('fecha_cobro_cheque', array('id'=>'Compraabonafecha_cobro_cheque', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Compraabonadescripcion">descripcion</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('descripcion', array('id'=>'Compraabonadescripcion', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Compraabonacancelado">cancelado</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('cancelado', array('id'=>'Compraabonacancelado', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Compraabonadeuda">deuda</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('deuda', array('id'=>'Compraabonadeuda', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
								?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Compraabona.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Compraabona.id'))); ?></li>
				<li><?php echo $this->Html->link(__('List Compraabonas'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('List Compras'), array('controller' => 'compras', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Compra'), array('controller' => 'compras', 'action' => 'add')); ?> </li>
		</ul>
	</div>
<?php */ ?>