<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Contabilizar pago'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Contabilizar pago'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Contabilizar pago'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
							<dt><?php echo __('Compra'); ?></dt>
							<dd>
								<?php echo h($compraabona['Compra']['num_compra']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Núm Pago'); ?></dt>
							<dd>
								<?php echo h($compraabona['Compraabona']['num_pago']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Fecha Pago'); ?></dt>
							<dd>
								<?php echo h($compraabona['Compraabona']['fecha_pago']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Núm Cheque'); ?></dt>
							<dd>
								<?php echo h($compraabona['Compraabona']['num_cheque']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Fecha Cobro Cheque'); ?></dt>
							<dd>
								<?php echo h($compraabona['Compraabona']['fecha_cobro_cheque']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Descripcion'); ?></dt>
							<dd>
								<?php echo h($compraabona['Compraabona']['descripcion']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Total'); ?></dt>
							<dd>
								<?php echo h($compraabona['Compraabona']['total']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Cancelado'); ?></dt>
							<dd>
								<?php echo h($compraabona['Compraabona']['cancelado']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Deuda'); ?></dt>
							<dd>
								<?php echo h($compraabona['Compraabona']['deuda']); ?>
								&nbsp;
							</dd>
						</dl>
						<p>
                            	<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Compraabona'), array('action' => 'edit', $compraabona['Compraabona']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Compraabona'), array('action' => 'delete', $compraabona['Compraabona']['id']), array(), __('Are you sure you want to delete # %s?', $compraabona['Compraabona']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Compraabonas'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Compraabona'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Compras'), array('controller' => 'compras', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Compra'), array('controller' => 'compras', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>