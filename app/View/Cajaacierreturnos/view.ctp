<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Corte X Y'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Corte X Y'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Corte X Y'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
		<dt><?php echo __('Caja'); ?></dt>
		<dd>
			<?php echo h($cajaacierreturno['Caja']['denominacion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Turno'); ?></dt>
		<dd>
			<?php echo h($cajaacierreturno['Cajaturno']['denominacion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo h($cajaacierreturno['User']['username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Monto'); ?></dt>
		<dd>
			<?php echo h($cajaacierreturno['Cajaacierreturno']['monto']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha'); ?></dt>
		<dd>
			<?php echo h($cajaacierreturno['Cajaacierreturno']['created']); ?>
			&nbsp;
		</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $cajaacierreturno['Cajaacierreturno']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Cajaacierreturno'), array('action' => 'edit', $cajaacierreturno['Cajaacierreturno']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Cajaacierreturno'), array('action' => 'delete', $cajaacierreturno['Cajaacierreturno']['id']), array(), __('Are you sure you want to delete # %s?', $cajaacierreturno['Cajaacierreturno']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Cajaacierreturnos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cajaacierreturno'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cajas'), array('controller' => 'cajas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Caja'), array('controller' => 'cajas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cajaturnos'), array('controller' => 'cajaturnos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cajaturno'), array('controller' => 'cajaturnos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>