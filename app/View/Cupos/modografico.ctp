<?php 
  
      $mes    = date('m');
      $anho   = date('Y');
      $numero = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
  if ($mescontroller != "" AND $anhocontroller != "") {
      $mes    = $mescontroller;
      $anho   = $anhocontroller;
      $numero = cal_days_in_month(CAL_GREGORIAN, $mescontroller, $anhocontroller);
  }



  $mesdenominacion = array(
                          '01'=>'Enero',
                          '02'=>'Febrero',
                          '03'=>'Marzo',
                          '04'=>'Abril',
                          '05'=>'Mayo',
                          '06'=>'Junio',
                          '07'=>'Julio',
                          '08'=>'Agosto',
                          '09'=>'Septiembre',
                          '10'=>'Octubre',
                          '11'=>'Noviembre',
                          '12'=>'Diciembre'
  
                            );
    


    
 ?>

  <input type="hidden" id="hiddenmes"        value="<?=$mes?>">
  <input type="hidden" id="hiddenanhoselect" value="<?=$anho?>">
 

 <style type="text/css">

 table tr th{
  width: 12px !important;
  text-align: center;
  font-size: 12px;
  color: white;
  font-weight: bold;
  background-color: #1a66ff;
  border-color: black;
 }

 table tr td{
  border-style: solid;
  border-color: black;
 }

 table tr td:hover{
 background-color: silver;
 color:#030303;
 cursor: pointer;


 }

 .pantalla{
  font-size: 5% !important;
 }
 </style>



 <div class="container" class="pantalla">
   

<div class="box box-content box-primary">

  
  <div class="container">
    <h2>Establece Cupos y Precios por Defecto <a class="btn btn-success" style="cursor: pointer;" onclick="modalPrecioCupoDefecto()">Aqui</a>  </h2>

    <div class="row">
      <div class="col-md-4" > Cupos por defecto <b id="defcupos"> 0</b> </div>
      <div class="col-md-4" > Precio Pax por defecto <b id="defpreciopax">  0 </b></div>
      <div class="col-md-4" > Precio Pax Adicional por defecto <b id="defpreciopaxad">  0 </b></div>
    </div>
    

    <h2>Configuracion de Cupo y Precio para el mes de <b> <?=$mesdenominacion[$mes]?> </b> </h2>

    <div class="container">
      

      <form action="/Cupos/modografico/" method="POST">
        

    
      <div class="row">

        <div class="col-md-3">
          <select name="mes" id="mes" class='form-control'>
            <?php 

              $meses = array(
                          1  => 'Enero', 
                          2  => 'Febrero', 
                          3  => 'Marzo', 
                          4  => 'Abril', 
                          5  => 'Mayo', 
                          6  => 'Junio', 
                          7  => 'Julio', 
                          8  => 'Agosto', 
                          9  => 'Septiembre', 
                          10 => 'Octubre', 
                          11 => 'Noviembre', 
                          12 => 'Diciembre'
              );

              for ($i=1; $i <= 12 ; $i++) { 

                if ( $i == $mes ) {
        
                    echo "<option selected value=".$i." >".$meses[$i]."</option>";
                  
                }else{
                    echo "<option value=".$i." >".$meses[$i]."</option>";

                }
            } ?>
          </select>
        </div>

        <div class="col-md-3">

          <select name="anhoselect" id="anhoselect" class="form-control">
            <?php 

              for ($i=2018; $i <= 2030 ; $i++) { 

                if ( $i == $anho ) {
                    
                    echo "<option selected value=".$i." >".$i."</option>";
                  
                }else{
                    echo "<option value=".$i." >".$i."</option>";

                }
              }

             ?>
          </select>
          
        </div>

        <div class="col-md-3">
          <select name="tipotemporadas" id="idtipotemporadas" class="form-control">
            <?php foreach ($tipotemporadas as $key): ?>
              <?php if ($selecttipotemporadas == $key['Tipotemporada']['id']): ?>
                
                <option selected value="<?=$key['Tipotemporada']['id']?>"> <?=$key['Tipotemporada']['denominacion']?> </option>
              
              <?php endif ?>

              <?php if ($selecttipotemporadas != $key['Tipotemporada']['id']): ?>
                
                <option value="<?=$key['Tipotemporada']['id']?>"> <?=$key['Tipotemporada']['denominacion']?> </option>
              
              <?php endif ?>

            <?php endforeach ?>
          </select>
        </div>

        <div class="col-md-3">
          <button onclick="getdata()" class="btn btn-success">
            Acceder
          </button>
        </div>

      </div>

      </form>
        <br>
        <br>
        
        
        <a class="btn btn-warning" onclick="modalPrecioFechas()">Establezca precios por rango de fechas</a>
    </div>

  </div>



  <br>

</div>


<div class="box box-body box-success" style="overflow-y: scroll;">

  <table class="table table-bordered table-striped table-responsive">

  

        

        <?php foreach ($tipohab as $key): ?>

        <tr >
            <th>
              <?=$key['Tipohabitacione']['denominacion'] ?>
            </th>
            
            <?

             for ($i=1; $i <= $numero ; $i++) { 
          
               echo "<th>".$i.'<br> '.nomdia($i).'</th>';

            }?> 
        </tr>

 
        <tr>
    
        <td>
          
          Cupo

        </td>
               <?php for ($i=1; $i <= $numero ; $i++) { 
               $th = $key['Tipohabitacione']['id'];
               if ($i < 10) {
                 $i = '0'.$i;
               }
               echo "<td id='tdc_".$anho.'-'.$mes."-".$i."_".$key['Tipohabitacione']['id']."' onclick='cupo($i, $th)'></td>";

              } ?>
        </tr>

  <tr>
    

              <td>
                Precio
              </td>
               <?php for ($i=1; $i <= $numero ; $i++) { 
               $th = $key['Tipohabitacione']['id'];
               if ($i < 10) {
                 $i = '0'.$i;
               }
               echo "<td id='tdp_".$anho.'-'.$mes."-".$i."_".$key['Tipohabitacione']['id']."' onclick='precio($i, $th)'></td>";

              } ?>

  


    </tr>
        <?php endforeach ?>

  </table>

</div>
 
</div>



<?php 

  function nomdia($dia){
    $mes    = date('m');
    $anho   = date('Y');
    if ($dia < 10) {
      $dia = '0'.$dia;
    }

  

    $fecha = $dia.'-'.$mes.'-'.$anho;
    
    $fechats = strtotime($fecha);

    switch (date('w', $fechats)){ 
        case 0: return "D"; break; 
        case 1: return "L"; break; 
        case 2: return "M"; break; 
        case 3: return "M"; break; 
        case 4: return "J"; break; 
        case 5: return "V"; break; 
        case 6: return "S"; break; 
    }  

  }
  



 ?>


<input type="hidden" id="diaseleccionado">
<input type="hidden" id="thseleccionado">


 <script>
  
   function modalPrecioCupoDefecto(){
      $("#cupodef").val('');
      $("#preciopaxdef").val('');
      $("#preciopaxadicdef").val('');
      $("#modalPrecioCupoDefecto").modal('show');
   }
     
    function modalPrecioFechas(){
        $("#modalPrecioCupoFechas").modal('show');
    }
     
     function enviardatafechas(){
         var diainicio = $("#diainicio").val();
         
         var diafin    = $("#diafin").val();
         
         if(diafin < diainicio){
             alert('Dia final no puede ser menor que el dia de inicio');   
             return false;
             
         }
         
         if(diafin == diainicio){
             alert('Dia final debe ser mayor a dia inicio');    
             return false;
         }
         
         var cantidadcupos    = $("#cupofec").val();
         
         var preciopax        = $("#preciopaxfec").val();
         
         var preciopaxadicfec = $("#preciopaxadicfec").val();
         
         var idtipohabitacion = $("#idtipohabitacion").val();
         
         if (cantidadcupos == ''){
             alert('Este valor no puede quedar vacio  CANTIDAD DE CUPOS  ');
             $("#cupofec").focus();
             return false;
            
         }
         if (preciopax == ''){
             alert('Este valor no puede quedar vacio PRECIO POR PAX');
             $("#preciopaxfec").focus();
             
         }
         if (preciopaxadicfec == ''){
             alert('Este valor no puede quedar vacio PRECIO POR PAX ADICIONAL');
             $("#preciopaxadicfec").focus();
             
         }
         var anhio = <?=$anho?>;
         var mes   = <?=$mes?>;
         
         var idtipotemporada = $("#idtipotemporadas").val();
         $.ajax({
             url  : '/Cupos/rangofechas/',
             type : 'POST',
             data : {
                 
                 diainicio        : diainicio,
                 diafin           : diafin,
                 cantidadcupos    : cantidadcupos,
                 preciopax        : preciopax, 
                 preciopaxadicfec : preciopaxadicfec,
                 idtipohabitacion : idtipohabitacion,
                 anhio            : anhio,
                 mes              : mes,
                 idtipotemporada  : idtipotemporada
                 
                 
                 
             },
             success:function(response){
                 getdata();
                 getdatadefault();
             }
         })
     }




   function cupo(dia, th){
    $("#diaseleccionado").val(dia);
    $("#thseleccionado").val(th);
    $("#cupo").val('');
    $("#myModalCupo").modal('show');
   }


   function precio(dia, th){
    $("#diaseleccionado").val(dia);
    $("#thseleccionado").val(th);
    $("#precio").val('');
    $("#preciopaxadicional").val('');
    $("#myModalPrecio").modal('show');

   }


   function enviarcupo(){

    var dato = $("#cupo").val();


    if (dato == '') {
      alert('Debe ingresar un dato');
      return false;
    }


    var mes  = $("#hiddenmes")         .val();
    var anho = $("#hiddenanhoselect")  .val();

    if (mes < 0) {
      mes = '0'+mes;
    }

    var diaseleccionado = $("#diaseleccionado").val();
    if (diaseleccionado < 0) {
      diaseleccionado = '0'+diaseleccionado;
    }
    var thseleccionado = $("#thseleccionado").val();
      
    var idtemporada = $("#idtipotemporadas").val();

    console.log('idtemporada: '+idtemporada)

    $.ajax({
              url:"/Cupos/addcupos",
              type:"post",
              data:{
                    cupo        : dato,
                    fecha       : anho+'-'+mes+'-'+diaseleccionado,
                    th          : thseleccionado,
                    idtemporada : idtemporada 
                   },
              success:function(n){
                if(n!="0"){
                       console.log('ok');
                       getdata();
                               
                }
              }
          });


   }


   function enviarprecio(){

    var dato = $("#precio").val();
    var datoadicional = $("#preciopaxadicional").val();


    if (dato == '') {
      alert('Debe ingresar un dato');
      return false;
    } 

    var mes  = $("#hiddenmes")         .val();
    var anho = $("#hiddenanhoselect")  .val();


    if (mes < 0) {
      mes = '0'+mes;
    }

    var diaseleccionado = parseInt($("#diaseleccionado").val());

    if (diaseleccionado < 0) {
      diaseleccionado = '0'+diaseleccionado;
    }

    var thseleccionado = $("#thseleccionado").val();
    var idtemporada    = $("#idtipotemporadas").val();

    $.ajax({
              url:"/Cupos/addprecios",
              type:"post",
              data:{
                    precio             : dato,
                    preciopaxadicional : datoadicional,
                    fecha              : anho+'-'+mes+'-'+diaseleccionado,
                    th                 : thseleccionado,
                    idtemporada        : idtemporada

                   },
              success:function(n){
                if(n!="0"){
                       console.log('ok');
                       getdata();
                               
                }
              }
          });

   }





   function getdata(){

    var idtemporada    = $("#idtipotemporadas").val();
    var mes            = $("#hiddenmes")         .val();
    var anho           = $("#hiddenanhoselect")  .val();


    $.ajax({
      url:"/Cupos/getdata/"+mes+"/"+anho+"/"+idtemporada,
      type:"post",
      data:{},
      success:function(response){
        
       
        var data = JSON.parse(response);

        if (data.resp == true) {
          

          data.valores.forEach(datos => {

            $("#tdc_"+datos.Cupo.fecha+"_"+datos.Cupo.tipohabitacione_id).html(datos.Cupo.cantidad);
            $("#tdp_"+datos.Cupo.fecha+"_"+datos.Cupo.tipohabitacione_id).html("<b style='color:green;'>"+datos.Cupo.precio+"</b>&nbsp;<br>&nbsp;<b style='color:red;'>"+datos.Cupo.preciopaxadicional+"</b>");
            
          });
        }

      }
    });
   }


  function getdatadefault(){

  

    $.ajax({
      url:"/Cupos/getdatadefault/",
      type:"post",
      data:{},
      success:function(response){
        
       
        var data = JSON.parse(response);

        if (data.resp == true) {
          

          data.valores.forEach(datos => {

            console.log(datos)

            $("#defcupos").html(datos.Setting.cupos);
            $("#defpreciopax").html(datos.Setting.preciopax);
            $("#defpreciopaxad").html(datos.Setting.preciopaxadicional);
            
          });
        }

      }
    });
   }
   function enviardatadefault(){
    var cupodef          = $("#cupodef").val();
    var preciopaxdef     = $("#preciopaxdef").val();
    var preciopaxadicdef = $("#preciopaxadicdef").val();

    $.ajax({
              url:"/Cupos/updatedefault",
              type:"post",
              data:{
                    cupodef:  cupodef,
                    preciopaxdef: preciopaxdef,
                    preciopaxadicdef: preciopaxadicdef,
                    
                   },
              success:function(n){
                if(n!="0"){
                       
                       getdatadefault();
                               
                }
              }
          });



   }
   getdata();
   getdatadefault();
 </script>

 <!-- The Modal -->
 <div class="modal" id="modalPrecioCupoDefecto">
   <div class="modal-dialog">
     <div class="modal-content">

       <!-- Modal Header -->
       <div class="modal-header">
         <h4 class="modal-title">Establece Cupo / Precio por Defecto</h4>
         <button type="button" class="close" data-dismiss="modal">&times;</button>
       </div>

       <!-- Modal body -->
       <div class="modal-body">
          

          <div class="row">
            <div class="col-md-4">              
                <input id="cupodef" type="text" placeholder="Cupos por Defecto"  class="form-control">
            </div>
            <div class="col-md-4">
                <input id="preciopaxdef" type="text" placeholder="Precio por Pax Defecto"  class="form-control">
            </div>
            <div class="col-md-4">
                <input id="preciopaxadicdef" type="text" placeholder="Precio por Pax Adicional Defecto"  class="form-control">
            </div>
          </div>



       </div>

       <!-- Modal footer -->
       <div class="modal-footer">
         <button onclick="enviardatadefault()" type="button" class="btn btn-success" data-dismiss="modal">Establecer</button>
       </div>

     </div>
   </div>
 </div>





 <!-- The Modal por fechas-->
 <div class="modal" id="modalPrecioCupoFechas">
   <div class="modal-dialog">
     <div class="modal-content">

       <!-- Modal Header -->
       <div class="modal-header">
         <h4 class="modal-title">Establece Cupo / Precio por rango de dias para el mes de <?=$mesdenominacion[$mes]?></h4>
         <button type="button" class="close" data-dismiss="modal">&times;</button>
       </div>

       <!-- Modal body -->
       <div class="modal-body">
           
           
           <div class="row">
               <div class="col-md-4">
                   <label>Desde</label>
                   <select id="diainicio" class="form-control">
                        <?php 
                                
                            for($a = 1; $a <= $numero - 1; $a++){
                                echo "<option value='".$a."'>".$a."</option>";
                            }
                        
                        ?>
                   </select>
               </div>
               <div class="col-md-4">
                   <label>Hasta</label>
                   <select id="diafin" class="form-control">
                        <?php 
                                
                            for($a = 2; $a <= $numero; $a++){
                                echo "<option value='".$a."'>".$a."</option>";
                            }
                        
                        ?>
                   </select>
               </div>
               <div class="col-md-4">
                   <label>
                    Tipo Habitacion
                   </label>
                   <select id="idtipohabitacion" class="form-control">
                   <?php foreach ($tipohab as $key): ?>
                       <option value="<?=$key['Tipohabitacione']['id']?>"><?=$key['Tipohabitacione']['denominacion']?></option>
                   <?php endforeach ?>
                   </select>
                   
               </div>
           </div>
           
           <br>
           <br>
          

          <div class="row">
            <div class="col-md-4">              
                <input type="number" id="cupofec" type="text" placeholder="Cantidad de cupos"  class="form-control">
            </div>
            <div class="col-md-4">
                <input type="number" id="preciopaxfec" type="text" placeholder="Precio por Pax"  class="form-control">
            </div>
            <div class="col-md-4">
                <input type="number" id="preciopaxadicfec" type="text" placeholder="Precio por Pax Adicional"  class="form-control">
            </div>
          </div>



       </div>

       <!-- Modal footer -->
       <div class="modal-footer">
         <button onclick="enviardatafechas()" type="button" class="btn btn-success" data-dismiss="modal">Establecer</button>
       </div>

     </div>
   </div>
 </div>




 <!-- The Modal -->
 <div class="modal" id="myModalPrecio">
   <div class="modal-dialog">
     <div class="modal-content">

       <!-- Modal Header -->
       <div class="modal-header">
         <h4 class="modal-title">Establece Precio</h4>
         <button type="button" class="close" data-dismiss="modal">&times;</button>
       </div>

       <!-- Modal body -->
       <div class="modal-body">
          

          <div class="row">
            <div class="col-md-6">              
                <input id="precio" type="text" placeholder="Precio por Pax"  class="form-control">
            </div>
            <div class="col-md-6">
                <input id="preciopaxadicional" type="text" placeholder="Precio por Pax Adicional"  class="form-control">
            </div>
          </div>



       </div>

       <!-- Modal footer -->
       <div class="modal-footer">
         <button onclick="enviarprecio()" type="button" class="btn btn-success" data-dismiss="modal">Establecer</button>
       </div>

     </div>
   </div>
 </div>


 <!-- The Modal -->
<div class="modal" id="myModalCupo">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Establece Cupo</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
         <input id="cupo" type="text" class="form-control">       
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button onclick="enviarcupo()
        console.log();" type="button" class="btn btn-success" data-dismiss="modal">Establecer</button>
      </div>

    </div>
  </div>
</div>

