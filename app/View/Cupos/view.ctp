<section class="content-header">
  <h1>
    Sistema de Encuestas
    <small><?php echo __('Cupos'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Cupos'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Cupos'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($cupo['Cupo']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha'); ?></dt>
		<dd>
			<?php echo h($cupo['Cupo']['fecha']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tipohabitacione'); ?></dt>
		<dd>
			<?php echo $this->Html->link($cupo['Tipohabitacione']['denominacion'], array('controller' => 'tipohabitaciones', 'action' => 'view', $cupo['Tipohabitacione']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cantidad'); ?></dt>
		<dd>
			<?php echo h($cupo['Cupo']['cantidad']); ?>
			&nbsp;
		</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $cupo['Cupo']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Cupo'), array('action' => 'edit', $cupo['Cupo']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Cupo'), array('action' => 'delete', $cupo['Cupo']['id']), array(), __('Are you sure you want to delete # %s?', $cupo['Cupo']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Cupos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cupo'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipohabitaciones'), array('controller' => 'tipohabitaciones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipohabitacione'), array('controller' => 'tipohabitaciones', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>