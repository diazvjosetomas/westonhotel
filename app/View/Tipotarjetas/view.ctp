<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Tipo de tarjetas'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Tipo de tarjetas'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Tipo de tarjetas'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
		<dt><?php echo __('Descripción'); ?></dt>
		<dd>
			<?php echo h($tipotarjeta['Tipotarjeta']['descripcion']); ?>
			&nbsp;
		</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $tipotarjeta['Tipotarjeta']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Tipotarjeta'), array('action' => 'edit', $tipotarjeta['Tipotarjeta']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Tipotarjeta'), array('action' => 'delete', $tipotarjeta['Tipotarjeta']['id']), array(), __('Are you sure you want to delete # %s?', $tipotarjeta['Tipotarjeta']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipotarjetas'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipotarjeta'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tarjetacreditos'), array('controller' => 'tarjetacreditos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tarjetacredito'), array('controller' => 'tarjetacreditos', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Tarjetacreditos'); ?></h3>
	<?php if (!empty($tipotarjeta['Tarjetacredito'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Cliente Id'); ?></th>
		<th><?php echo __('Tipotarjeta Id'); ?></th>
		<th><?php echo __('Agencia Id'); ?></th>
		<th><?php echo __('Corporativo Id'); ?></th>
		<th><?php echo __('Num Tajeta'); ?></th>
		<th><?php echo __('Vencimiento'); ?></th>
		<th><?php echo __('Nombre Segun Tarjeta'); ?></th>
		<th><?php echo __('Cuatro Ultimos'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($tipotarjeta['Tarjetacredito'] as $tarjetacredito): ?>
		<tr>
			<td><?php echo $tarjetacredito['id']; ?></td>
			<td><?php echo $tarjetacredito['cliente_id']; ?></td>
			<td><?php echo $tarjetacredito['tipotarjeta_id']; ?></td>
			<td><?php echo $tarjetacredito['agencia_id']; ?></td>
			<td><?php echo $tarjetacredito['corporativo_id']; ?></td>
			<td><?php echo $tarjetacredito['num_tajeta']; ?></td>
			<td><?php echo $tarjetacredito['vencimiento']; ?></td>
			<td><?php echo $tarjetacredito['nombre_segun_tarjeta']; ?></td>
			<td><?php echo $tarjetacredito['cuatro_ultimos']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'tarjetacreditos', 'action' => 'view', $tarjetacredito['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'tarjetacreditos', 'action' => 'edit', $tarjetacredito['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'tarjetacreditos', 'action' => 'delete', $tarjetacredito['id']), array(), __('Are you sure you want to delete # %s?', $tarjetacredito['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Tarjetacredito'), array('controller' => 'tarjetacreditos', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<?php */ ?>