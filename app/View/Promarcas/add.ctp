<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Marcas'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Marcas'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Add Marcas'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Promarca', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
	
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Promarcaprotipo_id">Tipo producto</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('protipo_id', array('id'=>'Promarcaprotipo_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', "onChange"=>"selectTagRemote('".$this->Html->url('/Promarcas/producto')."', 'div-producto', this.value);", 'empty'=>'--Seleccione--')); 	
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Promarcaproproducto_id">Producto</label>';		
									echo'<div class="col-md-9" id="div-producto">';			
									echo $this->Form->input('proproducto_id', array('id'=>'Promarcaproproducto_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Promarcadenominacion">Denominación</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('denominacion', array('id'=>'Promarcadenominacion', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
								?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Html->link(__('List Promarcas'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('List Protipos'), array('controller' => 'protipos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Protipo'), array('controller' => 'protipos', 'action' => 'add')); ?> </li>
		</ul>
	</div>
<?php */ ?>