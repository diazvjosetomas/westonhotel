<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Marcas'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Marcas'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Marcas'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								<dt><?php echo __('Tipo producto'); ?></dt>
								<dd>
									<?php echo h($promarca['Protipo']['denominacion']); ?>
									&nbsp;
								</dd>
								<dt><?php echo __('Producto'); ?></dt>
								<dd>
									<?php echo h($promarca['Proproducto']['denominacion']); ?>
									&nbsp;
								</dd>
								<dt><?php echo __('Denominación'); ?></dt>
								<dd>
									<?php echo h($promarca['Promarca']['denominacion']); ?>
									&nbsp;
								</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $promarca['Promarca']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Promarca'), array('action' => 'edit', $promarca['Promarca']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Promarca'), array('action' => 'delete', $promarca['Promarca']['id']), array(), __('Are you sure you want to delete # %s?', $promarca['Promarca']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Promarcas'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Promarca'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Protipos'), array('controller' => 'protipos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Protipo'), array('controller' => 'protipos', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>