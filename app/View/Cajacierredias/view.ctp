<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Corte Z'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Corte Z'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Corte Z'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
							<dt><?php echo __('Caja'); ?></dt>
							<dd>
								<?php echo h($cajacierredia['Caja']['denominacion']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('User'); ?></dt>
							<dd>
								<?php echo h($cajacierredia['User']['username']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Monto'); ?></dt>
							<dd>
								<?php echo h($cajacierredia['Cajacierredia']['monto']); ?>
								&nbsp;
							</dd>
							<dt><?php echo __('Fecha'); ?></dt>
							<dd>
								<?php echo h($cajacierredia['Cajacierredia']['created']); ?>
								&nbsp;
							</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $cajacierredia['Cajacierredia']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Cajacierredia'), array('action' => 'edit', $cajacierredia['Cajacierredia']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Cajacierredia'), array('action' => 'delete', $cajacierredia['Cajacierredia']['id']), array(), __('Are you sure you want to delete # %s?', $cajacierredia['Cajacierredia']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Cajacierredias'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cajacierredia'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cajas'), array('controller' => 'cajas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Caja'), array('controller' => 'cajas', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>