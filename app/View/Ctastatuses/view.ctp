<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Status Cuenta Corriente'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Status Cuenta Corriente'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Status Cuenta Corriente'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dt><?php echo __('Denominación'); ?></dt>
		<dd>
			<?php echo h($ctastatus['Ctastatus']['denominacion']); ?>
			&nbsp;
		</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $ctastatus['Ctastatus']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ctastatus'), array('action' => 'edit', $ctastatus['Ctastatus']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ctastatus'), array('action' => 'delete', $ctastatus['Ctastatus']['id']), array(), __('Are you sure you want to delete # %s?', $ctastatus['Ctastatus']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Ctastatuses'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ctastatus'), array('action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>