<section class="content-header">
  <h1>
    Sistema de Encuestas
    <small><?php echo __('Descuentos'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Descuentos'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Descuentos'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($descuento['Descuento']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Monto'); ?></dt>
		<dd>
			<?php echo h($descuento['Descuento']['monto']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Estatus'); ?></dt>
		<dd>
			<?php echo h($descuento['Descuento']['estatus']); ?>
			&nbsp;
		</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $descuento['Descuento']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Descuento'), array('action' => 'edit', $descuento['Descuento']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Descuento'), array('action' => 'delete', $descuento['Descuento']['id']), array(), __('Are you sure you want to delete # %s?', $descuento['Descuento']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Descuentos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Descuento'), array('action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>