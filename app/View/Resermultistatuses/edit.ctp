<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Resermultistatuses'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Resermultistatuses'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Edit Resermultistatus'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Resermultistatus', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
			
echo $this->Form->input('id', array('class'=>'form-horizontal'));	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Resermultistatusresermultiple_id">resermultiple_id</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('resermultiple_id', array('id'=>'Resermultistatusresermultiple_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Resermultistatusreserstatusmultiple_id">reserstatusmultiple_id</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('reserstatusmultiple_id', array('id'=>'Resermultistatusreserstatusmultiple_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Resermultistatusconftipopagoreserva_id">conftipopagoreserva_id</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('conftipopagoreserva_id', array('id'=>'Resermultistatusconftipopagoreserva_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Resermultistatusfecha">fecha</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('fecha', array('id'=>'Resermultistatusfecha', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Resermultistatusmonto_penalidad">monto_penalidad</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('monto_penalidad', array('id'=>'Resermultistatusmonto_penalidad', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Resermultistatusobservaciones">observaciones</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('observaciones', array('id'=>'Resermultistatusobservaciones', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Resermultistatustotal">total</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('total', array('id'=>'Resermultistatustotal', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Resermultistatuspago">pago</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('pago', array('id'=>'Resermultistatuspago', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	
echo'<div class="form-group">';	
echo'<label class="control-label col-md-2" for="Resermultistatusdebe">debe</label>';		
echo'<div class="col-md-9">';			
echo $this->Form->input('debe', array('id'=>'Resermultistatusdebe', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
echo '</div>';	
echo '</div>';
	?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Resermultistatus.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Resermultistatus.id'))); ?></li>
				<li><?php echo $this->Html->link(__('List Resermultistatuses'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('List Resermultiples'), array('controller' => 'resermultiples', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resermultiple'), array('controller' => 'resermultiples', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Reserstatusmultiples'), array('controller' => 'reserstatusmultiples', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reserstatusmultiple'), array('controller' => 'reserstatusmultiples', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Conftipopagoreservas'), array('controller' => 'conftipopagoreservas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Conftipopagoreserva'), array('controller' => 'conftipopagoreservas', 'action' => 'add')); ?> </li>
		</ul>
	</div>
<?php */ ?>