<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Resermultistatuses'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Resermultistatuses'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Resermultistatuses'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($resermultistatus['Resermultistatus']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Resermultiple'); ?></dt>
		<dd>
			<?php echo $this->Html->link($resermultistatus['Resermultiple']['id'], array('controller' => 'resermultiples', 'action' => 'view', $resermultistatus['Resermultiple']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Reserstatusmultiple'); ?></dt>
		<dd>
			<?php echo $this->Html->link($resermultistatus['Reserstatusmultiple']['denominacion'], array('controller' => 'reserstatusmultiples', 'action' => 'view', $resermultistatus['Reserstatusmultiple']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Conftipopagoreserva'); ?></dt>
		<dd>
			<?php echo $this->Html->link($resermultistatus['Conftipopagoreserva']['denominacion'], array('controller' => 'conftipopagoreservas', 'action' => 'view', $resermultistatus['Conftipopagoreserva']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha'); ?></dt>
		<dd>
			<?php echo h($resermultistatus['Resermultistatus']['fecha']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Monto Penalidad'); ?></dt>
		<dd>
			<?php echo h($resermultistatus['Resermultistatus']['monto_penalidad']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Observaciones'); ?></dt>
		<dd>
			<?php echo h($resermultistatus['Resermultistatus']['observaciones']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total'); ?></dt>
		<dd>
			<?php echo h($resermultistatus['Resermultistatus']['total']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pago'); ?></dt>
		<dd>
			<?php echo h($resermultistatus['Resermultistatus']['pago']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Debe'); ?></dt>
		<dd>
			<?php echo h($resermultistatus['Resermultistatus']['debe']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($resermultistatus['Resermultistatus']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($resermultistatus['Resermultistatus']['modified']); ?>
			&nbsp;
		</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $resermultistatus['Resermultistatus']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Resermultistatus'), array('action' => 'edit', $resermultistatus['Resermultistatus']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Resermultistatus'), array('action' => 'delete', $resermultistatus['Resermultistatus']['id']), array(), __('Are you sure you want to delete # %s?', $resermultistatus['Resermultistatus']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Resermultistatuses'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resermultistatus'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resermultiples'), array('controller' => 'resermultiples', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resermultiple'), array('controller' => 'resermultiples', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Reserstatusmultiples'), array('controller' => 'reserstatusmultiples', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reserstatusmultiple'), array('controller' => 'reserstatusmultiples', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Conftipopagoreservas'), array('controller' => 'conftipopagoreservas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Conftipopagoreserva'), array('controller' => 'conftipopagoreservas', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>