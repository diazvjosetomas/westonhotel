<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Carmbiar status de reserva'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Carmbiar status de reserva'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Add Carmbiar status de reserva'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Resermultistatus', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
<?php 
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Resermultistatusresermultiple_id">Reserva Multiple</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('resermultiple_id2', array('id'=>'Resermultistatusresermultiple_id2', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>true, 'value'=>'DNI:'.$resermultiples2[0]['Cliente']['dni'].' '.$resermultiples2[0]['Cliente']['nombre_completo']));		
									echo $this->Form->input('resermultiple_id',  array('id'=>'Resermultistatusresermultiple_id',  'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'hidden', 'readonly'=>true, 'value'=>$resermultiples2[0]['Resermultiple']['id']));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Resermultistatusreserstatusmultiple_id">Status</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('reserstatusmultiple_id', array('id'=>'Resermultistatusreserstatusmultiple_id', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'empty'=>'--Seleccione--',"onChange"=>'javascript:status_multiple_habitacion();'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group" id="Resermultistatusconftipopagoreserva_id_v" style="display:none;">';	
									echo'<label class="control-label col-md-2" for="Resermultistatusconftipopagoreserva_id">Tipo de Pago</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('conftipopagoreserva_id', array('id'=>'Resermultistatusconftipopagoreserva_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group" id="Resermultistatusfecha_v" style="display:none;">';	
									echo'<label class="control-label col-md-2" for="Resermultistatusfecha">Fecha</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('fecha', array('id'=>'Resermultistatusfecha', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group" id="Resermultistatusmonto_penalidad_v" style="display:none;">';	
									echo'<label class="control-label col-md-2" for="Resermultistatusmonto_penalidad">Monto penalidad</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('monto_penalidad', array('value'=>0, 'id'=>'Resermultistatusmonto_penalidad', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';


									echo'<div class="form-group" id="Resermultistatustotal_v" style="display:none;">';	
									echo'<label class="control-label col-md-2" for="Resermultistatustotal">Total</label>';		
									echo'<div class="col-md-9" id="div-total">';			
									echo $this->Form->input('total', array('value'=>$resermultiples2[0]['Resermultiple']['total'], 'id'=>'Resermultistatustotal', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>true));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group" id="Resermultistatuspago_v" style="display:none;">';	
									echo'<label class="control-label col-md-2" for="Resermultistatuspago">Pago</label>';		
									echo'<div class="col-md-9" id="div-pago">';			
									echo $this->Form->input('pago', array('value'=>0, 'id'=>'Resermultistatuspago', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text',"onChange"=>'javascript:pago_multiples_habitacion();'));			
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group" id="Resermultistatusdebe_v" style="display:none;">';	
									echo'<label class="control-label col-md-2" for="Resermultistatusdebe">Debe</label>';		
									echo'<div class="col-md-9" id="div-debe">';			
									echo $this->Form->input('debe', array('value'=>0, 'id'=>'Resermultistatusdebe', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'type'=>'text', 'readonly'=>true));		
									echo '</div>';	
									echo '</div>';

									echo'<div class="form-group" id="Resermultistatusobservaciones_v" style="display:none;">';	
									echo'<label class="control-label col-md-2" for="Resermultistatusobservaciones">Observaciones</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('observaciones', array('id'=>'Resermultistatusobservaciones', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
								?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index/'.$id)); ?>
	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Html->link(__('List Resermultistatuses'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('List Resermultiples'), array('controller' => 'resermultiples', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resermultiple'), array('controller' => 'resermultiples', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Reserstatusmultiples'), array('controller' => 'reserstatusmultiples', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reserstatusmultiple'), array('controller' => 'reserstatusmultiples', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Conftipopagoreservas'), array('controller' => 'conftipopagoreservas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Conftipopagoreserva'), array('controller' => 'conftipopagoreservas', 'action' => 'add')); ?> </li>
		</ul>
	</div>
<?php */ ?>