<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Proveedores'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Proveedores'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Add Proveedores'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Proveedore', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Proveedorecuit">Cuit</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('cuit', array('id'=>'Proveedorecuit', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Proveedorerazon_social">Razon social</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('razon_social', array('id'=>'Proveedorerazon_social', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Proveedoredireccion">Dirección</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('direccion', array('id'=>'Proveedoredireccion', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Proveedoretelefono">Teléfono</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('telefono', array('id'=>'Proveedoretelefono', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Proveedoreemail">Email</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('email', array('id'=>'Proveedoreemail', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Proveedoreimpuesto">Impuesto</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('impuesto', array('id'=>'Proveedoreimpuesto', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
							?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Html->link(__('List Proveedores'), array('action' => 'index')); ?></li>
			</ul>
	</div>
<?php */ ?>