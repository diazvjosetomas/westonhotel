<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Tarjeta de credito'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Tarjeta de credito'); ?></li>
  </ol>
</section>
<section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('Tarjeta de credito'); ?> Detalles</h3>
                        <hr>
                    </div><!-- /.box-header -->
                    <div class="box-body">
						<dl class="dl-horizontal">
								
		<dt><?php echo __('Cliente'); ?></dt>
		<dd>
			<?php echo $this->Html->link($tarjetacredito['Cliente']['dni'], array('controller' => 'clientes', 'action' => 'view', $tarjetacredito['Cliente']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tipo de tarjeta'); ?></dt>
		<dd>
			<?php echo $this->Html->link($tarjetacredito['Tipotarjeta']['descripcion'], array('controller' => 'tipotarjetas', 'action' => 'view', $tarjetacredito['Tipotarjeta']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Num Tajeta'); ?></dt>
		<dd>
			<?php echo h($tarjetacredito['Tarjetacredito']['num_tajeta']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vencimiento'); ?></dt>
		<dd>
			<?php echo h(formatmy($tarjetacredito['Tarjetacredito']['vencimiento'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nombre'); ?></dt>
		<dd>
			<?php echo h($tarjetacredito['Tarjetacredito']['nombre_segun_tarjeta']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cod seguridad'); ?></dt>
		<dd>
			<?php echo h($tarjetacredito['Tarjetacredito']['cuatro_ultimos']); ?>
			&nbsp;
		</dd>
						</dl>
						<p>
						    		<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $tarjetacredito['Tarjetacredito']['id'])); ?>
                            |
                            		<?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>
                        </p>
					</div>
			    </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?><div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Tarjetacredito'), array('action' => 'edit', $tarjetacredito['Tarjetacredito']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Tarjetacredito'), array('action' => 'delete', $tarjetacredito['Tarjetacredito']['id']), array(), __('Are you sure you want to delete # %s?', $tarjetacredito['Tarjetacredito']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Tarjetacreditos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tarjetacredito'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Clientes'), array('controller' => 'clientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cliente'), array('controller' => 'clientes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipotarjetas'), array('controller' => 'tipotarjetas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipotarjeta'), array('controller' => 'tipotarjetas', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php */ ?>