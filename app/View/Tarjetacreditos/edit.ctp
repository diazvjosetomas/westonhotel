<section class="content-header">
  <h1>
    Sistema de gestión
    <small><?php echo __('Tarjeta de credito'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?= $this->Html->url('/Dashboard/')?>"><i class="fa fa-newspaper-o"></i> Inicio</a></li>
    <li class="active"><?php echo __('Tarjeta de credito'); ?></li>
  </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('Edit Tarjeta de credito'); ?></h3>
                    <hr>
                </div><!-- /.box-header -->
                <div class="box-body">
					<?php echo $this->Form->create('Tarjetacredito', array('class'=>'form-horizontal')); ?>
					<div class='row'>
							<div class='col-md-12'>
								<?php
			
									echo $this->Form->input('id', array('class'=>'form-horizontal'));	
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Tarjetacreditocliente_id">Cliente</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('cliente_id', array('id'=>'Tarjetacreditocliente_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Tarjetacreditotipotarjeta_id">Tipo de tarjeta</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('tipotarjeta_id', array('id'=>'Tarjetacreditotipotarjeta_id', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Tarjetacreditonum_tajeta">Num tajeta</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('num_tajeta', array('id'=>'Tarjetacreditonum_tajeta', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Tarjetacreditovencimiento">Vencimiento</label>';		
									echo'<div class="col-md-4">';			
									echo $this->Form->input('vencimiento1', array('id'=>'Tarjetacreditovencimiento', 'div'=>false, 'label'=>false, 'class'=>'form-control', 'value' => $this->request->data['Tarjetacredito']['vencimiento'], 'readonly' => true ));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Tarjetacreditonombre_segun_tarjeta">Nombre</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('nombre_segun_tarjeta', array('id'=>'Tarjetacreditonombre_segun_tarjeta', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
										
									echo'<div class="form-group">';	
									echo'<label class="control-label col-md-2" for="Tarjetacreditocuatro_ultimos">Cod seguridad</label>';		
									echo'<div class="col-md-9">';			
									echo $this->Form->input('cuatro_ultimos', array('id'=>'Tarjetacreditocuatro_ultimos', 'div'=>false, 'label'=>false, 'class'=>'form-control'));		
									echo '</div>';	
									echo '</div>';
	?>
							</div>
							<div class="col-md-12">
								<div class="form-group">
	                                <div class="col-md-12">
	                                    <?php echo $this->Html->link(__('Volver al listado'), array('action' => 'index')); ?>	                                    <input value="Guardar" class="btn btn-primary pull-right" type="submit">
	                                </div>
	                            </div>
                            </div>
					</div></form>                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<?php /* ?>	<div class="actions">
		<h3><?php echo __('Actions'); ?></h3>
		<ul>
				<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Tarjetacredito.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Tarjetacredito.id'))); ?></li>
				<li><?php echo $this->Html->link(__('List Tarjetacreditos'), array('action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('List Clientes'), array('controller' => 'clientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cliente'), array('controller' => 'clientes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tipotarjetas'), array('controller' => 'tipotarjetas', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tipotarjeta'), array('controller' => 'tipotarjetas', 'action' => 'add')); ?> </li>
		</ul>
	</div>
<?php */ ?>


<script type="text/javascript">

      jQuery(function($){
          $.datepicker.regional['es'] = {
              closeText: 'Cerrar',
              prevText: '&#x3c;Ant',
              nextText: 'Sig&#x3e;',
              currentText: 'Hoy',
              monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
              'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
              monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
              'Jul','Ago','Sep','Oct','Nov','Dic'],
              dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
              dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
              dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
              weekHeader: 'Sm',
              dateFormat: 'mm/yy',
              changeMont: true,
              changeYear: true,
              firstDay: 1,
              isRTL: false,
              showMonthAfterYear: true,
              yearRange: '-100:+30',
              yearSuffix: ''};
          $.datepicker.setDefaults($.datepicker.regional['es']);
      });

      $('#Tarjetacreditovencimiento').datepicker();

</script>