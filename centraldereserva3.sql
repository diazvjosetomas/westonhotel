-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 16-02-2018 a las 14:51:53
-- Versión del servidor: 10.1.26-MariaDB-0+deb9u1
-- Versión de PHP: 7.0.19-1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `centraldereserva3`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bodymail`
--

CREATE TABLE `bodymail` (
  `id` int(11) NOT NULL,
  `denominacion` varchar(256) NOT NULL,
  `cuerpoemail` text NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `bodymail`
--

INSERT INTO `bodymail` (`id`, `denominacion`, `cuerpoemail`, `created`, `modified`) VALUES
(1, 'Reservacion Iniciada', '<p>Estimado _nombres_, su reserva fue procesada para el dia _fechainicio_ hasta el _fechasalida_</p>\r\n', '2017-06-21', '2017-06-21'),
(2, 'Reservacion Ingresada', '<p>Reservacion Ingresada</p>\r\n', '2017-06-21', '2017-06-21'),
(3, 'Reservacion Egresada', '<p>Editar el cuerpo de este mail</p>\r\n', '2017-06-21', '2017-06-21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cajaacierreturnos`
--

CREATE TABLE `cajaacierreturnos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `caja_id` int(11) NOT NULL,
  `cajaturno_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `monto` float(26,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cajaacierreturnos`
--

INSERT INTO `cajaacierreturnos` (`id`, `caja_id`, `cajaturno_id`, `user_id`, `monto`, `created`, `modified`) VALUES
(1, 1, 1, 1, 100.00, '2016-12-10 11:09:47', '2016-12-10 11:09:47'),
(2, 1, 1, 1, 100.00, '2017-03-06 14:22:20', '2017-03-06 14:22:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cajaaperturas`
--

CREATE TABLE `cajaaperturas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `caja_id` int(11) NOT NULL,
  `cajaturno_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `monto` float(26,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cajaaperturas`
--

INSERT INTO `cajaaperturas` (`id`, `caja_id`, `cajaturno_id`, `user_id`, `monto`, `created`, `modified`) VALUES
(1, 1, 1, 1, 100.00, '2017-03-06 13:11:39', '2017-03-06 13:11:39'),
(2, 1, 2, 1, 5000.00, '2017-04-25 14:26:18', '2017-04-25 14:26:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cajacierredias`
--

CREATE TABLE `cajacierredias` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `caja_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `monto` float(26,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cajas`
--

CREATE TABLE `cajas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `denominacion` varchar(200) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cajas`
--

INSERT INTO `cajas` (`id`, `denominacion`, `estatus`, `created`, `modified`) VALUES
(1, 'Caja 1', 1, '2016-12-10 08:53:30', '2017-10-04 19:47:10'),
(2, 'Caja 2', 1, '2016-12-10 08:53:40', '2016-12-10 08:53:40'),
(3, 'prueba ', 1, '2017-03-06 13:11:07', '2017-03-06 13:11:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cajaturnos`
--

CREATE TABLE `cajaturnos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `denominacion` varchar(200) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cajaturnos`
--

INSERT INTO `cajaturnos` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'MaÃ±ana', '2016-12-10 10:54:48', '2016-12-10 10:54:57'),
(2, 'Tarde', '2016-12-10 10:55:06', '2016-12-10 10:55:06'),
(3, 'Noche', '2016-12-10 10:55:14', '2016-12-10 10:55:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `tipocliente_id` int(11) NOT NULL,
  `tipoclientesub_id` int(11) NOT NULL,
  `pai_id` int(11) NOT NULL DEFAULT '0',
  `dni` varchar(45) NOT NULL,
  `nombre_completo` varchar(45) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `ciudad` varchar(300) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `codigo_postal` varchar(100) NOT NULL,
  `telf` varchar(45) NOT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `cod_pos` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `tipocliente_id`, `tipoclientesub_id`, `pai_id`, `dni`, `nombre_completo`, `direccion`, `ciudad`, `fecha_nacimiento`, `codigo_postal`, `telf`, `correo`, `cod_pos`) VALUES
(4, 1, 3, 0, '33231678', 'FERNANDO COSTA ', 'LAS HERAS 419 ', '', '0000-00-00', '', '4313712', 'ettarholmes118@hotmail.com', '5519'),
(5, 2, 4, 0, '30701307115', 'DRATLER LEONARDO ', 'Calle EspaÃ±a 948 ciudad Mendoza', '', '0000-00-00', '', '4253533', '', ''),
(6, 2, 5, 0, '1356498465', 'SANCHEZ LAURA ', 'ARGENTINA ', '', '0000-00-00', '', '4253533', '', ''),
(7, 2, 5, 0, '98746511', 'MARTINEZ NATALIA', 'BS AS ', '', '0000-00-00', '', '85495846', '', ''),
(8, 2, 5, 0, '942165165', 'REIDY JUAN ', 'ARGENTINA', '', '0000-00-00', '', '4253533', '', ''),
(10, 2, 6, 0, '9846152103', 'EBRECHT VERONICA ', 'ARGENTINA ', '', '0000-00-00', '', '0111111111', '', ''),
(11, 2, 5, 1, '6516846546', 'HORISBERGER SEBASTIAN ', 'BUENOS AIRES ', 'BUENOS AIRES ', '2017-08-11', '', '4250333', 'reservas@premiumtowersuites.com', ''),
(12, 2, 7, 1, '654564654', 'GIOCONDO ROXANA ', 'BUENOS AIRES ', 'BUENOS AIRES ', '2017-08-11', '', '01111154894848', '', ''),
(13, 2, 6, 1, '4641866', 'PAOLO SILVINA ', 'BUENOS AIRES ', 'BUENOS AIRES ', '2017-02-02', '', '654654641', '', ''),
(14, 2, 4, 1, '987143641', 'ALVAREZ MATEO ', 'BUENOS AIRES ', 'BUENOS AIRES ', '2017-01-11', '', '0111111111', 'RESERVAS@PREMIUMTOWERSUITES.COM', ''),
(15, 2, 6, 1, '98469146146', 'TOIW MARIO ', 'BUENOS AIRES ', 'BUENOS AIRES ', '2017-02-14', '', '01111111111', 'reservas@premiumtowersuites.com', ''),
(16, 2, 5, 1, '658469146941', 'GALLO MIGUEL', 'BUENOS AIRES', 'BUENOS AIRES ', '2017-01-09', '', '01111111111', 'RESERVAS@PREMIUMTOWERSUITES.COM', ''),
(17, 2, 5, 1, '587412451247', 'VILLAR JUAN CARLOS ', 'BUENOS AIRES ', 'BUENOS AIRES ', '2017-01-27', '', '011111111111111', 'RESERVAS@PREMIUMTOWERSUITES.COM', ''),
(18, 2, 8, 1, '69456514512', 'VILLAR JUAN CARLOS ', 'BUENOS AIRES ', 'BUENOS AIRES ', '2017-01-19', '', '01111111', '', ''),
(19, 2, 5, 1, '9849819496', 'MONTELEONE JULIO ', 'BUENOS AIRES ', 'BUENOS AIRES ', '2017-01-11', '', '0111111111', 'reservas@premiumtowersuites.com', ''),
(20, 2, 6, 1, '1111111', 'MESA ALAN ', 'nnnnnnn', 'NNNNN', '2017-01-27', '', 'nnnnnn', 'reservas@premiumtowersuites.com', 'nnnnnn'),
(21, 1, 3, 1, '465146541351465', 'PROVIDENZA LILIANA ', 'NNNNN', 'NNNNN', '2017-01-27', '', 'NNNN', 'reservas@premiumtowersuites.com', 'nnnnnnn'),
(22, 2, 6, 1, '89441894', 'JONATHAN WILLIAM ', 'NNNN', 'NNNN', '2017-02-01', '', 'NNNN', 'NNNN', 'NNNN'),
(23, 1, 3, 1, '7527867857', 'PIBERERNUS LEANDRO', 'NNNN', 'NNNN', '2017-01-27', '', 'NNNN', 'NNNN', 'NNNNN'),
(24, 3, 9, 1, '155511155511', 'DA SILVA FABIAN ', 'NNNNNN', 'NNNNNN', '2017-01-31', '', 'NNNNNN', 'NNNNNN', 'NNNNNN'),
(25, 3, 9, 1, '8149616946', 'HARFUCH JORDAN ', 'NNNN', 'NNNN', '2017-01-31', '', 'NNNN', 'NNNN', 'NNNN'),
(26, 1, 3, 1, '95464161', 'PROVIDENZA LILIANA ', 'NNNNNN', 'NNNNN', '2017-01-31', '', 'NNNNN', 'NNNNN', 'NNNNN'),
(27, 2, 6, 1, '857425728752', 'MARTINEZ JIMENA ', 'NNNNN', 'NNNNN', '2017-01-31', '', 'NNNNN', 'NNNNN', 'NNNNNN'),
(28, 2, 5, 1, '587274274', 'QUEVEDO MARISA ', 'NNNNN', 'NNNN', '2017-01-31', '', 'NNN', 'NN', 'NN'),
(29, 3, 10, 1, '782742578', 'CANO SEBASTIAN ', 'NNN', 'NNNN', '2017-01-02', '', 'NN', 'NN', 'N'),
(30, 2, 5, 1, '14679784', 'DA SILVA FABIAN ', 'SOLDADO DE LOS RODS 6123', 'BS AS ', '2017-01-31', '', '011 1566071639', 'FABIANDASILVA10@GMAIL.COM', 'NNNN'),
(31, 2, 11, 1, '9596529', 'SANTOS ANA ', 'NNNNN', 'NNNNN', '2017-01-31', '', 'NNNNN', 'NNNNN', 'NNNN '),
(32, 2, 5, 1, '689723757', 'RICAGNO LAURA ', 'NNN', 'NNN', '2017-01-31', '', 'NNN', 'NNN', 'NN'),
(33, 2, 4, 1, '95959959', 'DUZO IRINA ', 'NNNN', 'NNNN', '2017-01-31', '', 'NNNN', 'NNNNN', 'NNN'),
(34, 2, 5, 1, '894596', 'OIZEROVICH ALEJANDRO ', 'NNN', 'NNNN', '2017-01-31', '', 'NNN', 'NNN', 'NN'),
(35, 2, 5, 1, '782742782', 'RAMIREZ CEFERINA ', 'NNN', 'NNNN', '2017-01-31', '', 'NN', 'NN', 'NN'),
(36, 2, 5, 1, '55532525285258', 'VOUILLAT CINTIA ', 'NNN', 'NNN', '2017-01-31', '', 'NN', 'NN', 'NN'),
(37, 2, 5, 1, '5374784', 'GARCIA VIVIAN ', 'NNNN', 'NNN', '2017-01-31', '', 'NNN', 'NNN', 'NNNN'),
(38, 2, 5, 1, '7852745274', 'FERNANDEZ SILVIA ', 'NNNN', 'NNNN', '2017-01-31', '', 'NNNN', 'NN', 'NNN'),
(39, 2, 5, 1, '8587585', 'PAILOS PEDRO ', 'NNNN', 'NNNN', '2017-01-31', '', 'NNN', 'NNN', 'NNNN'),
(40, 2, 5, 1, '87527578', 'REIZENTHALER ROMINA ', 'NNN', 'NNNN', '2017-01-31', '', 'NNN', 'NNN', 'NNN'),
(41, 2, 5, 1, '6887287', 'LOPEZ JAVIER ', 'NNN', 'NNNN', '2017-02-01', '', 'NNN', 'NN', 'NN'),
(42, 2, 5, 1, '45242', 'BERNARDINI NORA ', 'NNN', 'NNNN', '2017-02-01', '', 'NNN', 'NNN', 'NNN'),
(43, 2, 6, 1, '89724524525', 'SAINT GERMAIN JOSE', 'NNN', 'NNNN', '2017-02-01', '', 'NNN', 'NNN', 'NN'),
(44, 2, 5, 1, '9885872575987', 'GIACCAGLIA NORMA ', 'NNN', 'NNNN', '2017-02-01', '', 'NNN', 'NNN', 'NN'),
(45, 2, 5, 1, '96458518515', 'MICHE DAMIAN EDUARDO ', 'NNN', 'NNNN', '2017-02-01', '', 'NNN', 'NN', 'NN'),
(46, 2, 15, 1, '85852852', 'SELLES HUGO ', 'NN', 'NNN', '2017-03-05', '', 'NN', 'NNN', 'NN'),
(47, 3, 16, 1, '525272421', 'BLOQUEO AMERICAN JET ', 'NNN', 'NNN', '2017-02-01', '', 'NNN', 'NN', 'NN'),
(48, 2, 4, 1, '98459845', 'AHMED ADEL MOHAMED ABDELRAHMAN ', 'NN', 'NN', '2017-02-02', '', 'NN', 'NN', 'NN'),
(49, 2, 5, 1, '478965412', 'ROJAS MARIANA SOLEDAD', 'N', 'N', '2017-02-03', '', 'N', 'N', 'N'),
(50, 2, 17, 1, '25987412', 'GALLI DANIEL', 'N', 'N', '2017-02-03', '', 'N', 'N', 'N'),
(51, 2, 5, 1, '87965213', 'KLECER CLARA', 'N', 'N', '2017-02-03', '', 'N', 'N', 'N'),
(52, 2, 5, 1, '782361927', 'BORGES LUCIANA', 'N', 'N', '2017-02-03', '', 'N', 'N', 'N'),
(53, 2, 5, 1, '5496459', 'FAGGIOLI FEDERICO', 'NNN', 'NNNN', '2017-02-03', '', 'NN', 'NN', 'N'),
(54, 2, 6, 1, '74859632', 'STIMOLI MARCELA NOEMÃ', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(55, 2, 5, 1, '4989', 'BONAIUTO GONZALO ', 'NN', 'NNN', '2017-03-06', '', 'NNN', 'N', 'N'),
(56, 2, 18, 0, '22222222222', 'GRUPO GUIBOR 2Âº INGRESO ', 'NNN', '', '0000-00-00', '', 'NN', 'NN', 'NN'),
(57, 2, 5, 1, '74856952', 'BOLDU EZEQUIEL GASTON', 'N', 'N', '2017-02-03', '', 'N', 'N', 'N'),
(58, 2, 5, 1, '74874147', 'VALLEJOS MARIA MARCELINA', 'N', 'N', '2017-02-03', '', 'N', 'N', 'N'),
(59, 2, 6, 0, '78965412', 'KJELL RUGIDO WESTBYE', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(60, 2, 6, 0, '74125639', 'LEIF ENRIQUE HOLMBERG', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(61, 2, 6, 0, '89365149', 'Kari Tapio Karjalainen', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(62, 2, 5, 1, '8575', 'MERLO CLAUDIO ', 'NNN', 'NNNN', '2017-02-06', '', 'NN', 'NN', 'NN'),
(63, 2, 5, 1, '4648464', 'ACUÃ‘A CINTIA ', 'NNN', 'NNN', '2017-02-06', '', 'NN', 'NN', 'NN'),
(64, 2, 5, 1, '964868464', 'ACUÃ‘A CINTIA ', 'NNN', 'NNN', '2017-02-06', '', 'NN', 'NN', 'NN'),
(65, 2, 5, 1, '89456', 'PRADO NOEMI ', 'NN', 'NNN', '2017-02-06', '', 'NN', 'NN', 'NN'),
(66, 2, 5, 1, '98416', 'PERUCCI GERARDO ', 'NNN', 'NNNN', '2017-02-06', '', 'NNN', 'NNN', 'NNN'),
(67, 2, 6, 1, '98494846', 'ZEBALLO LUCAS ', 'NNN', 'NNN', '2017-02-06', '', 'NNN', 'NNN', 'NN'),
(68, 2, 5, 1, '9846984616', 'SOUZA ROBERTO ', 'NNN', 'NNN', '2017-02-06', '', 'NN', 'NN', 'NN'),
(69, 2, 19, 1, '984564', 'BURKE THOMAS ', 'NN', 'NNN', '2017-02-06', '', 'NN', 'NN', 'NN'),
(70, 2, 20, 0, '8949496496', 'BLOQUEO FACILTUR 2Âº INGRESO ', 'NNN', '', '0000-00-00', '', 'NNN', 'NNN', 'NNN'),
(71, 1, 3, 1, '846541684', 'LYON ALEJANDRA ', 'NNN', 'NNN', '2017-02-06', '', 'NNN', 'NNN', 'NNN'),
(72, 2, 5, 1, '6354465', 'KESSLER CAROLINA ', 'NNN', 'NNN', '2017-02-06', '', 'NNN', 'NNN', 'NN'),
(73, 3, 10, 1, '36156415', 'VILLANUEVA CESAR ', 'NNN', 'NNNN', '2017-02-06', '', 'NNN', 'NNN', 'NN'),
(74, 2, 5, 1, '3546546', 'LOISE PATRICIA ', 'NN', 'NN', '2017-03-10', '', 'NN', 'NN', 'NN'),
(75, 2, 21, 1, '16544894', 'STEHLE LUIS ', 'NN', 'NNN', '2017-02-07', '', 'NN', 'NN', 'NN'),
(76, 2, 5, 1, '321549854', 'RUFINENGO MARTA ', 'NNN', 'NNN', '2017-02-07', '', 'NN', 'NN', 'NN'),
(77, 2, 5, 1, '19516', 'ALMADA GRISELDA ', 'NNN', 'NN', '2017-02-07', '', 'NNN', 'NNN', 'NN'),
(78, 2, 5, 1, '6165841685', 'GRANDEZ AREVALO MIGUEL ', 'NNN', 'NNN', '2017-02-07', '', 'NNN', 'NNN', 'NN'),
(79, 2, 5, 1, '94651465', 'CLAPSOS ROSANA ', 'NNN', 'NN', '2017-02-07', '', 'NNN', 'NNN', 'NN'),
(80, 2, 5, 1, '2684614', 'FAVRET CHRISTIAN ', 'NNN', 'NNN', '2017-02-07', '', 'NNN', 'NNN', 'NN'),
(81, 2, 6, 1, '646489', 'VELTRI ADRIANA ', 'NNN', 'NNN', '2017-02-07', '', 'NNN', 'NNN', 'NNN'),
(82, 2, 5, 1, '782742', 'FRUSELA LILIANA ', 'NN', 'NN', '2017-02-07', '', 'NN', 'NN', 'NN'),
(83, 2, 22, 1, '8974596', 'INOSSA RENATO ', 'NNN', 'NN', '2017-02-07', '', 'NN', 'NNN', 'NN'),
(84, 2, 22, 1, '496481', 'INOSSA RENATO ', 'NNN', 'NNN', '2017-03-12', '', 'NNN', 'NNN', 'NNN'),
(85, 2, 5, 1, '78452146', 'CUGNIET VICTOR MANUEL', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(86, 2, 6, 1, '49648614', 'INOSSA RENATO ', 'NNN', 'NNN', '2017-02-07', '', 'NNN', 'NN', 'NNN'),
(87, 2, 5, 1, '9841869', 'YAPURA EDUARDO ', 'NNN', 'NN', '2017-02-07', '', 'NN', 'NNN', 'NN'),
(88, 2, 5, 1, '49841648', 'PETER LAURA ', 'nnn', 'nn', '2017-02-07', '', 'nn', 'nnn', 'nn'),
(89, 2, 5, 1, '859486', 'MORELLI MARIA ', 'NNN', 'NNN', '2017-02-07', '', 'NNN', 'NNN', 'NN'),
(90, 2, 5, 1, '4984165489', 'PEREZ DANIEL ', 'NNN', 'NNN', '2017-02-07', '', 'NNN', 'NNN', 'NN'),
(91, 2, 5, 1, '549479', 'NARDUZZI NOELIA BELEN', 'NNN', 'NNN', '2017-02-07', '', 'NNN', 'NNN', 'NN'),
(92, 2, 6, 1, '1648641', 'AUROUSSEAU BRUNO ', 'NNN', 'NNN', '2017-02-07', '', 'NNN', 'NNN', 'NN'),
(93, 2, 5, 1, '48941646', 'GRUDEN MONICA ', 'NNN', 'NNN', '2017-02-07', '', 'NNN', 'NNN', 'NN'),
(94, 2, 6, 1, '87963214', 'VANNI CARLOS', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(95, 2, 4, 1, '78965872', 'CHIUFRE CORTI MARIANA', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(96, 2, 5, 1, '85639741', 'VIDIGAL FLAVIA', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(97, 2, 5, 1, '78523654', 'VIDIGAL RAFAEL ', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(98, 3, 23, 1, 'nnnnnn', 'MIN. SEG. DE LA NACION ', 'NNN', 'NNN', '2017-02-08', '', 'NNN', 'NNN', 'NN'),
(99, 2, 24, 1, '1684', 'ROLLE ALEJANDRA ', 'NNN', 'NN', '2017-02-08', '', 'NNN', 'NNN', 'NNN'),
(100, 2, 4, 1, '6541684798', 'LEA CARLOS ', 'NNN', 'NNN', '2017-02-08', '', 'NNN', 'NNN', 'NNN'),
(101, 2, 4, 1, '156486', 'SOTO CRISTIAN', 'NNN', 'NN', '2017-02-08', '', 'NNN', 'NNN', 'NN'),
(102, 2, 4, 1, '468486165', 'JAMES CHRISTIAN ', 'NNN', 'NNN', '2017-02-08', '', 'NN', 'NNN', 'NNN'),
(103, 2, 25, 0, '984646941', 'BLOQUEO PLAY PATAGONIA ', 'NN', '', '0000-00-00', '', 'NNN', 'NNN', 'NNN'),
(104, 2, 5, 1, '35498416', 'GARRIDO CARLOS ', 'NN', 'NNN', '2017-02-08', '', 'NNN', 'NN', 'NNN'),
(105, 2, 5, 1, '496846516', 'PEDOTO SUSANA ', 'NNN', 'NNN', '2017-02-08', '', 'NNN', 'NNN', 'NNN'),
(106, 2, 6, 1, '9816531', 'REYMUNDO LILIANA ', 'NNN', 'NNN', '2017-02-08', '', 'NNN', 'NNN', 'NNN'),
(107, 1, 3, 1, '984651561', 'SALINAS CARLOS AMADOR OTTO', 'NNN', 'NN', '2017-02-08', '', 'NNN', 'NNN', 'NN'),
(108, 2, 5, 1, '631556163', 'REPETTO MAGALI ', 'NNN', 'NNN', '2017-02-08', '', 'NNN', 'NNN', 'NNN'),
(109, 2, 5, 1, '98456156', 'GONZALEZ GUILLERMO ', 'NNN', 'NNN', '2017-02-08', '', 'NNN', 'NNN', 'NN'),
(110, 2, 5, 1, '314846', 'VERA HECTOR ', 'NN', 'NNN', '2017-02-08', '', 'NNN', 'NN', 'NNN'),
(111, 2, 5, 1, '45848516', 'SUAREZ MARCELO ', 'NNN', 'NN', '2017-02-08', '', 'NNN', 'NNN', 'NNN'),
(123, 2, 5, 1, '65123', 'CASTRO PABLO IGNACIO', 'NNN', 'NNN', '2017-02-08', '', 'NNN', 'NNN', 'NNN'),
(171, 2, 5, 1, '8464', 'CASTRO PABLO IGNACIO', 'N', 'N', '2017-02-08', '', 'N', 'N', 'NN'),
(172, 2, 27, 0, '894651', 'BLOQUEO PATAGONIA ROAD ', 'NN', '', '0000-00-00', '', 'NNN', 'NN', 'NNN'),
(217, 3, 26, 1, '98476156', 'SANCHEZ FERNANDO ', 'NNN', 'NN', '2017-02-08', '', 'NNN', 'NN', 'NNN'),
(219, 2, 5, 1, '789658720', 'FRUTOS SONIA BEATRIZ', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(220, 2, 28, 1, 'NXR3K59C2', 'BEELAARD WILHELMUS', 'N', 'N', '2017-06-21', '', 'N', 'N', 'N'),
(221, 2, 5, 1, '89332556', 'ALONSO NORA ANDREA', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(222, 2, 6, 1, '85987231', 'FAURA ANDRES IGNACIO', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(223, 2, 5, 1, '85963214', 'ROLDAN ERICA', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(224, 2, 5, 1, '78451236', 'LOZA VIAGIOLI VICTOR ANDRES', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(225, 2, 29, 1, '17588984', 'TABOADA PABLO', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(226, 2, 5, 1, '69852147', 'BROMBERG FACUNDO', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(227, 2, 5, 1, '85212258', 'MARCHELLO GUSTAVO DANIEL', 'N', 'N', '2017-02-02', '', 'N', 'N', 'N'),
(228, 2, 5, 1, '98465165', 'ABALO ETELINDA ', 'NNN', 'NN', '2017-02-10', '', 'NNN', 'NNN', 'NN'),
(229, 2, 6, 1, '1348646', 'PEREIRA ERIKA ', 'NNN', 'NN', '2017-02-10', '', 'NNN', 'NNN', 'NN'),
(230, 2, 5, 1, '5649968', 'DEMINAS CARLOS ', 'NNN', 'NN', '2017-03-10', '', 'NN', 'NNN', 'NN'),
(231, 2, 24, 1, '189479841', 'BRUNO GUSTAVO ', 'NNN', 'NN', '2017-03-10', '', 'NNN', 'NNN', 'NN'),
(232, 2, 24, 1, '36189469', 'KATZ BENJAMIN ', 'NNN', 'NNN', '2017-03-10', '', 'NNN', 'NNN', 'NNN'),
(233, 2, 5, 1, '9846516', 'KATZ BENJAMIN', 'NNN', 'NNN', '2017-02-10', '', 'NNN', 'NNN', 'NN'),
(234, 2, 5, 1, '894613', 'ADOLFO NISTOR ', 'NNN', 'NN', '2017-02-10', '', 'NNN', 'NNN', 'NN'),
(235, 2, 5, 1, '89657412', 'MANAVELLA ANALIA LAURA', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(245, 2, 5, 1, '12457896', 'GRECO MARIA GUADALUPE', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(246, 2, 5, 1, '78965423', 'GAITAN LAURA ESTER', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(247, 2, 5, 1, '984156', 'DEMINAS CARLOS ', 'nnn', 'nn', '2017-02-11', '', 'nnn', 'nnn', 'nnn'),
(248, 2, 5, 1, '021654651', 'Mariano Morez', 'NNN', 'NN', '2017-02-11', '', 'NNN', 'NN', 'NNN'),
(249, 2, 5, 1, '2314646', 'CARRIZO NATALIA ', 'NNN', 'NN', '2017-02-11', '', 'NN', 'NNN', 'NN'),
(250, 2, 5, 1, '91316', 'SPIEGEL EMILIO', 'NNN', 'NNN', '2017-02-11', '', 'NNN', 'NNN', 'NN'),
(251, 2, 5, 1, '8946516', 'MEDINA YESICA ', 'NNN', 'NN', '2017-02-11', '', 'NN', 'NN', 'NNN'),
(252, 2, 5, 1, '549894', 'LOPEZ SILVINA ', 'NNN', 'NN', '2017-02-11', '', 'NNN', 'NNN', 'NNN'),
(253, 2, 5, 1, '815616', 'FERNANDEZ ALLAN ', 'NN', 'NN', '2017-02-11', '', 'NNN', 'NNN', 'NN'),
(254, 2, 5, 1, '82146', 'MONTORO GERARDO ', 'NNN', 'NN', '2017-02-11', '', 'NNN', 'NN', 'NNN'),
(255, 2, 5, 1, '2154516', 'AGRANATI ALEJANDRO ', 'NN', 'NN', '2017-02-11', '', 'NNN', 'NNN', 'NNN'),
(256, 2, 5, 1, '8165486416', 'NAVARRO EVELYN ', 'NN', 'NN', '2017-02-11', '', 'NNN', 'NN', 'NNN'),
(257, 2, 5, 1, '9846521', 'LUIS PASCUAL ', 'NNN', 'NN', '2017-02-11', '', 'NN', 'NNN', 'NN'),
(258, 2, 5, 1, '75141984', 'SANTIAGO SEBASTIAN ', 'NNN', 'NN', '2017-02-11', '', 'NN', 'NNN', 'NNN'),
(259, 2, 5, 1, '8465143514', 'RUIZ MABEL ', 'NNN', 'NN', '2017-02-11', '', 'NN', 'NNN', 'NNN'),
(260, 2, 6, 1, '8913561465', 'BOLO PILAR ', 'NN', 'NN', '2017-02-11', '', 'NNN', 'NN', 'NNN'),
(261, 2, 30, 1, '984684', 'ESPERTINO MIGUEL ', 'NN', 'NN', '2017-02-11', '', 'NNN', 'NNN', 'NN'),
(262, 2, 5, 1, '1489465', 'CORDOBA JORGE ', 'NNN', 'NN', '2017-02-12', '', 'NNN', 'NNN', 'NN'),
(263, 2, 5, 1, '8416849', 'DERCIO GUSTALLI', 'NNN', 'NN', '2017-02-11', '', 'NNN', 'NN', 'NNN'),
(264, 2, 5, 1, '89465416', 'GAITAN LAURA ', 'NNN', 'NN', '2017-02-11', '', 'NNN', 'NNN', 'NN'),
(265, 2, 5, 1, '123456', 'FERNANDEZ MARIA ESTEFANIA', 'NN', 'NN', '2017-02-13', '', 'NN', 'NN', 'NN'),
(266, 2, 5, 1, '2222222', 'MANRIQUE JOSE MOM ', 'NN', 'NN', '2017-02-13', '', 'NN', 'NN', ''),
(267, 2, 5, 1, '1234', 'ROUSSELLE DAVID MARC', 'XX', 'XX', '2017-03-10', '', 'XX', 'XX', 'XX'),
(279, 2, 5, 1, '12345678', 'LOBAY MARIA JOSE ', 'NN', 'NN', '2017-02-13', '', 'NN', 'NN', 'NN'),
(282, 2, 5, 1, '1234567', 'MARTINS ALEX ROLDAN ', 'XXX', 'XX', '2017-02-13', '', 'XX', 'XX', 'XX'),
(283, 2, 6, 1, '78545587', 'VASQUEZ FORTUNATO GARCIA', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(284, 2, 5, 1, '96369639', 'ARGOTA JOSE AUGUSTO', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(285, 2, 5, 1, '78788589', 'FERNANDEZ MANUEL ', 'N', 'N', '2017-02-08', '', 'N', 'N', 'N'),
(286, 2, 5, 1, '85263219', 'BODENLLE ESTHER ANGELA', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(296, 2, 5, 1, '12346', 'POLETTI LILIANA ESTHER', 'NN', 'NN', '2017-02-14', '', 'N', 'NN', 'NN'),
(297, 2, 5, 1, '14112225', 'MACHADO JOSE DANIEL', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(298, 2, 5, 1, '85258753', 'POLETTI LILIANA ESTHER', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(299, 2, 5, 1, '87898858', 'SALVADOR SEBASTIAN JAVIER', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(300, 2, 5, 1, '74569821', 'MUÃ‘OZ MANUEL', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(301, 2, 5, 1, '87888963', 'SOLER GUSTAVO', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(302, 2, 5, 1, '74147444', 'MUÃ‘OZ MANUEL', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(303, 2, 5, 1, '55555888', 'RADICE CLAUDIA', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(304, 2, 5, 1, '58887963', 'COLINA PATRICIA', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(305, 2, 5, 1, '85888896', 'MALTISOTTO CARMELO', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(306, 2, 4, 1, '85232121', 'PUYOS FLORENCIA', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(307, 2, 5, 1, '85889999', 'GABRILEIKO VLADIMIR', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(308, 2, 5, 1, '85888877', 'SOTO MARTA SUSANA', 'n', 'n', '2017-02-01', '', 'n', 'n', 'n'),
(309, 2, 5, 1, '22222225', 'CHIRINO DANIEL', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(310, 2, 6, 1, '58887896', 'CORDERO MELISA DEL VALLE', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(311, 2, 5, 1, '89878858', 'PONCE PABLO CHRISTIAN', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(312, 2, 5, 1, '87896698', 'DE LA CALLE NAHUEL MATIAS', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(313, 2, 6, 1, '58555555', 'VASQUEZ FORTUNATO GARCIA', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(314, 2, 5, 1, '78999999', 'MUÃ‘OZ JORGE ALBERTO', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(315, 2, 4, 1, '14515952651', 'ESCOBAR JUAN MANUEL', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(316, 2, 5, 1, '888887774', 'ALBEROLA ALFREDO LUIS', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(317, 2, 6, 1, '1', 'CRIADO MARIA CRISTINA ', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(324, 2, 31, 1, '88552221', 'RISOLA MARCELO', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(325, 2, 32, 1, '478878885', 'OLIER MARIA ', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(327, 2, 5, 1, '12225212', 'TABACMAN MARIO EDUARDO', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(328, 2, 5, 1, '85887888', 'PEÃ‘A NELCY SUSANA ABELLA', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(329, 2, 5, 1, '87898587', 'QUILCI SOFIA', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(330, 3, 33, 0, '18091437', 'SAFFIRIO CLAUDIO', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(331, 3, 34, 1, '7', 'VEGA GUILLERMO ALEJANDRO', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(332, 2, 6, 1, '89878545', 'VERCESI MARIANO ', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(333, 2, 8, 1, '858887887', 'DAGOSTINO GRACIANA', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(334, 2, 5, 1, '89996698', 'STRACQUADAINI ROSANA', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(335, 2, 5, 1, '78789999', 'LLANOS FERNANDO DANIEL ', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(336, 2, 4, 1, '74522212', 'FELICIONI SEBASTIAN', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(337, 2, 5, 1, '58585587', 'MATTOS RAFAEL', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(338, 2, 5, 1, '85696632', 'DI MATTEO BEATRIZ CRISTINA', 'n', 'n', '2017-02-01', '', 'n', 'n', 'n'),
(339, 2, 5, 1, '8', 'ABAIT SANDRA ELISABET', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(340, 2, 5, 1, '7411111', 'CAMPOS LEANDRO GABRIEL', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(341, 1, 3, 1, '123654', 'SANCHEZ VICTOR ', 'NN', 'NN', '2017-02-20', '', 'NN', 'NN', 'NN'),
(342, 1, 3, 1, '456123', 'ETCHEDER FLORENCIA', 'NN', 'NN', '2017-02-20', '', 'NN', 'NN', 'NN'),
(343, 3, 35, 1, '74474447', 'CELMAN SEBASTIAN', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(344, 3, 36, 1, '1212112', 'BASUALDO DANIEL', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(345, 2, 5, 1, '3333232356', 'MUÃ‘OZ RICARDO', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(346, 2, 6, 1, '47874748', 'HERRERALAFAILLE ADRIANA', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(347, 1, 3, 1, '121', 'PARTIANSKY EHUD ', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(348, 2, 5, 1, '14654156204', 'VELIZ EVANDRO', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(349, 2, 5, 1, '14545624259', 'MARTINEZ FRANCISCO MARCOS', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(350, 3, 37, 1, '166868969', 'REVAH JOSE LUIS', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(351, 1, 3, 1, '456789', 'CARDOZO MATIAS ', 'NN', 'NN', '2017-02-22', '', 'NN', 'NN', 'NN'),
(352, 2, 5, 1, '25474125', 'BRESSAN MARIELA SILVANA', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(353, 2, 5, 1, '25624954295289+', 'MENDOZA GAUCHE GUILLERMO', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(354, 2, 4, 1, '4528646819', 'CENA ANDREA', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(355, 1, 3, 1, '491981989', 'KATZ SERGIO', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(356, 2, 5, 1, '542824894194', 'MORALES MARGARITA SUSANA', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(357, 3, 39, 0, '27.683.865 ', 'MADDIO DIEGO LEONARDO', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(358, 3, 39, 0, '34.078.583 ', 'TARDITTI MAXIMILIANO ', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(359, 3, 39, 0, '36.169.441 ', 'SOSA MARTIN', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(360, 3, 39, 0, '36.164.722', 'PATEMOSTRO NICOLAS PABLO', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(361, 2, 6, 1, '516546', 'DOMINGUEZ CONSTANZA MAGALI', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(362, 2, 40, 1, '58', 'CESARIS CARLOS DANIEL', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(363, 2, 41, 1, '156218419', 'CARDILE MONICA', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(364, 3, 38, 1, '122', 'BUSICO LUS MARIA', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(365, 2, 6, 1, '15046', 'MORETTA LUCIA', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(366, 2, 5, 1, '548', 'PARRAGUEZ BERNARDITA', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(367, 2, 6, 1, '54564564', 'FONTES RUY', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(369, 2, 6, 1, '559459', 'SILVEIRA ANTONIO CARLOS', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(370, 2, 6, 1, '562494', 'MARTINS CARLOS ALBERTO', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(371, 2, 6, 1, '48946447', 'MARTINS CARLOS ALBERTO', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(372, 2, 5, 1, '41274527', 'BADANO JAVIER', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(373, 2, 5, 1, '25665646', 'DE SAO FRANCISCO JOSE', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(375, 2, 6, 1, '5648798794879', 'DE SAO FRANCISCO JOSE', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(376, 2, 5, 1, '46464642878', 'SMITH AGUSTINA', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(377, 3, 36, 1, '10501905', 'CHIABO ELBIO MANUEL', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(378, 2, 6, 1, '5619849849', 'SORIA FERNANDO', 'N', 'N', '2017-02-01', '', 'N', 'N', 'N'),
(379, 2, 24, 1, '47474478', 'CECCO MATIAS OSVALDO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(380, 2, 6, 1, '14145214', 'ACINAPURO SEBASTIAN ARIEL', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(381, 2, 6, 1, '726878791879', 'SCADUTO ANA CAROLINA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(384, 2, 6, 1, '123', 'VILLALBA MARIANO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(385, 2, 6, 1, '454949879', 'JELCIC GUILLERMO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(386, 2, 6, 1, '7879', 'SAINT GERMAIN JOSE RAMON', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(387, 2, 6, 1, '58929889', 'ROSTON JESICA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(388, 2, 6, 1, '78', 'RANGUGNI JORGE LUIS', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(389, 2, 6, 1, '9874556', 'GRYNBERG EDGARDO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(390, 2, 5, 1, '1245878', 'LALLANA ALBERTO ANTONIO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(391, 2, 5, 1, '178', 'LALLANA ALBERTO ANTONIO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(392, 1, 3, 1, '7857187', 'DI LONARDI SILVINA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(393, 2, 5, 1, '487', 'VARGAS JULIAN', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(394, 2, 4, 1, '1441141', 'BROZZONE VIRGINIA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(395, 3, 42, 1, '18484', 'OLEA SANTIAGO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(396, 2, 5, 1, '41741751687', 'ARISTEGUI SABRINA NATALIA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(397, 2, 5, 1, '8942848978', 'PISANO OSVALDO JOSE', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(398, 2, 5, 1, '54678777', 'SANTA MARIA ALEJANDRA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(400, 2, 5, 1, '898889454', 'LEGUIZAMON LAURA ', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(401, 1, 3, 1, '7878979798879', 'KATZ MIRTA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(402, 3, 43, 1, '47477466211211', 'AVILA ALBERTO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(403, 2, 44, 1, '15178', 'CAPECE FERNANDO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(404, 2, 5, 1, '5787878787', 'FIGUEREDO JUAN MANUEL ALMADA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(405, 3, 45, 1, '12411554', 'ARREGUI RICARDO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(406, 2, 46, 1, '487879', 'JOCKEY CLUB ROSARIO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(407, 2, 47, 1, '44718917', 'ABAD CARLOS ADRIAN', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(408, 2, 5, 1, '332154754', 'MALDONADO SUSANA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(409, 3, 10, 1, '17080762', 'SCHAJNOVETZ CESAR', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(410, 2, 21, 1, '516544994898', 'SPOONER PHILIP GUY ALFRED', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(411, 2, 5, 1, '12522587', 'SPOOL CRISTIAN TOMERIA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(412, 3, 48, 1, '5488712221478', 'VAENA ALBERTO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(413, 2, 8, 1, '141741741741', 'PEDROTE ANGEL', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(414, 3, 49, 1, '12156464646789', 'PICO MARCELO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(415, 2, 5, 1, '14178714956651689', 'SAAVEDRA LILIANA ROSARIO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(437, 3, 10, 1, '18012030', 'POTES CLAUDIO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(438, 3, 50, 1, '114699977', 'BARATTI NICOLAS', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(439, 3, 42, 1, '14745118', 'BEDETTI GISELA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(440, 2, 6, 1, '14141719639396', 'BARATTI PABLO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(441, 3, 51, 0, '124552214', 'LOWISZ ALBERTO MARTIN', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(442, 1, 3, 0, '1245', 'HERNANDEZ ISABEL', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(443, 3, 16, 1, '551414156', 'ZAYAS EMILIO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(444, 2, 5, 1, '7477788887', 'GALVAN CARLA DANIELA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(445, 2, 5, 1, 'ASDASD', 'VARAS RAMIREZ VARAS', 'N', 'N', '2017-03-01', '', 'N', 'N', ''),
(446, 3, 16, 0, '141441414', 'AMERICAN JET ATR X1', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(447, 2, 6, 1, '14145412', 'VERGER NORBERTO ', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(448, 1, 3, 1, '14111', 'BARANDIARÃN RAÃšL', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(449, 3, 52, 1, '2154617461', 'SEGHIER SILVIO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(450, 3, 10, 1, '05647874', 'GAMIZ SEBASTIAN MARTI', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(451, 3, 10, 1, '15646456888', 'CATTANEO DIEGO MATIAS', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(452, 3, 10, 1, '156431651', 'MEDIALDEA ADRIAN', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(453, 3, 37, 1, '54865635', 'SINISTERRA LARISSA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(454, 3, 10, 1, '1124224', 'ALONSO CLAUDIO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(455, 2, 5, 1, '12114125', 'MARTINEZ LEANDRO AGUSTIN', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(456, 2, 4, 1, '15649979756123', 'BURSTEIN NIDIA IRENE X1', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(457, 2, 4, 1, '14784521', 'MALDONADO SILVIA X3', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(458, 2, 4, 1, '9455647', 'OTERO JORGE X1', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(459, 2, 5, 1, '1554546771', 'COLUSSI FABIAN MARCELO X2', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(460, 2, 5, 1, '5648797425266', 'DORR MARCELO GASTÃ“N X4', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(461, 2, 5, 1, '5898464', 'ESCUDERO ALEJANDRA BEATRIZ', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(462, 2, 5, 1, '747411914', 'RODRIGUES SANTOS MURILLO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(463, 2, 5, 1, '87799899', 'ALTAMIRANO YASNA PAOLA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(464, 2, 4, 1, '7789389', 'ROLDAN VALERIA MARCELA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(465, 2, 5, 1, '21587478', 'MARTINEZ MARIA BELEN X1', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(466, 2, 5, 1, '564224924479', 'MARINELLI LUCIANO DIEGO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(467, 2, 5, 1, '53853244', 'FESTA ALEJANDRO OSVALDO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(468, 3, 53, 1, '26079140', 'NAVARRO DARIO X1', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(469, 3, 53, 1, 'MAIALE MAXIMILIANO X1', '31962628', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(470, 3, 53, 1, '32320580', 'CASTEIG JUAN JOSE MUÃ‘OZ X1', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(471, 3, 53, 1, '31962628', 'MAIALE MAXIMILIANO X1', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(472, 3, 26, 1, '564204040689', 'CARRIZO HERNANDO X1', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(473, 3, 26, 1, '727453233474327432', 'MOURE CLAUDIO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(474, 2, 5, 1, '15298299', 'MONTIEL MAURICIO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(475, 2, 5, 1, '2156427', 'HELLMAN IVAN', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(476, 2, 4, 1, '629774187', 'HELLMAN IVAN', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(477, 2, 6, 1, '124587524', 'CASTRO DEBORA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(478, 2, 5, 1, '17415635275287', 'CISNEROS MARIA FLORENCIA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(479, 2, 54, 1, '49498849', 'COMOLLI DIEGO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(480, 2, 55, 0, '84789529', 'BLOQUEO WEDELL TRAVEL', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(483, 4, 56, 1, '123321', 'BERTONA JUAN', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(484, 3, 52, 1, '52528528518', 'SANCHEZ JOSE FRANCISCO ', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(485, 2, 5, 1, '121456456456', 'ZULEMA ITATI SALAS', 'n', 'n', '2017-03-01', '', 'n', 'n', 'n'),
(486, 3, 33, 1, '18423890', 'FLORES DANIEL', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(487, 2, 4, 1, '124334824', 'CAPUTO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(488, 2, 4, 1, '54478784289429', 'CAPUTO DIEGO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(489, 2, 32, 0, '11451611', 'BLOQUEO AEROLÃNEAS', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(490, 3, 10, 1, '123636544', 'BERTEDOR HUGO ALFREDO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(491, 3, 50, 1, '15614249849', 'ROSSI EMILIO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(492, 3, 26, 1, '787987963638484', 'Wanschelbaum Daniel ', 'n', 'n', '2017-03-01', '', 'n', 'n', 'n'),
(493, 3, 26, 1, '526231145', 'GIUGGIO ROBERTO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(494, 2, 57, 0, '159523489249', 'BLOQUEO CORDITUR', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(495, 2, 8, 1, '6717885452414144', 'VALENZUELA CARLOS', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(496, 3, 33, 1, '16891488', 'SANCHEZ FERNANDO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(497, 2, 58, 1, '8677474274274', 'BUSTOS SILVIA SUSANA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(498, 2, 5, 0, '5428248489', 'BARRAU MARINA GISEL ', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(499, 4, 56, 1, '6421115', 'PASTOR MARIA TERESA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(501, 2, 31, 1, '63366399489', 'HURIE ARIEL OMAR', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(502, 2, 31, 1, '87777888', 'PALOPOLI JUAN MANUEL', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(503, 2, 6, 1, '11544', 'ANDRADA SERGIO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(504, 3, 60, 1, '123164564', 'MANDOLESI SERGIO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(505, 2, 5, 1, '564892487', 'BENITEZ EMILCE LIDIA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(506, 2, 5, 1, '5642977297', 'REMONDA GUILLERMO RICARDO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(507, 3, 10, 1, '24545426', 'PARDO FABRICIO ISAIAS', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(508, 2, 6, 1, '52824829', 'CAMBRIA CLAUDIO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(509, 2, 5, 1, '27747999988', 'ADAMS MARCOS GUILLERMO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(510, 2, 6, 1, '5247471456656', 'PAVON NILDA CRISTINA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(512, 2, 5, 1, '568661786', 'JORQUERA PINTO PABLO RODRIGO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(514, 2, 4, 1, '5681962627', 'AGOSTI JOSE RAUL', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(515, 2, 4, 1, '2474751561234', 'CEPPARO CARLOS MARTIN', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(516, 2, 5, 1, '574741236996', 'MINGO CRISTIAN', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(517, 2, 4, 1, '4864624889', 'BARRIO MARIANA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(518, 2, 5, 1, '4387668', 'OCAMPO ADELA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(519, 2, 6, 1, '11111111111111111', 'MORANDINI LIDIA ', 'NNN', 'NNN', '2017-03-14', '', 'NN', 'NNN', 'NN'),
(520, 2, 44, 1, '588623238789', 'GALDAMES ALDERETE', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(521, 2, 5, 1, '587718789419', 'FIGUEROA SILVIA CAROLINA ', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(522, 3, 60, 1, '56789756587', 'DE MONTE SERGIO', 'N', 'N', '2017-03-01', '', 'N', 'NN', 'N'),
(523, 3, 61, 1, '5484288449', 'ALBIERO GASTON', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(524, 3, 62, 1, '746654988', 'PERCO LEONARDO GASTON', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(525, 3, 10, 1, '54679879546546', 'VALENZUELA CARLOS', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(526, 3, 10, 1, '878945315379', 'VALENZUELA HERALDO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(528, 3, 10, 1, '969328417', 'VALENZUELA HERALDO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(529, 3, 10, 1, '52488789729792817879', 'VERNA FACUNDO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(530, 3, 10, 1, '56481771', 'MATIJACEVICH MAURICIO JAVIER', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(531, 2, 63, 1, 'PA0026263', 'CASTILLO NORTE VERONICA MARIA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(532, 2, 5, 1, '68871789', 'RIVERO NATALIA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(533, 2, 5, 1, '47171719', 'CHIARANDIA MARIA JULIA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(534, 2, 8, 1, '678748199', 'LLOREDA MIGUEL ANGEL', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(535, 2, 6, 1, '468486484846', 'EMERY DANIEL', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(536, 2, 5, 1, '45694984', 'ARCA MARIO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(537, 2, 5, 1, '222222222222', 'GONZALEZ VICTOR ', 'nnn', 'nnn', '2017-03-15', '', 'nnn', 'nnn', 'nnn'),
(538, 3, 26, 1, '787897564545678797623489', 'INOSTRAZA JAVIER', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(539, 3, 64, 1, '1215445455588', 'VERBITSKY JUAN', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(540, 3, 10, 1, '78778796969633232958', 'MALDONADO DANIEL GERARDO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(541, 2, 6, 1, '882864564', 'BUELGA CELINA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(542, 1, 3, 1, '5564544648515648', 'PASTENE CRISTIAN', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(543, 2, 4, 1, '48422474122121', 'STRAFFURINI RAUL OSCAR', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(544, 2, 22, 1, '228789879', 'COBO BEATRIZ X2', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(545, 3, 51, 1, '848947418787', 'MENEGHINI JUAN', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(546, 2, 5, 1, '4444444444444444', 'DELIS EDUARDO ELEUTERIO', 'NN', 'NN', '2017-03-16', '', 'NN', 'NN', 'NN'),
(547, 3, 65, 1, '16820054', 'ARCE JOSE LUIS', 'N', 'N', '2015-04-01', '', 'N', 'N', 'N'),
(549, 3, 66, 1, '20503451', 'CURRAO SALVADOR', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(550, 3, 37, 1, '1234564654', 'DELFINO MARCELO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(551, 2, 6, 1, '25484114711', 'ASTESIANO CARMELO EDUARDO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(552, 2, 5, 1, '742882789', 'VILLAMIL JUAN CARLOS', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(553, 3, 67, 1, '855987', 'VIVALDA NICOLAS', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(554, 3, 67, 1, '7879879879879', 'DE KEMMETER JORGELINA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(555, 3, 67, 1, '14114711174', 'PARRAGUEZ JORGE', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(556, 3, 67, 1, '232323235654', 'VIVALDA NICOLAS', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(557, 1, 3, 0, '121344798', 'MCDORMAN FRANK', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(558, 2, 5, 1, '548445615646', 'GONZALEZ PATRICIA MONICA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(559, 2, 5, 1, '449744994', 'RODRIGUEZ CARMEN MARIA ALCALDE', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(560, 2, 6, 1, '82187778123288744', 'MORANDI LIDIA RENE', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(561, 2, 31, 1, '787513548789', 'CANCINO MONICA ', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(562, 3, 68, 1, '4846848486', 'COMBA PABLO ', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(563, 2, 31, 1, '574141841', 'CAMPOS MARCELO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(564, 2, 31, 1, '4787778899969', 'GARCIA / BERTONE', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(565, 2, 31, 1, '57421121141', 'GRANT / SKOROJOD', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(567, 2, 5, 1, '544458819', 'ROJAS JUAN CARLOS', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(568, 2, 5, 1, '9848648647', 'ARRIBAS JULIETA BELEN', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(569, 2, 5, 1, '8997848927/*', 'ROJAS JUAN CARLOS', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(570, 2, 5, 1, '99966663669', 'DOMINGUEZ ROSA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(571, 2, 5, 1, '484974516516', 'DONCOSO REBECA BEATRIZ', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(572, 4, 56, 1, '58744878787', 'CARLETI ROSANA ', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(573, 2, 31, 1, '1546654561554', 'HAURIGOT AGUSTIN', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(574, 2, 5, 1, '5487414187', 'BALUSTRO MARTA SUSANA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(575, 2, 6, 1, '71741285283939', 'ZUCCARI ANALIA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(576, 2, 31, 1, '54646851546', 'MONGE MAUGER LUCIANO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(577, 2, 5, 1, '4648971', 'BALUSTRO MARTA SUSANA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(578, 2, 5, 1, '7489819', 'GAVILANES TERESA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(579, 1, 3, 1, '465464564897551', 'GIUDICI GUILLERMO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(581, 2, 5, 1, '56465454894', 'REYNOLD CLAUDIO AUGUSTO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(582, 2, 5, 1, '62489754867', 'COLUSSI FABIAN MARCELO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(583, 2, 4, 1, '45486465486162841', 'WILLI LIU IBRAHIM AFRANIO', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(584, 2, 69, 1, '5484534898794', 'FALASCA JIMENA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(585, 2, 69, 1, '8246819452542', 'IGLESIAS GABRIELA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(586, 2, 22, 1, '456486', 'CORREA LUZ', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(587, 1, 3, 1, '666666666666666', 'COMINI FABIAN ', 'NNN', 'NNN', '2017-03-21', '', 'NNN', 'NN', 'NN'),
(588, 1, 3, 1, '777777777777777', 'DA COSTA MARQUEZ HERNAN', 'NNN', 'NN', '2017-03-21', '', 'NNN', 'NNN', 'NN'),
(589, 1, 3, 1, '88888888888888888', 'SALSE CLAUDIA ', 'NNN', 'NNN', '2017-03-21', '', 'NNN', 'NN', 'NNN'),
(590, 2, 5, 1, '99999999999999', 'KERVORK CARLOS', 'NNN', 'NNN', '2017-03-21', '', 'NNN', 'NNN', 'NNN'),
(591, 2, 5, 1, '986548646', 'BOZZO GASTON ', 'NNN', 'NNNN', '2017-03-21', '', 'NNN', 'NNN', 'NNN'),
(592, 2, 5, 1, '56461565646', 'CASTRIOTA NATALIA DEBORA', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(593, 2, 5, 1, '54646464645', 'MULVIHILL CYNTHIA EUGENIA ', 'N', 'N', '2017-03-01', '', 'N', 'N', 'N'),
(595, 1, 3, 1, '62626262', 'DDFDD', 'SDFDSF', 'FSDFDSF', '2017-04-05', '', '45345', 'sil.beranek@gmail.com', '435345'),
(596, 2, 5, 1, '123456789', 'SUAREZ ENZO MAXIMILIANO', 'N', 'N', '2017-07-01', '', 'N', 'N', 'N'),
(599, 2, 4, 1, '987654321', 'BARBITTA JULIAN', 'N', 'N', '2017-05-01', '', 'N', 'N', 'N'),
(600, 2, 6, 1, '628818168419', 'CAJE CUEVAS CRISTINA', 'N', 'N', '2017-05-17', '', 'N', 'N', 'N'),
(601, 2, 4, 1, '33345484111148', 'DOBLAS ROMINA', 'N', 'N', '2017-05-02', '', 'N', 'N', 'N'),
(602, 2, 5, 1, '787982314', 'CUELLO DIXCIA', 'N', 'N', '2017-05-01', '', 'N', 'N', 'N'),
(603, 2, 5, 1, '518747125262', 'ROA ROMINA LORENA', 'N', 'N', '2017-05-04', '', 'N', 'N', 'N'),
(604, 2, 5, 1, '5656525818489', 'EIRIZ OSVALDO LUCIO ', 'N', 'N', '2017-05-04', '', 'N', 'N', 'N'),
(605, 2, 5, 1, '51561615', 'ROJAS ALVARO TOMAS', 'N', 'N', '2017-05-18', '', 'N', 'N', 'N'),
(606, 2, 5, 1, '24564845157', 'APREA OSCAR ALBERTO', 'N', 'N', '2017-05-18', '', 'N', 'N', 'N'),
(607, 2, 5, 1, '1212681184156', 'DOMINE GUSTAVO', 'N', 'N', '2017-05-23', '', 'N', 'N', 'N'),
(608, 2, 5, 1, '865684954656849', 'MALDONADO HUGO RICARDO', 'N', 'N', '2017-05-02', '', 'N', 'N', 'N'),
(609, 2, 5, 1, '78786154874348945146', 'RAMACCIOTTI RICARDO DANIEL', 'N', 'N', '2017-05-18', '', 'N', 'N', 'N'),
(610, 2, 5, 1, '57546546849449', 'DEPINO SEBASTIAN', 'N', 'N', '2017-05-24', '', 'N', 'N', 'N'),
(611, 2, 5, 1, '41546515919519', 'DHERS JUAN', 'N', 'N', '2017-05-03', '', 'N', 'N', 'N'),
(612, 1, 3, 1, '30176247', 'Ricardo Lucero', 'espaÃ±a 2642 ', 'Mendoza', '1984-06-05', '', '2612063559', 'lucero_ricardo22@hotmail.com', '5500'),
(613, 2, 5, 1, '2123156417', 'FERNANDEZ MARIA JOSE', 'N', 'N', '2017-05-17', '', 'N', 'N', 'N'),
(614, 2, 5, 1, '8987987954621384979', 'TORRES ALVARO GASTON', 'N', 'N', '2017-05-24', '', 'N', 'N', 'N'),
(615, 2, 5, 1, '564818917781', 'CURTI ITAMAR ', 'N', 'N', '2017-05-24', '', 'N', 'N', 'N'),
(616, 2, 5, 1, '575654612348', 'PERETTI GISELA ELIZABETH', 'N', 'N', '2017-05-16', '', 'N', 'N', 'N'),
(617, 2, 5, 1, '24466741741741', 'GOMEZ RAMON ALBERTO', 'N', 'N', '2017-05-01', '', 'N', 'N', 'N'),
(618, 2, 5, 1, '2313548914879', 'PEREZ MONICA LILIANA ', 'N', 'N', '2017-05-02', '', 'N', 'N', 'N'),
(619, 2, 5, 1, '484618589849849', 'MARSICO MABEL ADRIANA', 'N', 'N', '2017-05-17', '', 'N', 'N', 'N'),
(620, 2, 5, 1, '822792545415454', 'ALBEIO RAUL DAMIAN', 'N', 'N', '2017-05-09', '', 'N', 'N', 'N'),
(621, 2, 4, 1, '54654654684684684', 'PUCCIARIELLO MIGUEL MAJUL', 'N', 'N', '2017-05-03', '', 'N', 'N', 'N'),
(622, 2, 5, 1, '64646874565', 'RUBEL CAROLINA', 'N', 'N', '2017-08-21', '', 'N', 'N', 'N'),
(623, 2, 4, 1, '547771952727', 'RUBEL CAROLINA', 'N', 'N', '2017-05-23', '', 'N', 'N', 'N'),
(624, 2, 4, 1, '545446546546546', 'MANES ROMINA', 'N', 'N', '2017-05-09', '', 'N', 'N', 'N'),
(625, 2, 5, 1, '44651346846846', 'QUARCHIONI ALEJANDRO DANIEL', 'N', 'N', '2017-05-23', '', 'N', 'N', 'N'),
(626, 2, 4, 1, '5648645648792', 'FONTANARI CARLOS ANDRES', 'N', 'N', '2017-05-23', '', 'N', 'N', 'N'),
(627, 2, 4, 1, '8876554541', 'PIVA NOEMI ZULEMA', 'N', 'N', '2017-05-16', '', 'N', 'N', 'N'),
(628, 2, 5, 1, '78789561562587686', 'GAMARRA LUIS HERNAN ROMERO', 'N', 'N', '2017-05-23', '', 'N', 'N', 'N'),
(629, 2, 5, 1, '798713145123696393', 'LOPEZ MARICEL LUJAN', 'N', 'N', '2017-05-16', '', 'N', 'N', 'N'),
(630, 2, 5, 1, '48641878789', 'SANTISTEBAN AMALIA X1', 'N', 'N', '2017-05-02', '', 'N', 'N', 'N'),
(631, 3, 39, 1, '151619191919', 'BRUNO SANDRO', 'N', 'N', '2017-05-17', '', 'N', 'N', 'N'),
(632, 3, 39, 1, '35623986', 'ESPOSITO JOSE', 'N', 'N', '2017-05-31', '', 'N', 'N', 'N'),
(633, 1, 3, 1, '849849849494', 'MARTINO DIEGO', 'N', 'N', '2017-05-01', '', 'N', 'N', 'N'),
(634, 2, 5, 1, '54656495489', 'FAINGOL LILIANA HAYDEE', 'N', 'N', '2017-05-23', '', 'N', 'N', 'N'),
(635, 2, 5, 1, '64646492717', 'CUELLO FAVIO HERNAN', 'N', 'N', '2017-05-09', '', 'N', 'N', 'N'),
(636, 2, 5, 1, '646527817816899', 'PRIETO SILVIA LEONOR', 'N', 'N', '2017-05-01', '', 'N', 'N', 'N'),
(637, 2, 5, 1, '28197817817981', 'WILLIAM MARIA IRENE', 'N', 'N', '2017-05-01', '', 'N', 'N', 'N'),
(639, 2, 70, 1, '65649878912341', 'MASTROFILIPPO JOSE', 'N', 'N', '2017-05-17', '', 'N', 'N', 'N'),
(640, 2, 71, 1, '5656454064', 'ROMEIRO ROGELIO', 'N', 'N', '2017-05-02', '', 'N', 'N', 'N'),
(641, 2, 72, 1, '56797546897949', 'BOBADILLA NESTOR', 'N', 'N', '2017-05-26', '', 'N', 'N', 'N'),
(643, 2, 15, 1, '1254298248498498498464', 'HEFFERNAN CONNOR', 'N', 'N', '2017-05-01', '', 'N', 'N', 'N'),
(644, 2, 5, 1, '564645648252747462', 'ARGANDOLA GONZALO', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(645, 2, 5, 1, '515649498', 'SALAS SALINAS NICOLE ANDREA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(646, 2, 5, 1, '5886435487846', 'ANDRADA MARIA VICTORIA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(647, 2, 5, 1, '417288', 'VIDAL MARIA BELEN', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(648, 2, 5, 1, '8789711562232111', 'MARIÃ‘O ALICIA CRISTINA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(649, 2, 5, 1, '448747989', 'CRISCIONE ANDREA JUDITH', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(650, 2, 5, 1, '897897949', 'FERRARI STELLA MARIS', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(651, 2, 4, 1, '884849546', 'SCOTTI SILVANA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(652, 2, 5, 1, '8484849489', 'REZENDE MARCIA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(653, 2, 5, 1, '149', 'GARCIA JORGE LUIS', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(654, 2, 4, 1, '17517', 'SEPULVEDA CRISTIAN CASAMAYOR', 'N', 'N', '2017-06-08', '', 'N', 'N', 'N'),
(656, 2, 5, 1, '4889849', 'BERRETA MARIA SUSANA', 'N', 'N', '2017-06-07', '', 'N', 'N', 'N'),
(657, 2, 5, 1, '8848', 'ADAD ZULMA BEATRIZ', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(658, 2, 5, 1, '5848798', 'BESTANI YESICA BARBERIS', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(659, 2, 5, 1, '515151619', 'MOLINA CARLOS', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(660, 2, 5, 1, '1295796', 'OLIVO JORGE LUIS', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(661, 2, 5, 1, '4849179', 'NACHAJON RICARDO ELIAS', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(662, 2, 5, 1, '94070', 'SALDAÃ‘A MARIA INES', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(663, 2, 6, 1, '47175285', 'PALACIO ANA MARIA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(664, 2, 6, 1, '4494', 'PESOK MELO JUAN CARLOS', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(666, 2, 5, 1, '787607', 'BOYANO MATIAS', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(667, 2, 5, 1, '5498649', 'BARRIONUEVO PABLO', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(668, 2, 5, 1, '567698', 'LEDESMA JORGE SEBASTIAN', 'N', 'N', '2017-06-07', '', 'N', 'N', 'N'),
(669, 2, 5, 1, '77879849', 'SCHIRO YAMILA NORA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(670, 2, 73, 1, '154894', 'GRILLO MIGUEL ANGEL', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(671, 2, 73, 1, '7484984', 'LORENZO JOSE LUIS', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(672, 2, 4, 1, '5454849', 'MULLER RICARDO ADRIAN', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(673, 2, 5, 1, '4172863', 'QUINTEROS FERNANDO MIGUEL', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(674, 2, 5, 1, '85687', 'POGGI JOSE LUIS', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(675, 2, 5, 1, '44884', 'PORRECA MARTIN', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(676, 2, 5, 1, '727527', 'PORRECA MARTIN', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(677, 2, 5, 1, '7427638', 'FABIANI NORA GRACIELA ', 'N', 'N', '2017-06-01', '', 'N', 'N', ''),
(678, 2, 4, 1, '14894848', 'FERREYRA CORDOBA OSCAR ANIBAL', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(679, 2, 5, 1, '441154', 'CAMPOS CLAUDIA PATRICIA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(680, 2, 5, 1, '5487949', 'GONZALEZ MARCIA GISELE', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(681, 2, 5, 1, '454894', 'RECINELLO MARIA ESTELA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(682, 2, 5, 1, '7278686', 'MAGGI DANIEL ESTEBAN', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(683, 2, 5, 1, '8424278', 'MARIA IRENE MAC WILLIAM', 'n', 'n', '2017-06-01', '', 'n', 'n', 'n'),
(684, 2, 5, 1, '479879', 'DEPASQUALE PATRICIA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(685, 2, 5, 1, '117478478', 'PRIETO DAIANA SANDRA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(686, 2, 4, 1, '85727', 'PRIETO DAIANA SANDRA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(687, 2, 5, 1, '767575', 'HINOJOSA DARIO', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(688, 2, 4, 1, '57857', 'ROMANO MARIO', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(689, 2, 74, 1, '741741', 'ALOJAMIENTO COLGATE', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(690, 1, 3, 1, '675517', 'LOPEZ GISEL', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(691, 1, 3, 1, '4174172', 'SALAZAR FERNANDA X5', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(692, 2, 5, 1, '58271', 'GARRIDO YASNA X2', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(693, 2, 7, 1, '794749', 'BARONETTI / SUFIA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(696, 2, 76, 0, '8671879', 'BLOQUEO TURYSOL', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(697, 2, 5, 1, '875285', 'URIA IGNACIO FERNANDO', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(698, 2, 5, 1, '565484894', 'COBARRUBIA CLAUDIA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(699, 2, 5, 1, '55646486', 'VIDAL MARIA BELEN X4', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(700, 2, 19, 1, '578971*', 'CONTENTO MARCELO', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(717, 2, 78, 1, '5565545', 'SMAIS PATRICIA X3', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(718, 3, 79, 1, '547546', 'BUFULCO WALTER', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(719, 2, 4, 1, '57285282', 'MULLER RICARDO', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(720, 2, 6, 1, '572572157', 'BELTRAMINO MARIA LAURA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(721, 2, 6, 1, '4564894984', 'RECINELLO SILVIA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(723, 2, 6, 1, '572858', 'BARBIERI PABLO', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(724, 2, 6, 1, '556486879', 'GUYOT JUAN PABLO', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(725, 2, 6, 1, '5456489', 'GIUNTA MARCELO EDUARDO', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(726, 2, 5, 1, '175283', 'BARBERIS BESTANI YESICA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(727, 2, 5, 1, '5283572', 'AGUILAR LAURA ELIZABETH', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N');
INSERT INTO `clientes` (`id`, `tipocliente_id`, `tipoclientesub_id`, `pai_id`, `dni`, `nombre_completo`, `direccion`, `ciudad`, `fecha_nacimiento`, `codigo_postal`, `telf`, `correo`, `cod_pos`) VALUES
(728, 2, 5, 1, '874894849', 'BULLON MARIANA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(729, 2, 5, 1, '548491749', 'AZZOLLINI MIRTA ARACELLI', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(730, 2, 5, 1, '456449498', 'PIORNO PABLO EMILIANO', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(731, 2, 5, 1, '84984984949', 'TUDELA JOCSAN CUADRAS', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(732, 2, 5, 1, '7417417', 'NICOLOSI FABIAN', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(733, 2, 5, 1, '417417', 'AGUIAR ESTEBAN ALBERTO', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(734, 2, 5, 1, '721737', 'BADEN NOELIA VANINA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(735, 2, 5, 1, '28538', 'KIMURA ROBERTO', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(736, 2, 8, 1, '4879849', 'CHINELLATO MARCELA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(755, 2, 80, 1, '28826418', 'DO NASCIMENTO / RODRIGUEZ X2', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(756, 2, 4, 1, '7417', 'FEINSTEIN VICTORIA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(757, 1, 3, 1, '27678987', 'Silvina Beranek', 'LA RIOJA 987', 'Mendoza', '1980-02-23', '', '0261- 4567890', 'sil.beranek@gmail.com', '5500'),
(758, 2, 5, 1, '35662174', 'EMILIANO FILIPPINI', '20 de Junio 160', 'Mendoza', '1991-03-06', '', '2612191810', 'emilianofili91@hotmail.com', '5501'),
(759, 1, 3, 1, '18643687', 'JOSETOM', 'BUENOS AIRES', 'BUENOS AIRES', '2017-06-21', '', '04124616235', 'diazvjosetomas@gmail.com', '2101'),
(760, 2, 7, 1, '12156', 'RINGA MARIA BELEN X2', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(761, 2, 5, 1, '2131', 'VALLERO VILMA DEL VALLE', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(762, 2, 5, 1, '577527', 'MONASTEROLO SILVINA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(763, 2, 5, 1, '1564684', 'TOLEDO JARA RUBEN', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(764, 2, 5, 1, '422', 'LOLEZ MARIANO MARTIN', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(765, 2, 5, 1, '74168', 'CANDISANO CECILIA SAISI', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(767, 2, 6, 1, '757276', 'BELTRAMINO MARIA LAURA X1', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(768, 2, 5, 1, '6467578', 'WILLENBRINK RICARDO PINO ', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(769, 4, 56, 1, '56465446', 'SANTIANO', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(770, 2, 5, 1, '4964849', 'GUARDIA PAOLA ANDREA ', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(771, 2, 5, 1, '5272185', 'MARTINEZ ELIAS RAFAEL X2', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(772, 2, 5, 1, '272167', 'LUCHESSI YANINA IRIDE', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(773, 2, 5, 1, '417575', 'ALONSO JORGE JAVIER ', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(774, 2, 5, 1, '564816789', 'ALVAREZ MARCELO DANIEL X2', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(775, 2, 5, 1, '7897985', 'DIAZ LUCRECIA DANIELA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(776, 2, 5, 1, '75267', 'PASO MATIAS NICOLAS', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(777, 2, 5, 1, '5289267', 'STORELLO GABRIELA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(778, 2, 5, 1, '58689', 'JULL CRAIG', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(782, 3, 81, 1, '78278', 'ROJAS CARLOS', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(783, 2, 5, 1, '7894158', 'LAQUALE GABRIELA KARINA X2', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(784, 2, 4, 1, '24287', 'BERNARDI RICARDO ALBERTO', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(785, 2, 5, 1, '052277', 'CENTENO CAROLINA ', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(786, 2, 5, 1, '72676', 'CUESTAS NICOLAS ALFREDO', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(787, 2, 5, 1, '72687', 'BIRCZ HECTOR RUBEN', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(788, 2, 6, 1, '577217', 'BRASILI VALERIA ', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(789, 2, 47, 1, '5486464', 'DAVEL VERONICA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(790, 2, 6, 1, '546879', 'CHELI CARLA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(791, 2, 47, 1, '487497489', 'CHELI CARLA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(792, 2, 6, 1, '456727', 'SERRITELLA DANIELA ', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(793, 2, 6, 1, '563727', 'STAGNOLI MIGUEL', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(794, 1, 3, 1, '8468454656', 'DELGADO DAMIAN (NOCHE DE BODAS)', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(795, 2, 8, 1, '54869', 'ALU IGNACIO', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(796, 2, 5, 1, '5427527', 'CAJE CUEVAS CRISTINA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(797, 2, 6, 1, '4828', 'CAJE CUEVAS CRISTINA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(798, 1, 3, 1, '84979879', 'INSPECTOR AFA // CONMEBOL (CLUB GODOY CRUZ)', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(799, 1, 3, 1, '5878979', 'DR. ANTIDOPING / CONMEBOL (CLUB GODOY CRUZ)', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(800, 1, 3, 1, '8327', 'OFICIAL DE SEGURIDAD DE CONMEBOL', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(801, 1, 3, 1, '532587527', 'CONMEBOL X2 / ARBITROS (CLUB GODOY CRUZ)', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(802, 1, 3, 1, '8864', 'INSPECTOR CONMEBOL (CLUB GODOY CRUZ)', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(803, 1, 3, 1, '1949', 'DELEGADO CONMEBOL (CLUB GODOY CRUZ)', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(804, 2, 74, 1, '797867', 'GALLARDO VERONICA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(805, 2, 5, 1, '878789', 'LOZES MARIANO MARTIN', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(806, 3, 82, 1, '56494', 'PEREZ SEBASTIAN', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(807, 2, 6, 1, '579787', 'SAISI CANDISANO CECILIA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(808, 1, 3, 1, '8789779', 'DIAZ ROGELIO NORMANDO', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(811, 3, 83, 0, '487148', 'BLOQUEO CORREO X2', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(812, 4, 56, 1, '564982548', 'MECA RUBEN', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(813, 2, 5, 1, '0527527', 'QUARCHIONI ALEJANDRO ', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(814, 2, 5, 1, '620787', 'ARRATIA LUCIO', 'N', 'N', '2017-07-01', '', 'N', 'N', 'N'),
(815, 2, 5, 1, '149989', 'AMOROSO EMANUEL JOEL ', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(816, 2, 5, 1, '051527', 'FALCO NICOLAS', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(817, 2, 5, 1, '257256', 'RAGAGLIA STELLA MARIS', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(818, 2, 4, 1, '6792557', 'ROSSI DANILO GABRIEL ', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(819, 2, 5, 1, '23727', 'PAFUNDI HERNAN EDUARDO', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(820, 2, 5, 1, '367425197', 'LOPEZ LINARES JIMENA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(821, 2, 5, 1, '62786727', 'VERDENELLI VANINA CINTIA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(822, 2, 5, 1, '577275', 'PEREZ MONICA LILIANA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(823, 2, 5, 1, '549849', 'PEREZ MONICA LILIANA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(824, 2, 5, 1, '2425762', 'GOMEZ RAMON ALBERTO', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(825, 1, 3, 1, '527867', 'COLLADO VIRGINIA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(826, 2, 5, 1, '57275387', 'GRASSI MAURO', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(827, 2, 6, 1, '4278272', 'GRASSI MAURO', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(828, 2, 5, 1, '1572828', 'NUÃ‘EZ BARRIOS FAUSTO GASTON', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(829, 2, 5, 1, '52758', 'NUÃ‘EZ BARRIOS FACUNDO HERBEN', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(831, 2, 5, 1, '57271', 'LOPEZ CRISTIAN DAMIAN', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(832, 2, 4, 1, '57256', 'GASCON MARIA CONSTANZA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(833, 2, 4, 1, '5498487', 'VILLAR GRACIELA ANTONIA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(834, 2, 5, 1, '427537', 'TEIJO GABRIEL ADRIAN', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(835, 1, 3, 1, '48489489', 'GINI MONICA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(836, 2, 5, 1, '53837', 'MAYORAL MARCOS', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(837, 2, 5, 1, '27417471', 'AYUNTA DANIELA VERONICA', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(838, 2, 5, 1, '5275272', 'ARAUJO CARLOS', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(839, 2, 6, 1, '71717', 'ARAUJO CARLOS', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(840, 2, 5, 1, '7577747', 'PROCOPOWICH ALDANA SOL', 'N', 'N', '2017-06-01', '', 'N', 'N', 'N'),
(841, 1, 3, 1, '57276876', 'RUBIÃ‘AL MARTIN', 'N', 'N', '2017-07-01', '', 'N', 'N', 'N'),
(842, 2, 5, 1, '53863873', 'CELADA DANIEL ALBERTO', 'N', 'N', '2017-07-01', '', 'N', 'N', 'N'),
(843, 1, 3, 1, '1564696849', 'ABRAMOVICH CATALINA', 'N', 'N', '2017-07-01', '', 'N', 'N', 'N'),
(844, 2, 5, 1, '272745', 'AZZARO GRACIELA MARTA', 'N', 'N', '2017-07-01', '', 'N', 'N', 'N'),
(845, 2, 5, 1, '18752', 'LOBERTINI ANALIA JAZMIN', 'N', 'N', '2017-07-01', '', 'N', 'N', 'N'),
(846, 2, 5, 1, '5165176', 'FOLLONIER PATRICIO JOSE', 'N', 'N', '2017-07-01', '', 'N', 'N', 'N'),
(847, 2, 8, 1, '646758', 'TENEMBAUM HOWARD', 'N', 'N', '2017-07-01', '', 'N', 'N', 'N'),
(848, 2, 5, 1, '419451', 'DESBORDES PABLO MORANDE', 'N', 'N', '2017-07-01', '', 'N', 'N', 'N'),
(849, 1, 3, 1, '24633523', 'fernando cruz', 'akaka', 'akak', '2017-08-18', '', 'nana', 'nana', '522'),
(850, 2, 24, 1, '17623781', 'DONANTUENO DIEGO X3', 'N', 'N', '2017-08-01', '', 'N', 'N', 'N'),
(851, 2, 5, 1, '12352', 'VASCONCELLOS PABLO X2', 'N', 'N', '2017-08-01', '', 'N', 'N', 'N'),
(852, 2, 84, 1, '1231414', 'PERRONE GRACIELA X1', 'N', 'N', '2017-08-28', '', 'N', 'N', 'N'),
(853, 2, 5, 1, '8478274892', 'LUCCA ALISSON X1', 'N', 'N', '2017-08-28', '', 'N', 'N', 'N'),
(854, 2, 5, 1, '1283183801', 'ALMEIDA BRUNO X2', 'N', 'N', '2017-08-28', '', 'N', 'N', 'N'),
(855, 2, 6, 1, '44823490', 'KOHN ALBERTO X4', 'N', 'N', '2017-08-28', '', 'N', 'N', 'N'),
(856, 2, 19, 1, '189273817389', 'KOHN RICARDO X6', 'N', 'N', '2017-08-01', '', 'N', 'N', 'N'),
(857, 2, 5, 1, '123817893', 'SANHUEZA MARCELO X4', 'N', 'N', '2017-08-28', '', 'N', 'N', 'N'),
(858, 2, 5, 1, '189578274', 'COURTIS CAROLINA JUDITH X2', 'N', 'N', '2017-08-01', '', 'N', 'N', 'N'),
(859, 2, 6, 1, '892840294', 'SAMOKHVALOVA ANASTASIYA X2', 'N', 'N', '2017-08-01', '', 'N', 'N', 'N'),
(860, 2, 24, 1, '2183081', 'ALASTRA MARCOS X2', 'N', 'N', '2017-09-15', '', 'N', 'N', 'N'),
(861, 2, 5, 1, '53662', 'GUZMAN FLORENCIA X2', 'N', 'N', '2017-08-01', '', 'N', 'N', 'N'),
(862, 2, 20, 0, '872859084', 'GRUPO PERLATUR', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(863, 2, 20, 0, '5718390190', 'GRUPO SUZANA', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(864, 2, 5, 1, '895710401', 'VENEGAS MUÃ‘OZ CLAUDIA X1', 'N', 'N', '2017-08-28', '', 'N', 'N', 'N'),
(865, 2, 4, 1, '8193893', 'HERNANDEZ DUARTE ANDRES X4', 'N', 'N', '2017-08-02', '', 'N', 'N', 'N'),
(866, 2, 4, 1, '865982402', 'CAMPOS ZAVAN PAULA X4', 'N', 'N', '2017-09-16', '', 'N', 'N', 'N'),
(867, 2, 5, 1, '748101', 'AVENDANO CLAUDIO X3', 'N', 'N', '2017-08-28', '', 'N', 'N', 'N'),
(868, 2, 5, 1, '8758183810', 'MULLER ESTEBAN X3', 'N', 'N', '2017-08-28', '', 'N', 'N', 'N'),
(869, 2, 5, 1, '1IO83091841', 'MULLER ESTEBAN X4', 'N', 'N', '2017-09-16', '', 'N', 'N', 'N'),
(870, 3, 85, 1, '890381893', 'JEM SRL X2', 'N', 'N', '2017-08-28', '', 'N', 'N', 'N'),
(871, 2, 6, 1, '2840918841', 'ROSA SAMUEL MARCELO X2', 'N', 'N', '2017-08-28', '', 'N', 'N', 'N'),
(872, 2, 86, 0, '1028301', 'GRUPO IDEIA', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(873, 2, 87, 1, '859824891', 'PAOLO CARLA X6', 'N', 'N', '2017-08-21', '', 'N', 'N', 'N'),
(874, 2, 88, 0, '182388134', 'GRUPO UNI EXPLORER 1Â° INGRESO', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(875, 2, 20, 1, '92831831', 'ANDRADE / SILVA X2', 'N', 'N', '2017-08-01', '', 'N', 'N', 'N'),
(876, 3, 89, 1, '09812039', 'GRYNBERG EDGARDO X1', 'N', 'N', '2017-08-22', '', 'N', 'N', 'N'),
(877, 2, 20, 0, '1982318391', 'GRUPO VOYAGE', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(878, 2, 20, 0, '19823081', 'GRUPO PERLATUR 2Â° INGRESO', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(879, 2, 90, 1, '19231931', 'DOMINGUEZ BELEN X1', 'N', 'N', '2017-09-21', '', 'N', 'N', 'N'),
(880, 3, 37, 1, '182730138', 'SANCHEZ ROBERTO X1', 'N', 'N', '2017-08-28', '', 'N', 'N', 'N'),
(881, 3, 37, 1, '8980813', 'CASTILLO SUAREZ CARLOS X2', 'N', 'N', '2017-08-21', '', 'N', 'N', 'N'),
(882, 2, 20, 0, '1982301', 'GRUPO SANTA FE SET 1Â° ING.', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(883, 2, 20, 0, '12831073', 'GRUPO VIAJATUR', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(884, 2, 88, 0, '813891382', 'GRUPO UNI EXPLORER 2Â° INGRESO', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(885, 2, 20, 0, '5892838923', 'GRUPO EVIDENCE ', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(886, 2, 5, 1, '02310321', 'ORELLANA LILIANA ANGELICA X2', 'N', 'N', '2017-08-28', '', 'N', 'N', 'N'),
(887, 3, 91, 1, 'IO3O1829310', 'METALMECANICA X1', 'N', 'N', '2017-08-28', '', 'N', 'N', 'N'),
(888, 2, 71, 1, '12983911', 'GALVAO ROSA X2', 'N', 'N', '2017-08-28', '', 'N', 'N', 'N'),
(889, 2, 5, 1, '1928308', 'LAUB FRITZI X2', 'N', 'N', '2017-08-09', '', 'N', 'N', 'N'),
(890, 2, 4, 1, '89808091231', 'PRICERES OSVALDO CESAR X3', 'N', 'N', '2017-08-28', '', 'N', 'N', 'N'),
(891, 2, 5, 1, '123018301', 'MENDES FERNANDO DIEGO X3', 'N', 'N', '2017-08-28', '', 'N', 'N', 'N'),
(892, 2, 5, 1, '9820981031', 'LACANNA GUIDO X1', 'N', 'N', '2017-08-28', '', 'N', 'N', 'N'),
(893, 2, 92, 1, '812830183', 'COLANTUONO / FALACAR X2', 'N', 'N', '2017-08-28', '', 'N', 'N', 'N'),
(894, 2, 5, 1, '9812391', 'PERAFAN MARCELA X2', 'N', 'N', '2017-08-28', '', 'N', 'N', 'N'),
(895, 2, 5, 1, '7817293781', 'PERAFAN MARCELA X2', 'N', 'N', '2017-08-28', '', 'N', 'N', 'N'),
(896, 2, 5, 1, 'U1387478401', 'AGUILAR BENITO X4', 'N', 'N', '2017-08-08', '', 'N', 'N', 'N'),
(897, 2, 5, 1, '183201371', 'AGUILAR BENITO X4', 'N', 'N', '2017-08-22', '', 'N', 'N', 'N'),
(898, 2, 20, 0, '488172873199', 'GRUPO SANTA FE 2Â° INGRESO', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(899, 2, 5, 1, '787197328918273', 'AHUMADA MARCIA X4', 'N', 'N', '2017-08-28', '', 'N', 'N', 'N'),
(900, 2, 20, 0, '4789173891', 'GRUPO EVIDENCE 2Â° INGRESO', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(901, 2, 5, 1, '75873981', 'ZELARYAN ANDREA LOPEZ X2', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(902, 2, 5, 1, '7189237981', 'SESTO ANAHI SOLEDAD X4', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(903, 2, 5, 1, '128320', 'RIVAS CAROLINA X1', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(904, 2, 5, 1, '162783198', 'MUQUILLAZA FABIOLA X3', 'N', 'N', '2017-08-22', '', 'N', 'N', 'N'),
(905, 2, 5, 1, '1231', 'PASCALE SANTIAGO', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(906, 2, 5, 1, '6552342', 'NOVIELLI ANGELA X2', 'N', 'N', '2017-09-30', '', 'N', 'N', 'N'),
(907, 2, 6, 1, '1823018', 'TONASSO RODRIGO X2', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(908, 2, 76, 1, '123r13r231r', 'GRUPO ESTUDIANTES', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(909, 2, 76, 1, '89739818738701', 'GRUPO ESTUDIANTES', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(910, 2, 5, 0, '273018301', 'VENTURIN FABIAN X2', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(911, 3, 94, 1, '1723719873', 'WILHELM MARIO X2', 'N', 'N', '2017-08-22', '', 'N', 'N', 'N'),
(914, 3, 94, 1, '7878741874814', 'WILHELM MARIO X2', 'N', 'N', '2017-08-22', '', 'N', 'N', 'N'),
(915, 2, 95, 0, '9187381793', 'GRUPO ALVAN TURISMO', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(916, 2, 6, 1, '293813810', 'SAVOLDELLI LUIZ X2', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(917, 2, 6, 1, '231512', 'DASSI PABLO X3', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(918, 1, 3, 1, '2848138910', 'ZUCCH ISUSANA X2', 'N', 'N', '2017-08-28', '', 'N', 'N', 'N'),
(919, 2, 5, 1, '87203103210', 'AYUNTA DANIELA VERONICA X2', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(920, 3, 94, 1, '182381083', 'KROCHIK ROBERTO DAVID X2', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(921, 3, 94, 1, '718237108', 'DUBOIS FABIER X4', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(923, 2, 96, 1, '98123981', 'FIORA / BARGAS X2', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(924, 2, 5, 1, '748173017', 'FALLETTI CARLOS DANTE X2', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(925, 3, 94, 1, '09183093', 'BLOQUEO CONGRESO IADEF', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(926, 3, 94, 1, '123019309', 'MARSALA GLADYS X3', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(927, 3, 94, 1, 'U0138018301', 'KANTER SILVIA X2', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(928, 2, 5, 1, '98283819839183', 'NASCIMENTO RONDINELLI X2', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(929, 3, 94, 1, 'U13013808', 'BLANCHE REYES CRISTIAN X3', 'N', 'N', '2017-10-04', '', 'N', 'N', 'N'),
(930, 3, 94, 1, '3091830813', 'BATISTA / ARDITO X2', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(931, 3, 94, 1, '09819830183', 'CALCATERRA GABRIELA X2', 'N', 'N', '2017-08-22', '', 'N', 'N', 'N'),
(932, 2, 5, 1, '884984092', 'CANDIA NADIR EMANUEL X4', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(933, 2, 58, 1, '8492801840924', 'CANDIA NADIR EMANUEL X4', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(934, 2, 95, 0, '8301831', 'GRUPO ALVAN TURISMO 2Â° INGRESO', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(935, 3, 94, 0, '98230183', 'DI TULLIO OSVALDO X2', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(936, 3, 94, 1, '989789788', 'UNIVERSIDAD DE CONGRESO', 'N', 'N', '2017-10-05', '', 'N', 'N', 'N'),
(937, 3, 94, 1, '1030918301', 'LANCIANI BRUNO X2', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(938, 2, 97, 1, '831823018', 'CATA TURISMO', 'N', 'N', '2017-10-05', '', 'N', 'N', 'N'),
(939, 3, 98, 1, '8321938013', 'ACKERMAN MARIO X1', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(940, 3, 94, 1, '7474719889', 'CHRISTENSEN NATALIA X3', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(941, 2, 27, 1, '12831803', 'FREISE JUERGUEN X1', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(942, 2, 6, 1, '818923891', 'LIBRELATO ARCANGELO X2', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(943, 2, 6, 1, '1892738137', 'LIBRELATO ALOIR X2', 'N', 'N', '2017-10-06', '', 'N', 'N', 'N'),
(944, 2, 6, 1, '817238917830', 'SPRICIGO JOSE CARLOS X2', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(945, 2, 6, 1, '17831739781', 'TESMANN ENOR X2', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(946, 2, 6, 1, '9182301', 'SAVI FILHO ZEFERINO X2', 'N', 'N', '2017-08-22', '', 'N', 'N', 'N'),
(947, 2, 99, 0, '8913801', 'GRUPO BEATRIZ CABRERA', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(948, 2, 100, 1, '107230173', 'BLOQUEO ANDES TRAVEL X3', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(949, 2, 6, 1, '981801', 'MASANES GONZALEZ NICOLAS X2', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(950, 2, 5, 1, '129391', 'GUIMARAES CARLOS EDUARDO X2', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(951, 2, 27, 1, '1239189', 'PASTORS WOLFGANG X1', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(952, 2, 6, 1, '8131307', 'CHAVEZ MARCIA X2', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(953, 2, 6, 1, '89127891', 'CHAVES MARCIA X2', 'N', 'N', '2017-08-22', '', 'N', 'N', 'N'),
(954, 2, 20, 0, '18730131', 'GRUPO LEONAIR', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(955, 2, 20, 0, '281038018', 'GRUPO MARLISE', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(956, 2, 5, 1, '81380183', 'VELTRI CHRISTIAN RODRIGUEZ X4', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(957, 2, 4, 1, '108230', 'SILVA REZENDE DALILA MARCIA X2', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(958, 2, 5, 1, '123891389', 'STEIN MARCUS X2', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(959, 2, 20, 0, '9839031', 'GRUPO FELTRIN', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(960, 2, 5, 1, 'U3183018', 'MARSICO MABEL ADRIANA X2', 'N', 'N', '2017-08-29', '', 'N', 'N', 'N'),
(961, 2, 5, 1, '89137013', 'DE OLIVEIRA LENICE X2', 'N', 'N', '2017-08-01', '', 'N', 'N', 'N'),
(962, 3, 101, 0, '90820908', 'BLOQUEO NEVARES', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(963, 2, 20, 0, '93198391', 'GRUPO VDR PETRI', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(964, 2, 20, 0, '78917381879371', 'GRUPO NICCOTOUR', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(965, 2, 20, 0, '1873819739', 'GRUPO NICCOTUR', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(966, 2, 5, 1, '01203813', 'DANDREA LUCIANA ROCIO X4', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(967, 2, 5, 1, '39817389173', 'OSORIO ARIEL JAVIER X2', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(968, 2, 5, 1, '1238013', 'SARTO DANIEL CARLOS X1', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(969, 2, 5, 1, '189391839', 'MINORU MAEDA MARCELO X2', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(970, 2, 5, 1, '81730173', 'CESTARO ORLANDO X2', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(971, 2, 5, 1, '897138981730', 'MARANGAO CARLOS X2', 'N', 'N', '2017-10-15', '', 'N', 'N', 'N'),
(972, 2, 5, 1, '1I72310914', 'BUSCH PAULA DANILO X2', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(973, 2, 24, 1, '29313817', 'ALGAMIS HUSNI TAMARA YAEL X2', 'N', 'N', '2017-08-09', '', 'N', 'N', 'N'),
(974, 2, 4, 1, '471837189938', 'CARCAGNO LUIS EMILIO X4', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(975, 2, 5, 1, '819273173', 'LOPEZ MARICEL LUJAN X3', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(976, 2, 5, 1, '12301301', 'RODRIGUEZ RUBEN OSCAR X4', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(977, 2, 102, 1, '1923981', 'CASTRO MARIA / GRUTTULINI', 'N', 'N', '2017-08-15', '', 'N', 'N', 'N'),
(978, 1, 3, 1, '2831301', 'D\'ALESSIO GLADYS X2', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(979, 2, 5, 1, '192830183', 'MUÃ‘IZ THIAGO X2', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(980, 1, 3, 1, '2342342625', 'GARGIULO MARIA CRISTINA X4', 'N', 'N', '2017-08-24', '', 'N', 'N', 'N'),
(981, 2, 103, 1, '1982309831', 'LABRA ADRIANA X4', 'N', 'N', '2017-08-16', '', 'N', 'N', 'N'),
(982, 2, 5, 1, '231983791', 'RIBEIRO HUGO X4', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(983, 3, 104, 1, '19727381', 'FR. BICHERON X1', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(984, 3, 104, 1, '893283420', 'MUSTAPHA MEKKI X2', 'N', 'N', '2017-08-15', '', 'N', 'N', 'N'),
(985, 2, 5, 1, '18232198301', 'PERRAULT FLORENCE X4', 'N', 'N', '2017-08-23', '', 'N', 'N', 'N'),
(986, 2, 5, 1, '74897841', 'FERNANDEZ ATILIO ROBERTO X2', 'N', 'N', '2017-10-20', '', 'N', 'N', 'N'),
(987, 2, 5, 1, '013821983', 'RIOS MARIBEL LORENA X3', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(988, 2, 5, 1, '89172317', 'OLGUIN DIAZ MARIA VERONICA X2', 'N', 'N', '2017-08-24', '', 'N', 'N', 'N'),
(989, 3, 104, 1, '1830918301', 'DIDIER GUEVEL X1', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(990, 2, 4, 1, '0138918301', 'MULLER GARCIA CRISTIAN X2', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(991, 2, 5, 1, '1732891379', 'GOGGI VICTORIA X2', 'N', 'N', '2017-08-09', '', 'N', 'N', 'N'),
(992, 3, 104, 1, '18932781973', 'GOFFAUX GERALDINE X1', 'N', 'N', '2017-08-23', '', 'N', 'N', 'N'),
(993, 2, 5, 1, '0912203801', 'RAMIREZ MARIA FLORENCIA X1', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(994, 2, 5, 1, '1723891993', 'BASIC NARCISO X4', 'N', 'N', '2017-08-23', '', 'N', 'N', 'N'),
(995, 3, 104, 1, '09834204', 'BIBAL FEDERIC X6', 'N', 'N', '2017-10-22', '', 'N', 'N', 'N'),
(996, 3, 104, 1, '9018230180', 'EHRENFELD MICHAEL X2', 'N', 'N', '2017-08-23', '', 'N', 'N', 'N'),
(997, 3, 104, 1, '238482', 'PIGNARRE GENEVIERE X1', 'N', 'N', '2017-08-09', '', 'N', 'N', 'N'),
(998, 3, 104, 1, '0098800132', 'GARDNER DANIEL X1', 'N', 'N', '2017-08-02', '', 'N', 'N', 'N'),
(999, 3, 104, 1, '18328101', 'WESSNER PIERRE X1', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(1000, 3, 104, 1, '98213891', 'BRUN PHILIPPE X4', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(1001, 3, 104, 1, '81283183103', 'PIERRE PHILIPPE X2', 'N', 'N', '2017-08-08', '', 'N', 'N', 'N'),
(1002, 3, 104, 1, '2013901', 'LEDUC FABRICE X2', 'N', 'N', '2017-08-16', '', 'N', 'N', 'N'),
(1003, 2, 5, 1, '1823103801', 'BLANCO LEILA IVANA X2', 'N', 'N', '2017-08-01', '', 'N', 'N', 'N'),
(1004, 2, 5, 1, '8172381738917', 'NELLISSEN MARIA MARTA X3', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(1005, 2, 6, 1, '87317891', 'GHISANI VIVIANA X3', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(1007, 1, 3, 1, '189237013', 'SERRAVALLE LAURA X1', 'N', 'N', '2017-08-15', '', 'N', 'N', 'N'),
(1008, 1, 3, 1, '2981893', 'YANNUZZI HERNAN X1', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(1009, 2, 5, 1, '183703', 'VEGGIA LIDIA BEATRIZ X1', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(1010, 2, 5, 1, '129837103', 'HOURS ROMINA X2', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(1011, 2, 4, 1, '38180301', 'MATEI BIANCA NICOLETA X1', 'N', 'N', '2017-08-24', '', 'N', 'N', 'N'),
(1012, 2, 24, 1, '475183714', 'ROBLEDO ALBA ANDREA X2', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(1013, 1, 3, 1, '10938083', 'CAMARGO NATALIA X3', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(1014, 2, 4, 1, '3281031', 'MELLO PAULA X2', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(1015, 2, 5, 1, '8310981', 'ALEGRE JANTUS FEDERICO X3', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(1016, 2, 4, 1, '3810938013', 'VICENTE MENDOZA FERNANDA X4', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1017, 2, 4, 1, '9839081301', 'VICENTE CLAVE ALVARO X4', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(1018, 2, 5, 1, '823091831', 'ALKMIN ANA BEATRIZ X2', 'N', 'N', '2017-08-16', '', 'N', 'N', 'N'),
(1019, 2, 5, 1, '8301', 'SEURA SWANECK JORGE X4', 'N', 'N', '2017-08-23', '', 'N', 'N', 'N'),
(1020, 2, 4, 1, '198321983', 'VICENTE MENDOZA ISIDORA X4', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(1021, 2, 5, 1, '837758010', 'DE LA MAZA ROMAN BEATRIZ X3', 'N', 'N', '2017-08-24', '', 'N', 'N', 'N'),
(1022, 2, 4, 1, '183093', 'CARRIZO MARIA LUJAN X2', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(1023, 2, 105, 0, '19231083', 'BLOQUEO NITES X2', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(1024, 2, 4, 1, '1391837189', 'CARRIZO RAUL X3', 'N', 'N', '2017-08-30', '', 'N', 'N', 'N'),
(1025, 2, 4, 1, '83083018', 'DILEGO OSCAR X3', 'N', 'N', '2017-08-08', '', 'N', 'N', 'N'),
(1026, 2, 5, 1, '39871298371', 'MACHADO GUZMAN RAUL X2', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1027, 2, 20, 0, '819839189301', 'GRUPO SANTA FE 1Â° INGRESO', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(1028, 2, 5, 1, '98108', 'SACCO ADRIAN EZEQUIEL X4', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1029, 2, 6, 1, '19873289', 'GRANERO LUCCHETTI ALESSANDRO X2', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1030, 2, 6, 1, '381928301', 'ORTIZ MARCELA X3', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1031, 2, 6, 1, '87981731', 'OROZCO LILIANA BEATRIZ X2', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1032, 2, 19, 0, '19830138', 'BLOQUEO HONTRAVEL X5', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(1033, 2, 20, 0, '129830831', 'GRUPO SANTA FE 2Â° INGRESO', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(1034, 2, 5, 1, '198321', 'AMIGO EUGENIA INES X4', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1035, 2, 5, 1, '1293\'219831', 'ROMERO LILIANA X3', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1036, 2, 4, 1, '12983013', 'ROJAS ADORNO CARLOS X4', 'N', 'N', '2017-08-24', '', 'N', 'N', 'N'),
(1037, 2, 5, 1, '12389318', 'FIGUEREDO DE PONTES BARBARA X2', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1038, 2, 5, 1, '0992301\'31', 'UNDURRAGA MARIA CAROLINA X2', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1039, 4, 56, 1, '19238903', 'DIEGO ALEJANDRO X3', 'N', 'N', '2017-11-18', '', 'N', 'N', 'N'),
(1040, 2, 5, 1, '10981098', 'MARTINEZ MICAELA SOFIA X2', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1041, 1, 3, 1, '123183018', 'DIEGO ALEJANDRO X2', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1042, 2, 57, 0, '1283908', 'GRUPO CORDITUR X2', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(1043, 2, 57, 0, '1823098198301', 'GRUPO CORDITUR X3', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(1044, 2, 57, 0, '2313819\'', 'RAMIREZ / RAMIREZ', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(1045, 2, 5, 1, '2131414', 'CASSETTA LAURA SILVANA X4', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1048, 2, 19, 1, '7478898774', 'MARSUS X2', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1049, 1, 3, 1, '12031031', 'RABUÃ‘AL MARTIN X2', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1050, 2, 5, 1, '83918301', 'ALVAREZ DANIELA AGUSTINA X3', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1051, 2, 22, 1, '18370183', 'PRYOSZTEJN / IGLESIAS X2', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1052, 2, 22, 1, '2837103701', 'PRYOSZTEJN ISAAX X2', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1053, 2, 5, 1, '1283139813', 'ZANFROLIN MARILIA X2', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1054, 2, 5, 1, '12313141', 'RAIMUNDEZ SILVANA X3', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1055, 1, 3, 1, '23810173', 'SCANIO PAOLA X2', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1056, 2, 5, 1, '3183\'183', 'CACERES BRAVO JOSE', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1057, 2, 5, 1, '318303', 'SACOFF MARTIN EDGAR X3', 'N', 'N', '2017-08-01', '', 'N', 'N', 'N'),
(1058, 2, 5, 1, '12831831', 'RACITI MARCELO X4', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1059, 2, 5, 1, '23719831031', 'RACITI CARMELO ITALO X3', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1060, 1, 3, 1, '1823913801', 'MARSON VERONICA X2', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1061, 2, 15, 1, '897318391', 'VALLEJO GONZALO X4', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1062, 2, 6, 1, '173813701', 'BUCCOLO MARIA FIORELLA X2', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1063, 1, 3, 1, '9018301830', 'MARSON LIA X3', 'N', 'N', '2017-08-31', '', 'N', 'N', 'N'),
(1064, 2, 20, 0, '19839031', 'GRUPO SANTA EMILIA', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(1065, 2, 20, 0, '371837187391', 'GRUPO PROMOVE', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(1066, 2, 20, 0, '19319381', 'GRUPO EMPREENDETUR', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(1067, 2, 20, 0, '13981301', 'GRUPO FONTE CRISTALINA', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(1068, 2, 20, 0, '12938101', 'GRUPO FONTE CRISTALINA 2Â° INGRESO', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(1069, 2, 20, 0, '1983913', 'GRUPO EXPRESSO UNIAO', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(1071, 2, 20, 0, '123813801', 'GRUPO CANGURU', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(1072, 2, 20, 0, '82731031', 'GRUPO ROSETURISMO', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(1073, 2, 20, 0, '121414141', 'GRUPO SPAZZINI', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(1074, 2, 20, 0, 'N', 'GRUPO ELOS TRAVEL 1Â° INGRESO', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(1079, 2, 20, 0, '254131', 'GRUPO ELOS TRAVEL 2Â° INGRESO', 'N', '', '0000-00-00', '', 'N', 'N', 'N'),
(1080, 2, 5, 1, '7837183871893', 'NIETO DIEGO ELIAS', 'N', 'N', '2017-09-01', '', 'N', 'N', 'N'),
(1081, 2, 6, 1, '8232183193', 'CRESPO LUIS ALBERTO X2', 'N', 'N', '2017-09-07', '', 'N', 'N', 'N'),
(1082, 2, 106, 1, '204920\'42', 'GOMEZ EXEQUIEL LUCAS X2', 'N', 'N', '2017-09-12', '', 'N', 'N', 'N'),
(1083, 3, 107, 1, '90138918301', 'GALLO ALEJANDRA', 'N', 'N', '2017-09-14', '', 'N', 'N', 'N'),
(1084, 2, 5, 1, '1293893', 'NARBONE MABEL X3', 'N', 'N', '2017-09-06', '', 'N', 'N', 'N'),
(1085, 4, 56, 1, '1280381031', 'CIOFFI PAULO X1', 'N', 'N', '2017-09-16', '', 'N', 'N', 'N'),
(1086, 3, 16, 1, '1273817391', 'PEYROUS', 'N', 'N', '2017-09-18', '', 'N', 'N', 'N'),
(1087, 3, 16, 1, 'i9183013', 'MARZIALETTI', 'N', 'N', '2017-09-11', '', 'N', 'N', 'N'),
(1088, 3, 85, 1, '23524852', 'SANTA CRUZ ADRIAN', 'CORRIENTES 2470', 'CABA', '1974-01-16', '', '49515508', 'MSALINASEHIJOS@YAHOO.COM.AR', '?'),
(1089, 4, 0, 1, '102u39031', 'CARLETI NORMA', 'N', 'N', '2017-09-18', '', 'N', 'N', 'N'),
(1090, 4, 56, 1, '789273817391', 'CARLETI NORMA', 'N', 'N', '2017-09-18', '', 'N', 'N', 'N'),
(1091, 3, 108, 1, '1U27398194714', 'WIDER GONZALO', 'N', 'N', '2017-09-18', '', 'N', 'N', 'N'),
(1092, 3, 42, 1, '78917389173', 'SERNA VERONICA', 'N', 'N', '2017-09-12', '', 'N', 'N', 'N'),
(1098, 3, 42, 1, '87381738917389', 'SERNA VERONICA', 'N', 'N', '2017-09-12', '', 'N', 'N', 'N'),
(1099, 3, 109, 1, '891730481083', 'LATIN AMERICAN WINGS', 'N', 'N', '2017-09-18', '', 'N', 'N', 'N'),
(1100, 3, 85, 1, 'OI48098301', 'ANDRES KRISAK', 'N', 'N', '2017-09-18', '', 'N', 'N', 'N'),
(1101, 2, 5, 1, '283193801', 'JUAREZ EDUARDO LUIS X1', 'N', 'N', '2017-09-11', '', 'N', 'N', 'N'),
(1102, 3, 85, 1, '08319831', 'VITA DIEGO X1', 'N', 'N', '2017-09-18', '', 'N', 'N', 'N');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compraabonas`
--

CREATE TABLE `compraabonas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `compra_id` int(11) NOT NULL,
  `compratipopago_id` int(11) NOT NULL,
  `num_pago` varchar(100) NOT NULL,
  `fecha_pago` date NOT NULL DEFAULT '0000-00-00',
  `num_cheque` varchar(100) NOT NULL DEFAULT '0',
  `num_transferencia` varchar(100) NOT NULL DEFAULT '0',
  `fecha_cobro_cheque` date DEFAULT NULL,
  `descripcion` text NOT NULL,
  `total` float(26,2) NOT NULL,
  `cancelado` float(26,2) NOT NULL,
  `deuda` float(26,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `compraabonas`
--

INSERT INTO `compraabonas` (`id`, `compra_id`, `compratipopago_id`, `num_pago`, `fecha_pago`, `num_cheque`, `num_transferencia`, `fecha_cobro_cheque`, `descripcion`, `total`, `cancelado`, `deuda`, `created`, `modified`) VALUES
(1, 3, 1, '1', '2016-11-21', '', '', NULL, '', 147.40, 100.00, 47.40, '2016-11-21 15:21:31', '2016-11-21 15:21:31'),
(3, 3, 1, '2', '2016-11-21', '', '', NULL, '', 147.40, 10.00, 37.40, '2016-11-21 15:36:05', '2016-11-21 15:36:05'),
(4, 3, 1, '3', '2016-11-21', '', '', NULL, '', 147.40, 37.40, 0.00, '2016-11-21 15:37:28', '2016-11-21 15:37:28'),
(5, 6, 1, '1', '2016-11-21', '', '', NULL, '', 1474.00, 100.00, 1374.00, '2016-11-21 15:55:31', '2016-11-21 15:55:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compraproductos`
--

CREATE TABLE `compraproductos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `compra_id` int(11) NOT NULL,
  `proproducto_id` int(11) NOT NULL,
  `cantidad` float(26,2) NOT NULL,
  `precio` float(26,2) NOT NULL,
  `total` float(26,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `compraproductos`
--

INSERT INTO `compraproductos` (`id`, `compra_id`, `proproducto_id`, `cantidad`, `precio`, `total`, `created`, `modified`) VALUES
(1, 2, 1, 1.00, 147.40, 147.40, '2016-11-10 20:05:08', '2016-11-10 20:05:08'),
(2, 2, 1, 2.00, 147.40, 294.80, '2016-11-10 23:00:12', '2016-11-10 23:00:12'),
(4, 3, 1, 1.00, 147.40, 147.40, '2016-11-18 16:52:38', '2016-11-18 16:52:38'),
(6, 4, 1, 10.00, 147.40, 1474.00, '2016-11-18 16:56:45', '2016-11-18 16:56:45'),
(7, 6, 1, 10.00, 147.40, 1474.00, '2016-11-18 16:59:13', '2016-11-18 16:59:13'),
(8, 8, 1, 1.00, 147.40, 147.40, '2016-11-18 17:01:46', '2016-11-18 17:01:46'),
(9, 9, 1, 1.00, 147.40, 147.40, '2016-12-16 23:22:12', '2016-12-16 23:22:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `proveedore_id` int(11) NOT NULL,
  `tipocompra_id` int(11) NOT NULL,
  `ano_compra` varchar(10) NOT NULL,
  `num_compra` varchar(100) NOT NULL,
  `fecha_compra` date NOT NULL,
  `fecha_vencimiento` date NOT NULL,
  `num_factura` varchar(100) NOT NULL,
  `observaciones` text NOT NULL,
  `total` float(26,2) NOT NULL,
  `pagado` float(26,2) NOT NULL DEFAULT '0.00',
  `deuda` float(26,2) NOT NULL DEFAULT '0.00',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compratipopagos`
--

CREATE TABLE `compratipopagos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `denominacion` varchar(200) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `compratipopagos`
--

INSERT INTO `compratipopagos` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'Efectivo', '2016-11-19 22:42:35', '2016-11-19 22:42:35'),
(2, 'Transferencia', '2016-11-19 22:42:44', '2016-11-19 22:42:44'),
(3, 'Cheque', '2016-11-19 22:42:57', '2016-11-19 22:42:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `conftipopagoreservas`
--

CREATE TABLE `conftipopagoreservas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `denominacion` varchar(300) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `conftipopagoreservas`
--

INSERT INTO `conftipopagoreservas` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'Tarjeta de Credito', '2016-11-01 14:54:45', '2016-11-01 14:54:45'),
(2, 'SeÃ±a', '2016-11-01 14:54:56', '2016-11-01 14:54:56'),
(3, 'Cuenta Corriente', '2016-11-01 14:55:05', '2016-11-01 14:55:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `conftipopagos`
--

CREATE TABLE `conftipopagos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `denominacion` varchar(300) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `conftipopagos`
--

INSERT INTO `conftipopagos` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'Contado', '2016-11-01 14:54:03', '2016-11-01 14:54:03'),
(2, 'Credito', '2016-11-01 14:54:13', '2016-11-01 14:54:13'),
(3, 'Cheque', '2016-11-01 14:54:24', '2016-11-01 14:54:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consumoproductos`
--

CREATE TABLE `consumoproductos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `consumo_id` int(11) NOT NULL,
  `proproducto_id` int(11) NOT NULL,
  `cantidad` float(26,2) NOT NULL,
  `precio` float(26,2) NOT NULL,
  `total` float(26,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `consumoproductos`
--

INSERT INTO `consumoproductos` (`id`, `consumo_id`, `proproducto_id`, `cantidad`, `precio`, `total`, `created`, `modified`) VALUES
(7, 2, 1, 2.00, 147.40, 294.80, '2016-11-11 09:19:03', '2016-11-11 09:19:03'),
(8, 2, 1, 2.00, 147.40, 294.80, '2016-11-11 09:19:03', '2016-11-11 09:19:03'),
(9, 3, 1, 1.00, 147.40, 147.40, '2016-11-18 23:02:55', '2016-11-18 23:02:55'),
(10, 4, 1, 1.00, 147.40, 147.40, '2016-12-07 11:13:17', '2016-12-07 11:13:17'),
(12, 5, 2, 2.00, 14.52, 29.04, '2017-03-06 14:27:23', '2017-03-06 14:27:23'),
(13, 5, 2, 5.00, 14.52, 72.60, '2017-03-06 14:27:23', '2017-03-06 14:27:23'),
(14, 6, 2, 1.00, 14.52, 14.52, '2017-04-25 13:51:50', '2017-04-25 13:51:50'),
(15, 7, 2, 2.00, 14.52, 29.04, '2017-04-25 20:23:34', '2017-04-25 20:23:34'),
(16, 8, 2, 1.00, 14.52, 14.52, '2017-05-01 21:09:39', '2017-05-01 21:09:39'),
(17, 9, 2, 1.00, 14.52, 14.52, '2017-05-23 12:02:42', '2017-05-23 12:02:42'),
(18, 9, 3, 2.00, 32.16, 64.32, '2017-05-23 12:02:42', '2017-05-23 12:02:42'),
(19, 10, 2, 2.00, 14.52, 29.04, '2017-05-30 19:23:17', '2017-05-30 19:23:17'),
(20, 1, 2, 1.00, 14.52, 14.52, '2017-06-24 19:11:09', '2017-06-24 19:11:09'),
(21, 2, 2, 1.00, 14.52, 14.52, '2017-09-13 17:41:19', '2017-09-13 17:41:19'),
(22, 2, 3, 1.00, 32.16, 32.16, '2017-09-13 17:41:19', '2017-09-13 17:41:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consumos`
--

CREATE TABLE `consumos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipohabitacione_id` int(11) NOT NULL,
  `habitacione_id` int(11) NOT NULL,
  `reserindividuale_id` int(11) NOT NULL,
  `ano_consumo` varchar(10) NOT NULL,
  `num_consumo` varchar(100) NOT NULL,
  `fecha_consumo` date NOT NULL,
  `observaciones` text NOT NULL,
  `total` float(26,2) NOT NULL,
  `pagado` float(26,2) NOT NULL DEFAULT '0.00',
  `deuda` float(26,2) NOT NULL DEFAULT '0.00',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `consumos`
--

INSERT INTO `consumos` (`id`, `tipohabitacione_id`, `habitacione_id`, `reserindividuale_id`, `ano_consumo`, `num_consumo`, `fecha_consumo`, `observaciones`, `total`, `pagado`, `deuda`, `created`, `modified`) VALUES
(1, 2, 35, 4, '2017', '1', '2017-06-24', '', 14.52, 0.00, 0.00, '2017-06-24 19:11:09', '2017-06-24 19:11:09'),
(2, 3, 34, 188, '2017', '1', '2017-09-13', '', 46.68, 0.00, 0.00, '2017-09-13 17:41:19', '2017-09-13 17:41:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ctacorrientes`
--

CREATE TABLE `ctacorrientes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipocliente_id` int(11) NOT NULL,
  `tipoclientesub_id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `descripcion` varchar(300) NOT NULL,
  `limitecredito` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ctacorrientes`
--

INSERT INTO `ctacorrientes` (`id`, `tipocliente_id`, `tipoclientesub_id`, `cliente_id`, `descripcion`, `limitecredito`, `created`, `modified`) VALUES
(4, 2, 4, 33, 'nada', '10000', '2017-04-25 20:32:12', '2017-04-25 20:32:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ctastatus`
--

CREATE TABLE `ctastatus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `denominacion` varchar(300) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ctastatus`
--

INSERT INTO `ctastatus` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'Sin Procesar', '2016-11-01 14:53:33', '2016-11-01 14:53:33'),
(2, 'Prosesada', '2016-11-01 14:53:43', '2016-11-01 14:53:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE `empresas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cuit` varchar(300) NOT NULL,
  `razon_social` varchar(300) NOT NULL,
  `email` varchar(200) NOT NULL,
  `telefono` varchar(100) NOT NULL,
  `website` varchar(200) NOT NULL,
  `provincia` varchar(200) NOT NULL,
  `direccion` varchar(300) NOT NULL,
  `carpeta_imagen` varchar(45) NOT NULL,
  `nombre_imagen` varchar(45) NOT NULL,
  `tipo_imagen` varchar(45) NOT NULL,
  `ruta_imagen` varchar(45) NOT NULL,
  `correo_smtp` varchar(300) NOT NULL DEFAULT '0',
  `clave_smtp` varchar(300) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresas`
--

INSERT INTO `empresas` (`id`, `cuit`, `razon_social`, `email`, `telefono`, `website`, `provincia`, `direccion`, `carpeta_imagen`, `nombre_imagen`, `tipo_imagen`, `ruta_imagen`, `correo_smtp`, `clave_smtp`, `created`, `modified`) VALUES
(2, '30712247432', 'HOTELES DEL SUR SA', 'sistema@grupocioffi.com.ar', '4253433-4250333', 'www.premiumtowersuites.com', 'MENDOZA', 'Argentina', 'upload', 'a_25042017_124637.png', 'image/png', 'upload/a_25042017_124637.png', 'reservas@premiumtowersuites.com', '*luzalba*', '2016-10-20 07:53:57', '2017-04-25 12:46:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturadetalles`
--

CREATE TABLE `facturadetalles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `factura_id` int(11) NOT NULL,
  `facturatipoproducto_id` int(11) NOT NULL,
  `total` float(26,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturapagos`
--

CREATE TABLE `facturapagos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `factura_id` int(11) NOT NULL,
  `facturatipopago_id` int(11) NOT NULL,
  `fecha_pago` date NOT NULL,
  `numero_pago` varchar(100) NOT NULL,
  `pago` float(26,2) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `facturapagos`
--

INSERT INTO `facturapagos` (`id`, `factura_id`, `facturatipopago_id`, `fecha_pago`, `numero_pago`, `pago`, `user_id`, `created`, `modified`) VALUES
(1, 1, 4, '2016-12-08', '1', 100.00, 0, '2016-12-08 15:23:43', '2016-12-08 15:23:43'),
(2, 1, 4, '2016-12-08', '1', 100.00, 0, '2016-12-08 15:24:06', '2016-12-08 15:24:06'),
(3, 1, 4, '2016-12-08', '1', 100.00, 0, '2016-12-08 15:24:23', '2016-12-08 15:24:23'),
(4, 2, 4, '2016-12-08', '1', 100.00, 0, '2016-12-08 15:25:29', '2016-12-08 15:25:29'),
(5, 5, 1, '2017-05-01', '1', 4260.52, 1, '2017-05-01 21:12:12', '2017-05-01 21:12:12'),
(6, 3, 4, '2017-05-23', '12', 2500.00, 1, '2017-05-23 11:38:58', '2017-05-23 11:38:58'),
(7, 6, 4, '2017-05-23', '456', 1671.06, 1, '2017-05-23 12:09:20', '2017-05-23 12:09:20'),
(8, 7, 4, '2017-05-30', '88469', 231.34, 1, '2017-05-30 19:29:01', '2017-05-30 19:29:01'),
(9, 8, 4, '2017-10-31', '1', 1698.40, 1, '2017-10-31 16:21:40', '2017-10-31 16:21:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas`
--

CREATE TABLE `facturas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `numero` varchar(100) NOT NULL,
  `fecha` date NOT NULL,
  `tipocliente_id` int(11) NOT NULL,
  `tipoclientesub_id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `concepto` text NOT NULL,
  `total` float(26,2) NOT NULL,
  `pagado` float(26,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `reserindividuale_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `facturas`
--

INSERT INTO `facturas` (`id`, `numero`, `fecha`, `tipocliente_id`, `tipoclientesub_id`, `cliente_id`, `concepto`, `total`, `pagado`, `created`, `modified`, `reserindividuale_id`) VALUES
(3, '1001', '2017-03-06', 1, 3, 4, 'prueba. ', 101.64, 2500.00, '2017-03-06 14:29:50', '2017-05-23 11:38:58', 0),
(4, '435', '2017-03-14', 2, 4, 33, 'NADA', 0.00, 0.00, '2017-03-13 16:09:43', '2017-03-13 16:09:43', 0),
(5, '12', '2017-05-01', 1, 3, 4, 'Salida', 4260.52, 4260.52, '2017-05-01 21:11:33', '2017-05-01 21:12:12', 456),
(6, '546', '2017-05-23', 1, 3, 612, '1671.09 ES EL TOTAL', 78.84, 1671.06, '2017-05-23 12:08:19', '2017-05-23 12:09:20', 0),
(7, '659', '2017-05-30', 1, 3, 612, 'ALOJAMIENTO HOTEL ', 231.34, 231.34, '2017-05-30 19:28:09', '2017-05-30 19:29:01', 0),
(8, '101', '2017-10-31', 1, 3, 612, 'Sin Cambios', 1698.40, 1698.40, '2017-10-31 16:20:28', '2017-10-31 16:21:41', 385),
(9, '10803', '2017-11-10', 1, 3, 4, 'asf', 5000.00, 5000.00, '2017-11-10 11:09:33', '2017-11-10 11:09:33', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturatipopagos`
--

CREATE TABLE `facturatipopagos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `denominacion` varchar(300) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `facturatipopagos`
--

INSERT INTO `facturatipopagos` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'Cuenta Corriente', '2016-12-08 15:00:21', '2016-12-08 15:00:21'),
(2, 'Tarjeta Credito', '2016-12-08 15:00:37', '2016-12-08 15:00:37'),
(3, 'Tarjeta Debito', '2016-12-08 15:00:45', '2016-12-08 15:00:45'),
(4, 'Efectivo', '2016-12-08 15:00:54', '2016-12-08 15:00:54'),
(5, 'Transferencia', '2016-12-08 15:01:03', '2016-12-08 15:01:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturatipoproductos`
--

CREATE TABLE `facturatipoproductos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `denominacion` varchar(200) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habistatus`
--

CREATE TABLE `habistatus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `denominacion` varchar(300) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `habistatus`
--

INSERT INTO `habistatus` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'LIBRE', '2016-12-20 07:43:47', '2016-12-20 07:43:47'),
(4, 'OCUPADA', '2016-12-20 07:43:41', '2016-12-20 07:43:41'),
(6, 'MANTENIMIENTO', '2016-12-20 07:43:55', '2016-12-20 07:43:55'),
(7, 'LIMPIEZA', '2016-12-20 07:44:03', '2016-12-20 07:44:03'),
(8, 'BLOQUEADA', '2017-06-20 00:23:37', '2017-06-20 00:23:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habitaciones`
--

CREATE TABLE `habitaciones` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipohabitacione_id` int(11) NOT NULL,
  `numhabitacion` int(11) NOT NULL,
  `habistatu_id` int(11) NOT NULL DEFAULT '1',
  `desde` date DEFAULT NULL,
  `hasta` date DEFAULT NULL,
  `descripcion` varchar(300) NOT NULL,
  `capacidad` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `habitaciones`
--

INSERT INTO `habitaciones` (`id`, `tipohabitacione_id`, `numhabitacion`, `habistatu_id`, `desde`, `hasta`, `descripcion`, `capacidad`, `created`, `modified`) VALUES
(5, 6, 301, 1, NULL, NULL, 'nada', 2, '2016-12-20 08:01:18', '2016-12-20 08:01:18'),
(6, 6, 302, 1, NULL, NULL, 'nada', 2, '2016-12-20 08:01:37', '2017-06-21 21:29:07'),
(7, 6, 401, 1, NULL, NULL, 'nada', 2, '2016-12-20 08:03:12', '2017-04-25 19:38:48'),
(8, 6, 402, 1, NULL, NULL, 'nada', 4, '2016-12-20 08:05:40', '2016-12-20 08:05:40'),
(9, 6, 501, 1, NULL, NULL, 'nada', 4, '2016-12-20 08:05:55', '2016-12-20 08:05:55'),
(10, 6, 502, 2, NULL, NULL, 'nada', 4, '2016-12-20 08:06:09', '2017-09-18 16:57:53'),
(11, 6, 601, 1, NULL, NULL, 'nada', 4, '2016-12-20 08:06:27', '2016-12-20 08:06:27'),
(13, 6, 701, 1, NULL, NULL, 'nada', 4, '2016-12-20 08:06:54', '2016-12-20 08:06:54'),
(14, 6, 702, 2, NULL, NULL, 'nada', 4, '2016-12-20 08:07:08', '2017-09-13 17:31:25'),
(15, 7, 802, 1, NULL, NULL, 'nada', 4, '2016-12-20 08:07:24', '2016-12-20 08:07:24'),
(16, 7, 901, 2, NULL, NULL, 'nada', 4, '2016-12-20 08:07:37', '2017-09-18 19:21:12'),
(17, 7, 1001, 1, NULL, NULL, 'nada', 4, '2016-12-20 08:07:48', '2016-12-20 08:07:48'),
(18, 7, 1002, 1, NULL, NULL, 'nada', 4, '2016-12-20 08:07:59', '2016-12-20 08:07:59'),
(20, 7, 1102, 2, NULL, NULL, 'nada', 4, '2016-12-20 08:08:27', '2017-09-18 16:43:21'),
(21, 7, 1201, 2, NULL, NULL, 'nada', 4, '2016-12-20 08:09:13', '2017-06-25 02:16:13'),
(22, 7, 1301, 2, NULL, NULL, 'nada', 4, '2016-12-20 08:09:41', '2017-09-18 16:15:22'),
(23, 3, 307, 1, NULL, NULL, 'nada', 2, '2016-12-20 08:10:01', '2017-11-10 11:08:39'),
(24, 3, 308, 1, NULL, NULL, 'nada', 2, '2016-12-20 08:10:14', '2016-12-20 08:10:14'),
(25, 3, 407, 1, NULL, NULL, 'nada', 2, '2016-12-20 08:10:24', '2016-12-20 08:10:24'),
(26, 3, 408, 1, NULL, NULL, 'nada', 2, '2016-12-20 08:10:44', '2016-12-20 08:10:44'),
(27, 3, 507, 1, NULL, NULL, 'nada', 2, '2016-12-20 08:11:00', '2016-12-20 08:11:00'),
(29, 3, 607, 2, NULL, NULL, 'nada', 2, '2016-12-20 08:11:28', '2017-09-18 19:18:26'),
(30, 3, 608, 2, NULL, NULL, 'nada', 2, '2016-12-20 08:11:37', '2017-09-18 16:17:14'),
(31, 3, 707, 2, NULL, NULL, 'nada', 2, '2016-12-20 08:11:56', '2017-09-12 15:17:11'),
(34, 3, 808, 2, NULL, NULL, 'nada', 2, '2016-12-20 08:12:29', '2017-09-13 17:36:01'),
(36, 2, 1007, 8, '2017-10-01', '2017-12-31', 'nada', 2, '2016-12-20 08:15:22', '2017-09-12 15:56:02'),
(38, 2, 1107, 8, '2017-09-12', '2018-12-31', 'nada', 2, '2016-12-20 08:16:27', '2017-09-12 15:57:59'),
(39, 2, 1108, 2, NULL, NULL, 'nada', 2, '2016-12-20 08:16:35', '2017-10-31 17:28:15'),
(40, 2, 1307, 1, NULL, NULL, 'nada', 2, '2016-12-20 08:16:44', '2016-12-20 08:16:44'),
(41, 2, 1308, 2, NULL, NULL, 'nada', 2, '2016-12-20 08:16:53', '2017-09-18 18:11:51'),
(43, 4, 304, 1, NULL, NULL, 'nada', 2, '2016-12-20 08:17:25', '2016-12-20 08:19:09'),
(44, 4, 305, 8, '2017-09-12', '2018-12-31', 'nada', 2, '2016-12-20 08:17:41', '2017-09-12 15:40:12'),
(45, 4, 306, 1, NULL, NULL, 'nada', 2, '2016-12-20 08:18:03', '2016-12-20 08:18:03'),
(47, 4, 403, 2, NULL, NULL, 'NADA', 2, '2016-12-20 08:19:48', '2017-04-25 20:14:42'),
(48, 4, 404, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:20:06', '2016-12-20 08:20:06'),
(49, 4, 405, 2, NULL, NULL, 'NADA', 2, '2016-12-20 08:21:25', '2017-09-18 19:19:18'),
(50, 4, 406, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:21:41', '2016-12-20 08:21:41'),
(52, 4, 410, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:22:05', '2016-12-20 08:22:05'),
(53, 4, 503, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:22:21', '2016-12-20 08:22:21'),
(54, 4, 504, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:22:31', '2016-12-20 08:22:31'),
(55, 4, 505, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:22:41', '2016-12-20 08:22:41'),
(56, 4, 506, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:22:56', '2017-04-25 13:43:40'),
(57, 4, 509, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:23:56', '2016-12-20 08:23:56'),
(59, 4, 603, 8, '2017-09-12', '2018-03-31', 'NADA', 2, '2016-12-20 08:24:36', '2017-09-12 15:48:07'),
(60, 4, 604, 2, NULL, NULL, 'NADA', 2, '2016-12-20 08:24:47', '2017-09-18 19:23:00'),
(61, 4, 605, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:24:56', '2016-12-20 08:24:56'),
(62, 4, 606, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:25:08', '2016-12-20 08:25:08'),
(63, 4, 610, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:25:41', '2016-12-20 08:25:41'),
(64, 4, 609, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:27:55', '2016-12-20 08:27:55'),
(65, 4, 703, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:28:12', '2016-12-20 08:28:12'),
(66, 4, 704, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:28:22', '2016-12-20 08:28:22'),
(69, 4, 709, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:28:57', '2016-12-20 08:28:57'),
(70, 4, 710, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:29:08', '2016-12-20 08:29:08'),
(72, 4, 804, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:29:38', '2016-12-20 08:29:38'),
(73, 4, 805, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:30:08', '2016-12-20 08:30:08'),
(74, 4, 806, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:30:19', '2016-12-20 08:30:19'),
(76, 4, 809, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:30:40', '2016-12-20 08:30:50'),
(77, 5, 903, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:32:02', '2016-12-20 08:32:02'),
(78, 5, 904, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:32:16', '2016-12-20 08:32:16'),
(80, 5, 906, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:32:45', '2016-12-20 08:32:45'),
(81, 5, 909, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:33:04', '2016-12-20 08:33:04'),
(82, 5, 910, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:33:15', '2016-12-20 08:33:15'),
(83, 5, 1003, 8, '2017-09-12', '2018-12-31', 'NADA', 2, '2016-12-20 08:33:29', '2017-09-12 15:54:31'),
(85, 5, 1005, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:33:56', '2016-12-20 08:33:56'),
(86, 5, 1006, 2, NULL, NULL, 'NADA', 2, '2016-12-20 08:34:05', '2017-09-18 17:20:23'),
(87, 5, 1009, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:34:15', '2016-12-20 08:34:15'),
(89, 5, 1103, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:34:49', '2016-12-20 08:34:49'),
(91, 5, 1105, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:35:06', '2016-12-20 08:35:06'),
(92, 5, 1106, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:35:16', '2016-12-20 08:35:16'),
(93, 5, 1109, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:35:26', '2016-12-20 08:35:26'),
(94, 5, 1110, 8, '2017-09-12', '2017-12-31', 'NADA', 2, '2016-12-20 08:35:37', '2017-09-12 16:00:08'),
(95, 5, 1203, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:35:56', '2016-12-20 08:35:56'),
(96, 5, 1204, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:36:04', '2016-12-20 08:36:04'),
(97, 5, 1303, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:36:29', '2016-12-20 08:36:29'),
(98, 5, 1304, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:37:08', '2016-12-20 08:37:19'),
(99, 5, 1305, 8, '2017-09-12', '2017-12-31', 'NADA', 2, '2016-12-20 08:37:30', '2017-09-12 15:41:39'),
(100, 5, 1306, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:37:42', '2016-12-20 08:37:42'),
(101, 5, 1309, 1, NULL, NULL, 'NADA', 2, '2016-12-20 08:37:53', '2016-12-20 08:37:53'),
(102, 8, 1310, 2, NULL, NULL, 'SALÃ“N CIOFFI', 150, '2017-09-18 19:34:52', '2017-09-18 19:39:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `impresiones`
--

CREATE TABLE `impresiones` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipoimpresora_id` int(11) NOT NULL,
  `puerto` varchar(40) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `impresiones`
--

INSERT INTO `impresiones` (`id`, `tipoimpresora_id`, `puerto`, `created`, `modified`) VALUES
(1, 1, 'COM1', '2016-10-20 23:48:17', '2016-10-20 23:48:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulos`
--

CREATE TABLE `modulos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `denominacion` varchar(300) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `modulos`
--

INSERT INTO `modulos` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'Clientes', '2016-12-20 19:46:26', '2016-12-20 19:46:26'),
(2, 'Habitaciones', '2016-12-20 19:46:39', '2016-12-20 19:46:39'),
(3, 'Productos', '2016-12-20 19:46:48', '2016-12-20 19:46:48'),
(4, 'Cuentas Corrientes', '2016-12-20 19:47:01', '2016-12-20 19:47:01'),
(5, 'Compras', '2016-12-20 19:47:10', '2016-12-20 19:47:10'),
(6, 'Reservaciones', '2016-12-20 19:47:25', '2016-12-20 19:47:25'),
(7, 'Consumo Resto', '2016-12-20 19:47:39', '2016-12-20 19:47:39'),
(8, 'Factura', '2016-12-20 19:47:49', '2016-12-20 19:47:49'),
(9, 'Caja', '2016-12-20 19:48:03', '2016-12-20 19:48:03'),
(10, 'Empleados', '2016-12-20 19:48:14', '2016-12-20 19:48:14'),
(11, 'Usuarios', '2016-12-20 19:48:25', '2016-12-20 19:48:25'),
(12, 'Estadisticas', '2016-12-20 19:48:51', '2016-12-20 19:48:51'),
(13, 'Sistema', '2016-12-20 19:49:06', '2016-12-20 19:49:06'),
(14, 'Reporte', '2017-01-14 00:00:00', '2017-01-14 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `denominacion` varchar(300) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'Argentina', '2016-12-26 21:54:47', '2016-12-26 21:54:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personales`
--

CREATE TABLE `personales` (
  `id` int(11) NOT NULL,
  `nidentidad` varchar(60) NOT NULL,
  `nombres` varchar(90) NOT NULL,
  `apellidos` varchar(90) NOT NULL,
  `telefono` varchar(25) NOT NULL,
  `email` varchar(90) NOT NULL,
  `direccion` varchar(512) NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `nombre_archivo` varchar(500) NOT NULL,
  `created` date DEFAULT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `personales`
--

INSERT INTO `personales` (`id`, `nidentidad`, `nombres`, `apellidos`, `telefono`, `email`, `direccion`, `fecha_ingreso`, `nombre_archivo`, `created`, `modified`) VALUES
(2, '1', 'Eduardo', 'Ruiz', '2614250333', 'reservas@premiumtowersuites.com', '20 de junio 1410 godoy cruz', '2016-12-01', '', '2016-12-20', '2016-12-20'),
(3, '2', 'Fernando', 'Cruz', '4250333', 'reservas@premiumtowersuites.com', 'nn', '2016-12-01', '', '2016-12-20', '2016-12-20'),
(4, '3', 'Emiliano', 'Filipini', '2614250333', 'reservas@premiumtowersuites.com', 'nn', '2016-12-01', '', '2016-12-20', '2016-12-20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proalmacenes`
--

CREATE TABLE `proalmacenes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `protipopiso_id` int(11) NOT NULL,
  `denominacion` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proalmacenes`
--

INSERT INTO `proalmacenes` (`id`, `protipopiso_id`, `denominacion`, `created`, `modified`) VALUES
(1, 1, 'Almacen 1', '2016-10-25 16:56:02', '2016-10-25 16:56:02'),
(2, 2, 'Almacen 2', '2016-10-25 16:56:47', '2016-10-25 16:56:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proinventarios`
--

CREATE TABLE `proinventarios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `protipopiso_id` int(11) NOT NULL,
  `proalmacene_id` int(11) NOT NULL,
  `protipo_id` int(11) NOT NULL,
  `promarca_id` int(11) NOT NULL,
  `proproducto_id` int(11) NOT NULL,
  `cantidad` float(26,2) NOT NULL,
  `cantidadminima` float(26,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proinventarios`
--

INSERT INTO `proinventarios` (`id`, `protipopiso_id`, `proalmacene_id`, `protipo_id`, `promarca_id`, `proproducto_id`, `cantidad`, `cantidadminima`, `created`, `modified`) VALUES
(1, 1, 1, 1, 1, 1, 10.00, 1.00, '2016-10-25 23:54:11', '2016-10-26 00:01:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proivas`
--

CREATE TABLE `proivas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `denominacion` varchar(300) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proivas`
--

INSERT INTO `proivas` (`id`, `denominacion`, `created`, `modified`) VALUES
(2, '34', '2016-12-27 14:53:32', '2016-12-27 14:53:32'),
(3, '21', '2017-03-06 14:05:08', '2017-03-06 14:05:08'),
(4, '10.5', '2017-03-06 14:05:18', '2017-03-06 14:05:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promarcas`
--

CREATE TABLE `promarcas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `protipo_id` int(11) NOT NULL,
  `proproducto_id` int(11) NOT NULL,
  `denominacion` varchar(300) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proproductos`
--

CREATE TABLE `proproductos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `protipo_id` int(11) NOT NULL,
  `cod_producto` varchar(100) NOT NULL,
  `denominacion` varchar(300) NOT NULL,
  `plan_cuenta` varchar(300) NOT NULL,
  `precio_compra` float(26,2) NOT NULL,
  `ganancia` float(26,2) NOT NULL,
  `precio_neto` float(26,2) NOT NULL,
  `iva` float(26,2) NOT NULL,
  `impuesto` varchar(100) NOT NULL,
  `pvp` float(26,2) NOT NULL,
  `cantidad` float(26,2) NOT NULL,
  `cantidad_minima` float(26,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proproductos`
--

INSERT INTO `proproductos` (`id`, `protipo_id`, `cod_producto`, `denominacion`, `plan_cuenta`, `precio_compra`, `ganancia`, `precio_neto`, `iva`, `impuesto`, `pvp`, `cantidad`, `cantidad_minima`, `created`, `modified`) VALUES
(2, 3, '2', 'Coca Lata ', '1105', 10.00, 20.00, 12.00, 2.52, '21', 14.52, 100.00, 0.00, '2017-03-06 14:08:24', '2017-03-06 14:08:24'),
(3, 4, '12', 'Papas Fritas Lays', '001', 12.00, 100.00, 24.00, 8.16, '34', 32.16, 100.00, 0.00, '2017-04-25 20:29:57', '2017-04-25 20:29:57'),
(4, 2, '2314', 'sprite', '1101', 10.00, 100.00, 20.00, 4.20, '21', 24.20, 10.00, 0.00, '2017-04-25 20:30:05', '2017-04-25 20:30:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `protipopisos`
--

CREATE TABLE `protipopisos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `denominacion` varchar(300) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `protipopisos`
--

INSERT INTO `protipopisos` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'Piso de inventario', '2016-10-25 16:50:12', '2016-10-25 16:50:12'),
(2, 'Piso de ventas', '2016-10-25 16:50:24', '2016-10-25 16:50:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `protipos`
--

CREATE TABLE `protipos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `denominacion` varchar(300) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `protipos`
--

INSERT INTO `protipos` (`id`, `denominacion`, `created`, `modified`) VALUES
(2, 'Gaseosas', '2017-03-06 14:05:44', '2017-03-06 14:05:44'),
(3, 'FRIGOBAR ', '2017-03-06 14:06:48', '2017-03-06 14:06:48'),
(4, 'Papas Fritas', '2017-04-25 20:26:50', '2017-04-25 20:26:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cuit` varchar(100) NOT NULL,
  `razon_social` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `impuesto` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pstemporadas`
--

CREATE TABLE `pstemporadas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipotemporada_id` int(11) NOT NULL,
  `tipohabitacione_id` int(11) NOT NULL,
  `precio` float(26,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pstemporadas`
--

INSERT INTO `pstemporadas` (`id`, `tipotemporada_id`, `tipohabitacione_id`, `precio`, `created`, `modified`) VALUES
(3, 12, 2, 2123.00, '2016-12-20 08:57:41', '2016-12-20 08:57:41'),
(4, 12, 3, 2123.00, '2016-12-20 08:57:52', '2016-12-20 08:57:52'),
(5, 12, 4, 2510.00, '2016-12-20 08:58:04', '2016-12-20 08:58:04'),
(6, 12, 5, 2510.00, '2016-12-20 08:58:16', '2016-12-20 08:58:16'),
(7, 12, 6, 3486.00, '2016-12-20 09:00:29', '2016-12-20 09:00:29'),
(8, 12, 7, 3486.00, '2016-12-20 09:00:45', '2016-12-20 09:00:54');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserindividualeextras`
--

CREATE TABLE `reserindividualeextras` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reserindividuale_id` int(11) NOT NULL,
  `nombres` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reserindividualeextras`
--

INSERT INTO `reserindividualeextras` (`id`, `reserindividuale_id`, `nombres`, `created`, `modified`) VALUES
(2, 378, 'EDUARDO RUIZ ', '2017-03-13 16:04:34', '2017-03-13 16:04:34'),
(3, 454, 'Silvina Mariela Beranek', '2017-04-25 13:47:57', '2017-04-25 13:47:57'),
(4, 501, 'TOBIAS LUCERO ', '2017-05-30 19:19:29', '2017-05-30 19:19:29'),
(5, 4, 'juan pacheco', '2017-06-24 09:09:03', '2017-06-24 09:09:03'),
(6, 375, 'AMARILLA TAMARA ABIGAIL', '2017-09-12 15:17:11', '2017-09-12 15:17:11'),
(7, 385, 'lucero ricardo', '2017-10-31 12:42:28', '2017-10-31 12:42:28'),
(8, 385, 'lucero ricardo', '2017-10-31 12:43:14', '2017-10-31 12:43:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserindividuales`
--

CREATE TABLE `reserindividuales` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipocliente_id` int(11) NOT NULL,
  `tipoclientesub_id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `tipohabitacione_id` int(11) NOT NULL,
  `habitacione_id` int(11) NOT NULL,
  `reserstatusindividuale_id` int(11) NOT NULL DEFAULT '1',
  `fecha_entrada` date NOT NULL,
  `fecha_salida` date NOT NULL,
  `obseraciones` text NOT NULL,
  `observaciones_cliente` text NOT NULL,
  `dias` varchar(100) NOT NULL,
  `precioxdia` float(26,2) NOT NULL,
  `total` float(26,2) NOT NULL,
  `pagado` float(26,2) NOT NULL,
  `facturado` int(11) NOT NULL DEFAULT '0',
  `descuento` float(26,2) NOT NULL,
  `resermultiple_id` int(11) NOT NULL,
  `desayuno` int(11) NOT NULL DEFAULT '0',
  `almuerzo` int(11) NOT NULL DEFAULT '0',
  `cena` int(11) NOT NULL DEFAULT '0',
  `automovil` varchar(100) NOT NULL DEFAULT '0',
  `cantidad_personas` int(11) NOT NULL DEFAULT '0',
  `detallereservas` text,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reserindividuales`
--

INSERT INTO `reserindividuales` (`id`, `tipocliente_id`, `tipoclientesub_id`, `cliente_id`, `tipohabitacione_id`, `habitacione_id`, `reserstatusindividuale_id`, `fecha_entrada`, `fecha_salida`, `obseraciones`, `observaciones_cliente`, `dias`, `precioxdia`, `total`, `pagado`, `facturado`, `descuento`, `resermultiple_id`, `desayuno`, `almuerzo`, `cena`, `automovil`, `cantidad_personas`, `detallereservas`, `created`, `modified`) VALUES
(1, 1, 3, 4, 3, 23, 5, '2017-11-10', '2017-11-10', 'Sin observaciones', '', '0', 0.00, 0.00, 0.00, 1, 0.00, 0, 0, 1, 0, '', 1, '<table class=\'table\'><thead><tr><th>Tipo Temporada</th><th>Dias</th><th>Costo</th><tr></thead><tbody><tr><td>TEMPORADA BAJA</td><td>0</td><td>2.123,00</td></tr></table>', '2017-11-10 11:08:17', '2017-11-10 11:09:33'),
(2, 1, 3, 21, 6, 7, 1, '2017-12-15', '2017-11-17', '', '', '0', 0.00, 0.00, 0.00, 0, 12.00, 0, 0, 0, 0, '', 0, '', '2017-11-17 10:23:50', '2017-11-17 10:23:50'),
(3, 1, 3, 26, 6, 8, 1, '2017-11-13', '2017-11-17', '', '', '0', 0.00, 0.00, 0.00, 0, 0.00, 0, 0, 0, 0, '', 0, '', '2017-11-17 10:31:33', '2017-11-17 10:31:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserindivistatus`
--

CREATE TABLE `reserindivistatus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reserindividuale_id` int(11) NOT NULL,
  `reserstatusindividuale_id` int(11) NOT NULL,
  `conftipopagoreserva_id` int(11) DEFAULT '0',
  `fecha` date NOT NULL,
  `monto_penalidad` float(26,2) NOT NULL DEFAULT '0.00',
  `observaciones` text,
  `total` float(26,2) NOT NULL,
  `pago` float(26,2) NOT NULL,
  `debe` float(26,2) NOT NULL,
  `tarjetacredito_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reserindivistatus`
--

INSERT INTO `reserindivistatus` (`id`, `reserindividuale_id`, `reserstatusindividuale_id`, `conftipopagoreserva_id`, `fecha`, `monto_penalidad`, `observaciones`, `total`, `pago`, `debe`, `tarjetacredito_id`, `created`, `modified`) VALUES
(1, 1, 2, 1, '2016-11-09', 0.00, 'nada', 1603.20, 100.00, 1503.20, 0, '2016-11-09 20:40:22', '2016-11-09 20:40:22'),
(2, 1, 2, 1, '2016-11-09', 0.00, 'nada', 1603.20, 100.00, 1503.20, 0, '2016-11-09 20:40:55', '2016-11-09 20:40:55'),
(3, 1, 4, 1, '2016-11-11', 0.00, 'nada', 1603.20, 0.00, 0.00, 0, '2016-11-11 00:14:17', '2016-11-11 00:14:17'),
(4, 1, 4, 3, '2016-11-18', 0.00, 'nada', 1603.20, 0.00, 0.00, 0, '2016-11-18 22:49:58', '2016-11-18 22:49:58'),
(5, 1, 4, 1, '2016-11-18', 0.00, 'nada', 1603.20, 0.00, 0.00, 0, '2016-11-18 22:55:54', '2016-11-18 22:55:54'),
(6, 1, 4, 1, '2016-11-18', 0.00, 'n', 1603.20, 0.00, 0.00, 0, '2016-11-18 22:59:24', '2016-11-18 22:59:24'),
(7, 1, 4, 1, '2016-12-07', 0.00, 'nada', 1603.20, 0.00, 0.00, 0, '2016-12-07 10:59:37', '2016-12-07 10:59:37'),
(8, 1, 4, 1, '2016-12-07', 0.00, '', 1603.20, 0.00, 0.00, 0, '2016-12-07 11:01:40', '2016-12-07 11:01:40'),
(9, 3, 4, 1, '2016-12-07', 0.00, '', 0.00, 0.00, 0.00, 0, '2016-12-07 11:10:22', '2016-12-07 11:10:22'),
(10, 3, 5, 1, '2016-12-07', 0.00, '', 0.00, 0.00, 0.00, 0, '2016-12-07 11:13:51', '2016-12-07 11:13:51'),
(11, 4, 2, 1, '2016-12-07', 0.00, '', 400.80, 100.00, 300.80, 0, '2016-12-07 16:24:22', '2016-12-07 16:24:22'),
(12, 4, 5, 1, '2016-12-07', 0.00, '', 400.80, 0.00, 0.00, 0, '2016-12-07 16:24:36', '2016-12-07 16:24:36'),
(13, 5, 2, 1, '2016-12-07', 0.00, '', 501.00, 100.00, 401.00, 0, '2016-12-07 16:35:15', '2016-12-07 16:35:15'),
(14, 5, 5, 1, '2016-12-07', 0.00, '', 501.00, 0.00, 0.00, 0, '2016-12-07 16:35:30', '2016-12-07 16:35:30'),
(15, 6, 2, 1, '2016-12-18', 0.00, '', 200.40, 200.00, 0.40, 0, '2016-12-18 11:28:27', '2016-12-18 11:28:27'),
(16, 6, 4, 1, '2016-12-18', 0.00, '', 200.40, 0.00, 0.00, 0, '2016-12-18 11:28:45', '2016-12-18 11:28:45'),
(17, 7, 2, 1, '2016-12-30', 0.00, '', 1292.20, 100.00, 1192.20, 0, '2016-12-29 13:32:00', '2016-12-29 13:32:00'),
(18, 25, 2, 1, '2017-01-27', 0.00, 'ES TARJETA DE CRÃ‰DITO DE GARANTÃA ', 6972.00, 0.00, 0.00, 2, '2017-01-27 15:09:45', '2017-01-27 15:09:45'),
(19, 25, 2, 1, '2017-01-27', 0.00, 'ES TARJETA DE CRÃ‰DITO DE GARANTÃA ', 6972.00, 0.00, 0.00, 2, '2017-01-27 15:10:36', '2017-01-27 15:10:36'),
(20, 25, 2, 1, '2017-01-27', 0.00, 'ES TARJETA DE CRÃ‰DITO DE GARANTÃA ', 6972.00, 0.00, 0.00, 2, '2017-01-27 15:11:23', '2017-01-27 15:11:23'),
(21, 25, 2, 1, '2017-01-27', 0.00, 'ES TARJETA DE CRÃ‰DITO DE GARANTÃA ', 6972.00, 0.00, 0.00, 2, '2017-01-27 15:11:56', '2017-01-27 15:11:56'),
(22, 25, 2, 1, '2017-01-27', 0.00, 'ES GARANTIA ', 6972.00, 0.00, 0.00, 2, '2017-01-27 15:20:57', '2017-01-27 15:20:57'),
(23, 288, 2, 2, '2017-03-06', 0.00, '', 1486.10, 1486.10, 0.00, 0, '2017-03-06 12:22:23', '2017-03-06 12:22:23'),
(24, 288, 4, NULL, '2017-03-06', 0.00, 'acompaÃ±ante ', 1486.10, 0.00, 0.00, 0, '2017-03-06 12:40:09', '2017-03-06 12:40:09'),
(25, 288, 5, NULL, '2017-03-06', 0.00, 'salio antes. es prueba ', 1486.10, 0.00, 0.00, 0, '2017-03-06 14:14:34', '2017-03-06 14:14:34'),
(26, 378, 2, 3, '2017-03-13', 0.00, '', 2123.00, 2123.00, 0.00, 0, '2017-03-13 16:03:54', '2017-03-13 16:03:54'),
(27, 378, 4, NULL, '2017-03-13', 0.00, 'ACOMPALÃ‘ANTE ', 2123.00, 0.00, 0.00, 0, '2017-03-13 16:04:34', '2017-03-13 16:04:34'),
(28, 378, 5, NULL, '2017-03-14', 0.00, 'NADA', 2123.00, 0.00, 0.00, 0, '2017-03-13 16:08:42', '2017-03-13 16:08:42'),
(29, 453, 2, 1, '2017-04-25', 0.00, '', 22590.00, 10000.00, 12590.00, 3, '2017-04-25 13:41:12', '2017-04-25 13:41:12'),
(30, 453, 6, NULL, '2017-04-25', 5000.00, '', 22590.00, 0.00, 0.00, 3, '2017-04-25 13:43:40', '2017-04-25 13:43:40'),
(31, 454, 4, NULL, '2017-04-25', 0.00, '', 6972.00, 0.00, 0.00, 3, '2017-04-25 13:47:57', '2017-04-25 13:47:57'),
(32, 454, 5, NULL, '2017-04-27', 0.00, '', 6972.00, 0.00, 0.00, 0, '2017-04-25 19:38:47', '2017-04-25 19:38:47'),
(33, 407, 2, 2, '2017-04-28', 0.00, '', 6369.00, 0.00, 0.00, 0, '2017-04-25 20:12:30', '2017-04-25 20:12:30'),
(34, 407, 4, NULL, '2017-04-25', 0.00, '', 6369.00, 0.00, 0.00, 0, '2017-04-25 20:14:41', '2017-04-25 20:14:41'),
(35, 456, 4, NULL, '2017-05-01', 0.00, 'Sin observaciones', 4246.00, 0.00, 0.00, 0, '2017-05-01 21:08:42', '2017-05-01 21:08:42'),
(36, 456, 5, NULL, '2017-05-01', 0.00, 'Sin observaciones', 4246.00, 0.00, 0.00, 0, '2017-05-01 21:10:41', '2017-05-01 21:10:41'),
(37, 438, 2, 1, '2017-05-23', 0.00, '', 0.00, 0.00, 0.00, 0, '2017-05-23 11:35:12', '2017-05-23 11:35:12'),
(38, 438, 5, NULL, '2017-05-23', 0.00, '', 0.00, 0.00, 0.00, 0, '2017-05-23 11:37:16', '2017-05-23 11:37:16'),
(39, 471, 2, 2, '2017-05-23', 0.00, 'PAGA EFECTIVO', 1592.25, 1592.25, 0.00, 0, '2017-05-23 11:56:42', '2017-05-23 11:56:42'),
(40, 471, 4, NULL, '2017-05-23', 0.00, 'NINGUNA', 1592.25, 0.00, 0.00, 0, '2017-05-23 11:58:51', '2017-05-23 11:58:51'),
(41, 471, 5, NULL, '2017-05-23', 0.00, 'TIENE CONSUMOS DEL FARO', 1592.25, 0.00, 0.00, 0, '2017-05-23 12:03:56', '2017-05-23 12:03:56'),
(42, 501, 2, 2, '2017-05-30', 0.00, 'COBRAR DIFERENCIA.\r\n', 212.30, 10.00, 202.30, 0, '2017-05-30 19:18:33', '2017-05-30 19:18:33'),
(43, 501, 4, NULL, '2017-05-30', 0.00, 'VIENE CON LUCERO RICARDO ', 212.30, 0.00, 0.00, 0, '2017-05-30 19:19:29', '2017-05-30 19:19:29'),
(44, 501, 5, NULL, '2017-05-30', 0.00, 'PAGA PENALIDAD DE LA NOCHE \r\n$2123 90% PAGO EN EFECTIVO. ', 212.30, 0.00, 0.00, 0, '2017-05-30 19:25:33', '2017-05-30 19:25:33'),
(45, 92, 4, NULL, '2017-06-21', 0.00, '', 73206.00, 0.00, 0.00, 0, '2017-06-21 21:24:59', '2017-06-21 21:24:59'),
(46, 92, 4, NULL, '2017-06-21', 0.00, '', 73206.00, 0.00, 0.00, 0, '2017-06-21 21:26:44', '2017-06-21 21:26:44'),
(47, 92, 5, NULL, '2017-06-22', 0.00, '', 73206.00, 0.00, 0.00, 0, '2017-06-21 21:29:07', '2017-06-21 21:29:07'),
(48, 4, 4, NULL, '2017-06-24', 0.00, 'a', 0.00, 0.00, 0.00, 0, '2017-06-24 09:09:03', '2017-06-24 09:09:03'),
(49, 104, 2, 2, '2017-06-24', 0.00, 'Nada', 12550.00, 5000.00, 7550.00, 0, '2017-06-25 01:50:55', '2017-06-25 01:50:55'),
(50, 107, 2, 2, '2017-06-24', 0.00, '', 10458.00, 8000.00, 2458.00, 0, '2017-06-25 02:16:13', '2017-06-25 02:16:13'),
(51, 375, 4, NULL, '2017-09-11', 0.00, '', 8207.40, 0.00, 0.00, 0, '2017-09-12 15:17:11', '2017-09-12 15:17:11'),
(52, 186, 2, 3, '2017-09-13', 0.00, '', 9154.60, 0.00, 0.00, 0, '2017-09-13 17:31:25', '2017-09-13 17:31:25'),
(53, 188, 2, 3, '2017-09-13', 0.00, '', 6369.00, 0.00, 0.00, 0, '2017-09-13 17:35:29', '2017-09-13 17:35:29'),
(54, 188, 4, NULL, '2017-09-13', 0.00, '', 6369.00, 0.00, 0.00, 0, '2017-09-13 17:36:01', '2017-09-13 17:36:01'),
(55, 202, 4, NULL, '2017-09-16', 0.00, '', 17430.00, 0.00, 0.00, 0, '2017-09-18 16:15:22', '2017-09-18 16:15:22'),
(56, 203, 4, NULL, '2017-09-17', 0.00, '', 10615.00, 0.00, 0.00, 0, '2017-09-18 16:17:14', '2017-09-18 16:17:14'),
(57, 377, 4, NULL, '2017-09-16', 0.00, '', 17430.00, 0.00, 0.00, 0, '2017-09-18 16:43:21', '2017-09-18 16:43:21'),
(58, 57, 4, NULL, '2017-09-18', 0.00, '', 6600.00, 0.00, 0.00, 0, '2017-09-18 16:57:53', '2017-09-18 16:57:53'),
(59, 58, 4, NULL, '2017-09-17', 0.00, '', 7920.00, 0.00, 0.00, 0, '2017-09-18 17:20:23', '2017-09-18 17:20:23'),
(60, 379, 4, NULL, '2017-09-04', 0.00, '', 233530.00, 0.00, 0.00, 0, '2017-09-18 18:11:51', '2017-09-18 18:11:51'),
(61, 25, 4, NULL, '2017-09-18', 0.00, '', 8492.00, 0.00, 0.00, 0, '2017-09-18 19:18:26', '2017-09-18 19:18:26'),
(62, 207, 4, NULL, '2017-09-18', 0.00, '', 10040.00, 0.00, 0.00, 0, '2017-09-18 19:19:18', '2017-09-18 19:19:18'),
(63, 200, 4, NULL, '2017-09-16', 0.00, '', 13944.00, 0.00, 0.00, 0, '2017-09-18 19:21:12', '2017-09-18 19:21:12'),
(64, 378, 4, NULL, '2017-09-15', 0.00, '', 45180.00, 0.00, 0.00, 0, '2017-09-18 19:23:00', '2017-09-18 19:23:00'),
(65, 381, 4, NULL, '2017-09-18', 0.00, '', 0.00, 0.00, 0.00, 0, '2017-09-18 19:39:31', '2017-09-18 19:39:31'),
(66, 385, 2, 2, '2017-10-31', 0.00, 'cancela co descuento por ser empleado del hotel', 1698.40, 0.00, 0.00, 0, '2017-10-31 12:38:59', '2017-10-31 12:38:59'),
(67, 385, 4, NULL, '2017-10-31', 0.00, 'ninguna, ingresa 9 de la maÃ±ana', 1698.40, 0.00, 0.00, 0, '2017-10-31 12:42:28', '2017-10-31 12:42:28'),
(68, 385, 4, NULL, '2017-10-31', 0.00, 'ninguna, ingresa 9 de la maÃ±ana', 1698.40, 0.00, 0.00, 0, '2017-10-31 12:43:14', '2017-10-31 12:43:14'),
(69, 385, 4, NULL, '2017-10-31', 0.00, 'NADA', 1698.40, 0.00, 0.00, 0, '2017-10-31 12:44:07', '2017-10-31 12:44:07'),
(70, 385, 5, NULL, '2017-10-31', 0.00, 'NADA', 1698.40, 0.00, 0.00, 0, '2017-10-31 12:44:32', '2017-10-31 12:44:32'),
(71, 385, 1, NULL, '2017-10-31', 0.00, '', 1698.40, 0.00, 0.00, 0, '2017-10-31 15:21:51', '2017-10-31 15:21:51'),
(72, 385, 5, NULL, '2017-10-31', 0.00, '', 1698.40, 0.00, 0.00, 0, '2017-10-31 16:19:52', '2017-10-31 16:19:52'),
(73, 385, 4, NULL, '2017-10-31', 0.00, '', 1698.40, 0.00, 0.00, 0, '2017-10-31 16:35:48', '2017-10-31 16:35:48'),
(74, 385, 1, NULL, '2017-10-31', 0.00, '', 1698.40, 0.00, 0.00, 0, '2017-10-31 17:01:01', '2017-10-31 17:01:01'),
(75, 385, 1, NULL, '2017-10-31', 0.00, '', 1698.40, 0.00, 0.00, 0, '2017-10-31 17:01:15', '2017-10-31 17:01:15'),
(76, 385, 4, NULL, '2017-10-31', 0.00, '', 1698.40, 0.00, 0.00, 0, '2017-10-31 17:01:26', '2017-10-31 17:01:26'),
(77, 385, 4, NULL, '2017-10-31', 0.00, '', 1698.40, 0.00, 0.00, 0, '2017-10-31 17:16:15', '2017-10-31 17:16:15'),
(78, 385, 3, NULL, '2017-10-31', 0.00, '', 1698.40, 0.00, 0.00, 0, '2017-10-31 17:17:05', '2017-10-31 17:17:05'),
(79, 385, 4, NULL, '2017-10-31', 0.00, '', 1698.40, 0.00, 0.00, 0, '2017-10-31 17:17:19', '2017-10-31 17:17:19'),
(80, 385, 5, NULL, '2017-10-31', 0.00, '12', 1698.40, 0.00, 0.00, 0, '2017-10-31 17:27:47', '2017-10-31 17:27:47'),
(81, 385, 5, NULL, '2017-10-31', 0.00, '12', 1698.40, 0.00, 0.00, 0, '2017-10-31 17:27:55', '2017-10-31 17:27:55'),
(82, 385, 4, NULL, '2017-10-31', 0.00, '', 1698.40, 0.00, 0.00, 0, '2017-10-31 17:28:14', '2017-10-31 17:28:14'),
(83, 1, 5, NULL, '2017-11-10', 0.00, 'Ninguna', 0.00, 0.00, 0.00, 0, '2017-11-10 11:08:39', '2017-11-10 11:08:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resermulhabitaciones`
--

CREATE TABLE `resermulhabitaciones` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `resermultiple_id` int(11) NOT NULL,
  `habitacione_id` int(11) NOT NULL,
  `ocupada` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `resermulhabitaciones`
--

INSERT INTO `resermulhabitaciones` (`id`, `resermultiple_id`, `habitacione_id`, `ocupada`, `created`, `modified`) VALUES
(1, 4, 2, 0, '2016-11-09 08:01:12', '2016-11-09 08:01:12'),
(2, 5, 3, 0, '2016-11-18 23:18:49', '2016-11-18 23:18:49'),
(3, 9, 4, 0, '2016-11-19 00:17:23', '2016-11-19 00:17:23'),
(4, 9, 3, 0, '2016-11-19 00:17:23', '2016-11-19 00:17:23'),
(5, 9, 1, 0, '2016-11-19 00:17:23', '2016-11-19 00:17:23'),
(6, 10, 4, 0, '2016-12-07 08:04:05', '2016-12-07 08:04:05'),
(7, 10, 3, 0, '2016-12-07 08:04:05', '2016-12-07 08:04:05'),
(8, 11, 47, 0, '2017-02-06 14:58:31', '2017-02-06 14:58:31'),
(9, 11, 48, 0, '2017-02-06 14:58:31', '2017-02-06 14:58:31'),
(10, 11, 49, 0, '2017-02-06 14:58:31', '2017-02-06 14:58:31'),
(11, 11, 50, 0, '2017-02-06 14:58:31', '2017-02-06 14:58:31'),
(12, 11, 51, 0, '2017-02-06 14:58:31', '2017-02-06 14:58:31'),
(13, 11, 52, 0, '2017-02-06 14:58:31', '2017-02-06 14:58:31'),
(14, 11, 53, 0, '2017-02-06 14:58:31', '2017-02-06 14:58:31'),
(15, 11, 54, 0, '2017-02-06 14:58:31', '2017-02-06 14:58:31'),
(16, 11, 56, 0, '2017-02-06 14:58:31', '2017-02-06 14:58:31'),
(17, 11, 57, 0, '2017-02-06 14:58:31', '2017-02-06 14:58:31'),
(18, 11, 58, 0, '2017-02-06 14:58:31', '2017-02-06 14:58:31'),
(19, 11, 60, 0, '2017-02-06 14:58:31', '2017-02-06 14:58:31'),
(20, 11, 61, 0, '2017-02-06 14:58:31', '2017-02-06 14:58:31'),
(21, 11, 62, 0, '2017-02-06 14:58:31', '2017-02-06 14:58:31'),
(22, 11, 64, 0, '2017-02-06 14:58:31', '2017-02-06 14:58:31'),
(23, 11, 63, 0, '2017-02-06 14:58:31', '2017-02-06 14:58:31'),
(24, 11, 65, 0, '2017-02-06 14:58:31', '2017-02-06 14:58:31'),
(25, 11, 66, 0, '2017-02-06 14:58:31', '2017-02-06 14:58:31'),
(26, 11, 72, 0, '2017-02-06 14:58:31', '2017-02-06 14:58:31'),
(27, 12, 93, 0, '2017-02-08 16:51:53', '2017-02-08 16:51:53'),
(28, 12, 95, 0, '2017-02-08 16:51:53', '2017-02-08 16:51:53'),
(29, 12, 96, 0, '2017-02-08 16:51:53', '2017-02-08 16:51:53'),
(30, 12, 79, 0, '2017-02-08 16:51:53', '2017-02-08 16:51:53'),
(31, 13, 74, 0, '2017-02-08 16:56:33', '2017-02-08 16:56:33'),
(32, 13, 72, 0, '2017-02-08 16:56:33', '2017-02-08 16:56:33'),
(33, 13, 70, 0, '2017-02-08 16:56:33', '2017-02-08 16:56:33'),
(34, 13, 69, 0, '2017-02-08 16:56:33', '2017-02-08 16:56:33'),
(35, 13, 68, 0, '2017-02-08 16:56:33', '2017-02-08 16:56:33'),
(36, 13, 66, 0, '2017-02-08 16:56:33', '2017-02-08 16:56:33'),
(37, 13, 65, 0, '2017-02-08 16:56:33', '2017-02-08 16:56:33'),
(38, 13, 63, 0, '2017-02-08 16:56:33', '2017-02-08 16:56:33'),
(39, 13, 64, 0, '2017-02-08 16:56:33', '2017-02-08 16:56:33'),
(40, 13, 62, 0, '2017-02-08 16:56:33', '2017-02-08 16:56:33'),
(41, 13, 61, 0, '2017-02-08 16:56:33', '2017-02-08 16:56:33'),
(42, 13, 60, 0, '2017-02-08 16:56:33', '2017-02-08 16:56:33'),
(43, 13, 58, 0, '2017-02-08 16:56:33', '2017-02-08 16:56:33'),
(44, 13, 57, 0, '2017-02-08 16:56:33', '2017-02-08 16:56:33'),
(45, 13, 56, 0, '2017-02-08 16:56:33', '2017-02-08 16:56:33'),
(46, 13, 55, 0, '2017-02-08 16:56:33', '2017-02-08 16:56:33'),
(47, 13, 52, 0, '2017-02-08 16:56:33', '2017-02-08 16:56:33'),
(48, 13, 54, 0, '2017-02-08 16:56:33', '2017-02-08 16:56:33'),
(49, 13, 51, 0, '2017-02-08 16:56:33', '2017-02-08 16:56:33'),
(50, 14, 47, 0, '2017-02-08 17:50:52', '2017-02-08 17:50:52'),
(51, 14, 52, 0, '2017-02-08 17:50:52', '2017-02-08 17:50:52'),
(52, 14, 53, 0, '2017-02-08 17:50:52', '2017-02-08 17:50:52'),
(53, 14, 54, 0, '2017-02-08 17:50:52', '2017-02-08 17:50:52'),
(54, 14, 55, 0, '2017-02-08 17:50:52', '2017-02-08 17:50:52'),
(55, 14, 56, 0, '2017-02-08 17:50:52', '2017-02-08 17:50:52'),
(56, 14, 58, 0, '2017-02-08 17:50:52', '2017-02-08 17:50:52'),
(57, 14, 60, 0, '2017-02-08 17:50:52', '2017-02-08 17:50:52'),
(58, 14, 61, 0, '2017-02-08 17:50:52', '2017-02-08 17:50:52'),
(59, 14, 62, 0, '2017-02-08 17:50:52', '2017-02-08 17:50:52'),
(60, 14, 64, 0, '2017-02-08 17:50:52', '2017-02-08 17:50:52'),
(61, 14, 63, 0, '2017-02-08 17:50:52', '2017-02-08 17:50:52'),
(62, 14, 65, 0, '2017-02-08 17:50:52', '2017-02-08 17:50:52'),
(63, 14, 66, 0, '2017-02-08 17:50:52', '2017-02-08 17:50:52'),
(64, 14, 67, 0, '2017-02-08 17:50:52', '2017-02-08 17:50:52'),
(65, 15, 47, 0, '2017-02-08 18:23:09', '2017-02-08 18:23:09'),
(66, 15, 48, 0, '2017-02-08 18:23:09', '2017-02-08 18:23:09'),
(67, 15, 49, 0, '2017-02-08 18:23:09', '2017-02-08 18:23:09'),
(68, 15, 50, 0, '2017-02-08 18:23:09', '2017-02-08 18:23:09'),
(69, 15, 51, 0, '2017-02-08 18:23:09', '2017-02-08 18:23:09'),
(70, 15, 52, 0, '2017-02-08 18:23:09', '2017-02-08 18:23:09'),
(71, 15, 53, 0, '2017-02-08 18:23:09', '2017-02-08 18:23:09'),
(72, 15, 54, 0, '2017-02-08 18:23:09', '2017-02-08 18:23:09'),
(73, 15, 55, 0, '2017-02-08 18:23:09', '2017-02-08 18:23:09'),
(74, 15, 56, 0, '2017-02-08 18:23:09', '2017-02-08 18:23:09'),
(75, 15, 57, 0, '2017-02-08 18:23:09', '2017-02-08 18:23:09'),
(76, 15, 58, 0, '2017-02-08 18:23:09', '2017-02-08 18:23:09'),
(77, 15, 60, 0, '2017-02-08 18:23:09', '2017-02-08 18:23:09'),
(78, 15, 61, 0, '2017-02-08 18:23:09', '2017-02-08 18:23:09'),
(79, 15, 62, 0, '2017-02-08 18:23:09', '2017-02-08 18:23:09'),
(80, 16, 73, 0, '2017-02-08 18:56:30', '2017-02-08 18:56:30'),
(81, 16, 74, 0, '2017-02-08 18:56:30', '2017-02-08 18:56:30'),
(82, 16, 76, 0, '2017-02-08 18:56:30', '2017-02-08 18:56:30'),
(83, 16, 70, 0, '2017-02-08 18:56:30', '2017-02-08 18:56:30'),
(84, 17, 79, 0, '2017-03-02 10:06:52', '2017-03-02 10:06:52'),
(85, 17, 82, 0, '2017-03-02 10:06:52', '2017-03-02 10:06:52'),
(86, 17, 87, 0, '2017-03-02 10:06:52', '2017-03-02 10:06:52'),
(87, 18, 56, 0, '2017-03-03 15:16:31', '2017-03-03 15:16:31'),
(88, 18, 49, 0, '2017-03-03 15:16:31', '2017-03-03 15:16:31'),
(89, 19, 47, 0, '2017-03-08 13:03:46', '2017-03-08 13:03:46'),
(90, 19, 48, 0, '2017-03-08 13:03:46', '2017-03-08 13:03:46'),
(91, 19, 49, 0, '2017-03-08 13:03:46', '2017-03-08 13:03:46'),
(92, 19, 50, 0, '2017-03-08 13:03:46', '2017-03-08 13:03:46'),
(93, 19, 51, 0, '2017-03-08 13:03:46', '2017-03-08 13:03:46'),
(94, 19, 53, 0, '2017-03-08 13:03:46', '2017-03-08 13:03:46'),
(95, 19, 54, 0, '2017-03-08 13:03:46', '2017-03-08 13:03:46'),
(96, 19, 55, 0, '2017-03-08 13:03:46', '2017-03-08 13:03:46'),
(97, 19, 56, 0, '2017-03-08 13:03:46', '2017-03-08 13:03:46'),
(98, 19, 57, 0, '2017-03-08 13:03:46', '2017-03-08 13:03:46'),
(99, 19, 52, 0, '2017-03-08 13:03:46', '2017-03-08 13:03:46'),
(100, 20, 47, 0, '2017-03-09 10:11:52', '2017-03-09 10:11:52'),
(101, 20, 48, 0, '2017-03-09 10:11:52', '2017-03-09 10:11:52'),
(102, 20, 51, 0, '2017-03-09 10:11:52', '2017-03-09 10:11:52'),
(103, 21, 47, 0, '2017-03-09 10:17:18', '2017-03-09 10:17:18'),
(104, 21, 48, 0, '2017-03-09 10:17:18', '2017-03-09 10:17:18'),
(105, 21, 51, 0, '2017-03-09 10:17:18', '2017-03-09 10:17:18'),
(106, 22, 47, 0, '2017-03-09 10:20:15', '2017-03-09 10:20:15'),
(107, 22, 48, 0, '2017-03-09 10:20:15', '2017-03-09 10:20:15'),
(108, 22, 51, 0, '2017-03-09 10:20:15', '2017-03-09 10:20:15'),
(109, 23, 47, 0, '2017-03-09 10:23:36', '2017-03-09 10:23:36'),
(110, 23, 48, 0, '2017-03-09 10:23:36', '2017-03-09 10:23:36'),
(111, 23, 51, 0, '2017-03-09 10:23:36', '2017-03-09 10:23:36'),
(112, 24, 47, 0, '2017-03-09 10:25:22', '2017-03-09 10:25:22'),
(113, 24, 48, 0, '2017-03-09 10:25:22', '2017-03-09 10:25:22'),
(114, 24, 51, 0, '2017-03-09 10:25:22', '2017-03-09 10:25:22'),
(115, 25, 47, 0, '2017-03-09 10:28:37', '2017-03-09 10:28:37'),
(116, 25, 48, 0, '2017-03-09 10:28:37', '2017-03-09 10:28:37'),
(117, 25, 51, 0, '2017-03-09 10:28:37', '2017-03-09 10:28:37'),
(118, 26, 47, 0, '2017-03-09 10:30:07', '2017-03-09 10:30:07'),
(119, 26, 48, 0, '2017-03-09 10:30:07', '2017-03-09 10:30:07'),
(120, 26, 51, 0, '2017-03-09 10:30:07', '2017-03-09 10:30:07'),
(121, 1, 35, 0, '2017-06-24 08:57:21', '2017-06-24 08:57:21'),
(122, 2, 35, 0, '2017-06-24 08:58:08', '2017-06-24 08:58:08'),
(123, 3, 27, 0, '2017-06-24 12:24:55', '2017-06-24 12:24:55'),
(124, 3, 25, 0, '2017-06-24 12:24:55', '2017-06-24 12:24:55'),
(125, 3, 32, 0, '2017-06-24 12:24:55', '2017-06-24 12:24:55'),
(126, 45, 36, 0, '2017-08-30 15:47:16', '2017-08-30 15:47:16'),
(127, 45, 23, 0, '2017-08-30 15:47:16', '2017-08-30 15:47:16'),
(128, 46, 35, 0, '2017-08-30 17:56:20', '2017-08-30 17:56:20'),
(129, 46, 36, 0, '2017-08-30 17:56:20', '2017-08-30 17:56:20'),
(130, 46, 37, 0, '2017-08-30 17:56:20', '2017-08-30 17:56:20'),
(131, 46, 38, 0, '2017-08-30 17:56:20', '2017-08-30 17:56:20'),
(132, 46, 39, 0, '2017-08-30 17:56:20', '2017-08-30 17:56:20'),
(133, 46, 40, 0, '2017-08-30 17:56:20', '2017-08-30 17:56:20'),
(134, 46, 41, 0, '2017-08-30 17:56:20', '2017-08-30 17:56:20'),
(135, 46, 23, 0, '2017-08-30 17:56:20', '2017-08-30 17:56:20'),
(136, 46, 24, 0, '2017-08-30 17:56:20', '2017-08-30 17:56:20'),
(137, 46, 25, 0, '2017-08-30 17:56:20', '2017-08-30 17:56:20'),
(138, 46, 26, 0, '2017-08-30 17:56:20', '2017-08-30 17:56:20'),
(139, 46, 27, 0, '2017-08-30 17:56:20', '2017-08-30 17:56:20'),
(140, 46, 28, 0, '2017-08-30 17:56:20', '2017-08-30 17:56:20'),
(141, 46, 29, 0, '2017-08-30 17:56:20', '2017-08-30 17:56:20'),
(142, 46, 30, 0, '2017-08-30 17:56:20', '2017-08-30 17:56:20'),
(143, 46, 31, 0, '2017-08-30 17:56:20', '2017-08-30 17:56:20'),
(144, 46, 32, 0, '2017-08-30 17:56:20', '2017-08-30 17:56:20'),
(145, 46, 33, 0, '2017-08-30 17:56:20', '2017-08-30 17:56:20'),
(146, 46, 34, 0, '2017-08-30 17:56:20', '2017-08-30 17:56:20'),
(147, 47, 38, 0, '2017-08-30 18:03:22', '2017-08-30 18:03:22'),
(148, 47, 39, 0, '2017-08-30 18:03:22', '2017-08-30 18:03:22'),
(149, 47, 40, 0, '2017-08-30 18:03:22', '2017-08-30 18:03:22'),
(150, 47, 41, 0, '2017-08-30 18:03:22', '2017-08-30 18:03:22'),
(151, 47, 23, 0, '2017-08-30 18:03:22', '2017-08-30 18:03:22'),
(152, 47, 24, 0, '2017-08-30 18:03:22', '2017-08-30 18:03:22'),
(153, 47, 25, 0, '2017-08-30 18:03:22', '2017-08-30 18:03:22'),
(154, 47, 26, 0, '2017-08-30 18:03:22', '2017-08-30 18:03:22'),
(155, 47, 27, 0, '2017-08-30 18:03:22', '2017-08-30 18:03:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resermultiples`
--

CREATE TABLE `resermultiples` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tipocliente_id` int(11) NOT NULL,
  `tipoclientesub_id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `tipohabitacione_id` int(11) NOT NULL,
  `cantidad` varchar(100) NOT NULL,
  `cantidad_ocupada` int(11) NOT NULL DEFAULT '0',
  `reserstatusmultiple_id` int(11) NOT NULL DEFAULT '1',
  `fecha_entrada` date NOT NULL,
  `fecha_salida` date NOT NULL,
  `fecha_tope` date NOT NULL,
  `obseraciones` text NOT NULL,
  `dias` varchar(100) NOT NULL,
  `precioxdia` float(26,2) NOT NULL,
  `total` float(26,2) NOT NULL,
  `pagado` float(26,2) NOT NULL,
  `facturado` int(11) NOT NULL DEFAULT '0',
  `descuento` float(26,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `resermultiples`
--

INSERT INTO `resermultiples` (`id`, `tipocliente_id`, `tipoclientesub_id`, `cliente_id`, `tipohabitacione_id`, `cantidad`, `cantidad_ocupada`, `reserstatusmultiple_id`, `fecha_entrada`, `fecha_salida`, `fecha_tope`, `obseraciones`, `dias`, `precioxdia`, `total`, `pagado`, `facturado`, `descuento`, `created`, `modified`) VALUES
(4, 3, 83, 811, 4, '7', 0, 1, '2017-07-05', '2017-07-07', '2017-06-30', 'BLOQUEO CORREO ARGENTINO\r\nRVA REF 56116\r\nTARIFA SGL/DBL $1295\r\nTOTAL 7 HAB X 2 NTS = $18.130.-\r\nVENCIMIENTO GARANTIA: 23 DE JUNIO', '0', 1295.00, 0.00, 0.00, 0, 0.00, '2017-06-27 22:07:49', '2017-06-27 22:07:49'),
(7, 3, 16, 47, 5, '2', 0, 1, '2017-09-10', '2017-09-16', '2017-08-31', '', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-28 16:48:24', '2017-08-28 16:48:24'),
(8, 3, 16, 47, 5, '2', 0, 1, '2017-09-17', '2017-09-23', '2017-08-31', '', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-28 16:49:22', '2017-08-28 16:49:22'),
(9, 3, 16, 47, 4, '2', 0, 1, '2017-09-24', '2017-09-30', '2017-08-31', '', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-28 16:50:42', '2017-08-28 16:50:42'),
(10, 3, 16, 47, 5, '2', 0, 1, '2017-10-01', '2017-10-07', '2017-08-31', '', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-28 16:51:44', '2017-08-28 17:07:59'),
(11, 3, 16, 47, 5, '2', 0, 1, '2017-10-08', '2017-10-14', '2017-08-31', '', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-28 17:05:24', '2017-08-28 17:05:24'),
(12, 3, 16, 47, 5, '2', 0, 1, '2017-10-15', '2017-10-19', '2017-08-31', '', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-28 17:06:34', '2017-08-28 17:06:34'),
(13, 3, 16, 47, 5, '2', 0, 1, '2017-10-22', '2017-10-28', '2017-08-31', '', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-28 17:09:49', '2017-08-28 17:09:49'),
(14, 3, 16, 47, 5, '2', 0, 1, '2017-10-29', '2017-11-04', '2017-08-31', '', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-28 17:10:25', '2017-08-28 17:10:25'),
(15, 3, 16, 47, 5, '2', 0, 1, '2017-11-05', '2017-11-11', '2017-08-31', '', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-28 17:11:42', '2017-08-28 17:11:42'),
(16, 3, 16, 47, 5, '2', 0, 1, '2017-11-12', '2017-11-18', '2017-08-31', '', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-28 17:12:38', '2017-08-28 17:12:38'),
(17, 3, 16, 47, 5, '2', 0, 1, '2017-11-19', '2017-11-25', '2017-08-31', '', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-28 17:20:46', '2017-08-28 17:20:46'),
(18, 3, 16, 47, 5, '2', 0, 1, '2017-11-26', '2017-12-02', '2017-08-31', '', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-28 17:21:37', '2017-08-28 17:21:37'),
(19, 3, 16, 47, 5, '2', 0, 1, '2017-12-03', '2017-12-09', '2017-08-31', '', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-28 17:22:21', '2017-08-28 17:22:21'),
(20, 3, 16, 47, 5, '2', 0, 1, '2017-12-10', '2017-12-16', '2017-08-31', '', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-28 17:23:11', '2017-08-28 17:23:11'),
(21, 3, 16, 47, 5, '2', 0, 1, '2017-12-17', '2017-12-23', '2017-08-31', '', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-28 17:23:58', '2017-08-28 17:23:58'),
(22, 3, 16, 47, 5, '2', 0, 1, '2017-12-24', '2017-12-30', '2017-08-31', '', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-28 17:24:36', '2017-08-28 17:24:36'),
(23, 2, 86, 872, 5, '20', 0, 1, '2017-09-18', '2017-09-20', '2017-08-31', 'RESERVA REFERENCIA EN BIG SYS 57846', '2', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-28 18:28:03', '2017-08-28 18:28:03'),
(24, 2, 88, 874, 5, '16', 0, 1, '2017-09-18', '2017-09-19', '2017-08-31', '', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-28 19:04:15', '2017-08-28 19:04:15'),
(25, 2, 20, 877, 5, '19', 0, 1, '2017-09-20', '2017-09-22', '2017-08-31', 'RESERVA REFERENCIA BIG SYS 52987', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-28 19:42:00', '2017-08-28 19:42:00'),
(26, 2, 20, 878, 2, '19', 0, 1, '2017-09-21', '2017-09-23', '2017-08-31', 'RESERVA REFERENCIA EN BIG SYS 53605', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-28 19:51:46', '2017-08-28 19:51:46'),
(27, 2, 20, 882, 5, '21', 0, 1, '2017-09-22', '2017-09-23', '2017-08-31', '', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-28 20:40:28', '2017-08-28 20:40:57'),
(28, 2, 20, 883, 7, '21', 0, 1, '2017-09-22', '2017-09-24', '2017-08-31', 'RESERVA REFERENCIA BIG SYS 52343', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-28 20:50:33', '2017-08-28 20:50:33'),
(29, 2, 88, 884, 2, '16', 0, 1, '2017-09-23', '2017-09-24', '2017-08-31', '', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-28 21:26:42', '2017-08-28 21:26:42'),
(30, 2, 20, 885, 5, '23', 0, 1, '2017-09-23', '2017-09-25', '2017-08-31', 'RESERVA REFERENCIA BIG SYS 53959', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-28 21:34:05', '2017-08-28 21:34:05'),
(31, 2, 20, 898, 5, '21', 0, 1, '2017-09-27', '2017-09-28', '2017-08-31', 'RESERVA REFERENCIA EN BIG SYS 49798', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-29 15:02:43', '2017-08-29 15:02:43'),
(32, 2, 20, 900, 5, '23', 0, 1, '2017-09-29', '2017-09-30', '2017-08-31', 'RESERVA DE REFERENCIA EN BIG SYS 53977', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-29 15:11:05', '2017-08-29 15:11:05'),
(33, 2, 86, 872, 5, '15', 0, 1, '2017-09-29', '2017-09-30', '2017-08-31', 'RESERVA REFERENCIA EN BIG SYS 57866', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-29 15:20:07', '2017-08-29 15:20:07'),
(34, 2, 76, 909, 7, '20', 0, 1, '2017-10-01', '2017-10-06', '2017-08-31', '', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-29 16:13:12', '2017-08-29 16:13:12'),
(35, 2, 95, 915, 2, '15', 0, 1, '2017-10-01', '2017-10-02', '2017-08-31', 'RESERVA REFERENCIA EN BIG SYS 54216', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-29 16:38:51', '2017-08-29 16:38:51'),
(36, 2, 95, 934, 2, '15', 0, 1, '2017-10-05', '2017-10-06', '2017-08-31', 'RESERVA DE REFERENCIA EN BIG SYS 54231', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-29 18:06:16', '2017-08-29 18:06:16'),
(37, 2, 99, 947, 2, '7', 0, 1, '2017-10-06', '2017-10-09', '2017-08-31', 'RESERVA REFERENCIA EN BIG SYS 56562', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-29 19:01:55', '2017-08-29 19:01:55'),
(38, 2, 20, 954, 5, '20', 0, 1, '2017-10-07', '2017-10-09', '2017-08-31', 'RESERVA REFERENCIA BIG SYS 53297', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-29 19:45:58', '2017-08-29 19:45:58'),
(39, 2, 20, 955, 5, '21', 0, 1, '2017-10-07', '2017-10-09', '2017-08-31', 'RESERVA REFERENCIA BIG SYS 52156', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-29 20:00:52', '2017-08-29 20:00:52'),
(40, 2, 20, 959, 4, '17', 0, 1, '2017-10-09', '2017-10-11', '2017-08-31', 'RESERVA REFERENCIA BIG SYS 56606', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-29 20:28:02', '2017-08-29 20:28:02'),
(41, 3, 101, 962, 5, '17', 0, 1, '2017-10-12', '2017-10-14', '2017-08-31', 'RESERVA REFERENCIA EN BIG SYS 56306', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-29 20:54:35', '2017-08-29 20:54:35'),
(42, 2, 20, 963, 5, '17', 0, 1, '2017-10-12', '2017-10-14', '2017-08-31', 'RESERVA DE REFERENCIA EN BIG SYS 56146', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-29 21:22:18', '2017-08-29 21:22:18'),
(43, 2, 20, 965, 5, '21', 0, 1, '2017-10-12', '2017-10-14', '2017-08-31', 'RESERVA REFERENCIA BIG SYS 53413', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-29 21:33:07', '2017-08-29 21:33:07'),
(44, 2, 46, 406, 3, '14', 0, 1, '2017-12-01', '2017-12-04', '2017-11-01', 'ninguna', '4', 100.00, 5600.00, 0.00, 0, 0.00, '2017-08-29 21:48:33', '2017-08-29 21:48:33'),
(47, 1, 3, 759, 3, '9', 0, 1, '2017-12-28', '2017-12-30', '2017-11-30', '', '2', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-30 18:03:22', '2017-08-30 18:03:22'),
(48, 2, 105, 1023, 5, '18', 0, 1, '2017-10-28', '2017-10-30', '2017-08-31', 'RESERVA REFERENCIA EN BIG SYS 56097', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-31 00:49:04', '2017-08-31 00:49:04'),
(49, 2, 20, 1027, 5, '24', 0, 2, '2017-11-06', '2017-11-07', '2017-08-31', 'RESERVA REFERENCIA EN BIG SYS 57763', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-31 15:09:25', '2017-08-31 15:20:46'),
(50, 2, 19, 1032, 7, '4', 0, 1, '2017-11-10', '2017-11-12', '2017-08-31', 'RESERVA REFERENCIA EN BIG SYS 56522', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-31 15:55:51', '2017-08-31 15:55:51'),
(51, 2, 20, 1033, 5, '24', 0, 1, '2017-11-11', '2017-11-12', '2017-08-31', 'RESERVA REFERENCIA EN BIG SYS 57781', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-31 16:09:20', '2017-08-31 16:09:20'),
(52, 2, 57, 1042, 5, '22', 0, 1, '2017-11-19', '2017-11-20', '2017-08-31', 'RESERVA REFERENCIA EN BIG SYS 53101', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-31 17:43:19', '2017-08-31 17:43:19'),
(53, 2, 57, 1043, 5, '5', 0, 1, '2017-11-19', '2017-11-20', '2017-08-31', 'RESERVA REFERENCIA BIG SYS 53101', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-31 17:52:34', '2017-08-31 17:52:34'),
(54, 2, 20, 1064, 5, '12', 0, 1, '2017-12-29', '2017-12-31', '2017-08-31', 'RESERVA REFERENCIA BIG SYS 56327', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-08-31 20:47:59', '2017-08-31 20:47:59'),
(55, 2, 20, 1065, 2, '21', 0, 1, '2018-01-04', '2018-01-06', '2017-09-04', 'RESERVA REFERENCIA EN BIG SYS 56674', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-09-04 17:27:06', '2017-09-04 17:27:06'),
(56, 2, 20, 1066, 5, '21', 0, 1, '2018-01-04', '2018-01-06', '2017-09-04', 'RESERVA REFERENCIA EN BIG SYS 55149', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-09-04 17:45:24', '2017-09-04 17:45:24'),
(57, 2, 76, 696, 7, '25', 0, 1, '2018-01-07', '2018-01-11', '2017-09-04', 'RESERVA REFERENCIA EN BIG SYS 58075', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-09-04 18:00:21', '2017-09-04 18:00:21'),
(58, 2, 20, 1067, 4, '21', 0, 1, '2018-01-07', '2018-01-09', '2017-09-04', 'RESERVA REFERENCIA EN BIG SYS 55314', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-09-04 18:05:31', '2017-09-04 18:05:31'),
(59, 2, 20, 1068, 5, '21', 0, 1, '2018-01-11', '2018-01-12', '2017-09-04', 'RESERVA REFERENCIA EN BIG SYS 55335', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-09-04 18:09:04', '2017-09-04 18:09:04'),
(60, 2, 20, 1069, 5, '21', 0, 1, '2018-01-11', '2018-01-13', '2017-09-04', 'RESERVA REFERENCIA EN BIG SYS', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-09-04 18:14:58', '2017-09-04 18:14:58'),
(62, 2, 20, 1072, 4, '19', 0, 1, '2018-01-25', '2018-01-27', '2017-09-04', 'RESERVA DE REFERENCIA EN BIG SYS 55130', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-09-04 18:28:28', '2017-09-04 18:28:28'),
(63, 2, 76, 696, 4, '25', 0, 1, '2018-02-04', '2018-02-08', '2017-09-04', 'RESERVA REFERENCIA EN BIG SYS 58102', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-09-04 18:32:48', '2017-09-04 18:32:48'),
(64, 2, 20, 1073, 5, '19', 0, 1, '2018-02-11', '2018-02-13', '2017-09-04', 'RESERVA REFERENCIA EN BIG SYS 49475', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-09-04 19:56:15', '2017-09-04 19:56:15'),
(65, 2, 20, 1074, 4, '21', 0, 1, '2018-05-13', '2018-05-15', '2017-09-04', 'RESERVA REFERENCIA BIG SYS 57706', '0', 0.00, 0.00, 0.00, 0, 0.00, '2017-09-04 20:03:19', '2017-09-04 20:03:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resermultistatus`
--

CREATE TABLE `resermultistatus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `resermultiple_id` int(11) NOT NULL,
  `reserstatusmultiple_id` int(11) NOT NULL,
  `conftipopagoreserva_id` int(11) NOT NULL DEFAULT '0',
  `fecha` date NOT NULL,
  `monto_penalidad` float(26,2) NOT NULL DEFAULT '0.00',
  `observaciones` text NOT NULL,
  `total` float(26,2) NOT NULL,
  `pago` float(26,2) NOT NULL,
  `debe` float(26,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `resermultistatus`
--

INSERT INTO `resermultistatus` (`id`, `resermultiple_id`, `reserstatusmultiple_id`, `conftipopagoreserva_id`, `fecha`, `monto_penalidad`, `observaciones`, `total`, `pago`, `debe`, `created`, `modified`) VALUES
(1, 4, 2, 1, '2016-11-09', 0.00, 'nada', 701.40, 100.00, 601.40, '2016-11-09 23:29:57', '2016-11-09 23:29:57'),
(2, 4, 2, 1, '2016-12-07', 0.00, 'nada', 701.40, 100.00, 601.40, '2016-12-07 07:51:55', '2016-12-07 07:51:55'),
(3, 4, 2, 1, '2016-12-07', 0.00, '', 701.40, 100.00, 601.40, '2016-12-07 07:52:37', '2016-12-07 07:52:37'),
(4, 4, 2, 1, '2016-12-07', 0.00, 'nada', 701.40, 100.00, 601.40, '2016-12-07 07:54:21', '2016-12-07 07:54:21'),
(5, 6, 2, 1, '2016-12-07', 0.00, '', 901.80, 100.00, 801.80, '2016-12-07 08:00:51', '2016-12-07 08:00:51'),
(6, 10, 2, 1, '2016-12-07', 0.00, '', 600.00, 100.00, 500.00, '2016-12-07 08:04:35', '2016-12-07 08:04:35'),
(7, 1, 2, 2, '2017-06-24', 0.00, 'a', 61567.00, 12.00, 61555.00, '2017-06-24 09:00:18', '2017-06-24 09:00:18'),
(8, 49, 2, 1, '2017-08-31', 0.00, '', 0.00, 0.00, 0.00, '2017-08-31 15:20:46', '2017-08-31 15:20:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserstatusindividuales`
--

CREATE TABLE `reserstatusindividuales` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `denominacion` varchar(300) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reserstatusindividuales`
--

INSERT INTO `reserstatusindividuales` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'INICIADA ', '2016-11-01 15:04:23', '2017-03-13 15:54:43'),
(2, 'CONFIRMADA ', '2016-11-01 15:04:34', '2017-03-13 15:55:28'),
(3, 'AUDITADA ', '2016-11-01 15:04:46', '2017-03-13 15:55:39'),
(4, 'INGRESADA', '2016-11-01 15:05:00', '2017-03-13 15:54:11'),
(5, 'EGRESADA ', '2016-11-01 15:05:11', '2017-03-13 15:54:58'),
(6, 'NO SHOW', '2016-11-09 13:05:28', '2017-03-13 15:55:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserstatusmultiples`
--

CREATE TABLE `reserstatusmultiples` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `denominacion` varchar(300) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reserstatusmultiples`
--

INSERT INTO `reserstatusmultiples` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'Nueva', '2016-11-09 08:02:42', '2016-11-09 08:02:42'),
(2, 'Confirmado', '2016-11-09 23:23:20', '2016-11-09 23:35:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` date DEFAULT NULL,
  `modified` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `title`, `alias`, `created`, `modified`) VALUES
(1, 'Admin', 'admin', '2009-04-05', '0000-00-00'),
(4, 'Gobernatas', 'Gobernatas', '2016-10-17', '2016-12-06'),
(5, 'Reserva', 'Reserva', '2016-11-16', '2016-12-06'),
(6, 'Facturacion', 'Facturacion', '2016-12-01', '2016-12-01'),
(7, 'Cuenta Corriente', 'CtaCte', '2017-09-18', '2017-09-18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rolesmodulos`
--

CREATE TABLE `rolesmodulos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(11) NOT NULL,
  `modulo_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rolesmodulos`
--

INSERT INTO `rolesmodulos` (`id`, `role_id`, `modulo_id`, `created`, `modified`) VALUES
(1, 1, 1, '2016-12-20 19:54:21', '2016-12-20 19:54:21'),
(2, 1, 2, '2016-12-20 19:54:30', '2016-12-20 19:54:30'),
(3, 1, 3, '2016-12-20 19:54:40', '2016-12-20 19:54:40'),
(4, 1, 4, '2016-12-20 19:54:51', '2016-12-20 19:54:51'),
(5, 1, 5, '2016-12-20 19:55:00', '2016-12-20 19:55:00'),
(6, 1, 6, '2016-12-20 19:55:11', '2016-12-20 19:55:11'),
(7, 1, 7, '2016-12-20 19:55:20', '2016-12-20 19:55:20'),
(8, 1, 8, '2016-12-20 19:55:29', '2016-12-20 19:55:29'),
(9, 1, 10, '2016-12-20 19:55:41', '2016-12-20 19:55:41'),
(10, 1, 9, '2016-12-20 19:55:57', '2016-12-20 19:55:57'),
(11, 1, 11, '2016-12-20 19:56:12', '2016-12-20 19:56:12'),
(12, 1, 13, '2016-12-20 19:56:23', '2016-12-20 19:56:23'),
(13, 1, 12, '2016-12-20 19:56:42', '2016-12-20 19:56:42'),
(14, 5, 1, '2016-12-20 22:50:10', '2016-12-20 22:50:10'),
(15, 5, 6, '2016-12-20 22:50:22', '2016-12-20 22:50:22'),
(16, 6, 7, '2016-12-20 22:50:50', '2016-12-20 22:50:50'),
(17, 6, 8, '2016-12-20 22:51:02', '2016-12-20 22:51:02'),
(18, 4, 2, '2016-12-20 22:53:23', '2016-12-20 22:53:23'),
(19, 1, 14, '2017-01-14 00:00:00', '2017-01-14 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarjetacreditos`
--

CREATE TABLE `tarjetacreditos` (
  `id` int(11) NOT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `tipotarjeta_id` int(11) NOT NULL,
  `num_tajeta` varchar(45) NOT NULL,
  `vencimiento` date NOT NULL,
  `nombre_segun_tarjeta` varchar(45) NOT NULL,
  `cuatro_ultimos` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tarjetacreditos`
--

INSERT INTO `tarjetacreditos` (`id`, `cliente_id`, `tipotarjeta_id`, `num_tajeta`, `vencimiento`, `nombre_segun_tarjeta`, `cuatro_ultimos`) VALUES
(2, 23, 1, '5254 8908 5326 6944', '2017-05-17', 'PIBERERNUS LEANDRO ', '133');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoclientes`
--

CREATE TABLE `tipoclientes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `denominacion` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipoclientes`
--

INSERT INTO `tipoclientes` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'Particular', '2016-10-21 12:05:54', '2016-10-21 12:05:54'),
(2, 'Agencia', '2016-10-21 12:06:06', '2016-10-21 12:06:06'),
(3, 'Coorporativo', '2016-10-21 12:06:20', '2016-10-21 12:06:20'),
(4, 'PROPIETARIO', '2017-03-08 13:13:06', '2017-03-08 13:13:06'),
(5, 'Empleados', '2017-04-25 18:28:33', '2017-04-25 18:28:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoclientesubs`
--

CREATE TABLE `tipoclientesubs` (
  `id` int(11) NOT NULL,
  `tipocliente_id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `cuit` varchar(45) NOT NULL,
  `contacto_ref` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `telf_fijo` varchar(45) DEFAULT NULL,
  `telf_celular` varchar(45) NOT NULL,
  `direccion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipoclientesubs`
--

INSERT INTO `tipoclientesubs` (`id`, `tipocliente_id`, `nombre`, `cuit`, `contacto_ref`, `correo`, `telf_fijo`, `telf_celular`, `direccion`) VALUES
(3, 1, 'PARTICULAR', '0', '0', '0', '0', '0', '0'),
(4, 2, 'DESPEGAR.COM PAGO EN DESTINO ', '30-70130711-5', 'DESPEGAR ', 'VANINA OLIVERA ', '', '011- 123456789', 'BUENOS AIRES '),
(5, 2, 'DESPEGAR.COM CTA CTE ', '30-70130711-5', 'VANINA OLIVERA ', 'VANINA OLIVERA ', '', '011 123456789', 'BUENOS AIRES '),
(6, 2, 'SOUTH NET TURISMO CTA CTE', '30-70721021 - 0 ', 'Julian ', 'south net ', '011 - 111111111', '011 11111111', 'BUENOS AIRES '),
(7, 2, 'STOPPEL GABRIEL  - PREPAGO ', '20 - 17658206 - 6', 'ELBIO STOPPEL ', 'operaciones1@stoppelviajes.com', '011 - 4325 4003', '011 11111111', 'MAIPU 484. PB DPTO D19. CAPITAL FEDERAL '),
(8, 2, 'HOTEL SOLUTIONS SA ', '30-71278220 - 6', 'Daniela Kanelson ', 'daniela@bebetterhotels.com', '', '011 11111111', 'BUENOS AIRES '),
(9, 3, 'SAN CRISTOBAL SMSG ', '34 - 50004533 - 9', 'SILVIO PIZARRO ', 'PizarroS@sancristobal.com.ar', '0261 - 5209400 ', '0261 5360276', 'San Martin 621 Mendoza. Capital '),
(10, 3, 'YPF SOCIEDAD ANONIMA', '30 - 54668997 - 9 ', 'NANCY CARRIZO ', 'NCARRIZO@REPSOLYPF.COM', '011 - 449 7606', 'NNNN', 'MACACHA GUEMES 515 CAPITAL FEDERAL '),
(11, 2, 'TOURICO HOLIDAYS', '1111111111111', 'CAROLINA STERNBERG ', 'NNNN', 'NNNN', 'NNNN', 'NN'),
(15, 2, 'CLUB TURAVIA SA DE CV', 'CTU940107CK8', 'NICOLAS OLIVIERI ', 'ONLINE.LATAM@HBDS.COM', '0052 998 881 75 28 ', '0052 998 881 75 28 ', 'AV TULUM 286 SM8 MZA2 '),
(16, 3, 'AMERICAN JET ', '30 - 62016517 - 0 ', 'SABRINA ', 'comercial@americanjet.com.ar', '011 - 155054 7330', '011 - 155054 7330', 'FLORIDA 890 20B. CABA. '),
(17, 2, 'TURAR S.A. PREPAGO', '3055960726-2', 'Gabriel Nariccio', 'gabriel@turar.com', '011 4103 3149', '011 4103 3149', 'Sarmiento 580, 1Âº piso of. 106, Buenos Aires'),
(18, 2, 'MAXXI TOUR ', '2222222222', 'ARIEL BECK ', 'ARIEL@MAXXITOURS.COM.BR', '0055 54 30 28 06 19 ', '0055 54 96 42 00 38 ', 'Rua Botafogo 1792. Caxia do Sul. Brasil '),
(19, 2, 'NAVARRO MARCELO ', '23 - 2317573119 - 9', 'NAVARRO MARCELO ', 'leandro@hontravel.com', '111111111', '11111111111', 'AMIGORENA 56 DTO 28 MENDOZA '),
(20, 2, 'PATAGONIA TRAVEL ', '111112135465465', 'SEBASTIAN NUÃ‘EZ / KARINA NUÃ‘EZ / ROXANA NUÃ', 'KARINAPATAGONIA@TERRA.COM.BR', '0055 5192697669', '0055 5192697669', 'BRASIL '),
(21, 2, 'AMICHI VIAJES ', '26666666666666', 'ELIANA SINCOVICH ', 'RECEPCION@AMICHI.COM.AR', '011 - 41315000', '011 - 41315000', '25 DE MAYO 555. PISO 15. CABA. '),
(22, 2, 'TURISMO AYMARA SRL ', '30 - 64612485 - 5 ', 'MARTIN GARCIA ', 'RECEPTIVO@AYMARA.COM.AR', '4202064', 'NN', 'NNN'),
(23, 3, 'MINISTERIO DE SEGURIDAD DE LA NACION', '999999999999999999999', 'MARIELA SANTARELLI', 'marielasantarelli@gmail.com', '011- 156 18 06 869', '011- 156 18 06 869', 'nnnn '),
(24, 2, 'ASATEJ SRL.', '30 - 65951462 - 8', 'DANIELA DA SILVA ', 'daniela.dasilva@almundo.com', '011 - 53541200 int 1359', '011 - 53541200 int 1359', 'FLORIDA 825 CABA '),
(25, 2, 'PLAY PATAGONIA ', '55555555555555555555', 'CARLA CENTENO ', 'carla.centeno@playpatagonia.com', '(011) 2150-1900 - Interno: 105', '11 3.462.5735 ', 'DirecciÃ³n: Av. del Libertador 6680 - piso 7 - (CP1428) - Belgrano - Bs. As.'),
(26, 3, 'ZUCAMOR CUYO ', '30 - 60629511 - 8', 'ALEJANDRA BORGOGNO ', 'Alejandra.Borgogno@grupozucamor.com.ar', '011 - 4365 8199 / 011 - 4223 1012', '011 - 4365 8199 / 011 - 4223 1012', 'MOREAU DE JUSTO 170 P.1 D.7. CABA. BS AS '),
(27, 2, 'PATAGONIA ROAD ', '44444444444444444', 'SILVIA WEYS ', 'sylviaw@patagonia-road.com', '0294 - 4448579', '0294 - 4448579', 'BARILOCHE. ARGENTINA '),
(28, 2, 'NEW WORLD TRAVEL DESIGNERS S.A.', '3071102740-4', 'Lilian Broekhuisen', 'lilian@newworld.travel', '011 4314 1360', '011 4314 1360', 'Marcelo T. de Alvear, 10th Floor - C1058AAM, Buenos Aires, Argentina'),
(29, 2, 'CIENITOURS', '3074589621-1', 'CAROLINA ARJONES', 'carolinaarjones1@gmail.com', '52722220', '52722220', 'www.cienitours.com'),
(30, 2, 'CLUB SAN JORGE ', '666666666666666', 'NN', 'NN', 'NN', 'NN', 'NN'),
(31, 2, 'SERVICIOS DE VIAJES Y TURISMO BIBLOS', '3057717092-0', 'VERONICA SALA', 'vsala@biblostravel.com', '011 5031 7799 int. 7518', '011 5031 7799 int. 7518', 'Av. Chiclana 3345 8Â° (C1260ACA) Buenos Aires, Argentina'),
(32, 2, 'GRUPO OCHO', '3070823764-3', 'MAGALHAES JUAN CARLOS', 'juan@grupo8.com.ar', '+5411 5276-5000 / 4393-5544 ', '+5411 5276-5000 / 4393-5544 ', 'San MartÃ­n 663 6ÂºPiso (C1004AAM) Ciudad de Buenos Aires - Argentina '),
(33, 3, 'DEL BARCO RAUL CEFERINO', '2017211836-5', 'Raul del Barco', 'raul.delbarco@aero.tur.ar', '011 4747 1128', '011 15 3606 6699', 'San Isidro'),
(34, 3, 'PRICE WATERHOUSE & CO ASES EMP SRL', '30-70864208-4', 'HEYDI DOMINGUEZ', 'heydi.dominguez.palmer@ar.pwc.com', '(+5411) 4850-6163 ', '(+5411) 4850-6163', 'Bouchard 557 Piso 8Â° | (C1106ABG) Ciudad de Buenos Aires | Argentina'),
(35, 3, 'TRANSPORTADORA DE GAS DEL NORTE S.A.', '30-65786305-6', 'CELMAN SEBASTIAN', 'sebastian.celman@tgn.com.ar', '0346415689915', '0346415689915', 'santa fe'),
(36, 3, 'ASOCIACION MUTUAL JERARQUICOS SALUD', '30-68695518-0', 'SALIERNO FIORELA', 'fsalierno@jerarquicos.com', '4', '4', '4'),
(37, 3, 'FUNDACIÃ“N ALTA DIRECCIÃ“N', '30-68418409-8', 'GENELICH LAURA', 'lgenelich@aden.org', '1', '1', '1'),
(38, 3, 'ROHLIG', '123456789', 'LA BARBA VANINA', 'vanina.barba@rohlig.com', '01152750505', '011 52750505', '. Leandro N. ALem 1002 Piso 2 | 1001 â€“ Ciudad AutÃ³noma de Buenos Aires | Argentina'),
(39, 3, 'BANCO DE LA NACIÃ“N ARGENTINA', '30-50001091-2', 'MARIA ESTHER SANTI', '2400ADM@BNA.COM.AR', '0261 449 9860', '0261 449 9860', 'Argentina'),
(40, 2, 'HUENTATA', '1', 'SALVI GUSTAVO', 'gustavosalvi@huentata.tur.ar', '02614258950', '0261 155324087', 'colon 531 piso 3 of. 1'),
(41, 2, 'TURISMO PECOM SACFI', '30-54678349-5', 'GONGORA EDUARDO DANIEL DIAZ', 'ediazg@turismo-pecom.com.ar', '011 4324 4400 int. 4438', '011 4324 4400 int. 4438', 'Av.Corrientes 880 10Âº Piso (C1043AAV) Buenos Aires, Argentina'),
(42, 3, 'BIOPROFARMA S.A.', '3064936404-0', 'MATIAS GONZALEZ', 'mgonzalez@bioprofarma.com', '011 40166200 int 126', '011 40166200 int 126', 'TERRADA 1270 / C.A.B.A.'),
(43, 3, 'NALCO ARGENTINA SRL', '3058737529-6', 'MARIANA VIDELA', 'mariana.videla@nalco.com', '0299 4482362 int 103', '0299  154279993', 'MAESTROS NEUQUINOS 1190 ESQ. LELOIR 7Âº PISO'),
(44, 2, 'VIAJES FALABELLA S.A.', '3070737343-8', 'EVELYN ALORI', 'ealori@falabella.com.ar', '011 5777 4100 int 7053908', '011 5777 4100 int 7053908', '011 5777 4100 int 7053908'),
(45, 3, 'PROVINCIA ASEGURADORA DE RIESGOS DE TRABAJO', '3068825409-0', 'MARCELA INES GUTIERREZ', 'magutierrez@`provart.com.ar', '5411 4819 2800 int. 4612', '5411 4819 2800 int. 4612', 'Carlos Pellegrini 91 Piso 4 | C1009ABA '),
(46, 2, 'PLAY PATAGONIA S.R.L.', '3071068757-5', 'OROFINO VERONICA', 'veronica.orofino@playpatagonia.com', '(011) 2150-1900 - Interno: 132/133', '(011) 2150-1900 - Interno: 132/133', 'Av. del Libertador 6680 - piso 7 - (CP1428) - Belgrano - Bs. As.'),
(47, 2, 'BCD TRAVEL ', '3053128401-8', 'DANIELA TOPPINO', 'daniela.toppino@bcdtravel.com.ar', '011 5550 772', '011 55507 859', 'Cnel Intend A. Avalos 2829 piso 1 dpto C'),
(48, 3, 'RIO URUGUAY COOP. SEGUROS LIMITADA', '3050006171-1', 'PABLO DI BARI', 'pablo.dibari@riouruguay.com.ar', '0261 4203540 / 3541', '0261 156 771 668', 'Mendoza'),
(49, 3, 'GRUAS ARGENTINAS S.A.', '30-70945338-2', 'MONICA PENZ', 'info@gruasargentinassa.com.ar', '0261-4319990-4315515', '0261-4319990-4315515', 'Mendoza'),
(50, 3, 'FEDERACION PATRONAL SEGUROS', '3370736658-9', 'NOELIA GIULIANO', 'ngiuliano@fedpat.com.ar', '54 261 4494700', '54 261 4494700', '25 de mayo 1258 - Ciudad de Mendoza'),
(51, 3, 'ACINDAR IND. ARGENTINA DE ACEROS S.A.', '3050119925-3', 'ALEJANDRO DAFFADA', 'alejandro.daffada@arcelormittal.com.ar', '54 341 4596135', '54 341 4596121', 'RUTA PCIAL 21 KM 247'),
(52, 3, 'GBT II ARGENTINA S.R.L.', '3071446660-3', 'Florencia Tempone', 'amexbuejohnson@aexp.com', '+54 11 4789 7200 int. 333', '+54 11 4789 7200 int. 333', 'BS AS'),
(53, 3, 'GRUPO PEÃ‘AFLOR S.A.', '3050054804 - 1', 'JUAN JOSE MUÃ‘OZ', 'jmunoz@grupopenaflor.com.ar', '(54 11) 5198 8068', '(54 11) 5198 8068', 'Arenales 460 - Vicente LÃ³pez - Pcia. Buenos Aires - Argentina'),
(54, 2, 'BERKLEY INTERNATIONAL SEGUROS', '3050003578-8', 'ANA EUGENIA CORRALES', 'acorrales@berkley.com.ar', '0261 4290470 / 4230397', '0261 4290470 / 4230397', 'EspaÃ±a 818 - Mendoza, Argentina'),
(55, 2, 'WEDELL TRAVEL', '123', 'FEDERICO GIGENA SOBRERO', 'fg@wedelltravel.com', '54-11-52580933/34', '54-11-52580933/34', 'Marcelo T de Alvear 1261 2do 35 - Capital Fed'),
(56, 4, 'PROPIETARIO', '123456789', 'BERTONA JUAN', 'juanbertona@yahoo.com.ar', '123', '123', '123'),
(57, 2, 'CORDITUR', '145221456', 'Tamara Yamila Ulrich', 'tulrich@corditur.com', 'Tel: 4222279 - 4222255', 'Cel: (343) 154662347', 'Uruguay 113 - (3100) ParanÃ¡. Entre RÃ­os'),
(58, 2, 'INELTUR S.R.L.', '3064254425-6', 'MARIA JOSE JARA', 'mjjara@argentinahtl.com', '(54) (261) 429 7256 /57 /58', '(54) (261) 4259600 / 4232380', 'Granaderos 998  Mendoza - Argentina'),
(59, 3, 'EXSA S.R.L.', '30697690605-5', 'MARIELA LIROSI', 'administracioncuyo@exsa-srl.com.ar', 'Tel: 2614312740', 'Cel: 261-5979219', 'ALEM  620  Pta  Baja  Dto  1  Coronel Dorrego -Guaymallen -Mendoza'),
(60, 3, 'INDIGO CONSULTING S.A.', '3070817886-8', 'Carolina', 'administracion@indigoconsulting.com.ar', '011 1560168106', '011 1560168106', 'BS AS'),
(61, 3, 'MONTE VERDE S.A.', '3070074314-0', 'ADRIANA RIVERO MESTRE', 'adriana.rivero@bioderma.com.ar', '011 4509 7100', '011 4509 7100', 'RUTA 40 KM 155'),
(62, 3, 'TELECOM ARGENTINA S.A.', '3063945373-8', 'MUHAFRA GISELLE ', 'hotelestelecom@ttsviajes.com', '+54 11 2206 7801', '+54 11 2206 7801', 'Buenos Aires'),
(63, 2, 'AERO SRL', '3070736214-2', 'ADRIANA GIUNTA', 'adriana.giunta@aerolaplata.com.ar', '011 5352 0557', '011 5352 0557', 'calle 48'),
(64, 3, 'ANDINUS S.A.', '3071130264-2', 'JUAN VERBITSKY', 'juan_verbitsky@andinus.com', '011 1544022942', '011 1544022942', 'ALMIRANTE BROWN'),
(65, 3, 'BIOSIDUS S.A.', '3059811709-4', 'JOSE LUIS ARCE', 'j.arce@biosidus.com.ar', '0351 157 199622', '0351 157 199622', 'Quiroga 90 , Monte Cristo, CÃ³rdoba'),
(66, 3, 'HERRAJES BAUZA S.A.', '212133124561294', 'SALVADOR CURRAO', 'compras@herrajesbauza.com.ar', '+54 (0)261 4377557', '+54 (0)261 4377557', 'DamiÃ¡n Hudson 380, Mendoza'),
(67, 3, 'LAN AIRLINES S.A.', '3060962072 - 9', 'STEPHANIE FRANCO', 'central.reservasAR@lan.com', '0810 9999 526', '0810 9999 526', 'OBLGADO RAFAEL'),
(68, 3, 'EDENRED ARGENTINA S.A.', '3062360867-7', 'ELSA OLDACH', 'elsa.oldach@edenred.com', '(54-11) 4909-1211 ', '(54-11) 4909-1211 ', 'Av. Corrientes 316, 6Â° piso - C1043AAQ - CABA - Argentina '),
(69, 2, 'AUTOMOVIL CLUB ARGENTINO', '3050014329-7', 'ORLANDINI MATIAS', 'matias_orlandini@aca.org.ar', '011 0800 888 3777', '011 0800 888 3777', 'www.aca.org.ar/servicios/turismo'),
(70, 2, 'QUIAN TOURS', '1', 'Pablo Polita', 'pablop@quiantours.tur.ar', '5252 2256', '+5491170392499', 'C:A.B.A: Esmeralda 779 1Â° of 8'),
(71, 2, 'ARGENWAY SRL', '3071230888-1', 'ROMINA ASTUDILLO', 'romina@argenway.com', 'Tel: 54 9 11 5235-6916', '1553119110', 'Defensa 269 1er piso Lofty 4 | CABA'),
(72, 2, 'LUXOR TOUR', '2', 'JUDITH MARIN', 'operaciones@luxor.tur.ar', 'Tel. (00-54) 11-4382-1010  ', 'Tel. (00-54) 11-4382-1010  ', 'E.V.T. Leg.7095 - Lavalle 1125 Piso 5Â° Of.12  (1048) Capital Federal '),
(73, 2, 'TURISMO SEPEAN DE ANDIA, J. ALBERTO', '2012117915-7', 'Eduardo', 'reservasmdz1@sepean.com', '0261 4204162', '0261 4543530', 'Primitivo de la Reta 1088 - Mendoza'),
(74, 2, 'VIAJES VERGER S.A.', '3054006641-4', 'LEGASPI GRACIELA', 'g.legaspi@barceloviajes.tur.ar', '+54 11 43165258', '+54 11 4316 5200', 'Suipacha 570 Piso 9Âº - Oficina B  (C1008AAL) Buenos Aires â€“ Argentina '),
(75, 2, 'VIAJES VERGER S.A.', '3054006641-4', 'LEGASPI GRACIELA', 'g.legaspi@barceloviajes.tur.ar', '+54 11 43165258', '+54 11 4316 5200', 'Suipacha 570 Piso 9Âº - Oficina B  (C1008AAL) Buenos Aires â€“ Argentina '),
(76, 2, 'TURYSOL', '123456', 'Cintia Molina', 'turysol@hotmail.com', 'Tel 0379 4468444', 'Tel 0379 4468444', 'Suc. Corrientes Junin 1630'),
(77, 2, 'HON TRAVEL', '23-17573119-9', 'Marcelo Navarro', '123', '123', '123', '123'),
(78, 2, 'CLARAMINTE TRAVEL', '484989849849849', 'HERNAN ARISTI', 'operaciones3@claramintetravel.com', '(+54) 11 5218-1501/06 (lÃ­neas rotativas)', '(+54) 11 5218-1501/06 (lÃ­neas rotativas)', 'Sarmiento 930 -1Âº Piso Of. \"B\" - (C1041AAT) Buenos Aires, Argentina.'),
(79, 3, 'WAIR TECH S.A.', '30-70814311-8', 'Walter Bifulco', 'wbifulco@wairtechsa.com.ar', '0111551853140', '0111551853140', 'Argentina'),
(80, 2, 'JULIA TOURS', '3057685676-4', 'GUIDO BELLA', 'nacional@juliatours.com.ar', '54 11 43256029', '54 11 43256029', '123'),
(81, 3, 'SANTIANO BROTHERS', '123456789', 'SANTIANO', 'mike@santianobrothers.com', '1233', '1245', '124534'),
(82, 3, 'LIBERTY SEGUROS ', '30-70496198-3', 'SONIA MORILLO', 'smorillo@integrityseguros.com.ar', '(0261) 4200116', '(0261) 4200116', 'Montevideo 456  - 4to. Piso - Mendoza (M5500GKF)'),
(83, 3, 'CORREO OFICIAL REP. ARGENTINA', '30-70857483-6', 'CLAUDIA CHIA', 'CCHIA@CORREOARGENTINO.COM.AR', '0261 4499579', '156260006', 'Jefatura de Operaciones RegiÃ³n Oeste'),
(84, 2, 'DESTINOS AUSTRALES', '123', '123', '123', '123', '123', '123'),
(85, 3, 'JEM ASOCIADOS SRL', '1234', '1234', '1234', '1234', '1234', '1234'),
(86, 2, 'IDEIA', '12314', 'Sergio DÃ­as', 'hoteles.ideiaturismo@gmail.com', '55 4836280129', '55 4836280129', 'Brasil'),
(87, 2, 'JUDITH TOURS', '1231445', '1231445', '1231445', '1231445', '1231445', '1231445'),
(88, 2, 'UNI EXPLORER', '876', '876', '876', '876', '876', '876'),
(89, 3, 'CIRCULO ODONTOLOGICO DEL ESTE', '908', '08', '908', '098', '8', '88'),
(90, 2, 'OLA S.A.', '098', '098', '098', '098', '098', '098'),
(91, 3, 'FUNDACION PRO MENDOZA ', '?', '?', '?', '?', '?', '?'),
(92, 2, 'VOYENBUS', '?', '?', '?', '?', '?', '?'),
(93, 2, 'TURYSOL', '?', 'Sergio', '?', '?', '?', '?'),
(94, 3, 'IADEF', '?', '?', '?', '?', '?', '?'),
(95, 2, 'ALVAN TURISMO', '?', '?', '?', '?', '?', '?'),
(96, 2, 'ROBITAR S.A.', '30-71068580-7', 'GUSTAVO SALVI', 'reservas@huentata.com.ar', '4258950', '4290518', 'gustavosalvi@huentata.tur.ar'),
(97, 2, 'CATA TURISMO', '?', '?', '?', '?', '?', '?'),
(98, 3, 'UNIVERSIDAD NACIONAL DE CUYO', '30-5466946-3', '?', '?', '?', '?', '?'),
(99, 2, 'BEATRIZ CABRERA VIAJES Y TURISMO S.A.', '30-70722278-2', 'Mauricio Ruiz', 'mruiz@beatrizcabrera.com', '4296637', '4296637', 'caja@beatrizcabrera.com'),
(100, 2, 'ANDES TRAVEL ', '?', 'CLAUDIO MARTINEZ', 'andestravelagency@gmail.com', '2615501323', '2615501323', 'andestravelagency@gmail.com'),
(101, 3, 'MERCEDES NEVARES', '?', '?', 'mdenevares@apdes.edu.ar', '011 47626115', '011 47626115', 'mdenevares@apdes.edu.ar'),
(102, 2, 'CARTAGENA VIAJES ', '?', '?', '?', '?', '?', '?'),
(103, 2, 'PANDO ELIANA KARIN', '27-28184358-9', 'Eliana Pando', 'consultas@mendozaunlugar.com.ar', '4394432', '152150814', 'Dr. ANGEL ROFFO 225'),
(104, 3, 'UNIVERSIDAD DE MENDOZA', '30-51859446-6', 'OMAR SALOMON', 'silvia.viggiani@um.edu.ar', '4202017 int. 123 / int. 219', '4202017 int. 123 / int. 219', 'Boulougne Sur Mer 665'),
(105, 2, 'VIACOR S.A.', '30-67639584-5', '?', 'mlucero@nites.travel', '4253804', '4253804', 'receptivo@nites.travel'),
(106, 2, 'ALMUNDO.COM SRL', '3065951462-8', '?', '?', '?', '?', '?'),
(107, 3, 'RADIO MITRE', '?', '?', '?', '?', '?', '?'),
(108, 3, 'IMPROVING IDEAS S.A.', '30-71162370-8', 'Wider Gonzalo', 'N', 'N', 'N', 'N'),
(109, 3, 'LATIN AMERICAN WINGS', '30-71568600-3', 'LEONARDO LOZA', 'leonardo.loza@vuelalaw.com', '156922120', '156922120', '9 de julio 1257');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipocompras`
--

CREATE TABLE `tipocompras` (
  `id` int(11) NOT NULL,
  `denominacion` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipohabitaciones`
--

CREATE TABLE `tipohabitaciones` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `denominacion` varchar(30) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipohabitaciones`
--

INSERT INTO `tipohabitaciones` (`id`, `denominacion`, `created`, `modified`) VALUES
(2, 'EJECUTIVO SUPERIOR', '2016-12-20 07:40:46', '2016-12-20 07:42:23'),
(3, 'EJECUTIVO NORMAL', '2016-12-20 07:40:54', '2017-04-25 18:34:25'),
(4, 'SUITE ESTANDAR', '2016-12-20 07:41:43', '2016-12-20 07:41:43'),
(5, 'SUITE SUPERIOR', '2016-12-20 07:42:59', '2016-12-20 07:42:59'),
(6, 'PREMIUM ESTANDAR', '2016-12-20 07:43:10', '2016-12-20 07:43:10'),
(7, 'PREMIUM SUPERIOR', '2016-12-20 07:43:19', '2016-12-20 07:43:19'),
(8, 'SALÃ“N DE EVENTOS', '2017-09-18 19:34:17', '2017-09-18 19:34:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoimpresoras`
--

CREATE TABLE `tipoimpresoras` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `denominacion` varchar(300) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipoimpresoras`
--

INSERT INTO `tipoimpresoras` (`id`, `denominacion`, `created`, `modified`) VALUES
(1, 'Epson TM-T285F ', '2016-10-20 23:40:17', '2016-10-20 23:45:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipotarjetas`
--

CREATE TABLE `tipotarjetas` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipotarjetas`
--

INSERT INTO `tipotarjetas` (`id`, `descripcion`) VALUES
(1, 'Visa'),
(2, 'Mastercard'),
(3, 'American Express'),
(4, 'Diners Club');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipotemporadas`
--

CREATE TABLE `tipotemporadas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `denominacion` varchar(300) NOT NULL,
  `fechadesde` date NOT NULL,
  `fechahasta` date NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipotemporadas`
--

INSERT INTO `tipotemporadas` (`id`, `denominacion`, `fechadesde`, `fechahasta`, `created`, `modified`) VALUES
(3, 'CARNAVAL', '2017-02-25', '2017-02-28', '2016-12-20 08:39:24', '2016-12-20 08:39:24'),
(4, 'SEMANA SANTA', '2017-04-13', '2016-12-16', '2016-12-20 08:39:47', '2016-12-20 08:39:47'),
(5, 'FIN DE SEMANA LARGO MARZO 2017', '2017-03-03', '2017-03-05', '2016-12-20 08:41:17', '2016-12-20 08:41:17'),
(6, 'FIN DE SEMANA LARGO MARZO II 2017', '2017-03-24', '2017-03-26', '2016-12-20 08:42:46', '2016-12-20 08:42:46'),
(7, 'FIN DE SEMANA LARGO AGOSTO 2017', '2017-08-19', '2017-08-21', '2016-12-20 08:44:19', '2016-12-20 08:44:19'),
(8, 'FIN DE SEMANA LARGO OCTUBRE', '2017-10-07', '2017-10-09', '2016-12-20 08:44:51', '2016-12-20 08:44:51'),
(9, 'FIN DE SEMANA LARGO NOVIEMBRE 2017', '2017-11-25', '2017-11-27', '2016-12-20 08:45:19', '2016-12-20 08:45:19'),
(10, 'FIN DE SEMANA LARGO DICIEMBRE 2017', '2017-12-08', '2017-12-10', '2016-12-20 08:45:48', '2016-12-20 08:45:48'),
(11, 'FIN DE SEMANA LARGO DICIEMBRE NAVIDAD 2017', '2017-12-23', '2017-12-25', '2016-12-20 08:46:27', '2016-12-20 08:46:27'),
(12, 'TEMPORADA BAJA', '2017-01-01', '2017-12-31', '2016-12-20 08:47:47', '2016-12-20 08:47:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `userestados`
--

CREATE TABLE `userestados` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `estado` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `userestados`
--

INSERT INTO `userestados` (`id`, `estado`) VALUES
(1, 'ACTIVO'),
(2, 'VENCIDO'),
(3, 'INACTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(20) NOT NULL,
  `role_id` int(11) NOT NULL,
  `personale_id` int(11) NOT NULL DEFAULT '0',
  `username` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `modified` date NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `role_id`, `personale_id`, `username`, `password`, `email`, `website`, `image`, `bio`, `status`, `modified`, `created`) VALUES
(1, 1, 4, 'root', 'c33367701511b4f6020ec61ded352059', '', NULL, NULL, NULL, 1, '2017-09-18', '2016-10-17 01:22:00'),
(2, 1, 2, 'FCRUZ', '8d7ba9aa6cecde3c4d36f61a7f535330', '', NULL, NULL, NULL, 1, '2017-09-18', '2017-09-18 18:52:08'),
(3, 1, 2, 'afernandez', '2075f637cc023474f6410fd419a6a45d', '', NULL, NULL, NULL, 1, '2017-09-18', '2017-09-18 19:00:37'),
(4, 1, 2, 'rcastillo', 'c1634df7cb4b0e058f192b5452239139', '', NULL, NULL, NULL, 1, '2017-09-18', '2017-09-18 19:01:18'),
(5, 1, 2, 'efilippini', '7085bf24297b6c12331860e8700f0a1f', '', NULL, NULL, NULL, 1, '2017-09-18', '2017-09-18 19:02:41'),
(7, 1, 2, 'lcano', '2075f637cc023474f6410fd419a6a45d', '', NULL, NULL, NULL, 1, '2017-09-18', '2017-09-18 20:17:39'),
(8, 1, 2, 'laura', '13a1e05d1341967d6c812563851a19b0', '', NULL, NULL, NULL, 1, '2017-09-18', '2017-09-18 20:19:30');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bodymail`
--
ALTER TABLE `bodymail`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cajaacierreturnos`
--
ALTER TABLE `cajaacierreturnos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `cajaaperturas`
--
ALTER TABLE `cajaaperturas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `cajacierredias`
--
ALTER TABLE `cajacierredias`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `cajas`
--
ALTER TABLE `cajas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `cajaturnos`
--
ALTER TABLE `cajaturnos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `dni` (`dni`);

--
-- Indices de la tabla `compraabonas`
--
ALTER TABLE `compraabonas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `compraproductos`
--
ALTER TABLE `compraproductos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `compratipopagos`
--
ALTER TABLE `compratipopagos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `conftipopagoreservas`
--
ALTER TABLE `conftipopagoreservas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `conftipopagos`
--
ALTER TABLE `conftipopagos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `consumoproductos`
--
ALTER TABLE `consumoproductos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `consumos`
--
ALTER TABLE `consumos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `ctacorrientes`
--
ALTER TABLE `ctacorrientes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `ctastatus`
--
ALTER TABLE `ctastatus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `facturadetalles`
--
ALTER TABLE `facturadetalles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `facturapagos`
--
ALTER TABLE `facturapagos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `facturas`
--
ALTER TABLE `facturas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `facturatipopagos`
--
ALTER TABLE `facturatipopagos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `facturatipoproductos`
--
ALTER TABLE `facturatipoproductos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `habistatus`
--
ALTER TABLE `habistatus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `habitaciones`
--
ALTER TABLE `habitaciones`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `impresiones`
--
ALTER TABLE `impresiones`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `modulos`
--
ALTER TABLE `modulos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `personales`
--
ALTER TABLE `personales`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proalmacenes`
--
ALTER TABLE `proalmacenes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `proinventarios`
--
ALTER TABLE `proinventarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `proivas`
--
ALTER TABLE `proivas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `promarcas`
--
ALTER TABLE `promarcas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `proproductos`
--
ALTER TABLE `proproductos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `protipopisos`
--
ALTER TABLE `protipopisos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `protipos`
--
ALTER TABLE `protipos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `pstemporadas`
--
ALTER TABLE `pstemporadas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `reserindividualeextras`
--
ALTER TABLE `reserindividualeextras`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `reserindividuales`
--
ALTER TABLE `reserindividuales`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `reserindivistatus`
--
ALTER TABLE `reserindivistatus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `resermulhabitaciones`
--
ALTER TABLE `resermulhabitaciones`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `resermultiples`
--
ALTER TABLE `resermultiples`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `resermultistatus`
--
ALTER TABLE `resermultistatus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `reserstatusindividuales`
--
ALTER TABLE `reserstatusindividuales`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `reserstatusmultiples`
--
ALTER TABLE `reserstatusmultiples`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role_alias` (`alias`);

--
-- Indices de la tabla `rolesmodulos`
--
ALTER TABLE `rolesmodulos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `tarjetacreditos`
--
ALTER TABLE `tarjetacreditos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipoclientes`
--
ALTER TABLE `tipoclientes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `tipoclientesubs`
--
ALTER TABLE `tipoclientesubs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipocompras`
--
ALTER TABLE `tipocompras`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipohabitaciones`
--
ALTER TABLE `tipohabitaciones`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `tipoimpresoras`
--
ALTER TABLE `tipoimpresoras`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `tipotarjetas`
--
ALTER TABLE `tipotarjetas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipotemporadas`
--
ALTER TABLE `tipotemporadas`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `userestados`
--
ALTER TABLE `userestados`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bodymail`
--
ALTER TABLE `bodymail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `cajaacierreturnos`
--
ALTER TABLE `cajaacierreturnos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `cajaaperturas`
--
ALTER TABLE `cajaaperturas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `cajacierredias`
--
ALTER TABLE `cajacierredias`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cajas`
--
ALTER TABLE `cajas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `cajaturnos`
--
ALTER TABLE `cajaturnos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1103;
--
-- AUTO_INCREMENT de la tabla `compraabonas`
--
ALTER TABLE `compraabonas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `compraproductos`
--
ALTER TABLE `compraproductos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `compratipopagos`
--
ALTER TABLE `compratipopagos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `conftipopagoreservas`
--
ALTER TABLE `conftipopagoreservas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `conftipopagos`
--
ALTER TABLE `conftipopagos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `consumoproductos`
--
ALTER TABLE `consumoproductos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `consumos`
--
ALTER TABLE `consumos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `ctacorrientes`
--
ALTER TABLE `ctacorrientes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `ctastatus`
--
ALTER TABLE `ctastatus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `empresas`
--
ALTER TABLE `empresas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `facturadetalles`
--
ALTER TABLE `facturadetalles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `facturapagos`
--
ALTER TABLE `facturapagos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `facturas`
--
ALTER TABLE `facturas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `facturatipopagos`
--
ALTER TABLE `facturatipopagos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `facturatipoproductos`
--
ALTER TABLE `facturatipoproductos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `habistatus`
--
ALTER TABLE `habistatus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `habitaciones`
--
ALTER TABLE `habitaciones`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT de la tabla `impresiones`
--
ALTER TABLE `impresiones`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `modulos`
--
ALTER TABLE `modulos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `personales`
--
ALTER TABLE `personales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `proalmacenes`
--
ALTER TABLE `proalmacenes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `proinventarios`
--
ALTER TABLE `proinventarios`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `proivas`
--
ALTER TABLE `proivas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `promarcas`
--
ALTER TABLE `promarcas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `proproductos`
--
ALTER TABLE `proproductos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `protipopisos`
--
ALTER TABLE `protipopisos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `protipos`
--
ALTER TABLE `protipos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pstemporadas`
--
ALTER TABLE `pstemporadas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `reserindividualeextras`
--
ALTER TABLE `reserindividualeextras`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `reserindividuales`
--
ALTER TABLE `reserindividuales`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `reserindivistatus`
--
ALTER TABLE `reserindivistatus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT de la tabla `resermulhabitaciones`
--
ALTER TABLE `resermulhabitaciones`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;
--
-- AUTO_INCREMENT de la tabla `resermultiples`
--
ALTER TABLE `resermultiples`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT de la tabla `resermultistatus`
--
ALTER TABLE `resermultistatus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `reserstatusindividuales`
--
ALTER TABLE `reserstatusindividuales`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `reserstatusmultiples`
--
ALTER TABLE `reserstatusmultiples`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `rolesmodulos`
--
ALTER TABLE `rolesmodulos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `tarjetacreditos`
--
ALTER TABLE `tarjetacreditos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tipoclientes`
--
ALTER TABLE `tipoclientes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `tipoclientesubs`
--
ALTER TABLE `tipoclientesubs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;
--
-- AUTO_INCREMENT de la tabla `tipocompras`
--
ALTER TABLE `tipocompras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tipohabitaciones`
--
ALTER TABLE `tipohabitaciones`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `tipoimpresoras`
--
ALTER TABLE `tipoimpresoras`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tipotarjetas`
--
ALTER TABLE `tipotarjetas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `tipotemporadas`
--
ALTER TABLE `tipotemporadas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `userestados`
--
ALTER TABLE `userestados`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
